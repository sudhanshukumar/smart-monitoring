package com.lnt.smartit.models;

public class AMQLogFileSize implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String fileName;
	private double fileSize;
	private double warningSizeLimit;
	private double criticalSizeLimit;
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public double getFileSize() {
		return fileSize;
	}
	public void setFileSize(double fileSize) {
		this.fileSize = fileSize;
	}
	public double getWarningSizeLimit() {
		return warningSizeLimit;
	}
	public void setWarningSizeLimit(double warningSizeLimit) {
		this.warningSizeLimit = warningSizeLimit;
	}
	public double getCriticalSizeLimit() {
		return criticalSizeLimit;
	}
	public void setCriticalSizeLimit(double criticalSizeLimit) {
		this.criticalSizeLimit = criticalSizeLimit;
	}
}
