package com.lnt.smartit.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class AlertMessage implements Serializable {
	private static final long serialVersionUID = 1L;
	private String alertMessageId;
	private String alertMessageName;
	private String alertMessageType;
	private String alertMessageHostName;
	private String alertMessageparameterName;
	private Date alertMessageDatetime;
	private String description;
	private AMQLogFileSize logFilesize;
	private AMQWinRAM winRAM;
	private AMQWinCPU winCPU;
	private AMQWinDisk winDisk;
	private AMQRAM linuxRAM;
	private AMQCPU linuxCPU;
	private AMQDisk linuxDisk;
	private AMQCEDError cedError;
	private AMQURLDOWN urlDown;
	private String osType;
	private String appName;
	private String region;
	private String platform;
	private String headers;
	private String display;
	private String id;
	private String itsmTktDescription;
	private Map<String,String> otherParams;
	
	/**
	 * This is used to know from which hazelcast map we should query 12 hours history data.
	 * This will be used to pass from UI to service which is giving 12 hours data
	 */
	private String hazelcastMapName;
	
	private List<String> serviceStatus = new ArrayList<String>();
	private List<String> logFileStatus = new ArrayList<String>();
	private AMQServerDown serverDown;
	private Response responsetime;
	
	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getHazelcastMapName() {
		return hazelcastMapName;
	}

	public void setHazelcastMapName(String hazelcastMapName) {
		this.hazelcastMapName = hazelcastMapName;
	}

	public String getAlertMessageId() {
		return alertMessageId;
	}

	public void setAlertMessageId(String alertMessageId) {
		this.alertMessageId = alertMessageId;
	}

	public String getAlertMessageName() {
		return alertMessageName;
	}

	public void setAlertMessageName(String alertMessageName) {
		this.alertMessageName = alertMessageName;
	}

	public String getAlertMessageType() {
		return alertMessageType;
	}

	public void setAlertMessageType(String alertMessageType) {
		this.alertMessageType = alertMessageType;
	}

	public String getAlertMessageHostName() {
		return alertMessageHostName;
	}

	public void setAlertMessageHostName(String alertMessageHostName) {
		this.alertMessageHostName = alertMessageHostName;
	}

	public String getAlertMessageparameterName() {
		return alertMessageparameterName;
	}

	public void setAlertMessageparameterName(String alertMessageparameterName) {
		this.alertMessageparameterName = alertMessageparameterName;
	}

	public Date getAlertMessageDatetime() {
		return alertMessageDatetime;
	}

	public void setAlertMessageDatetime(Date alertMessageDatetime) {
		this.alertMessageDatetime = alertMessageDatetime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public AMQLogFileSize getLogFilesize() {
		return logFilesize;
	}

	public void setLogFilesize(AMQLogFileSize logFilesize) {
		this.logFilesize = logFilesize;
	}

	public AMQWinRAM getWinRAM() {
		return winRAM;
	}

	public void setWinRAM(AMQWinRAM winRAM) {
		this.winRAM = winRAM;
	}

	public AMQWinCPU getWinCPU() {
		return winCPU;
	}

	public void setWinCPU(AMQWinCPU winCPU) {
		this.winCPU = winCPU;
	}

	public AMQWinDisk getWinDisk() {
		return winDisk;
	}

	public void setWinDisk(AMQWinDisk winDisk) {
		this.winDisk = winDisk;
	}

	public AMQRAM getLinuxRAM() {
		return linuxRAM;
	}

	public void setLinuxRAM(AMQRAM linuxRAM) {
		this.linuxRAM = linuxRAM;
	}

	public AMQCPU getLinuxCPU() {
		return linuxCPU;
	}

	public void setLinuxCPU(AMQCPU linuxCPU) {
		this.linuxCPU = linuxCPU;
	}

	public AMQDisk getLinuxDisk() {
		return linuxDisk;
	}

	public void setLinuxDisk(AMQDisk linuxDisk) {
		this.linuxDisk = linuxDisk;
	}

	public AMQCEDError getCedError() {
		return cedError;
	}

	public void setCedError(AMQCEDError cedError) {
		this.cedError = cedError;
	}

	public AMQURLDOWN getUrlDown() {
		return urlDown;
	}

	public void setUrlDown(AMQURLDOWN urlDown) {
		this.urlDown = urlDown;
	}

	public String getOsType() {
		return osType;
	}

	public void setOsType(String osType) {
		this.osType = osType;
	}

	public List<String> getServiceStatus() {
		return serviceStatus;
	}

	public void setServiceStatus(List<String> serviceStatus) {
		this.serviceStatus = serviceStatus;
	}

	public List<String> getLogFileStatus() {
		return logFileStatus;
	}

	public void setLogFileStatus(List<String> logFileStatus) {
		this.logFileStatus = logFileStatus;
	}

	public AMQServerDown getServerDown() {
		return serverDown;
	}

	public void setServerDown(AMQServerDown serverDown) {
		this.serverDown = serverDown;
	}

	public Response getResponsetime() {
		return responsetime;
	}

	public void setResponsetime(Response responsetime) {
		this.responsetime = responsetime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getHeaders() {
		return headers;
	}

	public void setHeaders(String headers) {
		this.headers = headers;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Map<String, String> getOtherParams() {
		return otherParams;
	}

	public void setOtherParams(Map<String, String> otherParams) {
		this.otherParams = otherParams;
	}
	
	public String getItsmTktDescription() {
		return itsmTktDescription;
	}

	public void setItsmTktDescription(String itsmTktDescription) {
		this.itsmTktDescription = itsmTktDescription;
	}

	@Override
	public String toString() {
		return "AlertMessage [alertMessageId=" + alertMessageId
				+ ", alertMessageName=" + alertMessageName
				+ ", alertMessageType=" + alertMessageType
				+ ", alertMessageHostName=" + alertMessageHostName
				+ ", alertMessageparameterName=" + alertMessageparameterName
				+ ", alertMessageDatetime=" + alertMessageDatetime
				+ ", description=" + description + ", logFilesize="
				+ logFilesize + ", winRAM=" + winRAM + ", winCPU=" + winCPU
				+ ", winDisk=" + winDisk + ", linuxRAM=" + linuxRAM
				+ ", linuxCPU=" + linuxCPU + ", linuxDisk=" + linuxDisk
				+ ", cedError=" + cedError + ", urlDown=" + urlDown
				+ ", osType=" + osType + ", serviceStatus=" + serviceStatus
				+ ", logFileStatus=" + logFileStatus + ", serverDown="
				+ serverDown + ", responsetime=" + responsetime + ", hazelcastMapName="+hazelcastMapName+", id="+id+", headers="
				+ headers + ", display=" + display + ", itsmTktDescription="+itsmTktDescription+", otherParams=" + otherParams +"]";
	}
}