package com.lnt.smartit.services;

import java.util.List;

import com.lnt.smartit.mysql.model.NotificationGroup;
import com.lnt.smartit.mysql.model.ThresholdNotifyGroupMapping;

public interface NotificationGroupService {
	public String savenotificationGroup(NotificationGroup notificationGroup);
	public List<NotificationGroup> getAllNotificationGroup();
	public NotificationGroup getNotificationGroupById(NotificationGroup notificationGroup);
	public String deleteNotificationGroup(NotificationGroup notificationGroup);
	public List<ThresholdNotifyGroupMapping> getNotificationThresholdMappingById(String notificationGroupId);
	public void saveNotificationThresholdMapping(List<ThresholdNotifyGroupMapping> thresholdNotifyGroupMappingList);
}
