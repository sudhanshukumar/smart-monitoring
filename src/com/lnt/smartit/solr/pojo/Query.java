package com.lnt.smartit.solr.pojo;

import java.util.Arrays;
import java.util.List;

public class Query {
	
	private String url;
	private String collection;
	private String query;
	private int start;
	private int rows;
	private String filter;
	private long total_hits;
	private List<Field> fields;
	private List<String> results;
	private String sort_field;
	private String sort_order;
	private String[] selected_fields;
	private String chart;
	private String chart_filed;
	private String month;
	private String start_facet;
	private String end_facet;
	private String range_gap;
	private String field_value;
	private String chart_name;
	
	
	
	
	public String getChart_name() {
		return chart_name;
	}
	public void setChart_name(String chart_name) {
		this.chart_name = chart_name;
	}
	public String getField_value() {
		return field_value;
	}
	public void setField_value(String field_value) {
		this.field_value = field_value;
	}
	public String getRange_gap() {
		return range_gap;
	}
	public void setRange_gap(String range_gap) {
		this.range_gap = range_gap;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getStart_facet() {
		return start_facet;
	}
	public void setStart_facet(String start_facet) {
		this.start_facet = start_facet;
	}
	public String getEnd_facet() {
		return end_facet;
	}
	public void setEnd_facet(String end_facet) {
		this.end_facet = end_facet;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getChart() {
		return chart;
	}
	public void setChart(String chart) {
		this.chart = chart;
	}
	public String getChart_filed() {
		return chart_filed;
	}
	public void setChart_filed(String chart_filed) {
		this.chart_filed = chart_filed;
	}
	public String[] getSelected_fields() {
		return selected_fields;
	}
	public void setSelected_fields(String[] selected_fields) {
		this.selected_fields = selected_fields;
	}
	public String getSort_field() {
		return sort_field;
	}
	public void setSort_field(String sort_field) {
		this.sort_field = sort_field;
	}
	public String getSort_order() {
		return sort_order;
	}
	public void setSort_order(String sort_order) {
		this.sort_order = sort_order;
	}
	public String getCollection() {
		return collection;
	}
	public void setCollection(String collection) {
		this.collection = collection;
	}
	
	public long getTotal_hits() {
		return total_hits;
	}
	public List<Field> getFields() {
		return fields;
	}
	public void setFields(List<Field> fields) {
		this.fields = fields;
	}
	public void setTotal_hits(long total_hits) {
		this.total_hits = total_hits;
	}
	
	public List<String> getResults() {
		return results;
	}
	public void setResults(List<String> results) {
		this.results = results;
	}
	
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	public String getFilter() {
		return filter;
	}
	public void setFilter(String filter) {
		this.filter = filter;
	}
	
	@Override
	public String toString() {
		return "Query [url=" + url + ", collection=" + collection + ", query="
				+ query + ", start=" + start + ", rows=" + rows + ", filter="
				+ filter + ", total_hits=" + total_hits + ", fields=" + fields
				+ ", results=" + results + ", sort_field=" + sort_field
				+ ", sort_order=" + sort_order + ", selected_fields="
				+ Arrays.toString(selected_fields) + ", chart=" + chart
				+ ", chart_filed=" + chart_filed + ", month=" + month
				+ ", start_facet=" + start_facet + ", end_facet=" + end_facet
				+ ", range_gap=" + range_gap + ", field_value=" + field_value
				+ "]";
	}
}
