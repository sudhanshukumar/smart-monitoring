/*$(document).ready(function(){
	//createdonutchart();
});
var series = [];
function createdonutchart(){
	$.ajax({
	    type: "GET",
	    url: "donutChart.html",
	    success: function(response){
	  	  var result = response.toString();
	  	  var xAxisData=[];
	  	  var objList=JSON.parse(result);
	  	  var series1 = [];
	  	  var yAxisArray = [];
	  	  for(b=0;b<objList.length;b++){
						var actualObject=objList[b];
						series1.push([actualObject["alertName"],actualObject["alertValue"]]);
	  	  }
	  	  
	    	var chart = c3.generate({
	    		bindto : '#donutchart',
	    	    data: {
	    	        columns: series1,
	    	        type : 'donut',
	    	        onclick: function(d, element) {
		    	        chart.internal.config.tooltip_show = true;
		    	        chart.internal.showTooltip([d], element);
		    	        chart.internal.config.tooltip_show = false;
		    	    }
	    	    },
	    	    donut: {
	    	        title: "Alerts",
	    	    },
	    	     tooltip : {
	    	    	 show : true
	    	     }
	    	    
	    	});
	    },
	    failure : function(){
	  	  alert(failure);
	    }
		
	});
	
	$("#donutchart").click(function(d, i){
	    var evt = new MouseEvent("click");	
	    var sliceName=$(".c3-tooltip").find(".name").prop('outerText');
	    var sliceValue=$(".c3-tooltip").find(".value").prop('outerText');
	    $.ajax({
	    	type: "GET",
		    url: "donutChartGrid.html",
		    data : "sliceName="+sliceName+"&sliceValue="+sliceValue,
		    success: function(response){
		    	var result = response.toString();
			  	  var xAxisData=[];
			  	  var objList=JSON.parse(result);
			  	  var series1 = [];
			  	  var yAxisArray = [];	
			  	  for(b=0;b<objList.length;b++){
								var actualObject=objList[b];
								series1.push(actualObject);
			  	  }
		    	 $("#donutgrid").dialog({
		        	 autoOpen: true,
		             width: 833,
		             height: 275,
		             modal: true,
		             resizable: false,
		             open: function (event, ui) {
		            	 createdonuttable(series1);
		             },
		             close: function (event, ui) { 
		            	 $("#JsGrid").GridUnload(true).trigger("reloadGrid");
		             }       	
		        });
		  	  
		    },
		    failure : function(){
		  	  alert(failure);
		    }	    	
	    });
	 });
	 
	function createdonuttable(dataArray){
		var clients =dataArray;
		$("#JsGrid").jqGrid({
			data : clients,
			datatype : 'local',
			width: 833,
            height: 275,
			colNames : getColNames(dataArray[0]),
			colModel : getColModels(dataArray[0]),
			pager : '#pager1',
			viewrecords : true
		});

	    function getColNames(data) {
	        var keys = [];
	        for(var key in data) {
	            if (data.hasOwnProperty(key)) {
	                keys.push(key);
	            }
	        }

	        return keys;
	    }

	    function  getColModels(data) {
	        var colNames= getColNames(data);
	        var colModelsArray = [];
	        for (var i = 0; i < colNames.length; i++) {
	            var str;
	            if (i === 0) {
	                str = {
	                    name: colNames[i],
	                    index:colNames[i],
	                    key:true,
	                    editable:true
	                };
	            } else {
	                str = {
	                    name: colNames[i],
	                    index:colNames[i],
	                    editable:true
	                };
	            }
	            colModelsArray.push(str);
	        }

	        return colModelsArray;
	    }
		//jQuery("#JsGrid").jqGrid('navGrid','#pager',{edit:true,add:true,del:true});
	}
	
	
}
*/