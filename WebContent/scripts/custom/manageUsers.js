/****
 * 
 * 
 */
$(document).ready(
				function() {
					$("#alert").hide();
					$("#confirmDelete").hide();
					manageUsersGrid();
					$("#addNewUser").click(function() {
						window.location.href="user.html";
					});
				});
				
function manageUsersGrid() {
	$("#addedUserHistory").jqGrid(
			{
				jsonReader : {
					root : "gridData", // an array that contains the  actual data
					page : "page", // current page of the query
					total : "total", // total pages for the query
					records : "records",// total number of records for the query
					repeatitems : false,
					id : "userId" // the unique id of the row
				},
				url : 'populateUsersGrid.html',
				datatype : "json",
				mtype : "POST",
				colModel : [
							{
								name : 'userId',
								label : 'UserId',
								align : 'center',
								width : 30,
								sortable: true
							},
							{
								name : 'name',
								label : 'Name',
								align : 'center',
								width : 90,
								sortable: true
							},
							{
								name : 'emailId',
								label : 'Email Id',
								align : 'center',
								width : 90,
								sortable: true
							},
							{
								label : 'Actions',
								width : 90,
								fixed : true,
								sortable : false,
								resize : false,
								formatter : function(cellvalue, options,
										rowObject) {

									return addedUserActionButtons(cellvalue,
											options, rowObject);
								}
							} ],
							
							pager : '#addedUserHistoryPager',
							rowNum : 10,
							rowList : [ 10, 20, 30 ],
							viewrecords : true,
							gridview : true,
							loadonce: true,
							width : 900,
							height: 300
			});
}

function addedUserActionButtons(cellvalue,options, rowObject)
{
	var previewDevLink= '<img title="Preview" onClick=previewUserInfo("'
	    +encodeURI(JSON.stringify(rowObject))
	    +'") src="images/common/preview-16.png"/>';
 
	var editDevLink = '<img title="Edit" onClick=editUserInfo("'
			+ encodeURI(JSON.stringify(rowObject))
			+ '") src="images/common/edit-6-16.png"/>';
	
	var deleteDevLink = '<img title="Delete" onClick=deleteUser("'
		+ encodeURI(JSON.stringify(rowObject))
		+ '") src="images/common/delete-3-16.png"/>';
	
	var actionDeviceLink =  "&nbsp;&nbsp;&nbsp;" + previewDevLink + "&nbsp;&nbsp;&nbsp;" + editDevLink+ "&nbsp;&nbsp;&nbsp;" + deleteDevLink;
	return actionDeviceLink;
}

function previewUserInfo(rowobject)
{
	var dataJSON = JSON.parse(decodeURI(rowobject));
	var userId=dataJSON.userId;	
	window.location.href =  "user.html?userId="+userId+"&action=preview";
}

function editUserInfo(rowobject)
{
	var dataJSON = JSON.parse(decodeURI(rowobject));
	var userId=dataJSON.userId;	
	window.location.href =  "user.html?userId="+userId+"&action=edit";
}

function deleteUser(rowobject)
{
			 var dataJSON = JSON.parse(decodeURI(rowobject));
			 var userId=dataJSON.userId;	
			 $("#confirmDelete").show();
				$( "#confirmDelete" ).dialog({
					 resizable: false,
					 autoOpen: false,
					 height:200,
					 width: 340,
					 position: [485, 185],
					 modal: true,
					 buttons: {
						 "Yes": function() {
							 	$.ajax({
							 			type : "POST",
							 			url : "\deleteUser.html",
							 			data : "userId=" + userId,
							 			success : function(response) {
				
							 				window.location.href="manageUsers.html"
							 			},
									 	cache: false
							 	}); 
						 },
							Cancel: function() {
				 							$( this ).dialog( "close" );
				 							window.location.href="manageUsers.html"
				 						}
					 }//end of buttons
				});
				$("#confirmDelete").dialog("open");
				$(".ui-dialog-title").text('Delete?');
				$(".ui-dialog-titlebar-close").hide();
				$("#confirmDelete").text("Delete this user ?");
}
