/**
 * 
 */
package com.lnt.smartit.models;

/**
 * @author 299273
 *
 */
public class Space {
	
	private String capacity;
	private String used;    
	private String free; 
	private String used_percentage;
	public String getCapacity() {
		return capacity;
	}

	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	public String getUsed() {
		return used;
	}

	public void setUsed(String used) {
		this.used = used;
	}

	public String getFree() {
		return free;
	}

	public void setFree(String free) {
		this.free = free;
	}

	public String getUsed_percentage() {
		return used_percentage;
	}

	public void setUsed_percentage(String used_percentage) {
		this.used_percentage = used_percentage;
	}

	@Override
	public String toString() {
		return "Space [capacity=" + capacity + ", used=" + used + ", free="
				+ free + ", used_percentage=" + used_percentage + "]";
	}
}
