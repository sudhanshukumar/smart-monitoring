/**
 * <table id="addedDeviceHistory"></table>
<div id="addedDeviceHistoryPager"></div>
 */
$(document).ready(function() {

	$('#toLog_datepicker').datetimepicker({
		format : 'Y-m-d H:i:s',
		dayOfWeekStart : 1,
		lang : 'en'
	});
	
	$('#fromLog_datepicker').datetimepicker({
		format : 'Y-m-d H:i:s',
		dayOfWeekStart : 1,
		lang : 'en'
	});
	getfilterCriterias();
	problemHistoryJqgrid();
});

$(document).onload()

//This function is added by Pinakin to caclulate last page no based on total no of records and window size.
/**************Function starts here*************/
function getLastPageNo(totalRec, winSize)
{
	if(totalRec%winSize == 0){
		return totalRec/winSize;
	}
	else{
		return (totalRec/winSize)+1;
	}
};
/**************Function ends here**************/

function problemHistoryJqgrid() {
	/**Following variables are added by Pinakin. 
	 * These variables would be used to send pagination information to client.*/
	 var queryParams = {
                        "page" : 0,
                        "rows" : 10,
                        "hostname":"",
    					"alerttype":"",
    					"parametername":"",
    					"fromLogDate":"",
    					"toLogDate":"",
                        };
                        queryParams.page = 1;
                        queryParams.rows = 20;
	//This is added by Pinakin on 21st for Pagination.
    $('#problemHistoryGrid').jqGrid('GridUnload');
    $("#problemHistoryGrid").jqGrid(
			{
				jsonReader : {
					root : "gridData", // an array that contains the actual data
					page : "page", // current page of the query
					total : "total", // total pages for the query
					records : "records",// total number of records for the query
					repeatitems : false,
					//id : "amId" // the unique id of the row
				},
				
				/* URL is modified by Pinakin to send pagination parameters to server.*/
				url : 'populateproblemHistoryGrid2.html',
				//Added for pagination by Pinakin
				/********Start********/
				ajaxGridOptions : {
                                    contentType : 'application/json'
                },
                ajaxEditOptions : {
		                            contentType : 'application/json',
		                            dataType : 'json'
                },
                /*********End*********/
				datatype : "json",
				//Added for pagination by Pinakin
				/***************Start***********/
				postData : JSON.stringify(queryParams),
				shrinkToFit: false,
				forceFit : false,
				/*autowidth: true,*/
				/***************End*************/
				mtype : "POST",
				
				colModel : [
				            	/*{
				            		name : 'amId',
				            		label : 'amId',
				            		align : 'center',
				            		width : "30px",
				            		hidden:true,
				            		sortable: true//,
				            	},*/
				            	{
				            		name : 'hostname',
				            		label : 'Host Name',
				            		align : 'center',
									width : "150px",
									sortable: true//,
				            	},
				            	{
				            		name : 'appname',
				            		label : 'Application Name',
				            		align : 'center',
									width : "120px",
									hidden:true,
									sortable: true//,
				            	},
				            	{
				            		name : 'parametername',
				            		label : 'Monitoring Variable',
				            		align : 'center',
				            		width : "120px",
				            		sortable: true//,
				            	},
				            	{
				            		name : 'alerttype',
				            		label : 'Alert Type',
				            		align : 'center',
									width : "100px",
									sortable: true//,
				            	},
				            	
				            	{
				            		name:'alertdate',
				            		label: 'Issue Detected On',
				            		align : 'center',
				            		width : "150px",
				            		sortable: true,
				            		formatter: function(cellValue, options) {
							       /* if(cellValue) {
							        	cellValue = cellValue.split(".")[0];
							        }*/
							        	/*var options = {
							        		    weekday: "long", year: "numeric", month: "short",
							        		    day: "numeric", hour: "2-digit", minute: "2-digit"
							        		};
							        	cellValue = new Date(cellValue);
							        	cellValue=cellValue.toLocaleString(options);
							        }*/
							        return cellValue;
				            		}
				            	},
				            	/*{
				            		name:'time',
				            		label:'Issue Detection Time',
				            		sortable: true,
				            		hidden:true,
//				            		width : "650px",
				            		align : 'center'
				            	},*/
								{
									name:'description',
									label : 'Description',
									align :'left',
//									autowidth : true,
									width : "900px",
									sortable: true
						}],
							
							pager : '#problemHistoryPager',
							rowNum : 20,
							rowList : [ 20, 40, 60 ],
							viewrecords : true,
							gridview : true,
							//Value changed to false for pagination by Pinakin on 21st August
							loadonce: false,
							width : 950,
							height: 400,
							
						/*** Action listner added by Pinakin for pagination bar.
						 * This will listen to events on data change in pagination bar ****/
						/***********************Action Listner code starts here*******************************/
						 onPaging : function(pgButton) {
							var pageNumber = $("#problemHistoryPager .ui-pg-input").val();
							var rowNum = $("#problemHistoryPager .ui-pg-selbox").val();

							if (pgButton.toLowerCase().indexOf("next") >= 0) {

								++pageNumber;

							} else if (pgButton.toLowerCase().indexOf("prev") >= 0) {

								--pageNumber;

							} else if (pgButton.toLowerCase().indexOf("last") >= 0) {
								pageNumber = $("#problemHistoryPager #sp_1_problemHistoryPager").text();
							} else if (pgButton.toLowerCase().indexOf("first") >= 0) {
								pageNumber = 1;
							}
							queryParams.page = pageNumber;
							queryParams.rows = rowNum;
							
							$("#problemHistoryGrid").setGridParam({
								postData : JSON.stringify(queryParams)
							});
						}
						/***********Action Listner code ends here********************/ 		
                
	});
    
    $('#searchExecute').click(function(event){
    	event.preventDefault();
    	queryParams.page = 1;
		queryParams.rows = 20;
//    	queryParams.hostname = $("#txtHostname option:selected").text();
		queryParams.hostname = $("#txtHostname").val();
    	queryParams.parametername = $("#txtParametername").val();
    	queryParams.alerttype = $("#txtAlerttype").val();
    	queryParams.fromLogDate =  $("#fromLog_datepicker").val();
    	queryParams.toLogDate =  $("#toLog_datepicker").val();

    	$("#problemHistoryGrid").setGridParam({
    		postData : JSON.stringify(queryParams)}).trigger('reloadGrid');
    });
    
    $('#refreshSearch').click(function(event){
    	event.preventDefault();
    	$("#txtHostname").val('');
    	$("#txtParametername").val('');
    	$("#txtAlerttype").val('');
    	$("#fromLog_datepicker").val('');
    	$("#toLog_datepicker").val('');
    	
    	queryParams.page = 1;
		queryParams.rows = 20;
		queryParams.hostname = "";
    	queryParams.parametername = "";
    	queryParams.alerttype = "";
    	queryParams.fromLogDate = "";
    	queryParams.toLogDate = "";
    	
    	$("#problemHistoryGrid").setGridParam({
    		postData : JSON.stringify(queryParams)}).trigger('reloadGrid');
	});
	
}

function getfilterCriterias() {

	$.ajax({
		type : "POST",
		url : "getUniqueHostnames.html",
		success : function(response) {
//			$('#txtHostname').empty();
			$("#txtHostname").append('<option value="" selected >Select</option>');
			for ( var i = 0; i < response.length; i++) {
				console.log(response[i]);
				$("#txtHostname").append($('<option>').text(response[i]))
						.append($('</option>'));
			}
		}
	});
	
	$.ajax({
		type : "POST",
		url : "getUniqueParameters.html",
		success : function(response) {
//			$('#txtHostname').empty();
			$("#txtParametername").append('<option value="" selected >Select</option>');
			for ( var i = 0; i < response.length; i++) {
				console.log(response[i]);
				$("#txtParametername").append($('<option>').text(response[i]))
						.append($('</option>'));
			}
		}
	});
	
	$.ajax({
		type : "POST",
		url : "getUniqueAlerttypes.html",
		success : function(response) {
//			$('#txtHostname').empty();
			$("#txtAlerttype").append('<option value="" selected >Select</option>');
			for ( var i = 0; i < response.length; i++) {
				console.log(response[i]);
				$("#txtAlerttype").append($('<option>').text(response[i]))
						.append($('</option>'));
			}
		}
	});
}

