package com.lnt.smartit.mysql.dao;

import java.util.List;

import com.lnt.smartit.mysql.model.Alertmessageinfo;
import com.lnt.smartit.solr.pojo.JqGridData;

public interface ProblemHistoryDao {
	public List<Alertmessageinfo> getProblemHistory(Integer pageNo, Integer pageRowCount, boolean cache);
	public Integer getProblemHistroyRecordsCount();
	public List<String> getDistinctHostnames();
	public List<String> getDistinctParameters();
	public void connectDatabase();
	public List<String> getDistinctAlerttypes();
	public Integer getProblemHistroyRecordsCount(String app, String hostname,
			String parameter, String alertType, String fromDate, String toDate);
	public JqGridData getProblemHistoryWithCriteria(Integer pageNo,
			Integer pageRowCount, boolean cache, String app, String hostname,
			String parameter, String alertType, String fromDate, String toDate);
}
