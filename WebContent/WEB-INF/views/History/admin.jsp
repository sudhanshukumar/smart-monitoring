<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Problem History</title>

<!-- Common CSS files of Bootstrap and JQuery -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/common/ui.jqgrid.css" />
<link rel="stylesheet" type="text/css" href="css/common/jquery-ui.min.css" />
<link rel="stylesheet" type="text/css" href="css/common/jquery.datetimepicker.css" />

<!-- Custom CSS files for this Page -->
<link rel="stylesheet" href="css/custom/specific/nav.css">

<!-- Common Scripts of Bootstrap and JQuery -->
<script src="scripts/common/jquery-2.2.3.min.js"></script>
<script src="scripts/common/jquery-ui.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="scripts/common/grid.locale-en.js"></script>
<script src="scripts/common/jquery.jqGrid.min.js"></script>


<!-- Custom scripts for this Page -->
<script src="scripts/custom/problemHistory.js"></script>
<script type="text/javascript" src="scripts/common/jquery.datetimepicker.js"></script>

<script src="scripts/custom/tab_menu.js"></script>

<% String session_val = (String)session.getAttribute("modules"); %>

<script type="text/javascript">
	session_obj= '<%=session_val%>';
	function getData(){
		$("#alertFrame").contents().find("ul.nav navbar").css("display","none");
	}
</script>

</head>
<body style="font-size:11px!important;" onload="getData()">
	<div class="container-fluid">
	  <%@ include file="../common/header.jsp"%>
  
	  <nav class="navbar navbar-inverse" style="margin-top: 0px">
	      <ul class="nav navbar-nav navbar-xs">
	        <li id="Dashboard"><a href="dashboard.html">Dashboard<span class="sr-only">(current)</span></a></li>
	        <li class="active" id="AlertHistory"><a href="problemHistory.html">Alert History</a></li>
	        <!-- <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Manage<span class="caret"></span></a>
	          <ul class="dropdown-menu">
	            <li><a href="#">Manage Device</a></li>
	            <li><a href="#">Manage Threshold</a></li>
	          </ul>
	        </li> -->
	        <li id="LogAnalysis"><a href="query.html">Log Analysis</a></li>
	        <%-- <li id="LogCorrelation"><a href="correlation.html">Log Correlation</a></li>
	        <li id="VariableAnalysis"><a href="variableAnalysis.html"><spring:message code="label.admin.menu5"></spring:message></a></li> 
	        <li class="dropdown" id="Reports">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reports <span class="caret"></span></a>
	          <ul class="dropdown-menu">
	            	<li id="CreateReport"><a href="charts.html">Create Report</a></li>
		            <li id="CreateDashboard"><a href="createuserdashboard.html">Create Dashboard</a></li>
		            <li id="ViewDashboard"><a href="list_dashboard.html">View Dashboard</a></li>
	          </ul>
	        </li>--%>
	      </ul>
	  </nav>

<div class="container-fluid innerContainer">
<iframe id="alertFrame" src="http://10.103.20.64:5601/app/kibana#/discover/Alert-History?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:now%2Fy,mode:quick,to:now%2Fy))&_a=(columns:!(hostname,parametername,description),filters:!(),index:alerts,interval:auto,query:(query_string:(analyze_wildcard:!t,query:'*')),sort:!(alertdate,desc))" style="width: 100%; height: 530px;" ></iframe>
	   <!-- <div class="col-md-3" style="background-color:#e0e0e0; height:460px;">
	         <div class="row" style=" margin-top: 20px; margin-left: .05px;">
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="col-sm-4 text-style" >Hostname:</label>
						<div class="col-sm-8">
							<select class="form-control text-style" id="txtHostname" style="width:165px;">
								<option value=''></option>
							</select>
						</div>
					</div>

				</form>
			</div>
			<div class="row" style=" margin-left: .05px;">
			     <form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="col-sm-4 text-style" >Monitoring variable:</label>
						<div class="col-sm-8">
							<select class="form-control text-style" id="txtParametername" style="width:165px">
								<option value=''></option>
							</select>
						</div>
					</div>

				</form>
			</div>
			<div class="row" style=" margin-left: .05px;">
			     <form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="col-sm-4 text-style" >Alerttype:</label>
						<div class="col-sm-8">
							<select class="form-control text-style" id="txtAlerttype" style="width:165px;">
								<option value=''></option>
							</select>
						</div>
					</div>
				</form>
		  </div>
		  <div class="row" style=" margin-left: .05px;">
		         <form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="col-sm-4 text-style" >From:</label>
						<div class="col-sm-8">
							<div class="inner-addon left-addon">
						<i class="glyphicon glyphicon-calendar"></i> 
							<input type="text" id="fromLog_datepicker" style="width:165px" placeholder="yyyy-MM-dd hh:mm:ss">
						</div>
						</div>
					</div>
				</form>
			</div>
			<div class="row" style=" margin-left: .05px;">
			    <form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="col-sm-4 text-style" >To:</label>
						<div class="col-sm-8">
							<div class="inner-addon left-addon">
						<i class="glyphicon glyphicon-calendar"></i> 
							<input type="text" id="toLog_datepicker" style="width:165px" placeholder="yyyy-MM-dd hh:mm:ss">
						</div>
						</div>
					</div>
				</form>
			</div>
		   <div class="row" style=" margin-left: 120px;">
		   <div class="col-sm-4">
		   		  <form class="form-horizontal" role="form">
					<div class="form-group">
						<div class="col-sm-8">
							<input type="button" class="btn btn-primary btn-sm" id="searchExecute" name="Execute" value=" Search"/> 

						</div>
					</div>
				</form>
		   </div>
		   <div class="col-sm-4" style=" margin-left: 15px;">
		    <form class="form-horizontal" role="form">
					<div class="form-group">
						<div class="col-sm-8">
		                    <input type="button" id="refreshSearch" class="btn btn-primary btn-sm"  name="Refresh" value="Refresh" />
						</div>
					</div>
				</form>
		   </div>
		   </div>
	  </div>
	   <div class="col-md-5" style="height:460px;">
          <table id="problemHistoryGrid" style="height:460px;"></table>
          <div id="problemHistoryPager"></div>
	  </div> -->
</div>
<%@ include file="../common/footer.jsp"%>
	</div>
</body>
</html>