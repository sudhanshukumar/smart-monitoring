/**
 * 
 */
$(document).ready(function() {
	$("#edit_threshold").show();
	document.getElementById("headingManageThreshold").innerHTML = "Add New Threshold";
	$("#alert").hide();
	$("#confirmEdit").hide();
	$("#preview_th_buttons").hide();
	$("#plusImageLog").hide();
	$("#logFileSizeTitle").hide();
	$("#plusImageServices").hide();
	$("#servicesTitle").hide();
	$("#preview_ok").click(function(){
		window.location.href="manageThresHold.html";
	});
	$("#cancel_threshold").click(function(){
		window.location.href="manageThresHold.html";
	});
	//var id=getQueryVariable("thresholdId");
	//var action = getQueryVariable("action");
	/*if(action == "edit")
	{
		$("#preview_th_buttons").hide();
		$("#add_threshold_submit").show();
		$("#urlResp").hide();
		$("#appResp").hide();
	$.ajax({
		type : "POST",
		url : "editThreshold.html",
		data : "thresholdId=" + id, 
		success : function(response) {
			 $("#urlResp").show();
			 $("#appResp").show();
			var parameterName =  response.parameterMaster.parameterName
			if(parameterName == "LOGFILESIZE")	
			{
				$(".hideOnLogAndService").hide();
				$("#plusImageLog").show();
				$("#logFileSizeTitle").show();
				$("#plusImageServices").hide();
				$("#servicesTitle").hide();
				$("#logFileTable").show();
				$("#servicesTable").hide();
				$('#threshold_parameter').val("LOGFILESIZE");
				$("#urlResp").hide();
				$("#appResp").hide();
					getLogPreview(id);
			}
			else if(parameterName == "SERVICES")
			{
				$(".hideOnLogAndService").hide();
				$("#plusImageLog").hide();
				$("#logFileSizeTitle").hide();
				$("#plusImageServices").show();
				$("#servicesTitle").show();
				$("#logFileTable").hide();
				$("#servicesTable").show();
				$('#threshold_parameter').val("SERVICES");
				$("#urlResp").hide();
				$("#appResp").hide();
				getServicePreview(id);
			}
			document.getElementById("headingManageThreshold").innerHTML = "Edit Threshold";
			 if((response.pageValue!= "") && (response.appValue!= "")) {
				 $("#addThresHold tbody tr:eq(0)").after('<tr class="hideOnLogAndService"><td class="col1"><label>Page (URL)</label></td><td class="col2"><input type="text" id="urlResp"></td><td class="col1"><label>Application Name</label></td><td class="col2"><input type="text" id="appResp"></td></tr>');
				 $('#urlResp').attr('readonly', false);
				 $('#appResp').attr('readonly', false);
				 $("#urlResp").val(response.pageValue);
				 $("#appResp").val(response.appValue);
				 $('#threshold_parameter').val(response.parameterMaster.parameterName);
				 $('#threshold_parameter').attr('disabled', false);
			 }
			
			$('#threshold_Name').attr('readonly', false);
			$('#threshold_hostName').attr('disabled', false);	
			$('#threshold_parameter').attr('readonly', false);
			$('#threshold_enable_status').attr('readonly', false);
			$('#threshold_notification_status').attr('readonly', false);
			$('#low_warning_threshold').attr('readonly', false);
			$('#high_warning_threshold').attr('readonly', false);
			$('#threshold_warning_breach_duration_val').attr('readonly', false);
			$('#low_alert_threshold').attr('readonly', false);
			$('#high_alert_threshold').attr('readonly', false);
			$('#threshold_alert_breach_duration_val').attr('readonly', false);
			$('#threshold_alert_breach_dataType').attr('disabled', false);
			$('#threshold_group_name_sel').attr('disabled', false);
			$('#threshold_group_name').attr('disabled', false);
			$('#threshold_additionalEmailList').attr('readonly', false);
		var map = response.groupIds;
		for(key in map)
			{
			$('#threshold_group_name_sel').append('<option value="'+key+'">' + map[key] + '</option>');
			
			$("#threshold_group_name option[value='" + key + "']").remove();
			}
			$('#threshold_Name').val(response.thresholdName);
			$('#threshold_hostName').val(response.deviceMaster.deviceId);
			$('#threshold_parameter').val(response.parameterMaster.parameterId);
			if(response.thresholdEnabled==true)
				{
				$('#threshold_enable_status').attr('checked', true);
				}
			else
				{
				$('#threshold_enable_status').attr('checked', false);
				}
			if(response.notificationEnabled==true)
			{
			$('#threshold_notification_status').attr('checked', true);
			}
			else
			{
			$('#threshold_notification_status').attr('checked', false);
			}
			
			$('#low_warning_threshold').val(response.warningLowThreshold);
			$('#high_warning_threshold').val(response.warningHighThreshold);
			$('#threshold_warning_breach_duration_val').val(response.warningBreachDuration);
			
			$('#low_alert_threshold').val(response.alertLowThreshold);
			$('#high_alert_threshold').val(response.alertHighThreshold);
			$('#threshold_alert_breach_duration_val').val(response.alertBreachDuration);
			
			$('#threshold_alert_breach_dataType').val(response.thresholdDataType.toLowerCase());
			$('#threshold_additionalEmailList').val(response.defaultEmailId);
			
			//put here setting valyue for select group
			debugger
			// set selected in OS information combo
			setSelectedInList('threshold_parameter', response.parameterName);
			
		},
        cache: false
	});
	}
	
	if(action == "preview")
	{
		$("#add_Threshold_buttons").hide();
		$("#preview_th_buttons").show();
		document.getElementById("headingManageThreshold").innerHTML = "Preview Threshold";
	$.ajax({
		type : "POST",
		url : "previewThreshold.html",
		data : "thresholdId=" + id, 
		success : function(response) {
			
			var parameterName =  response.parameterMaster.parameterName
		if(parameterName == "LOGFILESIZE")	
		{
			$(".hideOnLogAndService").hide();
			$("#plusImageLog").show();
			$("#logFileSizeTitle").show();
			$("#plusImageServices").hide();
			$("#servicesTitle").hide();
			$("#logFileTable").show();
			$("#servicesTable").hide();
			$('#threshold_parameter').val("LOGFILESIZE");
				getLogPreview(id);
		}
		else if(parameterName == "SERVICES")
		{
			$(".hideOnLogAndService").hide();
			$("#plusImageLog").hide();
			$("#logFileSizeTitle").hide();
			$("#plusImageServices").show();
			$("#servicesTitle").show();
			$("#logFileTable").hide();
			$("#servicesTable").show();
			$('#threshold_parameter').val("SERVICES");
			getServicePreview(id);
		}
		else{
			 if((response.pageValue!= "") && (response.appValue!= "")){
				 $("#addThresHold tbody tr:eq(0)").after('<tr><td class="col1"><label>Page (URL)</label></td><td class="col2"><input type="text" id="urlResp"></td><td class="col1"><label>Application Name</label></td><td class="col2"><input type="text" id="appResp"></td></tr>');
				 $("#urlResp").show();
				 $("#appResp").show();
				 $('#urlResp').attr('readonly', true);
				 $('#appResp').attr('readonly', true);
				 $("#urlResp").val(response.pageValue);
				 $("#appResp").val(response.appValue);
				 $('#threshold_parameter').val(response.parameterMaster.parameterName);
				 $('#threshold_parameter').attr('disabled', true);
					 }
			 $("#urlResp").hide();
			 $("#appResp").hide();
			 $('#threshold_Name').val(response.thresholdName);
			 $('#threshold_hostName').val(response.deviceMaster.deviceId);	
		   	 $('#threshold_parameter').val(response.parameterMaster.parameterId);
			if(response.thresholdEnabled==true)
				{
				$('#threshold_enable_status').attr('checked', true);
				}
			else
				{
				$('#threshold_enable_status').attr('checked', false);
				}
			if(response.notificationEnabled==true)
			{
			$('#threshold_notification_status').attr('checked', true);
			}
		else
			{
			$('#threshold_notification_status').attr('checked', false);
			}
			$('#low_warning_threshold').val(response.warningLowThreshold);
			$('#high_warning_threshold').val(response.warningHighThreshold);
			$('#threshold_warning_breach_duration_val').val(response.warningBreachDuration);
			
			$('#low_alert_threshold').val(response.alertLowThreshold);
			$('#high_alert_threshold').val(response.alertHighThreshold);
			$('#threshold_alert_breach_duration_val').val(response.alertBreachDuration);
			
			$('#threshold_alert_breach_dataType').val(response.thresholdDataType.toLowerCase());
			$('#threshold_additionalEmailList').val(response.defaultEmailId);
			
			$('#threshold_parameter').val(response.parameterMaster.parameterName);
			var map = response.groupIds;
			for(key in map)
				{
				$('#threshold_group_name_sel').append('<option value="'+key+'">' + map[key] + '</option>');
				
				$(
				  "#threshold_group_name option[value='" + key + "']").remove();
				}
			//put here setting valyue for select group
			$('#threshold_Name').attr('readonly', true);
			$('#threshold_hostName').attr('disabled', true);	
			$('#threshold_parameter').attr('readonly', true);
			
			$('#threshold_enable_status').attr('readonly', true);
			$('#threshold_notification_status').attr('readonly', true);
			
			$('#low_warning_threshold').attr('readonly', true);
			$('#high_warning_threshold').attr('readonly', true);
			$('#threshold_alert_breach_duration_val').attr('readonly', true);
			
			$('#low_alert_threshold').attr('readonly', true);
			$('#high_alert_threshold').attr('readonly', true);
			$('#threshold_alert_breach_duration_val').attr('readonly', true);
			$('#threshold_group_name_sel').attr('disabled', true);
			$('#threshold_group_name').attr('disabled', true);
			$('#threshold_alert_breach_dataType').attr('disabled', true);
			$('#threshold_additionalEmailList').attr('readonly', true);
		}
			debugger;
			// set selected in OS information combo
			setSelectedInList('threshold_parameter', response.parameterMaster.parameterId);
			
		},
        cache: false
	});
	}
	
	$("#add_threshold_cancel").click(function() {
		window.location.href="manageThresHold.html";
	});
	
	$("#add_threshold_submit").click(function() {
		addThresholdSubmit(id);
	});
	
	$("#AddGroupName").click(function() {
		assignList();
	});

	$("#removeGroupName").click(function() {
		unassignList();
	});*/
	$("#edit_threshold").click(function() {
		//var parameterName = $("#threshold_parameter").val();
		console.log($("#critical_threshold").val());
		console.log($("#warning_threshold").val());
		console.log($("#threshold_hostName").val());
		console.log($("#threshold_ParamName").val());
		console.log($("#id").val());
		var thresholdObject={paramName:$("#threshold_ParamName").val(),hostName:$("#threshold_hostName").val(),id:$("#id").val(),warnThreshold:$("#warning_threshold").val(),criticalThreshold:$("#critical_threshold").val()};
		$( "#confirmEdit" ).show();
		$( "#confirmEdit" ).dialog({ buttons:{"Ok":function() {
			window.location.href = "editThreshold.html?thresholdMaster="+JSON.stringify(thresholdObject);

		}} });
		/*if(parameterName == '1005')
			{
			getLogEdit(id);
			}
		if(parameterName == '1006')
			{
			getServiceEdit(id);
			}*/
	});
});

/*function getLogPreview(id)
{
	$.ajax({
		type : "POST",
		url : "previewThresholdForLog.html",
		data : "thresholdId=" + id, 
		success : function(response) {
			var group;
			var groupId;
			for(var i=0;i<response.length;i++)
				{
				$("#threshold_Name").val(response[i].threshold_name);
				$("#threshold_hostName").val(response[i].HostName);
				$("#threshold_parameter").val(response[i].parameter_id);
				if(response[i].threshold_enabled=='true')
				{
					$('#threshold_enable_status').attr('checked', true);
				}
				else
				{
					$('#threshold_enable_status').attr('checked', false);
				}
				if(response[i].notification_enabled=='true')
				{
					$('#threshold_notification_status').attr('checked', true);
				}
				else
				{
					$('#threshold_notification_status').attr('checked', false);
				}
				groupId = response[i].group_id;
				if(response[i].group_id == 1){
					group = "Oracle";
				}
				var rowValues = '<tr id="logFileSize'+i+'"><td class="col1"><label>Log File Name</label></td><td class="col2"><input type="text" id="logFileName'+i+'" value="'+response[i].log_file_name+'"></td><td class="col1"><label>Critical Threshold</label></td><td class="col2"><input type="text" id="logCriticalThreshold'+i+'" value="'+response[i].critical_threshold_size+'"></td><td class="col1"><label>Warning Threshold</label></td><td class="col2"><input type="text" id="logWarningThreshold'+i+'" value="'+response[i].warning_threshold_size+'"></td></tr>';
				$("#logFileTable").last().append(rowValues);
				logIds.push("logFileSize"+i);
				}
			$('#threshold_group_name_sel').append('<option value="'+groupId+'">' + group + '</option>');
		}
	});
}


function getServicePreview(id)
{
	$.ajax({
		type : "POST",
		url : "previewThresholdForService.html",
		data : "thresholdId=" + id, 
		success : function(response) {
			var group;
			var groupId;
			
			for(var i=0;i<response.length;i++)
				{
				$("#servicesTable").show();
				$("#threshold_Name").val(response[i].threshold_name);
				$("#threshold_hostName").val(response[i].HostName);
				$("#threshold_parameter").val(response[i].parameter_id);

				if(response[i].threshold_enabled=='true')
				{
					$('#threshold_enable_status').attr('checked', true);
				}
				else
				{
					$('#threshold_enable_status').attr('checked', false);
				}
				if(response[i].notification_enabled=='true')
				{
					$('#threshold_notification_status').attr('checked', true);
				}
				else
				{
					$('#threshold_notification_status').attr('checked', false);
				}
				groupId = response[i].group_id;
				if(response[i].group_id == 1){
					group = "Oracle";
				}
				var rowValues1 = '<tr id="services"'+i+'"><td class="col1"><label>Services</label></td><td class="col2"><input type="text" id="serviceName'+i+'" value="'+response[i].service_name+'"></td></tr>';
				$("#servicesTable").last().append(rowValues1);
				}
		
			$('#threshold_group_name_sel').append(
					'<option value="'+groupId+'">'
							+ group + '</option>');
		}
	});
}

function getLogEdit(id)
{
	thresholdName=$('#threshold_Name').val();
	thresholdEnabled=true;
	notificationEnabled=true;
	additionalEmailId=$('#threshold_additionalEmailList').val();
	threshold_hostName=$('#threshold_hostName').val();
	 threshold_parameter=$('#threshold_parameter').val();
	 var buffer = '';
		var e = document.getElementById("threshold_group_name_sel");
		for ( var i = 0; i < e.options.length; i++) {
			buffer += e.options[i].value + ",";
		}
	threshold_group_name_sel = buffer.toString();
		for(var i=0;i<logIds.length;i++)
		{
			logFileName.push($("#logFileName"+i).val());
			logWarningThreshold.push($("#logWarningThreshold"+i).val());
			logCriticalThreshold.push($("#logCriticalThreshold"+i).val());
		}
		
	$.ajax({
		type : "POST",
		url : "editThresholdsForLog.html",
		data : 'thresholdName='+thresholdName +'&thresholdEnabled='+thresholdEnabled +
		'&notificationEnabled='+notificationEnabled+'&defaultEmailId='+additionalEmailId+'&threshold_group_name_sel='+threshold_group_name_sel+
		'&threshold_hostName='+threshold_hostName+'&threshold_parameter='+threshold_parameter+'&id='+id+'&logFileName='+logFileName+'&logWarningThreshold='+logWarningThreshold+'&logCriticalThreshold='+logCriticalThreshold,

		success : function(response) {
			window.location.href='manageThresHold.html';
		}
	});
}
function getServiceEdit(id)
{
			//Code to edit thresholds
			thresholdName=$('#threshold_Name').val();
			thresholdEnabled=true;
			notificationEnabled=true;
			additionalEmailId=$('#threshold_additionalEmailList').val();
			threshold_hostName=$('#threshold_hostName').val();
			 threshold_parameter=$('#threshold_parameter').val();
			 var buffer = '';
				var e = document.getElementById("threshold_group_name_sel");
				for ( var i = 0; i < e.options.length; i++) {
					buffer += e.options[i].value + ",";
				}
			threshold_group_name_sel = buffer.toString();
			for(var i=0;i<serviceId.length;i++)
			{
				serviceName.push($("#serviceName"+i).val());
			}
			$.ajax({
				type : "POST",
				url : "editThresholdsForService.html",
				data : 'thresholdName='+thresholdName +'&thresholdEnabled='+thresholdEnabled +
				'&notificationEnabled='+notificationEnabled+'&defaultEmailId='+additionalEmailId+'&threshold_group_name_sel='+threshold_group_name_sel+
				'&threshold_hostName='+threshold_hostName+'&threshold_parameter='+threshold_parameter+'&id='+id+'&serviceName='+serviceName,
				success : function(response) {
					window.location.href='manageThresHold.html';
				}
			});
}

var logIds = new Array();
var logFileName = new Array();
var logWarningThreshold = new Array();
var logCriticalThreshold = new Array();

var serviceId = new Array();
var serviceName = new Array();
var selectedParam; 

function checkIfAnythingToAdd() {
	if (!! $('#urlResp')) {
		$("#urlResp").parent().parent().remove();
		}
	selectedParam = $("#threshold_parameter").val();
	//action for logfilesize
	if(selectedParam == "1005")
		{
		$("#plusImageLog").show();
		$("#logFileTable").find("tr:gt(0)").remove();
		$(".hideOnLogAndService").hide();
		$("#logFileSizeTitle").show();
		$("#logFileTable").show();
		$("#servicesTable").hide();
		$("#plusImageServices").hide();
		$("#servicesTitle").hide();
		
		var rowValues = '<tr id="logFileSize'+0+'"><td class="col1"><label>Log File Name</label></td><td class="col2"><input type="text" id="logFileName'+0+'"></td><td class="col1"><label>Critical Threshold</label></td><td class="col2"><input type="text" id="logCriticalThreshold'+0+'"></td><td class="col1"><label>Warning Threshold</label></td><td class="col2"><input type="text" id="logWarningThreshold'+0+'"></td></tr>';
		$("#logFileTable").last().append(rowValues);
		logIds.push("logFileSize0");
		
		var i=1;
		$("#plusImageLog").click(function(){
			var rowValues = '<tr id="logFileSize'+i+'"><td class="col1"><label>Log File Name</label></td><td class="col2"><input type="text" id="logFileName'+i+'"></td><td class="col1"><label>Critical Threshold</label></td><td class="col2"><input type="text" id="logCriticalThreshold'+i+'"></td><td class="col1"><label>Warning Threshold</label></td><td class="col2"><input type="text" id="logWarningThreshold'+i+'"></td></tr>';
			$("#logFileTable").last().append(rowValues);
			logIds.push("logFileSize"+i);
			i++;
		});
		}

	//action for services
	if(selectedParam == "1006")
	{
		$("#plusImageServices").show();
		$("#servicesTable").find("tr:gt(0)").remove();
		$(".hideOnLogAndService").hide();
		$("#plusImageLog").hide();
		$("#logFileSizeTitle").hide();
		$("#servicesTitle").show();
		$("#logFileTable").hide();
		$("#servicesTable").show();
		
		var rowValues1 = '<tr id="services"'+0+'"><td class="col1"><label>Services</label></td><td class="col2"><input type="text" id="serviceName0"></td></tr>';
		$("#servicesTable").last().append(rowValues1);
		serviceId.push("services0")
		
		var i=1;
		$("#plusImageServices").click(function(){
			var rowValues1 = '<tr id="services"'+i+'"><td class="col1"><label>Services</label></td><td class="col2"><input type="text" id="serviceName'+i+'"></td></tr>';
			$("#servicesTable").last().append(rowValues1);
			serviceId.push("services"+i)
			i++;
		});
	}
	
	if(selectedParam == "1004") { //1004 is parameter id of RESPONSETIME
		$("#addThresHold tbody tr:eq(0)").after('<tr><td class="col1"><label>Page (URL)</label></td><td class="col2"><input type="text" id="urlResp"></td><td class="col1"><label>Application Name</label></td><td class="col2"><input type="text" id="appResp"></td></tr>');
	autocomplete for page urls starts	
		 var availablePageURLs = [
	"M275DCeAypExamEMD.do",
	"M284DCeAypExamEMD.do",
	"M276CCeAypExamESF.do",
	"M274DCeAypExamEMD.do",
	"M270DInstAypSectionEMD.do",
	"M433DTpoAypPlmtStudentEMD.do",
	"M305ECeEeexamApplicationEMS.do",
	"M008EInstCourseClassYearEMS.do",
]
		                     	
     	 $('#urlResp').autocomplete({
     	     source: function (request, response) {
     	         var matches = $.map(availablePageURLs, function (acItem) {
     	             if( (acItem.toUpperCase().indexOf(request.term.toUpperCase()) == 0) || ((acItem.toLowerCase().indexOf(request.term.toLowerCase()) == 0)) ){
     	                 return acItem;
     	             }
     	         });
     	         response(matches);
     	     }
     	 });
		 autocomplete for page urls ends	
		 
		 autocomplete for application names starts	
		 var availableApplicationNames = [
		                          "CampusNext (VJTI)",
		                          "MARS",
		                         ]
		                     	
     	 $('#appResp').autocomplete({
     	     source: function (request, response) {
     	         var matches = $.map(availableApplicationNames, function (acItem) {
     	             if( (acItem.toUpperCase().indexOf(request.term.toUpperCase()) == 0) || ((acItem.toLowerCase().indexOf(request.term.toLowerCase()) == 0)) ){
     	                 return acItem;
     	             }
     	         });
     	         response(matches);
     	     }
     	 });
		 autocomplete for application names ends	
	}
}
var thresholdName,thresholdEnabled,notificationEnabled,
	warningHighThreshold,warningLowThreshold,warningBreachDuration,
	alertHighThreshold,alertLowThreshold,alertBreachDuration,
	thresholdDataType,
	threshold_group_name_sel,additionalEmailId,
	threshold_hostName,threshold_parameter,resp_url,resp_app;

function addThresholdSubmit(id) {
	selectedParam =  $("#threshold_parameter").val();
	thresholdName=$('#threshold_Name').val();
	thresholdEnabled=true;
	notificationEnabled=true;
	additionalEmailId=$('#threshold_additionalEmailList').val();
	threshold_hostName=$('#threshold_hostName').val();
	 threshold_parameter=$('#threshold_parameter').val();
	 var buffer = '';
		var e = document.getElementById("threshold_group_name_sel");
		for ( var i = 0; i < e.options.length; i++) {
			buffer += e.options[i].value + ",";
		}
	threshold_group_name_sel = buffer.toString();
	if(selectedParam == "1005")
	{
		for(var i=0;i<logIds.length;i++)
		{
			logFileName.push($("#logFileName"+i).val());
			logWarningThreshold.push($("#logWarningThreshold"+i).val());
			logCriticalThreshold.push($("#logCriticalThreshold"+i).val());
		}
		$.ajax({
			type : "POST",
			url : "addNewThresholdOnSubmitForLogFile.html",
			data : 'thresholdName='+thresholdName +'&thresholdEnabled='+thresholdEnabled +
			'&notificationEnabled='+notificationEnabled+'&defaultEmailId='+additionalEmailId+'&threshold_group_name_sel='+threshold_group_name_sel+
			'&threshold_hostName='+threshold_hostName+'&threshold_parameter='+threshold_parameter+'&id='+id+'&logFileName='+logFileName+'&logWarningThreshold='+logWarningThreshold+'&logCriticalThreshold='+logCriticalThreshold,
			success : function(response) {
				window.location.href='manageThresHold.html';
			}
		});
	}

	else if(selectedParam == "1006")
	{
		for(var i=0;i<serviceId.length;i++)
		{
			serviceName.push($("#serviceName"+i).val());
		}

		$.ajax({
			type : "POST",
			url : "addNewThresholdOnSubmitForServices.html",
			data : 'thresholdName='+thresholdName +'&thresholdEnabled='+thresholdEnabled +
			'&notificationEnabled='+notificationEnabled+'&defaultEmailId='+additionalEmailId+'&threshold_group_name_sel='+threshold_group_name_sel+
			'&threshold_hostName='+threshold_hostName+'&threshold_parameter='+threshold_parameter+'&id='+id+'&serviceName='+serviceName,
			success : function(response) {
				window.location.href='manageThresHold.html';
			}
		});
	}else{
			if(id == false)
			{
				id = "null";
			}
			var selectedParam = $("#threshold_parameter").val();
	thresholdName=$('#threshold_Name').val();
	resp_url = $('#urlResp').val();
	resp_app = $('#appResp').val();
	thresholdEnabled=true;
	notificationEnabled=true;
	thresholdDataType=$('#threshold_alert_breach_dataType').val();
	warningHighThreshold=$('#high_warning_threshold').val();
	warningLowThreshold=$('#low_warning_threshold').val();
	warningBreachDuration=$('#threshold_warning_breach_duration_val').val();+$('#threshold_warning_breach_duration').val();
	alertHighThreshold=$('#high_alert_threshold').val();
	alertLowThreshold=$('#low_alert_threshold').val();
	alertBreachDuration=$('#threshold_alert_breach_duration_val').val();+$('#threshold_alert_breach_duration').val();
	additionalEmailId=$('#threshold_additionalEmailList').val();

	var buffer = '';
	var e = document.getElementById("threshold_group_name_sel");
	for ( var i = 0; i < e.options.length; i++) {
		buffer += e.options[i].value + ",";
	}
	 threshold_group_name_sel = buffer.toString();
	 threshold_hostName=$('#threshold_hostName').val();
	 threshold_parameter=$('#threshold_parameter').val();
	 threshold_email_ContentName=$('#threshold_email_ContentName').val();
	$.ajax({
		type : "POST",
		url : "addNewThresholdOnSubmit.html",
		data : 'thresholdName='+thresholdName +'&thresholdEnabled='+thresholdEnabled +'&thresholdDataType='+thresholdDataType+
		   '&warningHighThreshold='+warningHighThreshold +'&warningLowThreshold='+warningLowThreshold+'&warningBreachDuration='+warningBreachDuration+
		   '&alertHighThreshold='+alertHighThreshold+'&alertLowThreshold='+alertLowThreshold+'&alertBreachDuration='+alertBreachDuration+
		  '&notificationEnabled='+notificationEnabled+'&defaultEmailId='+additionalEmailId+'&threshold_group_name_sel='+threshold_group_name_sel+
		  '&threshold_hostName='+threshold_hostName+'&threshold_parameter='+threshold_parameter+'&id='+id+'&respURL='+resp_url+'&respApp='+resp_app,
		
		success : function(response) {
			if(response.indexOf("Select")>-1||response.indexOf("Empty")>-1||response.indexOf("email")>-1){alert(response);
			}else{
			window.location.href="manageThresHold.html";
			}},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    	openAlert(jqXHR);
	    	openAlert(textStatus);
	    	openAlert(errorThrown);
	    },
		cache : false
	}); 
	}//end of else
}

function assignList() {
	$('#threshold_group_name :selected').each(
			function(i, selected) {
				// append to second list box
				$('#threshold_group_name_sel').append(
						'<option value="'+selected.value+'">'
								+ selected.text + '</option>');
				// remove from first list box
				$("#threshold_group_name option[value='" + selected.value + "']").remove();
			});
}

function unassignList() {
	$('#threshold_group_name_sel :selected').each(
			function(i, selected) {
				// append to first list box
				$('#threshold_group_name').append('<option value="'+selected.value+'">'+ selected.text + '</option>');
				// remove from second list box
				$("#threshold_group_name_sel option[value='" + selected.value + "']").remove();
			});
}

function getQueryVariable(variable)
{
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}


function setSelectedInList(componentId, selectedValue)
{
	$options = $('#' + componentId + ' option');
	$options.filter('[value="' + selectedValue+ '"]').prop('selected', true);
	
 }*/