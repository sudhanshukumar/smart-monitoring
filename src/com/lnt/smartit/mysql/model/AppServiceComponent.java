package com.lnt.smartit.mysql.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="app_service_comp")
public class AppServiceComponent {
	
	@Id
	@Column(name="id")
	private String appServiceCompId;
	
	@ManyToOne
	@JoinColumn(name = "app_id", referencedColumnName = "id", insertable = false, updatable = false)
	private Applications app;
	
	@ManyToOne
	@JoinColumn(name = "service_comp_type_code", referencedColumnName = "code", insertable = false, updatable = false)
	private ServiceComponentType typecode;
	
	@ManyToOne
	@JoinColumn(name = "service_component_code", referencedColumnName = "code", insertable = false, updatable = false)
	private ServiceComponent serviceComponent ;
	
	@Column(name="description")
	private String description;
	
	@Column(name="void_ind")
	private String voidInd;

	public String getAppServiceCompId() {
		return appServiceCompId;
	}

	public void setAppServiceCompId(String appServiceCompId) {
		this.appServiceCompId = appServiceCompId;
	}

	public Applications getApp() {
		return app;
	}

	public void setApp(Applications app) {
		this.app = app;
	}

	public ServiceComponentType getTypecode() {
		return typecode;
	}

	public void setTypecode(ServiceComponentType typecode) {
		this.typecode = typecode;
	}

	public ServiceComponent getServiceComponent() {
		return serviceComponent;
	}

	public void setServiceComponent(ServiceComponent serviceComponent) {
		this.serviceComponent = serviceComponent;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getVoidInd() {
		return voidInd;
	}

	public void setVoidInd(String voidInd) {
		this.voidInd = voidInd;
	}
	
	

}
