/**
 * 
 */
package com.lnt.smartit.rule.mysql.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author 10612068
 *
 */
@Entity
@Table(name="operation")
public class Operation implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name="id")
	@Id
	@GeneratedValue
	private int operationId;
	
	@Column(name="exec_seq_no")
	private int execSequence;
	
	@ManyToOne
	@JoinColumn(name="operation_type_code", referencedColumnName="code")
	private OperationType operationType;
	
	@ManyToOne
	@JoinColumn(name="expression_id_lhs",referencedColumnName="id")
	private Expression exprLHS;
	
	@ManyToOne
	@JoinColumn(name="expression_id_rhs",referencedColumnName="id")
	private Expression exprRHS;
	
	public int getOperationId() {
		return operationId;
	}
	public void setOperationId(int id) {
		this.operationId = id;
	}
	public int getExecSequence() {
		return execSequence;
	}
	public void setExecSequence(int execSequence) {
		this.execSequence = execSequence;
	}
	public OperationType getOperationType() {
		return operationType;
	}
	public void setOperationType(OperationType operationType) {
		this.operationType = operationType;
	}
	public Expression getExprLHS() {
		return exprLHS;
	}
	public void setExprLHS(Expression exprLHS) {
		this.exprLHS = exprLHS;
	}
	public Expression getExprRHS() {
		return exprRHS;
	}
	public void setExprRHS(Expression exprRHS) {
		this.exprRHS = exprRHS;
	}
	
	
	
	

}
