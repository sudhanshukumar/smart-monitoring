package com.lnt.smartit.services;

import java.util.List;
import java.util.Map;

import com.lnt.smartit.models.GridHeaderModel;


// TODO: Auto-generated Javadoc
/**
 * The Interface ExportService.
 * Author 	: Pinakin Abhyankar
 * Date 	: 15/06/2016
 */
public interface ExportService {

	/**
	 * Export as excel.
	 *
	 * @param excelFilePath the excel file path
	 * @param sheetName the sheet name
	 * @param gridData the grid data
	 * @param headers the headers
	 * @return true, if export as excel
	 */
	public boolean exportAsExcel(String excelFilePath, String sheetName, List<Map<String, Object>> gridData, List<GridHeaderModel> headers);
	
	/**
	 * Export as pdf.
	 *
	 * @param pdfFilePath the pdf file path
	 * @param gridData the grid data
	 * @param headers the headers
	 * @param columnCount the column count
	 * @return true, if export as pdf
	 */
	public boolean exportAsPDF(String pdfFilePath, List<Map<String, Object>> gridData, List<GridHeaderModel> headers, Integer columnCount);
}
