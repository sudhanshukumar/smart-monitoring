package com.lnt.smartit.mysql.dao;

import java.util.List;

import com.lnt.smartit.mysql.model.SoftwareMaster;

public interface SoftwareDao {
	public List<SoftwareMaster> getAllSoftwareMaster();
	public void saveNewService(SoftwareMaster softwareMaster);
	public SoftwareMaster getApplicationMasterById(SoftwareMaster softMaster);
	public String deleteService(SoftwareMaster softMaster);
	public List<SoftwareMaster> checkIfExists(SoftwareMaster softMaster);
	public SoftwareMaster getServiceById(SoftwareMaster softwareMaster);
	public boolean exists(SoftwareMaster softwareMaster);
}
