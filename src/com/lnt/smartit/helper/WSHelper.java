package com.lnt.smartit.helper;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.lnt.smartit.constants.URLConstants;
import com.lnt.smartit.constants.WSConstants;

public class WSHelper {
	
public static String getBaseURL(){
	String baseURL = null;
	InputStream paramPropertiesStream = null;
	try{
		Properties props = new Properties();
		paramPropertiesStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("params.properties");
		props.load(paramPropertiesStream);
		baseURL = props.getProperty("ws.baseurl");
	}
	catch(FileNotFoundException e){
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}
	finally{
		try {
			paramPropertiesStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	return baseURL;
}

public static String constructURLForMachineData(String param,String vmIP,String osType) {
	StringBuffer sbf = new StringBuffer();
	if(osType.equalsIgnoreCase("windows")){
		sbf.append(URLConstants.HTTP_PROTOCOL);
		sbf.append(WSHelper.getBaseURL());
		sbf.append("WIN"+param);
		sbf.append(URLConstants.URL_DEL);
		sbf.append(vmIP);
		sbf.append(URLConstants.URL_DEL);
	}
	else if(osType.equalsIgnoreCase("linux")){
		sbf.append(URLConstants.HTTP_PROTOCOL);
		sbf.append(WSHelper.getBaseURL());
		sbf.append(param);
		sbf.append(URLConstants.URL_DEL);
		sbf.append(vmIP);
		sbf.append(URLConstants.URL_DEL);
	}
	else{
		sbf.append(URLConstants.HTTP_PROTOCOL);
		sbf.append(WSHelper.getBaseURL());
		sbf.append(param);
		sbf.append(URLConstants.URL_DEL);
		sbf.append(vmIP);
		sbf.append(URLConstants.URL_DEL);
	}
	return sbf.toString();
}
public static String constructURLForResponseTimeData(String param,String addressedMachine) {
	StringBuffer sbf = new StringBuffer();
	sbf.append(URLConstants.HTTP_PROTOCOL);
	sbf.append(WSHelper.getBaseURL());
	sbf.append(param);
	sbf.append(URLConstants.URL_DEL);
	sbf.append(addressedMachine);
	sbf.append(URLConstants.URL_DEL);
	System.out.println("response"+sbf);
	return sbf.toString();
}

public static String constructURLForCorrealtingResponseTime(String param,String vmIP) {
	StringBuffer sbf = new StringBuffer();
	sbf.append(URLConstants.HTTP_PROTOCOL);
	sbf.append(WSHelper.getBaseURL());
	sbf.append(param);
	sbf.append(URLConstants.URL_DEL);
	sbf.append(vmIP);
	sbf.append(URLConstants.URL_DEL);
	return sbf.toString();
}
public static String constructURLForPredictionResponseTime(String app,String page) {
	StringBuffer sbf = new StringBuffer();
	sbf.append(URLConstants.HTTP_PROTOCOL);
	sbf.append(WSConstants.PREDICTION_BASEURL);
	sbf.append(app.replaceAll(" ","+"));
	sbf.append(URLConstants.URL_DEL);
	sbf.append(page);
	sbf.append(URLConstants.URL_DEL);
	return sbf.toString();
}
public static String constructURLTogetLastUpdatedRespTime(String param,String app,String page,String limit) {
	StringBuffer sbf = new StringBuffer();
	sbf.append(URLConstants.HTTP_PROTOCOL);
	sbf.append(WSHelper.getBaseURL());
	sbf.append(param);
	sbf.append(URLConstants.URL_DEL);
	sbf.append(app.replaceAll(" ","+"));
	sbf.append(URLConstants.URL_DEL);
	sbf.append(page);
	sbf.append(URLConstants.URL_DEL);
	sbf.append(limit);
	sbf.append(URLConstants.URL_DEL);
	return sbf.toString();
}
public static String constructURLForURLData(String param,String vmIP) {
	StringBuffer sbf = new StringBuffer();
	sbf.append(URLConstants.HTTP_PROTOCOL);
	sbf.append(WSHelper.getBaseURL());
	sbf.append(param);
	sbf.append(URLConstants.URL_DEL);
	sbf.append(vmIP);
	sbf.append(URLConstants.URL_DEL);
	return sbf.toString();
}
}
