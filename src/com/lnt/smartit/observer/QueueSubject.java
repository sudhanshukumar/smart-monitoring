package com.lnt.smartit.observer;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Queue;

import org.apache.log4j.Logger;

import com.lnt.smartit.models.AllMachinesAlertMessages;
/**
 * @author 294766
 *
 */
public class QueueSubject implements QueueSubjectI {
	private static Logger logger = Logger.getLogger(QueueSubject.class);

	List<ObserverI> observerList = new ArrayList<ObserverI>();	
	private final Object MUTEX= new Object();
	public static Queue<AllMachinesAlertMessages> alertMessageEventQueue = new LinkedList<AllMachinesAlertMessages>();
	public static int eventCounter = 0;
	public static boolean changed = false;
	int clearThreshold = 0;
	static int count=0; 
	
	public QueueSubject() {
		InputStream paramPropertiesStream = null;
		try{
			Properties props = new Properties();
			paramPropertiesStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("params.properties");
			props.load(paramPropertiesStream);
			clearThreshold = Integer.parseInt(props.getProperty("temp.queue.threshold"));
			logger.info("Clear event threshold = "+clearThreshold);
		}
		catch(FileNotFoundException e){
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally{
			try {
				if(paramPropertiesStream != null)
					paramPropertiesStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void pushDataInQueue() {
	}
	
	@Override
	public void register(ObserverI observer) {
		logger.debug("No. of observers = "+ count);
		if(null == observer) 
			logger.debug("Observer is null");
		List<ObserverI> observersLocal = null;
		synchronized (MUTEX) {
			observersLocal = new ArrayList<ObserverI>(this.observerList);
        	if(observersLocal.size() > 0){
	        	for(ObserverI ob : observersLocal){
	        		if(ob.getName().equals(observer.getName()) && ob.getResponse().equals(observer.getResponse())){
	        			logger.debug("Observer already exists");
	        			return;
	        		}
	        	}
        	}
        	observerList.add(observer);
			logger.debug("Observer Added........ "+System.currentTimeMillis() +" "+ observer.getName());
			count++;
		}
	}
	
	@Override
	public void unRegister(ObserverI observer) {
		if(null == observer) logger.debug("Observer is null");
		List<ObserverI> observersLocal = null;
		synchronized (MUTEX) {
			observersLocal = new ArrayList<ObserverI>(this.observerList);
        	for(ObserverI ob : observersLocal){
        		if(ob.getName().equals(observer.getName())){
        			observerList.remove(ob);
        			try {
						ob.getResponse().getWriter().close();
					} catch (IOException e) {
						e.printStackTrace();
					}
        			logger.debug("Observer removed........ "+ ob.getName());
        			count--;
        		}
        	}
		}
	}
	
	@Override
	public void notifyObservers() {
		logger.debug("------notifyObservers-------");
		List<ObserverI> observersLocal = null;
        //synchronization is used to make sure any observer registered after message is received is not notified
        synchronized (MUTEX) {
            if (!changed)
                return;
            observersLocal = new ArrayList<ObserverI>(this.observerList);
            QueueSubject.changed=false;
        }
        logger.debug("Observers = "+observersLocal.size());
        
        /*
         * Synchronization is used to make sure no new events are missed while sending clear event to dashboard
         */
        synchronized (this) {
        	if(QueueSubject.eventCounter == clearThreshold){
        		logger.debug("No. of events has crossed the threshold value "+ clearThreshold);
        		QueueSubject.eventCounter = 0;
		        for (ObserverI obj : observersLocal) {
		        	logger.debug("Updating clear event for Observer : "+obj.getName());
					obj.update(obj,"Clear Dashboard");
		        }
	        } else {
	        	for (ObserverI obj : observersLocal){
	        		logger.debug("Updating for Observer : "+obj.getName());
	        		obj.update(obj,null);
	        	}
	        	QueueSubject.alertMessageEventQueue.poll();
        	}
        }
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public Object getUpdates(ObserverI observer) {
		String queueData = "";
		if (!QueueSubject.alertMessageEventQueue.isEmpty()) {
			Iterator it = alertMessageEventQueue.iterator();
			while (it.hasNext()) {
				queueData = queueData + it.next();
			}
		}
		return queueData;
	}
}
