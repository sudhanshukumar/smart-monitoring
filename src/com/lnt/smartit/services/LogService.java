package com.lnt.smartit.services;

import java.util.List;

import com.lnt.smartit.mysql.model.LogParams;
import com.lnt.smartit.solr.pojo.Query;


public interface LogService {

	public String getLogId(String appId, String hostname, String logType);

	public LogParams getLogParams(String logId, String param);

	public String getAppServiceComponentId(String appId, String compType,String compCode);

	public String getLogServiceComponentParamValue(String logId,String appServiceCompId);

	public String getLogTypeByOS(String os);

	public String getOSfromdeviceId(String deviceId);

	public String getAppIdByName(String appName);

	public List<Object> getMapforSolrData(Query uquery) throws Exception;

}
