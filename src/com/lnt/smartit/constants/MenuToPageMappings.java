package com.lnt.smartit.constants;

import java.util.LinkedHashMap;
import java.util.Map;

public class MenuToPageMappings {
	
	public static final Map<String, String> menuPageMap;
    static
    {
    	menuPageMap = new LinkedHashMap<String, String>();
    	menuPageMap.put("Dashboard", "Dashboard/dashboard");
    	menuPageMap.put("AlertHistory", "History/admin");
    	menuPageMap.put("ManageDevice", "Manage/manageDevice");
    	menuPageMap.put("ManageThreshold", "Manage/manageThreshold");
    	menuPageMap.put("LogAnalysis", "solr/LogAnalysis");
    	menuPageMap.put("LogCorrelation", "solr/correlation");
    	menuPageMap.put("CreateReport", "");
    	menuPageMap.put("CreateDashboard", "");
    	menuPageMap.put("ViewDashboard", "");
    	menuPageMap.put("MQEnrollment", "Discovery/MQEnrollment");
    	menuPageMap.put("ManageEnrollment", "Discovery/manageEnrollment");
    	menuPageMap.put("MQEnquiry", "Discovery/MQEnquiry");
    	menuPageMap.put("ManageMQPreference", "Discovery/managePreference");
    	menuPageMap.put("BrokerEnrollment", "Discovery/BrokerEnrollment");
    	menuPageMap.put("ManageBroker", "Discovery/manageBroker");
    	menuPageMap.put("EnquireBroker", "Discovery/BrokerEnquiry");
    	menuPageMap.put("ManageBrokerPreference", "Discovery/manageBrokerPreference");
    	menuPageMap.put("InterfaceRepository", "Discovery/InterfaceRepo");
    }

}
