/**
 * 
 */
$(document).ready(function() {
	document.getElementById("headingGroupNotification").innerHTML="Add New Notification Group";
	
	$("#alert").hide();
	$("#preview_th_buttons").hide();
	$("#preview_ok").click(function(){
		window.location.href="newNotificationGroup.html";
	});
	
	var action = getQueryVariable("action");
	var id=getQueryVariable("groupId");
	
	if(action == "edit")
	{
		document.getElementById("headingGroupNotification").innerHTML="Edit Notification Group";
	$.ajax({
		type : "POST",
		url : "editNotification.html",
		data : "groupId=" + id, 
		success : function(response) {
			$('#notification_groupName').val(response.groupName);
			$('#notification_groupName_desc').val(response.description);	
			$('#notification_groupName_emailList').val(response.groupEmailIds);	
		},
        cache: false
	});
	}
	if(action == "preview")
	{
		$("#addNotificationGroup_buttons").hide();
		$("#preview_th_buttons").show();
		document.getElementById("headingGroupNotification").innerHTML="Preview Notification Group";
	$.ajax({
		type : "POST",
		url : "previewNotification.html",
		data : "groupId=" + id, 
		success : function(response) {
			$('#notification_groupName').val(response.groupName);
			$('#notification_groupName_desc').val(response.description);	
			$('#notification_groupName_emailList').val(response.groupEmailIds);
			$('#notification_groupName').attr('readonly', true);
			$('#notification_groupName_desc').attr('readonly', true);
			$('#notification_groupName_emailList').attr('readonly', true);
		},
        cache: false
	});
	
	}
	
	$("#addNotificationGroup_submit").click(function() {
		addNotificationGroup_submit(id);
		window.location.href="manageNotificationGroup.html";
	});
	
	$("#addNotificationGroup_cancel").click(function() {
		window.location.href="manageNotificationGroup.html";
	});
	
	$("#preview_ok").click(function() {
		window.location.href="manageNotificationGroup.html";
	});
});

var groupName,groupEmailIds,description;

function addNotificationGroup_submit(id) {
	groupName=$('#notification_groupName').val();
	groupEmailIds=$('#notification_groupName_emailList').val();
	description=$('#notification_groupName_desc').val();
	if(id==false)
		{
		id=null;
		}
	
	$.ajax({
		type : "POST",
		url : "addNotificationGroupOnSubmit.html",
		data : 'groupName='+groupName +'&description='+description +'&groupEmailIds='+groupEmailIds+'&id='+id,
		
		success : function(response) {
			if(response.indexOf("Name")>-1 ||response.indexOf("email")>-1){
				openAlert(response);
			}else{
				window.location.href="manageNotificationGroup.html";
			}
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    	openAlert(jqXHR);
	    	openAlert(textStatus);
	    	openAlert(errorThrown);
	    },
		cache : false
	}); 
	
}
function getQueryVariable(variable)
{
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}