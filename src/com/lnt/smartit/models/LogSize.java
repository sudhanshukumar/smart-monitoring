package com.lnt.smartit.models;


public class LogSize {

	private String host;
	private String logfileName;
	private double file_size;
	private String path;
	private long timestamp;
	private String app;
	private String Date;
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getLogfileName() {
		return logfileName;
	}
	public void setLogfileName(String logfileName) {
		this.logfileName = logfileName;
	}
	public double getFile_size() {
		return file_size;
	}
	public void setFile_size(double file_size) {
		this.file_size = file_size;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public String getApp() {
		return app;
	}
	public void setApp(String app) {
		this.app = app;
	}
	@Override
	public String toString() {
		return "LogSize [host=" + host + ", logfileName=" + logfileName
				+ ", file_size=" + file_size + ", path=" + path
				+ ", timestamp=" + timestamp + ", app=" + app + ", Date="
				+ Date + "]";
	}
}
