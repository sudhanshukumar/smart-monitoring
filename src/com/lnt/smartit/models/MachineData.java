/**
 * 
 */
package com.lnt.smartit.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 299273
 *
 */
public class MachineData implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String hostName;
	private String IP;
	//Time
	private Calendar datetime;
	private Calendar dataReceivedTime;
	private List<Process> top3MemProcess = new ArrayList<Process>();
	//RAM
	private Long rammemTotal;
	private Long rammemUsed;
	private Long rammemFree;
	private Long rammemBuffers;
	private Long rammemShared;
	private Long rammemCached;
	private Long ramtotal;
	private Long ramused;
	private Long ramfree;
	//CPU
	private Double cpuloadAverage5min;
	private Double cpuloadAverage10min;
	private Double cpuloadAverage15min;
	//Disk
	private List<FileSystem> fileSystem = new ArrayList<FileSystem>();
	private Double totalSize;
	private Double totalUsed;
	private Double totalAvail;
	private Double totalUsePercent;
	//Heap
	private Map<Integer,String> javaProcesses = new HashMap<Integer, String>();
	private List<Heap> heapList=new ArrayList<Heap>();
	
	public void addFileSystem(FileSystem fileSystem) {
		this.fileSystem.add(fileSystem);
	}
	
	public void putJavaProcesses(Integer pid, String javaProcessName) {
		this.javaProcesses.put(pid, javaProcessName);
	}

	public void addHeapList(Heap heap) {
		this.heapList.add(heap);
	}
	
	public Calendar getDatetime() {
		return datetime;
	}

	public void setDatetime(Calendar datetime) {
		this.datetime = datetime;
	}

	public Calendar getDataReceivedTime() {
		return dataReceivedTime;
	}

	public void setDataReceivedTime(Calendar dataReceivedTime) {
		this.dataReceivedTime = dataReceivedTime;
	}

	public List<Process> getTop3MemProcess() {
		return top3MemProcess;
	}

	public void setTop3MemProcess(List<Process> top3MemProcess) {
		this.top3MemProcess = top3MemProcess;
	}

	public Long getRammemTotal() {
		return rammemTotal;
	}

	public void setRammemTotal(Long rammemTotal) {
		this.rammemTotal = rammemTotal;
	}

	public Long getRammemUsed() {
		return rammemUsed;
	}

	public void setRammemUsed(Long rammemUsed) {
		this.rammemUsed = rammemUsed;
	}

	public Long getRammemFree() {
		return rammemFree;
	}

	public void setRammemFree(Long rammemFree) {
		this.rammemFree = rammemFree;
	}

	public Long getRammemBuffers() {
		return rammemBuffers;
	}

	public void setRammemBuffers(Long rammemBuffers) {
		this.rammemBuffers = rammemBuffers;
	}

	public Long getRammemShared() {
		return rammemShared;
	}

	public void setRammemShared(Long rammemShared) {
		this.rammemShared = rammemShared;
	}

	public Long getRammemCached() {
		return rammemCached;
	}

	public void setRammemCached(Long rammemCached) {
		this.rammemCached = rammemCached;
	}

	public Long getRamtotal() {
		return ramtotal;
	}

	public void setRamtotal(Long ramtotal) {
		this.ramtotal = ramtotal;
	}

	public Long getRamused() {
		return ramused;
	}

	public void setRamused(Long ramused) {
		this.ramused = ramused;
	}

	public Long getRamfree() {
		return ramfree;
	}

	public void setRamfree(Long ramfree) {
		this.ramfree = ramfree;
	}

	public Double getCpuloadAverage5min() {
		return cpuloadAverage5min;
	}

	public void setCpuloadAverage5min(Double cpuloadAverage5min) {
		this.cpuloadAverage5min = cpuloadAverage5min;
	}

	public Double getCpuloadAverage10min() {
		return cpuloadAverage10min;
	}

	public void setCpuloadAverage10min(Double cpuloadAverage10min) {
		this.cpuloadAverage10min = cpuloadAverage10min;
	}

	public Double getCpuloadAverage15min() {
		return cpuloadAverage15min;
	}

	public void setCpuloadAverage15min(Double cpuloadAverage15min) {
		this.cpuloadAverage15min = cpuloadAverage15min;
	}

	public List<FileSystem> getFileSystem() {
		return fileSystem;
	}

	public void setFileSystem(List<FileSystem> fileSystem) {
		this.fileSystem = fileSystem;
	}

	public Double getTotalSize() {
		return totalSize;
	}

	public void setTotalSize(Double totalSize) {
		this.totalSize = totalSize;
	}

	public Double getTotalUsed() {
		return totalUsed;
	}

	public void setTotalUsed(Double totalUsed) {
		this.totalUsed = totalUsed;
	}

	public Double getTotalAvail() {
		return totalAvail;
	}

	public void setTotalAvail(Double totalAvail) {
		this.totalAvail = totalAvail;
	}

	public Double getTotalUsePercent() {
		return totalUsePercent;
	}

	public void setTotalUsePercent(Double totalUsePercent) {
		this.totalUsePercent = totalUsePercent;
	}

	public Map<Integer, String> getJavaProcesses() {
		return javaProcesses;
	}

	public void setJavaProcesses(Map<Integer, String> javaProcesses) {
		this.javaProcesses = javaProcesses;
	}

	public List<Heap> getHeapList() {
		return heapList;
	}

	public void setHeapList(List<Heap> heapList) {
		this.heapList = heapList;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getIP() {
		return IP;
	}

	public void setIP(String iP) {
		IP = iP;
	}

	@Override
	public String toString() {
		return "MachineData [hostName=" + hostName + ", IP=" + IP
				+ ", datetime=" + datetime + ", dataReceivedTime="
				+ dataReceivedTime + ", top3MemProcess=" + top3MemProcess
				+  ", rammemTotal=" + rammemTotal
				+ ", rammemUsed=" + rammemUsed + ", rammemFree=" + rammemFree
				+ ", rammemBuffers=" + rammemBuffers + ", rammemShared="
				+ rammemShared + ", rammemCached=" + rammemCached
				+ ", ramtotal=" + ramtotal + ", ramused=" + ramused
				+ ", ramfree=" + ramfree + ", cpuloadAverage5min="
				+ cpuloadAverage5min + ", cpuloadAverage10min="
				+ cpuloadAverage10min + ", cpuloadAverage15min="
				+ cpuloadAverage15min + ", fileSystem=" + fileSystem
				+ ", totalSize=" + totalSize + ", totalUsed=" + totalUsed
				+ ", totalAvail=" + totalAvail + ", totalUsePercent="
				+ totalUsePercent + ", javaProcesses=" + javaProcesses
				+ ", heapList=" + heapList + "]";
	}
}


