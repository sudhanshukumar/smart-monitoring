package com.lnt.smartit.models;


public class Service {
	
	private String Date;
	private String server;
	private String service_name;
	private String service_display_name;
	private String status;
	private long timestamp;
	
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public String getService_name() {
		return service_name;
	}
	public void setService_name(String service_name) {
		this.service_name = service_name;
	}
	public String getService_display_name() {
		return service_display_name;
	}
	public void setService_display_name(String service_display_name) {
		this.service_display_name = service_display_name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "Service [Date=" + Date + ", server=" + server
				+ ", service_name=" + service_name + ", service_display_name="
				+ service_display_name + ", status=" + status + ", timestamp="
				+ timestamp + "]";
	}
}
