/**
 * 
 */
$(document).ready(function() {
	$("#alert").hide();
	$("#confirmDelete").hide();
	$("#addNewNotification").click(function() {
		//alert('addNewNotification');
		window.location.href="newNotificationGroup.html";
	});	
	manageNotificationGrid();
});

function manageNotificationGrid() {
	
	$("#addednotificationGrpHistory").jqGrid({
		
		jsonReader : {
		     root: "gridData", //an array that contains the actual data
		     page: "page", //current page of the query
		     total: "total",  //total pages for the query
		     records: "records",//total number of records for the query
		     repeatitems: true, 
		     id: "groupId"    //the unique id of the row
		   },
		   url :'populateNotificationGrpGrid.html',
			datatype :"json",
			mtype :"POST",
			colNames:['Group Id','Group Name','Group Descrition','Actions'],
			colModel:[
						{
							name : 'groupId',
							label : 'Group Id',
							align : 'left',
							width:30,
							hidden:true,
							sortable: true
						},
						{
							name : 'groupName',
							label : 'Group Name',
							align : 'left',
							width:50,
							sortable: true
						},
						{
							name : 'description',
							label : 'Group Descrition',
							align : 'left',
							width:70
						},{ width:80, fixed:true, sortable:false, resize:false,
							formatter : function (cellvalue, options, rowObject) {
					    		  return addedNotificaActionButtons(cellvalue, options, rowObject);
					    	}}
			          ],
			          pager : '#addednotificationGrpPager',
						rowNum : 10,
						rowList : [ 10, 20, 30 ],
						viewrecords : true,
						gridview : true,
						loadonce: true,
						width : 900,
						height: 300
	});
}


function addedNotificaActionButtons(cellvalue, options, rowObject){
	var updateNotificaActionLink = '<img title="Edit"  onClick=editNotificationGrp("'+encodeURI(JSON.stringify(rowObject))+'"); src="images/common/edit-6-16.png" />';
	var deleteNotificaActionLink = '<img title="Delete" onClick=deleteNotificationGrp("'+encodeURI(JSON.stringify(rowObject))+'") src="images/common/delete-3-16.png"/>';
	var previewNotificaActionLink= '<img title="Preview" onClick=previewNotificationGrp("'
	    +encodeURI(JSON.stringify(rowObject))
	    +'") src="images/common/preview-16.png"/>';
	var actionLink = "&nbsp;"+previewNotificaActionLink + "&nbsp;"+"&nbsp;"+updateNotificaActionLink+ "&nbsp;"+"&nbsp;"+deleteNotificaActionLink; 
	return actionLink;
}
function editNotificationGrp(value){
	var dataJSON = JSON.parse(decodeURI(value));
	var groupId=dataJSON.groupId;
	window.location.href = "newNotificationGroup.html?groupId="+groupId+"&action=edit";
}
function previewNotificationGrp(value){
	var dataJSON = JSON.parse(decodeURI(value));
	var groupId=dataJSON.groupId;
	window.location.href = "newNotificationGroup.html?groupId="+groupId+"&action=preview";
}
function deleteNotificationGrp(value){
	$("#confirmDelete").show();
	$( "#confirmDelete" ).dialog({
		 resizable: false,
		 autoOpen: false,
		 height:200,
		 width: 340,
		 position: [485, 185],
		 modal: true,
		 buttons: {
			 "Yes": function() {
				 	var dataJSON = JSON.parse(decodeURI(value));
				 	var grpid=dataJSON.groupId;
				 	$.ajax({
				 		type : "POST",
				 		url : "deleteNotificationGrp.html",
				 		data : "groupId=" + grpid, 
				 		success : function(response) {
				 			window.location.href = "manageNotificationGroup.html";
				 		},
				 		cache: false
				 	}); 
			 },
				Cancel: function() {
			
	 							$( this ).dialog( "close" );
	 						}
		 }
		 
	});
	
	$("#confirmDelete").dialog("open");
	$(".ui-dialog-title").text('Delete?');
	$(".ui-dialog-titlebar-close").hide();
	$("#confirmDelete").text("Delete this Group ?");
}
