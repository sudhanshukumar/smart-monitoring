/**
 * 
 */
package com.lnt.smartit.observer;

import javax.servlet.http.HttpServletResponse;

/**
 * @author 298102
 *
 */
public interface ObserverI {
	public void setSubject(QueueSubjectI sub);
	public void setResponse(HttpServletResponse response);
	public HttpServletResponse getResponse();
	public void setName(String name);
	public String getName();
	/* Added String parameter clearMsg to send a clear event to UI*/
	public void update(ObserverI observer, String clearMsg);
}
