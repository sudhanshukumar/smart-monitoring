package com.lnt.smartit.services;

import java.util.List;

import com.lnt.smartit.mysql.model.AppDeviceMaster;
import com.lnt.smartit.mysql.model.DeviceApplicationMapping;
import com.lnt.smartit.mysql.model.DeviceMaster;
import com.lnt.smartit.mysql.model.DeviceParams;
import com.lnt.smartit.mysql.model.DeviceSoftwareMapping;

public interface DeviceService {
	public List<DeviceParams> getDeviceParams(String appId);
	public void saveDeviceparams(DeviceParams deviceParams);
	public void saveAppDeviceMaster(AppDeviceMaster appDeviceMaster);
	public String getDeviceTypecode(String searchCriteria);
	public Integer getMaxAppDeviceId();
	public AppDeviceMaster getAppDeviceMasterById(AppDeviceMaster appDeviceMaster);
	public String deleteAppDeviceMaster(AppDeviceMaster devMaster);
	public String getDeviceMasterByHostName(String name);
	void deleteDeviceParams(String deviceId);
	public List<DeviceParams> getDeviceParamsById(String deviceid);
	public AppDeviceMaster getOsById(String deviceid);
	public void updateDeviceparams(DeviceParams deviceparams);
	public void updateAppDeviceMaster(AppDeviceMaster appdevicemaster1);

}
