<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><spring:message code="views.manage.threshold.title"></spring:message></title>
<link rel="shortcut icon"  href="favicon.ico" />

<!-- Common CSS files of Bootstrap and JQuery -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="assets/css/bootstrap-combined.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/common/ui.jqgrid.css" />
<link rel="stylesheet" type="text/css" href="css/common/jquery-ui.min.css" />
<link rel="stylesheet" type="text/css" href="css/common/jquery.datetimepicker.css" />

<!-- Custom CSS files for this Page -->
<link rel="stylesheet" href="css/custom/specific/nav.css">

<!-- Common Scripts of Bootstrap and JQuery -->
<script src="scripts/common/jquery-2.2.3.min.js"></script>
<script src="scripts/common/jquery-ui.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="scripts/common/grid.locale-en.js"></script>
<script type="text/javascript" src="scripts/common/jquery.datetimepicker.js"></script>
<script src="scripts/common/jquery.jqGrid.min.js"></script>

<!-- custom script -->
<script src="scripts/custom/manageThreshold.js"></script>
<script src="scripts/common/common.js"></script>

<!-- custom stylesheet -->
<link rel="stylesheet" type="text/css" href="css/custom/specific/manageThreshold.css" />
</head>
<body>

	<div class="container-fluid">
		<%@ include file="../common/header.jsp"%>

	<nav class="navbar navbar-inverse" style="margin-top: 0px">
	      <ul class="nav navbar-nav navbar-xs">
	        <li id="Dashboard"><a href="dashboard.html">Dashboard<span class="sr-only">(current)</span></a></li>
	        <!-- <li id="AlertHistory"><a href="problemHistory.html">Alert History</a></li> -->
	       <!--  <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Manage<span class="caret"></span></a>
	          <ul class="dropdown-menu"> -->
	            <li class="active"><a href="manageThresHold.html">Manage Rule</a></li>
	            <!-- <li><a href="#">Manage Threshold</a></li>
	          </ul>
	        </li> -->
	        <li class="dropdown" >
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Insights<span class="caret"></span></a>
	          <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
	            <li><a href="query.html" style="font-size:11px!important;">Analytics</a></li>
	           
	            <!-- <li><a href="variableAnalysis.html">Variable Analysis</a></li> -->
	            <li class="dropdown-submenu"> <a tabindex="-1" href="#" style="font-size:11px!important;">Reports</a>
                       <ul class="dropdown-menu">
                        <li><a href="kibanaDashboard.html" style="font-size:11px!important;">Prediction</a></li>
                        <li>
                           <a href="trendingreport.html" style="font-size:11px!important;">Trends</a> </li>
                         <li>
                           <a href="alerthistoryreport.html" style="font-size:11px!important;">Alert History</a> </li>
                           <li>
                           <a href="correlationreport.html" style="font-size:11px!important;">Log Correlation</a> </li>
                           <li>
                           <a href="weblogicreport.html" style="font-size:11px!important;">Weblogic Report</a> </li>
                           <li>
                           <a href="weblogicdrilldownreport.html" style="font-size:11px!important;">Weblogic DrillDown Report</a> </li>
                           <li>
                           <a href="sapapplicationreport.html" style="font-size:11px!important;">Sap Application Report</a> </li>
                        </ul>
                    </li>
	          </ul>
	        </li>
	        <!-- <li class="active" id="LogAnalysis"><a href="query.html">Insights</a></li> -->
	        <%-- <li id="LogCorrelation"><a href="correlation.html">Log Correlation</a></li>
	        <li id="VariableAnalysis"><a href="variableAnalysis.html"><spring:message code="label.admin.menu5"></spring:message></a></li>
	        <li class="dropdown" id="Reports">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reports <span class="caret"></span></a>
	          <ul class="dropdown-menu">
	            	<li id="CreateReport"><a href="charts.html">Create Report</a></li>
		            <li id="CreateDashboard"><a href="createuserdashboard.html">Create Dashboard</a></li>
		            <li id="ViewDashboard"><a href="list_dashboard.html">View Dashboard</a></li>
	          </ul>
	        </li> --%>
	      </ul>
  </nav>
  
  
	<div style="float: right; height: 40px; margin-top: 3px;">
	<div class="form-group" style="text-align: right;">
		<div class="col-md-12">
			<input type="button" class="btn btn-primary btn-sm form-control" id="addRuleBtn" value="Add Rule" style=" font-size: 12px; font-weight: bold;">
		</div>
	</div>
  </div>
  <div style="float: left; height: 30px; margin-top: 5px;">
	<div  style="text-align: left; display: -webkit-inline-box;">		
			<img src='maintenance_icon.jpg' /><p style=" font-size: 11px;">   Maintenance Window</p>
	</div>
  </div>
	<div id="manageThreshGrid" style="margin-top: 4%;" >
		<table id="manageThresholdGridTable"></table>
		<div id="manageThresholdGridPager" role="group"></div>
	</div>
	<!-- 
	<div style="float: right; height: 40px; margin-top: 10px;">
	<div class="form-group" style="text-align: right;">
		<div class="col-md-12"><input type="button" class="btn btn-primary btn-sm form-control" id="addRuleBtn" value="Add Rule"/> </div>
	</div>
  </div> -->

	<div id="confirmDelete"></div>
	<div id="dialog" title="Alert">
	  <p>Alert is already disabled</p>
	</div>
	<div id="deleteAlert" title="Alert">
  		<p>These Changes would be reflected after one business day</p>
	</div>
	
	<div id='ruleForm'>
		<table id="addRuleTable" style="width: 100%" width="100%">
			<tr>
				<td><label class="manageRuleItem" style="font-size: 11px; margin-top:10px;">Host Name: </label></td>
				<td><input type="text" id="threshold_hostName" class="manageRuleItem" style="font-size: 11px; margin-top:10px; "></td>
												
				<td><label class="FormLabel" style="font-size: 11px; margin-top:10px;">Parameter Type: </label></td>
				<td colspan="2"><select id="threshold_ParamType" class="manageRuleItem" style="font-size: 11px; margin-top:10px; ">
						<option value="0" selected>---Select---</option>
						<!-- <option value="NBR">Number</option>
						<option value="CHAR">Keyword</option> -->
					</select>
				</td>
				
				<td><input type="hidden" id="paramRuleId"/></td>
			</tr>
			<tr>
				<td><label class="FormLabel" style="font-size: 11px; margin-top:10px;">Parameter Name: <span
						class="mandatory"> </span></label></td>
				<td colspan="1">
					<select id="threshold_ParamName" class="manageRuleItem" style="font-size: 11px; margin-top:10px; ">
						<option value="0" selected>---Select---</option>					
					</select>
				</td>
			
				<td><label class="FormLabel" style="font-size: 11px; margin-top:10px;">Operator: </label></td>
				<td>
					<select id="expressionTypeLHS" class="manageRuleItem" style="font-size: 11px; margin-top:10px; ">
						<option value="0" selected>---Select---</option>					
					</select>
				</td>
			</tr>
			<tr>
				<td><label class="FormLabel" style="font-size: 11px; margin-top:10px;">Alert Type: </label></td>
				<td>
					<select id="thresholdType" class="manageRuleItem" style="font-size: 11px; margin-top:10px; ">
						<option value="0" selected>---Select---</option>					
						<!-- <option>Critical</option>
						<option>Warning</option> -->
					</select>
				</td>
						
				<td><label class="FormLabel" style="font-size: 11px; margin-top:10px;">Value: </label></td>
				<td><input type="text" id="thresholdValueLHS" class="manageRuleItem" style="font-size: 11px; margin-top:10px; "></td>
			</tr>
			<tr>
				<td><label class="FormLabel" style="font-size: 11px; margin-top:10px;">Alert Duration(Seconds): </label></td>
				<td><input type="text" id="alertDuration" class="manageRuleItem" style="font-size: 11px; margin-top:10px; "></td>
				
				<td><label class="FormLabel" style="font-size: 11px; margin-top:10px;">Logical operator: </label></td>
				<td>
					<select id="logicalOperator" class="manageRuleItem" style="font-size: 11px; margin-top:10px; ">
						<option value="null" selected>---Select---</option>					
						<!-- <option value="AND">AND</option>
						<option value="OR">OR</option> -->
					</select>
				</td>
			</tr>
			<!-- </table> -->
			<!-- <table id="RHSTable" style="width: 100%" width="100%"> -->
			<tr>
				<td><label class="FormLabel" style="font-size: 11px; margin-top:10px;">Auto Incident: </label></td>
				<td>
					<select id="autoIncident" class="manageRuleItem" style="font-size: 11px; margin-top:10px; ">
						<!-- <option value="0" selected>---Select---</option> -->					
						<option value="Yes">Yes</option>
						<option value="No">No</option>
					</select>
				</td>
				
				<td><label class="FormLabel" id="expressionTypeRHSLabel" style="font-size: 11px; margin-top:10px; ">Operator: </label></td>
				<td>
					<select id="expressionTypeRHS" class="manageRuleItem" style="font-size: 11px; margin-top:10px; ">
						<option value="0" selected>---Select---</option>
					</select>
				</td>
			</tr>
			<tr>
				<!-- <td colspan="2">&nbsp;</td> -->
				<td><label class="FormLabel" style="font-size: 11px; margin-top:10px;">Deduplication Window(Seconds): </label></td>
				<td>
					<!-- <select id="deduplicationWindow" class="manageRuleItem" style="font-size: 11px; margin-top:10px; ">
						<option value="0" selected>---Select---</option>					
						<option value="1">1 hours</option>
						<option value="4">4 hours</option>
						<option value="12">12 hours</option>
						<option value="24">24 hours</option>
					</select> -->
				<input type="text" id="deduplicationWindow" class="manageRuleItem" style="font-size: 11px; margin-top:10px; ">
				</td>
				
				<td><label class="FormLabel" id="thresholdValueRHSLabel" style="font-size: 11px; margin-top:10px; ">Value: </label></td>
				<td><input type="text" id="thresholdValueRHS" class="manageRuleItem" style="font-size: 11px; margin-top:10px; "></td>
			</tr>	
			
			<!-- 10614080 Email Notification Implementation 03/03/2017 Start -->
			<tr>
			<td><label class="FormLabel" style="font-size: 11px; margin-top:10px;">Notification: <span class="mandatory"> </span></label></td>						
			<td>
				<select id="notification" class="manageRuleItem" style="font-size: 11px; margin-top:10px; ">
					<option value="0" selected>---Select---</option>
					<option id="No" value="No">No</option>
					<option id="Yes" value="Yes">Yes</option>					
				</select>
			</td>
		<!--	<td><label class="FormLabel" style="font-size: 11px; margin-top:10px;">Maintenance : <span class="mandatory"> </span></label></td>			
			 <td>
				<select id="maintenanceWindow" class="manageRuleItem" style="font-size: 11px; margin-top:10px; ">
					<option value="0" selected>---Select---</option>
					<option id="Enable" value="Enable">Enable</option>
					<option id="Disable" value="Disable">Disable</option>					
				</select>
			</td> -->
			</tr>
			
			<tr>
			<td><label class="FormLabel" id="emailToLabel" style="font-size: 11px; margin-top:10px;">Email TO: </label></td>
			<td><input type="text" id="notificationTO" class="manageRuleItem" style="font-size: 11px; margin-top:10px; "></td>
			
			<!-- <td><label class="FormLabel" id="fromMWLabel" style="font-size: 11px; margin-top:10px;">From: </label></td>
			<td><input type="text" id="fromMW" class="manageRuleItem" style="font-size: 11px; margin-top:10px; "></td>	 -->		
			</tr>
			<tr>
			<td><label class="FormLabel" id="emailCCLabel" style="font-size: 11px; margin-top:10px;">Email CC: </label></td>
			<td><input type="text" id="notificationCC" class="manageRuleItem" style="font-size: 11px; margin-top:10px; "></td>
			
			<!-- <td><label class="FormLabel" id="toMWLabel" style="font-size: 11px; margin-top:10px;">To: </label></td>
			<td><input type="text" id="toMW" class="manageRuleItem" style="font-size: 11px; margin-top:10px; "></td> -->		
			</tr>		
			
			<!-- 10614080 Email Notification Implementation 03/03/2017 End -->
			<tr>
				<td><label class="FormLabel" id="fromMWLabel" style="font-size: 11px; margin-top:10px;">Maintenance Start Time: </label></td>
				<td> 
					<div style="width: 148px !important;border: 1px solid #ccc;height: 20px !important;">				
						<input type="text" id="fromMW" class="manageRuleItem" placeholder="yyyy-MM-dd hh:mm:ss" 
							style="font-size: 11px;margin-top: 0px !important;width: 128px !important;border: 0px solid #ccc;height: 18px !important;">
						 <i class="glyphicon glyphicon-calendar" style="top: -1px;"></i>  
					 </div>
				</td>
			</tr>	
			<tr>
				<td><label class="FormLabel" id="toMWLabel" style="font-size: 11px; margin-top:10px;">Maintenance End Time: </label></td>
				<td>	
					<div style="width: 148px !important;border: 1px solid #ccc;height: 20px !important;">	
						<input type="text" id="toMW" class="manageRuleItem" placeholder="yyyy-MM-dd hh:mm:ss"
							style="font-size: 11px;margin-top: 0px !important;width: 128px !important;border: 0px solid #ccc;height: 18px !important;">
						<i class="glyphicon glyphicon-calendar" style="top: -1px;"></i> 
					</div>
				</td>
			</tr>				
			</table>
			
			
			
		<!-- <div id="NextAlert">
			<input type="button" id="AddAlert" value="Add Alert" class="btn btn-sm form-control" style="width: 80px; margin-top: 8px; background-color: gainsboro;" />
			<div id="SecondAlertForm">
			<table id="addRuleTable" style="width: 100%" width="100%">
				<tr>
				<td><label class="FormLabel" style="font-size: 11px; margin-top:10px;">Alert Type: </label></td>
				<td><select id="thresholdType2" style="font-size: 11px; margin-top:10px;">
							<option>Critical</option>
							<option>Warning</option>
				</select></td>
				<td><label class="FormLabel" style="font-size: 11px; margin-top:10px;">Operator: </label></td>
				<td><select id="expressionTypeLHS2" style="font-size: 11px; margin-top:10px;">
				</select></td>
				<td><label class="FormLabel" style="font-size: 11px; margin-top:10px;">Value: </label></td>
				<td><input type="text" id="thresholdValueLHS2" style="font-size: 11px; margin-top:10px;">
				</td>
				<td><label class="FormLabel" style="font-size: 11px; margin-top:10px;">Logical operator: </label></td>
				<td><select id="logicalOperator2" style="font-size: 11px; margin-top:10px;">
							<option value="0" selected>--No Operator--</option>
							<option value="AND">AND</option>
							<option value="OR">OR</option>
				</select></td>
				</tr>
			</table>
			<table id="RHSTable" style="width: 100%" width="100%">
				<tr>
				<td colspan="2">&nbsp;</td>
				<td><label class="FormLabel" style="font-size: 11px; margin-top:10px;">Operator: </label></td>
				<td><select id="expressionTypeRHS2" style="font-size: 11px; margin-top:10px;">
				</select></td>
				<td><label class="FormLabel" style="font-size: 11px; margin-top:10px;">Value: </label></td>
				<td><input type="text" id="thresholdValueRHS2" style="font-size: 11px; margin-top:10px;">
				</td>
				<td colspan="2">&nbsp;</td>
				</tr>
			</table>
			</div>
		</div>
	</div> -->
	</div>
	<!-- <div id="wait" style="display:none;width:69px;height:89px;border:2px solid black;position:absolute;top:35%;left:35%;padding:2px;">
	 <img src='images/custom/demo_wait.gif' width="64" height="64" /> 
	<br>Loading..</div> -->
	
	 
	<%@ include file="../common/footer.jsp"%>
</div>
</body>
</html>