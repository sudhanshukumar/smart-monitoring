package com.lnt.smartit.services;

import java.util.List;

import com.lnt.smartit.mysql.model.ParameterMaster;

public interface ParameterService {
	public List<ParameterMaster> getParameters();
	public ParameterMaster getParameterMasterById(String id);
	public String getParameterMasterByParamName(String threshold_parameter);
}
