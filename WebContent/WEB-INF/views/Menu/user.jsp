<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=8">
<script type="text/javascript" src="scripts/common/menu/dropdowntabs.js"></script>
<link href="css/custom/general/bluetabs.css" rel="stylesheet" />

<title></title>
</head>

<body>

<div id="menu_outerBlock" align="center">

  <table width="100%" border="0">
    <tr>
      <td>
        <table width="100%" border="0"></table>
        
  <nav id="bluemenu" class="bluetabs">
  <ul>
 		 <li><a href="userDashboard.html"><spring:message code="label.user.menu1"></spring:message></a></li>
  		 <li><a href="problemHistoryUser.html"><spring:message code="label.user.menu2"></spring:message></a></li>
		 <li><a href="variableAnalysis.html"><spring:message code="label.user.menu3"></spring:message></a></li>
		 
		 <!-- need to replace this one with appropriate code -->
		 <!-- for center aligning the menu, if no. of menus are more, spaces wont be required -->
		   <li>&nbsp;&nbsp;&nbsp;&nbsp;</li>
    	<li>&nbsp;&nbsp;&nbsp;&nbsp;</li>
     	<li>&nbsp;&nbsp;&nbsp;&nbsp;</li>
     	<li>&nbsp;&nbsp;&nbsp;&nbsp;</li>
     	<li>&nbsp;&nbsp;&nbsp;&nbsp;</li>
     	<li>&nbsp;&nbsp;&nbsp;&nbsp;</li>
     	<li>&nbsp;&nbsp;&nbsp;&nbsp;</li>
     	<li>&nbsp;&nbsp;&nbsp;&nbsp;</li>
     	<li>&nbsp;&nbsp;&nbsp;&nbsp;</li>
     	<li>&nbsp;&nbsp;&nbsp;&nbsp;</li>
     	<li>&nbsp;&nbsp;</li>
     	<li>&nbsp;&nbsp;</li>
     	<li>&nbsp;&nbsp;</li>
     	<li>&nbsp;&nbsp;</li>
     	<li>&nbsp;&nbsp;</li>
     	<li>&nbsp;&nbsp;</li>
     	<li>&nbsp;&nbsp;</li>
    
  </ul>
  </nav>
   <div class="ddcolortabsline">&nbsp;</div>
  </td>
  </tr>
  </table>

</div>
</body>
</html>