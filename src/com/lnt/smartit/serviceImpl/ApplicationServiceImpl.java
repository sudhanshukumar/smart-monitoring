package com.lnt.smartit.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lnt.smartit.mysql.dao.ApplicationDao;
import com.lnt.smartit.mysql.model.ApplicationMaster;
import com.lnt.smartit.mysql.model.DeviceApplicationMapping;
import com.lnt.smartit.services.ApplicationService;

@Service
public class ApplicationServiceImpl implements ApplicationService{
	@Autowired
	private ApplicationDao dao;
	
	public List<ApplicationMaster> getAllApplicationMaster() {
		return dao.getAllApplicationMaster();
	}

	public String saveNewApplication(ApplicationMaster applicationMaster) {
		return dao.saveNewApplication(applicationMaster);
	}

	public String deleteApplication(ApplicationMaster applicationMaster) {
		return dao.deleteApplication(applicationMaster);
	}

	public ApplicationMaster getApplicationMasterById(ApplicationMaster applicationMaster) {
		return dao.getApplicationMasterById(applicationMaster);
	}

	public List<DeviceApplicationMapping> getDeviceApplicationMappingById(String applicationId) {
		return dao.getDeviceApplicationMappingById(applicationId);
	}
}
