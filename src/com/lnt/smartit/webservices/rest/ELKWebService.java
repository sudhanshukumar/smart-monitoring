package com.lnt.smartit.webservices.rest;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ELKWebService {
	
	@RequestMapping(value = "/updateIncidentStatus" , method = RequestMethod.GET)
	@ResponseBody
	public void updateIncidentStatusInELK(String incidentNumber, String sysId, String incidentStatus, String incidentPriority){
		System.out.println("incidentNumber : "+incidentNumber);
		System.out.println("sysId : "+sysId);
		System.out.println("incidentStatus : "+incidentStatus);
		System.out.println("incidentPriority : "+incidentPriority);
				
		TransportClient client = null;
		String elasticSearchLoc=null;
		InputStream paramPropertiesStream= null;
		
		QueryBuilder queryBuilder = QueryBuilders.boolQuery()
		        .must(QueryBuilders.termQuery("incidentNumber.keyword", incidentNumber))
		        .must(QueryBuilders.termQuery("incidentSysId.keyword", sysId));
		        
		
		Properties props = new Properties();
		paramPropertiesStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("params.properties");
		try {
			props.load(paramPropertiesStream);
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		elasticSearchLoc = props.getProperty("machine.name");
		
		try {
			client = new PreBuiltTransportClient(Settings.EMPTY).addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(elasticSearchLoc),9300));
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}	
		
		 SearchResponse response = null; 	        
	            response = client.prepareSearch() 
	                    .setIndices("alerts") 
	                    .setTypes("alerts")
	                    .setSize(100)
	                    .setQuery(queryBuilder).execute().actionGet(); 
	       
	        Set<String> result = new HashSet<String>(); 
	        if(response != null){
	        	System.out.println(response.getHits());
	        	   JSONObject json = null;
	        	   JSONObject innerJson1 = null;
	        	   JSONArray innerJson2 = null;
	        	   JSONObject finalJson = null;
	        	   UpdateRequest updateRequest = null;
	       		
				try {
					json = new JSONObject(response.toString());								
					innerJson1 = new JSONObject(json.get("hits").toString());				
					innerJson2 = new JSONArray(innerJson1.get("hits").toString());	
					
					for(int i = 0; i<innerJson2.length(); i++){
						finalJson = (JSONObject) innerJson2.get(i);
						
						System.out.println(finalJson.get("_index"));				
						System.out.println(finalJson.get("_type"));				
						System.out.println(finalJson.get("_id"));
						
						updateRequest = new UpdateRequest(finalJson.get("_index").toString(), finalJson.get("_type").toString(), finalJson.get("_id").toString())
						.doc(jsonBuilder()
						.startObject()
						.field("incidentStatus", incidentStatus)
						.field("incidentPriority", incidentPriority)					
						.endObject());
						client.update(updateRequest).get();
					}
					
				} catch (org.codehaus.jettison.json.JSONException |InterruptedException | ExecutionException |IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        }		
	
		
		
		
	}

}
