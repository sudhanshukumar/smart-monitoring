package com.lnt.smartit.mysql.model;

import java.io.Serializable;


@SuppressWarnings("serial")
public class ThresholdDetails implements Serializable{
	private int id;
	private String keyname;
	private String keyvalue;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getKeyname() {
		return keyname;
	}
	public void setKeyname(String keyname) {
		this.keyname = keyname;
	}
	public String getKeyvalue() {
		return keyvalue;
	}
	public void setKeyvalue(String keyvalue) {
		this.keyvalue = keyvalue;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((keyname == null) ? 0 : keyname.hashCode());
		result = prime * result
				+ ((keyvalue == null) ? 0 : keyvalue.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ThresholdDetails other = (ThresholdDetails) obj;
		if (id != other.id)
			return false;
		if (keyname == null) {
			if (other.keyname != null)
				return false;
		} else if (!keyname.equals(other.keyname))
			return false;
		if (keyvalue == null) {
			if (other.keyvalue != null)
				return false;
		} else if (!keyvalue.equals(other.keyvalue))
			return false;
		return true;
	}
}
