var gridCols=[];
var modelFields=[];
var gridView;

$(document).ready(function() {
	getapplications();
	$("#result_grid").hide();
	$("#grid_table").hide();
	$("#month").hide();
	$("#aggregator").hide();
	$("#agrId").hide();
	$("#field_dropdown").hide();
	$("#save").prop("disabled", true);
	$("#chart_name").prop("disabled", true);
	$( "#alertDialog" ).hide();
});

function showAlert(message){
	
	$( "#alertDialog" ).show();
	$("#alertDialog").text(message);
	$( "#alertDialog" ).dialog({
		buttons:{
				"OK":function() {
					$( this ).dialog( "close" );
                                 }
				} 
							});

}

function enable_save(){
	if($("#main_container").children().length>1 || $("#chart_container").children().length>0 ){
		$("#save").prop("disabled", false);
		$("#chart_name").prop("disabled",false);
	}
	
}

function getapplications()
{
	$.ajax({
		type : "GET",
		url : "getApplications.html",
		success : function(response) {
			$('#application').empty();
//			 $("#application").append('<option value="" selected disabled>Select Application</option>');
			for ( var i = 0; i < response.length; i++) {
				 $("#application").append('<option value="'+response[i].appId+'">'+response[i].appName+'</option>');
			}
			var appsel=$('#application :selected').val();
			
			getCollectionsFromDB(appsel);
			$('#application').change(
					function() {
			var appsel=$('#application :selected').val();
			
			getCollectionsFromDB(appsel);
					});
		}
	});
}



function getCollectionsFromDB(appsel){
	$.ajax({
		type : "GET",
		url : "getCollectionsFromDB.html",
		data:"appname="+appsel,
		success : function(response) {
			$("#collections").empty();
			 $("#collections").append('<option class="placeHolder" value=""> --- Select --- </option>');
			for ( var i = 0; i < response.length; i++) {
				 $("#collections").append('<option value="'+response[i]+'">'+response[i]+'</option>');
			}
			//var col_name=$('#collections :selected').val();
			//console.log("col_name "+col_name);
			//getschema(col_name);
			
			$('#collections').focus(function() {
			    $(this).find(".placeHolder").hide();
			}).blur(function() {
			    $(this).find(".placeHolder").show();
			});
			
			$('#collections').change(
					function() {
						//showAlert("Collection Was changed.......!!!")
			var col_name=$('#collections :selected').val();
			console.log("col_name "+col_name);
			refresh();
			getschema(col_name);
			// Every time when collection name is changed, Application will fetch data from backend for the selected collection name.
			getChartData(col_name);
			});
			
		}
	});
	
}
function refresh(){
	
	$('#chart_container').empty();
	//$('#chartGrid').jqGrid('clearGridData');
	$('#chartGrid').jqGrid('GridDestroy');
	//$('#chartGrid').remove();
}

function pusharray(fieldvalue){
	gridCols.push(fieldvalue);
	var modelFieldsArr={
  		  name : fieldvalue,
  		  index : fieldvalue,	
  		  width : 150
  		
     };
	modelFields.push(modelFieldsArr);
}

function modifiedpusharray(fieldvalue, isHidden){
	gridCols.push(fieldvalue);
	var modelFieldsArr={
  		  name : fieldvalue,
  		  index : fieldvalue,	
  		  width : 150,
  		  hidden: isHidden
     };
	modelFields.push(modelFieldsArr);
}
function getschema(col_name) {
	gridCols=[];
	modelFields=[];
	$.ajax({
		type : "POST",
		url : "getManageReportFields.html",
		data : "collection=" + col_name+"&flag=null",
		success : function(response) {
			var chart_type=$('#chart').val();
			if(chart_type=="grid"){
				$("#save").prop("disabled", true);
				$("#chart_name").prop("disabled", true);
				$("#field_dropdown").hide();
				$("#resId").find("tr:gt(0)").remove();
				for ( var i = 0; i < response.length; i++) {
				
						$("#resId").find('tbody').append(
								$('<tr>').append(
										$('<td>').append(
												'<input class="check" name="select_fields" type="checkbox" value='
														+ response[i].name + '>')
												.append($('</td>'))).append(
										$('<td>').text(response[i].name).append(
												$('</td>'))));
						pusharray(response[i].name);
				}
				$("#result_grid").show();
				$( "<table id='chartGrid'></table><div id='chartGridPager'></div>" ).insertAfter( "#chart_container");
				
				$(".check").change(function() {
					
					
				});
			}
		else if(chart_type=='pie'){
			$("#save").prop("disabled", true);
			$("#chart_name").prop("disabled", true);
			$("#result_grid").hide();
			$("#field_dropdown").show();
			$("#resId").find("tr:gt(0)").remove();
			for ( var i = 0; i < response.length; i++) {
				 $("#field_value").append('<option value="'+response[i].name+'">'+response[i].name+'</option>');
			}
		}
		else if(chart_type=='time'){
			$("#save").prop("disabled", true);
			$("#chart_name").prop("disabled", true);
			$("#result_grid").hide();
			$("#field_dropdown").show();
			for ( var i = 0; i < response.length; i++) {
				 $("#field_value").append('<option value="'+response[i].name+'">'+response[i].name+'</option>');
			}
		}
		else if(chart_type=='column'){
			$("#save").prop("disabled", true);
			$("#chart_name").prop("disabled", true);
			$("#field_dropdown").hide();
			$("#resId").find("tr:gt(0)").remove();
			for ( var i = 0; i < response.length; i++) {
			
					$("#resId").find('tbody').append(
							$('<tr>').append(
									$('<td>').append(
											'<input class="check" name="select_fields" type="checkbox" value='
													+ response[i].name + '>')
											.append($('</td>'))).append(
									$('<td>').text(response[i].name).append(
											$('</td>'))));
			}
			$("#result_grid").show();
			
		}else if(chart_type=='x-y'){
			$("#save").prop("disabled", true);
			$("#chart_name").prop("disabled", true);
			$("#result_grid").hide();
			$("#field_dropdown").show();
			for ( var i = 0; i < response.length; i++) {
				 $("#field_value").append('<option value="'+response[i].name+'">'+response[i].name+'</option>');
			}
		}
		
		}
	});
}

function getChartData(collectionName) {
	//showAlert("getting data for the collection name: "+collectionName);
	var query='*:*';
	var queryParams = {
             			'collection' : collectionName,
             			'query' : query
					  	};
	$.ajax({
		type 		: "POST",
		datatype 	: "json",
	    url			:'getBasicQueryforReportsNoFilter.html',
	    data		: JSON.stringify(queryParams),
		success : function(response) {
			
			for ( var i = 0; i < response.length; i++) {
				var temp_date = response[i].Date;
				temp_date = $.format.date(temp_date, "dd/MM/yyyy HH:mm:ss.SSS");
				response[i].Date = temp_date;
			}
			gridView = $('#chart_container');
			jQuery.data(gridView, "gridDataArray", response );
			drawGidChart(response);
			enable_save();
		}
	});	
}

function updateColModel(){
	gridCols=[];
	 modelFields=[];
	
	$.each(
			$("input[name='select_fields']:checked"),
			function() {
				var fieldvalue=$(this)
				.val();
				//pusharray(fieldvalue);
				modifiedpusharray(fieldvalue,false);
			});
	
	$.each(
			$("input[name='select_fields']:not(:checked)"),
			function() {
				var fieldvalue=$(this)
				.val();
				modifiedpusharray(fieldvalue,true);
			});
}
function extractGridData(responseDataArray){
	//showAlert('data extraction started........');
	//showAlert('Response Data: '+responseDataArray)
	var i=0;
	var fieldArray = modelFields;
	var desirableArr = [];
	for(i in responseDataArray){
		console.log(i);
		console.log(responseDataArray[i]);
		var rowDes = {};
		var row = responseDataArray[i];
		for(j in fieldArray){
			console.log(j);
			console.log(row);
			var temp = fieldArray[j]['name'];
			console.log(temp);
			console.log(row[temp]);
			rowDes[temp] = row[temp];
		}
	desirableArr.push(rowDes);
	}
	console.log(desirableArr);
	return desirableArr;
}
function gettime(col,query,field_value) {
	
	$("#grid_table").hide();
	$("#chart_container").show();
	$.ajax({
		type : "POST",
		url : "getCustomTimeSeries.html",
		data : "collection=" + col+"&query="+query+"&field_value="+field_value,
		success : function(response) {
			var timedata = new Array();
			var dataX=[];
			var dataY=[];
			dataX.push("Time");
			dataY.push("Count");
			for ( var i = 1; i < response.length; i++) {
				timedata[i] = [ response[i].lable, response[i].count ];
				dataX[i]=[response[i].lable];
				dataY[i]=[response[i].count];
			}
			
			var chart = c3.generate({
				bindto: '#chart_container' ,
				data: {
				 xs:{
					
					'Time':'Count'
				},
				
			      columns: [dataX,dataY]
											
			    },
			    subchart: {
				      show: true
				    }, 
				    zoom: {
				      enabled: true
				    }
			});
			
			enable_save();
		}
	});
}

function getpie(col,query,field,limit,filter) {
	$("#grid_table").hide();
	$("#chart_container").show();

	$.ajax({
				type : "POST",
				url : "getCustomChartsNofilter.html",
				data : "collection=" + col + "&chart=pie&field=" + field+"&limit="+limit+"&query="+query+"&filter="+filter,
				success : function(response) {
					var piedata = new Array();
					var Value=[];
					var Count=[];
					var facetVal=[];
					for ( var i = 0; i < response.length; i++) {
						Count[response[i].facetValue]=response[i].count;
						facetVal.push(response[i].facetValue);
						piedata[i] = [ response[i].lable, response[i].count ];
						Value[i]=response[i].facetValue;
					}
				
				
			
					chart = c3.generate({
						bindto: '#chart_container' ,
						data: {
		                    json: [ Count ],
		                    keys: {
		                        value: facetVal
		                    },
		                    type:'pie'
		                },
	                    legend: {
	                        show: false
	                    },
	                    
	                    pie: {
	                      label: {
	                        format: function(value, ratio, id) {
	                          return id;
	                        }
	                      }
	                    },
	                       tooltip: {
	                        format: {
	                          },
	                          contents: function (d, defaultTitleFormat, defaultValueFormat, color) {
	                              var $$ = this, config = $$.config,
	                                  titleFormat = config.tooltip_format_title || defaultTitleFormat,
	                                  nameFormat = config.tooltip_format_name || function (name) { return name; },
	                                  valueFormat = config.tooltip_format_value || defaultValueFormat,
	                                  text, i, title, value, name, bgcolor;
	                              for (i = 0; i < d.length; i++) {
	                                  if (! (d[i] && (d[i].value || d[i].value === 0))) { continue; }

	                                  if (! text) {
	                                      title = titleFormat ? titleFormat(d[i].x) : d[i].x;
	                                      text = "<table class='" + $$.CLASS.tooltip + "'>" + (title || title === 0 ? "<tr><th colspan='2'>" + title + "</th></tr>" : "");
	                                  }

	                                  name = nameFormat(d[i].name);
	                                  value = valueFormat(d[i].value, d[i].ratio, d[i].id, d[i].index);
	                                  bgcolor = $$.levelColor ? $$.levelColor(d[i].value) : color(d[i].id);

	                                  text += "<tr class='" + $$.CLASS.tooltipName + "-" + d[i].id + "'>";
	                                  text += "<td class='name'><span style='background-color:" + bgcolor + "'></span>Facet Value=" + name + "</td>";
	                                  text += "<td class='value'>" + value + "</td>";
	                                  text += "</tr>";
	                              }
	                              return text + "</table>";
	                          }
	                    }
		            });
					enable_save();
				}
			});
}

function getcolumn(col,query,field,limit,filter) {
	$("#grid_table").hide();
	$("#chart_container").show();
	$.ajax({
		type : "POST",
		url : "getCustomChartsNofilter.html",
		data : "collection=" + col + "&chart=column&field=" + field+"&limit="+limit+"&query="+query+"&filter="+filter,
		success : function(response) {
		    var columnData=[];
			var y=[];
			var x=[];
			for(var i=0;i<field.length;i++){
				y.push([field[i]]);
				x.push(['x'+field[i]]);
				for(var j=0;j<response.length;j++){
					if(field[i]==response[j].lable){
						y[i].push(response[j].count);
		                var temp=Math.round(response[j].facetValue);
						x[i].push(temp);
					}
			}
				
			}
			var xs = {};
			for(var i=0;i<x.length;i++){
				columnData.push(x[i]);
				columnData.push(y[i]);
				xs[y[i][0]] = x[i][0];
			}
			
			var chart = c3.generate({
				bindto: '#chart_container' ,
				data: {
				xs:xs,
				
			      columns:columnData
			    },
			    axis: {
				      x: {
				        tick: {count: response.length,
				          fit: true,
				          format: function(x) {
				             return Math.round(x);
				          }
				        }
				      }
				    },
				    subchart: {
				      show: true
				    }, 
				    zoom: {
				      enabled: true
				    }
			}); 
			
			//Bar chart in c3
			
			/*
			var xcolumndata = new Array();
			var ycolumndata = new Array();
			var TwoDArray=new Array();
			xcolumndata.push('Status');
			ycolumndata.push('Values');
			var label = new Array();	
			for ( var i = 0; i < response.length; i++) {
				xcolumndata.push(response[i].facetValue);
				ycolumndata.push(response[i].count);
				label.push(response[i].lable);
				TwoDArray.push([response[i].facetValue,response[i].count]);
			}
			
			
			
			var chart = c3.generate({
			    bindto: '#chart_container',
			    data: {
			    	 x: 'Status',
			      columns: [
			        
                  xcolumndata,
                  ycolumndata
			      ],
			      type: 'bar'
			      
			    },
			    bar: {
			      width: { ratio: 0.5 }
			    },
			    axis: {
			      x: {
			        tick: {count: response.length,
			          fit: true
			        }
			      }
			    },
			    subchart: {
			      show: true
			    }, 
			    zoom: {
			      enabled: true
			    },
			    tooltip: {
			    	format: {
			           
			        },
			        contents: function (d, defaultTitleFormat, defaultValueFormat, color) {
			            var $$ = this, config = $$.config,
			                titleFormat = config.tooltip_format_title || defaultTitleFormat,
			                nameFormat = config.tooltip_format_name || function (name) { return name; },
			                valueFormat = config.tooltip_format_value || defaultValueFormat,
			                text, i, title, value, name, bgcolor;
			                
			            for (i = 0; i < d.length; i++) {
			                if (! (d[i] && (d[i].value || d[i].value === 0))) { continue; }

			                if (! text) {
			                    title = titleFormat ? titleFormat(d[i].x) : d[i].x;
			                    text = "<table class='" + $$.CLASS.tooltip + "'>" + (title || title === 0 ? "<tr><th colspan='2'>" + title + "</th></tr>" : "");
			                }
			                var index;
			                name = nameFormat(d[i].name);
			                value = valueFormat(d[i].value, d[i].ratio, d[i].id, d[i].index);
			                bgcolor = $$.levelColor ? $$.levelColor(d[i].value) : color(d[i].id);
                            for(var k = 0; k < response.length; k++){
			                	
			                    if(TwoDArray[k][0].toString()==title.toString() && TwoDArray[k][1].toString()==value.toString()){
			                     index=k;
			                        break;
			                    }
			                }
			                text += "<tr class='" + $$.CLASS.tooltipName + "-" + d[i].id + "'>";
			                text += "<td class='name'><span style='background-color:" + bgcolor + "'></span>" + name + "</td>";
			                text += "<td class='value'>" + value + "</td>";
			                
			                text += "</tr>";
			                text += "<tr class='" + 'Type' + "-" + d[i].id + "'>";
			                text += "<td class='name'><span style='background-color:" + bgcolor + "'></span>" + 'Type' + "</td>";
			                text += "<td class='Type'>" + label[index] + "</td>";
			                
			                text += "</tr>";
			            }
			            return text + "</table>";
			        }
			    }
			});*/
			enable_save();
		}
	});
}


function drawGidChart(updatedData){
$("#chartGrid").jqGrid(                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        {
		datatype : "local",
		data : updatedData,
	    colNames:gridCols,
	    colModel : modelFields,
	    viewrecords : true,
	    gridview : true,
	    width: 950,
	    height: '100%',
	    forceFit:true,
		shrinkToFit:false,
		caption:"GRID REPORTS",
		loadonce: false,
		jsonReader: {
						repeatitems: false,
						root: function (obj) { 
												console.log(obj);
												return obj; 
											  },
		                page: function (obj) { return 1; },
		                total: function (obj) { return 1; },
		                records: function (obj) { return obj.length; }                  
					},
				pager : '#chartGridPager',
				rowNum : 15,
				rowList : [ 5, 10, 15 ],
				viewrecords : true,
				gridview : true,
				loadonce: true,
	}); 

}

function chartgrid(col){
	 $('#chartGrid').jqGrid('GridUnload');
	var query="*:*";
	var queryParams = {
             			"collection" : col,
             			"query" : query
           
					  };

	
$("#chartGrid").jqGrid(                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        {
		datatype : "json",
	    url:'getBasicQueryforReportsNoFilter.html',
	    ajaxGridOptions : {
   		   					contentType : 'application/json'
	    				  },
	    postData:JSON.stringify(queryParams),
	    mtype: 'POST',
	    colNames:gridCols,
	    colModel : modelFields,
	    viewrecords : true,
	    gridview : true,
	    width: '100%',
	    height: '100%',
	    forceFit:true,
		shrinkToFit:false,
		caption:"GRID REPORTS",
		loadonce: false,
		
		jsonReader: {
						repeatitems: false,
						root: function (obj) { 
												return obj; 
											  },
		                page: function (obj) { return 1; },
		                total: function (obj) { return 1; },
		                records: function (obj) { return obj.length; }                  
					}
				   
				
		/*rowNum:10,
	    rowList:[10,20,30],
	    pager: '#pager',
	    sortname: 'id',
	    viewrecords: true,
	    altRows:true,
	    sortorder: "desc",
	    caption:"Weeks Grid",
	    height: "100%"*/
	}); 

}
function getresults(col) {
	chartgrid(col);
	

}

function savechart() {
	
	var selected_chart = $('#chart').find(":selected").val();
	var appsel=$('#application :selected').val();
	var col = $('#collections :selected').val();
	// var selected_field = $('input[name=select_fields]:checked').val();
//	var limit=$("#limit").val();
//	var query=$("#query").val();
	var chart_name=$("#chart_name").val();
	//var aggregator=$("agrId").val();
	var field_value=$('#field_value :selected').val();
	var selected_field=[];
	$.each(
			$("input[name='select_fields']:checked"),
			function() {
				 selected_field.push($(this)
							.val());
				 

			});
	/*if(aggregator==undefined){
		aggregator="Not Applicable";
	}*/

	$.ajax({
		type : "POST",
		url : "save_chart.html",
		data :"chart_name="+encodeURIComponent(chart_name)+"&chart_type="+encodeURIComponent(selected_chart)+"&collection="+encodeURIComponent(col)+"&field="+encodeURIComponent(selected_field)+"&appsel="+encodeURIComponent(appsel)+"&field_value="+encodeURIComponent(field_value),
		success : function(response) {
			if(response.isSaved == true){
				
				showAlert("Report saved");
			}
			else{
				showAlert(response.saveFailureReason);
				
			}
		}
	});
}

function aggFunc()
{
	var selected_chart = $('#chart').find(":selected").val();
	if (selected_chart == "time") {
		$("#aggregator").show();
		$("#agrId").show();
	}

	if (selected_chart == "grid") {
		$("#aggregator").hide();
		$("#agrId").hide();
	}

	if (selected_chart == "pie") {
		$("#aggregator").show();
		$("#agrId").show();
	}

	if (selected_chart == "column") {
		$("#aggregator").show();
		$("#agrId").show();
	}
	
	if(selected_chart=="response_time")
	{
		$("#aggregator").show();
		$("#agrId").show();
	}
}
function getcount(col,field_value,query)
{
	$("#grid_table").hide();
	$("#chart_container").show();
	$.ajax({
		type : "POST",
		url : "getCount.html",
		data : "collection=" + col+"&field_value="+field_value+"&query="+query,
		success : function(response) {

			var xcolumndata = new Array();
			var ycolumndata = new Array();
			xcolumndata[0]='x';
			ycolumndata[0]='y';
			for ( var i = 0; i < response.length; i++) {
				xcolumndata.push(response[i].facetValue);
				ycolumndata.push(response[i].count);
			}
			
			var chart = c3.generate({
				bindto: '#chart_container' ,
				
			    data: {
			    	
			    	 x: 'x',
			        columns: [
                    xcolumndata,
                    ycolumndata
			        ],
			        type: 'bar'
			    },
			    legend: {
		            show: false
		        }, 
				    subchart: {
				        show: true
				    },
				    zoom: {
				        enabled: true
				    },
				    axis : {
				        x : {
				        	label:field_value
				            
				        },
				        y : {
				        	label:'Count Value'
				        }
				    }
			});
			
		}
	});
}

function page_click(id)
{

var page = id.split("_");
var page_number=page[1];
//var col = $("#col_title").html();
var query=$("#query").val();
var selected_month = $('#month_selector').find(":selected").val();
getpagingresponsetime(col,selected_month,query,page_number);
}

function getpagingresponsetime(col,selected_month,query,page_number)
{
	$.ajax({
		type : "POST",
		url : "getPagingResponseTimeSeries.html",
		data : "collection=" + col+"&month="+selected_month+"&query="+query+"&page="+page_number,
		success : function(response) {
			var timedata = new Array();
			for ( var i = 0; i < response.length; i++) {
				timedata[i] = [ response[i].date, response[i].size ];
			}

			$('#chart_container').highcharts('StockChart', {
				chart : {
				},
				title : {
					text : 'Time Series'
				},

				yAxis : {
					min : 0,

					title : {
						text : 'Count'
					},
					plotLines : [ {
						value : 0,
						width : 1,
						color : '#808080'
					} ]
				},

				legend : {
					layout : 'vertical',
					align : 'right',
					verticalAlign : 'middle',
					borderWidth : 0
				},
				series : [ {
					type : 'line',
					data : timedata
				} ]
			});
		}
	});
}


function getnumeric(col,field_value,query,start_range,end_range,range_gap)
{
	$.ajax({
		type : "POST",
		url : "getNumeric.html",
		data : "collection=" + col+"&field_value="+field_value+"&query="+query+"&start_range="+start_range+"&end_range="+end_range+"&range_gap="+range_gap,
		success : function(response) {
			var timedata = new Array();
			var dataX=[];
			for ( var i = 0; i < response.length; i++) {
				timedata[i] = [ response[i].lable, response[i].count ];
				dataX[i]=[response[i].lable];
				
			}
			$('#chart_container').highcharts({title: {
	            text: 'Numeric Series',
	            x: -20 //center
	        },
	       subtitle: {
	            text: response[0].col_name,
	            x: -20
	        },
	        xAxis: {
	            categories: dataX
	        },
	        yAxis: {
	            title: {
	                text: 'Count'
	            },
	            plotLines: [{
	                value: 0,
	                width: 1,
	                color: '#808080'
	            }]
	        },
	        /*tooltip: {
	            valueSuffix: 'Â°C'
	        },*/
	        legend: {
	            layout: 'vertical',
	            align: 'right',
	            verticalAlign: 'middle',
	            borderWidth: 0
	        },
	        series: [{
	        	type : 'line',
	            data:timedata
	        }]});
		}
	});
}

$(function() {
	$('#from_datepicker').datetimepicker({
		format : 'Y-m-d H:i:s',
		dayOfWeekStart : 1,
		lang : 'en'
	});
	
	var d = new Date();
	d.setHours(d.getHours()-12);
	var temp_from_date=d.getDate();
	var temp_from_month=d.getMonth()+1;
	if(temp_from_date<10){
		temp_from_date="0"+temp_from_date;
	}
	
	if(temp_from_month<10){
		temp_from_month="0"+temp_from_month;
	}
	
	var temp_to_date=d.getFullYear()+"-"+temp_from_month+"-"+temp_from_date+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
	$('#from_datepicker').datetimepicker({
		value : temp_to_date
	});

	$('#to_datepicker').datetimepicker({
		format : 'Y-m-d H:i:s',
		dayOfWeekStart : 1,
		lang : 'en'
	});
	
	var dt = new Date();
	var temp_date=dt.getDate();
	var temp_month=dt.getMonth()+1;
	
	if(temp_date<10){
		temp_date="0"+temp_date;
	}
	
	if(temp_month<10){
	temp_month="0"+temp_month;
	}
	
	var final_to_date=dt.getFullYear()+"-"+temp_month+"-"+temp_date+" "+dt.getHours()+":"+dt.getMinutes()+":"+dt.getSeconds();

	$('#to_datepicker').datetimepicker({
		value : final_to_date
	});
	
	$('#chart').change(
			function() {
				var col=$('#collections :selected').val();
				getschema(col);
				
			});
	$('#month_selector').change(
			function() {
				$("#pages").remove();
				$(".paging").remove();
			});

	$("#execute").click(function() {
		if($('#collections :selected').val()===""){
			showAlert('Log Type cannot be empty');
		}
		else{
		var checked_fields=[];
		var selected_chart = $('#chart').find(":selected").val();
		var col=$('#collections :selected').val();
		var field_value=[];
		field_value=$('#field_value :selected').val();
		var query="*:*";
		var limit="-1";
		var selected_field=[];
		$.each(
				$("input[name='select_fields']:checked"),
				function() {
					selected_field.push($(this)
							.val());
				});
		var date_filter="";
		if (selected_chart == "time") {
			gettime(col,query,field_value);
		}

		if (selected_chart == "grid") {
			refresh();
			if($('#collections :selected').val()!==""){
				$("#result_grid").show();
			}
			
			$.each(
					$("input[name='select_fields']:checked"),
					function() {
						checked_fields.push($(this).val());
					});
			
			if($('#collections :selected').val()!=="" && checked_fields.length>0){
				$( "<table id='chartGrid'></table><div id='chartGridPager'></div>" ).insertAfter( "#chart_container");
				
				updateColModel();
				drawGidChart(jQuery.data(gridView,"gridDataArray"));
			}else{
				showAlert('Field names must be selected!');
			}
		}

		if (selected_chart == "pie") {
			
			getpie(col,query,field_value,limit,date_filter);
		}

		if (selected_chart == "column") {
			
			getcolumn(col,query,selected_field,limit,date_filter);
		}
		
		if(selected_chart=="x-y")
		{
	
			getcount(col,field_value,query);
		}
		
	}
		
	});
	
	$("#save").click(function() {
		
		if($("#chart_name").val()!="" && $("#chart_name").val()!=null){
			savechart();
		}
		else{
			showAlert('Report name cannot be empty');
		}
		$("#chart_name").val("");
	});
	
	
});
