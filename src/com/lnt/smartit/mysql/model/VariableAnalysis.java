package com.lnt.smartit.mysql.model;


public class VariableAnalysis {
	
	private String page;
	private String applicationName;
	private String ServerIPAddress;
	private String date;
	private Double count;
	private long epochTime;
	private Double actualValues;
	private Double predictedValues;
	private String alertMsgType;
	Double responseTime;
	private Double fiveMinCPUAvg;
	private Double tenMinCPUAvg;
	private Double fifteenMinCPUAvg;
	private Double avgRamUsed;
	private Double avgMemUsed;
	private Integer startDate;
	private Integer startMonth;
	private Integer startYear;
	private Integer endDate;
	private Integer endMonth;
	private Integer endYear;
	private Double iowait;
	private Integer day;
	private Integer month;
	private Integer year;
	private Integer hour;
	
	public Integer getDay() {
		return day;
	}
	public void setDay(Integer day) {
		this.day = day;
	}
	public Integer getMonth() {
		return month;
	}
	public void setMonth(Integer month) {
		this.month = month;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Integer getHour() {
		return hour;
	}
	public void setHour(Integer hour) {
		this.hour = hour;
	}
	public Double getIowait() {
		return iowait;
	}
	public void setIowait(Double iowait) {
		this.iowait = iowait;
	}
	public Double getAvgMemUsed() {
		return avgMemUsed;
	}
	public void setAvgMemUsed(Double avgMemUsed) {
		this.avgMemUsed = avgMemUsed;
	}
	public Integer getEndDate() {
		return endDate;
	}
	public void setEndDate(Integer endDate) {
		this.endDate = endDate;
	}
	public Integer getEndMonth() {
		return endMonth;
	}
	public void setEndMonth(Integer endMonth) {
		this.endMonth = endMonth;
	}
	public Integer getEndYear() {
		return endYear;
	}
	public Integer getStartDate() {
		return startDate;
	}
	public void setStartDate(Integer startDate) {
		this.startDate = startDate;
	}
	public Integer getStartMonth() {
		return startMonth;
	}
	public void setStartMonth(Integer startMonth) {
		this.startMonth = startMonth;
	}
	public Integer getStartYear() {
		return startYear;
	}
	public void setStartYear(Integer startYear) {
		this.startYear = startYear;
	}
	public void setEndYear(Integer endYear) {
		this.endYear = endYear;
	}
	public Double getAvgRamUsed() {
		return avgRamUsed;
	}
	public void setAvgRamUsed(Double avgRamUsed) {
		this.avgRamUsed = avgRamUsed;
	}
	public String getServerIPAddress() {
		return ServerIPAddress;
	}
	public void setServerIPAddress(String serverIPAddress) {
		ServerIPAddress = serverIPAddress;
	}
	public Double getActualValues() {
		return actualValues;
	}
	public void setActualValues(Double actualValues) {
		this.actualValues = actualValues;
	}
	public Double getPredictedValues() {
		return predictedValues;
	}
	public void setPredictedValues(Double predictedValues) {
		this.predictedValues = predictedValues;
	}
	public Double getFiveMinCPUAvg() {
		return fiveMinCPUAvg;
	}
	public void setFiveMinCPUAvg(Double fiveMinCPUAvg) {
		this.fiveMinCPUAvg = fiveMinCPUAvg;
	}
	public Double getTenMinCPUAvg() {
		return tenMinCPUAvg;
	}
	public void setTenMinCPUAvg(Double tenMinCPUAvg) {
		this.tenMinCPUAvg = tenMinCPUAvg;
	}
	public Double getFifteenMinCPUAvg() {
		return fifteenMinCPUAvg;
	}
	public void setFifteenMinCPUAvg(Double fifteenMinCPUAvg) {
		this.fifteenMinCPUAvg = fifteenMinCPUAvg;
	}
	public long getEpochTime() {
		return epochTime;
	}
	public void setEpochTime(long epochTime) {
		this.epochTime = epochTime;
	}
	public Double getCount() {
		return count;
	}
	public void setCount(Double count) {
		this.count = count;
	}
	public Double getResponseTime() {
		return responseTime;
	}
	public void setResponseTime(Double responseTime) {
		this.responseTime = responseTime;
	}
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getAlertMsgType() {
		return alertMsgType;
	}
	public void setAlertMsgType(String alertMsgType) {
		this.alertMsgType = alertMsgType;
	}
	@Override
	public String toString() {
		return "VariableAnalysis [page=" + page + ", applicationName="
				+ applicationName + ", ServerIPAddress=" + ServerIPAddress
				+ ", date=" + date + ", count=" + count + ", epochTime="
				+ epochTime + ", actualValues=" + actualValues
				+ ", predictedValues=" + predictedValues + ", alertMsgType="
				+ alertMsgType + ", responseTime=" + responseTime
				+ ", fiveMinCPUAvg=" + fiveMinCPUAvg + ", tenMinCPUAvg="
				+ tenMinCPUAvg + ", fifteenMinCPUAvg=" + fifteenMinCPUAvg
				+ ", avgRamUsed=" + avgRamUsed + ", avgMemUsed=" + avgMemUsed
				+ ", startDate=" + startDate + ", startMonth=" + startMonth
				+ ", startYear=" + startYear + ", endDate=" + endDate
				+ ", endMonth=" + endMonth + ", endYear=" + endYear
				+ ", iowait=" + iowait + ", day=" + day + ", month=" + month
				+ ", year=" + year + ", hour=" + hour + "]";
	}
}
