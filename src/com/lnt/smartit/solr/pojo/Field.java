package com.lnt.smartit.solr.pojo;

public class Field {

	private String name;
	private String type;
	private boolean isindexed;
	private boolean isstored;
	private boolean isselected;
	private boolean	isprimary;
	private boolean issecondary;
	
	
	public boolean isIsprimary() {
		return isprimary;
	}
	public void setIsprimary(boolean isprimary) {
		this.isprimary = isprimary;
	}
	public boolean isIssecondary() {
		return issecondary;
	}
	public void setIssecondary(boolean issecondary) {
		this.issecondary = issecondary;
	}
	public boolean isIsselected() {
		return isselected;
	}
	public void setIsselected(boolean isselected) {
		this.isselected = isselected;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public boolean isIsindexed() {
		return isindexed;
	}
	public void setIsindexed(boolean isindexed) {
		this.isindexed = isindexed;
	}
	public boolean isIsstored() {
		return isstored;
	}
	public void setIsstored(boolean isstored) {
		this.isstored = isstored;
	}

	@Override
	public String toString() {
		return "Field [name=" + name + ", type=" + type + ", isindexed="
				+ isindexed + ", isstored=" + isstored + ", isselected="
				+ isselected + ", isprimary=" + isprimary + ", issecondary="
				+ issecondary + "]";
	}
}
