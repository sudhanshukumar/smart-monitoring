package com.lnt.smartit.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.lnt.smartit.mysql.model.DeviceParams;
import com.lnt.smartit.mysql.model.LogParams;
import com.lnt.smartit.mysql.model.ManageThreshold;
import com.lnt.smartit.mysql.model.ThresholdDetails;
import com.lnt.smartit.mysql.model.ThresholdMaster;
import com.lnt.smartit.rule.mysql.models.ExpressionDataType;
import com.lnt.smartit.rule.mysql.models.OperationType;
import com.lnt.smartit.rule.mysql.models.RuleType;
import com.lnt.smartit.rule.mysql.models.RuleVO;
import com.lnt.smartit.services.DeviceService;
import com.lnt.smartit.services.LogService;
import com.lnt.smartit.services.ParameterService;
import com.lnt.smartit.services.ThresholdService;
import com.lnt.smartit.solr.pojo.JqGridData;

@Controller
public class ThresholdController {
	@Autowired
	private ThresholdService thresholdService;

	@Autowired
	private DeviceService deviceService;
	
	@Autowired
	private LogService logService;

	@Autowired
	private ParameterService parameterService;
	
	@Value("${application.name}")
	private String app_name;
	private final Logger logger = LoggerFactory.getLogger(ThresholdController.class.getName());

	@RequestMapping(value = "manageThresHold.html")
	public ModelAndView manageThreshold() {
		return new ModelAndView("Manage/manageThreshold");
	}

	@RequestMapping(value = "populateThresholdGrid.html", method = RequestMethod.GET)
	public @ResponseBody JqGridData getThresholds(HttpServletRequest request) {
		JqGridData jqGridData = new JqGridData();
		try {
			HttpSession session = request.getSession();
			if(session.getAttribute("appId")==null){
				String appId = logService.getAppIdByName(app_name);
				session.setAttribute("appId",appId);
				session.setAttribute("appname", app_name);
			}
			String appId = session.getAttribute("appId").toString();
			List<RuleVO> thresholds = new ArrayList<RuleVO>();
			thresholds = thresholdService.getThresholds(appId);
			/*for (ManageThreshold thresholdMaster : thresholds) {
				System.out.println("controller=host name="+thresholdMaster.getHostName());
				System.out.println("controller=host name="+thresholdMaster.getParamName());

				if(thresholdMaster.getThresholdEnabled()){
					thresholdMaster.setThresholdStatus("Enabled");
				}else{
					thresholdMaster.setThresholdStatus("Disabled");
				}
			}*/
			
			jqGridData.setGridData(thresholds.toArray());
			jqGridData.setRows(thresholds.size());
			jqGridData.setRecords(thresholds.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jqGridData;
	}

	@RequestMapping(value = "threshold.html")
	public ModelAndView thresholdActions(HttpServletRequest request,String thresholdId,ModelMap map) {
		System.out.println("in edit controller="+thresholdId);
		HttpSession session = request.getSession();
		if(session.getAttribute("appId")==null){
			String contextPath = request.getContextPath();
			String[] temp = contextPath.split("_");
			String appId = logService.getAppIdByName(temp[temp.length-1]);
			String appName=temp[temp.length-1];
			session.setAttribute("appId",appId);
			session.setAttribute("appname", appName);
		}
		String appId = session.getAttribute("appId").toString();
		List<RuleVO> thresholds = new ArrayList<RuleVO>();
		thresholds = thresholdService.getThresholds(appId);
		for(int i=0;i<thresholds.size();i++){
			//if(thresholds.get(i).getId().equals(thresholdId)){
			
				map.addAttribute("thresholds", thresholds.get(i));
			//}
		}
		List<LogParams> logParam=new ArrayList<LogParams>();
		List<DeviceParams> devParam=new ArrayList<DeviceParams>();
		logParam=thresholdService.getLogParams();
		devParam=thresholdService.getAppDeviceParams();
		map.addAttribute("threshold_hostNames", devParam);
		map.addAttribute("threshold_ParamNames", logParam);
		return new ModelAndView("Manage/new/newThreshold", map);
	}

	public StringBuffer thresholdValidation(BindingResult bindingResult,ThresholdMaster thresholdMaster, StringBuffer stringBuffer) {
		if (thresholdMaster.getThresholdName().isEmpty() || thresholdMaster.getThresholdName() == null) {
			stringBuffer.append("\nThreshold Name can not be Empty!");
		}
		return stringBuffer;
	}

	@RequestMapping(value = "deleteThreshold.html")
	public @ResponseBody String deleteThreshold(@RequestParam(value = "thresholdId") String threshId) {
		thresholdService.deleteThreshold(threshId);
		return "success";
	}

	@RequestMapping(value = "editThreshold.html", method = RequestMethod.POST)
	public  String editThreshold(String thresholdMaster, HttpServletRequest request) throws IOException, JsonMappingException, IOException {
		System.out.println("edit controller="+thresholdMaster);
		ObjectMapper mapper=new ObjectMapper();
		RuleVO manageThreshold=mapper.readValue(thresholdMaster, RuleVO.class);
		
		System.out.println("manage="+manageThreshold.getId());
		thresholdService.editThresholds(manageThreshold);
		return "redirect:manageThresHold.html";
	}

	@RequestMapping(value = "previewThreshold.html", method = RequestMethod.POST)
	public @ResponseBody ThresholdMaster previewThreshold(ThresholdMaster thresholdMaster, HttpServletRequest request) {
		logger.debug("Entering ThresholdMasterController-->previewThreshold()");
		logger.debug("thresholdMaster " + thresholdMaster);
		ThresholdMaster thresholdMast;
		thresholdMast = thresholdService.getthresholdMasterById(thresholdMaster);
		Map<String, String> notificationMap = thresholdService.getThresholdNotifyMapping(thresholdMaster);
		thresholdMast.setGroupIds(notificationMap);
		logger.debug("thresholdMast"+thresholdMast);
		for (ThresholdDetails thresholdDetails : thresholdMast.getThresholdDetails()) {
			logger.debug("threshold : " + thresholdDetails.getKeyname());
			if (thresholdDetails.getKeyname().equalsIgnoreCase("PAGEURL")) {
				thresholdMast.setPageValue(thresholdDetails.getKeyvalue());
				logger.debug("pageurl : " + thresholdMast.getPageValue());
			} else if (thresholdDetails.getKeyname().equalsIgnoreCase("APPNAME")) {
				thresholdMast.setAppValue(thresholdDetails.getKeyvalue());
				logger.debug("pageurl : " + thresholdMast.getAppValue());
			}
		}
		return thresholdMast;
	}
	
	@RequestMapping(value = "previewThresholdForLog.html", method = RequestMethod.POST)
	public @ResponseBody List<Map<String,String>> previewThresholdForLog(@RequestParam(value = "thresholdId") String thresholdId, HttpServletRequest request) {
		List<Map<String,String>> logData = thresholdService.previewThresholdForLog(thresholdId);
		return logData;
	}
	
	@RequestMapping(value = "previewThresholdForService.html", method = RequestMethod.POST)
	public @ResponseBody List<Map<String,String>> previewThresholdForService(@RequestParam(value = "thresholdId") String thresholdId, HttpServletRequest request) {
		List<Map<String,String>> serviceData = thresholdService.previewThresholdForService(thresholdId);
		logger.debug("serviceData"+serviceData);
		return serviceData;
	}
	
	@RequestMapping(value = "thresholdRamCpu.html", method = RequestMethod.POST)
	public @ResponseBody String thresholdRamCpu(
			@RequestParam(value = "threshold_hostName") String threshold_hostName,
			@RequestParam(value = "threshold_parameter") String param,
			HttpSession session, HttpServletRequest request) {
		String appId = request.getSession().getAttribute("appId").toString();
		String deviceId = deviceService.getDeviceMasterByHostName(threshold_hostName);
		String osType = logService.getOSfromdeviceId(deviceId);
		String logId = logService.getLogId(appId, deviceId, osType);
		
		LogParams logParam = logService.getLogParams(logId, param);
		return logParam.getMaxValue();
	}
	
	@RequestMapping(value = "getParamsForHost.html", method = RequestMethod.POST)
	public @ResponseBody List<LogParams> getParamsByHost(@RequestParam(value = "hostname") String hostname){
		return thresholdService.getLogParamsForHost(hostname);
	}
	
	@RequestMapping(value = "getParameterTypeList.html", method = RequestMethod.GET)
	public @ResponseBody List<ExpressionDataType> getParameterTypeList(){		
		return thresholdService.getParameterTypeList();	
	}
	
	@RequestMapping(value = "getAlertTypeList.html", method = RequestMethod.GET)
	public @ResponseBody List<RuleType> getAlertTypeList(){		
		return thresholdService.getAlertTypeList();	
	}
	@RequestMapping(value = "getOperatorTypeList.html", method = RequestMethod.GET)
	public @ResponseBody List<OperationType> getOperatorTypeList(){		
		return thresholdService.getOperatorTypeList();	
	}
	
}
