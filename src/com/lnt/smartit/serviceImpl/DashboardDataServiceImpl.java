package com.lnt.smartit.serviceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.xmlbeans.impl.xb.xsdschema.RestrictionDocument.Restriction;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.lnt.smartit.constants.Constant;
import com.lnt.smartit.helper.WSHelper;
import com.lnt.smartit.mysql.dao.DashboardDataDao;
import com.lnt.smartit.mysql.daoImpl.DashboardDataDaoImpl;
import com.lnt.smartit.mysql.model.AppDeviceMaster;
import com.lnt.smartit.mysql.model.DashboardOnloadContent;
import com.lnt.smartit.services.DashboardDataService;

@Service
public class DashboardDataServiceImpl implements DashboardDataService{
	private  final Logger logger = LoggerFactory.getLogger(NotificationGroupServiceImpl.class.getName());
	@Autowired
	private DashboardDataDao dao;
	@Autowired
	private SessionFactory sessionFactory;
	
	@Value("${ws.baseurl}")
	String wsUrl; 

	/*
	 * This method will give values in json format as String on dashoboard.jsp
	 * loading. First time values will come from MySQL DB
	 */
	public List<String> getOnLoadContent() {
		logger.debug("SmartIT_UX::com.lnt.services::DashboardDataService.getOnLoadContent():: ...entry");
		List<String> values = new ArrayList<String>();
		@SuppressWarnings("unchecked")
		List<DashboardOnloadContent> alertList = (List<DashboardOnloadContent>) dao.createQuery("from DashboardOnloadContent", null);
		logger.debug("SmartIT_UX::com.lnt.services::DashboardDataService.getOnLoadContent():: on load the values are "
				+ alertList);
		logger.debug("SmartIT_UX::com.lnt.services::DashboardDataService.getOnLoadContent():: on load the values are "
				+ alertList);
		for (DashboardOnloadContent onLoadAlerts : alertList) {
			values.add(onLoadAlerts.getValues());
		}
		return values;
	}

	/*public String getAdditionalInfo(String parameterName,String addressedMachine, String respAppName, String respPageName,String osType) {
		HttpURLConnection urlConnection = null;
		BufferedReader in = null;
		StringBuffer jsonData = new StringBuffer();
		logger.debug("DEBUG::SmartIT_UX::com.lnt.services.DashboardDataService.getAdditionalInfo():: for parameter = "+ parameterName);
		try {
			if(parameterName.equals("DISK") || parameterName.equals("CPU") || parameterName.equals("RAM")){
				if(osType.equalsIgnoreCase("windows")){
					parameterName="WIN"+parameterName;
				}
		}
			String urlParameters  = "parameterName="+parameterName+"&mapName="+addressedMachine;//+"&urlname="+respAppName;
			String newURL =wsUrl +parameterName+"/"+addressedMachine;
			URL url = new URL(newURL);
			logger.debug("DEBUG::SmartIT_UX::com.lnt.services::DashboardDataService.getAdditionalInfo() :: requesting to service :: "+ url);
			if (Constant.isProxyRequired) {
				logger.info("INFO::SmartIT_UX::com.lnt.services::DashboardDataService.getAdditionalInfo:: connection through proxy");
				try {
					urlConnection = (HttpURLConnection) url.openConnection(getHttpProxy());
				} catch (IOException e) {
					logger.error("ERROR::SmartIT_UX::com.lnt.services::DashboardDataService.getAdditionalInfo() === \n",e);
				}
			} else {
				urlConnection = (HttpURLConnection) url.openConnection();
			}
			
			if(!parameterName.equals("URL") && !parameterName.equals("RESPONSETIME")){
				if(osType.equalsIgnoreCase("windows")){
					parameterName="WIN"+parameterName;
				}
			}
				
		//	byte[] postData  = urlParameters.getBytes();
			urlConnection.setDoOutput(true);
			urlConnection.setRequestMethod( "GET" );
			urlConnection.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded"); 
			urlConnection.setRequestProperty( "charset", "utf-8");
			urlConnection.setUseCaches( false );
			try( DataOutputStream wr = new DataOutputStream( urlConnection.getOutputStream())) {
			   wr.write( postData );
			}
			System.out.println(urlConnection.toString());
			in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			jsonData.append(in.readLine());
			
		} catch (Exception e) {
			System.out.println("ERROR::SmartIT_UX::com.lnt.services::DashboardDataService.getAdditionalInfo():: while connecting to Web Service!!!\n"
					+ e + "\n----------------------------\n");
			
		}
		
		System.out.println("jsonData.toString()"+jsonData.toString());
		return jsonData.toString();
	}*/
	
	public String getAdditionalInfo(String parameterName,String addressedMachine, String respAppName, String respPageName,String osType,String timerange,String xAxis,String yAxis) {
		StringBuffer jsonData = new StringBuffer();
		InputStream paramPropertiesStream= null;
		String xAxisValue=null;
		String yAxisValue=null;
		
		logger.debug("DEBUG::SmartIT_UX::com.lnt.services.DashboardDataService.getAdditionalInfo():: for parameter = "+ parameterName);
		try {
			if(parameterName.equals("DISK") || parameterName.equals("CPU") || parameterName.equals("RAM")){
				if(osType.equalsIgnoreCase("windows")){
					parameterName="WIN"+parameterName;
				}
		}
			/*String urlParameters  = "parameterName="+parameterName+"&mapName="+addressedMachine;//+"&urlname="+respAppName;
			String newURL =wsUrl +parameterName+"/"+addressedMachine;
			URL url = new URL(newURL);
			logger.debug("DEBUG::SmartIT_UX::com.lnt.services::DashboardDataService.getAdditionalInfo() :: requesting to service :: "+ url);
			if (Constant.isProxyRequired) {
				logger.info("INFO::SmartIT_UX::com.lnt.services::DashboardDataService.getAdditionalInfo:: connection through proxy");
				try {
					urlConnection = (HttpURLConnection) url.openConnection(getHttpProxy());
				} catch (IOException e) {
					logger.error("ERROR::SmartIT_UX::com.lnt.services::DashboardDataService.getAdditionalInfo() === \n",e);
				}
			} else {
				urlConnection = (HttpURLConnection) url.openConnection();
			}
			
			if(!parameterName.equals("URL") && !parameterName.equals("RESPONSETIME")){
				if(osType.equalsIgnoreCase("windows")){
					parameterName="WIN"+parameterName;
				}
			}
				
		//	byte[] postData  = urlParameters.getBytes();
			urlConnection.setDoOutput(true);
			urlConnection.setRequestMethod( "GET" );
			urlConnection.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded"); 
			urlConnection.setRequestProperty( "charset", "utf-8");
			urlConnection.setUseCaches( false );
			try( DataOutputStream wr = new DataOutputStream( urlConnection.getOutputStream())) {
			   wr.write( postData );
			}
			System.out.println(urlConnection.toString());
			in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));*/
			if(parameterName.equals("nagioslogs_new")){
			Properties props = new Properties();
			paramPropertiesStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("params.properties");
			props.load(paramPropertiesStream);
			yAxisValue = props.getProperty(yAxis).toString();
			xAxisValue = props.getProperty("XAXIS").toString();
			RestTemplate restCallToService=new RestTemplate();
			Map<String, String> params = new HashMap<String, String>();
		    params.put("parameterName",parameterName);
		    params.put("mapName",addressedMachine);
		    params.put("timerange",timerange);
		    params.put("xAxis",xAxisValue);
		    params.put("yAxis", yAxisValue);
		    System.out.println("hello");
			TransportClient client = null;
			String elasticSearchLoc=null;
			try {
				elasticSearchLoc = props.getProperty("machine.name");
				 client = new PreBuiltTransportClient(Settings.EMPTY).addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(elasticSearchLoc),9300));
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
			//For getting time hrs before the current time
			Calendar cal = Calendar.getInstance();
			  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		     String s = sdf.format(new Date());
		     Date date=sdf.parse(s);
			cal.setTime(date);
			cal.add(Calendar.HOUR_OF_DAY, Integer.parseInt(timerange)*-1);;
			String old = sdf.format(cal.getTime());
			System.out.println(old);
			Date oldDate=sdf.parse(old);
			//String old = "2017-04-17T01:58:46.317Z";
			//String s = "2017-04-17T13:58:46.317Z";
			//for getting the size of internal search hits dynamically
/*			SearchRequestBuilder searchRequestBuilder = client.prepareSearch()
		            .setIndices(parameterName)
		            .setQuery(QueryBuilders.rangeQuery(xAxisValue).from(old).to(s));
			
		    SearchResponse response = searchRequestBuilder.get();
		    SearchHits searchHits = response.getHits();
		    Long size=searchHits.getTotalHits();
		    //For getting the data from elasticsearch
		    SearchRequestBuilder searchRequestBuilderNew =  client.prepareSearch()
		            .setIndices(parameterName)
		            //.setTypes("logs")
		           // .add(xAxisValue,yAxisValue)
		           .setSize(size.intValue())
		           .setQuery(QueryBuilders.termQuery("metric", "CURRENT HOST STATE"))
		           .setQuery(QueryBuilders.rangeQuery(xAxisValue).from(old).to(s));
		          // .must(QueryBuilders.rangeQuery(xAxisValue).from(old).to(s)); //AND condition
		           //.must(QueryBuilders.termQuery("IP_Address", addressedMachine));
		           //.setSearchType(SearchType.DFS_QUERY_THEN_FETCH);
			System.out.println(searchRequestBuilderNew);
		   // SearchResponse responseNew = searchRequestBuilderNew.get();
*/			System.out.println("OLD  "+ old);
			System.out.println("NEW  "+ s);
			QueryBuilder qbDate = QueryBuilders.rangeQuery(xAxisValue)   
				    .gte(old)                            
				    .lt(s)                             
				    .includeLower(true)                 
				    .includeUpper(true); 
	
			QueryBuilder qnot = QueryBuilders.termQuery("state_ind","");
			//QueryBuilder qnull = QueryBuilders.termQuery("state_ind","null");
			QueryBuilder qField = QueryBuilders.termQuery("metric.keyword", "CURRENT HOST STATE");
			System.out.println(addressedMachine);
			QueryBuilder qip= QueryBuilders.termQuery("ip_address", addressedMachine);
			QueryBuilder qb =QueryBuilders.boolQuery().must(qbDate).must(qField).must(qip).mustNot(qnot);
			//QueryBuilder qb =QueryBuilders.boolQuery().must(qbDate).must(qfield2).mustNot(qnot).must(qnull);
		    SearchRequestBuilder searchRequestBuilde1r = client.prepareSearch()
		            .setIndices(parameterName)
		            .setQuery(qb)
		            .setQuery(qb);
		           
		    System.out.println(searchRequestBuilde1r);
			 SearchResponse response1 = searchRequestBuilde1r.get();
			 System.out.println(response1);
			 for (SearchHit hit : response1.getHits()) {
		    		
		            //Long id = hit.field("id").<Long>getValue();
		          System.out.println(hit.getSource().get("state_ind"));
		        }
		    System.out.println("Response ==  "+response1);
		    SearchHits searchHitsNew = response1.getHits();
		    ObjectMapper mapper = new ObjectMapper();
		    StringBuffer objJson=new StringBuffer();
		    List<Map<String,Object>> logsList=new ArrayList<Map<String,Object>>();
		    try {
		    	int count=0;
		    	/*for(int i=0;i<searchHitsNew.getTotalHits();i++){
		    		SearchHit hit=searchHitsNew.getAt(i);
		    	Map<String, Object> logMap=hit.sourceAsMap();
					logsList.add(logMap);
					System.out.println(count++);
		    }*/
		    	for (SearchHit hit : response1.getHits()) {
		    		Map<String,Object> axesMap=new HashMap<String, Object>();
		    		System.out.println(hit.getSource().get(yAxisValue));
		    		if(!hit.getSource().get(yAxisValue).equals("null")){
		            //Long id = hit.field("id").<Long>getValue();
		            axesMap.put("xaxis", hit.getSource().get(xAxisValue));
		            axesMap.put("yaxis", hit.getSource().get(yAxisValue));
		           logsList.add(axesMap);
		        }}
		    //to get data only for the past 12 hours
		    objJson.append(mapper.writeValueAsString(logsList));
		    } catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		    //String result=restCallToService.getForObject("http://localhost:7080/ELKDemo/search/{parameterName}/{mapName}/{timerange}/{xAxis}/{yAxis}",String.class,params); 
			//System.out.println("result recieved "+result);
			//jsonData.append(result);
			jsonData.append(objJson.toString());
			}
			else{
				Properties props = new Properties();
				paramPropertiesStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("params.properties");
				props.load(paramPropertiesStream);
				yAxisValue = props.getProperty(yAxis).toString();
				xAxisValue = props.getProperty("XAXIS_WIN").toString();
				RestTemplate restCallToService=new RestTemplate();
				Map<String, String> params = new HashMap<String, String>();
			    params.put("parameterName",parameterName);
			    params.put("mapName",addressedMachine);
			    params.put("timerange",timerange);
			    params.put("xAxis",xAxisValue);
			    params.put("yAxis", yAxisValue);
			    System.out.println("hello");
				TransportClient client = null;
				String elasticSearchLoc=null;
				try {
					elasticSearchLoc = props.getProperty("machine.name");
					 client = new PreBuiltTransportClient(Settings.EMPTY).addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(elasticSearchLoc),9300));
				} catch (UnknownHostException e) {
					e.printStackTrace();
				}
				//For getting time hrs before the current time
				Calendar cal = Calendar.getInstance();
				  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			      String s = sdf.format(new Date());
			      Date date=sdf.parse(s);
				cal.setTime(date);
				cal.add(Calendar.HOUR_OF_DAY, Integer.parseInt(timerange)*-1);;
				String old = sdf.format(cal.getTime());
				System.out.println(old);
				Date oldDate=sdf.parse(old);
				//for getting the size of internal search hits dynamically
				SearchRequestBuilder searchRequestBuilder = client.prepareSearch()
			            .setIndices(parameterName)
			            .setQuery(QueryBuilders.rangeQuery(xAxisValue).from(old).to(s));
			    SearchResponse response = searchRequestBuilder.get();
			    SearchHits searchHits = response.getHits();
			    Long size=searchHits.getTotalHits();
			    //For getting the data from elasticsearch
			    SearchRequestBuilder searchRequestBuilderNew =  client.prepareSearch()
			            .setIndices(parameterName)
			            //.setTypes("logs")
			           // .add(xAxisValue,yAxisValue)
			           .setSize(size.intValue())
			           .setQuery(QueryBuilders.rangeQuery(xAxisValue).from(old).to(s));
			          /* .must(QueryBuilders.rangeQuery(xAxisValue).from(old).to(s)) //AND condition
			           .must(QueryBuilders.termQuery("IP_Address", addressedMachine)))*/;
			           //.setSearchType(SearchType.DFS_QUERY_THEN_FETCH);
				System.out.println(searchRequestBuilderNew);
			    SearchResponse responseNew = searchRequestBuilderNew.get();
			    System.out.println("Response ==  "+responseNew);
			    SearchHits searchHitsNew = responseNew.getHits();
			    ObjectMapper mapper = new ObjectMapper();
			    StringBuffer objJson=new StringBuffer();
			    List<Map<String,Object>> logsList=new ArrayList<Map<String,Object>>();
			    try {
			    	int count=0;
			    	/*for(int i=0;i<searchHitsNew.getTotalHits();i++){
			    		SearchHit hit=searchHitsNew.getAt(i);
			    	Map<String, Object> logMap=hit.sourceAsMap();
						logsList.add(logMap);
						System.out.println(count++);
			    }*/
			    	for (SearchHit hit : responseNew.getHits()) {
			    		Map<String,Object> axesMap=new HashMap<String, Object>();
			            //Long id = hit.field("id").<Long>getValue();
			            axesMap.put("xaxis", hit.getSource().get(xAxisValue));
			            axesMap.put("yaxis", hit.getSource().get(yAxisValue));
			           logsList.add(axesMap);
			        }
			    //to get data only for the past 12 hours
			    objJson.append(mapper.writeValueAsString(logsList));
			    } catch (JsonParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			    //String result=restCallToService.getForObject("http://localhost:7080/ELKDemo/search/{parameterName}/{mapName}/{timerange}/{xAxis}/{yAxis}",String.class,params); 
				//System.out.println("result recieved "+result);
				//jsonData.append(result);
				jsonData.append(objJson.toString());
				
			
			}
		} catch (Exception e) {
			System.out.println("ERROR::SmartIT_UX::com.lnt.services::DashboardDataService.getAdditionalInfo():: while connecting to Web Service!!!\n"
					+ e + "\n----------------------------\n");
			e.printStackTrace();
			
		}
		System.out.println("jsonData.toString()"+jsonData.toString());
		return jsonData.toString();
	}


	public String correlateRespTime(String parameterName,String addressedMachine) {
		URL url = null;
		URLConnection urlConnection = null;
		BufferedReader in = null;
		StringBuffer jsonData = new StringBuffer();
		String wsUrl = WSHelper.constructURLForCorrealtingResponseTime(parameterName, addressedMachine);
		try {
			url = new URL(wsUrl);
			logger.debug("correlateRespTime::::URL >>> " + wsUrl);
			logger.debug("DEBUG::SmartIT_UX::com.lnt.services::DashboardDataService.correlateRespTime():: requesting to service ---------- \n"
					+ url + "\n-------------\n");
			if (Constant.isProxyRequired) {
				logger.info("INFO::SmartIT_UX::com.lnt.services::DashboardDataService.correlateRespTime:: connection through proxy");
				try {
					urlConnection = url.openConnection(getHttpProxy());
				} catch (IOException e) {
					e.printStackTrace();
					logger.error("ERROR::SmartIT_UX::com.lnt.services::DashboardDataService.correlateRespTime() === \n"
							+ e + "\n------------------\n");
				} catch (Exception e) {
					e.printStackTrace();
					logger.error("ERROR::SmartIT_UX::com.lnt.services::DashboardDataService.correlateRespTime():: "
							+ e);
				}
			} else {
				urlConnection = url.openConnection();
			}

			in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			jsonData.append(in.readLine());
		} catch (MalformedURLException urlE) {
			logger.error("ERROR::SmartIT_UX::com.lnt.services::DashboardDataService.correlateRespTime():: "
					+ urlE);
		} catch (IOException eIO) {
			logger.error("ERROR::SmartIT_UX::com.lnt.services::DashboardDataService.correlateRespTime():: "
					+ eIO);
		}
		return jsonData.toString();
	}
	
	public String predictResponseTime(String app, String page){
		URL url = null;
		URLConnection urlConnection = null;
		BufferedReader in = null;
		StringBuffer jsonData = new StringBuffer();
		String wsUrl = WSHelper.constructURLForPredictionResponseTime(app, page);
		try {
		url = new URL(wsUrl);
		logger.debug("DEBUG::SmartIT_UX::com.lnt.services::DashboardDataService.predictResponseTime():: requesting to service ---------- \n"
				+ url + "\n-------------\n");
		logger.debug("predictResponseTime ::: url >>>>>>> " + wsUrl);
		if (Constant.isProxyRequired) {
			logger.info("INFO::SmartIT_UX::com.lnt.services::DashboardDataService.predictResponseTime:: connection through proxy");
			try {
				urlConnection = url.openConnection(getHttpProxy());
			} catch (IOException e) {
				logger.error("ERROR::SmartIT_UX::com.lnt.services::DashboardDataService.predictResponseTime() === \n"
						+ e + "\n------------------\n");
			} catch (Exception e) {
				logger.error("ERROR::SmartIT_UX::com.lnt.services::DashboardDataService.predictResponseTime():: " + e);
			}
		} else {
			urlConnection = url.openConnection();
		}
		in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
		jsonData.append(in.readLine());
		}catch(MalformedURLException urlE) {
			logger.error("ERROR::SmartIT_UX::com.lnt.services::DashboardDataService.predictResponseTime():: "+urlE);
		}catch (IOException eIO) {
			logger.error("ERROR::SmartIT_UX::com.lnt.services::DashboardDataService.predictResponseTime():: "+eIO);
		}
		return jsonData.toString();
	}
	
	public String getLastUpdatedRespTime(String param,String app, String page,String limit){
		URL url = null;
		URLConnection urlConnection = null;
		BufferedReader in = null;
		StringBuffer jsonData = new StringBuffer();
		String wsUrl = WSHelper.constructURLTogetLastUpdatedRespTime(param,app, page,limit);
		try {
		url = new URL(wsUrl);
		logger.debug("DEBUG::SmartIT_UX::com.lnt.services::DashboardDataService.getLastUpdatedRespTime():: requesting to service ---------- \n"
				+ url + "\n-------------\n");
		logger.debug("getLastUpdatedRespTime ::: url >>>>>>> " + wsUrl);
		if (Constant.isProxyRequired) {
			logger.info("INFO::SmartIT_UX::com.lnt.services::DashboardDataService.getLastUpdatedRespTime:: connection through proxy");
			try {
				urlConnection = url.openConnection(getHttpProxy());
			} catch (IOException e) {
				logger.error("ERROR::SmartIT_UX::com.lnt.services::DashboardDataService.getLastUpdatedRespTime() === \n"
						+ e + "\n------------------\n");
			} catch (Exception e) {
				logger.error("ERROR::SmartIT_UX::com.lnt.services::DashboardDataService.getLastUpdatedRespTime():: " + e);
			}
		} else {
			urlConnection = url.openConnection();
		}
		in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
		jsonData.append(in.readLine());
		}catch(MalformedURLException urlE) {
			logger.error("ERROR::SmartIT_UX::com.lnt.services::DashboardDataService.getLastUpdatedRespTime():: "+urlE);
		}catch (IOException eIO) {
			logger.error("ERROR::SmartIT_UX::com.lnt.services::DashboardDataService.getLastUpdatedRespTime():: "+eIO);
		}
		return jsonData.toString();
	}
	public Proxy getHttpProxy() throws Exception{
		Proxy proxy = null;
		proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(Constant.PROXY_HOST, Integer.parseInt(Constant.PROXY_PORT)));
		return proxy;
	}

	@Override
	public String getAdditionalInfoForTrends(String parameterName,
			String addressedMachine, String respAppName, String respPageName,
			String osType, String timerange, String xAxis, String yAxis) {
		HttpURLConnection urlConnection = null;
		BufferedReader in = null;
		StringBuffer jsonData = new StringBuffer();
		InputStream paramPropertiesStream= null;
		String xAxisValue=null;
		String yAxisValue=null;
		
		logger.debug("DEBUG::SmartIT_UX::com.lnt.services.DashboardDataService.getAdditionalInfo():: for parameter = "+ parameterName);
		try {
			if(parameterName.equals("DISK") || parameterName.equals("CPU") || parameterName.equals("RAM")){
				if(osType.equalsIgnoreCase("windows")){
					parameterName="WIN"+parameterName;
				}
		}
			String urlParameters  = "parameterName="+parameterName+"&mapName="+addressedMachine;//+"&urlname="+respAppName;
			String newURL =wsUrl +parameterName+"/"+addressedMachine;
			URL url = new URL(newURL);
			/*logger.debug("DEBUG::SmartIT_UX::com.lnt.services::DashboardDataService.getAdditionalInfo() :: requesting to service :: "+ url);
			if (Constant.isProxyRequired) {
				logger.info("INFO::SmartIT_UX::com.lnt.services::DashboardDataService.getAdditionalInfo:: connection through proxy");
				try {
					urlConnection = (HttpURLConnection) url.openConnection(getHttpProxy());
				} catch (IOException e) {
					logger.error("ERROR::SmartIT_UX::com.lnt.services::DashboardDataService.getAdditionalInfo() === \n",e);
				}
			} else {
				urlConnection = (HttpURLConnection) url.openConnection();
			}
			
			if(!parameterName.equals("URL") && !parameterName.equals("RESPONSETIME")){
				if(osType.equalsIgnoreCase("windows")){
					parameterName="WIN"+parameterName;
				}
			}
				
		//	byte[] postData  = urlParameters.getBytes();
			urlConnection.setDoOutput(true);
			urlConnection.setRequestMethod( "GET" );
			urlConnection.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded"); 
			urlConnection.setRequestProperty( "charset", "utf-8");
			urlConnection.setUseCaches( false );
			try( DataOutputStream wr = new DataOutputStream( urlConnection.getOutputStream())) {
			   wr.write( postData );
			}
			System.out.println(urlConnection.toString());
			in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));*/
			Properties props = new Properties();
			paramPropertiesStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("params.properties");
			props.load(paramPropertiesStream);
			yAxisValue = props.getProperty(yAxis).toString();
			xAxisValue = props.getProperty("XAXIS").toString();
			RestTemplate restCallToService=new RestTemplate();
			Map<String, String> params = new HashMap<String, String>();
		    params.put("parameterName",parameterName);
		    params.put("mapName",addressedMachine);
		    params.put("timerange",timerange);
		    params.put("xAxis",xAxisValue);
		    params.put("yAxis", yAxisValue);
			String result=restCallToService.getForObject("http://localhost:7080/ELKDemo/search/{parameterName}/{mapName}/{timerange}/{xAxis}/{yAxis}",String.class,params); 
			System.out.println("result recieved "+result);
			//jsonData.append(result);
			jsonData.append(result);
			
		} catch (Exception e) {
			System.out.println("ERROR::SmartIT_UX::com.lnt.services::DashboardDataService.getAdditionalInfo():: while connecting to Web Service!!!\n"
					+ e + "\n----------------------------\n");
			
		}
		/*try {
			jsonData.append(in.readLine());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		System.out.println("jsonData.toString()"+jsonData.toString());
		return jsonData.toString();
	}

	@Override
	public String getDataForDonutChart() throws IOException {
		InputStream paramPropertiesStream= null;
		String count=null;
		Properties props = new Properties();
		paramPropertiesStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("params.properties");
		props.load(paramPropertiesStream);
		StringBuffer stringReturned=new StringBuffer();
		 List<Map<String,String>> logsList=new ArrayList<Map<String,String>>();
		try {
			String elasticSearchLoc = props.getProperty("machine.name");
			TransportClient client = new PreBuiltTransportClient(Settings.EMPTY).addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(elasticSearchLoc),9300));
			@SuppressWarnings("deprecation")
			SearchResponse responseall = client.prepareSearch("alerts").setSource(new SearchSourceBuilder().size(0)).get();
			double countAll=responseall.getHits().getTotalHits();
			SearchRequestBuilder searchRequestBuilderNew = client.prepareSearch()
		            .setIndices("alerts")
		            .addAggregation(AggregationBuilders.terms("alertname").field("alertname.keyword"))
		            .setSearchType(SearchType.DFS_QUERY_THEN_FETCH);
		    SearchResponse responseNew = searchRequestBuilderNew.execute().actionGet();
		    Terms agg1 =(Terms) responseNew.getAggregations().get("alertname");
		    for(org.elasticsearch.search.aggregations.bucket.terms.Terms.Bucket buck:agg1.getBuckets()){
		    	HashMap<String,String> map=new HashMap<String, String>();
		    	double countAlerts=buck.getDocCount();
				double alertPercent=(double)(countAlerts/countAll)*100;
				alertPercent=Double.parseDouble(new DecimalFormat("##.##").format(alertPercent));
				count=Double.toString(alertPercent);
				map.put("alertName",buck.getKeyAsString().toUpperCase());
				map.put("alertValue",count);
				logsList.add(map);
		    }	
			ObjectMapper mapper=new ObjectMapper();
			stringReturned.append(mapper.writeValueAsString(logsList));
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} 
		return stringReturned.toString();
	}

	@Override
	public String getDataForDonutChartGrid(String sliceName, String sliceValue) throws JsonGenerationException, JsonMappingException, IOException {
		InputStream paramPropertiesStream= null;
		String count=null;
		Properties props = new Properties();
		paramPropertiesStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("params.properties");
		props.load(paramPropertiesStream);
		StringBuffer stringReturned=new StringBuffer();
		 List<Map<String,String>> logsList=new ArrayList<Map<String,String>>();
		try {
			//Properties props = new Properties();
			//paramPropertiesStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("elasticsearch.properties");
			//props.load(paramPropertiesStream);
			String elasticSearchLoc = props.getProperty("machine.name");
			TransportClient client = new PreBuiltTransportClient(Settings.EMPTY).addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(elasticSearchLoc),9300));
			@SuppressWarnings("deprecation")
			//SearchResponse responseall = client.prepareSearch("alerts").get();
	        
			//int countAll=(int) responseall.getCount();
			SearchRequestBuilder searchRequestBuilderNew = client.prepareSearch()
		           .setIndices("alerts")
		           .setSize(500)
		           //.addStoredField("alertdate","alertname","alerttype","appname","description","hostname","parametername")
		           .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)       
		          .setQuery(QueryBuilders.matchQuery("alertname", sliceName));
		    SearchResponse responseNew = searchRequestBuilderNew.execute().actionGet();
		    SearchHits searchHitsNew = responseNew.getHits();
		    for(SearchHit hit:responseNew.getHits()){
		    	HashMap<String,String> map=new HashMap<String, String>();
		    	map.put("alertdate", hit.getSource().get("alertdate").toString());
		    	map.put("alertname",hit.getSource().get("alertname").toString());
		    	map.put("alerttype",hit.getSource().get("alerttype").toString());
		    	map.put("appname",hit.getSource().get("appname").toString());
		    	map.put("description",hit.getSource().get("description").toString());
		    	map.put("hostname",hit.getSource().get("hostname").toString());
		    	map.put("parametername",hit.getSource().get("parametername").toString());
		    	//System.out.println(hit.field("alertname").value());
				logsList.add(map);
		    }	
			ObjectMapper mapper=new ObjectMapper();
			stringReturned.append(mapper.writeValueAsString(logsList));
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} 
		return stringReturned.toString();
	}

	@Override
	public String getDataForPieChart() throws IOException {
		InputStream paramPropertiesStream= null;
		String count=null;
		Properties props = new Properties();
		paramPropertiesStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("params.properties");
		props.load(paramPropertiesStream);
		StringBuffer stringReturned=new StringBuffer();
		 List<Map<String,String>> logsList=new ArrayList<Map<String,String>>();
		try {
			String elasticSearchLoc = props.getProperty("machine.name");
			TransportClient client = new PreBuiltTransportClient(Settings.EMPTY).addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(elasticSearchLoc),9300));
			@SuppressWarnings("deprecation")
			SearchResponse responseall = client.prepareSearch("alerts").setSource(new SearchSourceBuilder().size(0)).get();
			double countAll=responseall.getHits().getTotalHits();
			SearchRequestBuilder searchRequestBuilderNew = client.prepareSearch()
		            .setIndices("alerts")
		            .addAggregation(AggregationBuilders.terms("by_fields").field("alerttype.keyword"))
		            .setSearchType(SearchType.DFS_QUERY_THEN_FETCH);
		    SearchResponse responseNew = searchRequestBuilderNew.execute().actionGet();
		    Terms agg1 =(Terms) responseNew.getAggregations().get("by_fields");
		    for(org.elasticsearch.search.aggregations.bucket.terms.Terms.Bucket buck:agg1.getBuckets()){
		    	HashMap<String,String> map=new HashMap<String, String>();
		    	double countAlerts=buck.getDocCount();
				double alertPercent=(double)(countAlerts/countAll)*100;
				alertPercent=Double.parseDouble(new DecimalFormat("##.##").format(alertPercent));
				count=Double.toString(alertPercent);
				map.put("alertType",buck.getKeyAsString().toUpperCase());
				map.put("alertValue",count);
				logsList.add(map);
		    }	
			ObjectMapper mapper=new ObjectMapper();
			stringReturned.append(mapper.writeValueAsString(logsList));
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} 
		return stringReturned.toString();
	}

	@Override
	public String getDataForMetricChart() throws JsonGenerationException, JsonMappingException, IOException {
		InputStream paramPropertiesStream= null;
		String count=null;
		Properties props = new Properties();
		paramPropertiesStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("params.properties");
		try {
			props.load(paramPropertiesStream);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		StringBuffer stringReturned=new StringBuffer();
		 List<Map<String,String>> logsList=new ArrayList<Map<String,String>>();
		 Integer size=0;
		try {
			String elasticSearchLoc = props.getProperty("machine.name");
			TransportClient client = new PreBuiltTransportClient(Settings.EMPTY).addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(elasticSearchLoc),9300));
			@SuppressWarnings("deprecation")
			/*SearchResponse responseall = client.prepareSearch("alerts").setSource(new SearchSourceBuilder().size(0)).get();
			double countAll=responseall.getHits().getTotalHits();*/
			SearchRequestBuilder searchRequestBuilderNew = client.prepareSearch()
		            .setIndices("windowssystemlogs")
		            .addAggregation(AggregationBuilders.terms("by_fields").field("hostname.keyword"))
		            .setSearchType(SearchType.DFS_QUERY_THEN_FETCH);
		    SearchResponse responseNew = searchRequestBuilderNew.execute().actionGet();
		    Terms agg1 =(Terms) responseNew.getAggregations().get("by_fields");
		    
		    for(org.elasticsearch.search.aggregations.bucket.terms.Terms.Bucket buck:agg1.getBuckets()){
		    	HashSet<String> setMachines=new HashSet<String>();
		    	setMachines.add(buck.getKeyAsString().toUpperCase());
				//map.put("alertValue",count);
				//logsList.add(map);
		    	size=setMachines.size();
		    }	
			ObjectMapper mapper=new ObjectMapper();
			HashMap<String, String> map=new HashMap<String, String>();
			map.put("noofmachines", size.toString());
			logsList.add(map);
			stringReturned.append(mapper.writeValueAsString(logsList));
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} 
		return stringReturned.toString();
	}

	@Override
	public String getDataMachineInfo() throws JsonGenerationException, JsonMappingException, IOException {
		Properties props = new Properties();
		InputStream paramPropertiesStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("params.properties");
		props.load(paramPropertiesStream);
		String applicationName = props.getProperty("application.name");
		Session session=sessionFactory.openSession();
		Query query=session.createQuery("From AppDeviceMaster adm WHERE adm.app.appName='"+applicationName+"'");
		List<AppDeviceMaster> appList=query.list();
		session.close();
		int windowsCount=0, linuxCount=0;
		for(AppDeviceMaster adm:appList){
			if(adm.getDevicetype().getName().equals("Windows Server")){
				windowsCount=windowsCount+1;
			}
			if(adm.getDevicetype().getName().equals("Linux Server")){
				linuxCount=linuxCount+1;
			}
		}
			ObjectMapper mapper=new ObjectMapper();
			StringBuffer stringReturned=new StringBuffer();
			 List<Map<String,String>> logsList=new ArrayList<Map<String,String>>();
			HashMap<String, String> map=new HashMap<String, String>();
			map.put("total", Integer.toString(appList.size()));
			map.put("windows", Integer.toString(windowsCount));
			map.put("linux", Integer.toString(linuxCount));
			logsList.add(map);
			stringReturned.append(mapper.writeValueAsString(logsList));
		return stringReturned.toString();
	}

}
