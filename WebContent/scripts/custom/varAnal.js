
$(document).ready(function() {
	/*$("#menu_variableAnalysis").css('background-color','black');*/
	
	$("#split2_section_pageSelectionDialog").hide();
	$("#listView").hide();
	
	$("#split3_section_CorrelatedGraphs").hide();
	$("#split3_section_PredictionGraphs").hide();
	$("#split2_section").hide();
	
	$(".split1_radio").click(function() {
		var parameterRadio="";
		parameterRadio = $("input[name=split1_radio]:checked").attr("id");
		checkParameterSelected(parameterRadio);
	});
	
	$(".split2_section_button_reset").click(function() {
		
		$("#split3_section_CorrelatedGraphs").hide();
		$("#split3_section_PredictionGraphs").hide();
		$("#split2_section_correlation_buttons").hide();
		
		$("#split2_section_responseTimeGraphContainer").hide();
		$(".split2_section_pageURL").val('');
	});
	
	/*autopopulate for application and server starts*/
	$.ajax({
		type : "POST",
		url : "fetchApplicationList.html",
		success : function(response) {
			var applications=[];
			var applications = response;
			
	 $('#split1_input1').autocomplete({
	     source: function (request, response) {
	         var matches = $.map(applications, function (acItem) {
	             if( (acItem.toUpperCase().indexOf(request.term.toUpperCase()) == 0) || ((acItem.toLowerCase().indexOf(request.term.toLowerCase()) == 0)) ){
	                 return acItem;
	             }
	         });
	         response(matches);
	     }
	 });
		}
	});
	 
	$.ajax({
		type : "POST",
		url : "fetchServerList.html",
		success : function(response) {
			var servers=[];
			var servers = response;
	 $('#split1_input2').autocomplete({
	     source: function (request, response) {
	    	// alert("request : "+JSON.stringify(request.term.toUpperCase()));
	         var matches = $.map(servers, function (acItem) {
	             if( (acItem.toUpperCase().indexOf(request.term.toUpperCase()) == 0) || ((acItem.toLowerCase().indexOf(request.term.toLowerCase()) == 0)) ){
	                 return acItem;
	             }
	         });
	         response(matches);
	     }
	 });
		}
	});
	
	/*autopopulate for application and server ends*/
	
	//displays the page selection in the split 2 section
	/*$(".split1_section_button_Go").click(function(){
		$("#split2_section").show();
	});
	*/
	//displays the historic response time graph
	$(".split1_section_button_Go").click(function(){
		$("#split2_section").show();
		applicationName = $('#split1_input1').val();
		page = $('.split2_section_pageURL').val();
		ServerIPAddress = $('#split1_input2').val();
		var sel = document.getElementById('select_param');
		var parameter= sel.options[sel.selectedIndex].text;
		var titleText;
		var yLabel;
		if(parameter=="RAM"){
			titleText= "Memory Usage prediction";
			yLabel="Memory Usage in %";
		}
		else if(parameter=="CPU"){
			titleText= "CPU Usage prediction";
			yLabel="CPU usage in %";
		}
		else if(parameter=="ResponseTime"){
			titleText= "Responsetime prediction";
			yLabel="Response-time in ms";
		}
		$.ajax({
			type : "POST",
			url : "drawHistoricGraphforResponseTime.html",
			//data : "parameterName="+"app-url-status"+"&addressedMachine="+"INFARSZC90433"+"&osType="+"windows"+"&timerange="+12+"&xAxisField="+"LogDate"+"&yAxisField="+"URL",
			data : "parameterName="+parameter,
			success : function(response) {
				

			  	  var result = response.toString();
			  	  var xAxisData=[];
			  	  var objList=JSON.parse(result);
			  	  var series1 = [];
			  	  var yAxisArray1 = [];
			  	  var yAxisArray2 = [];
			  	  yAxisArray1.push('Actual Value');
			  	  yAxisArray2.push('Predicted Value');
			  	  xAxisData.push('time');
			  	  for(b=0;b<objList.length;b++){
								var actualObject=objList[b];
								yAxisArray1.push(actualObject["yaxis1"]);
								yAxisArray2.push(actualObject["yaxis2"]);
								xAxisData.push(Date.parse(actualObject["xaxis"]));
			  	  }
			  	var chart = c3.generate({
					bindto:'#split2_section_responseTimeGraphContainer',
					data: {
				        x: 'time',
				        columns: [
			              xAxisData,         
			              yAxisArray1,
			              yAxisArray2
				        ]
				    },
				    
				    title : { 
				    	text : titleText
				    },
				    subchart: {
				        show: true
				    },
				    zoom: {
				        enabled: true
				    },
				    axis : {
				        x : {
				        	label:'Time',
				            type : 'timeseries',
				            tick: {
				            	fit:true,
				            	count: objList.length,
				                format:'%Y-%m-%d %I:%M:%S' 
				            }
				        },
				        y : {
				        	label:yLabel
				        }
				    }
				});
			  	
				/*
				console.log(response);
				var hitCountArray = new Array();
				var responseTimeAvgArray = new Array();
				for ( var i = 0; i < response.length; i++) {
					var countOfHits = response[i].count;
					var avgResponseTime=response[i].responseTime;
					hitCountArray[i] = [ (Date.UTC(response[i].year, response[i].month - 1, response[i].day,response[i].hour)), countOfHits ];
					console.log(hitCountArray[i]);
					responseTimeAvgArray[i] = [ (Date.UTC(response[i].year, response[i].month -1, response[i].day,response[i].hour)), avgResponseTime ];
					console.log(response[i].year, response[i].month - 1, response[i].day,response[i].hour);
				}
		
		 $('#split2_section_responseTimeGraphContainer').highcharts('StockChart',{
       	  chart: {
      		zoomType: 'x',  
	            height:250,
	            width:970
	        },
	        rangeSelector : {
	            buttons: [{
	                type: 'hour',
	                count: 1,
	                text: '1h'
	            }, {
	                type: 'day',
	                count: 1,
	                text: '1d'
	            }, {
	                type: 'month',
	                count: 1,
	                text: '1m'
	            }, {
	                type: 'year',
	                count: 1,
	                text: '1y'
	            }, {
	                type: 'all',
	                text: 'All'
	            }],
	            inputEnabled: false, // it supports only days
	            selected : 4 // all
	        },
          tooltip: {
          	  shared: true
              formatter: function() {
                  return ''+
                          "" +
                          'Time: '+ Highcharts.dateFormat('%e %b %Y %I:%M %p', this.x)+'  ,  '+this.y;
              }
          },
          
          xAxis: {
          	type: 'datetime' ,
              title: {
                  enabled: true
              },

              dateTimeLabelFormats : {
              	hour: '%H:%M',
              	day: '%e. %b',
              	week: '%e. %b',
              	month: '%b \'%y',
              	year: '%Y'
              },
              events: {
             	 setExtremes:function(event){
             		var timestamp = event.min;
             		var timestamp_end = event.max;
         
             	date = new Date(timestamp);
             	end_date = new Date(timestamp_end);
             	currentDate=date.getDate();
             	currentMonth=date.getMonth();
             	currentYear=date.getFullYear();
             	currentHour=date.getHours();
             	currentMin=date.getMinutes();
             	endDate=end_date.getDate();
             	endMonth=end_date.getMonth();
             	endYear=end_date.getFullYear();
             	endHour=end_date.getHours();
             	endMin=end_date.getMinutes();
          showCorrelatedData(timestamp,currentDate,currentMonth+1,currentYear,endDate,endMonth+1,endYear,ServerIPAddress);
             	 }
               }
          },
          yAxis: [{
        	  // Primary yAxis
              labels: {
                  format: '{value}',
                  style: {
                      color: Highcharts.getOptions().colors[1]
                  }
              },
              title: {
                  text: 'Response Time(sec) - (line)',
                  style: {
                      color: Highcharts.getOptions().colors[1]
                  }
              }
            
          }, { // Secondary yAxis
              title: {
                  text: 'Count - (columns)',
                  style: {
                      color: Highcharts.getOptions().colors[2]
                  }
              },
              labels: {
                  format: '{value}',
                  style: {
                      color: Highcharts.getOptions().colors[2]
                  }
              },
              opposite: true
          }],
          legend: {
              layout: 'vertical',
              align: 'left',
              x: 120,
              verticalAlign: 'top',
              y: 100,
              floating: true,
              backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
          },

          title : {
              text : 'Response Time'
          },
          plotOptions: {
          showInLegend: true,
          area: {
              fillColor: {
                  linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
                  stops: [
                      [0, Highcharts.getOptions().colors[0]],
                      [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                  ]
              },
              marker: {
                  radius: 2
              },
              lineWidth: 1,
              states: {
                  hover: {
                      lineWidth: 1
                  }
              },
              threshold: null
          }
          },
          series : [{
              name : 'Count of Hits (columns)',
              type: 'column',
              yAxis: 1,
              data : hitCountArray,
              tooltip: {
            	  valueSuffix: ''
              }
          },{
              name : 'Response Time (line)',
              type: 'line',
              yaxis:2,
              data : responseTimeAvgArray,
              tooltip: {
                  valueDecimals: 2,
                  valueSuffix: 'Sec'
              }
          }]
      }); // end of response time highchart
			*/} // end of success
	});//end of ajax function
		
})
	
/*section 2 response time graph ends*/	
	
/*section 2 page search dialog starts*/
	$(".split2_section_button_search").click(function(){
		$("#split2_section_pageSelectionDialog").show();
		$("#split2_section_pageSelectionDialog").dialog({
		    resizable: false,
		    autoOpen: false,
		    height:245,
		    width:700,
		    position:  'center',
		    modal: true,
		    buttons: {
	      }
		});
		$(".ui-dialog-titlebar-close").show();
		$(".ui-dialog-title").text('');
		$(".ui-dialog-title").text('Select Page');
		$("#split2_section_pageSelectionDialog").dialog("open");
		/*section 2 page search grid starts*/
		$("#split2_section_pageSelectionGrid").jqGrid({
			datatype:"local",
			//mtype: 'POST',
			colNames:['Pages'],
			colModel:[ 
			          {
			        	  name : 'page',
			        	  label : 'Pages',
			        	    width:"10px",
			        	    sortable: true
			          }, 
			          ],
			          onSelectRow: function ()
			          {
			        	  var myGrid = $('#split2_section_pageSelectionGrid');

			        	  selRowId = myGrid.jqGrid ('getGridParam', 'selrow');
			        	  celvalue1= myGrid.jqGrid ('getCell', selRowId, 'page');
			        	 $(".split2_section_pageURL").val(celvalue1);
			        	 $("#split2_section_pageSelectionDialog").dialog("close");
			        	 $(".ui-dialog-title").hide();
			        		$(".ui-dialog-titlebar-close").hide();
			          },
			        
			          pager : '#split2_section_pageSelectionGridPager',
			          rowNum : 10,
			          rowList : [ 10, 20, 30 ],
			          viewrecords : true,
			          loadonce: true,
			          width : 600,
			          height: 140
	  })
			var data =  [
						{ "page": "M008EInstCourseClassYearEMS.do"},		             
						{ "page": "M275DCeAypExamEMD.do"},
						{ "page": "M284DCeAypExamEMD.do"},
						{ "page": "M276CCeAypExamESF.do"},
						{ "page": "M274DCeAypExamEMD.do"},
						{ "page": "M270DInstAypSectionEMD.do"},
						{ "page": "M433DTpoAypPlmtStudentEMD.do"},
						{ "page": "M305ECeEeexamApplicationEMS.do"},
		             ];

		 for (var i = 0; i < data.length; i++)
		 { 
		 	$("#split2_section_pageSelectionGrid").jqGrid('addRowData', i + 1, data[i]);
		 }
	})
	
	/*section 2 page search dialog ends*/
	
	/*Related variables  graphs starts :: this function which executes on click of show related variables button*/
		$(".split2_section_button_showRelatedVariables").click(function(){
			$("#split3_section_CorrelatedGraphs").show();
			ServerIPAddress = $('#split1_input2').val();
			
			
		$.ajax({
			type : "POST",
			url : "drawHistoricGraphforResponseTime.html",
			data : "parameterName="+"windowssystemlogs"+"&addressedMachine="+"INFARSZC90433"+"&osType="+"windows"+"&timerange="+12+"&xAxisField="+"XAXIS"+"&yAxisField="+"RAM",
//			data : "page=" + page+"&applicationName="+ applicationName,
			success : function(response) {
				

			  	  var result = response.toString();
			  	  var xAxisData=[];
			  	  var objList=JSON.parse(result);
			  	  var series1 = [];
			  	  var yAxisArray = [];
			  	  yAxisArray.push('RAM Usage');
			  	  xAxisData.push('time');
			  	  for(b=0;b<objList.length;b++){
								var actualObject=objList[b];
								yAxisArray.push(actualObject["yaxis"]);
								xAxisData.push(Date.parse(actualObject["xaxis"]));
								series1.push([Date.parse(actualObject["xaxis"]),actualObject["yaxis"]]);
			  	  }
			  	  console.log(series1);
			  	var chart = c3.generate({
					bindto:'#split3_section_CorrelatedGraphs_1',
					data: {
				        x: 'time',
				        columns: [
			              xAxisData,         
			              yAxisArray
				        ]
				    },
				    
				    title : { 
				    	text : 'RAM Usage trend for last 12 hours'
				    },
				    subchart: {
				        show: true
				    },
				    zoom: {
				        enabled: true
				    },
				    axis : {
				        x : {
				        	label:'Time',
				            type : 'timeseries',
				            tick: {
				            	fit:true,
				            	count: objList.length,
				                format:'%Y-%m-%d %I:%M:%S' 
				            }
				        },
				        y : {
				        	label:'RAM Usage in %'
				        }
				    }
				});
			}
			});
			  	
			  	$.ajax({
					type : "POST",
					url : "drawHistoricGraphforResponseTime.html",
					data : "parameterName="+"windowssystemlogs"+"&addressedMachine="+"INFARSZC90433"+"&osType="+"windows"+"&timerange="+12+"&xAxisField="+"XAXIS"+"&yAxisField="+"CPU",
//					data : "page=" + page+"&applicationName="+ applicationName,
					success : function(response) {
						

					  	  var result = response.toString();
					  	  var xAxisData=[];
					  	  var objList=JSON.parse(result);
					  	  var series1 = [];
					  	  var yAxisArray = [];
					  	yAxisArray.push('RAM Usage');
					  	  xAxisData.push('time');
					  	  for(b=0;b<objList.length;b++){
										var actualObject=objList[b];
										yAxisArray.push(actualObject["yaxis"]);
										xAxisData.push(Date.parse(actualObject["xaxis"]));
										series1.push([Date.parse(actualObject["xaxis"]),actualObject["yaxis"]]);
					  	  }
					  	  console.log(series1);
					  	var chart = c3.generate({
							bindto:'#split3_section_CorrelatedGraphs_2',
							data: {
						        x: 'time',
						        columns: [
					              xAxisData,         
					              yAxisArray
						        ]
						    },
						    
						    title : { 
						    	text : 'CPU Usage trend for last 12 hours'
						    },
						    subchart: {
						        show: true
						    },
						    zoom: {
						        enabled: true
						    },
						    axis : {
						        x : {
						        	label:'Time',
						            type : 'timeseries',
						            tick: {
						            	fit:true,
						            	count: objList.length,
						                format:'%Y-%m-%d %I:%M:%S' 
						            }
						        },
						        y : {
						        	label:'RAM Usage in %'
						        }
						    }
						});
					}
			  	});

				
			/*show related variables graphs for CPU starts*/
			/*$.ajax({
				type : "POST",
				url : "plotRelatedVariabesChartsCPU.html",
				data : "ServerIPAddress=" + ServerIPAddress,
				success : function(response) {
					var dateValueArray = new Array();
					var fiveMinLoadAvg = new Array();
					var tenMinLoadAvg = new Array();
					var fifteenMinLoadAvg = new Array();
					for ( var i = 0; i < response.length; i++) {
						fiveMinLoadAvg[i] = [ (Date.UTC(response[i].year, response[i].month -1, response[i].day,response[i].hour)),response[i].fiveMinCPUAvg];
						tenMinLoadAvg[i] = [ (Date.UTC(response[i].year, response[i].month -1, response[i].day,response[i].hour)),response[i].tenMinCPUAvg];
						fifteenMinLoadAvg[i] = [ (Date.UTC(response[i].year, response[i].month -1, response[i].day,response[i].hour)),response[i].fifteenMinCPUAvg];
					} 
					$('#split3_section_CorrelatedGraphs_1').highcharts('StockChart', {
						 rangeSelector : {
					            buttons: [{
					                type: 'hour',
					                count: 1,
					                text: '1h'
					            }, {
					                type: 'day',
					                count: 1,
					                text: '1d'
					            }, {
					                type: 'month',
					                count: 1,
					                text: '1m'
					            }, {
					                type: 'year',
					                count: 1,
					                text: '1y'
					            }, {
					                type: 'all',
					                text: 'All'
					            }],
					            inputEnabled: false, // it supports only days
					            selected : 4 // all
					        },
			            title : {
			                text : 'Response Time Vs CPU Load'
			            },
			            tooltip: {
			                formatter: function() {
			                    return ''+
			                            "" +
			                            'Time: '+ Highcharts.dateFormat('%e %b %Y %I:%M %p', this.x) + 'CPU Load' +(point.y);
			                }
			            },
			            xAxis: {
			            	type: 'datetime' ,
			                title: {
			                    enabled: true,
			                    text: 'Hours of the Day'
			                },
			                dateTimeLabelFormats : {
			                	hour: '%H:%M',
			                	day: '%e. %b',
			                	week: '%e. %b',
			                	month: '%b \'%y',
			                	year: '%Y'
			                }
			            },
						yAxis : {
							min:0,
							title : {
								text : 'CPU Load Average',
								color : '#808080'
							},
							plotLines : [ {
								value : 0,
								width : 1,
								color : '#808080',
								states : {
									hover : {
										color : '#ffffff'
									}
								}
							} ]
						},
						chart : {
							type : 'line',
							renderTo : 'forcastMonthHighChart',
							marginTop : 80,
							marginRight : 40,
							height :280,
							width : 600,
							zoomType:'x'
						},
			            tooltip: {
			                formatter: function() {
			                    return ''+
			                            "" +
			                            'Time: '+ Highcharts.dateFormat('%e %b %Y %I:%M %p', this.x) + 'Load average'+Highcharts.numberFormat(this.y,2);
			                }
			            },
						legend : {
							layout : 'vertical',
							align : 'right',
							verticalAlign : 'middle',
							borderWidth : 0
						},
						series :
							[
					          {
					        	  name : 'Five Minute CPU Average',
					        	  data : fiveMinLoadAvg
					        	  
					          },
					          {
					        	  name : 'Ten Minute CPU Average',
					        	  data : tenMinLoadAvg
					        	  
					          },
					          {
					        	  name : 'Fifteen Minute CPU Average',
					        	  data : fifteenMinLoadAvg
					        	 
					          }
					          ]
					});
				}
			});
			show related variables graphs for CPU ends
			
			show related variables graphs for RAM starts
			$.ajax({
				type : "POST",
				url : "plotRelatedVariabesChartsRAM.html",
				data : "ServerIPAddress=" + ServerIPAddress,
				success : function(response) {
					if (jQuery.isEmptyObject(response)) {
						alert("No data found ");
					}
					var avgRAMUsage = new Array();
					for ( var i = 0; i < response.length; i++) {
						avgRAMUsage[i] = [ (Date.UTC(response[i].year, response[i].month -1, response[i].day,response[i].hour)),response[i].avgRamUsed];
						console.log(Date.UTC(response[i].year, response[i].month -1, response[i].day,response[i].hour));
					} 
			$('#split3_section_CorrelatedGraphs_2').highcharts('StockChart', {
				 rangeSelector : {
			            buttons: [{
			                type: 'hour',
			                count: 1,
			                text: '1h'
			            }, {
			                type: 'day',
			                count: 1,
			                text: '1d'
			            }, {
			                type: 'month',
			                count: 1,
			                text: '1m'
			            }, {
			                type: 'year',
			                count: 1,
			                text: '1y'
			            }, {
			                type: 'all',
			                text: 'All'
			            }],
			            inputEnabled: false, // it supports only days
			            selected : 4 // all
			        },
	            title : {
	                text : 'Response Time VS RAM Usage Trend'
	            },
	            tooltip: {
	                formatter: function() {
	                    return ''+
	                            "" +
	                            'Time: '+ Highcharts.dateFormat('%e %b %Y %I:%M %p', this.x) + '   Ram Usage'  +Highcharts.numberFormat(this.y);
	                },
	                valueDecimals: 2
	            },
	            xAxis: {
	            	type: 'datetime' ,
	                title: {
	                    enabled: true,
	                    text: 'Hours of the Day'
	                },
	                dateTimeLabelFormats : {
	                	hour: '%H:%M',
	                	day: '%e. %b',
	                	week: '%e. %b',
	                	month: '%b \'%y',
	                	year: '%Y'
	                }
	            },
				yAxis : {
					title : {
						text : 'RAM Usage (mb)',
						color : '#808080'
					},
					plotLines : [ {
						value : 0,
						width : 1,
						color : '#808080',
						states : {
							hover : {
								color : '#ffffff'
							}
						}
					} ]
				},
				chart : {
					type : 'line',
					renderTo : 'forcastMonthHighChart',
					marginTop : 80,
					marginRight : 40,
					height :280,
					width : 600,
					zoomType:'x'
				},
				legend : {
					layout : 'vertical',
					align : 'right',
					verticalAlign : 'middle',
					borderWidth : 0
				},
				series : [ 
				          {
				        	  name : 'Response Time Vs RAM',
				        	  data : avgRAMUsage
				          },
				      
				          ]

			}); // end of highchart
		}
	});*/
			/*show related variables graphs for RAM ends*/
			
			/*show related variables graphs for IO starts*/
			/*$.ajax({
				type : "POST",
				url : "plotRelatedVariabesChartsIO.html",
				data : "ServerIPAddress=" + ServerIPAddress,
				success : function(response) {
					if (jQuery.isEmptyObject(response)) {
						alert("No data found ");
					}
					var avgIOUsage = new Array();
					for ( var i = 0; i < response.length; i++) {
						avgIOUsage[i] = [ (Date.UTC(response[i].year, response[i].month -1, response[i].day,response[i].hour)),response[i].iowait];
						console.log(Date.UTC(response[i].year, response[i].month -1, response[i].day,response[i].hour));
					} 
				$('#split3_section_CorrelatedGraphs_3').highcharts('StockChart', {
					 rangeSelector : {
				            buttons: [{
				                type: 'hour',
				                count: 1,
				                text: '1h'
				            }, {
				                type: 'day',
				                count: 1,
				                text: '1d'
				            }, {
				                type: 'month',
				                count: 1,
				                text: '1m'
				            }, {
				                type: 'year',
				                count: 1,
				                text: '1y'
				            }, {
				                type: 'all',
				                text: 'All'
				            }],
				            inputEnabled: false, // it supports only days
				            selected : 4 // all
				        },
		            title : {
		                text : 'Response Time VS IO Trend'
		            },
		            tooltip: {
		                formatter: function() {
		                    return ''+
		                            "" +
		                            'Time: '+ Highcharts.dateFormat('%e %b %Y %I:%M %p', this.x) + '   \n IO Usage :'  +Highcharts.numberFormat(this.y);
		                }
		            },
		            xAxis: {
		            	type: 'datetime' ,
		                title: {
		                    enabled: true,
		                    text: 'Hours of the Day'
		                },

		                dateTimeLabelFormats : {
		                	
		                	hour: '%H:%M',
		                	day: '%e. %b',
		                	week: '%e. %b',
		                	month: '%b \'%y',
		                	year: '%Y'
		                }
		            },
					yAxis : {
						min:0,
						title : {
							text : 'IO wait (%)',
							color : '#808080'
						},
						plotLines : [ {
							value : 0,
							width : 1,
							color : '#808080',
							states : {
								hover : {
									color : '#ffffff'
								}
							}
						} ]
					},
					chart : {
						type : 'line',
						renderTo : 'forcastMonthHighChart',
						marginTop : 80,
						marginRight : 40,
						height :280,
						width : 600,
						zoomType:'x'
					},
					legend : {
						layout : 'vertical',
						align : 'right',
						verticalAlign : 'middle',
						borderWidth : 0
					},
					series : [ 
					          {
					        	  name : 'Response Time Vs IO',
					        	  data : avgIOUsage
					          },
					         ]
				});
				}
			});
			show related variables graphs for IO ends*/
			/*show related variables graphs end*/
		});
	
	$(".split2_section_button_prediction").click(function(){
		$("#split3_section_PredictionGraphs").show();
		page = $('.split2_section_pageURL').val();
		$.ajax({
			type : "POST",
			url : "showPredictionGraph.html",
			data : "page=" + page,
		
			success : function(response) {
				if (jQuery.isEmptyObject(response)) {
					alert("No data found ");
				}
				var dateValueArray = new Array();
				var predictedValueArray = new Array();
				var actualValueArray = new Array();
				for ( var i = 0; i < response.length; i++) {
					predictedValueArray[i] = [ ((response[i].epochTime)),response[i].predictedValues];
					if (0 == response[i].actualValues) {
						actualValueArray[i] = [ ((response[i].epochTime)),null];
					} else {
						actualValueArray[i] = [ ((response[i].epochTime)),response[i].actualValues];
					}
				} 
				$('#split3_section_PredictionGraph').highcharts('StockChart', {
					rangeSelector : {
				         buttons: [{
				             type: 'hour',
				             count: 1,
				             text: '1h'
				         }, {
				             type: 'day',
				             count: 1,
				             text: '1d'
				         }, {
				             type: 'month',
				             count: 1,
				             text: '1m'
				         }, {
				             type: 'year',
				             count: 1,
				             text: '1y'
				         }, {
				             type: 'all',
				             text: 'All'
				         }],
				         inputEnabled: false, // it supports only days
				         selected : 4 // all
				     },
		            title : {
		                text : 'Prediction History'
		            },
		            tooltip: {
		            	shared:true
		               /* formatter: function() {
		                    return ''+
		                            "" +
		                            'Time: '+ Highcharts.dateFormat('%e %b %Y %I:%M %p', this.x) + '   \n Response Time:'+Highcharts.numberFormat(this.y);
		                }*/
		            },
		            
		            xAxis : {
						title : {
							text : 'Date',
							color : '#808080'
						},
						dateTimeLabelFormats : {
		                	
		                	hour: '%H:%M',
		                	day: '%e. %b',
		                	week: '%e. %b',
		                	month: '%b \'%y',
		                	year: '%Y'
		                }
					},
					yAxis : {
						min:0,
						title : {
							text : 'Response Time',
							color : '#808080'
						},
						plotLines : [ {
							value : 0,
							width : 1,
							color : '#808080',
							states : {
								hover : {
									color : '#ffffff'
								}
							}
						} ]
					},
					chart : {
						type : 'line',
						renderTo : 'forcastMonthHighChart',
						marginTop : 80,
						marginRight : 40,
						height :280,
						width : 600,
						zoomType:'x'
					},
					legend : {
						layout : 'vertical',
						align : 'right',
						verticalAlign : 'middle',
						borderWidth : 0
					},
					series : [ 
					          {
					        	  name : 'Actual',
					        	  data : actualValueArray,
					        	  tooltip: {
					            	  valueSuffix: 'sec',
					            	  valueDecimals: 2
					              }
					          },
					          {
					        	  name : 'Forecast',
					        	  data : predictedValueArray,
					        	  tooltip: {
					            	  valueSuffix: 'sec',
					            	  valueDecimals: 2
					              }
					          }
					          ]

				}); // end of highchart
			} // end of success
		}); // end of ajax function
	})
})

function checkParameterSelected(parameter)
{
	if((parameter == 'split1_radio1'))
		{
		$("#split1_table1_column1").show();
		}
	else if(!(parameter == 'split1_radio1'))
		{
		$("#split1_table1_column1").hide();
		}
}
	
function showCorrelatedData(timestamp,startDate,startMonth,startYear,endDate,endMonth,endYear,ServerIPAddress)
{
	$.ajax({
		type : "POST",
		url : "showCorrelatedDataForRelatedVariablesCPU.html",
		data : "startDate=" + startDate+"&startMonth=" + startMonth+"&startYear=" + startYear+"&endDate=" +endDate+"&endMonth=" +endMonth+"&endYear=" + endYear+"&ServerIPAddress=" + ServerIPAddress,
		success : function(response) {
			var dateValueArray = new Array();
			var fiveMinLoadAvg = new Array();
			var tenMinLoadAvg = new Array();
			var fifteenMinLoadAvg = new Array();
			for ( var i = 0; i < response.length; i++) {
				fiveMinLoadAvg[i] = [ (Date.UTC(response[i].year, response[i].month -1, response[i].day,response[i].hour)),response[i].fiveMinCPUAvg];
				tenMinLoadAvg[i] = [ (Date.UTC(response[i].year, response[i].month -1, response[i].day,response[i].hour)),response[i].tenMinCPUAvg];
				fifteenMinLoadAvg[i] = [ (Date.UTC(response[i].year, response[i].month -1, response[i].day,response[i].hour)),response[i].fifteenMinCPUAvg];
			}
				$('#split3_section_CorrelatedGraphs_1').highcharts('StockChart', {
	            rangeSelector : {
	                selected : 1
	            },
	            title : {
	                text : 'Response Time Vs CPU Load'
	            },
	            tooltip: {
	                formatter: function() {
	                    return ''+
	                            "" +
	                            'Time: '+ Highcharts.dateFormat('%e %b %Y %I:%M %p', this.x) + 'CPU Average Load : '  +Highcharts.numberFormat(this.y);
	                },
	                valueDecimals: 2
	            },
	            xAxis: {
	            	
	            	min: timestamp,
	            	type: 'datetime' ,
	                title: {
	                    enabled: true,
	                    text: 'Hours of the Day'
	                },
	                dateTimeLabelFormats : {
	                	
	                	hour: '%H:%M',
	                	day: '%e. %b',
	                	week: '%e. %b',
	                	month: '%b \'%y',
	                	year: '%Y'
	                }
	            },
				yAxis : {
					min:0,
					title : {
						text : 'CPU Load Average',
						color : '#808080'
					},
					plotLines : [ {
						value : 0,
						width : 1,
						color : '#808080',
						states : {
							hover : {
								color : '#ffffff'
							}
						}
					} ]
				},
				chart : {
					type : 'line',
					renderTo : 'forcastMonthHighChart',
					marginTop : 80,
					marginRight : 40,
					height :280,
					width : 600
				},
				legend : {
					layout : 'vertical',
					align : 'right',
					verticalAlign : 'middle',
					borderWidth : 0
				},
				series :
						[
				          {
				        	  name : 'Five Minute CPU Average',
				        	  data : fiveMinLoadAvg
				        	  
				          },
				          {
				        	  name : 'Ten Minute CPU Average',
				        	  data : tenMinLoadAvg
				        	  
				          },
				          {
				        	  name : 'Fifteen Minute CPU Average',
				        	  data : fifteenMinLoadAvg
				        	 
				          }
				          ]
			});
			}
	});	
	
	/*correlated graphs for RAM starts*/
	$.ajax({
		type : "POST",
		url : "showCorrelatedDataForRelatedVariablesRAM.html",
		data : "startDate=" + startDate+"&startMonth=" + startMonth+"&startYear=" + startYear+"&endDate=" +endDate+"&endMonth=" +endMonth+"&endYear=" + endYear+"&ServerIPAddress=" + ServerIPAddress,
		success : function(response) {
			var avgRAMUsage = new Array();
			for ( var i = 0; i < response.length; i++) {
				avgRAMUsage[i] = [ (Date.UTC(response[i].year, response[i].month -1, response[i].day,response[i].hour)),response[i].avgRamUsed];
				console.log(Date.UTC(response[i].year, response[i].month -1, response[i].day,response[i].hour));
			} 
			$('#split3_section_CorrelatedGraphs_2').highcharts('StockChart', {
	            rangeSelector : {
	                selected : 1
	            },
	            title : {
	                text : 'Response Time VS RAM Usage Trend'
	            },
	            tooltip: {
	                formatter: function() {
	                    return ''+
	                            "" +
	                            'Time: '+ Highcharts.dateFormat('%e %b %Y %I:%M %p', this.x) + 'Ram Usage(mb) : '  +Highcharts.numberFormat(this.y);
	                },
	                valueDecimals: 2
	            },
	            xAxis: {
	            	type: 'datetime' ,
	                title: {
	                    enabled: true,
	                    text: 'Hours of the Day'
	                },
	                dateTimeLabelFormats : {
	                	
	                	hour: '%H:%M',
	                	day: '%e. %b',
	                	week: '%e. %b',
	                	month: '%b \'%y',
	                	year: '%Y'
	                }
	            },
				yAxis : {
					min:0,
					title : {
						text : 'RAM Usage (mb)',
						color : '#808080'
					},
					plotLines : [ {
						value : 0,
						width : 1,
						color : '#808080',
						states : {
							hover : {
								color : '#ffffff'
							}
						}
					} ]
				},
				chart : {
					type : 'line',
					renderTo : 'forcastMonthHighChart',
					marginTop : 80,
					marginRight : 40,
					height :280,
					width : 600
				},
				legend : {
					layout : 'vertical',
					align : 'right',
					verticalAlign : 'middle',
					borderWidth : 0
				},
				series : [ 
				          {
				        	  name : 'Response Time Vs RAM',
				        	  data : avgRAMUsage
				          },
				      
				          ]
			});
		}
	});
	/*correlated graphs for RAM ends*/
	
	/*correlated graphs for IO starts*/
	$.ajax({
		type : "POST",
		url : "showCorrelatedDataForRelatedVariablesIO.html",
		data : "startDate=" + startDate+"&startMonth=" + startMonth+"&startYear=" + startYear+"&endDate=" +endDate+"&endMonth=" +endMonth+"&endYear=" + endYear+"&ServerIPAddress=" + ServerIPAddress,
		success : function(response) {
			if (jQuery.isEmptyObject(response)) {
				alert("No data found ");
			}
			var avgIOUsage = new Array();
			for ( var i = 0; i < response.length; i++) {
				avgIOUsage[i] = [ (Date.UTC(response[i].year, response[i].month -1, response[i].day,response[i].hour)),response[i].iowait];
				console.log(Date.UTC(response[i].year, response[i].month -1, response[i].day,response[i].hour));
			} 
	
	$('#split3_section_CorrelatedGraphs_3').highcharts('StockChart', {
        rangeSelector : {
            selected : 1
        },
        title : {
            text : 'Response Time VS IO Usage Trend'
        },
        tooltip: {
            formatter: function() {
                return ''+
                        "" +
                        'Time: '+ Highcharts.dateFormat('%e %b %Y %I:%M %p', this.x) + '   \n IO Usage :'  +Highcharts.numberFormat(this.y);
            },
            valueDecimals: 2
        },
        xAxis: {
        	type: 'datetime' ,
            title: {
                enabled: true,
                text: 'Hours of the Day'
            },
            dateTimeLabelFormats : {
            	
            	hour: '%H:%M',
            	day: '%e. %b',
            	week: '%e. %b',
            	month: '%b \'%y',
            	year: '%Y'
            }
        },
		yAxis : {
			min:0,
			title : {
				text : 'IO Usage (mb)',
				color : '#808080'
			},
			plotLines : [ {
				value : 0,
				width : 1,
				color : '#808080',
				states : {
					hover : {
						color : '#ffffff'
					}
				}
			} ]
		},
		chart : {
			type : 'line',
			renderTo : 'forcastMonthHighChart',
			marginTop : 80,
			marginRight : 40,
			height :280,
			width : 600
		},
		
		legend : {
			layout : 'vertical',
			align : 'right',
			verticalAlign : 'middle',
			borderWidth : 0
		},
		series : [ 
		          {
		        	  name : 'Response Time Vs RAM',
		        	  data : avgIOUsage
		          },
		      
		          ]
	  });
				
	}
});
	/*correlated graphs for IO ends*/
}

function viewList(forLabel) {

	$('#listGridTable').jqGrid('GridUnload');
	$("#listView").show();
	$("#listGridTable").show();
	$("#listGridPager").show();
	$("#listView").dialog({
		resizable : false,
		autoOpen : false,
		height : 245,
		width : 600,
		position : 'center',
		modal : true,
		buttons : {

		}
	});
	
	$(".ui-dialog-titlebar-close").show();
	$(".ui-dialog-title").text('');

	if (forLabel == 'split1_input1_label') {
		$(".ui-dialog-title").text('Select Application');
		$("#listGridTable").jqGrid({
			datatype:"local",
			colModel:[ 
			          {
			        	  name : 'applications',
			        	  label : 'Applications',
			        	    width:"10px",
			        	    align:"left"
			          }, 
			          ],
			          onSelectRow: function ()
			          {
			        	  var myGrid = $('#listGridTable');
			        	  selRowId = myGrid.jqGrid ('getGridParam', 'selrow');
			        	  celvalue1= myGrid.jqGrid ('getCell', selRowId, 'applications');
			        	 $("#split1_input1").val(celvalue1);
			        	 $("#listView").dialog("close");
			          },
			        
			          pager : '#listGridPager',
			          rowNum : 10,
			          rowList : [ 10, 20, 30 ],
			          viewrecords : true,
			          rownumWidth : '5px',
			          loadonce: true,
			          gridview : true,
			          width : 600,
			          height: 140
		  });	
				var data =  [
				             { "applications": "CampusNext"},
				             { "applications": "FnA"},
				             { "applications": "PnP"},
				             { "applications": "Shiksha Cloud"},
			             ];

			 for (var i = 0; i < data.length; i++)
			 { 
			 	$("#listGridTable").jqGrid('addRowData', i + 1, data[i]);
			 }
			 jQuery("#listGridTable").jqGrid ('setLabel', 'Applications', '', {'text-align':'left'});
		$("#listView").dialog("open");
	} else if (forLabel == 'split1_input2_label') {
		$(".ui-dialog-title").text('Select Server');
		$('#listGridTable').jqGrid('GridUnload');
		$("#listGridTable").show();
		$("#listGridPager").show();
		$("#listView").dialog({
		    resizable: false,
		    autoOpen: false,
		    height:245,
		    width:700,
		    position:  'center',
		    modal: true,
		    buttons: {
	        }
			});
		$("#listGridTable").jqGrid({
			datatype:"local",
			colNames:['Servers'],
			colModel:[ 
			          {
			        	  name : 'server',
			        	  label : 'Servers',
			        	    width:"10px"
			          }, 
			          ],
			          onSelectRow: function ()
			          {
			        	  var myGrid = $('#listGridTable');
			        	  selRowId = myGrid.jqGrid ('getGridParam', 'selrow');
			        	  celvalue1= myGrid.jqGrid ('getCell', selRowId, 'server');
			        	 $("#split1_input2").val(celvalue1);
			        	 $("#listView").dialog("close");
			          },
			        
			          pager : '#listGridPager',
			          rowNum : 10,
			          rowList : [ 10, 20, 30 ],
			          viewrecords : true,
			          loadonce: true,
			          width : 600,
			          height: 140
		  });	
				var data =  [
							{ "server": "INFARSCZ90433"},
							/*{ "server": "172.25.38.161"},
							{ "server": "172.25.34.93"},
							{ "server": "172.25.38.49"},
							{ "server": "172.25.35.148"},
							{ "server": "172.17.88.164"},
							{ "server": "10.101.3.18"}*/
			             ];
			 for (var i = 0; i < data.length; i++)
			 { 
			 	$("#listGridTable").jqGrid('addRowData', i + 1, data[i]);
			 }
		$("#listView").dialog("open");
	}
}
