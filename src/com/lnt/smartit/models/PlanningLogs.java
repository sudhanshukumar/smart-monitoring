package com.lnt.smartit.models;

public class PlanningLogs {
	
	private String Date;
	private String epmhsp;
	private String notification;
	private String hsp;
	private String olap;
	private String tid;
	private String ecid;
	private String src_class;
	private String SRC_METHOD;
	private String cause_message;
	private String java_error;
	   
    public String getDate() {
	    return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getEpmhsp() {
		return epmhsp;
	}
	public void setEpmhsp(String epmhsp) {
		this.epmhsp = epmhsp;
	}
	public String getNotification() {
		return notification;
	}
	public void setNotification(String notification) {
		this.notification = notification;
	}
	public String getHsp() {
		return hsp;
	}
	public void setHsp(String hsp) {
		this.hsp = hsp;
	}
	public String getOlap() {
		return olap;
	}
	public void setOlap(String olap) {
		this.olap = olap;
	}
	public String getTid() {
		return tid;
	}
	public void setTid(String tid) {
		this.tid = tid;
	}
	public String getEcid() {
		return ecid;
	}
	public void setEcid(String ecid) {
		this.ecid = ecid;
	}
	public String getSrc_class() {
		return src_class;
	}
	public void setSrc_class(String src_class) {
		this.src_class = src_class;
	}
	public String getSRC_METHOD() {
		return SRC_METHOD;
	}
	public void setSRC_METHOD(String sRC_METHOD) {
		SRC_METHOD = sRC_METHOD;
	}
	public String getCause_message() {
		return cause_message;
	}
	public void setCause_message(String cause_message) {
		this.cause_message = cause_message;
	}
	public String getJava_error() {
		return java_error;
	}
	public void setJava_error(String java_error) {
		this.java_error = java_error;
	}
	@Override
	public String toString() {
		return "PlanningLogs [Date=" + Date + ", epmhsp=" + epmhsp
				+ ", notification=" + notification + ", hsp=" + hsp + ", olap="
				+ olap + ", tid=" + tid + ", ecid=" + ecid + ", src_class="
				+ src_class + ", SRC_METHOD=" + SRC_METHOD + ", cause_message="
				+ cause_message + ", java_error=" + java_error + "]";
	}
}
