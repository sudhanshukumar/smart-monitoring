package com.lnt.smartit.mysql.model;
import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="report_metadata")
public class ReportMetadata implements Serializable {
	private static final long serialVersionUID = 1L;
	@GeneratedValue
	@Id
	@Column(name="id")
	private String id;
	
	@ManyToOne
	@JoinColumn(name="report_id", referencedColumnName = "report_id")
	private Report reportId;
	

	@Column(name="field_name")
	private String fieldName;


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public Report getReportId() {
		return reportId;
	}


	public void setReportId(Report reportId) {
		this.reportId = reportId;
	}


	

	


	public String getFieldName() {
		return fieldName;
	}


	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}


	@Override
	public String toString() {
		return "ReportMetadata [id=" + id + ", reportId=" + reportId
				+ ", fieldName=" + fieldName + "]";
	}
	
	
	
}
