package com.lnt.smartit.models;

public class MachineLogs {
	
	private String Input_date;
	private String time_point;
	private String CPU01_User;
	private String CPU01_Sys;
	private String CPU01_Wait;
	private String CPU01_Idle;
	
	public String getInput_date() {
		return Input_date;
	}
	public void setInput_date(String input_date) {
		Input_date = input_date;
	}
	public String getTime_point() {
		return time_point;
	}
	public void setTime_point(String time_point) {
		this.time_point = time_point;
	}
	public String getCPU01_User() {
		return CPU01_User;
	}
	public void setCPU01_User(String cPU01_User) {
		CPU01_User = cPU01_User;
	}
	public String getCPU01_Sys() {
		return CPU01_Sys;
	}
	public void setCPU01_Sys(String cPU01_Sys) {
		CPU01_Sys = cPU01_Sys;
	}
	public String getCPU01_Wait() {
		return CPU01_Wait;
	}
	public void setCPU01_Wait(String cPU01_Wait) {
		CPU01_Wait = cPU01_Wait;
	}
	public String getCPU01_Idle() {
		return CPU01_Idle;
	}
	public void setCPU01_Idle(String cPU01_Idle) {
		CPU01_Idle = cPU01_Idle;
	}
	@Override
	public String toString() {
		return "MachineLogs [Input_date=" + Input_date + ", time_point="
				+ time_point + ", CPU01_User=" + CPU01_User + ", CPU01_Sys="
				+ CPU01_Sys + ", CPU01_Wait=" + CPU01_Wait + ", CPU01_Idle="
				+ CPU01_Idle + "]";
	}
}
