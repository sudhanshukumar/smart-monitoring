package com.lnt.smartit.mysql.dao;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

@Service
public interface DashboardDataDao {
	public List<?> createQuery(String strQuery,HashMap<String, Object> namedParameters);
	public Serializable save(Serializable entity);
	public void persist(Serializable entity);
	public void saveOrUpdate(Serializable entity);
	public Serializable merge(Serializable entity);
	public void update(Serializable entity);
	public void flush();
	public void evict(Serializable entity);
	public Object load(Class<? extends Serializable> clazz,Serializable entity);
	public void delete(Serializable entity);
	public Criteria createCriteria(Class<? extends Serializable> clazz); 
}
