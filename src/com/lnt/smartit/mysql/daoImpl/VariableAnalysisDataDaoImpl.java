package com.lnt.smartit.mysql.daoImpl;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import com.lnt.smartit.constants.QueryConstants;
import com.lnt.smartit.mysql.dao.VariableAnalysisDataDao;
import com.lnt.smartit.mysql.model.ApplicationMaster;
import com.lnt.smartit.mysql.model.DeviceMaster;
import com.lnt.smartit.mysql.model.VariableAnalysis;

@Repository
@Transactional
public class VariableAnalysisDataDaoImpl implements VariableAnalysisDataDao{

	@Autowired
	private SessionFactory sessionFactory;
	private static Logger logger = Logger.getLogger(VariableAnalysisDataDaoImpl.class);

	//for plotting the historic response time graph in split2
	@SuppressWarnings("unchecked")
	public List<VariableAnalysis> drawHistoricGraphForResponseTime(String page,String appName) {
		/*List<VariableAnalysis> historicGraphlistVariableAnalysis = new ArrayList<VariableAnalysis>();
		Calendar cal = Calendar.getInstance();
		  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		  TimeZone gmtTime = TimeZone.getTimeZone("GMT");
		  sdf.setTimeZone(gmtTime);	    
	      String s = sdf.format(new Date());
	      Date date;
	      Double avgRamUsage = 0.0;
	      Double avgCpuUsage = 0.0;
	      QueryBuilder queryBuilder = null;
	      String hostname = null;
	      String old = null;
			try {
				date = sdf.parse(s);
			
			cal.setTime(date);
			cal.add(Calendar.HOUR_OF_DAY, -1);	
			old = sdf.format(cal.getTime());
			System.out.println("Old Date"+old);
			System.out.println("Current"+s);
			
			
			queryBuilder = QueryBuilders.boolQuery()
			        .must(QueryBuilders.termQuery("hostname.keyword", "INFARSZC90433"))
					.filter(QueryBuilders.rangeQuery("LogDate").from(old).to(s));
			
			SearchRequestBuilder searchRequestBuilder = getClient().prepareSearch()
		            .setIndices("app-url-status").setSize(100)
		           .setPostFilter(queryBuilder)
					.addSort(SortBuilders.fieldSort("LogDate").order(SortOrder.DESC));
		    SearchResponse response = searchRequestBuilder.get();
		    SearchHits searchHitsNew = response.getHits();
		    Iterator it = searchHitsNew.iterator();
		    while(it.hasNext()){
		    	VariableAnalysis obj_VariableAnalysis = new VariableAnalysis();
		    	SearchHit hit = (SearchHit) it.next();
	    		Map<String, Object> logMap= hit.sourceAsMap();
	    		obj_VariableAnalysis.setDate();
	    		obj_VariableAnalysis.setResponseTime(Double.parseDouble(logMap.get("Responsetime-ms").toString()));
		    }*/
		
		logger.debug("Inside variable analysis data dao for historic graphs");
		Session session = sessionFactory.getCurrentSession();
		SQLQuery historicGraphForResponseTime = session.createSQLQuery(QueryConstants.historic_response_time_graph);
		historicGraphForResponseTime.setParameter(QueryConstants.page, page);
		logger.debug("RESPONSE TIME QUERY = " + historicGraphForResponseTime.getQueryString());
		List<Object[]> historicGraphForResponseTimeList = historicGraphForResponseTime.list();
		List<VariableAnalysis> historicGraphlistVariableAnalysis = new ArrayList<VariableAnalysis>();
		for (Object[] object : historicGraphForResponseTimeList) {
			VariableAnalysis obj_VariableAnalysis = new VariableAnalysis();
//			Double count=((BigInteger) object[1]).doubleValue();
			obj_VariableAnalysis.setDate((String) object[0]);
//			obj_VariableAnalysis.setCount(count);
			obj_VariableAnalysis.setResponseTime((Double) (object[2]));
			obj_VariableAnalysis.setDay(Integer.parseInt((String) object[3]));
			obj_VariableAnalysis.setMonth(Integer.parseInt((String) object[4]));
			obj_VariableAnalysis.setYear(Integer.parseInt((String) object[5]));
			obj_VariableAnalysis.setHour(Integer.parseInt((String) object[6]));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			TimeZone timeZone=TimeZone.getTimeZone("IST");
			SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy H");
			s.setTimeZone(timeZone);
			Date d;
			try {
				d = s.parse((String) object[0]);
				obj_VariableAnalysis.setEpochTime((d.getTime() + 1 * 24 * 60 * 60 * 1000)+19800);
				obj_VariableAnalysis.setEpochTime(d.getTime());
				} catch (ParseException e) {
						e.printStackTrace();
				}
			historicGraphlistVariableAnalysis.add(obj_VariableAnalysis);
		}
		return historicGraphlistVariableAnalysis;
	}
	
	//for plotting the prediction for response time  
	@SuppressWarnings("unchecked")
	public List<VariableAnalysis> drawResponseTimePredictionGraphs(String page) {
		logger.debug("Inside variable analysis data dao for prediction graphs");		
		Session session = sessionFactory.getCurrentSession();
		SQLQuery responseTimepredictionQuery = session.createSQLQuery(QueryConstants.prediction_response_time_graph);
		responseTimepredictionQuery.setParameter(QueryConstants.page, page);
		List<Object[]> responseTimepredictionQueryList = responseTimepredictionQuery.list();
		List<VariableAnalysis> predictionGraphList = new ArrayList<VariableAnalysis>();
		for (Object[] object : responseTimepredictionQueryList) {
			VariableAnalysis obj_VariableAnalysis = new VariableAnalysis();
			obj_VariableAnalysis.setDate((String) object[0]);
			obj_VariableAnalysis.setActualValues((Double) object[1]);
			obj_VariableAnalysis.setPredictedValues((Double) object[2]);
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy");
			Date d;
			try {
				d = s.parse((String) object[0]);
				obj_VariableAnalysis.setEpochTime(d.getTime() + 1 * 24 * 60
						* 60 * 1000);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			predictionGraphList.add(obj_VariableAnalysis);
		}
			return predictionGraphList;
	}

	//for fetching the application list while autopopulating
	@SuppressWarnings("unchecked")
	public List<ApplicationMaster> fetchApplicationList() {
		Session session = sessionFactory.getCurrentSession();
		SQLQuery fetchApplicationList = session.createSQLQuery(QueryConstants.fetchApplicationList);
		List<ApplicationMaster> fetchingapplicationList = fetchApplicationList.list();
		return fetchingapplicationList;
	}

	//for fetching the server list through autopopulating
	@SuppressWarnings("unchecked")
	public List<DeviceMaster> fetchServerList() {
		Session session = sessionFactory.getCurrentSession();
		SQLQuery fetchServerList = session.createSQLQuery(QueryConstants.fetchDeviceList);
		List<DeviceMaster> fetchingServerList = fetchServerList.list();
		return fetchingServerList;
	}
	
	//for fetching application list for the grid 
	@SuppressWarnings("unchecked")
	public List<ApplicationMaster> fetchApplicationListForGrid() {
		Session session = sessionFactory.getCurrentSession();
		Query fetchApplicationList = session.createSQLQuery(QueryConstants.fetchApplicationListGrid);
		List<Object[]> fetchApplicationListForGrid = fetchApplicationList.list();
		List<ApplicationMaster> fetchModifiedApplicationListForGrid = new ArrayList<ApplicationMaster>();
		for (int i = 0; i < fetchApplicationListForGrid.size(); i++) {
			ApplicationMaster applicationMaster = new ApplicationMaster();
			applicationMaster.setApplicationName(fetchApplicationListForGrid.get(i)[0].toString());
			fetchModifiedApplicationListForGrid.add(applicationMaster);
		}
		return fetchModifiedApplicationListForGrid;
	}


	//plot related variables chart for CPU
	@SuppressWarnings("unchecked")
	public List<VariableAnalysis> plotRelatedVariablesChartCPU(String serverIPAddress) {
		logger.debug("server IP in daoimpl for cpu vs responsetime"+serverIPAddress);
		Session session = sessionFactory.getCurrentSession();
		SQLQuery responseVsCPUQuery = session.createSQLQuery(QueryConstants.responseTimeVsCPU_graph);
		responseVsCPUQuery.setParameter(QueryConstants.server_ip_address, serverIPAddress);
		List<Object[]> responseVsCPUQueryList = responseVsCPUQuery.list();
		List<VariableAnalysis> responseVsCPUList = new ArrayList<VariableAnalysis>();
		for (Object[] cpuObject : responseVsCPUQueryList) {
			VariableAnalysis obj_VariableAnalysis = new VariableAnalysis();
			obj_VariableAnalysis.setDate((String)cpuObject[0]);
			obj_VariableAnalysis.setFiveMinCPUAvg((Double)cpuObject[1]);
			obj_VariableAnalysis.setTenMinCPUAvg((Double)cpuObject[2]);
			obj_VariableAnalysis.setFifteenMinCPUAvg((Double)cpuObject[3]);
			obj_VariableAnalysis.setDay(Integer.parseInt((String) cpuObject[4]));
			obj_VariableAnalysis.setMonth(Integer.parseInt((String) cpuObject[5]));
			obj_VariableAnalysis.setYear(Integer.parseInt((String) cpuObject[6]));
			obj_VariableAnalysis.setHour(Integer.parseInt((String) cpuObject[7]));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy");
			Date d;
			try {
				d = s.parse((String) cpuObject[0]);
				obj_VariableAnalysis.setEpochTime(d.getTime() + 1 * 24 * 60	* 60 * 1000);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			responseVsCPUList.add(obj_VariableAnalysis);
		}
		
		return responseVsCPUList;	
	}

	
	//plot related variables chart for RAM
	@SuppressWarnings("unchecked")
	public List<VariableAnalysis> plotRelatedVariablesChartRAM(String serverIPAddress) {
		
		logger.debug("inside daoimpl for RAM vs response time graph where server IP"+serverIPAddress);
		
		
		Session session = sessionFactory.getCurrentSession();

		SQLQuery responseVsRAMQuery = session
				.createSQLQuery(QueryConstants.responseTimeVsRAM_graph);
		responseVsRAMQuery.setParameter(QueryConstants.server_ip_address, serverIPAddress);
		
		
		List<Object[]> responseVsRAMQueryList = responseVsRAMQuery.list();
		List<VariableAnalysis> responseVsRAMList = new ArrayList<VariableAnalysis>();
		
		for (Object[] ramObject : responseVsRAMQueryList) {
			VariableAnalysis obj_VariableAnalysis = new VariableAnalysis();

			
			obj_VariableAnalysis.setDate((String)ramObject[0]);
			obj_VariableAnalysis.setAvgRamUsed((Double)ramObject[1]);
			
			obj_VariableAnalysis.setDay(Integer.parseInt((String) ramObject[2]));
			obj_VariableAnalysis.setMonth(Integer.parseInt((String) ramObject[3]));
			obj_VariableAnalysis.setYear(Integer.parseInt((String) ramObject[4]));
			obj_VariableAnalysis.setHour(Integer.parseInt((String) ramObject[5]));
		
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy");
			Date d;
			try {
				d = s.parse((String) ramObject[0]);
				obj_VariableAnalysis.setEpochTime(d.getTime() + 1 * 24 * 60
						* 60 * 1000);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			responseVsRAMList.add(obj_VariableAnalysis);
		}
		
		return responseVsRAMList;
	}
	
	
	//plot related variables chart for IO
	@SuppressWarnings("unchecked")
	public List<VariableAnalysis> plotRelatedVariablesChartIO(String serverIPAddress) {
		logger.debug("inside daoimpl for IO vs response time graph");
		Session session = sessionFactory.getCurrentSession();
		SQLQuery responseVsMemoryQuery = session.createSQLQuery(QueryConstants.responseTimeVsIO_graph);
		responseVsMemoryQuery.setParameter(QueryConstants.server_ip_address, serverIPAddress);
		List<Object[]> responseVsMemoryQueryList = responseVsMemoryQuery.list();
		List<VariableAnalysis> responseVsIoList = new ArrayList<VariableAnalysis>();
		for (Object[] ioObject : responseVsMemoryQueryList) {
			VariableAnalysis obj_VariableAnalysis = new VariableAnalysis();
			obj_VariableAnalysis.setDate((String)ioObject[0]);
			obj_VariableAnalysis.setIowait((Double)ioObject[1]);
			obj_VariableAnalysis.setDay(Integer.parseInt((String) ioObject[2]));
			obj_VariableAnalysis.setMonth(Integer.parseInt((String) ioObject[3]));
			obj_VariableAnalysis.setYear(Integer.parseInt((String) ioObject[4]));
			obj_VariableAnalysis.setHour(Integer.parseInt((String) ioObject[5]));
			responseVsIoList.add(obj_VariableAnalysis);
		}
		return responseVsIoList;
	}

	//plot correlation charts for CPU
	@SuppressWarnings("unchecked")
	public List<VariableAnalysis> plotCorrelatedVariablesChartCPU(VariableAnalysis variableAnalysis) {
		Integer day = variableAnalysis.getStartDate();
		Integer month = variableAnalysis.getStartMonth();
		Integer year = variableAnalysis.getStartYear();
		Integer end_day = variableAnalysis.getEndDate();
		Integer end_month = variableAnalysis.getEndMonth();
		Integer end_year = variableAnalysis.getEndYear();
		logger.debug("end date ::::"+end_day+"end month:: "+end_month+"end_year:::"+end_year);
		Session session = sessionFactory.getCurrentSession();
		String qryMonth="";
		String qrydate="";
		if(month<10){
			qryMonth="0" + month;
		}else{
			qryMonth=month.toString();
		}
		if(day<10){
			qrydate="0" + day;
		}else{
			qrydate=day.toString();
		}
		  // end of start date parsing
		String qryEndMonth="";
		String qryEnddate="";
		if(end_month<10){
			qryEndMonth="0" + end_month;
		}else{
			qryEndMonth=end_month.toString();
		}
		if(end_day<10){
			qryEnddate="0" + end_day;
		}else{
			qryEnddate=end_day.toString();
		}
		// end of end date parsing
		SQLQuery responseVsCPUQueryCorelated = session
				.createSQLQuery("SELECT DATE_FORMAT(storeDateTime,'%d-%m-%Y'),avg(fiveminloadavg),avg(tenminloadavg),avg(fifteenminloadavg) , DATE_FORMAT(storeDateTime,'%d'), DATE_FORMAT(storeDateTime,'%m'),DATE_FORMAT(storeDateTime,'%Y'),DATE_FORMAT(storeDateTime,'%H') FROM raw_cpu_data r where ipaddress='"
						+ variableAnalysis.getServerIPAddress()
						+ "' and storeDateTime BETWEEN '"+year+"-"+qryMonth+"-"+qrydate+" 00:00:00' AND '"+end_year+"-"+qryEndMonth+"-"+qryEnddate+" 00:00:00' group by DATE_FORMAT(storeDateTime,'%d-%m-%Y %H') order by DATE_FORMAT(storeDateTime,'%Y-%m-%d %H')");
		logger.debug("SELECT DATE_FORMAT(storeDateTime,'%d-%m-%Y'),avg(fiveminloadavg),avg(tenminloadavg),avg(fifteenminloadavg) , DATE_FORMAT(storeDateTime,'%d'), DATE_FORMAT(storeDateTime,'%m'),DATE_FORMAT(storeDateTime,'%Y'),DATE_FORMAT(storeDateTime,'%H') FROM raw_cpu_data r where ipaddress='"
				+ variableAnalysis.getServerIPAddress()
				+ "' and storeDateTime > '"+year+"-"+qryMonth+"-"+qrydate+" 00:00:00' group by DATE_FORMAT(storeDateTime,'%d-%m-%Y %H') order by DATE_FORMAT(storeDateTime,'%Y-%m-%d %H')");
		List<Object[]> responseVsCPUQueryListcorelated = responseVsCPUQueryCorelated.list();
		List<VariableAnalysis> responseVsCPUListCorelated = new ArrayList<VariableAnalysis>();
		for (Object[] cpuObject : responseVsCPUQueryListcorelated) {
			VariableAnalysis obj_VariableAnalysis = new VariableAnalysis();
			obj_VariableAnalysis.setDate((String)cpuObject[0]);
			obj_VariableAnalysis.setFiveMinCPUAvg((Double)cpuObject[1]);
			obj_VariableAnalysis.setTenMinCPUAvg((Double)cpuObject[2]);
			obj_VariableAnalysis.setFifteenMinCPUAvg((Double)cpuObject[3]);
			obj_VariableAnalysis.setDay(Integer.parseInt((String) cpuObject[4]));
			obj_VariableAnalysis.setMonth(Integer.parseInt((String) cpuObject[5]));
			obj_VariableAnalysis.setYear(Integer.parseInt((String) cpuObject[6]));
			obj_VariableAnalysis.setHour(Integer.parseInt((String) cpuObject[7]));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy");
			Date d;
			try {
				d = s.parse((String) cpuObject[0]);
				obj_VariableAnalysis.setEpochTime(d.getTime() + 1 * 24 * 60
						* 60 * 1000);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			responseVsCPUListCorelated.add(obj_VariableAnalysis);
		}
		return responseVsCPUListCorelated;	
	}


	//plot corelation charts for RAM
	@SuppressWarnings("unchecked")
	public List<VariableAnalysis> plotCorrelatedVariablesChartRAM(VariableAnalysis variableAnalysis) {
		Integer day = variableAnalysis.getStartDate();
		Integer month = variableAnalysis.getStartMonth();
		Integer year = variableAnalysis.getStartYear();
		Integer end_day = variableAnalysis.getEndDate();
		Integer end_month = variableAnalysis.getEndMonth();
		logger.debug("end month for correlation>>>>>>>>>>>>>>>>>> "+end_month);
		Integer end_year = variableAnalysis.getEndYear();
		Session session = sessionFactory.getCurrentSession();
		String qryMonth="";
		String qrydate="";
		if(month<10){
			qryMonth="0" + month;
		}else{
			qryMonth=month.toString();
		}
		if(day<10){
			qrydate="0" + day;
		}else{
			qrydate=day.toString();
		}
		// end of start date parsing
		String qryEndMonth="";
		String qryEnddate="";
		if(end_month<10){
			qryEndMonth="0" + end_month;
		}else{
			logger.debug("month value greater than 12");
			qryEndMonth=end_month.toString();
		}
		if(end_day<10){
			qryEnddate="0" + end_day;
		}else{
			qryEnddate=end_day.toString();
		}
		// end of end date parsing
		SQLQuery responseVsRAMQuery = session
				.createSQLQuery("SELECT DATE_FORMAT(machinedatareceivable,'%d-%m-%Y'),avg(rammemused), DATE_FORMAT(machinedatareceivable,'%d'), DATE_FORMAT(machinedatareceivable,'%m'),DATE_FORMAT(machinedatareceivable,'%Y'),DATE_FORMAT(machinedatareceivable,'%H') FROM raw_ram_data r where ipaddress='"
				+ variableAnalysis.getServerIPAddress()
				+ "' and machinedatareceivable  BETWEEN '"+year+"-"+qryMonth+"-"+qrydate+" 00:00:00' AND '"+end_year+"-"+qryEndMonth+"-"+qryEnddate+" 00:00:00' group by DATE_FORMAT(machinedatareceivable,'%d-%m-%Y %H') order by DATE_FORMAT(machinedatareceivable,'%Y-%m-%d %H')");
		logger.debug("SELECT DATE_FORMAT(machinedatareceivable,'%d-%m-%Y'),avg(rammemused), DATE_FORMAT(machinedatareceivable,'%d'), DATE_FORMAT(machinedatareceivable,'%m'),DATE_FORMAT(machinedatareceivable,'%Y'),DATE_FORMAT(machinedatareceivable,'%H') FROM raw_ram_data r where ipaddress='"
				+ variableAnalysis.getServerIPAddress()
				+ "' and machinedatareceivable  BETWEEN '"+year+"-"+qryMonth+"-"+qrydate+" 00:00:00' AND '"+end_year+"-"+qryEndMonth+"-"+qryEnddate+" 00:00:00' group by DATE_FORMAT(machinedatareceivable,'%d-%m-%Y %H') order by DATE_FORMAT(machinedatareceivable,'%Y-%m-%d %H')");
		
		List<Object[]> responseVsRAMQueryList = responseVsRAMQuery.list();
		logger.debug("correlated list sizze for RM "+responseVsRAMQuery.list());
		List<VariableAnalysis> responseVsRAMList = new ArrayList<VariableAnalysis>();
		for (Object[] ramObject : responseVsRAMQueryList) {
			VariableAnalysis obj_VariableAnalysis = new VariableAnalysis();
			obj_VariableAnalysis.setDate((String)ramObject[0]);
			obj_VariableAnalysis.setAvgRamUsed((Double)ramObject[1]);
			obj_VariableAnalysis.setDay(Integer.parseInt((String) ramObject[2]));
			obj_VariableAnalysis.setMonth(Integer.parseInt((String) ramObject[3]));
			obj_VariableAnalysis.setYear(Integer.parseInt((String) ramObject[4]));
			obj_VariableAnalysis.setHour(Integer.parseInt((String) ramObject[5]));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy");
			Date d;
			try {
				d = s.parse((String) ramObject[0]);
				obj_VariableAnalysis.setEpochTime(d.getTime() + 1 * 24 * 60 * 60 * 1000);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			responseVsRAMList.add(obj_VariableAnalysis);
		}
		return responseVsRAMList;
	}

	//plot corelation charts for IO
	@SuppressWarnings("unchecked")
	public List<VariableAnalysis> plotCorrelatedVariablesChartIO(VariableAnalysis variableAnalysis) {
		Integer day = variableAnalysis.getStartDate();
		Integer month = variableAnalysis.getStartMonth();
		Integer year = variableAnalysis.getStartYear();
		Integer end_day = variableAnalysis.getEndDate();
		Integer end_month = variableAnalysis.getEndMonth();
		Integer end_year = variableAnalysis.getEndYear();
		logger.debug("month for correlation : "+month);
		Session session = sessionFactory.getCurrentSession();
		String qryMonth="";
		String qrydate="";
		if(month<10){
			qryMonth="0" + month;
		}else{
			qryMonth=month.toString();
		}
		if(day<10){
			qrydate="0" + day;
		}else{
			qrydate=day.toString();
		}
		// end of start date parsing
				String qryEndMonth="";
				String qryEnddate="";
				if(end_month<10){
					qryEndMonth="0" + end_month;
				}else{
					qryEndMonth=end_month.toString();
				}
				if(end_day<10){
					qryEnddate="0" + end_day;
				}else{
					qryEnddate=end_day.toString();
				}
				// end of end date parsing
		
		SQLQuery responseVsIOQueryCorelated = session.createSQLQuery("SELECT DATE_FORMAT(storeDateTime,'%d-%m-%Y'),avg(iowait), DATE_FORMAT(storeDateTime,'%d'), DATE_FORMAT(storeDateTime,'%m'),DATE_FORMAT(storeDateTime,'%Y'),DATE_FORMAT(storeDateTime,'%H') FROM raw_io_data r where ipaddress='172.25.38.49' and storeDateTime  BETWEEN '"+year+"-"+qryMonth+"-"+qrydate+" 00:00:00' AND '"+end_year+"-"+qryEndMonth+"-"+qryEnddate+" 00:00:00' group by DATE_FORMAT(storeDateTime,'%d-%m-%Y %H')  order by DATE_FORMAT(storeDateTime,'%Y-%m-%d %H');");
		List<Object[]> responseVsIOQueryCorelatedList = responseVsIOQueryCorelated.list();
		logger.debug("list size for IO data "+responseVsIOQueryCorelatedList.size());
		List<VariableAnalysis> responseVsIOListcorelated = new ArrayList<VariableAnalysis>();
		for (Object[] ioObject : responseVsIOQueryCorelatedList) {
			VariableAnalysis obj_VariableAnalysis = new VariableAnalysis();
			obj_VariableAnalysis.setDate((String)ioObject[0]);
			obj_VariableAnalysis.setIowait((Double)ioObject[1]);
			obj_VariableAnalysis.setDay(Integer.parseInt((String) ioObject[2]));
			obj_VariableAnalysis.setMonth(Integer.parseInt((String) ioObject[3]));
			obj_VariableAnalysis.setYear(Integer.parseInt((String) ioObject[4]));
			obj_VariableAnalysis.setHour(Integer.parseInt((String) ioObject[5]));
			responseVsIOListcorelated.add(obj_VariableAnalysis);
		}
		return responseVsIOListcorelated;
		}
	
	public TransportClient getClient(){
		TransportClient client = null;
		String elasticSearchLoc=null;
		try {
			elasticSearchLoc = "AIRINMSADWB";
			client = new PreBuiltTransportClient(Settings.EMPTY).addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(elasticSearchLoc),9300));			
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		return client;
	}
	
	public String getAdditionalInfoForGraphs(String parameterName/*,String addressedMachine, String respAppName, String respPageName,String osType,String timerange,String xAxis,String yAxis*/) {
		StringBuffer jsonData = new StringBuffer();
		InputStream paramPropertiesStream= null;
		String xAxisValue=null;
		String yAxisValue2=null;
		String yAxisValue1=null;
		String indexName=null;
		String timeFrame=null;
		String timeFrame2=null;
		
		try {
			if(parameterName.equals("DISK") || parameterName.equals("CPU") || parameterName.equals("RAM")){
				/*if(osType.equalsIgnoreCase("windows")){
					parameterName="WIN"+parameterName;
				}*/
		}
			Properties props = new Properties();
			paramPropertiesStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("params.properties");
			props.load(paramPropertiesStream);
			
			xAxisValue = props.getProperty("XAxisPrediction").toString();
			yAxisValue1 = props.getProperty("YAxisActual").toString();
			yAxisValue2=props.getProperty("YAxisPrediction").toString();
			indexName=props.getProperty("indexNameForPrediction").toString();
			timeFrame=props.getProperty("Hours").toString();
			timeFrame2=props.getProperty("HoursAfter").toString();
			
			Map<String, String> params = new HashMap<String, String>();
		    params.put("parameterName",parameterName);
		    /*params.put("mapName",addressedMachine);
		    params.put("timerange",timerange);*/
		   // params.put("xAxis",xAxisValue);
		    System.out.println("hello");
			TransportClient client = null;
			String elasticSearchLoc=null;
			try {
				elasticSearchLoc = props.getProperty("machine.name");
				 client = new PreBuiltTransportClient(Settings.EMPTY).addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(elasticSearchLoc),9300));
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
			//For getting time hrs before the current time
			Calendar cal = Calendar.getInstance();
			  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		      String s = sdf.format(new Date());
		      Date date=sdf.parse(s);
			cal.setTime(date);
			cal.add(Calendar.HOUR_OF_DAY, Integer.parseInt(timeFrame)*-1);;
			String old = sdf.format(cal.getTime());
			System.out.println(old);
			Date oldDate=sdf.parse(old);
			
			Calendar cal2=Calendar.getInstance();
			cal2.setTime(date);
			cal2.add(Calendar.HOUR_OF_DAY, Integer.parseInt(timeFrame2));
			String dateAhead=sdf.format(cal2.getTime());
			//for getting the size of internal search hits dynamically
			SearchRequestBuilder searchRequestBuilder = client.prepareSearch()
		            .setIndices(indexName)
		            .setQuery(QueryBuilders.rangeQuery(xAxisValue).from(old).to(dateAhead));
		    SearchResponse response = searchRequestBuilder.get();
		    SearchHits searchHits = response.getHits();
		    Long size=searchHits.getTotalHits();
		    //For getting the data from elasticsearch
		    SearchRequestBuilder searchRequestBuilderNew =  client.prepareSearch()
		            .setIndices(indexName)
		            .setSize(size.intValue())
		            .setQuery(QueryBuilders.rangeQuery(xAxisValue).from(old).to(dateAhead));
			System.out.println(searchRequestBuilderNew);
		    SearchResponse responseNew = searchRequestBuilderNew.get();
		    System.out.println("Response ==  "+responseNew);
		    SearchHits searchHitsNew = responseNew.getHits();
		    ObjectMapper mapper = new ObjectMapper();
		    StringBuffer objJson=new StringBuffer();
		    List<Map<String,Object>> logsList=new ArrayList<Map<String,Object>>();
		    try {
		    	int count=0;
		    	for (SearchHit hit : responseNew.getHits()) {
		    		Map<String,Object> axesMap=new HashMap<String, Object>();
		    		if(hit.getSource().get("parameter_name").equals(parameterName)){
		            axesMap.put("xaxis", hit.getSource().get(xAxisValue));
		            axesMap.put("yaxis1", hit.getSource().get(yAxisValue1));
		            axesMap.put("yaxis2", hit.getSource().get(yAxisValue2));
		           logsList.add(axesMap);
		    		}
		        }
		    //to get data only for the past 12 hours
		    objJson.append(mapper.writeValueAsString(logsList));
		    } catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			jsonData.append(objJson.toString());
			
		} catch (Exception e) {
			System.out.println("ERROR::SmartIT_UX::com.lnt.services::DashboardDataService.getAdditionalInfo():: while connecting to Web Service!!!\n"
					+ e + "\n----------------------------\n");
			
		}
		System.out.println("jsonData.toString()"+jsonData.toString());
		return jsonData.toString();
	}
	}
