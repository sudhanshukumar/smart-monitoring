package com.lnt.smartit.models;

public class ErrorLogs {
	
	String Log_type;
	String IP;
	String cause_message;
	String Date;
	public String getLog_type() {
		return Log_type;
	}
	public void setLog_type(String log_type) {
		Log_type = log_type;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	public String getCause_message() {
		return cause_message;
	}
	public void setCause_message(String cause_message) {
		this.cause_message = cause_message;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
}
