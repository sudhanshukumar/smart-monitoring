package com.lnt.smartit.mysql.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="app_log")
public class AppLogMaster implements java.io.Serializable{
	
	@Id
	@Column(name="id")
	private String logId;
	
	@ManyToOne
	@JoinColumn(name = "app_id", referencedColumnName = "id", insertable = false, updatable = false)
	private Applications app;
	
	@ManyToOne
	@JoinColumn(name = "log_type_code", referencedColumnName = "code", insertable = false, updatable = false)
	private LogTypes logType;
	
	@ManyToOne
	@JoinColumn(name = "app_device_id", referencedColumnName = "id", insertable = false, updatable = false)
	private AppDeviceMaster device;
	
	@Column(name="log_rcvr_host_ip")
	private String hostName;
	
	@Column(name="log_rcvr_topic_port")
	private String logTopicPort;
	
	@Column(name="log_rcvr_fldr_loc")
	private String logFolderLocation;
	
	@Column(name="log_topic_partition")
	private int logTopicPartition;
	
	@Column(name="description")
	private String logDescription;
	
	@Column(name="void_ind")
	private String voidInd;

	public String getLogId() {
		return logId;
	}

	public void setLogId(String logId) {
		this.logId = logId;
	}

	public Applications getApp() {
		return app;
	}

	public void setApp(Applications app) {
		this.app = app;
	}

	public LogTypes getLogType() {
		return logType;
	}

	public void setLogType(LogTypes logType) {
		this.logType = logType;
	}

	public AppDeviceMaster getDevice() {
		return device;
	}

	public void setDevice(AppDeviceMaster device) {
		this.device = device;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getLogTopicPort() {
		return logTopicPort;
	}

	public void setLogTopicPort(String logTopicPort) {
		this.logTopicPort = logTopicPort;
	}

	public String getLogFolderLocation() {
		return logFolderLocation;
	}

	public void setLogFolderLocation(String logFolderLocation) {
		this.logFolderLocation = logFolderLocation;
	}

	public int getLogTopicPartition() {
		return logTopicPartition;
	}

	public void setLogTopicPartition(int logTopicPartition) {
		this.logTopicPartition = logTopicPartition;
	}

	public String getLogDescription() {
		return logDescription;
	}

	public void setLogDescription(String logDescription) {
		this.logDescription = logDescription;
	}

	public String getVoidInd() {
		return voidInd;
	}

	public void setVoidInd(String voidInd) {
		this.voidInd = voidInd;
	}

	@Override
	public String toString() {
		return "AppLogMaster [logId=" + logId + ", app=" + app + ", logType="
				+ logType + ", device=" + device + ", hostName=" + hostName
				+ ", logTopicPort=" + logTopicPort + ", logFolderLocation="
				+ logFolderLocation + ", logTopicPartition="
				+ logTopicPartition + ", logDescription=" + logDescription
				+ ", voidInd=" + voidInd + "]";
	}
	
	
}
