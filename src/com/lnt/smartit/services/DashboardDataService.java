package com.lnt.smartit.services;

import java.io.IOException;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;

public interface DashboardDataService {
	public List<String> getOnLoadContent();
	public String getAdditionalInfo(String parameterName,String addressedMachine, String respAppName,String respPageName,String osType,String timerange,String xAxis,String yAxis);
	public String correlateRespTime(String parameterName,String addressedMachine);
	public String predictResponseTime(String app, String page);
	public String getLastUpdatedRespTime(String param,String app, String page,String limit);
	public String getAdditionalInfoForTrends(String parameterName,String addressedMachine, String respAppName,String respPageName,String osType,String timerange,String xAxis,String yAxis);
	public String getDataForDonutChart() throws IOException;
	public String getDataForDonutChartGrid(String sliceName,String sliceValue) throws JsonGenerationException, JsonMappingException, IOException;
	public String getDataForPieChart() throws IOException;
	public String getDataForMetricChart() throws JsonGenerationException, JsonMappingException, IOException;
	public String getDataMachineInfo() throws JsonGenerationException, JsonMappingException, IOException;
}
