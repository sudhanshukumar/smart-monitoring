package com.lnt.smartit.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.lnt.smartit.mysql.model.Roles;
import com.lnt.smartit.mysql.model.Users;
import com.lnt.smartit.services.UserService;
import com.lnt.smartit.solr.pojo.JqGridData;
@Controller
public class UserController {
@Autowired
private UserService userService;

	@RequestMapping("manageUsers.html")
	public ModelAndView displayManageUserPage(ModelMap map) {
		return new ModelAndView("Manage/manageUsers");
	}
	
	@RequestMapping("populateUsersGrid.html")
	public  @ResponseBody
	JqGridData showExistingUsers(HttpServletRequest request, JqGridData gridPojo) {
		List<Users> usersList = new ArrayList<Users>();
		try {
			usersList = userService.getAllUsers();
			gridPojo.setGridData(usersList.toArray());
			gridPojo.setRows(usersList.size());
			gridPojo.setRecords(usersList.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return gridPojo;
	}
	
	@RequestMapping("user.html")
	public ModelAndView displayAddUserPage(ModelMap map) {
		List<Roles> roleList = new ArrayList<Roles>();
		roleList=userService.findRolesColl();
		map.put("roles", roleList);
		return new ModelAndView("Manage/new/newUser");
	}
	
	@SuppressWarnings("unused")
	@RequestMapping("addUser.html")
	public @ResponseBody String addUser(@Valid Users user,BindingResult bindingResult,HttpSession session, HttpServletRequest request) {
		int userId;
		userId=userService.addNewUser(user);
		return "";
	}
	
	@RequestMapping("updateUser.html")
	public @ResponseBody String updateUser(@Valid Users user,BindingResult bindingResult,
			HttpSession session, HttpServletRequest request) {
		userService.updateUser(user);
		return "User Updated Successfully";
	}
	
	@RequestMapping(value = "previewUser.html", method = RequestMethod.POST)
	public @ResponseBody Users previewUser(Users users,HttpServletRequest request){
		return userService.previewService(users.getUserId());
	}
	
	@RequestMapping("deleteUser.html")
	public @ResponseBody String deleteUser(Users users,HttpServletRequest request) {
		return userService.deleteUser(users.getUserId());
	}
}
