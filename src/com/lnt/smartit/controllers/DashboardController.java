package com.lnt.smartit.controllers;

import java.io.IOException;
import java.util.List;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lnt.smartit.helper.AMQListner;
import com.lnt.smartit.helper.ObserverFactory;
import com.lnt.smartit.observer.QueueSubject;
import com.lnt.smartit.observer.QueueSubjectI;
import com.lnt.smartit.observer.ObserverI;
import com.lnt.smartit.services.DashboardDataService;

@Controller
public class DashboardController {
	private static Logger logger = Logger.getLogger(DashboardController.class);
	@Autowired
	private DashboardDataService service;
	@Autowired
	private ObserverFactory observerFactory;
	@Autowired
	@Qualifier("amqListner")
	private AMQListner amqListner;
	public static ObserverI trailObser;
	public static QueueSubjectI queueSubject = new QueueSubject();

	@RequestMapping(value = "loadDashboard.html")
	public @ResponseBody List<String> loadDashboard() {
		logger.debug("SmartIT_UX::com.lnt.controllers::DashboardController.loadDashboard():: loading last updated dashoboard contents");
		return service.getOnLoadContent();
	}
	
	@RequestMapping(value = "unregisterObserver.html", method = RequestMethod.GET)
	public void unregisterObserver(HttpServletRequest request,HttpServletResponse response) {
		logger.debug("unregister starting..............");
		ObserverI subscriber = observerFactory.getSubscriber();
		subscriber.setName(request.getSession().getId()); // setting unique name
		logger.debug(request.getSession().getId());							
		queueSubject.unRegister(subscriber);
	/*	
		 * This is infinite loop running since we need to keep HTTP connection
		 * established alive, else once response is sent HTTP connection closes
		while (true) {
		}*/
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * This method will register the user and add it to observer list. 
	 * Also once added it is treated as subscriber and has unique name as its session id.
	 * @throws JMSException 
	 * @throws IOException 
	 */
	@RequestMapping(value = "queueListner.html")
	public void queueListner(HttpServletRequest request,HttpServletResponse response) throws JMSException, JSONException, IOException {
		logger.debug("Listen queue starting..............");
		ObserverI subscriber = observerFactory.getSubscriber();
		subscriber.setName(request.getSession().getId()); // setting unique name
															// to all
															// subscribers as
		subscriber.setResponse(response);
		subscriber.setSubject(queueSubject);
		trailObser=subscriber;
		queueSubject.register(subscriber);
		amqListner.recieveMessage();
		/*
		 * This is infinite loop running since we need to keep HTTP connection
		 * established alive, else once response is sent HTTP connection closes
		 */
		while (true) {
		}
	}

	@RequestMapping(value="checkForAlerts.html")
	public @ResponseBody String getAMQMessages(){
		StringBuffer sbf = new StringBuffer();
		List<String> alertList =  service.getOnLoadContent();
		if(alertList.size() != 0)
		sbf.append(alertList.get(0));
		return sbf.toString();
	}
	@RequestMapping(value="additionalInfo.html")
	public @ResponseBody String getAdditionalData(HttpServletRequest request) {
		String addressedMachine = null;
		String respAppName = null;
		String respPageName = null;
		String osType=null;
		String timerange=null;
		String xAxis=null;
		String yAxis=null;
	    logger.debug("SmartIT_UX::com.lnt.controllers::DashboardController.getAdditionalData():: called");
		String parameterName = request.getParameter("parameterName").toString();
		if(request.getParameter("addressedMachine") != null){
		 addressedMachine = request.getParameter("addressedMachine").toString();
		} if(request.getParameter("urlName") != null){
			respAppName = request.getParameter("urlName");
		}if(request.getParameter("page") != null){
			respPageName = request.getParameter("page");
		}if(request.getParameter("osType") != null){
			osType = request.getParameter("osType").toString();
		}if(request.getParameter("timerange")!=null){
			timerange=request.getParameter("timerange").toString();
		}if(request.getParameter("xAxisField")!=null){
			xAxis=request.getParameter("xAxisField").toString();
		}if(request.getParameter("yAxisField")!=null){
			yAxis=request.getParameter("yAxisField").toString();
		}
		String responseStr = service.getAdditionalInfo(parameterName, addressedMachine,respAppName,respPageName,osType,timerange,xAxis,yAxis);
		logger.debug("RESPONSE from Service === " + responseStr);
		return responseStr;
	}
	@RequestMapping(value="correlateResponseTime.html")
	public @ResponseBody String correlateRespTime(HttpServletRequest request){
		logger.debug("DEBUG::SmartIT_UX::com.lnt.controllers::DashboardController.correlateRespTime():: called");
		String parameterName = request.getParameter("parameterName").toString();
		String addressedMachine = request.getParameter("addressedMachine").toString();
//		addressedMachine = "172.25.38.49";
		return service.correlateRespTime(parameterName, addressedMachine);
	}
	@RequestMapping(value="respPrediction.html")
	public @ResponseBody String predictResponse(HttpServletRequest request){
		logger.debug("DEBUG::SmartIT_UX::com.lnt.controllers::DashboardController.predictResponse():: called");
		String app = request.getParameter("appName").toString();
		String page = request.getParameter("page").toString();
		return service.predictResponseTime(app, page);
	}
	@RequestMapping(value="lastUpdatedRespTime.html")
	public @ResponseBody String lastUpdatedRespTime(HttpServletRequest request){
		logger.debug("DEBUG::SmartIT_UX::com.lnt.controllers::DashboardController.lastUpdatedRespTime():: called");
		String app = request.getParameter("appName").toString();
		String page = request.getParameter("page").toString();
		String limit =  request.getParameter("limit").toString();
		return service.getLastUpdatedRespTime("RESPONSETIME",app, page,limit);
	}
	@RequestMapping(value="additionalInfoForTrends.html")
	public @ResponseBody String getAdditionalDataForTrends(HttpServletRequest request) {
		String addressedMachine = null;
		String respAppName = null;
		String respPageName = null;
		String osType=null;
		String timerange=null;
		String xAxis=null;
		String yAxis=null;
	    logger.debug("SmartIT_UX::com.lnt.controllers::DashboardController.getAdditionalDataForTrends():: called");
		String parameterName = request.getParameter("parameterName").toString();
		if(request.getParameter("addressedMachine") != null){
		 addressedMachine = request.getParameter("addressedMachine").toString();
		} if(request.getParameter("urlName") != null){
			respAppName = request.getParameter("urlName");
		}if(request.getParameter("page") != null){
			respPageName = request.getParameter("page");
		}if(request.getParameter("osType") != null){
			osType = request.getParameter("osType").toString();
		}if(request.getParameter("timerange")!=null){
			timerange=request.getParameter("timerange").toString();
		}if(request.getParameter("xAxisField")!=null){
			xAxis=request.getParameter("xAxisField").toString();
		}if(request.getParameter("yAxisField")!=null){
			yAxis=request.getParameter("yAxisField").toString();
		}
		String responseStr = service.getAdditionalInfo(parameterName, addressedMachine,respAppName,respPageName,osType,timerange,xAxis,yAxis);
		logger.debug("RESPONSE from Service === " + responseStr);
		return responseStr;
	}
	
	@RequestMapping(value="donutChart.html")
	@ResponseBody
	public String getDataForDonutChart(HttpServletRequest request) throws IOException{
	    logger.debug("SmartIT_UX::com.lnt.controllers::DashboardController.getDataForDonutChart():: called");
	    String countOfRecords=service.getDataForDonutChart();
	    System.out.println(countOfRecords);
		return countOfRecords;
	}
	
	@RequestMapping(value="donutChartGrid.html")
	@ResponseBody
	public String getDataForDonutChartGrid(HttpServletRequest request) throws IOException{
	    logger.debug("SmartIT_UX::com.lnt.controllers::DashboardController.getDataForDonutgrid():: called");
	    String sliceName=null;
	    String sliceValue=null;
	    if(request.getParameter("sliceName") != null){
			 sliceName = request.getParameter("sliceName").toString();
			} if(request.getParameter("sliceValue") != null){
				sliceValue = request.getParameter("sliceValue").toString();
			}
	    String countOfRecords=service.getDataForDonutChartGrid(sliceName, sliceValue);
	    System.out.println(countOfRecords);
		return countOfRecords;
	}
	
	@RequestMapping(value="pieChart.html")
	@ResponseBody
	public String getDataForpieChart(HttpServletRequest request) throws IOException{
	    logger.debug("SmartIT_UX::com.lnt.controllers::DashboardController.getDataForpieChart():: called");
	    String countOfRecords=service.getDataForPieChart();
	    System.out.println(countOfRecords);
		return countOfRecords;
	}
	
	@RequestMapping(value="metricChart.html")
	@ResponseBody
	public String getDataFormetricChart(HttpServletRequest request) throws IOException{
	    logger.debug("SmartIT_UX::com.lnt.controllers::DashboardController.getDataForpieChart():: called");
	    String countOfRecords=service.getDataForMetricChart();
	    System.out.println(countOfRecords);
		return countOfRecords;
	}
	
	@RequestMapping(value="machineInfo.html")
	@ResponseBody
	public String getDataMachineInfo(HttpServletRequest request) throws IOException{
	    logger.debug("SmartIT_UX::com.lnt.controllers::DashboardController.getDataMachineInfo():: called");
	    String countOfMachines=service.getDataMachineInfo();
	    System.out.println(countOfMachines);
		return countOfMachines;
	}
	
	
}
