package com.lnt.smartit.models;

public class EssbaseLogs {
	
	private String Date;
	private String Application;
	private String Log_type;
	private long Log_code;
	private String log;
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getApplication() {
		return Application;
	}
	public void setApplication(String application) {
		Application = application;
	}
	public String getLog_type() {
		return Log_type;
	}
	public void setLog_type(String log_type) {
		Log_type = log_type;
	}
	public long getLog_code() {
		return Log_code;
	}
	public void setLog_code(long log_code) {
		Log_code = log_code;
	}
	public String getLog() {
		return log;
	}
	public void setLog(String log) {
		this.log = log;
	}
	@Override
	public String toString() {
		return "EssbaseLogs [Date=" + Date + ", Application=" + Application
				+ ", Log_type=" + Log_type + ", Log_code=" + Log_code
				+ ", log=" + log + "]";
	}
}
