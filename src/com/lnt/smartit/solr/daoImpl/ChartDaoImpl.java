package com.lnt.smartit.solr.daoImpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lnt.smartit.mysql.model.AppLogServiceComponentParam;
import com.lnt.smartit.mysql.model.ApplicationMaster;
import com.lnt.smartit.mysql.model.Applications;
import com.lnt.smartit.mysql.model.ManageReport;
import com.lnt.smartit.mysql.model.Report;
import com.lnt.smartit.mysql.model.ReportMetadata;
import com.lnt.smartit.solr.dao.ChartDao;
import com.lnt.smartit.solr.dao.QueryDao;
import com.lnt.smartit.solr.pojo.ChartMaster;
import com.lnt.smartit.solr.pojo.Charting;
import com.lnt.smartit.solr.pojo.Dashboard;
import com.lnt.smartit.solr.pojo.DashboardUpdate;
import com.lnt.smartit.mysql.model.DashboardMaster;
import com.lnt.smartit.solr.pojo.Field;
import com.lnt.smartit.util.DatabaseConfig;

@Service
@Transactional
public class ChartDaoImpl implements ChartDao{
	private static Logger logger = Logger.getLogger(ChartDaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private QueryDao queryDao;

	@Override
	public String saveChartMaster(Report report) {
		logger.debug("Inside saveChart---- ");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(report);
    System.out.println("id=== "+report.getReportId());
    String id=report.getReportId();
			return id;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getChartNameList() {
		ArrayList<String> chart_name_List = new ArrayList<String>();
		Query query = sessionFactory.getCurrentSession().createQuery("select reportName from Report");
		chart_name_List = (ArrayList<String>) query.list();
		return chart_name_List;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Report> getCharts() {
		ArrayList<Report> chartList = new ArrayList<Report>();
		ArrayList<String> chart_name_List = new ArrayList<String>();
		Query query = sessionFactory.getCurrentSession().createQuery("select reportName from Report");
		chart_name_List = (ArrayList<String>) query.list();
		for (String c : chart_name_List) {
			Report chart_name = new Report();
			chart_name.setReportName(c);
			chartList.add(chart_name);
		}
		return chartList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Charting> drawChart(Report chart) {
		List<Charting> chart_data = new ArrayList<Charting>();
		Query query = sessionFactory.getCurrentSession().createQuery("from Report where reportName='" + chart.getReportName() + "'");
		query.setMaxResults(1);
		ArrayList<Report> chartList = (ArrayList<Report>) query.list();
		

		 
		if (chartList!=null && !chartList.isEmpty()) {
			Report draw_chart = chartList.get(0);
			String rep_id = draw_chart.getReportId();
			Query q = sessionFactory.getCurrentSession().createQuery(
					"from ReportMetadata where reportId='" + rep_id + "'");
			ArrayList<ReportMetadata> chartMetaList = (ArrayList<ReportMetadata>) q
					.list();
			List<String> fieldnames = new ArrayList<String>();
			for (ReportMetadata chartMeta : chartMetaList) {
				fieldnames.add(chartMeta.getFieldName());
			}
			com.lnt.smartit.solr.pojo.Query query_param = new com.lnt.smartit.solr.pojo.Query();
			query_param.setUrl(DatabaseConfig.solrUrl);
			query_param.setCollection(draw_chart.getCollection());
			query_param.setQuery("*:*");
			query_param.setRows(-1);
			query_param.setChart(draw_chart.getReportType());
			query_param.setChart_name(chart.getReportName());
			String[] fieldArr = new String[fieldnames.size()];
			fieldArr = fieldnames.toArray(fieldArr);
			query_param.setSelected_fields(fieldArr);
			try {
				if (draw_chart.getReportType().equalsIgnoreCase("time")) {
					chart_data = queryDao.getCustomTimeSeries(query_param);
				} else if (draw_chart.getReportType().equalsIgnoreCase("pie")
						|| draw_chart.getReportType()
								.equalsIgnoreCase("column")) {
					chart_data = queryDao.getCustomChartsNofilter(query_param);
				} else if (draw_chart.getReportType().equalsIgnoreCase("grid")) {
					Charting charting = new Charting();
					charting.setCol_name(draw_chart.getCollection());
					charting.setChart_type("grid");
					charting.setGridcols(fieldArr);
					chart_data.add(charting);
					return chart_data;
				} else if (draw_chart.getReportType().equalsIgnoreCase("x-y")) {
					chart_data = queryDao.getCount(query_param);
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.debug("Error");
			}
		}
		return chart_data;
	}

	
	
	
	@SuppressWarnings("unchecked")
	public List<Charting> drawChartonUpdate(DashboardUpdate dashboardUpdate) {
		List<Charting> chart_data = new ArrayList<Charting>();
		Query query = sessionFactory.getCurrentSession().createQuery("from Report where reportName='" + dashboardUpdate.getChartname() + "'");
		query.setMaxResults(1);
		ArrayList<Report> chartList = (ArrayList<Report>) query.list();
		Report draw_chart = chartList.get(0);
		String rep_id=draw_chart.getReportId();
		Query q = sessionFactory.getCurrentSession().createQuery("from ReportMetadata where reportId='" + rep_id + "'");
		ArrayList<ReportMetadata> chartMetaList = (ArrayList<ReportMetadata>) q.list();
		List<String> fieldnames= new ArrayList<String>();
		for (ReportMetadata chartMeta:chartMetaList){
			fieldnames.add(chartMeta.getFieldName());
		}
		com.lnt.smartit.solr.pojo.Query query_param = new com.lnt.smartit.solr.pojo.Query();
		query_param.setUrl(DatabaseConfig.solrUrl);
		query_param.setCollection(draw_chart.getCollection());
		query_param.setQuery("*:*");
		query_param.setRows(-1);
		query_param.setChart(draw_chart.getReportType());
		query_param.setChart_name(dashboardUpdate.getChartname());
		query_param.setStart_facet(dashboardUpdate.getStartdate());
		query_param.setEnd_facet(dashboardUpdate.getEnddate());
		query_param.setRange_gap("+1DAY");
		String[] fieldArr = new String[fieldnames.size()];
		fieldArr = fieldnames.toArray(fieldArr);

		query_param.setSelected_fields(fieldArr);
		try {
			if (draw_chart.getReportType().equalsIgnoreCase("time")) {
				chart_data = queryDao.getCustomTimeSeries(query_param);
			}else
			if (draw_chart.getReportType().equalsIgnoreCase("pie")
					|| draw_chart.getReportType().equalsIgnoreCase("column")) {
				chart_data = queryDao.getCustomCharts(query_param);
			}else
			if (draw_chart.getReportType().equalsIgnoreCase("grid")) {
				Charting charting =new Charting();
				charting.setCol_name(draw_chart.getCollection());
				charting.setChart_type("grid");
				charting.setGridcols(fieldArr);
				charting.setGridData(queryDao.getBasicQueryforReports(query_param));
				chart_data.add(charting);
				return chart_data;
			}else
				if (draw_chart.getReportType().equalsIgnoreCase("x-y")){
					chart_data = queryDao.getCount(query_param);
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return chart_data;
	}
	
	
	@Override
	public Boolean savedashboard(List<DashboardMaster> dashboard) {
		try {
			for (DashboardMaster dash : dashboard) {
				sessionFactory.getCurrentSession().saveOrUpdate(dash);
				
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("Error");
			return false;
		}
	
	
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DashboardMaster> getDashboards() {
		ArrayList<DashboardMaster> dashboardList = new ArrayList<DashboardMaster>();
		ArrayList<String> dashboard_name_List = new ArrayList<String>();
		Query query = sessionFactory.getCurrentSession().createQuery("select distinct dashboard_name from DashboardMaster");
		dashboard_name_List = (ArrayList<String>) query.list();
		for (String c : dashboard_name_List) {
			DashboardMaster dashboard_name = new DashboardMaster();
			dashboard_name.setDashboard_name(c);
			dashboardList.add(dashboard_name);
		}
		return dashboardList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<String> getDashboardNameList() {
		ArrayList<DashboardMaster> dashboardList = new ArrayList<DashboardMaster>();
		ArrayList<String> dashboard_name_List = new ArrayList<String>();
		Query query = sessionFactory.getCurrentSession().createQuery("select distinct dashboard_name from DashboardMaster");
		dashboard_name_List = (ArrayList<String>) query.list();
		return dashboard_name_List;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Dashboard> drawDashboard(DashboardMaster dash_name) {
		List<Dashboard> dash_data=new ArrayList<Dashboard>();
		List<DashboardMaster> dash_master_data=new ArrayList<DashboardMaster>();
		Query query=sessionFactory.getCurrentSession().createQuery("from DashboardMaster where dashboard_name='"+dash_name.getDashboard_name()+"' ORDER BY sequence ASC");
		dash_master_data=(ArrayList<DashboardMaster>)query.list();
		for(DashboardMaster d:dash_master_data)
		{
			Dashboard dash_element=new Dashboard();
			dash_element.setDashboard_data(d);
			List<Charting> chart_data=new ArrayList<Charting>();
			String chart=d.getChart_name();
			Query query1=sessionFactory.getCurrentSession().createQuery("from Report where reportName='"+chart+"'");
			query1.setMaxResults(1);
			ArrayList<Report> temp_chartList = (ArrayList<Report>) query1.list();
			Report draw_chart=temp_chartList.get(0);
			
			String rep_id=draw_chart.getReportId();
			Query q = sessionFactory.getCurrentSession().createQuery("from ReportMetadata where reportId='" + rep_id + "'");
			ArrayList<ReportMetadata> chartMetaList = (ArrayList<ReportMetadata>) q.list();
			List<String> fieldnames= new ArrayList<String>();
			for (ReportMetadata chartMeta:chartMetaList){
				fieldnames.add(chartMeta.getFieldName());
			}
			String[] fieldArr = new String[fieldnames.size()];
			fieldArr = fieldnames.toArray(fieldArr);
			com.lnt.smartit.solr.pojo.Query query_param= new com.lnt.smartit.solr.pojo.Query();
			query_param.setUrl(DatabaseConfig.solrUrl);
			query_param.setCollection(draw_chart.getCollection());
			query_param.setQuery("*:*");
			query_param.setSelected_fields(fieldArr);
			query_param.setRows(-1);
			query_param.setChart(draw_chart.getReportType());
			try {
				
			if(draw_chart.getReportType().equalsIgnoreCase("time"))
			{
				chart_data=queryDao.getCustomTimeSeries(query_param);
			}
			if(draw_chart.getReportType().equalsIgnoreCase("x-y"))
			{
				chart_data=queryDao.getCustomTimeSeries(query_param);
			}
			if(draw_chart.getReportType().equalsIgnoreCase("pie") || draw_chart.getReportType().equalsIgnoreCase("column"))
			{
				chart_data=queryDao.getCustomChartsNofilter(query_param);
			}
			if (draw_chart.getReportType().equalsIgnoreCase("grid")) {
				Charting charting =new Charting();
				charting.setCol_name(draw_chart.getCollection());
				charting.setChart_type("grid");
				charting.setGridcols(fieldArr);
				chart_data.add(charting);
			}
			} catch (Exception e) {
				e.printStackTrace();
			}
			dash_element.setChart_data(chart_data);
			dash_data.add(dash_element);
		}
		return dash_data;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Applications> getApplications() {
		List<Applications> appList = new ArrayList<Applications>();
		Query query = sessionFactory.getCurrentSession().createQuery(" from Applications");
		appList =  query.list();
		/*for(int i=0;i<appList.size();i++){
			System.out.println("my app"+appList.get(i));
		}*/
		return appList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Report> getReports() {
		List<Report> repList = new ArrayList<Report>();
		Query query = sessionFactory.getCurrentSession().createQuery(" SELECT a.reportName,a.reportId FROM Report a WHERE  NOT EXISTS (SELECT od.chartId FROM  DashboardMaster od WHERE  a.reportId = od.chartId) ");
		repList =  query.list();
		/*for(int i=0;i<repList.size();i++){
			System.out.println("my rep"+repList.get(i).getReportId());
		}*/
		return repList;
	}

	
	@Override
	public List<AppLogServiceComponentParam> getCollectionsFromDB(String appsel) {
		List<AppLogServiceComponentParam> colList = new ArrayList<AppLogServiceComponentParam>();
		Query query = sessionFactory.getCurrentSession().createQuery("SELECT DISTINCT t1.paramValue FROM AppLogServiceComponentParam t1 INNER JOIN  t1.appServiceComp t2 WHERE t2.app.appId=:appid AND t2.serviceComponent=:servicecomp AND t1.paramName=:param AND t2.appServiceCompId=t1.appServiceComp.appServiceCompId");
		query.setString("appid", appsel);
		query.setString("servicecomp", "SOLR");
		query.setString("param", "collection-name");
		System.out.println("appsel"+appsel);
		colList =  query.list();
		/*for(int i=0;i<colList.size();i++){
		System.out.println("my col"+colList.get(i));
	}*/
		return colList;
	}


	@Override
	public Boolean saveChartMaster(ReportMetadata saveChartMaster) {
		logger.debug("Inside saveChartMaster---- ");
		try {
			
			sessionFactory.getCurrentSession().saveOrUpdate(saveChartMaster);
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}


	@Override
	public Boolean selectReports(List<ManageReport> manageReportList) {
		logger.debug("Inside selectReports---- ");
		try {
			for(int y=0;y<manageReportList.size();y++){
			sessionFactory.getCurrentSession().saveOrUpdate(manageReportList.get(y));
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	@Override
	public List<Charting> getUpdatedReport(DashboardUpdate dashboardUpdate) {
		logger.debug("Inside getUpdatedReport---- ");
		List<DashboardMaster> dashboardMasters=new ArrayList<DashboardMaster>();
		Query query = sessionFactory.getCurrentSession().createQuery(" FROM DashboardMaster  WHERE dashboard_name=:dashboardname AND div_name=:divname");
		query.setString("dashboardname", dashboardUpdate.getDashname());
		query.setString("divname",dashboardUpdate.getDivname());
		dashboardMasters=query.list();
		DashboardMaster dashboardMaster=new DashboardMaster();
		dashboardMaster=dashboardMasters.get(0);
		String reportname=dashboardMaster.getChart_name();
		dashboardUpdate.setChartname(reportname);
		String divName[]=dashboardUpdate.getDivname().split("_");
		String divcount=divName[1];
		System.out.println("dashboardUpdates"+dashboardMasters.get(0));
		List<Charting> chart_data=new ArrayList<Charting>();
		chart_data=drawChartonUpdate(dashboardUpdate);
		for(int p=0;p<chart_data.size();p++){
		chart_data.get(p).setDiv_count(divcount);
		}
		return chart_data;
	}


	@Override
	public Boolean deleteReports(String[] selected_reports) {
		logger.debug("Inside deleteReports---- ");
		try {
			for(int y=0;y<selected_reports.length;y++){
				Report report= new Report();
				report.setReportId(selected_reports[y]);
				Query query=sessionFactory.getCurrentSession().createQuery("Delete FROM ReportMetadata WHERE reportId='"+selected_reports[y]+"'");
				query.executeUpdate();
				sessionFactory.getCurrentSession().delete(report);
				
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<ManageReport> getManageReportFields(String collection) {
		List<ManageReport> manageReports=new ArrayList<ManageReport>();
		Query query=sessionFactory.getCurrentSession().createQuery(" From ManageReport where colId='"+collection + "' order by field");
		manageReports=query.list();
		return manageReports;
	}


	@Override
	public void undoFilterType(List<ManageReport> manageReportList) {
		for(int y=0;y<manageReportList.size();y++){
			Query query=sessionFactory.getCurrentSession().createQuery("Delete FROM ManageReport WHERE colId='"+manageReportList.get(y).getColId()+"' AND field='"+manageReportList.get(y).getField() +"' ");
		query.executeUpdate();
		}
		
	}


	@Override
	public void undoInvalid(List<ManageReport> manageReportList) {
		Query query=sessionFactory.getCurrentSession().createQuery("Delete FROM ManageReport WHERE colId='"+manageReportList.get(0).getColId()+"' ");
		query.executeUpdate();
	}


	@Override
	public String getPrimaryFilter(String col) {
		
		Query query=sessionFactory.getCurrentSession().createQuery(" SELECT field FROM  ManageReport WHERE colId='"+col+"' AND filterType='Primary'");
		String PrimaryFilter=(String) query.uniqueResult();
		System.out.println("PrimaryFilter"+PrimaryFilter);
		return PrimaryFilter;
	}


	@Override
	public Boolean deleteDashboards(String selected_dashboards) {
		logger.debug("Inside deleteDashboards---- ");
		try {
			/*for(int y=0;y<selected_dashboards.length;y++){
				Query query=sessionFactory.getCurrentSession().createQuery("Delete FROM DashboardMaster WHERE dashboard_name='"+selected_dashboards[y]+"'");
				query.executeUpdate();
			}*/
			
			Query query=sessionFactory.getCurrentSession().createQuery("Delete FROM DashboardMaster WHERE dashboard_name='"+selected_dashboards+"'");
			query.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}


	@Override
	public Boolean setSequence(String[] sequenceArray,String dashName) {
		logger.debug("Inside setSequence---- ");
		int count=1;
		try {
			for(int y=0;y<sequenceArray.length;y++){
				System.out.println(y);
				
				Query query=sessionFactory.getCurrentSession().createQuery("Update DashboardMaster SET sequence='"+count+"' WHERE dashboard_name='"+dashName+"' AND  chart_name='"+sequenceArray[y]+"'");
				count++;
				query.executeUpdate();
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}


	@Override
	public Boolean removeChartfromDash(String chart_name, String dashboardName) {
		logger.debug("Inside setSequence---- ");
		try {
				Query query=sessionFactory.getCurrentSession().createQuery("Delete FROM  DashboardMaster  WHERE dashboard_name='"+dashboardName+"' AND  chart_name='"+chart_name+"'");
				query.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}


	@Override
	public String getReportIdfromName(String chart) {
		Query query=sessionFactory.getCurrentSession().createQuery(" Select reportId FROM  Report WHERE reportName='"+chart+"'");
		String chartId=(String) query.uniqueResult();
		return chartId;
	}


	
}
