package com.lnt.smartit.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lnt.smartit.mysql.dao.VariableAnalysisDataDao;
import com.lnt.smartit.mysql.model.ApplicationMaster;
import com.lnt.smartit.mysql.model.DeviceMaster;
import com.lnt.smartit.mysql.model.VariableAnalysis;
import com.lnt.smartit.services.VariableAnalysisService;

@Service
public class VariableAnalysisServiceImpl implements VariableAnalysisService {
	@Autowired
	VariableAnalysisDataDao variableAnalysisDataDao;

	// for plotting the response time historic graph
	public String drawHistoricResponseTimeGraph(String parameterName/*,String addressedMachine, String respAppName,String respPageName,String osType,String timerange,String xAxis,String yAxis*/) {
		String historicGraphList = variableAnalysisDataDao.getAdditionalInfoForGraphs(parameterName/*,addressedMachine,respAppName,respPageName,osType,timerange,xAxis,yAxis*/);
		return historicGraphList;
	}

	// for drawing the response time prediction graph
	public List<VariableAnalysis> drawResponseTimePredictionGraphs(String page) {
		List<VariableAnalysis> predictionGraphList = new ArrayList<VariableAnalysis>();
		predictionGraphList = variableAnalysisDataDao.drawResponseTimePredictionGraphs(page);
		return predictionGraphList;
	}

	// fetching the application list on load from database
	public List<ApplicationMaster> fetchApplicationList() {
		List<ApplicationMaster> fetchApplicationList = new ArrayList<ApplicationMaster>();
		fetchApplicationList = variableAnalysisDataDao.fetchApplicationList();
		return fetchApplicationList;
	}

	// fetching server list on load from database
	public List<DeviceMaster> fetchServerList() {
		List<DeviceMaster> fetchApplicationList = new ArrayList<DeviceMaster>();
		fetchApplicationList = variableAnalysisDataDao.fetchServerList();
		return fetchApplicationList;
	}

	public List<ApplicationMaster> fetchApplicationListForGrid() {
		List<ApplicationMaster> fetchApplicationListForGrid = new ArrayList<ApplicationMaster>();
		fetchApplicationListForGrid = variableAnalysisDataDao.fetchApplicationListForGrid();
		return fetchApplicationListForGrid;
	}

	// for plotting related variables chart for CPU
	@SuppressWarnings("unused")
	public List<VariableAnalysis> plotRelatedVariablesChartCPU(String serverIPAddress) {
		List<VariableAnalysis> listForCPURelatedVariables = new ArrayList<VariableAnalysis>();
		return listForCPURelatedVariables = variableAnalysisDataDao.plotRelatedVariablesChartCPU(serverIPAddress);
	}

	// for plotting related variables chart for RAM
	@SuppressWarnings("unused")
	public List<VariableAnalysis> plotRelatedVariablesChartRAM(String serverIPAddress) {
		List<VariableAnalysis> listForRAMRelatedVariables = new ArrayList<VariableAnalysis>();
		return listForRAMRelatedVariables = variableAnalysisDataDao.plotRelatedVariablesChartRAM(serverIPAddress);
	}

	// for plotting related variables chart for IO
	@SuppressWarnings("unused")
	public List<VariableAnalysis> plotRelatedVariablesChartIO(String serverIPAddress) {
		List<VariableAnalysis> listForIORelatedVariables = new ArrayList<VariableAnalysis>();
		return listForIORelatedVariables = variableAnalysisDataDao.plotRelatedVariablesChartIO(serverIPAddress);
	}

	// correlated graphs for cpu
	@SuppressWarnings("unused")
	public List<VariableAnalysis> showCorrelatedDataForRelatedVariablesCPU(VariableAnalysis variableAnalysis) {
		System.out.println("variable analysis in service for correlation showing related variables graphs"
						+ variableAnalysis.getStartDate()
						+ " start month :"
						+ variableAnalysis.getStartMonth()
						+ " start year :"
						+ variableAnalysis.getStartYear());
		List<VariableAnalysis> listForCorrelationCPURelatedVariables = new ArrayList<VariableAnalysis>();
		return listForCorrelationCPURelatedVariables = variableAnalysisDataDao.plotCorrelatedVariablesChartCPU(variableAnalysis);
	}

	@SuppressWarnings("unused")
	public List<VariableAnalysis> showCorrelatedDataForRelatedVariablesRAM(VariableAnalysis variableAnalysis) {
		List<VariableAnalysis> listForCorrelationRAMRelatedVariables = new ArrayList<VariableAnalysis>();
		return listForCorrelationRAMRelatedVariables = variableAnalysisDataDao.plotCorrelatedVariablesChartRAM(variableAnalysis);
	}

	@SuppressWarnings("unused")
	public List<VariableAnalysis> showCorrelatedDataForRelatedVariablesIO(VariableAnalysis variableAnalysis) {
		List<VariableAnalysis> listForCorrelationIORelatedVariables = new ArrayList<VariableAnalysis>();
		return listForCorrelationIORelatedVariables = variableAnalysisDataDao.plotCorrelatedVariablesChartIO(variableAnalysis);
	}
}
