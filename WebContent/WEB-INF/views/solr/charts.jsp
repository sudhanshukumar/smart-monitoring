<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../assets/ico/favicon.ico">
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

<title>SmartIT</title>

<!-- Bootstrap scripts -->
<script src="scripts/common/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="scripts/common/jquery.jqGrid.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/common/jquery.datetimepicker.css" />
<script type="text/javascript" src="scripts/common/jquery-ui.js"></script>
<script src="scripts/common/jquery-ui.min.js"></script>
<script type="text/javascript" src="scripts/common/grid.locale-en.js"></script>
<script type="text/javascript" src="scripts/common/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="scripts/common/exportData/jquery-dateFormat.min.js"></script>
<script type="text/javascript" src="scripts/common/jquery.loadmask.js"></script>

<!-- Bootstrap Styles -->
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap-responsive.css">
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap-theme.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/common/ui.jqgrid.css" />
<link rel="stylesheet" href="css/custom/specific/nav.css">
<link href="scripts/c3-0.4.10/c3.css" rel="stylesheet" type="text/css" charset="utf-8">
<link rel="stylesheet" type="text/css" href="css/common/jquery-ui.min.css" />

<!-- C3 scripts -->
<script src="scripts/d3-master/d3.js" charset="utf-8"></script>
<script src="scripts/c3-0.4.10/c3.js" charset="utf-8"></script>

<!-- Highcharts script -->
<script src="scripts/common/highstocks.js"></script>
<script src="scripts/common/highcharts.js"></script>

<!-- Custom javascripts -->
<script type="text/javascript" src="scripts/custom/charts.js"></script>

<script src="scripts/custom/tab_menu.js"></script>

<% String session_val = (String)session.getAttribute("modules"); %>

<script type="text/javascript">
	session_obj= '<%=session_val%>';
</script>

</head>
<body style="font-size:11px!important;">
<div class="container-fluid">
	<%@ include file="../common/header.jsp"%>
 	
 	<nav class="navbar navbar-inverse" style="margin-top: 0px">
	      <ul class="nav navbar-nav navbar-xs">
	        <li id="Dashboard"><a href="dashboard.html">Dashboard<span class="sr-only">(current)</span></a></li>
	        <li id="AlertHistory"><a href="problemHistory.html">Alert History</a></li>
	        <!-- <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Manage<span class="caret"></span></a>
	          <ul class="dropdown-menu">
	            <li><a href="#">Manage Device</a></li>
	            <li><a href="#">Manage Threshold</a></li>
	          </ul>
	        </li> -->
	        <li id="LogAnalysis"><a href="query.html">Log Analysis</a></li>
	        <li id="LogCorrelation"><a href="correlation.html">Log Correlation</a></li>
	        <li id="VariableAnalysis"><a href="variableAnalysis.html"><spring:message code="label.admin.menu5"></spring:message></a></li>
	        <li  class="active" class="dropdown" id="Reports">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reports <span class="caret"></span></a>
	          <ul class="dropdown-menu">
	            	<li id="CreateReport"><a href="charts.html">Create Report</a></li>
		            <li id="CreateDashboard"><a href="createuserdashboard.html">Create Dashboard</a></li>
		            <li id="ViewDashboard"><a href="list_dashboard.html">View Dashboard</a></li>
	          </ul>
	        </li>
	      </ul>
	  </nav> 

	<div class="container-fluid innerContainer" id = "inner_container">
		<div class="col-md-3" style="background-color:#e0e0e0; height:460px;">
			<div class="row" style=" margin-top: 20px; margin-left: .05px;">
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="col-sm-4" >Application:</label>
						<div class="col-sm-8">
							<select class="form-control" id="application" style="width:165px;">
								<option value=''></option>
							</select>
						</div>
					</div>

				</form>
			</div>
			<div class="row" style=" margin-left: .05px;">
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="col-sm-4" >Log Type:</label>
						<div class="col-sm-8">
							<select class="form-control" id="collections" style="width:165px;">
								<option value=''></option>
							</select>
						</div>
					</div>

				</form>
			</div>
			<div class="row" style=" margin-left: .05px;">
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="col-sm-4" >Chart:</label>
						<div class="col-sm-8">
							<select class="form-control" id="chart"  onchange="refresh()" style="width:165px;"> 
								<option value="grid">Grid</option>
									<option value="pie">Pie</option>
									<option value="column">Line</option>
									<option value="time">Time Series</option>
									<option value="x-y">X-Y Series</option>
								</select>
						</div>
					</div>

				</form>
			</div>
			<div class="row">
				<div class="col-md-12" style="border: 0px solid #000;" id="field_dropdown" >
					<form class="form-horizontal" role="form">
						<div class="form-group">
							<label class="col-sm-4" >Field Name:</label>
							<div class="col-sm-8">
								<select class="form-control" id="field_value">
								</select>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="row" id="result_grid"  style="overflow-y: scroll; height: 170px;  border: 0.1px solid #b2b2b2; margin-left: -15px;margin-bottom: 15px;">
				<div class="col-md-12" style=" padding-right: 0px;  padding-left: 0px;" >
					<table class="table table-bordered" id="resId" >
						<thead>
							<tr>
								<th></th>
								<th>Field Name</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
			<div class="row" style=" margin-left: .05px;">
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="col-sm-4" >Name:</label>
						<div class="col-sm-8">
							<input class="form-control" id="chart_name" type="text" placeholder="Enter Report Name" style="width:165px;">
						</div>
					</div>

				</form>
			</div>
		 <div class="row" style=" margin-left: 145px;">
		   <div class="col-sm-4">
		   		  <form class="form-horizontal" role="form">
					<div class="form-group">
						<div class="col-sm-8">
							<input type="button" class="btn btn-primary btn-sm" id="save" name="Save" value=" Save"/> 

						</div>
					</div>
				</form>
		   </div>
		   <div class="col-sm-4" style=" margin-left: 15px;">
		   		  <form class="form-horizontal" role="form">
					<div class="form-group">
						<div class="col-sm-8">
							<input type="button" class="btn btn-primary btn-sm" id="execute" name="Draw" value=" Draw"/> 

						</div>
					</div>
				</form>
		   </div>
		    </div>
		</div>
		<div class="col-md-5" style="height:460px;">
	          <div class="col-md-12" id="main_container">
					<div id="chart_container"></div>
				</div>
		  </div>
		</div>
	<div id="alertDialog"></div>	
		<%@ include file="../common/footer.jsp"%>
	</div>

</body>
</html>