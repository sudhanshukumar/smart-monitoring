package com.lnt.smartit.helper;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import com.lnt.smartit.models.AlertMessage;

public class AutmationFirstProcessor implements Runnable{
	
	private static Logger logger = Logger.getLogger(ConfigSetup.class);
	private AlertMessage alertMsg;
	private String muleInbound;
	
	public AutmationFirstProcessor(AlertMessage alert,String muleInbound) {
		this.alertMsg = alert;
		this.muleInbound = muleInbound;
	}

	@Override
	public void run() {
		logger.info("Create Incident :: Ticket Description = "+alertMsg.getItsmTktDescription());
		HttpURLConnection httpUrlConnection;
		DataOutputStream os = null;
		BufferedReader in = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			//String description = alertMsg.getItsmTktDescription().replace("\n", "$");
			/*if(alertMsg.getAlertMessageparameterName().equalsIgnoreCase("DISK")){*/
			String description =mapper.writeValueAsString(alertMsg);
			
			HttpURLConnectionHelper helper = new HttpURLConnectionHelper(muleInbound);
	        helper.addParam("WinDiskAlert",description);
	        helper.sendGet();
	        System.out.println("----------description is ------"+description);
			String descJson = "{\"WinDiskAlert\":\""+description+"\"}";
			System.out.println("----------descriptionJSDON is ------"+descJson);
			/*httpUrlConnection = (HttpURLConnection) new URL(muleInbound).openConnection();
			String USER_AGENT = "Mozilla/5.0";
			httpUrlConnection.setRequestProperty("User-Agent", USER_AGENT);
	        httpUrlConnection.setRequestMethod("POST");
	        httpUrlConnection.setRequestProperty("Content-Type", "application/json; charset=utf-8"); 
	        httpUrlConnection.setRequestProperty("Accept", "application/json; charset=utf-8");
	        httpUrlConnection.setDoOutput(true);
	        os = new DataOutputStream(httpUrlConnection.getOutputStream());
            os.write(descJson.getBytes("UTF-8"));
	        in = new BufferedReader(new InputStreamReader(httpUrlConnection.getInputStream()));*/
			/*}*/
		} catch (IOException e) {
			logger.error(" -- Something went wrong in mule -- ",e);
		} finally {
			try {
				if(os != null)
					os.close();
				if(in != null)
					in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
