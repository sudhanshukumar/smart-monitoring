package com.lnt.smartit.mysql.daoImpl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lnt.smartit.helper.HbmHelper;
import com.lnt.smartit.mysql.dao.UserDao;
import com.lnt.smartit.mysql.model.Roles;
import com.lnt.smartit.mysql.model.Users;

@Repository
@Transactional
public class UserDaoImpl implements UserDao{
@Autowired
private HbmHelper hbmHelper;
private static Logger logger = Logger.getLogger(UserDaoImpl.class);

	public List<Users> getAllUsers(){
		@SuppressWarnings("unchecked")
		List<Users> userList = (List<Users>) hbmHelper.createQuery("FROM Users", null);
		logger.debug(userList.toString());
		return userList;
	}
	
	public int addNewUser(Users user){
		Integer userId=null;
		String role = user.getRolename();
		logger.debug("role"+role);
		Roles roleObj = (Roles) hbmHelper.createQuery("FROM Roles where name='"+ role + "'", null).get(0);
		user.setRole_id(roleObj.getId());
		userId= (Integer) hbmHelper.save(user);
		logger.debug("userID in service :: User added Successfully "+userId);
		return userId;
	}

	public Users previewService(int userId) {
		@SuppressWarnings("unchecked")
		List<Users> userList = (List<Users>) hbmHelper.createQuery("FROM Users where userId='"+userId+"'", null);
		return userList.get(0);
	}

	public String deleteUser(int userId) {
		String deleteUserMasterMsg;
		try{
			@SuppressWarnings("unchecked")
			List<Users> userList = (List<Users>) hbmHelper.createQuery("FROM Users where userId='"+userId+"'", null);
			hbmHelper.delete(userList.get(0));
			deleteUserMasterMsg="User deleted successfully!!";
		}catch (Exception e) {
			e.printStackTrace();
			deleteUserMasterMsg="User deletion unsuccessfull due to "+e.getMessage();
		}
		return deleteUserMasterMsg;
	}

	public void updateUser(Users user) {
		logger.debug("inside update");
		Roles roleObj = (Roles) hbmHelper.createQuery("FROM Roles where name='"+ user.getRolename() + "'", null).get(0);
		user.setRole_id(roleObj.getId());
		hbmHelper.update(user);
		logger.debug("after update");
	}

	public List<Roles> findRolesColl() {
		@SuppressWarnings("unchecked")
		List<Roles> rolesList = (List<Roles>) hbmHelper.createQuery("FROM Roles", null);
		return rolesList;
	}
}
