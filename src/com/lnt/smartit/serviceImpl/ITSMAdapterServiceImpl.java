package com.lnt.smartit.serviceImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lnt.smartit.models.AlertMessage;
import com.lnt.smartit.mysql.dao.ITSMAdapterDao;
import com.lnt.smartit.mysql.model.AppServiceComponentParams;
import com.lnt.smartit.services.ITSMAdapterService;

@Service
public class ITSMAdapterServiceImpl implements ITSMAdapterService {

	@Autowired
	ITSMAdapterDao itsmAdapterDao;
	
	@Override
	public String getServiceCompNameForITSM() {
		// TODO Auto-generated method stub
		return itsmAdapterDao.getServiceCompNameForITSM();
	}

	@Override
	public List<AppServiceComponentParams> getServiceCompParamsForITSM() {
		// TODO Auto-generated method stub
		return itsmAdapterDao.getServiceCompParamsForITSM();
	}

	@Override
	public List<String> getAletTypeForIncidentCreation(String hostname) {
		// TODO Auto-generated method stub
		return itsmAdapterDao.getAletTypeForIncidentCreation(hostname);
	}

	@Override
	public List<String> checkItsmReqFlag() {
		// TODO Auto-generated method stub
		return itsmAdapterDao.checkItsmReqFlag();
	}
	
	@Override
	public int getDeduplicationWindowForIncidentCreation(AlertMessage alertMsg){
	
		return itsmAdapterDao.getDeduplicationWindowForIncidentCreation(alertMsg);
	}
	
	//10614080 on 09/03/2017 start	
	@Override
	public Map<Integer, HashMap<String,String>> getNotificationDetails(String hostname) {
		// TODO Auto-generated method stub
		return itsmAdapterDao.getNotificationDetails(hostname);
	}
	//10614080 on 09/03/2017 end
}
