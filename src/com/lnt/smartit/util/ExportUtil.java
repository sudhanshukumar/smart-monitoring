package com.lnt.smartit.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.security.core.userdetails.User;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.lnt.smartit.models.GridHeaderModel;


// TODO: Auto-generated Javadoc
/**
 * The Class ExportUtil.
 * Author 	: Pinakin Abhyankar
 * Date 	: 15/06/2016
 * This class has all the methods related to export activity.
 */
public class ExportUtil {

	
	/**
	 * Export to excel file.
	 * 
	 * @param outputFilePath the output file path
	 * @param sheetName the sheet name
	 * @param data the data
	 * @return true, if export to excel file
	 */
	public static boolean exportToExcelFile(String outputFilePath,String sheetName,Object data [][])
	{		
		Workbook workbook = null;
		Integer rowCount = data.length;
		Integer columnCount = data[0].length;
		try
		{
			if(outputFilePath.endsWith("xlsx"))
			{
				workbook = new XSSFWorkbook();
			}
			else if(outputFilePath.endsWith("xls"))
			{
				workbook = new HSSFWorkbook();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
		
		try {
			Sheet sheet = workbook.createSheet(sheetName);
			Row headerRow = sheet.createRow(0);
			
			for(int i=0; i<columnCount; i++)
			{
				Cell cell = headerRow.createCell(i);
				cell.setCellValue(data[0][i].toString());
				
			}
						
			for(int j=1;j<rowCount;j++)
			{
				Row row = sheet.createRow(j);
				for(int i=0; i<columnCount; i++)
				{
					Cell cell = row.createCell(i);
					cell.setCellValue(data[j][i].toString());
				}
			}
			
			FileOutputStream fos=null;
			fos = new FileOutputStream(outputFilePath);
			workbook.write(fos);
			fos.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true ;
	}
	
	/**
	 * Export as excel.
	 *
	 * @param excelFilePath the excel file path
	 * @param sheetName the sheet name
	 * @param gridData the grid data
	 * @param headers the headers
	 * @return true, if export as excel
	 */
	public static boolean exportAsExcel(String excelFilePath, String sheetName, List<Map<String, Object>> gridData, List<GridHeaderModel> headers)
	{		
		Workbook workbook = null;
		try
		{
			if(excelFilePath.endsWith("xlsx"))
			{
				workbook = new XSSFWorkbook();
			}
			else if(excelFilePath.endsWith("xls"))
			{
				workbook = new HSSFWorkbook();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
		
		try {
			Sheet sheet = workbook.createSheet(sheetName);
			Row headerRow = sheet.createRow(0);
			int i=0;
			int j=0;
			for(GridHeaderModel column:headers)
			{
				if(!column.isHidden())
				{
					Cell cell = headerRow.createCell(i);
					cell.setCellValue(column.getName());
					i++;
				}
			}
			
			i=1;
			for(Map<String, Object> rowData : gridData)
			{
				j=0;
				Row row = sheet.createRow(i);
				for(GridHeaderModel column:headers)
				{
					if(!column.isHidden())
					{
						Cell cell = row.createCell(j);
						cell.setCellValue(rowData.get(column.getName()).toString());
						j++;
					}
				}
				i++;
			}
			
			
			FileOutputStream fos=null;
			fos = new FileOutputStream(excelFilePath);
			workbook.write(fos);
			fos.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true ;
	}
	
	/**
	 * Export as pdf.
	 *
	 * @param pdfFilePath the pdf file path
	 * @param gridData the grid data
	 * @param headers the headers
	 * @param columnCount the column count
	 * @return true, if export as pdf
	 */
	public static boolean exportAsPDF(String pdfFilePath, List<Map<String, Object>> gridData, List<GridHeaderModel> headers, Integer columnCount)
	{
		Document pdfDocument = new Document();
		boolean isSuccessful = false;
		try {
			PdfWriter.getInstance(pdfDocument, new FileOutputStream(pdfFilePath));
			pdfDocument.open();
			//we have two columns in the Excel sheet, so we create a PDF table with two columns
			//Note: There are ways to make this dynamic in nature, if you want to.
			PdfPTable table = new PdfPTable(columnCount);
			 table.setWidthPercentage(100);
		     table.setSpacingBefore(0f);
		     table.setSpacingAfter(0f);
			//We will use the object below to dynamically add new data to the table
			PdfPCell tableCell;
			//Loop through rows.
			
			for(GridHeaderModel column:headers)
			{
				if(!column.isHidden())
				{
					tableCell=new PdfPCell(new Phrase(column.getName()));
					table.addCell(tableCell);
				}
			}
			
			for(Map<String, Object> rowData : gridData)
			{
				for(GridHeaderModel column:headers)
				{
					if(!column.isHidden())
					{
						tableCell=new PdfPCell(new Phrase(rowData.get(column.getName()).toString()));
						table.addCell(tableCell);
					}
				}
			}
			
			pdfDocument.add(table);                       
			pdfDocument.close(); 
			
			isSuccessful = true;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return isSuccessful;
	}
}