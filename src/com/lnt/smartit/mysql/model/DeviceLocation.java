package com.lnt.smartit.mysql.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.codehaus.jackson.map.annotate.JsonSerialize;


@JsonSerialize
@Entity
@Table(name="device_location")
public class DeviceLocation  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	
	@Id
	@Column(name="code")
	private String regionid;
	
	@Column(name="name")
	private String region_name;
	
	@Column(name="description")
	private String description;
	
	@Column(name="void_ind")
	private String void_ind;

	public String getRegionid() {
		return regionid;
	}

	public void setRegionid(String regionid) {
		this.regionid = regionid;
	}

	public String getRegion_name() {
		return region_name;
	}

	public void setRegion_name(String region_name) {
		this.region_name = region_name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getVoid_ind() {
		return void_ind;
	}

	public void setVoid_ind(String void_ind) {
		this.void_ind = void_ind;
	}

	@Override
	public String toString() {
		return "DeviceLocation [regionid=" + regionid + ", region_name="
				+ region_name + ", description=" + description + ", void_ind="
				+ void_ind + "]";
	}
	
	
	
	
}
