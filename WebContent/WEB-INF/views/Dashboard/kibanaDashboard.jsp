<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title> Log Analysis</title>
<link rel="shortcut icon"  href="favicon.ico" />
<!-- Common CSS files of Bootstrap and JQuery -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="assets/css/bootstrap-combined.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/common/ui.jqgrid.css" />
<link rel="stylesheet" type="text/css" href="css/common/jquery-ui.min.css" />
<link rel="stylesheet" type="text/css" href="css/common/jquery.datetimepicker.css" />

<!-- Custom CSS files for this Page -->
<link rel="stylesheet" href="css/custom/specific/nav.css">

<!-- Common Scripts of Bootstrap and JQuery -->
<script src="scripts/common/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="scripts/common/exportData/jspdf/jspdf.min.js"></script>
<script type="text/javascript" src="scripts/common/exportData/jspdf/jspdf.plugin.autotable.js"></script>

<!-- Custom scripts for this Page -->
<!-- <script type="text/javascript" src="scripts/custom/query.js"></script> -->

<!-- Common Scripts of Bootstrap and JQuery -->
<script src="assets/js/bootstrap.min.js"></script>
<script src="scripts/common/jquery.jqGrid.min.js"></script>
<script src="scripts/common/jquery-ui.js"></script>
<script type="text/javascript" src="scripts/common/grid.locale-en.js"></script>
<script type="text/javascript" src="scripts/common/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="scripts/common/exportData/jquery-dateFormat.min.js"></script>

<script src="scripts/custom/tab_menu.js"></script>

<% String session_val = (String)session.getAttribute("modules"); %>
<script>document.domain = '172.21.106.139:5601'</script>

<script type="text/javascript">
	session_obj= '<%=session_val%>';
</script>

</head>
<body style="font-size:11px!important;">
	<div class="container-fluid" style="height: 100%">
	  <%@ include file="../common/header.jsp"%>
  
	 <nav class="navbar navbar-inverse" style="margin-top: 0px">
	      <ul class="nav navbar-nav navbar-xs">
	        <li class="active" id="Dashboard_ELK"><a href="dashboard.html">Dashboard<span class="sr-only">(current)</span></a></li>
	        <li id="ManageThreshold_ELK"><a href="manageThresHold.html">Manage Rule</a></li>
	        <!-- <li id="AlertHistory"><a href="problemHistory.html">Alert History</a></li> -->
	        <li class="dropdown" id="Insights">
	          <a href="#" class="dropdown-toggle" id="Insights" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Insights<span class="caret"></span></a>
	          <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
	            <li id="Analytics"><a href="query.html" style="font-size:11px!important;">Analytics</a></li>
	           <!--  <li><a href="kibanaDashboard.html" style="font-size:11px!important;">Prediction</a></li> -->
	            <!-- <li><a href="variableAnalysis.html">Variable Analysis</a></li> -->
	            <li class="dropdown-submenu" id="Reports"> <a tabindex="-1" href="#" style="font-size:11px!important;">Reports</a>
                      <ul class="dropdown-menu">
							<li id="Prediction">
								<a href="predictionreport.html" style="font-size:11px!important;">Prediction</a> </li>
							<li id="Trends">
								<a href="trendingreport.html" style="font-size:11px!important;">Trends</a> </li>
							<li id="Alert_History">
							 	<a href="alerthistoryreport.html" style="font-size:11px!important;">Alert History</a> </li>
							<li id="LogCorrelation">
								<a href="correlationreport.html" style="font-size:11px!important;">Log Correlation</a> </li>
							<li id="WeblogicReport">
								<a href="weblogicreport.html" style="font-size:11px!important;">Weblogic Report</a> </li>
							<li id="WeblogicDrillDownReport">
								<a href="weblogicdrilldownreport.html" style="font-size:11px!important;">Weblogic DrillDown Report</a> </li>
							<li id="SapApplicationReport">
								<a href="sapapplicationreport.html" style="font-size:11px!important;">Sap Application Report</a> </li>
						    <li id="SapApplicationAdoption">
                           <a href="sapapplicationadoption.html" style="font-size:11px!important;">Sap Application Adoption</a> </li>
                            <li id ="SapPlatformOperation">
                           <a href="sapplatformoperation.html" style="font-size:11px!important;">Sap Platform Operations</a> </li>
                           <li id ="SapSecurityAuditing">
                           <a href="sapsecurityauditing.html" style="font-size:11px!important;">Sap Security Auditing</a> </li>
						</ul>
                    </li>
	          </ul>
	        </li>
	        <!-- <li class="active" id="LogAnalysis"><a href="query.html">Insights</a></li> -->
	        <%-- <li id="LogCorrelation"><a href="correlation.html">Log Correlation</a></li>
	        <li id="VariableAnalysis"><a href="variableAnalysis.html"><spring:message code="label.admin.menu5"></spring:message></a></li>
	        <li class="dropdown" id="Reports">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reports <span class="caret"></span></a>
	          <ul class="dropdown-menu">
	            	<li id="CreateReport"><a href="charts.html">Create Report</a></li>
		            <li id="CreateDashboard"><a href="createuserdashboard.html">Create Dashboard</a></li>
		            <li id="ViewDashboard"><a href="list_dashboard.html">View Dashboard</a></li>
	          </ul>
	        </li> --%>
	      </ul>
	  </nav>
	
	
  <div class="container-fluid innerContainer">
  <iframe id="dashboardFrame" src="https://mosaicauto.lntinfotech.com:5601/app/kibana#/dashboard/Dashboard?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:'2017-02-13T15:54:30.209Z',mode:absolute,to:'2017-03-12T08:25:32.758Z'))&_a=(filters:!(),options:(darkTheme:!f),panels:!((col:1,id:Cpu-Trend-Graph,panelIndex:1,row:1,size_x:12,size_y:2,type:visualization),(col:1,id:Response-Time-Trend-Graph,panelIndex:2,row:3,size_x:12,size_y:2,type:visualization),(col:1,id:CPU-Usage-Prediction,panelIndex:3,row:5,size_x:12,size_y:2,type:visualization),(col:1,id:Memory-Usage-Prediction,panelIndex:4,row:7,size_x:12,size_y:2,type:visualization),(col:1,id:Response-Time-Prediction,panelIndex:5,row:9,size_x:12,size_y:2,type:visualization)),query:(query_string:(analyze_wildcard:!t,query:'*')),title:Dashboard,uiState:(P-4:(vis:(colors:('Actual%20Value':%23447EBC,'Predicted%20Value':%23C15C17),legendOpen:!t)),P-5:(vis:(colors:('Actual%20Value':%233F6833,'Predicted%20Value':%23E24D42)))))"
  	 style="width: 100%; height: 100%;" ></iframe>
 </div>
	<%-- <div class="col-md-3" style="background-color:#e0e0e0; height:100%;">
	
      <div class="container-fluid innerContainer" style="margin-top: 20px; background-color:#e0e0e0; height:33%; overflow-y:auto; overflow-x:hidden;">
	        <form class="form-horizontal">
				<div class="form-group">
					<label class="col-sm-4">Logs:</label>
					<div class="col-sm-8">
						<select class="form-control text-style" id="logs">
						 	<c:forEach var="Collections" items="${Collections}">
								<option class="text-style" value=${Collections.key}>${Collections.value}</option>
							</c:forEach> 
						</select>
					</div>
				</div>
					
				<div class="form-group">
					<label class="col-sm-4">From:</label>
					<div class="col-sm-8">
					<div class="inner-addon left-addon">
						<i class="glyphicon glyphicon-calendar"></i> 
						<input class="form-control text-style" type="text" id="from_datepicker">
					</div>
					</div>
				</div>
					
				<div class="form-group">
					<label class="col-sm-4 text-style">To:</label>
					<div class="col-sm-8">
						<div class="inner-addon left-addon">
							<i class="glyphicon glyphicon-calendar"></i> 
							<input class="form-control text-style" type="text" id="to_datepicker">
						</div>
					</div>
				</div>
			</form>
			<div class="col-md-12" style="padding-right: 20px;">
					<center>
						<a id="advanced_feature"> Click here For Advanced Features</a>
					</center>
			</div>
			<div id="advanced1">
				<form class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-4 text-style">Query Field:</label>
						<div class="col-sm-8">
							<select class="form-control text-style" id="query_field"></select>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-4 text-style">Query Text:</label>
						<div class="col-sm-8">
							<input class="form-control text-style" id="query_text" type="text" value=''>		
						</div>
					</div>
				</form>
			</div>
	  </div> 
			
	  <label style="margin-left: 15px;" class="text-style">Fields:</label>
	  <a onclick="checkallfields()" class="text-style" style="margin-left: 54px;">Check All</a>
	  <a onclick="uncheckallfields()" class="text-style" style="margin-left: 15px;">Uncheck All</a>
      <div class="container-fluid innerContainer" style="background-color:#e0e0e0; height:40%; overflow:auto;">
	        <table class="table" id="resId" cellpadding=0 cellspacing=0>
				<tbody>
				</tbody>
			</table>
   		</div>
   
  		<div  class="panel" style="background-color:#e0e0e0;"> </div>

		<div class="row" style=" margin-left: 140px;">
		   <div class="col-sm-4">
		   		  <form class="form-horizontal" role="form">
					<div class="form-group">
						<div class="col-sm-8">
							<input type="button" class="btn btn-primary btn-sm" id="execute" name="Execute" value=" Execute"/> 
						</div>
					</div>
				</form>
		   </div>
		   <div class="col-sm-4" style=" margin-left: 15px;">
		    <form class="form-horizontal" role="form">
					<div class="form-group">
						<div class="col-sm-8">
		                    <input type="button" id="refresh" class="btn btn-primary btn-sm"  name="Refresh" value="Refresh" />
						</div>
					</div>
				</form>
		   </div>
	    </div>
		</div>

		<div class="col-md-9" style="padding-right: 0px; height: 100%">
			<div style="text-align: right;" >
				<button type="button" id="exportToExcelButton" data-toggle="tooltip" title="Export To Excel" style="background-color:transparent; border-color:transparent;" onclick='exportAndDownload("SmartITReport.xls","SmartITReport","download")'><img alt="Export to Excel" src="images/custom/exportToExcel.png"/></button>
				<!-- <button type="button" id="exportToPdfButton" data-toggle="tooltip" title="Export To PDF" style="background-color:transparent; border-color:transparent;" onclick='exportAndDownload("SmartITReport.pdf","SmartITReport","download")'><img alt="Export to PDF" src="images/custom/exportToPdf.png"/></button> -->
			</div>
			<div class="col-md-12" style="height:85%; overflow:auto;">
				<div class="panel panel-default" id="result_grid">
				</div>
				</div>
		 </div>
		 
	 	<div id="alertDate" title="Alert" class="text-style">
			Please put value inside "" if it contains special characters & space
		</div> 
		<div id="alertLength" title="Alert" class="text-style">
			Value should not exceed more than 20 characters
		</div> 
        <div id="alertSpace" title="Alert" class="text-style"> 
            Please use * for wildcard search eg.space, . , ( , ) , :
        </div>
		
	</div> --%>
		<%@ include file="../common/footer.jsp"%>
	</div>
	
	<script type="text/javascript">
        jQuery(function() {
        	console.log("Inside load iframe");
            var f = $('#alertFrame');
            console.log(f);
            f.load(function() {
            	console.log("Inside load iframe22");
            	//console.log(f.contents().find('.logo'));
                f.contents().find('navbar[name=discover]').hide();
               // f.contents().find('.blog-sidebar').hide();
            })

        })
    </script>
</body>
</html>