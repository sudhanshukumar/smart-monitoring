package com.lnt.smartit.models;


public class LogFileSizeThreshold {
	
	private String host;
	private String log_file_name;
	private double warning_file_size;
	private double critical_file_size;
	private String id;
	
	public double getWarning_file_size() {
		return warning_file_size;
	}
	public void setWarning_file_size(double warning_file_size) {
		this.warning_file_size = warning_file_size;
	}
	public double getCritical_file_size() {
		return critical_file_size;
	}
	public void setCritical_file_size(double critical_file_size) {
		this.critical_file_size = critical_file_size;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getLog_file_name() {
		return log_file_name;
	}
	public void setLog_file_name(String log_file_name) {
		this.log_file_name = log_file_name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return "LogFileSizeThreshold [host=" + host + ", log_file_name="
		+ log_file_name + ", warning_file_size=" + warning_file_size
		+ ", critical_file_size=" + critical_file_size + "]";
	}
}
