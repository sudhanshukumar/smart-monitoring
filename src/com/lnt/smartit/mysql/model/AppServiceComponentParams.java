package com.lnt.smartit.mysql.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="app_service_comp_param")
public class AppServiceComponentParams {
	
	@Id
	@Column(name="id")
	private String appServiceCompParamId;
	
	@ManyToOne
	@JoinColumn(name = "app_service_comp_id", referencedColumnName = "id", insertable = false, updatable = false)
	private AppServiceComponent appServiceComp;
	
	@Column(name="param_name")
	private String paramName;
	
	@Column(name="param_value")
	private String paramValue;
	
	@Column(name="void_ind")
	private String voisInd;

	public String getAppServiceCompParamId() {
		return appServiceCompParamId;
	}

	public void setAppServiceCompParamId(String appServiceCompParamId) {
		this.appServiceCompParamId = appServiceCompParamId;
	}

	public AppServiceComponent getAppServiceComp() {
		return appServiceComp;
	}

	public void setAppServiceComp(AppServiceComponent appServiceComp) {
		this.appServiceComp = appServiceComp;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public String getVoisInd() {
		return voisInd;
	}

	public void setVoisInd(String voisInd) {
		this.voisInd = voisInd;
	}
	
	

}
