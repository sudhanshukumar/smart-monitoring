package com.lnt.smartit.mysql.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="app_log_service_comp_param")
public class AppLogServiceComponentParam {

	@Id
	@Column(name="id")
	private String appServiceCompParamId;
	
	@ManyToOne
	@JoinColumn(name = "app_log_id", referencedColumnName = "id", insertable = false, updatable = false)
	private AppLogMaster log;
	
	@ManyToOne
	@JoinColumn(name = "app_service_comp_id", referencedColumnName = "id", insertable = false, updatable = false)
	private AppServiceComponent appServiceComp;
	
	@Column(name="param_name")
	private String paramName;
	
	@Column(name="param_value")
	private String paramValue;
	
	@Column(name="description")
	private String description;
	
	@Column(name="void_ind")
	private String voidInd;

	public String getAppServiceCompParamId() {
		return appServiceCompParamId;
	}

	public void setAppServiceCompParamId(String appServiceCompParamId) {
		this.appServiceCompParamId = appServiceCompParamId;
	}

	public AppLogMaster getLog() {
		return log;
	}

	public void setLog(AppLogMaster log) {
		this.log = log;
	}

	public AppServiceComponent getAppServiceComp() {
		return appServiceComp;
	}

	public void setAppServiceComp(AppServiceComponent appServiceComp) {
		this.appServiceComp = appServiceComp;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public String getVoidInd() {
		return voidInd;
	}

	public void setVoidInd(String voidInd) {
		this.voidInd = voidInd;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
