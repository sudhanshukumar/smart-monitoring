var ramDataArr = [];
var cpuDataArr = [];

function loadURL(i, parameterName, dataobj){
	 var hostName = dataobj.alertMessageHostName;
		
		if(hostName.indexOf(".") > -1) {
			hostName = hostName.split(".").join("_");
		}
	
		$("#label_ac-8").show();
		$("#ac8_container_small").show();
		
		/*$("#ac1_container_small").hide();
		$("#ac2_container_small").hide();
		$("#ac3_container_small").hide();*/

		$("#ac8_block_1").append("<div class='block_split1' id='ac-8_split1_"
						+ hostName
						+ "'>"
						+ "<table>"
						+ "<thead>"
						+ "<tr>"
						+ "<th>"
						+"System: "+hostName+"<br>"
						+ "</th>"
						+ "</tr>"
						+ "</thead>"
						+ "</table>"
						+ "<table id='ac-8_split2_table_"+hostName+"' border=1>"	+ "<thead>"	+ "<tr>"
						+ "<th style='background-color:grey'>Name</th>"
						+ "<th style='background-color:grey'>"
						+ "Status"
						+ "</th>"
						+ "</tr>"+ "</thead>"
						+ "<tbody style='overflow:scroll;'>"
						+ "</tbody>" + "</table>"+ "</div>");
		
		appendURL(i,dataobj);
		AC8Split1Count++;
		
}

function appendURL(i,dataobj){
	 var hostName = dataobj.alertMessageHostName;
	 var urlname = dataobj.urlDown.urlName;
	 if(urlname.indexOf("/") > -1) {
		 urlname = urlname.split("/")[2];
		 urlname = urlname.split(".").join("_").split(":")[0];
	 }
	 if(hostName.indexOf(".") > -1) {
		hostName = hostName.split(".").join("_");
	 }
	 appendRow= "<tr id='ac-8_spilt2_table_tr_"+urlname+"' onClick=showAC8Details("
					+ (i + 1)
					+ ",id,"
					+ AC8Split1Count
					+ ",'"
					+ dataobj.hazelcastMapName
					+ "','"+
					dataobj.alertMessageHostName
					+ "','"
					+ dataobj.urlDown.urlName
					+"')>"
					+ "<td class='ac-8_col1'>"
					+ "<div id='ac-8_split1_col1_"+ urlname + "'>"
					+ dataobj.urlDown.urlName
					+ "</div></td>"
					+ "<td class='ac-8_col2'>"
					+ "<span id='ac-8_split1_col3_"
					+ urlname
					+ "'>"
					+ dataobj.urlDown.status
					+ "</span></td>"
					+"</tr>";
	$("#ac-8_split2_table_" + hostName).last().append(appendRow);
}

function showAC8Details(blockNumber, id, totalBlocks, vm,host,url) {
	/* Onclick event for left block split 1 for Accordian 4 that is RESPONSETIME */

	/*
	 * clear rest of the split1 blocks toshow only selected block with id as
	 * 'id' result
	 */
	$.ajax({
				type : "POST",
				url : "additionalInfo.html",
				data : "parameterName=URL&addressedMachine="+vm+"&urlName="+url,
				success : function(response) {
					var data = response.split("@#$");
					var result = JSON.parse(data[1]);
					/*
					 * removing if buttons and divs are already created; else on
					 * every click buttons for correlate etc will get added.
					 */
					$("#ac8_split2_table1_" + blockNumber).each(function() {
						this.parentNode.removeChild(this);
					});
					$("#ac-8_split3_" + blockNumber).each(function() {
						this.parentNode.removeChild(this);
					});
					$("#ac8_block_2 div").empty();
					$("#ac8_block_2")
							.append(
									"<div id='ac8_container_small_second'>"
											+ "<div class='block_split2' id='ac-8_rightDiv_"
											+ blockNumber
											+ "' align='center'></div>"
											+ "<div class='buttonsRow' id='ac-8_button_row_"
											+ blockNumber
											+ "'>"
											+ "<table id='ac8_split2_table1_"
											+ blockNumber
											+ "'>"
											+ "<tbody>"
											+ "<tr>"
											+ "<td><input type='button' value='Correlate'	class='rightButton' id='ac-8_btn1_"
											+ blockNumber
											+ "' onClick=correlateURL(id,"
											+ blockNumber
											+ ",'"
											+ host
											+ "')></td>"
											+ "</tr>"
											+ "</tbody>"
											+ "</table>"
											+ "</div>"
											+ "<div class='block_split3' id='ac-8_split3_"
											+ blockNumber
											+ "'>"
											+ "<div class='ac-8_split3_1'>"
											+ "<div class='ac-8_split3_1_1' id='ac-8_split3_1_1_"
											+ blockNumber
											+ "'></div>"
											+ "<div class='ac-8_split3_1_2' id='ac-8_split3_1_2_"
											+ blockNumber
											+ "'></div>"
											+ "<div class='ac-8_split3_1_3' id='ac-8_split3_1_3_"
											+ blockNumber
											+ "'></div>"
											+ "</div>"
											+ "<div class='ac-8_split3_2' id='ac-8_split3_2_"
											+ blockNumber
											+ "'>"
											+ "<div class='ac-8_split3_2_1' id='ac-8_split3_2_1_"
											+ blockNumber
											+ "' align='center'></div>"
											+ "</div>" + "</div>" + "</div>");
					/* Hiding correlate and prediction charts */
					$("#ac-8_split3_" + blockNumber + "").hide();
					/* Right div's chart starts here. */

					 var yAxisData=[];
					 var xAxisData=[];
					 xAxisData.push('time');
					 yAxisData.push('Status');
						for (k = 0; k < result.length; k++) {
							if(result[k].status == "OK"){
								result[k].status = 1;
								
							}
								
							else if(result[k].status == "FAIL"){
								result[k].status = 0;
								
							}
								
						}

						for (k = 0; k < result.length; k++) {
							yAxisData.push(result[k].status);
							xAxisData.push(result[k].dataReceivedTime);
						}
						//c3 charts code
						var chart = c3.generate({
	  						bindto: '#ac-8_rightDiv_' + blockNumber,
	  						data: {
	  					        x: 'time',
	  					        columns: [
	  				              xAxisData,         
	  				              yAxisData
	  					        ]
	  					    },
	  					    title : {
								text : url+' status for last 12 hours '
							},
	  					    
	  					    subchart: {
	  					        show: true
	  					    },
	  					    zoom: {
	  					        enabled: true
	  					    },
	  					    axis : {
	  					        x : {
	  					        	label:'Time',
	  					            type : 'timeseries',
	  					            tick: {
	  					            	fit:true,
	  					            	count: result.length,
	  					            	format:'%Y-%m-%d %I:%M:%S'/* function (x) { return x.getFullYear(); } */
	  					              //format: '%Y' // format string is also available for timeseries data
	  					            }
	  					        },
	  					        y : {
	  					        	label:'Status'
	  					        }
	  					    }
	  					});
				}
			});
}

function correlateURL(id, blockNumber, host) {
	var ram_threshold =null;
	var cpu_threshold = null;
	var mapName=null,osType=null;
	
	$.ajax({
		type : "POST",
		url : "getMapNameForChart.html",
		data : "hostname="+host+"&component=Hbase",
		async : false,
		success : function(response) {
			var result = JSON.parse(response);
			mapName=result.mapName
			osType=result.osType;
		}
	});
	
	console.log('MapName--'+mapName);
	
	$.ajax({
				type : "POST",
				url : "thresholdRamCpu.html",
				data : "threshold_parameter=RAM&threshold_hostName=" + host,
				async : false,
				success : function(response) {
					ram_threshold=response;
				}
			});
	
	$.ajax({
		type : "POST",
		url : "thresholdRamCpu.html",
		data : "threshold_parameter=CPU&threshold_hostName=" + host,
		async : false,
		success : function(response) {
			cpu_threshold=response;
		}
	});
	
	console.log("ram_threshold :::::::::" + ram_threshold);
	console.log("cpu_threshold :::::::::" + cpu_threshold);
	$("#ac-8_split3_" + blockNumber + "").show();
	$
	.ajax({

		type : "POST",

		url : "additionalInfo.html",

		data : "parameterName=RAM&addressedMachine="+mapName+"&osType="+osType,

		success : function(response) {
		
			var result = response.split("@#$");
			var objList = JSON.parse(result[1]);
			console.log(objList);
			console.log("OSTYPE = "+osType);
			/** **data for chart** */
			 var yAxisDataRAM=[];
			 var xAxisDataRAM=[];
			 var yAxisDataCPU=[];
			 var xAxisDataCPU=[];
			 yAxisDataCPU.push('CPU Memory used');
			 xAxisDataCPU.push('timeCPU');
			 yAxisDataRAM.push('RAM Memory used');
			 xAxisDataRAM.push('timeRAM');
			
			 for(b=0;b<objList.length;b++){
					if(osType.toLowerCase() == "windows"){
						xAxisDataRAM.push(objList[b].dataReceivedTime);
						yAxisDataRAM.push(objList[b].memoryUsage);
						ramDataArr.push([ objList[b].dataReceivedTime,
					               objList[b].memoryUsage ]);
					}
					else if(osType.toLowerCase() == "linux"){
						xAxisDataRAM.push(objList[b].dataReceivedTime);
						yAxisDataRAM.push(objList[b].memUsed);
						ramDataArr.push([ objList[b].dataReceivedTime,  
								   objList[b].memUsed ]);
					}
				}
			console.log(ramDataArr);
			$
			.ajax({

				type : "POST",

				url : "additionalInfo.html",

				data : "parameterName=CPU&addressedMachine="+mapName+"&osType="+osType,

				success : function(response) {
					var result = response.split("@#$");
					var chartArray = JSON.parse(result[1]);
					
					for (var chart = 0; chart < chartArray.length; chart++) {
						if (osType.toLowerCase() == "windows") {
							yAxisDataCPU.push(chartArray[chart].cpuUsageInPercent);
							xAxisDataCPU.push(chartArray[chart].dataReceivedTime);
							cpuDataArr.push([ chartArray[chart].dataReceivedTime,
												chartArray[chart].cpuUsageInPercent]);
						} else if (osType.toLowerCase() == "linux") {
							yAxisDataCPU.push(chartArray[chart].loadAverage5min);
							xAxisDataCPU.push(chartArray[chart].cpudataReceivedTime);
							cpuDataArr.push([chartArray[chart].cpudataReceivedTime,
							chartArray[chart].loadAverage5min ]);
						}
					}
					console.log(cpuDataArr);
					console.log(xAxisDataCPU);
					console.log(yAxisDataCPU);
					//c3 charts code
					console.log(xAxisDataRAM);
					console.log(yAxisDataRAM);
					//alert($('#ac-8_split3_1_1_' + blockNumber).width());
					//$("#ac-8_split3_" + blockNumber + "").css('width:'+$('#ac-8_split3_1_1_' + blockNumber).width()+'px');
			var chart = c3.generate({
  						bindto: '#ac-8_split3_1_1_' + blockNumber,
  						data: {
  					        //x: 'time',
  							xs:{
  								'CPU Memory used':'timeCPU',
  								'RAM Memory used':'timeRAM'
  							},
  					        columns: [
  				              xAxisDataCPU,     
  				            xAxisDataRAM,     
  				            yAxisDataCPU,
  				          yAxisDataRAM
  					        ]
  					    },
  					  title : {
							text : 'CPU AND RAM Usage Trend of '+host
						},
  					    
  					    subchart: {
  					        show: true
  					    },
  					    zoom: {
  					        enabled: true
  					    },
  					    axis : {
  					        x : {
  					        	label:'Time',
  					            type : 'timeseries',
  					            tick: {
  					            	fit:true,
  					            	count: chartArray.length,
  					                format:'%Y-%m-%d %I:%M:%S' 
  					            }
  					        },
  					        y : {
  					        	label:'Memory Used'
  					        }
  					    }
  					});
				}
			});
		}
	});
}

function refreshCorrelatedGraphs(starttime, endTime, blockNumber) {
	var corrRamArr = [];
	var corrCpuArr = [];
	var corrIOArr = [];

	for (r = 0; r < ramDataArr.length; r++) {
		if (ramDataArr[r][0] > starttime && ramDataArr[r][0] < endTime) {
			corrRamArr.push([ ramDataArr[r][0], ramDataArr[r][1] ]);
		}
	}
			RAMChart = $('#ac-8_split3_1_1_' + blockNumber).highcharts();
			RAMChart.series[0].setData(corrRamArr);
			RAMChart.xAxis[0].setExtremes();
			
	for (s = 0; s < cpuDataArr.length; s++) {
		if (cpuDataArr[s][0] > starttime && cpuDataArr[s][0] < endTime) {
			corrCpuArr.push([ cpuDataArr[s][0], cpuDataArr[s][1] ]);
		}
	}
//			CPUChart = $('#ac-8_split3_1_2_' + blockNumber).highcharts();
			RAMChart.series[1].setData(corrCpuArr);
//			RAMChart.xAxis[1].setExtremes();
}
