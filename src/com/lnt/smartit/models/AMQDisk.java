/**
 * 
 */
package com.lnt.smartit.models;

import java.util.ArrayList;
import java.util.List;

import com.lnt.smartit.models.FileSystem;

/**
 * @author 299273
 *
 */
public class AMQDisk implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<FileSystem> fileSystem = new ArrayList<FileSystem>();
	private Double totalSize;
	private Double totalUsed;
	private Double totalAvail;
	private Double totalUsePercent;
	//Thread limit parameters
	private Double thresholdlimit_TotalSize;
	private Double thresholdlimit_TotalUsed;
	private Double thresholdlimit_TotalAvail;
	private Double thresholdlimit_TotalUsePercent;
	private double threshold_high_val;
	private double threshold_low_val;
	
	public double getThreshold_high_val() {
		return threshold_high_val;
	}

	public void setThreshold_high_val(double threshold_high_val) {
		this.threshold_high_val = threshold_high_val;
	}

	public double getThreshold_low_val() {
		return threshold_low_val;
	}

	public void setThreshold_low_val(double threshold_low_val) {
		this.threshold_low_val = threshold_low_val;
	}

	public Double getThresholdlimit_TotalSize() {
		return thresholdlimit_TotalSize;
	}

	public void setThresholdlimit_TotalSize(Double thresholdlimit_TotalSize) {
		this.thresholdlimit_TotalSize = thresholdlimit_TotalSize;
	}

	public Double getThresholdlimit_TotalUsed() {
		return thresholdlimit_TotalUsed;
	}

	public void setThresholdlimit_TotalUsed(Double thresholdlimit_TotalUsed) {
		this.thresholdlimit_TotalUsed = thresholdlimit_TotalUsed;
	}

	public Double getThresholdlimit_TotalAvail() {
		return thresholdlimit_TotalAvail;
	}

	public void setThresholdlimit_TotalAvail(Double thresholdlimit_TotalAvail) {
		this.thresholdlimit_TotalAvail = thresholdlimit_TotalAvail;
	}

	public Double getThresholdlimit_TotalUsePercent() {
		return thresholdlimit_TotalUsePercent;
	}

	public void setThresholdlimit_TotalUsePercent(
			Double thresholdlimit_TotalUsePercent) {
		this.thresholdlimit_TotalUsePercent = thresholdlimit_TotalUsePercent;
	}

	public List<FileSystem> getFileSystem() {
		return fileSystem;
	}

	public void setFileSystem(List<FileSystem> fileSystem) {
		this.fileSystem = fileSystem;
	}
 
	public void addFileSystem(FileSystem fileSystem) {
		this.fileSystem.add(fileSystem);
	}
	
	public Double getTotalSize() {
		return totalSize;
	}

	public void setTotalSize(Double totalSize) {
		this.totalSize = totalSize;
	}

	public Double getTotalUsed() {
		return totalUsed;
	}

	public void setTotalUsed(Double totalUsed) {
		this.totalUsed = totalUsed;
	}

	public Double getTotalAvail() {
		return totalAvail;
	}

	public void setTotalAvail(Double totalAvail) {
		this.totalAvail = totalAvail;
	}

	public Double getTotalUsePercent() {
		return totalUsePercent;
	}

	public void setTotalUsePercent(Double totalUsePercent) {
		this.totalUsePercent = totalUsePercent;
	}

	@Override
	public String toString() {
		return "AMQDisk [fileSystem=" + fileSystem + ", totalSize=" + totalSize
				+ ", totalUsed=" + totalUsed + ", totalAvail=" + totalAvail
				+ ", totalUsePercent=" + totalUsePercent
				+ ", thresholdlimit_TotalSize=" + thresholdlimit_TotalSize
				+ ", thresholdlimit_TotalUsed=" + thresholdlimit_TotalUsed
				+ ", thresholdlimit_TotalAvail=" + thresholdlimit_TotalAvail
				+ ", thresholdlimit_TotalUsePercent="
				+ thresholdlimit_TotalUsePercent + "]";
	}
}
