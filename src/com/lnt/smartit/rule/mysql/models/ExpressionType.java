/**
 * 
 */
package com.lnt.smartit.rule.mysql.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author 10612068
 *
 */
@Entity
@Table(name = "expression_type")
public class ExpressionType {
	
	@Id
	@Column(name = "code")
	private String code;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "exp_type_impl_class_name")
	private String exprClass;
	
	@Column(name = "void_ind")
	private String voidInd;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getExprClass() {
		return exprClass;
	}
	public void setExprClass(String exprClass) {
		this.exprClass = exprClass;
	}
	public String getVoidInd() {
		return voidInd;
	}
	public void setVoidInd(String voidInd) {
		this.voidInd = voidInd;
	}
	
	

}
