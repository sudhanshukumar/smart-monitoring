package com.lnt.smartit.mysql.daoImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.CloudSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lnt.smartit.helper.HbmHelper;
import com.lnt.smartit.mysql.dao.LogDao;
import com.lnt.smartit.mysql.model.AppDeviceMaster;
import com.lnt.smartit.mysql.model.AppLogMaster;
import com.lnt.smartit.mysql.model.AppLogServiceComponentParam;
import com.lnt.smartit.mysql.model.AppServiceCompParams;
import com.lnt.smartit.mysql.model.AppServiceComponent;
import com.lnt.smartit.mysql.model.AppServiceComponentParams;
import com.lnt.smartit.mysql.model.Applications;
import com.lnt.smartit.mysql.model.LogParams;
import com.lnt.smartit.services.ChartService;

@Repository
@Transactional
public class LogDaoImpl implements LogDao {
	
	@Autowired
	HbmHelper hbmHelper;
	
	@Autowired
	ChartService chartService;
	
	@Autowired
	SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public String getAppLogByHostAndLogType(String appId,String hostname, String logType) {
		List<AppLogMaster> logList = new ArrayList<AppLogMaster>();
		logList = (List<AppLogMaster>) hbmHelper.createQuery("from AppLogMaster al where al.app.appId='"+appId+"' and al.device.deviceId = '"+hostname+"' and al.logType.logTypeId='"+logType+"'", null);
		return logList.get(0).getLogId();
	}

	@SuppressWarnings("unchecked")
	@Override
	public LogParams getAppLogParamsByLogIdAndParam(String logId, String paramName) {
		List<LogParams> paramsList = new ArrayList<LogParams>();
		paramsList = (List<LogParams>) hbmHelper.createQuery("from LogParams l where l.log.logId='"+logId+"' and l.paramName = '"+paramName+"'", null);
		return paramsList.get(0);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String getOSFromDeviceId(String deviceId){
		List<AppDeviceMaster> paramsList = new ArrayList<AppDeviceMaster>();
		paramsList = (List<AppDeviceMaster>) hbmHelper.createQuery("from AppDeviceMaster ap where ap.deviceId='"+deviceId+"'", null);
		return paramsList.get(0).getDevicetype().getDeviceTypeCode();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public String getLogTypeByOS(String os){
		Session session = sessionFactory.getCurrentSession();
		String query = "SELECT al.logType.logTypeId, al.logType.logTypeName FROM AppLogMaster al INNER JOIN al.device dev INNER JOIN dev.devicetype type where dev.deviceId=al.device.deviceId and type.deviceTypeCode='"+os+"'";
		List<Object> result = session.createQuery(query).list();
		for (Object obj : result) {
		       Object[] row = (Object[])obj;
		       if(row[1].toString().contains("Sys")){
		    	   return row[0].toString();
		       }
		}
		return null; 
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public String getAppServiceComponentId(String appId, String compType, String compCode){
		List<AppServiceComponent> logList = new ArrayList<AppServiceComponent>();
		logList = (List<AppServiceComponent>) hbmHelper.createQuery("from AppServiceComponent a where a.app.appId='"+appId+"' and a.typecode.serviceCompTypeId = '"+compType+"' and a.serviceComponent.serviceCompId='"+compCode+"'", null);
		return logList.get(0).getAppServiceCompId();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String getLogServiceComponentParamValue(String logId, String appServiceCompId){
		List<AppLogServiceComponentParam> logList = new ArrayList<AppLogServiceComponentParam>();
		logList = (List<AppLogServiceComponentParam>) hbmHelper.createQuery("from AppLogServiceComponentParam ap where ap.log.logId='"+logId+"' and ap.appServiceComp.appServiceCompId = '"+appServiceCompId+"'", null);
		return logList.get(0).getParamValue();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String getAppIdByName(String appName){
		List<Applications> appList = new ArrayList<Applications>();
		appList = (List<Applications>) hbmHelper.createQuery("from Applications ap where ap.appName like'"+appName+"'", null);
		return appList.get(0).getAppId();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AppLogServiceComponentParam> getCollectionNames(String appServiceCompId) {
		List<AppLogServiceComponentParam> logList = new ArrayList<AppLogServiceComponentParam>();
		logList = (List<AppLogServiceComponentParam>) hbmHelper.createQuery("from AppLogServiceComponentParam ap where ap.appServiceComp.appServiceCompId = '"+appServiceCompId+"' and ap.paramName='collection-name'", null);
		return logList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AppServiceCompParams> getDbConfig(String appId) {
		System.out.println("appId  = "+ appId);
		Session session = sessionFactory.getCurrentSession();
		List<AppServiceComponentParams> paramsList = null;
		List<AppServiceCompParams> paramsList2= new ArrayList<AppServiceCompParams>();
		String queryString = "select p.paramName, p.paramValue,c.serviceCompId, t.serviceCompTypeId,s.id from AppServiceComponentParams p ,AppServiceComponent s, ServiceComponent c ,ServiceComponentType t ,Applications a " +
				"where  c.serviceCompId=s.serviceComponent and (c.serviceCompId='SOLR' or c.serviceCompId='ACTIVEMQ' or c.serviceCompId='KAFKA' or c.serviceCompId='MYSQL' or c.serviceCompId='HAZELCAST')and p.appServiceComp=s.id and s.typecode=t.serviceCompTypeId and a.appId='"+ appId +"' and a.appId=s.app" ;
		Query query = session.createQuery(queryString);
		paramsList=(List<AppServiceComponentParams>) query.list();
		
		ArrayList<String> str=new ArrayList<String>();
		HashMap<String, Object[]> mappedRecords = new HashMap<String, Object[]>();
		
		int mappingKey = 1;
		for (Object o : paramsList) {
			Object[] obj = (Object[]) o;
			str.add(mappingKey+"_"+(String)obj[1]);
			mappedRecords.put(mappingKey+"", obj);
			mappingKey++;
		}

		for (String str1 : str) {
			int mappingKeyFound = Integer.parseInt((String)((str1.split("_"))[0]));
			Object[] obj = mappedRecords.get(mappingKeyFound+"");
			AppServiceCompParams serObj = new AppServiceCompParams();
			serObj.setParamName(obj[0] != null ? obj[0].toString() : "");
			serObj.setParamValue(obj[1] != null ? obj[1].toString() : "");
			serObj.setServiceComp(obj[2] != null ? obj[2].toString() : "");
			serObj.setServiceCompType(obj[3] != null ? obj[3].toString() : "");
			serObj.setServiceCompId(obj[4] != null ? obj[4].toString() : "");
			paramsList2.add(serObj);
	}
		return paramsList2;
	}

	@Override
	public List<String> getTopicName(String serviceCompId) {
		Session session = sessionFactory.getCurrentSession();
		String queryString = "select distinct p.paramValue from AppLogServiceComponentParam p , AppServiceComponent a WHERE p.appServiceComp= a.appServiceCompId and a.appServiceCompId='"+serviceCompId+"'";
		Query query = session.createQuery(queryString);
		List<String> topicName= query.list();
		return topicName;
	}
	@Override
	public List<Object> getBasicQueryNoFilter(com.lnt.smartit.solr.pojo.Query uquery) throws Exception{
			
			String solrURL = uquery.getUrl() + "/solr";
		    CloudSolrServer solrSvr = new CloudSolrServer(solrURL);
		    solrSvr.setDefaultCollection(uquery.getCollection());
	    SolrQuery querySlr = new SolrQuery();
		querySlr.setQuery( "*:*" );
	    QueryResponse res = solrSvr.query(querySlr);
	    SolrDocumentList docs = res.getResults();
		
		
		System.out.println("docs"+docs);
		List<Object> records=new ArrayList<Object>();
		for (SolrDocument doc : docs) {
			
			Map<String, Object> docMap = doc.getFieldValueMap();
			Iterator itr = (docMap.keySet()).iterator();
			Map<String, Object> docReturnMap = new HashMap<String, Object>(); 
			while( itr.hasNext()) {
				String key = (String)itr.next();
				Object value = docMap.get(key);
				docReturnMap.put(key, value);
			}
			records.add(docReturnMap);
		}
		//uquery.setResults(records);
		return records;
	}
	
	@Override
	public List<Object> getBasicQueryForLogs(com.lnt.smartit.solr.pojo.Query uquery) throws Exception{
			
			String solrURL = uquery.getUrl() + "/solr";
		    CloudSolrServer solrSvr = new CloudSolrServer(solrURL);
		    solrSvr.setDefaultCollection(uquery.getCollection());
	    SolrQuery querySlr = new SolrQuery();
		querySlr.setQuery(uquery.getQuery());
		
		if (uquery!=null && uquery.getStart_facet()!=null && uquery.getEnd_facet()!=null) {
			String PrimFilter = chartService.getPrimaryFilter(uquery
					.getCollection());
			querySlr.addFilterQuery(PrimFilter + ":[" + uquery.getStart_facet()
					+ " TO " + uquery.getEnd_facet() + "]");
		}
		QueryResponse res = solrSvr.query(querySlr);
	    SolrDocumentList docs = res.getResults();
		
		
		System.out.println("docs"+docs);
		List<Object> records=new ArrayList<Object>();
		for (SolrDocument doc : docs) {
			
			Map<String, Object> docMap = doc.getFieldValueMap();
			Iterator itr = (docMap.keySet()).iterator();
			Map<String, Object> docReturnMap = new HashMap<String, Object>(); 
			while( itr.hasNext()) {
				String key = (String)itr.next();
				Object value = docMap.get(key);
				docReturnMap.put(key, value);
			}
			records.add(docReturnMap);
		}
		//uquery.setResults(records);
		return records;
	}
}
