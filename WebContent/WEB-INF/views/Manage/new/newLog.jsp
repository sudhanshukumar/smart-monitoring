<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Smart IT :: Add Logs</title>


<!-- common css -->
<link href="css/custom/general/style1366.css" rel="stylesheet" />
<link href="css/custom/general/style1024.css" rel="stylesheet" />
<link href="css/custom/general/bluetabs.css" rel="stylesheet" />

<!-- import css -->
<link href="css/common/ui.jqgrid.css" rel="stylesheet" type="text/css" />
<link href="css/common/jquery-ui-1.9.1.custom.css" rel="stylesheet"
	type="text/css" />

<!-- jQuery scripts -->
<script src="scripts/common/jquery-1.9.1.js"></script>
<script src="scripts/common/grid.locale-en.js"></script>
<script src="scripts/common/jquery.jqGrid.min.js"></script>
<script src="scripts/common/jquery-ui.js"></script>



<!-- custom script -->
<script src="scripts/custom/newLog.js"></script>
<script src="scripts/common/common.js"></script>

<!-- jQuery style sheets -->
<link href="css/common/jquery-ui.css" rel="stylesheet" />



<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
</style>
</head>
<body>


	<header>
		<%@ include file="../../common/header.jsp"%>
	</header>


	<nav>
		<%@ include file="../../Menu/admin.jsp"%>
	</nav>

	<div id="newLogInnerContainer" class="allInnerContainer">
		<div id="alert"></div>

		<div id="newLog_Details">

			<div id="headingManageLogs" class="topic col1"></div>

			<hr id="horizontalLine">


			<table id="addNewLog" class="itPredictInp">
				<tr>
					<td class="col1"><label> Host Name <span class="mandatory">*</span></label></td>
					<td class="col2"><input type="text" id="hostName"
						placeholder="Provide Host Name" /></td>
				</tr>

				<tr>
					<td class="col1"><label> Log File Name <span class="mandatory">*</span></label></td>
					<td class="col2"><input type="text" id="logFileName"
						placeholder="Provide Log File Name" /></td>
				</tr>
				<tr>
					<td class="col1"><label> Warning Threshold Size </label></td>
					<td class="col2"><input type="text" id="warningThresholdSize"
						placeholder="Provide Warning Threshold file size in Kb" /></td>
				</tr>
				<tr>
					<td class="col1"><label> Critical Threshold Size </label></td>
					<td class="col2"><input type="text" id="criticalThresholdSize"
						placeholder="Provide Critical Threshold file size in Kb" /></td>
				</tr>
				<tr>
					<td>
						<div id="addLogs_buttons" class="buttonPosition">
							<table>
								<tr>
									<td>
										<button id="addLogs_submit"
											class="buttonClass buttonClass_submit">Submit</button>
									</td>
									<td><button id="addLogs_cancel"
											class="buttonClass buttonClass_cancel">Cancel</button></td>
								</tr>
							</table>


						</div>
					</td>
				</tr>

			</table>
			<hr id="horizontalLine">
			<div id="preview_log_buttons" class="buttonPosition">
				<table>
					<tr>
						<td>
							<button id="preview_ok" class="buttonClass buttonClass_submit">Ok</button>
						</td>

					</tr>
				</table>

			</div>
		</div>


	</div>

	<div id="alert">
	</div>
	<footer>
		<%@ include file="../../common/footer.jsp"%>
	</footer>



</body>
</html>