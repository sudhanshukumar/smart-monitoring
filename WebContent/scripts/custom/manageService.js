/**
 * 
 */
$(document).ready(function() {
	$("#confirmDelete").hide();
	$("#alert").hide();
	$("#addNewService").click(function() {
		
		window.location.href = "addNewService.html";
	});
	manageServiceGrid();

});

function manageServiceGrid() {
	$("#addedServiceGrpHistory")
	.jqGrid(
			{
				jsonReader : {
					root : "gridData", // an array that contains the
										// actual data
					page : "page", // current page of the query
					total : "total", // total pages for the query
					records : "records",// total number of records for
										// the query
					repeatitems : true,
					id : "softwareId" // the unique id of the row
				},
				url : 'populateServiceGrid.html',
				datatype : "json",
				mtype : "POST",
				colNames : [ 'Service Id','Host Name','Service Name','Service Description', 'Actions' ],
				colModel : [
						{
							name : 'softwareId',
							label : 'Service Id',
							align : 'left',
							width : 30,
							hidden:true,
							sortable: true
						},
						{
							name : 'hostName',
							label : 'Host Name',
							align : 'left',
							width : 50,
							sortable: true
						},
						{
							name : 'softwareName',
							label : 'Service Name',
							align : 'left',
							width : 50,
							sortable: true
						},
						{
							name : 'description',
							label : 'Service Description',
							align : 'left',
							width : 70,
							sortable: true
						},
						{
							width : 80,
							fixed : true,
							sortable : false,
							resize : false,
							formatter : function(cellvalue, options,
									rowObject) {

								return addedServiceActionButtons(cellvalue,
										options, rowObject);
							}
						} ],
				pager : '#addedServiceGrpPager',
				rowNum : 10,
				rowList : [ 10, 20, 30 ],
				viewrecords : true,
				gridview : true,
				loadonce: true,
				width : 900,
				height: 300
			});

}

function addedServiceActionButtons(cellvalue, options, rowObject) {
var previewServiceLink= '<img title="Preview" onClick=previewService("'
    +encodeURI(JSON.stringify(rowObject))
    +'") src="images/common/preview-16.png"/>';

var updateServiceLink = '<img title="Edit"  onClick=editService("'
	+ encodeURI(JSON.stringify(rowObject))
	+ '"); src="images/common/edit-6-16.png" />';

var deleteServiceLink = '<img title="Delete" onClick=deleteService("'
	+ encodeURI(JSON.stringify(rowObject))
	+ '") src="images/common/delete-3-16.png"/>';

var actionLink = "&nbsp;" + previewServiceLink + "&nbsp;" + "&nbsp;" + updateServiceLink+ "&nbsp;" + "&nbsp;" + deleteServiceLink;
return actionLink;
}

function deleteService(value){
	$("#confirmDelete").show();
	$( "#confirmDelete" ).dialog({
		 resizable: false,
		 autoOpen: false,
		 height:200,
		 width: 340,
		 position: [485, 185],
		 modal: true,
		 buttons: {
			 "Yes": function() {
				 	var dataJSON = JSON.parse(decodeURI(value));
				 	var softwareId=dataJSON.softwareId;
				 	$.ajax({
				 		type : "POST",
				 		url : "deleteService.html",
				 		data : "softwareId=" + softwareId, 
				 		success : function(response) {
				 			alert(response);
				 			window.location.href = "manageService.html";
				 		},
				 		cache: false
				 	}); 
			 },
				Cancel: function() {
	 							$( this ).dialog( "close" );
	 						}
		 }
		 
	});
	
	$("#confirmDelete").dialog("open");
	$(".ui-dialog-title").text('Delete?');
	$(".ui-dialog-titlebar-close").hide();
	$("#confirmDelete").text("Delete this Service ?");
	
}
function editService(value){
	var dataJSON = JSON.parse(decodeURI(value));
	var softwareId=dataJSON.softwareId;	
	window.location.href =  "addNewService.html?softwareId="+softwareId+"&action=edit";
	
}
function previewService(value){
	
	var dataJSON = JSON.parse(decodeURI(value));
	var softwareId=dataJSON.softwareId;
	window.location.href =  "addNewService.html?softwareId="+softwareId+"&action=preview";	
}