function loadWindowsCPU(i, parameterName, dataobj){
	 var hostName = dataobj.alertMessageHostName;
		
		//check for . since it gives error as illegal character
		if(hostName.indexOf(".") > -1) {
			hostName = hostName.split(".").join("_");
		}
		console.log('Param name is CPU for linux in generateskeleton');
		$("#label_ac-2").show();
		$("#ac2_container_small").show();
		$("#ac2_block_1").append(
				 "<div class='block_split1' id='ac-2_split1_"
						+ hostName
						+ "' onClick=showAC2Details("+ (i + 1)+ ",id,"+ AC2Split1Count+ ",'"
						+ dataobj.hazelcastMapName+ "','"
						+ parameterName
						+ "','"
						+ dataobj.osType	
						+ "','"
						+dataobj.alertMessageHostName
						+"')>"
						+ "<table>"+ "<thead>"+ "<tr>"+ "<th>"
						+ strings['left_block_split1.header']
						+ "<span id='ac-2_split1_header_label'>"
						+ dataobj.alertMessageHostName
						+ "OS = "
						+ dataobj.osType+"  <br>"
						+ "  C = "
						+ dataobj.winCPU.critical_High
						+ "%  W = "
						+ dataobj.winCPU.warning_High
						+ "%"
						+ "</span> <br>"
						+ "<span class='alertdatetime' id='ac2_split1_datespan_"
						+hostName+"'>"+new Date(dataobj.alertMessageDatetime).toString()+"</span>"
						+"</td>"
						+ "</tr>"
						+ "</thead>"+ "</table>"
						+ "<table>"
						+ "<tbody>"
						+"<tr>"
						+ "<td class='ac-2_col1'> "
						+ strings['ac-2_left_block_split1.column1']
						+ "<br><span id='ac-2_split1_col1_"
						+ hostName
						+ "'>"
						+ dataobj.winCPU.cpuUsageInPercent.toFixed(2)
						+ " %</span></td>"
						+ "</tr>" +

						"</tbody>" + "</table>" +

						"</div>");
		console.log('WinCPU div got appended with id '+'ac-2_split1_'+ hostName);
		AC2Split1Count++;
	
}


function loadLinuxCPU(i, parameterName, dataobj){
	 var hostName = dataobj.alertMessageHostName;
		
		//check for . since it gives error as illegal character
		if(hostName.indexOf(".") > -1) {
			hostName = hostName.split(".").join("_");
		}
	console.log('Param name is CPU for WINDows in generateskeleton');
	$("#label_ac-2").show();
	$("#ac2_container_small").show();
	$("#ac2_block_1").append(
			 "<div class='block_split1' id='ac-2_split1_"
					+ hostName
					+ "' onClick=showAC2Details("+ (i + 1)+ ",id,"+ AC2Split1Count+ ",'"
					+ dataobj.hazelcastMapName+ "','"
					+ parameterName
					+ "','"
					+ dataobj.osType	
					+ "','"
					+dataobj.alertMessageHostName
					+"')>"
					+ "<table>"+ "<thead>"+ "<tr>"+ "<th>"
					+ strings['left_block_split1.header']
					+ "<span id='ac-2_split1_header_label'>"
					+ dataobj.alertMessageHostName
					+ " OS = "
					+ dataobj.osType +" </br>"
					+ " C = "
					+ dataobj.linuxCPU.threshold_high_val
					+ "%  W = "
					+ dataobj.linuxCPU.threshold_low_val
					+ "% </span> <br> "
					+ "<span class='alertdatetime' id='ac2_split1_datespan_"
					+hostName+"'>"+new Date(dataobj.alertMessageDatetime).toString()+"</span>"
					+"</th>"+ "</tr>"
					+ "</thead>"+ "</table>"
					+ "<table>"
					+ "<tbody>"
					+"<tr>"
					+ "<td class='ac-2_column1'> "
					+ strings['ac-2_left_block_split1.column2']
					+ "<br><span id='ac-2_split1_col1_"
					+ hostName
					+ "'>" 
					+ dataobj.linuxCPU.loadAverage5min		
					+"</span>"
					+ " </td>"
					+ "<td class='ac-2_column1'>"
					+ strings['ac-2_left_block_split1.column4']
					+ "<br><span id='ac2_split1_col2_"
					+ hostName
					+ "'>"
					+ dataobj.linuxCPU.loadAverage10min
					+"</span>"
					+ "</td>"
					+ "<td class='ac-2_column1'>"
					+ strings['ac-2_left_block_split1.column3']
					+ "<br><span id='ac2_split1_col3_"
					+ hostName
					+ "'>" 
					+ dataobj.linuxCPU.loadAverage15min
					+ "</span>"
					+ "</td>"
					+ "</tr>" +

					"</tbody>" + "</table>" +

					"</div>");
	console.log('LinuxCPU div got appended with id '+'ac-2_split1_'+ hostName);
	AC2Split1Count++;
}

function showAC2Details(blockNumber, id, totalBlocks, vm, param,osType,host) {

	/* Onclick event for left block split 1 for Accordian 2 that is CPU */

	/*
	 * clear rest of the split1 blocks toshow only selected block with id as
	 * 'id' result
	 */
	 var yAxisData=[];
	 var xAxisData=[];

	 xAxisData.push('time');
	$.ajax({

		type : "POST",

		url : "additionalInfo.html",

		data : "parameterName="+param+"&addressedMachine="+vm+"&osType="+osType,

		success : function(response) {
			console.log(response);
			/*
			 * response will consist of RAM object, processlist,heaplist,chart
			 * data seperated by @#$
			 */
			var result = response.split("@#$");
			var chartArray = JSON.parse(result[1]);
			for ( var chart = 0; chart < chartArray.length; chart++) {
				if (osType == "Windows") {
					yAxisData[0]='CPU Usage in %';
					xAxisData.push(chartArray[chart].dataReceivedTime);
					yAxisData.push(chartArray[chart].cpuUsageInPercent);
				} else if (osType == "Linux") {
					yAxisData[0]='Load Average 5 min';
					xAxisData.push(chartArray[chart].cpudataReceivedTime);
					yAxisData.push(chartArray[chart].loadAverage5min);
				}
			}
			$("#ac2_block_2 div").empty(); // this is to refresh
											// same graph
			$("#ac2_block_2").append(
					"<div class='block_split2' id='ac-2_rightDiv_"
							+ blockNumber + "'></div>");

			$('#ac-2_rightDiv_' + blockNumber).show();
			//c3 charts
			var chart = c3.generate({
				bindto: '#ac-2_rightDiv_' + blockNumber,
				data: {
			        x: 'time',
			        columns: [
		              xAxisData,         
		              yAxisData
			        ]
			    },
			  title: {
				  text : 'CPU Trend for last 12 hours of '+host,
				},
			    subchart: {
			        show: true
			    },
			    zoom: {
			        enabled: true
			    },
			    axis : {
			        x : {
			        	label:'Time',
			            type : 'timeseries',
			            tick: {
			            	fit:true,
			            	count: chartArray.length,
			                format:'%Y-%m-%d %I:%M:%S'
			            }
			        },
			        y : {
			        	label:yAxisData[0]
			        }
			    }
			});
		}
	});
}