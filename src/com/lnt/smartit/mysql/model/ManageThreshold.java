package com.lnt.smartit.mysql.model;

public class ManageThreshold {
	String paramName;
	public String getParamName() {
		return paramName;
	}
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}
	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAppLodId() {
		return appLodId;
	}
	public void setAppLodId(String appLodId) {
		this.appLodId = appLodId;
	}
	public String getAppDeviceId() {
		return appDeviceId;
	}
	public void setAppDeviceId(String appDeviceId) {
		this.appDeviceId = appDeviceId;
	}
	String hostName;
	String id;
	String appLodId;
	String appDeviceId;
	String warnThreshold;
	public String getWarnThreshold() {
		return warnThreshold;
	}
	public void setWarnThreshold(String warnThreshold) {
		this.warnThreshold = warnThreshold;
	}
	public String getCriticalThreshold() {
		return criticalThreshold;
	}
	public void setCriticalThreshold(String criticalThreshold) {
		this.criticalThreshold = criticalThreshold;
	}
	String criticalThreshold;
	String voidInd;
	public String getVoidInd() {
		return voidInd;
	}
	public void setVoidInd(String voidInd) {
		this.voidInd = voidInd;
	}

}
