package com.lnt.smartit.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lnt.smartit.mysql.dao.SoftwareDao;
import com.lnt.smartit.mysql.dao.ThresholdLogSizeDao;
import com.lnt.smartit.mysql.model.ThresholdLogSize;
import com.lnt.smartit.services.ThresholdLogSizeService;

@Service
public class ThresholdLogSizeServiceImpl implements ThresholdLogSizeService {

	@Autowired
	private ThresholdLogSizeDao dao;
	@Override
	public List<ThresholdLogSize> getAllThresholdLogSizeRecords() {
		// TODO Auto-generated method stub
		return dao.getAllThresholdLogSizeRecords();
	}

	@Override
	public void saveNewThresholdLogSizeRecord(
			ThresholdLogSize thresholdLogSizeRecord) {
		dao.saveNewThresholdLogSizeRecord(thresholdLogSizeRecord);

	}

	@Override
	public ThresholdLogSize getThresholdLogSizeRecordById(
			ThresholdLogSize thresholdLogSizeRecord) {
		return dao.getThresholdLogSizeById(thresholdLogSizeRecord);
	}

	@Override
	public String deleteThresholdLogSizeRecord(
			ThresholdLogSize thresholdLogSizeRecord) {
		return dao.deleteThresholdLogSizeRecord(thresholdLogSizeRecord);
	}

	@Override
	public Boolean exists(ThresholdLogSize thresholdLogSizeRecord) {
		// TODO Auto-generated method stub
		return dao.exists(thresholdLogSizeRecord);
	}

}
