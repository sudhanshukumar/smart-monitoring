package com.lnt.smartit.mysql.model;

public class GridPojo {
	
	private int page;
	// total pages for the query
	private int total;
	// total number of records for the query
	private int records;
	// an array that contains the actual data
	private Object[] gridData;
	// get how many rows we want to have into the grid - rowNum parameter in the
	// grid
	private int rows;
	// get index row - i.e. user click to sort. At first time sortname parameter
	// -
	// after that the index from colModel
	private String sidx;
	// sorting order - at first time sortorder
	private String sord;
	
	public GridPojo(){}
	
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public int getRecords() {
		return records;
	}
	public void setRecords(int records) {
		this.records = records;
	}
	public Object[] getGridData() {
		return gridData;
	}
	public void setGridData(Object[] gridData) {
		this.gridData = gridData;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
}
