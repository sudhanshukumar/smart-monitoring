<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html >
<html>
<head>
 <meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../assets/ico/favicon.ico">
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

<title>SmartIT</title>

<!-- Bootstrap Styles -->
<link rel="stylesheet" type="text/css"
	href="assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="assets/css/bootstrap-responsive.css">
<link rel="stylesheet" type="text/css"
	href="assets/css/bootstrap-theme.min.css">
<!-- Common Styles -->
<link rel="stylesheet" type="text/css" href="assets/css/footer.css" />
<link href="css/custom/general/style1366.css" rel="stylesheet" />
<link href="css/custom/general/style1024.css" rel="stylesheet" />
<link href="css/custom/general/bluetabs.css" rel="stylesheet" />

<!-- Jquery css -->

<script src="assets/js/jquery-1.9.1.min.js"></script>

<script src="assets/js/jquery-1.10.2.js"></script>

<script src="scripts/common/jquery-ui.js"></script>
	<link href="css/common/jquery-ui-1.9.1.custom.css" rel="stylesheet" />
<!-- Jquery scripts -->
<script type="text/javascript" src="scripts/common/grid.locale-en.js"></script>
<script src="scripts/common/grid.locale-en.js"></script>

<!-- Bootstrap scripts -->
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<!-- Custom  -->
<script type="text/javascript" src="scripts/custom/manageDashboards.js"></script>
<script type="text/javascript" src="scripts/common/menu/dropdowntabs.js"></script>

<script src="scripts/custom/tab_menu.js"></script>

<% String session_val = (String)session.getAttribute("modules"); %>

<script type="text/javascript">
	session_obj= '<%=session_val%>';
</script>

</head>
<body>

<header>
		<%@ include file="../common/header.jsp"%>
	</header>


	<nav id="bluemenu" class="bluetabs">
		<ul>
			<li><a href="dashboard.html"><spring:message
						code="label.admin.menu1"></spring:message></a></li>
			<li><a href="problemHistory.html"><spring:message
						code="label.admin.menu2"></spring:message></a></li>
			<li><a href="#" rel="dropmenu1_b"><spring:message
						code="label.admin.menu3"></spring:message></a></li>
			<li><a href="manageNotificationGroup.html"><spring:message
						code="label.admin.menu4"></spring:message></a></li>

 			<li><a id="selected" href="#" rel="dropmenu1_solr"><spring:message
 						code="label.admin.solr.manage.dashboard"></spring:message></a></li> 
			<li><a href="query.html"><spring:message
						code="label.admin.solr.query"></spring:message></a></li>
						  <li><a href="correlation.html"><spring:message
						code="label.admin.correlation"></spring:message></a></li>
		</ul>
	</nav>
	<div class="ddcolortabsline">&nbsp;</div>

	<div id="dropmenu1_b" class="dropmenudiv_b">
		<a href="manageDevice.html"><spring:message
				code="label.admin.menu3.submenu1"></spring:message></a> <a
			href="manageThresHold.html"><spring:message
				code="label.admin.menu3.submenu2"></spring:message></a> 
			<a href="manageApplication.html"><spring:message
				code="label.admin.menu3.submenu3"></spring:message></a> 
			<a href="manageService.html"><spring:message
				code="label.admin.menu3.submenu4"></spring:message></a> 
			<a href="manageUsers.html"><spring:message
				code="label.admin.menu3.submenu5"></spring:message></a>
<a href="manageLog.html"><spring:message
				code="label.admin.menu3.submenu6"></spring:message></a>

	</div>
	
	<div id="dropmenu1_solr" class="dropmenudiv_b">
	 <a href="charts.html"><spring:message
				code="label.admin.solr.charts"></spring:message></a> <a
			href="createuserdashboard.html"><spring:message
				code="label.admin.solr.custom.dashboard"></spring:message></a> 
				<a	href="list_dashboard.html"><spring:message
				code="label.admin.solr.view.dashboard"></spring:message></a> 
			<a href="manage_reports.html"><spring:message
				code="label.admin.solr.manage.reports"></spring:message></a> 
				<%-- <a href="manage_dashboard.html"><spring:message
				code="label.admin.solr.manage.dashboard"></spring:message></a>  --%>
	</div>

	<script type="text/javascript">
		//SYNTAX: tabdropdown.init("menu_id", [integer OR "auto"])
		tabdropdown.init("bluemenu")
	</script>


<div class="container" style="margin-top: 30px;">
   
  <div class="panel-group" id="accordion">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Delete Dashboard</a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse">
        <div class="panel-body">
         <div class="row">
       
        <div class="col-md-5" style="border: 0px solid #000;">
						<form class="form-horizontal" role="form">
							<div class="form-group">
								<label class="col-sm-4" style="padding: 10px 0px 10px 40px">Select Dashboard:</label>
								
	<div class="row" id="result_grid"  style="overflow-y: scroll; height: 250px; border: 0.1px solid #b2b2b2; margin-left: -15px;">
							<div class="col-md-12" style=" padding-right: 0px;  padding-left: 0px;">
								<table class="table table-bordered" id="resId">
									<thead>
										<tr>
											<th></th>
											<th>Dashboard Name</th>

										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>

					</div>
				</div>

					</div>
					</form>
				</div>
        </div>
          <div class="row">
        
					<div class="col-md-6">
						<input type="button" class="btn btn-primary" id="deleteDashboards"
							name="Delete" value="Delete"  style="margin-left: 400px;"/>

					</div>
        </div>
        
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Edit Dashboard</a>
        </h4>
      </div>
      <div id="collapse2" class="panel-collapse collapse">
        <div class="panel-body">
        <div class="row">
					<div class="col-md-5" style="border: 0px solid #000;">
						<form class="form-horizontal" role="form">
							<div class="form-group">
								<label class="col-sm-4" style="padding: 10px 0px 10px 40px">Select Dashboard:</label>
								<div class="col-sm-8">
							<select  class="form-control" id="dashboard_names">
						
							</select>
							
						</div>

					</div>
					</form>
				</div>
				</div>
				
                <div class="row">
       
        <div class="col-md-5" style="border: 0px solid #000;">
						<form class="form-horizontal" role="form">
							<div class="form-group">
								<label class="col-sm-4" style="padding: 10px 0px 10px 40px">Rearrange Sequence of Reports in Dashboard:</label>
								
	<div class="row" id="result_table"  style="overflow-y: scroll; height: 250px; border: 0.1px solid #b2b2b2; margin-left: -15px;">
							<div class="col-md-12" style=" padding-right: 0px;  padding-left: 0px;" >
								<table class="table table-bordered" id="sequence_table">
									<thead>
										<tr>
											<th>Sequence Number</th>
											<th>Dashboard Name</th>

										</tr>
									</thead>
									 <tbody>
									</tbody>
								</table>

					</div>
				</div>

					</div>
					</form>
				</div>
        </div>
          <div  class = "btn-group btn-group-sm"  style="left: 240px;">
						<input type="button" class="btn btn-default" id="UpChart"
							name="Up" value="Up"  />

						<input type="button" class="btn btn-default" id="DownChart"
							name="Down" value="Down"  />
						<input type="button" class="btn btn-default" id="removeChart"
							name="Remove" value="Remove"  />
						<input type="button" class="btn btn-default" id="SaveChart"
							name="Save" value="Save"  />
          
          </div>
        </div>
      </div>
    </div>
  </div> 
</div>
    


<footer>
		<%@ include file="../common/footer.jsp"%>
	</footer>
</body>
</html>



