package com.lnt.smartit.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lnt.smartit.mysql.dao.LogDao;
import com.lnt.smartit.mysql.model.AppLogServiceComponentParam;
import com.lnt.smartit.services.QueryService;
import com.lnt.smartit.solr.dao.QueryDao;
import com.lnt.smartit.solr.pojo.Charting;
import com.lnt.smartit.solr.pojo.Core;
import com.lnt.smartit.solr.pojo.Correlation;
import com.lnt.smartit.solr.pojo.Field;
import com.lnt.smartit.solr.pojo.Query;


@Service
public class QueryServiceImpl implements QueryService{
	
	@Autowired
	QueryDao queryDao;

	@Autowired
	LogDao logDao;
	
	@Override
	public Query getBasicQuery(Query query) throws Exception {
		return queryDao.getBasicQuery(query);
	}
	
	@Override
	public List<Object> getBasicQueryForLogAnalysis(Query query) throws Exception {
		return queryDao.getBasicQueryForLogAnalysis(query);
	}

	@Override
	public List<Object> getBasicQueryforReports(Query query) throws Exception {
		return queryDao.getBasicQueryforReports(query);
	}

	@Override
	public void getFacetingQuery(String query,String url) throws Exception {
		queryDao.getFacetingQuery(query,url);
	}

	@Override
	public void getJoinQuery(String query,String url) throws Exception {
		queryDao.getJoinQuery(query,url);
	}

	@Override
	public List<Core> getCollections(String url) throws Exception {
		return queryDao.getCollections(url);
	}

	@Override
	public List<Field> getSchema(Core coll_url) throws Exception {
		return queryDao.getSchema(coll_url);
	}

	@Override
	public List<Charting> getTimeSeries(Query query) throws Exception {
		return queryDao.getTimeSeries(query);
	}
	
	@Override
	public List<Charting> getCustomTimeSeries(Query query) throws Exception {
		return queryDao.getCustomTimeSeries(query);
	}

	@Override
	public List<Charting> getCharts(Query query) throws Exception {
		return queryDao.getCharts(query);
	}
	
	@Override
	public List<Charting> getCustomCharts(Query query) throws Exception {
		return queryDao.getCustomCharts(query);
	}
	@Override
	public List<Charting> getCustomChartsNofilter(Query query) throws Exception {
		return queryDao.getCustomChartsNofilter(query);
	}
	@Override
	public List<Charting> getCount(Query query) throws Exception {
		return queryDao.getCount(query);
	}

	@Override
	public List<Charting> getTopTenURLs(Query query) throws Exception {
		return queryDao.getTopTenURLs(query);
	}

	@Override
	public List<Charting> getTopTenIPs(Query query) throws Exception {
		return queryDao.getTopTenIPs(query);
	}

	@Override
	public List<Charting> getFailures(Query query) throws Exception {
		return queryDao.getFailures(query);
	}

	@Override
	public Query getUrlCount(Query query) throws Exception {
		return queryDao.getUrlCount(query);
	}

	@Override
	public Query getErrorCount(Query query) throws Exception {
		return queryDao.getErrorCount(query);
	}

	@Override
	public List<Charting> getResponseTime(Query query) throws Exception {
		return queryDao.getResponseTime(query);
	}

	@Override
	public List<Charting> getErrorCountSeries(Query query) throws Exception {
		return queryDao.getErrorCountSeries(query);
	}

	@Override
	public List<Field> getTimeSchema(Core coll_url) throws Exception {
		return queryDao.getTimeSchema(coll_url);
	}

	@Override
	public List<Charting> getResponseTimeSeries(Query query) throws Exception {
		return queryDao.getResponseTimeSeries(query);
	}

	@Override
	public List<Charting> getStatusCharts(Query query) throws Exception {
		return queryDao.getStatusCharts(query);
	}

	@Override
	public List<Charting> getURLCharts(Query query) throws Exception {
		return queryDao.getURLCharts(query);
	}
	
	@Override
	public List<Correlation> getBasicQueryForCorrelation(Query q) {
		return queryDao.getBasicQueryForCorrelation(q);
	}

	@Override
	public List<Correlation> getSelectedCorrelationFieldsQuery(Query q) {
		return queryDao.getSelectedCorrelationFieldsQuery(q);
	}
	
	@Override
	public List<AppLogServiceComponentParam> getCollectionNames(String appId) {
		return logDao.getCollectionNames(appId);
	}

	@Override
	public List<Charting> getNumeric(Query q) throws Exception {
		return queryDao.getNumeric(q);
	}

	@Override
	public List<Object> getBasicQueryforReportsNoFilter(Query q)
			throws Exception {
		return queryDao.getBasicQueryforReportsNoFilter(q);
	}
	@Override
	public List<Object> getMapforSolrData(Query uquery) throws Exception {
		return logDao.getBasicQueryForLogs(uquery);
	}
}
