package com.lnt.smartit.helper;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import kafka.consumer.ConsumerIterator;

import org.apache.log4j.Logger;
import org.fusesource.hawtbuf.ByteArrayInputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.kafka.consumer.Consumer;
import com.lnt.smartit.email.notify.NotifyUsers;
import com.lnt.smartit.helper.adapter.factory.ITSMAdapterFactory;
import com.lnt.smartit.helper.adapter.service.ITSMAdapter;
import com.lnt.smartit.models.AlertMessage;
import com.lnt.smartit.models.AllMachinesAlertMessages;
import com.lnt.smartit.mysql.dao.ITSMAdapterDao;
import com.lnt.smartit.services.ITSMAdapterService;
import com.lnt.smartit.util.DatabaseConfig;

/**
 * @author 10612068
 *
 */
@Component
public class ConfigSetup {
	
	private static Logger logger = Logger.getLogger(ConfigSetup.class);
	List<String> alertsList = new ArrayList<String>();
	NotifyUsers users = new NotifyUsers();
		
	@Autowired
	DatabaseConfig dbConfig;
	
	@Autowired
	ITSMAdapterFactory adapterFactory;
	
	@Autowired
	ITSMAdapterService itsmAdapterService;
	
	private Consumer kafkaConsumer;
	
	@Value("${application.name}")
	String appId;
	
	@Value("${mule.inbound}")
	String muleInboud;
	
	@Value("${incident.creation.alerts}")
	String alertTypes;
	
	/*@PreDestroy
	public void destroyConsumer(){
		logger.info("Shutting down the Consumer");
		kafkaConsumer.shutdown();
	}*/
	
	/**
	 * this method with @PostConstruct annotation is used to get the configurations from DB and
	 * creating and starting kafka consumer on startup for passing alert to ITSM for creating incident
	 */
	@PostConstruct
	public void loadConfig(){
		logger.info("loading configurations for Application ..........." + appId);
		dbConfig.loadDb(appId);
		System.out.println("CONNECTED::::::::::::::::::::");
		//alertsList = Arrays.asList(alertTypes.split(","));
		alertsList = itsmAdapterService.checkItsmReqFlag();
		if(!alertsList.isEmpty()){
			String url = null,timeout = null, partitionCount = null, groupId = null;
			Map<String,String> propsMap = new HashMap<String, String>();
			List<String> topicName = DatabaseConfig.topicName;
			for(int i=0;i<DatabaseConfig.config.size();i++){
				if(DatabaseConfig.config.get(i).getServiceComp().equalsIgnoreCase("KAFKA")&& DatabaseConfig.config.get(i).getServiceCompType().equalsIgnoreCase("PROC_STORE")){
					if(DatabaseConfig.config.get(i).getParamName().equalsIgnoreCase("zookeeperAddressCommaSeperatedList")){
						url=DatabaseConfig.config.get(i).getParamValue();
					}
					else if(DatabaseConfig.config.get(i).getParamName().equalsIgnoreCase("zookeeperSessionTimeout")){
						timeout=DatabaseConfig.config.get(i).getParamValue();
					}
					else if(DatabaseConfig.config.get(i).getParamName().equalsIgnoreCase("partitionCount")){
						partitionCount=DatabaseConfig.config.get(i).getParamValue();
					}
					else if(DatabaseConfig.config.get(i).getParamName().equalsIgnoreCase("consumerId2")){
						groupId=DatabaseConfig.config.get(i).getParamValue();
					}
				}
			}
			propsMap.put("zookeeper.connect",url+":2181");
			propsMap.put("zookeeper.connection.timeout.ms",timeout);
			propsMap.put("group.id", groupId+"abc83534xc");
			propsMap.put("partitionCount",partitionCount);
				
			logger.info("SmartIT :: KAFKA :: Zookeeper address = "+url +" :: Topic name = "+ topicName.get(0));
	    	kafkaConsumer = new Consumer(topicName.get(0), groupId+"6347yhghety",propsMap);
		    final ConsumerIterator<byte[], byte[]> it =  kafkaConsumer.getTopicIterator();
		    Runnable consumerThread = new Runnable() {
				@Override
				public void run() {
					logger.info("--- Consumer Thread started in background ---");
			        while (it.hasNext()){
			        	byte[] bytes = it.next().message();
			        	ObjectInputStream objectInputStream = null;
			        	try {
			        		logger.info("Message arrived in background consumer thread !!");
							objectInputStream = new ObjectInputStream(new ByteArrayInputStream(bytes));
							AllMachinesAlertMessages allMachinesAlertMessages = (AllMachinesAlertMessages) objectInputStream.readObject();
							List<AlertMessage> alerMessageList = allMachinesAlertMessages.getAlertMessageList();
							logger.debug("SmartIT :: KAFKA message recieved in background Consumer  :: alertList :: " + alerMessageList.toString());
							logger.info(" -- Sending alert to ITSM to create ticket -- ");
							for(AlertMessage alert : alerMessageList){
								List<String> alertTypeList = itsmAdapterService.getAletTypeForIncidentCreation(alert.getAlertMessageHostName());
								//10614080 on 09/03/2017 start
								Map<Integer, HashMap<String,String>> emailList = itsmAdapterService.getNotificationDetails(alert.getAlertMessageHostName());
								//10614080 on 09/03/2017 end
											
								if(alertTypeList.contains(alert.getAlertMessageparameterName().trim()))
								{
									createTicketInITSM(alert);
									System.out.println("::::::::ITSM CALL COMMENTED:::::::::::");
								}							
								
								//10614080 on 24/03/2017 start
								for(int i=1;i<=emailList.size();i++)
								{
									Map<String,String> map= emailList.get(i);
									
									if(map!= null && map.get("param").contains(alert.getAlertMessageparameterName()))
									{
										try {
											users.notifyUsers(alert,map);
										} catch (Exception e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									}
																		
								}
								//10614080 on 24/03/2017 end
							}
						} catch (IOException e) {
							e.printStackTrace();
						} catch (ClassNotFoundException e) {
							e.printStackTrace();
						} finally {
							try {
								if(objectInputStream != null)
									objectInputStream.close();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
			        }
				}	
	        };
	        new Thread(consumerThread).start();
		}
	}
		
	/**
	 * this method creates the thread for every alert to connect to mule flow
	 */
	public void createTicketInITSM(final AlertMessage alert){
		System.out.println("Inside create ticket method");
		String serviceCompCode = itsmAdapterService.getServiceCompNameForITSM();
		final ITSMAdapter itsmAdapter = (ITSMAdapter) adapterFactory.getITSMAdapter(serviceCompCode);
		Runnable runnable = new Runnable() {
			
			@Override
			public void run() {
				itsmAdapter.createITSMincident(alert);
			}
		};
		new Thread(runnable).start();
		
	}
}
