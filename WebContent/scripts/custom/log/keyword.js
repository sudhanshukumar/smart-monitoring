function loadcederror(i, parameterName, dataobj){
	var hostName = dataobj.alertMessageHostName;
	//check for . since it gives error as illegal character
	if(hostName.indexOf(".") > -1) {
		hostName = hostName.split(".").join("_");
	}

	$("#label_ac-9").show();
	$("#ac9_container_small").show();
	$("#ac9_block_1").append("<div class='block_split1' id='ac-9_split1_"
					+ hostName+"'>"
					+ "<table>"
					+ "<thead>"
					+ "<tr>"
					+ "<th>" 
					+ dataobj.alertMessageHostName
					+ "<span id='oracle_alert' style='float:right; font-size:10px;'> "
					+ "<img id='alertLogo' width='10px' src='images/custom/alertVal.png'>1</span>"
					+ "<br><span class='alertdatetime' id='ac9_split1_datespan_"
					+hostName+"'>"+new Date(dataobj.alertMessageDatetime).toString()+"</span>"
					+ "</th>"
					+ "</tr>"
					+ "</thead>"
					+ "</table>"
					+"<div style='overflow:auto; height:195px;'>"
					+"<table id='ac-9_split2_table_"+hostName
					+ "' border=1 cellSpacing=0> <tbody>"
					+"<tr>"
					+ "<td>"
					+ "<div style='white-space: normal'>"
					+ dataobj.cedError.errorMessage
					+ "</div>"
					+ "</td>"
					+"</tr>"
					+"</tbody>" + "</table> </div>" + "</div>");
	console.log('CEDERROR div got appended with id '+'ac-9_split1_'+ hostName);
	AC9Split1Count++;
	}

function appendOracleLog(dataobj){
	 var hostName = dataobj.alertMessageHostName;
	 if(hostName.indexOf(".") > -1) {
		hostName = hostName.split(".").join("_");
	 }
	 appendRow= "<tr>"
				+ "<td>"
				+ "<div style='white-space: normal'>"
				+ dataobj.cedError.errorMessage
				+ "</div> </td>"
				+"</tr>";
	$("#ac-9_split2_table_" + hostName+" tbody").prepend(appendRow);
}