package com.lnt.smartit.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.lnt.smartit.mysql.model.AppLogServiceComponentParam;
import com.lnt.smartit.solr.pojo.Charting;
import com.lnt.smartit.solr.pojo.Core;
import com.lnt.smartit.solr.pojo.Correlation;
import com.lnt.smartit.solr.pojo.Field;
import com.lnt.smartit.solr.pojo.Query;

@Service
public interface QueryService {
	
	public Query getBasicQuery(Query query) throws Exception;
	public List<Object> getBasicQueryForLogAnalysis(Query query) throws Exception;
	public void getFacetingQuery(String query,String url) throws Exception;
	public void getJoinQuery(String query,String url) throws Exception;
	public List<Core> getCollections(String url) throws Exception;
	public List<Field> getSchema(Core coll_url) throws Exception;
	public List<Field> getTimeSchema(Core coll_url) throws Exception;
	public List<Charting> getTimeSeries(Query query) throws Exception;
	public List<Charting> getCharts(Query query) throws Exception;
	public List<Charting> getCustomCharts(Query query) throws Exception;
	public List<Charting>  getCustomChartsNofilter(Query query) throws Exception;
	public List<Charting> getCustomTimeSeries(Query query) throws Exception;
	public List<Charting> getResponseTimeSeries(Query query) throws Exception;
	//shiksha cloud dashboard
	public List<Charting> getStatusCharts(Query query) throws Exception;
	public List<Charting> getURLCharts(Query query) throws Exception;
	public List<Charting> getTopTenURLs(Query query) throws Exception;
	public List<Charting> getTopTenIPs(Query query) throws Exception;
	public List<Charting> getFailures(Query query) throws Exception;
	public Query getUrlCount(Query query) throws Exception;
	public Query getErrorCount(Query query) throws Exception;
	public List<Charting> getResponseTime(Query query) throws Exception;
	public List<Charting> getErrorCountSeries(Query query) throws Exception;
	public List<Correlation> getBasicQueryForCorrelation(Query q);
	public List<Correlation> getSelectedCorrelationFieldsQuery(Query q);
	public List<AppLogServiceComponentParam> getCollectionNames(String appId);
	public List<Object> getBasicQueryforReports(Query query) throws Exception;
	public List<Charting> getCount(Query q)throws Exception;
	public List<Charting> getNumeric(Query q)throws Exception;
	public List<Object> getBasicQueryforReportsNoFilter(Query q)throws Exception;
	public List<Object> getMapforSolrData(Query uquery) throws Exception;

}
