package com.lnt.smartit.controllers;

import static com.lnt.smartit.util.Util.saveAuditCreation;
import static com.lnt.smartit.util.Util.saveAuditUpdation;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lnt.smartit.mysql.model.NotificationGroup;
import com.lnt.smartit.mysql.model.ThresholdNotifyGroupMapping;
import com.lnt.smartit.serviceImpl.NotificationGroupServiceImpl;
import com.lnt.smartit.solr.pojo.JqGridData;
import com.lnt.smartit.util.EmailValidator;

@Controller
public class NotificationGroupController {

	private static Logger logger = Logger.getLogger(NotificationGroupController.class);
	@Autowired
	private NotificationGroupServiceImpl notificationGroupService;

	/**
	 * @author 298038
	 * @param request
	 * @param DeviceMasterJqGridPojo
	 * @return
	 */

	@RequestMapping(value = "populateNotificationGrpGrid.html", method = RequestMethod.POST)
	public @ResponseBody JqGridData populateNotificationGrpGrid(HttpServletRequest request, JqGridData gridPojo) {
		logger.debug("Entering NotificationGroupController-->populateNotificationGrpGrid (POST)");
		List<NotificationGroup> NotificationGroupList = new ArrayList<NotificationGroup>();
		try {
			NotificationGroupList = notificationGroupService.getAllNotificationGroup();
			for (NotificationGroup notificationGroup : NotificationGroupList) {
				logger.debug(notificationGroup.toString());
			}
			gridPojo.setGridData(NotificationGroupList.toArray());
			gridPojo.setRows(NotificationGroupList.size());
			gridPojo.setRecords(NotificationGroupList.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return gridPojo;
	}

	/**
	 * @author 298038 newNotificationGroup redirects to add new notification
	 *         Group
	 * */
	@RequestMapping("newNotificationGroup.html")
	public String newNotificationGroup(ModelMap map) {
		return "Manage/new/addNewNotification";
	}

	/**
	 * @author 298038 manageNotificationGroup redirects to add new notification
	 *         Group
	 * */
	@RequestMapping("manageNotificationGroup.html")
	public String manageNotificationGroup(ModelMap map) {
		return "Manage/manageNotification";
	}

	@RequestMapping(value = "addNotificationGroupOnSubmit.html", method = RequestMethod.POST)
	public @ResponseBody String addNotificationGroupOnSubmit(
			@Valid NotificationGroup notificationGroup,BindingResult bindingResult, HttpSession session,HttpServletRequest request) {
		Integer notificationGrpId;
		logger.debug("Entering NotificationGroupController-->addNotificationGroupOnSubmit()");
		logger.debug(notificationGroup.toString());
		String notificationid = request.getParameter("id");
		String notificationGroupMsg;
		/*
		 * if(!notificationGroup.getGroupEmailIds().contains("@")){
		 * notificationGroupMsg="Kindly" }
		 */
		StringBuffer stringBuffer = new StringBuffer();
		EmailValidator emailValidator = new EmailValidator();
		boolean isEmailValid = emailValidator.validateEmail(notificationGroup.getGroupEmailIds());
		if (!isEmailValid | bindingResult.hasErrors()) {
			if (!isEmailValid) {
				stringBuffer.append(" * Kindly provide a valid email ids!!<br />");
			}
			if (bindingResult.hasErrors()){
				notificationValidator(bindingResult, stringBuffer,notificationGroup);
			}
			return stringBuffer.toString();
		}
		else {
			if (null == notificationid || notificationid.equalsIgnoreCase("null")) {
				notificationGrpId = ((int) (Math.random() * 1000));
				saveAuditCreation(notificationGroup);
				notificationGroup.setGroupId(notificationGrpId.toString());
				try {
					notificationGroupService.savenotificationGroup(notificationGroup);
					notificationGroupMsg = "New notification group deatils have been Saved Successfully !!. ";
				} catch (Exception e) {
					e.printStackTrace();
					notificationGroupMsg = "New notification group  Save failed due to "
							+ e.getMessage();

				}
			} else {
				notificationGrpId = Integer.parseInt(notificationid);
				notificationGroup.setGroupId(notificationGrpId.toString());
				NotificationGroup noteGrp = notificationGroupService.getNotificationGroupById(notificationGroup);
				List<ThresholdNotifyGroupMapping> thresholdNotifyGroupMappingList= notificationGroupService.getNotificationThresholdMappingById(notificationGrpId.toString());
				saveAuditUpdation(notificationGroup);
				notificationGroup.setCreatedBy(noteGrp.getCreatedBy());
				notificationGroup.setCreationDate(noteGrp.getCreationDate());
				noteGrp.setGroupName(notificationGroup.getGroupName());
				noteGrp.setDescription(notificationGroup.getDescription());
				noteGrp.setGroupEmailIds(notificationGroup.getGroupEmailIds());
				try {
					notificationGroupService.savenotificationGroup(noteGrp);
					notificationGroupService.saveNotificationThresholdMapping(thresholdNotifyGroupMappingList);
					notificationGroupMsg = "New notification group deatils have been updated Successfully !! ";
				} catch (Exception e) {
					e.printStackTrace();
					notificationGroupMsg = "New notification group  update failed due to "
							+ e.getMessage();
				}
			}
			return notificationGroupMsg;
		}

	}

	private String notificationValidator(BindingResult bindingResult,
			StringBuffer stringBuffer, NotificationGroup notificationGroup) {
		if (bindingResult.hasErrors()) {
			if (notificationGroup.getGroupName().isEmpty()|| notificationGroup.getGroupName() == null) {
				stringBuffer.append(" * Name can not be empty! <br />");
			}
			if (notificationGroup.getGroupEmailIds().isEmpty()|| notificationGroup.getGroupEmailIds() == null) {
				stringBuffer.append(" * Kindly provide atleast one email address!<br />");
			}
		}
		return stringBuffer.toString();
	}

	@RequestMapping(value = "deleteNotificationGrp.html", method = RequestMethod.POST)
	public @ResponseBody String deleteNotificationGrp(NotificationGroup notificationGroup) {
		logger.debug("Entering NotificationGroupController-->deleteNotificationGrp()");
		logger.debug("notificationGroup " + notificationGroup);
		NotificationGroup notificationgrp;
		String deleteNotificationGrp;
		try {
			notificationgrp = notificationGroupService.getNotificationGroupById(notificationGroup);
			deleteNotificationGrp = notificationGroupService.deleteNotificationGroup(notificationgrp);
		} catch (Exception e) {
			e.printStackTrace();
			deleteNotificationGrp = e.getLocalizedMessage();
		}
		return deleteNotificationGrp;
	}

	@RequestMapping(value = "editNotification.html", method = RequestMethod.POST)
	public @ResponseBody NotificationGroup editNotification(NotificationGroup notificationGroup, HttpServletRequest request) {
		logger.debug("Entering NotificationGroupController-->editNotification()");
		logger.debug("notificationGroup " + notificationGroup);
		NotificationGroup notificationgp;
		notificationgp = notificationGroupService.getNotificationGroupById(notificationGroup);
		logger.debug("notificationGroup " +notificationgp);
		return notificationgp;
	}

	@RequestMapping(value = "previewNotification.html", method = RequestMethod.POST)
	public @ResponseBody NotificationGroup previewNotification(NotificationGroup notificationGroup, HttpServletRequest request) {
		logger.debug("Entering NotificationGroupController-->previewNotification()");
		logger.debug("notificationGroup " + notificationGroup);
		NotificationGroup notificationgp;
		notificationgp = notificationGroupService.getNotificationGroupById(notificationGroup);
		logger.debug("notificationGp " +notificationgp);
		return notificationgp;
	}
}
