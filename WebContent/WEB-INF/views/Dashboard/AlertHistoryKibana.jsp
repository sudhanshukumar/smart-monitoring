<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Alert History</title>
<link rel="shortcut icon"  href="favicon.ico" />
<!-- Common CSS files of Bootstrap and JQuery -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="assets/css/bootstrap-combined.min.css">
<link rel="stylesheet" type="text/css" href="css/common/jquery-ui.min.css" />

<!-- Custom CSS files for this Page -->
<link rel="stylesheet" href="css/custom/specific/nav.css">

<!-- Common Scripts of Bootstrap and JQuery -->
<script src="scripts/common/jquery-2.2.3.min.js"></script>

<!-- Custom scripts for this Page -->
<!-- <script type="text/javascript" src="scripts/custom/query.js"></script> -->

<!-- Common Scripts of Bootstrap and JQuery -->
<script src="assets/js/bootstrap.min.js"></script>

<script src="scripts/custom/tab_menu.js"></script>

<% String session_val = (String)session.getAttribute("modules"); %>
<script>document.domain = '172.21.106.139:5601'</script>

<script type="text/javascript">
	session_obj= '<%=session_val%>';
</script>

</head>
<body style="font-size:11px!important;">
	<div class="container-fluid" style="height: 100%">
	  <%@ include file="../common/header.jsp"%>
  
	 <nav class="navbar navbar-inverse" style="margin-top: 0px">
	      <ul class="nav navbar-nav navbar-xs">
	        <li class="active" id="Dashboard_ELK"><a href="dashboard.html">Dashboard<span class="sr-only">(current)</span></a></li>
	        <li id="ManageThreshold_ELK"><a href="manageThresHold.html">Manage Rule</a></li>
	        <!-- <li id="AlertHistory"><a href="problemHistory.html">Alert History</a></li> -->
	        <li class="dropdown" id="Insights">
	          <a href="#" class="dropdown-toggle" id="Insights" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Insights<span class="caret"></span></a>
	          <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
	            <li id="Analytics"><a href="query.html" style="font-size:11px!important;">Analytics</a></li>
	           <!--  <li><a href="kibanaDashboard.html" style="font-size:11px!important;">Prediction</a></li> -->
	            <!-- <li><a href="variableAnalysis.html">Variable Analysis</a></li> -->
	            <li class="dropdown-submenu" id="Reports"> <a tabindex="-1" href="#" style="font-size:11px!important;">Reports</a>
                      <ul class="dropdown-menu">
							<li id="Prediction">
								<a href="predictionreport.html" style="font-size:11px!important;">Prediction</a> </li>
							<li id="Trends">
								<a href="trendingreport.html" style="font-size:11px!important;">Trends</a> </li>
							<li id="Alert_History">
							 	<a href="alerthistoryreport.html" style="font-size:11px!important;">Alert History</a> </li>
							<li id="LogCorrelation">
								<a href="correlationreport.html" style="font-size:11px!important;">Log Correlation</a> </li>
							<li id="WeblogicReport">
								<a href="weblogicreport.html" style="font-size:11px!important;">Weblogic Report</a> </li>
							<li id="WeblogicDrillDownReport">
								<a href="weblogicdrilldownreport.html" style="font-size:11px!important;">Weblogic DrillDown Report</a> </li>
							<li id="SapApplicationReport">
								<a href="sapapplicationreport.html" style="font-size:11px!important;">Sap Application Report</a> </li>
						    <li id="SapApplicationAdoption">
                           <a href="sapapplicationadoption.html" style="font-size:11px!important;">Sap Application Adoption</a> </li>
                            <li id ="SapPlatformOperation">
                           <a href="sapplatformoperation.html" style="font-size:11px!important;">Sap Platform Operations</a> </li>
                           <li id ="SapSecurityAuditing">
                           <a href="sapsecurityauditing.html" style="font-size:11px!important;">Sap Security Auditing</a> </li>
						</ul>
                    </li>
	          </ul>
	        </li>
	        <!-- <li class="active" id="LogAnalysis"><a href="query.html">Insights</a></li> -->
	        <%-- <li id="LogCorrelation"><a href="correlation.html">Log Correlation</a></li>
	        <li id="VariableAnalysis"><a href="variableAnalysis.html"><spring:message code="label.admin.menu5"></spring:message></a></li>
	        <li class="dropdown" id="Reports">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reports <span class="caret"></span></a>
	          <ul class="dropdown-menu">
	            	<li id="CreateReport"><a href="charts.html">Create Report</a></li>
		            <li id="CreateDashboard"><a href="createuserdashboard.html">Create Dashboard</a></li>
		            <li id="ViewDashboard"><a href="list_dashboard.html">View Dashboard</a></li>
	          </ul>
	        </li> --%>
	      </ul>
	  </nav>
	
  <div class="container-fluid innerContainer">
  	<iframe id="dashboardFrame" src="https://mosaicauto.lntinfotech.com:5601/app/kibana#/dashboard/AlertHistoryDashboard?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:now-7d,mode:quick,to:now))&_a=(filters:!(('$$hashKey':'object:2943','$state':(store:appState),meta:(alias:!n,apply:!t,disabled:!f,index:alerts,key:alerttype.keyword,negate:!f,value:CRITICAL),query:(match:(alerttype.keyword:(query:CRITICAL,type:phrase))))),options:(darkTheme:!f),panels:!((col:1,id:Alert-Statistics,panelIndex:2,row:1,size_x:7,size_y:3,type:visualization),(col:8,id:Host-wise-Alert-Type,panelIndex:3,row:1,size_x:5,size_y:3,type:visualization),(col:1,columns:!(hostname,alertdate,alerttype,description,parametername,incidentNumber,incidentStatus,incidentPriority,appname),id:Alert-History,panelIndex:1,row:4,size_x:12,size_y:5,sort:!(alertdate,desc),type:search)),query:(query_string:(analyze_wildcard:!t,query:'*')),title:AlertHistoryDashboard,uiState:(P-2:(vis:(colors:(CRITICAL:%23BF1B00,WARNING:%23EAB839)))))"
  	 style="width: 100%; height: 100%;" ></iframe>
 	</div>
	<%@ include file="../common/footer.jsp"%>
	</div>
</body>
</html>