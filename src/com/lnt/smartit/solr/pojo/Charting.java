package com.lnt.smartit.solr.pojo;

import java.util.Arrays;
import java.util.List;

public class Charting {
	
	private long date;
	private long size;
	private String lable;
	private long count;
	private String chart_type;
	private long pages;
	private String col_name; //this field created specifically to hold collection name to call jqgrid.
	private String[] gridcols;
	private String chart_name;
	private String div_count;
	private String facetValue;
	private List<Object> gridData;
	
	
	public String getFacetValue() {
		return facetValue;
	}
	public void setFacetValue(String facetValue) {
		this.facetValue = facetValue;
	}
	public String getChart_name() {
		return chart_name;
	}
	public String getDiv_count() {
		return div_count;
	}
	public void setDiv_count(String div_count) {
		this.div_count = div_count;
	}
	public void setChart_name(String chart_name) {
		this.chart_name = chart_name;
	}
	public String[] getGridcols() {
		return gridcols;
	}
	public void setGridcols(String[] gridcols) {
		this.gridcols = gridcols;
	}
	public String getCol_name() {
		return col_name;
	}
	public void setCol_name(String col_name) {
		this.col_name = col_name;
	}
	public long getPages() {
		return pages;
	}
	public void setPages(long pages) {
		this.pages = pages;
	}
	public String getChart_type() {
		return chart_type;
	}
	public void setChart_type(String chart_type) {
		this.chart_type = chart_type;
	}
	public String getLable() {
		return lable;
	}
	public void setLable(String lable) {
		this.lable = lable;
	}
	public long getCount() {
		return count;
	}
	public void setCount(long count) {
		this.count = count;
	}
	public long getDate() {
		return date;
	}
	public void setDate(long date) {
		this.date = date;
	}
	public long getSize() {
		return size;
	}
	public void setSize(long size) {
		this.size = size;
	}
	
	public List<Object> getGridData() {
		return gridData;
	}
	public void setGridData(List<Object> gridData) {
		this.gridData = gridData;
	}
	@Override
	public String toString() {
		return "Charting [date=" + date + ", size=" + size + ", lable=" + lable
				+ ", count=" + count + ", chart_type=" + chart_type
				+ ", pages=" + pages + ", col_name=" + col_name + ", gridcols="
				+ Arrays.toString(gridcols) + ", chart_name=" + chart_name
				+ ", div_count=" + div_count + ", facetValue=" + facetValue
				+ ", gridData=" + gridData + "]";
	}
	
	
	
}
