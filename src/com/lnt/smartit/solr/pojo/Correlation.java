package com.lnt.smartit.solr.pojo;

public class Correlation {
	String record;
	String logFileType;
	String date;
	
	public String getRecord() {
		return record;
	}
	public void setRecord(String record) {
		this.record = record;
	}
	public String getLogFileType() {
		return logFileType;
	}
	public void setLogFileType(String logFileType) {
		this.logFileType = logFileType;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	@Override
	public String toString() {
		return "Correlation [record=" + record + ", logFileType=" + logFileType
				+ ", date=" + date + "]";
	}
}
