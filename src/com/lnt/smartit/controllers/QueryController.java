package com.lnt.smartit.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.lnt.smartit.mysql.model.AppLogServiceComponentParam;
import com.lnt.smartit.mysql.model.Applications;
import com.lnt.smartit.mysql.model.DashboardMaster;
import com.lnt.smartit.mysql.model.ManageReport;
import com.lnt.smartit.mysql.model.Report;
import com.lnt.smartit.mysql.model.ReportMetadata;
import com.lnt.smartit.services.ChartService;
import com.lnt.smartit.services.LogService;
import com.lnt.smartit.services.QueryService;
import com.lnt.smartit.solr.pojo.Charting;
import com.lnt.smartit.solr.pojo.Core;
import com.lnt.smartit.solr.pojo.Correlation;
import com.lnt.smartit.solr.pojo.Dashboard;
import com.lnt.smartit.solr.pojo.DashboardUpdate;
import com.lnt.smartit.solr.pojo.Field;
import com.lnt.smartit.solr.pojo.Query;
import com.lnt.smartit.util.DatabaseConfig;
import com.lnt.smartit.util.Lib;

@Controller
public class QueryController {
	@Autowired
	LogService logService;
	@Autowired
	QueryService queryService;
	
	@Autowired
	private DatabaseConfig dbconfig;
	
	@Autowired
	ChartService chartService;
	
	@Value("${application.name}")
	private String app_name;
	
	private static Logger logger = Logger.getLogger(NotificationGroupController.class);
	
	@RequestMapping(value="query.html")
	public ModelAndView openQueryPage(ModelMap map,HttpServletRequest request){
		/*String appId = request.getParameter("appId");
		if(appId==null){
			 appId = request.getSession().getAttribute("appId").toString();
		}
		String id = logService.getAppServiceComponentId(appId, "RAW_STORE", "SOLR");
		List<AppLogServiceComponentParam> collectionnames = queryService.getCollectionNames(id);
		Map<String, String> colMap = new HashMap<String, String>();
		for(AppLogServiceComponentParam app : collectionnames){
			colMap.put(app.getParamValue(), app.getDescription());
		}
		System.out.println("Collections == "+colMap);
		map.addAttribute("Collections", colMap);*/
		return new ModelAndView("solr/LogAnalysis");
	}
	//temporary mapping done for correlation page
	@RequestMapping(value="correlation.html")
	public ModelAndView correlation() {
		logger.debug("..........correlation........");
		return new ModelAndView("solr/correlation");
	}
			
	@RequestMapping(value="correlated_query.html")
	public ModelAndView openCorrelatedQueryPage(){
		return new ModelAndView("solr/correlated_query");
	}
	//request mappings for Reports Tab JSP
	@RequestMapping(value="charts.html")
	public ModelAndView createchart(HttpServletRequest request){
		String appId = logService.getAppIdByName(app_name);
		HttpSession session = request.getSession(true);
		session.setAttribute("appId",appId);
		session.setAttribute("appname", app_name);
		dbconfig.loadDb(appId);
		return new ModelAndView("solr/charts");
	}
	
	@RequestMapping(value="userdashboard.html")
	public ModelAndView getUserdashboard(HttpServletRequest request){
		String appId = logService.getAppIdByName(app_name);
		HttpSession session = request.getSession(true);
		session.setAttribute("appId",appId);
		session.setAttribute("appname", app_name);
		dbconfig.loadDb(appId);
		logger.debug("Inside getUserdashboard");
		return new ModelAndView("solr/solr_dashboard");
	}
	
	@RequestMapping(value="list_dashboard.html")
	public ModelAndView list_dashboard(HttpServletRequest request){
		String appId = logService.getAppIdByName(app_name);
		HttpSession session = request.getSession(true);
		session.setAttribute("appId",appId);
		session.setAttribute("appname", app_name);
		dbconfig.loadDb(appId);
		logger.debug("Inside list_dashboard");
		return new ModelAndView("solr/list_dashboard");
	}
	
	@RequestMapping(value="createuserdashboard.html")
	public ModelAndView createUserdashboard(HttpServletRequest request){
		String appId = logService.getAppIdByName(app_name);
		HttpSession session = request.getSession(true);
		session.setAttribute("appId",appId);
		session.setAttribute("appname", app_name);
		dbconfig.loadDb(appId);
		logger.debug("Inside createUserdashboard");
		return new ModelAndView("solr/user_dashboard");
	}
	
	@RequestMapping(value="manage_reports.html")
	public ModelAndView managereports(HttpServletRequest request){
		String appId = logService.getAppIdByName(app_name);
		HttpSession session = request.getSession(true);
		session.setAttribute("appId",appId);
		session.setAttribute("appname", app_name);
		dbconfig.loadDb(appId);
		logger.debug("Inside managereports");
		return new ModelAndView("solr/manage_reports");
	}
	@RequestMapping(value="manage_dashboard.html")
	public ModelAndView managedashboard(HttpServletRequest request){
		String appId = logService.getAppIdByName(app_name);
		HttpSession session = request.getSession(true);
		session.setAttribute("appId",appId);
		session.setAttribute("appname", app_name);
		dbconfig.loadDb(appId);
		logger.debug("Inside managedashboard");
		return new ModelAndView("solr/manage_dashboard");
	}
	
	//request mappings for Reports Tab JSP end here

	@RequestMapping(value="getApplications.html")
	public @ResponseBody List<Applications> getApplications(){
		logger.debug("Inside getApplications controller");
		List<Applications> appList = new ArrayList<Applications>();
		appList=chartService.getApplications();
		return appList;
	}
	
	@RequestMapping(value="getReports.html")
	public @ResponseBody List<Report> getReports(){
		logger.debug("Inside getReports controller");
		List<Report> repList = new ArrayList<Report>();
		repList=chartService.getReports();
		return repList;
	}
	
	@RequestMapping(value="deleteReports.html")
	public @ResponseBody Boolean deleteReports(@RequestParam(value="selected_reports") String [] selected_reports){
		logger.debug("Inside deleteReports controller");
		
		
		return chartService.deleteReports(selected_reports);
	}
	
	@RequestMapping(value="getDashboards.html")
	public @ResponseBody List<DashboardMaster> getDashboards(){
		logger.debug("Inside getDashboards controller");
		List<DashboardMaster> dashList = new ArrayList<DashboardMaster>();
		dashList=chartService.getDashboards();
		return dashList;
	}
	
	@RequestMapping(value="deleteDashboards.html")
	public @ResponseBody Boolean deleteDashboards(@RequestParam(value="select_dashboards") String select_dashboards){
		logger.debug("Inside deleteDashboards controller");
		
		
		return chartService.deleteDashboards(select_dashboards);
	}
	
	
	@RequestMapping(value="getCollectionsFromDB.html")
	public @ResponseBody List<AppLogServiceComponentParam> getCollectionsFromDB(@RequestParam(value="appname") String appsel){
		logger.debug("Inside getCollectionsFromDB controller");
		List<AppLogServiceComponentParam> colList = new ArrayList<AppLogServiceComponentParam>();
		colList=chartService.getCollectionsFromDB(appsel);
		return colList;
	}
	
	
	@RequestMapping(value="selectReports.html")
	public @ResponseBody Boolean selectReports(@RequestParam(value="selected_fields") String [] selected_fields,@RequestParam(value="col_name") String  col_name,@RequestParam(value="flag") String  flag){
	
		logger.debug("Inside selectReports controller");
		
		
		List <ManageReport> ManageReportList =new ArrayList<ManageReport>();
		 ArrayList<String> ListofSecondary= new ArrayList<String>();
		for(int y=0;y<selected_fields.length;y++){
			ManageReport manageReport= new ManageReport();
			manageReport.setField(selected_fields[y]);
			manageReport.setColId(col_name);
			if(flag.equalsIgnoreCase("Invalid")){
				ManageReportList.add(manageReport);
			}
			else if(flag.equalsIgnoreCase("Primary")){
				manageReport.setFilterType("Primary");
				ManageReportList.add(manageReport);
			
			}
			else if(flag.equalsIgnoreCase("Secondary")){
				manageReport.setFilterType("Secondary");
				ManageReportList.add(manageReport);
			}
		}
		if(flag.equalsIgnoreCase("Invalid")){
			String fieldforPrimary = null;
			String fieldforSecondary;
		List<ManageReport> manageList=chartService.getManageReportFields(col_name);
		for(int y=0;y<manageList.size();y++){
			try {
				if (manageList.get(y).getFilterType()
						.equalsIgnoreCase("Primary")) {
					fieldforPrimary = manageList.get(y).getField();
				} else if (manageList.get(y).getFilterType()
						.equalsIgnoreCase("Secondary")) {
					fieldforSecondary = manageList.get(y).getField();
					ListofSecondary.add(fieldforSecondary);
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}		
			chartService.undoInvalid(ManageReportList);
			for(int y=0;y<ManageReportList.size();y++){
				try {
					if(ManageReportList.get(y).getField().equals(fieldforPrimary)){
						ManageReportList.get(y).setFilterType("Primary");
					}
					for(int i=0;i<ListofSecondary.size();i++){
						if(ManageReportList.get(y).getField().equals(ListofSecondary.get(i))){
							ManageReportList.get(y).setFilterType("Secondary");
						}
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}else if(flag.equalsIgnoreCase("Primary")){
			chartService.undoFilterType(ManageReportList);
		}else if(flag.equalsIgnoreCase("Secondary")){
			chartService.undoFilterType(ManageReportList);
		}
			Boolean save=chartService.selectReports(ManageReportList);	
			
		
		return save;
	}
	
	@RequestMapping(value="getCollections.html")
	public @ResponseBody String getCollections(HttpServletRequest request){
		logger.debug("QueryController :: Inside getCollections controller");
		HttpSession session = request.getSession();
		String appId = session.getAttribute("appId").toString();
		session.setAttribute("appname", app_name);
		dbconfig.loadDb(appId);
		String id = logService.getAppServiceComponentId(appId, "RAW_STORE", "SOLR");
		List<AppLogServiceComponentParam> collectionnames = queryService.getCollectionNames(id);
//		List<Core> coreList = new ArrayList<Core>();
		Map<String,String> map = new HashMap<String, String>();
		for(AppLogServiceComponentParam app : collectionnames){
			map.put(app.getParamValue(), app.getDescription());
			/*Core c = new Core();
			c.setCol_name(app.getParamValue());
			c.setCol_name(app.getDescription());
			coreList.add(c);*/
		}
		/*try {
			coreList=queryService.getCollections(DatabaseConfig.solrUrl);
		} catch (Exception e) {
			e.printStackTrace();
		}*/
//		logger.debug("QueryController :: Inside getCollections controller :: CollectionList :: "+coreList.toString());
		JSONObject jsonobj = new JSONObject(map);
		return jsonobj.toString();
	
	}
	
	@RequestMapping(value="getBasicQuery.html")
	public @ResponseBody Query getBasicQuery(@RequestParam(value="collection") String collection,@RequestParam(value="query") String query,@RequestParam(value="start") int start,@RequestParam(value="rows") int rows,@RequestParam(value="filter") String filter,@RequestParam(value="sort_field") String sort_field,@RequestParam(value="sort_order")String sort_order){
		logger.debug("QueryController :: Inside getBasicQuery controller :: DatabaseConfig.solrUrl = "+ DatabaseConfig.solrUrl);
		logger.debug("QueryController :: Inside getBasicQuery controller :: Collection = "+ collection);
		Query q=new Query();
		System.out.println("DatabaseConfig.solrUrl...."+DatabaseConfig.solrUrl);
		q.setUrl(DatabaseConfig.solrUrl);
		q.setQuery(query);
		q.setStart(start);
		q.setRows(rows);
		q.setFilter("default");
		q.setCollection(collection);
		q.setSort_field(sort_field);
		q.setSort_order(sort_order);
		
		logger.debug("QueryController :: Inside getBasicQuery controller :: Query = "+ query);
		try {
			q=queryService.getBasicQuery(q);
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.debug("QueryController :: Inside getBasicQuery controller :: Query ="+ q.toString());
		return q;
	}
	
	@RequestMapping(value="getBasicQueryforReports.html")
	public @ResponseBody List<Object> getBasicQueryforReports(@RequestBody String queryParams,HttpServletRequest request ){
		logger.debug("QueryController :: Inside getBasicQuery controller :: DatabaseConfig.solrUrl = "+ DatabaseConfig.solrUrl);
		JSONObject json = null;
		 List<Object> records=new ArrayList<Object>();
		 Query q=new Query();
		try {
			json = new JSONObject(queryParams);
		q.setUrl(DatabaseConfig.solrUrl);
		q.setQuery((String) json.get("query"));
		q.setRows(-1);
		q.setFilter("default");
		q.setCollection((String) json.get("collection"));
		//TODO THINGS TO BE SET AFTER GETTING THESE VALUES FROM UI
		if(json.getString("final_from_date") != null && !json.getString("final_from_date").equalsIgnoreCase("null"))
		q.setStart_facet(json.getString("final_from_date"));
		if(json.getString("final_to_date") != null && !json.getString("final_to_date").equalsIgnoreCase("null"))
		q.setEnd_facet(json.getString("final_to_date"));
		q.setRange_gap("+1DAY");
		 //------------------------------------------------
		records=queryService.getBasicQueryforReports(q);
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.debug("QueryController :: Inside getBasicQuery controller :: Query ="+ q.toString());
		System.out.println("======= "+q.toString());
		System.out.println("records== "+records.get(0));
		return records;
	}
	
	@RequestMapping(value="getBasicQueryforReportsNoFilter.html")
	public @ResponseBody List<Object> getBasicQueryforReportsNoFilter(@RequestBody String queryParams,HttpServletRequest request ){
		logger.debug("QueryController :: Inside getBasicQueryforReportsNoFilter controller :: DatabaseConfig.solrUrl = "+ DatabaseConfig.solrUrl);
		JSONObject json = null;
		 List<Object> records=new ArrayList<Object>();
		 Query q=new Query();
		try {
			json = new JSONObject(queryParams);
		q.setUrl(DatabaseConfig.solrUrl);
		q.setQuery((String) json.get("query"));
		q.setRows(-1);
		q.setFilter("default");
		q.setCollection((String) json.get("collection"));
		System.out.println("qqqqq"+q.toString());
		records=queryService.getBasicQueryforReportsNoFilter(q);
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.debug("QueryController :: Inside getBasicQueryforReportsNoFilter controller :: Query ="+ q.toString());
		System.out.println("======= "+q.toString());
		System.out.println("records== "+records.get(0));
		return records;
	}
	
	
	@RequestMapping(value="updateReportonDashboard.html")
	public @ResponseBody List<Charting> updateReportonDashboard(@RequestParam(value="dashname") String dashname,@RequestParam(value="divId") String divId,@RequestParam(value="from_date") String from_date,@RequestParam(value="to_date") String to_date){
	DashboardUpdate dashboardUpdate=new DashboardUpdate();
	dashboardUpdate.setDashname(dashname);
	dashboardUpdate.setDivname(divId);
	dashboardUpdate.setStartdate(from_date);
	dashboardUpdate.setEnddate(to_date);
	
	List<Charting> chart_data=chartService.getUpdatedReport(dashboardUpdate);
	return chart_data;
	}
	
	
	@RequestMapping(value="getBasicQueryForCorrelation.html")
	public @ResponseBody List<Correlation> getBasicQueryForCorrelation(@RequestParam(value="collection") String collection,@RequestParam(value="query") String query,@RequestParam(value="start") int start,@RequestParam(value="rows") int rows,@RequestParam(value="filter") String filter,@RequestParam(value="sort_field") String sort_field,@RequestParam(value="sort_order")String sort_order){


		
		logger.debug("QueryController :: Inside getBasicQueryForCorrelation controller :: Collection = "+ collection);
		Query q=new Query();
		q.setUrl(DatabaseConfig.solrUrl);
		q.setQuery(query);
		q.setStart(start);
		q.setRows(rows);
		q.setFilter("default");
		q.setCollection(collection);
		q.setSort_field(sort_field);
		q.setSort_order(sort_order);
		logger.debug("Inside getBasicQuery controller"+q);
		List<Correlation> correlationList=queryService.getBasicQueryForCorrelation(q);
		return correlationList;
	}
	
	@RequestMapping(value="getSelectedCorrelationFieldsQuery.html")
	public @ResponseBody List<Correlation> getBasicQueryForCorrelation(@RequestParam(value="collection") String collection,@RequestParam(value="query") String query,@RequestParam(value="start") int start,@RequestParam(value="rows") int rows,@RequestParam(value="filter") String filter,@RequestParam(value="sort_field") String sort_field,@RequestParam(value="sort_order")String sort_order,@RequestParam(value="fields") String[] fields){
		logger.debug("QueryController :: Inside getSelectedCorrelationFieldsQuery controller :: DatabaseConfig.solrUrl = "+ DatabaseConfig.solrUrl);
		logger.debug("QueryController :: Inside getSelectedCorrelationFieldsQuery controller :: Collection = "+ collection);
		Query q=new Query();
		q.setUrl(DatabaseConfig.solrUrl);
		q.setQuery(query);
		q.setStart(start);
		q.setRows(rows);
		q.setFilter(filter);
		q.setCollection(collection);
		q.setSort_field(sort_field);
		q.setSort_order(sort_order);
		q.setSelected_fields(fields);
		logger.debug("QueryController :: Inside getSelectedCorrelationFieldsQuery controller :: "+q);
		List<Correlation> correlationList=queryService.getSelectedCorrelationFieldsQuery(q);
		return correlationList;
	}
	
	@RequestMapping(value="getManageReportFields.html")
	public @ResponseBody List<Field>  getManageReportFields(@RequestParam(value="collection") String collection,@RequestParam(value="flag") String flag){
		List<ManageReport> manageReportFieldList = new ArrayList<ManageReport>();
		 List<Field> FieldList =new ArrayList<Field>();
		
			 manageReportFieldList=chartService.getManageReportFields(collection);
			 for(int i=0;i<manageReportFieldList.size();i++){
				 Field fields = new Field();
				 fields.setName(manageReportFieldList.get(i).getField());
				 if(flag.equalsIgnoreCase("Secondary")){
					 try{
					 if(manageReportFieldList.get(i).getFilterType().equalsIgnoreCase("Secondary")){
						 fields.setIssecondary(true);
					 }
					 }
					catch(NullPointerException e){System.out.println(e);} 
				 }
				 FieldList.add(fields); 
			 }
			 
		return FieldList;
	}
	
	@RequestMapping(value="getSchema.html")
	public @ResponseBody List<Field> getSchema(@RequestParam(value="collection") String collection){
		
		List<ManageReport> manageReportFieldList=chartService.getManageReportFields(collection);
		
		Core c=new Core();
		c.setUrl(DatabaseConfig.solrUrl);
		System.out.println("url=== "+DatabaseConfig.solrUrl);
		System.out.println("col==  "+collection);
		c.setCol_name(collection);
		logger.debug("QueryController :: Inside getSchema controller :: Collection = "+collection);
		List<Field> fieldList = new ArrayList<Field>();
		try {
			fieldList=queryService.getSchema(c);
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		
		for(int i=0;i<manageReportFieldList.size();i++){
			for(int j=0;j<fieldList.size();j++){
				if(manageReportFieldList.get(i).getField().equals(fieldList.get(j).getName())){
					fieldList.get(j).setIsselected(true);
				}
				//if(TestList.get(i).g\)
			}
		}
		
		logger.debug("QueryController :: Inside getSchema controller :: fields = "+fieldList.toString());
		return fieldList;
	}
	
	@RequestMapping(value="getTimeSchema")
	public @ResponseBody List<Field> getTimeSchema(@RequestParam(value="collection") String collection){
		Core c=new Core();
		c.setUrl(DatabaseConfig.solrUrl);
		c.setCol_name(collection);
		logger.debug("Inside getSchema controller"+collection);
		List<Field> fieldList = new ArrayList<Field>();
		try {
			fieldList=queryService.getTimeSchema(c);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fieldList;
	}
	
	/*@RequestMapping(value="getSelectedFieldsQuery.html")
	public @ResponseBody Query getSelectedFieldsQuery(@RequestParam(value="collection") String collection,@RequestParam(value="query") String query,@RequestParam(value="start") int start,@RequestParam(value="rows") int rows,@RequestParam(value="filter") String filter,@RequestParam(value="sort_field") String sort_field,@RequestParam(value="sort_order")String sort_order,@RequestParam(value="fields") String[] fields){
		logger.debug("QueryController :: Inside getSelectedFieldsQuery :: DatabaseConfig.solrUrl = "+ DatabaseConfig.solrUrl);
		logger.debug("QueryController :: Inside getSelectedFieldsQuery :: Collection = "+ collection);
		Query q=new Query();
		q.setUrl(DatabaseConfig.solrUrl);
		q.setQuery(query);
		q.setStart(start);
		q.setRows(rows);
		q.setFilter(filter);
		q.setCollection(collection);
		q.setSort_field(sort_field);
		q.setSort_order(sort_order);
		q.setSelected_fields(fields);
		logger.debug("QueryController :: Inside getSelectedFieldsQuery :: Query = "+q.toString());
		try {
			q=queryService.getBasicQuery(q);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return q;
	}*/
	
	@RequestMapping(value="getSelectedFieldsQuery.html")
	public @ResponseBody List<Object> getSelectedFieldsQuery(@RequestParam(value="collection") String collection,@RequestParam(value="query") String query,@RequestParam(value="start") int start,@RequestParam(value="rows") int rows,@RequestParam(value="filter") String filter,@RequestParam(value="sort_field") String sort_field,@RequestParam(value="sort_order")String sort_order,@RequestParam(value="fields") String[] fields){
		logger.debug("QueryController :: Inside getSelectedFieldsQuery :: DatabaseConfig.solrUrl = "+ DatabaseConfig.solrUrl);
		logger.debug("QueryController :: Inside getSelectedFieldsQuery :: Collection = "+ collection);
		Query q=new Query();
		q.setUrl(DatabaseConfig.solrUrl);
		q.setQuery(query);
		q.setStart(start);
		q.setRows(rows);
		q.setFilter(filter);
		q.setCollection(collection);
		q.setSort_field(sort_field);
		q.setSort_order(sort_order);
		q.setSelected_fields(fields);
		logger.debug("QueryController :: Inside getSelectedFieldsQuery :: Query = "+q.toString());
		try {
			return queryService.getBasicQueryForLogAnalysis(q);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@RequestMapping(value="getTimeSeries.html")
	public @ResponseBody List<Charting> getTimeSeries(@RequestParam(value="collection") String collection){
		List<Charting> series=new ArrayList<Charting>();
		Query q=new Query();
		q.setUrl(DatabaseConfig.solrUrl);
		q.setCollection(collection);
		logger.debug("Inside getTimeSeries controller");
		try {
			series=queryService.getTimeSeries(q);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return series;
	}
	
	@RequestMapping(value="getCustomTimeSeries.html")
	public @ResponseBody List<Charting> getCustomTimeSeries(@RequestParam(value="collection") String collection,@RequestParam(value="query") String query,@RequestParam(value="field_value") String[] field_value){
		List<Charting> series=new ArrayList<Charting>();
		Query q=new Query();
		q.setUrl(DatabaseConfig.solrUrl);
		q.setQuery(query);
		q.setCollection(collection);
		q.setChart("time");
		/*q.setStart_facet(start_facet);
		q.setEnd_facet(end_facet);*/
		q.setSelected_fields(field_value);
		try {
			series=queryService.getCustomTimeSeries(q);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return series;
	}
	
	@RequestMapping(value="getNumeric.html")
	public @ResponseBody List<Charting> getNumeric(@RequestParam(value="collection") String collection,@RequestParam(value="query") String query,@RequestParam(value="start_range") String start_range,@RequestParam(value="end_range") String end_range,@RequestParam(value="field_value") String field_value,@RequestParam(value="range_gap") String range_gap){
		List<Charting> series=new ArrayList<Charting>();
		Query q=new Query();
		q.setUrl(DatabaseConfig.solrUrl);
		q.setQuery(query);
		q.setCollection(collection);
		q.setChart("time");
		q.setStart_facet(start_range);
		q.setEnd_facet(end_range);
		q.setRange_gap(range_gap);
		q.setField_value(field_value);
		logger.debug("Inside getNumeric controller"+start_range+" End date"+end_range+" range_gap"+range_gap);
		try {
			series=queryService.getNumeric(q);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return series;
	}
	
	@RequestMapping(value="getResponseTimeSeries.html")
	public @ResponseBody List<Charting> getResponseTimeSeries(@RequestParam(value="collection") String collection,@RequestParam(value="month") String month,@RequestParam(value="query") String query){
		List<Charting> series=new ArrayList<Charting>();
		Query q=new Query();
		q.setUrl(DatabaseConfig.solrUrl);
		q.setQuery(query);
		q.setMonth(month);
		q.setCollection(collection);
		q.setChart("time");
		logger.debug("Inside getResponseTimeSeries controller");
		try {
			series=queryService.getResponseTimeSeries(q);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return series;
	}
	
	@RequestMapping(value="getPagingResponseTimeSeries.html")
	public @ResponseBody List<Charting> getPagingResponseTimeSeries(@RequestParam(value="collection") String collection,@RequestParam(value="month") String month,@RequestParam(value="query") String query,@RequestParam(value="page") int page){
		List<Charting> series=new ArrayList<Charting>();
		int start=(page-1)*1000;
		Query q=new Query();
		q.setUrl(DatabaseConfig.solrUrl);
		q.setQuery(query);
		q.setMonth(month);
		q.setStart(start);
		q.setCollection(collection);
		q.setChart("time");
		logger.debug("Inside getResponseTimeSeries controller");
		try {
			series=queryService.getResponseTimeSeries(q);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return series;
	}
	
	@RequestMapping(value="getCharts.html")
	public @ResponseBody List<Charting> getCharts(@RequestParam(value="collection") String collection,@RequestParam(value="chart") String chart,@RequestParam(value="field") String field){
		List<Charting> series=new ArrayList<Charting>();
		Query q=new Query();
		q.setUrl(DatabaseConfig.solrUrl);
		q.setCollection(collection);
		q.setChart(chart);
		q.setChart_filed(field);
		logger.debug("Inside getCharts controller---------chart"+chart+"-------field"+field);
		try {
			series=queryService.getCharts(q);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return series;
	}
	
	@RequestMapping(value="getCustomCharts.html")
	public @ResponseBody List<Charting> getCustomCharts(@RequestParam(value="collection") String collection,@RequestParam(value="chart") String chart,@RequestParam(value="field") String [] field,@RequestParam(value="limit") int limit,@RequestParam(value="query") String query,@RequestParam(value="filter") String filter){
		List<Charting> series=new ArrayList<Charting>();
		Query q=new Query();
		q.setUrl(DatabaseConfig.solrUrl);
		q.setCollection(collection);
		q.setQuery(query);
		q.setChart(chart);
		q.setSelected_fields(field);
		q.setFilter(filter);
		q.setRows(limit);
		logger.debug("Inside getCustomCharts controller---------filter"+filter);
		try {
			series=queryService.getCustomCharts(q);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return series;
	}
	
	
	@RequestMapping(value="getCustomChartsNofilter.html")
	public @ResponseBody List<Charting> getCustomChartsNofilter(@RequestParam(value="collection") String collection,@RequestParam(value="chart") String chart,@RequestParam(value="field") String [] field,@RequestParam(value="limit") int limit,@RequestParam(value="query") String query,@RequestParam(value="filter") String filter){
		List<Charting> series=new ArrayList<Charting>();
		Query q=new Query();
		q.setUrl(DatabaseConfig.solrUrl);
		q.setCollection(collection);
		q.setQuery(query);
		q.setChart(chart);
		q.setSelected_fields(field);
		q.setFilter(filter);
		q.setRows(limit);
		logger.debug("Inside getCustomCharts controller---------filter"+filter);
		try {
			series=queryService.getCustomChartsNofilter(q);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return series;
	}
	
	
	@RequestMapping(value="getCount.html")
	public @ResponseBody List<Charting> getCount(@RequestParam(value="collection") String collection,@RequestParam(value="field_value") String[] field_value,@RequestParam(value="query") String query){
		List<Charting> series=new ArrayList<Charting>();
		Query q=new Query();
		q.setUrl(DatabaseConfig.solrUrl);
		q.setCollection(collection);
		q.setQuery(query);
		q.setSelected_fields(field_value);
		try {
			series=queryService.getCount(q);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return series;
	}
	
	@RequestMapping(value="getStatusCharts.html")
	public @ResponseBody List<Charting> getStatusCharts(@RequestParam(value="collection") String collection,@RequestParam(value="chart") String chart,@RequestParam(value="field") String field,@RequestParam(value="limit") int limit,@RequestParam(value="query") String query,@RequestParam(value="filter") String filter){
		List<Charting> series=new ArrayList<Charting>();
		Query q=new Query();
		q.setUrl(DatabaseConfig.solrUrl);
		q.setCollection(collection);
		q.setQuery(query);
		q.setChart(chart);
		q.setChart_filed(field);
		q.setRows(limit);
		q.setFilter(filter);
		logger.debug("Inside getCustomCharts controller---------chart"+chart+"-------field"+field);
		try {
			series=queryService.getStatusCharts(q);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return series;
	}
	
	@RequestMapping(value="getURLCharts.html")
	public @ResponseBody List<Charting> getURLCharts(@RequestParam(value="collection") String collection,@RequestParam(value="chart") String chart,@RequestParam(value="field") String field,@RequestParam(value="limit") int limit,@RequestParam(value="query") String query,@RequestParam(value="filter") String filter){
		List<Charting> series=new ArrayList<Charting>();
		Query q=new Query();
		q.setUrl(DatabaseConfig.solrUrl);
		q.setCollection(collection);
		q.setQuery(query);
		q.setChart(chart);
		q.setChart_filed(field);
		q.setRows(limit);
		q.setFilter(filter);
		logger.debug("Inside getCustomCharts controller---------chart"+chart+"-------field"+field+"filter"+filter);
		try {
			series=queryService.getURLCharts(q);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return series;
	}
	
	@RequestMapping(value="getTopTenURLs.html")
	public @ResponseBody List<Charting> getTopTenURLs(@RequestParam(value="collection") String collection){
		List<Charting> series=new ArrayList<Charting>();
		Query q=new Query();
		q.setUrl(DatabaseConfig.solrUrl);
		q.setCollection(collection);
		try {
			series=queryService.getTopTenURLs(q);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return series;
	}
	
	@RequestMapping(value="getTopTenIPs.html")
	public @ResponseBody List<Charting> getTopTenIPs(@RequestParam(value="collection") String collection){
		List<Charting> series=new ArrayList<Charting>();
		Query q=new Query();
		q.setUrl(DatabaseConfig.solrUrl);
		q.setCollection(collection);
		try {
			series=queryService.getTopTenIPs(q);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return series;
	}
	
	@RequestMapping(value="getFailures.html")
	public @ResponseBody List<Charting> getFailures(@RequestParam(value="collection") String collection){
		List<Charting> series=new ArrayList<Charting>();
		Query q=new Query();
		q.setUrl(DatabaseConfig.solrUrl);
		q.setCollection(collection);
		try {
			series=queryService.getFailures(q);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return series;
	}
	
	@RequestMapping(value="getUrlCount.html")
	public @ResponseBody Query getUrlCount(@RequestParam(value="collection") String collection){
		Query q=new Query();
		q.setUrl(DatabaseConfig.solrUrl);
		q.setCollection(collection);
		try {
			q=queryService.getUrlCount(q);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return q;
	}
	
	@RequestMapping(value="getErrorCount.html")
	public @ResponseBody Query getErrorCount(@RequestParam(value="collection") String collection){
		Query q=new Query();
		q.setUrl(DatabaseConfig.solrUrl);
		q.setCollection(collection);
		try {
			q=queryService.getErrorCount(q);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return q;
	}
	
	@RequestMapping(value="getResponseTime.html")
	public @ResponseBody List<Charting> getResponseTime(@RequestParam(value="collection") String collection){
		List<Charting> series=new ArrayList<Charting>();
		Query q=new Query();
		q.setUrl(DatabaseConfig.solrUrl);
		q.setCollection(collection);
		try {
			series=queryService.getResponseTime(q);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return series;
	}
	
	@RequestMapping(value="getErrorCountSeries.html")
	public @ResponseBody List<Charting> getErrorCountSeries(@RequestParam(value="collection") String collection){
		List<Charting> series=new ArrayList<Charting>();
		Query q=new Query();
		q.setUrl(DatabaseConfig.solrUrl);
		q.setCollection(collection);
		try {
			series=queryService.getErrorCountSeries(q);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return series;
	}
	
	/*@RequestMapping(value="save_chart.html")
	public @ResponseBody Boolean saveChartPage(@RequestParam(value="chart_name")String chart_name,@RequestParam(value="chart_type")String chart_type,@RequestParam(value="collection")String collection,@RequestParam(value="field")List<String> field,@RequestParam(value="appsel")String appsel,@RequestParam(value="aggregator")String aggregator@RequestParam(value="field_value")String field_value){
		boolean check=true;
		Report report =new Report();
		//System.out.println("aggregator"+aggregator);
		Applications app=new Applications();
		app.setAppId(appsel);
		report.setAppId(app);
		report.setCollection(collection);
		report.setReportName(chart_name);
		report.setReportType(chart_type);
	
		if (chart_type.equalsIgnoreCase("x-y") || chart_type.equalsIgnoreCase("time")){
			field.add(field_value);
		}
		
			//report.setAggregator(aggregator);
		
			 String id=chartService.saveChartMaster(report);
			 report.setReportId(id);
			 
			 for(int p=0;p<field.size();p++){
				 ReportMetadata reportmeta= new ReportMetadata();
				 reportmeta.setReportId(report);
				 System.out.println("fieldvalue "+field.get(p));
				 reportmeta.setFieldName(field.get(p));
				
				 if(! chartService.saveChartMaster(reportmeta))
				 {
					check=false;
					return check;
				 }
				 
				}
		
		 
		 return check;
		
	}
	*/
	@RequestMapping(value="save_chart.html")
	public @ResponseBody void saveChartPage(HttpServletRequest request, HttpServletResponse response,@RequestParam(value="chart_name")String chart_name,@RequestParam(value="chart_type")String chart_type,@RequestParam(value="collection")String collection,@RequestParam(value="field")List<String> field,@RequestParam(value="appsel")String appsel,/*@RequestParam(value="aggregator")String aggregator*/@RequestParam(value="field_value")String field_value) throws IOException{
		boolean check=true;
		Report report =new Report();
		//System.out.println("aggregator"+aggregator);
		Applications app=new Applications();
		
		JSONObject json=new JSONObject();
		boolean isSaved = false;
		String saveFailureReason = "";
		PrintWriter writer = response.getWriter();
		response.setContentType("application/json");
		
		
		if(chart_name ==null)
		{
			saveFailureReason = "Chart Name cannot be null...!!";
			Lib.jsonPut(json,"saveFailureReason", saveFailureReason);
			Lib.jsonPut(json,"isSaved", isSaved);
			writer.write(json.toString());
			return;
		}
		
		List<String> chartNameList = chartService.getChartNameList();
		
		if(chartNameList.contains(chart_name))
		{
			saveFailureReason = "Report Name "+chart_name+" has been already used. Please use different report name.";
			Lib.jsonPut(json,"saveFailureReason", saveFailureReason);
			Lib.jsonPut(json,"isSaved", isSaved);
			writer.write(json.toString());
			return;
		}
		app.setAppId(appsel);
		report.setAppId(app);
		report.setCollection(collection);
		report.setReportName(chart_name);
		report.setReportType(chart_type);
	
		if (chart_type.equalsIgnoreCase("x-y") || chart_type.equalsIgnoreCase("time")){
			field.add(field_value);
		}
		
			//report.setAggregator(aggregator);
		
			 String id=chartService.saveChartMaster(report);
			 report.setReportId(id);
			 
			 for(int p=0;p<field.size();p++){
				 ReportMetadata reportmeta= new ReportMetadata();
				 reportmeta.setReportId(report);
				 System.out.println("fieldvalue "+field.get(p));
				 reportmeta.setFieldName(field.get(p));
				
				 if(! chartService.saveChartMaster(reportmeta))
				 {
					saveFailureReason = "Report metadata could not be saved...!!";
					Lib.jsonPut(json,"saveFailureReason", saveFailureReason);
					Lib.jsonPut(json,"isSaved", isSaved);
					writer.write(json.toString());
					return;
				 }
				 
				}
		
		 isSaved = true;
		 Lib.jsonPut(json,"isSaved", isSaved);
		 
		 writer.write(json.toString());
		 return;
		
	}
	
	@RequestMapping(value="getChartnames.html")
	public @ResponseBody List<Report> getCharts(){
		logger.debug("Inside getCharts controller");
		List<Report> chartList = new ArrayList<Report>();
		try {
			chartList=chartService.getCharts();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return chartList;
	}
	
	@RequestMapping(value="drawChart.html")
	public @ResponseBody List<Charting> drawChart(@RequestParam(value="chart_name")String chart_name){
		logger.debug("Inside drawChart controller");
		Report chart = new Report();
		List<Charting> chart_data=new ArrayList<Charting>();
		chart.setReportName(chart_name);
		try {
			chart_data=chartService.drawChart(chart);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return chart_data;
	}
	
	@RequestMapping(value="savedashboard.html")
	public @ResponseBody void savedashboard(@RequestParam(value="charts")String[] charts,@RequestParam(value="dashboard")String dashboard_name,@RequestParam(value="widths")int[] widths,@RequestParam(value="heights")int[] heights,@RequestParam(value="lefts")float[] lefts,@RequestParam(value="tops")float[] tops,@RequestParam(value="divs")String[] divs,@RequestParam(value="chart_types")String[] chart_types, HttpServletRequest request, HttpServletResponse response) throws IOException{
		logger.debug("Inside savedashboard controller"+charts.length);
		List<DashboardMaster> dash_charts=new ArrayList<DashboardMaster>();
		
		JSONObject json=new JSONObject();
		boolean isSaved = false;
		String saveFailureReason = "";
		PrintWriter writer = response.getWriter();
		response.setContentType("application/json");
		
		if(dashboard_name ==null)
		{
			saveFailureReason = "Dashboard Name cannot be null...!!";
			Lib.jsonPut(json,"saveFailureReason", saveFailureReason);
			Lib.jsonPut(json,"isSaved", isSaved);
			writer.write(json.toString());
			return;
		}
		List<String> dashboardNameList = chartService.getDashboardNameList();
		if (dashboardNameList == null || dashboardNameList.isEmpty() || dashboardNameList.contains(dashboard_name)) {
			saveFailureReason = "Dashboard Name "+dashboard_name+" has been already used. Please use different dashboard name.";
			Lib.jsonPut(json,"saveFailureReason", saveFailureReason);
			Lib.jsonPut(json,"isSaved", isSaved);
			writer.write(json.toString());
			return;
		}
			int sequence = 1;
			for (int i = 0; i < charts.length; i++) {
				String chartId = chartService.getReportIdfromName(charts[i]);
				DashboardMaster dash = new DashboardMaster();
				dash.setChart_name(charts[i]);
				dash.setChartId(chartId);
				dash.setSequence(sequence);
				dash.setDashboard_name(dashboard_name);
				dash.setHeight(heights[i]);
				dash.setWidth(widths[i]);
				dash.setLeft(lefts[i]);
				dash.setTop(tops[i]);
				dash.setDiv_name(divs[i]);
				dash.setChart_type(chart_types[i]);
				dash_charts.add(dash);
				sequence++;
			}
			 if(!chartService.savedashboard(dash_charts))
			 {
				 saveFailureReason = "Dashboard could not be saved....!!";
					Lib.jsonPut(json,"saveFailureReason", saveFailureReason);
					Lib.jsonPut(json,"isSaved", isSaved);
					writer.write(json.toString());
					return;
			 }
			 isSaved = true;
			 Lib.jsonPut(json,"isSaved", isSaved);
			 
			 writer.write(json.toString());
			 return;
	}
	
	
	@RequestMapping(value="setSequence.html")
	public @ResponseBody boolean setSequence(@RequestParam(value="sequence_array")String[] sequenceArray,@RequestParam(value="dashboardName")String dashName){
		logger.debug("Inside setSequence controller");
		
		return chartService.setSequence(sequenceArray,dashName);
	}
	
	@RequestMapping(value="getDashboardnames.html")
	public @ResponseBody List<DashboardMaster> getDashboardnames(){
		logger.debug("Inside getCharts controller");
		List<DashboardMaster> dashboardList = new ArrayList<DashboardMaster>();
		try {
			dashboardList=chartService.getDashboards();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dashboardList;
	}
	
	@RequestMapping(value="drawDashboard.html")
	public @ResponseBody List<Dashboard> drawDashboard(@RequestParam(value="dash_name")String dash_name){
		logger.debug("Inside drawDashboard controller");
		DashboardMaster dashboard = new DashboardMaster();
		List<Dashboard> dashboard_data=new ArrayList<Dashboard>();
		dashboard.setDashboard_name(dash_name);
		try {
			dashboard_data=chartService.drawDashboard(dashboard);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dashboard_data;
	}




@RequestMapping(value="removeChartfromDash.html")
public @ResponseBody boolean removeChartfromDash(@RequestParam(value="chart_name")String chart_name,@RequestParam(value="dashboardName")String dashboardName){
	logger.debug("Inside removeChartfromDash controller");
	
	return chartService.removeChartfromDash(chart_name,dashboardName);
}

@RequestMapping(value="getQueryForGrid.html")
public @ResponseBody List<Object> getQueryForGrid(@RequestParam(value="collection") String collection,@RequestParam(value="from_date") String from_date,@RequestParam(value="to_date") String to_date){
	List<Object> abc = null;
	logger.debug("QueryController :: Inside getSelectedFieldsQuery :: DatabaseConfig.solrUrl = "+ DatabaseConfig.solrUrl);
	logger.debug("QueryController :: Inside getSelectedFieldsQuery :: Collection = "+ collection);
	Query query=new Query();
	query.setUrl(DatabaseConfig.solrUrl);
	query.setCollection(collection);
	query.setQuery("*:*");
	query.setRows(-1);
	query.setStart_facet(from_date);
	query.setEnd_facet(to_date);
	query.setRange_gap("+1DAY");
	
	logger.debug("QueryController :: Inside getSelectedFieldsQuery :: Query = "+query.toString());
	try {
		 abc= queryService.getMapforSolrData(query);
		System.out.println("Grid Result :");
		System.out.println(abc);
	} catch (Exception e) {
		e.printStackTrace();
	}
	return abc;
	
}

@RequestMapping(value="getLogDataNoFilter.html")
public @ResponseBody List<Object> getLogDataNoFilter(@RequestParam(value="collection") String collection,@RequestParam(value="query") String query,@RequestParam(value="start") int start,@RequestParam(value="rows") int rows,@RequestParam(value="filter") String filter,@RequestParam(value="sort_field") String sort_field,@RequestParam(value="sort_order")String sort_order,@RequestParam(value="fields") String[] fields){
	List<Object> abc = null;
	logger.debug("QueryController :: Inside getSelectedFieldsQuery :: DatabaseConfig.solrUrl = "+ DatabaseConfig.solrUrl);
	logger.debug("QueryController :: Inside getSelectedFieldsQuery :: Collection = "+ collection);
	Query q=new Query();
	q.setUrl(DatabaseConfig.solrUrl);
	q.setQuery(query);
	q.setStart(start);
	q.setRows(rows);
	q.setFilter(filter);
	q.setCollection(collection);
	q.setSort_field(sort_field);
	q.setSort_order(sort_order);
	q.setSelected_fields(fields);
	logger.debug("QueryController :: Inside getSelectedFieldsQuery :: Query = "+q.toString());
	try {
		 abc= queryService.getMapforSolrData(q);
		System.out.println(abc);
	} catch (Exception e) {
		e.printStackTrace();
	}
	return abc;
}

@RequestMapping(value="kibanaDashboard.html")
public ModelAndView openDashboard(ModelMap map,HttpServletRequest request){
	
	return new ModelAndView("Dashboard/kibanaDashboard");
}
}
