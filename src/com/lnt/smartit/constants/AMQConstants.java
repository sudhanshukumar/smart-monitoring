package com.lnt.smartit.constants;

public class AMQConstants {
	public static final String ACTIVEMQ_QUEUE_NAME = "smartitHyperion";//"ITPREDECTIVEQUEUE";
	//public static final String ACTIVEMQ_QUEUE_NAME = "RKTestQueue1";
	public static final String ACTIVEMQURL = "tcp://10.101.3.146:61616";
	public static final String EMAIL_ID_SEPERATOR = ",";
	public static final String proxyHost = "172.25.74.10";

	public static final int proxyPort = 2006;
}
