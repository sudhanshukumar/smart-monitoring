package com.lnt.smartit.models;

import java.io.Serializable;


public class URLLog implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String tag;
	private String url;
	private int responseCode;
	private String Status;
	private int timeToConnect;
	private String date;
	private long dataReceivedTime;
	private String hostName;
	
	public long getDataReceivedTime() {
		return dataReceivedTime;
	}
	public void setDataReceivedTime(long dataReceivedTime) {
		this.dataReceivedTime = dataReceivedTime;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public int getTimeToConnect() {
		return timeToConnect;
	}
	public void setTimeToConnect(int timeToConnect) {
		this.timeToConnect = timeToConnect;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	@Override
	public String toString() {
		return "URLLog [tag=" + tag + ", url=" + url + ", responseCode="
				+ responseCode + ", Status=" + Status + ", timeToConnect="
				+ timeToConnect + ", date=" + date + ", dataReceivedTime="
				+ dataReceivedTime + ", hostName=" + hostName + "]";
	}
}
