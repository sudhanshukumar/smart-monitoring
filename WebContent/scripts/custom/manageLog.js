/**
 * 
 */
$(document).ready(function() {
	$("#confirmDelete").hide();
	$("#alert").hide();
	$("#addNewLog").click(function() {
		
		window.location.href = "addNewLog.html";
	});
	manageLogGrid();

});

function manageLogGrid() {
	$("#addedLogGrpHistory")
	.jqGrid(
			{
				jsonReader : {
					root : "gridData", // an array that contains the
										// actual data
					page : "page", // current page of the query
					total : "total", // total pages for the query
					records : "records",// total number of records for
										// the query
					repeatitems : true,
					id : "id" // the unique id of the row
				},
				url : 'populateLogGrid.html',
				datatype : "json",
				mtype : "POST",
				colNames : [ 'Threshold Log Size Id', 'Host Name','Log File Name','Warning Threshold Size','Critical Threshold Size', 'Actions' ],
				colModel : [
						{
							name : 'id',
							label : 'Threshold Log Size Id',
							align : 'left',
							width : 30,
							hidden:true,
							sortable: true
						},
						{
							name : 'hostName',
							label : 'Host Name',
							align : 'left',
							width : 50,
							sortable: true
						},
						{
							name : 'logFileName',
							label : 'Log File Name',
							align : 'left',
							width : 70,
							sortable: true
						},
						{
							name : 'warningThresholdSize',
							label : 'Warning Threshold Siz',
							align : 'left',
							width : 70,
							sortable: true
						},
						{
							name : 'criticalThresholdSize',
							label : 'Critical Threshold Size',
							align : 'left',
							width : 70,
							sortable: true
						},
						{
							width : 80,
							fixed : true,
							sortable : false,
							resize : false,
							formatter : function(cellvalue, options,
									rowObject) {

								return addedServiceActionButtons(cellvalue,
										options, rowObject);
							}
						} ],
				pager : '#addedLogGrpPager',
				rowNum : 10,
				rowList : [ 10, 20, 30 ],
				viewrecords : true,
				gridview : true,
				loadonce: true,
				width : 900,
				height: 300
			});

}

function addedServiceActionButtons(cellvalue, options, rowObject) {
var previewLogLink= '<img title="Preview" onClick=previewLog("'
    +encodeURI(JSON.stringify(rowObject))
    +'") src="images/common/preview-16.png"/>';

var updateLogLink = '<img title="Edit"  onClick=editLog("'
	+ encodeURI(JSON.stringify(rowObject))
	+ '"); src="images/common/edit-6-16.png" />';

var deleteLogLink = '<img title="Delete" onClick=deleteLog("'
	+ encodeURI(JSON.stringify(rowObject))
	+ '") src="images/common/delete-3-16.png"/>';

var actionLink = "&nbsp;" + previewLogLink + "&nbsp;" + "&nbsp;" + updateLogLink+ "&nbsp;" + "&nbsp;" + deleteLogLink;
return actionLink;
}

function deleteLog(value){
	$("#confirmDelete").show();
	$( "#confirmDelete" ).dialog({
		 resizable: false,
		 autoOpen: false,
		 height:200,
		 width: 340,
		 position: [485, 185],
		 modal: true,
		 buttons: {
			 "Yes": function() {
				 	var dataJSON = JSON.parse(decodeURI(value));
				 	var id=dataJSON.id;
				 	$.ajax({
				 		type : "POST",
				 		url : "deleteLog.html",
				 		data : "id=" + id, 
				 		success : function(response) {
				 			alert(response);
				 			window.location.href = "manageLog.html";
				 		},
				 		cache: false
				 	}); 
			 },
				Cancel: function() {
	 							$( this ).dialog( "close" );
	 						}
		 }
		 
	});
	
	$("#confirmDelete").dialog("open");
	$(".ui-dialog-title").text('Delete?');
	$(".ui-dialog-titlebar-close").hide();
	$("#confirmDelete").text("Delete this Log ?");
	
}
function editLog(value){
	var dataJSON = JSON.parse(decodeURI(value));
	var id=dataJSON.id;	
	window.location.href =  "addNewLog.html?id="+id+"&action=edit";
	
}
function previewLog(value){
	var dataJSON = JSON.parse(decodeURI(value));
	var id=dataJSON.id;
	window.location.href =  "addNewLog.html?id="+id+"&action=preview";	
}