package com.lnt.smartit.mysql.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="app_log_param")
public class LogParams implements Serializable{
	
	@Id
	@Column(name="id")
	private String appId;
	
	@ManyToOne
	@JoinColumn(name = "app_log_id", referencedColumnName = "id", insertable = false, updatable = false)
	private AppLogMaster log;
	
	@Column(name="param_name")
	private String paramName;
	
	@Column(name="param_value_max")
	private String maxValue;
	
	@Column(name="param_value_min")
	private String minValue;
	
	@Column(name="description")
	private String description;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public AppLogMaster getLog() {
		return log;
	}

	public void setLog(AppLogMaster log) {
		this.log = log;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(String maxValue) {
		this.maxValue = maxValue;
	}

	public String getMinValue() {
		return minValue;
	}

	public void setMinValue(String minValue) {
		this.minValue = minValue;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "LogParams [appId=" + appId + ", log=" + log + ", paramName="
				+ paramName + ", maxValue=" + maxValue + ", minValue="
				+ minValue + ", description=" + description + "]";
	}
}
