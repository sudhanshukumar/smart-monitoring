$(document).ready(function() {
});

function getapplication() {
	var col = "shiksha_cloud_demo";
	var date_filter="request_date:[2015-03-02T00:00:00Z TO 2015-04-06T12:59:59Z]";
	getsortingfields(col);
	var query = $("#query").val();
	var start = $("#start").val();
	var rows = $("#rows").val();
	var filter = date_filter;
	var sort_field = null;
	var sort_order = $('#sorting_order').find(":selected").text();
	var fields = [];
	$.each($("input[name='select_fields']:checked"), function() {
		fields.push($(this).val());
	});
	getresults(col, query, start, rows, filter, sort_field, sort_order);
}

function getcollections() {
	 $.ajax({
			type : "GET",
			url : "getCollections.html",

			success : function(response) {
				for ( var i = 0; i < response.length; i++) {
					$("#collections1").append(
							$('<option>').text(response[i].col_name))
							.append($('</option>'));

				}
				$("#col_title").html(response[0].col_name);
				getsortingfields(response[0].col_name);
				$('#collections1').change(
								function() {
									var selected_field = $('#collections1').find(":selected").text();
									$("#col_title").html(selected_field);
									getsortingfields(selected_field);
									var query = $("#query").val();
									var start = $("#start").val();
									var rows = $("#rows").val();
									var filter = $("#filter").val();
									var col = $("#col_title").text();
									var sort_field = $('#sorting').find(":selected").text();
									var sort_order = $('#sorting_order').find(":selected").text();
									$("#result_grid").show(500);
									var fields = [];
									$.each(
											$("input[name='select_fields']:checked"),
											function() {
												fields.push($(this)
														.val());
											});
									getresults(col, query, start, rows,
											filter, sort_field, sort_order);
								});
			}
		});
}

function getresults(col, query, start, rows, filter, sort_field, sorting_order) {
	
	$.ajax({
		type : "POST",
		url : "getBasicQuery.html",
		data : "query=" + query + "&start=" + start + "&rows=" + rows
				+ "&filter=" + filter + "&collection=" + col + "&sort_field="
				+ sort_field + "&sort_order=" + sorting_order,
		success : function(response) {

			if(col=="shiksha_cloud_demo"){
				$("#records_count").text(
						"CampusNext Access Logs-Total Records-"
								+ response.total_hits);
			}
			else if(col="shiksha_cloud_error"){
				$("#records_count").text(
						"CampusNext Error Logs-Total Records-"
								+ response.total_hits);
			}
			getschema(col);
		}
	});
}

function getselectedfields(col, query, start, rows, filter, sort_field,sorting_order, fields) {

	$.ajax({
		type : "POST",
		url : "getSelectedFieldsQuery.html",
		data : "query=" + query + "&start=" + start + "&rows=" + rows
				+ "&filter=" + filter + "&collection=" + col + "&sort_field="
				+ sort_field + "&sort_order=" + sorting_order + "&fields="
				+ fields,
		success : function(response) {
			$("#outId").find("tr:gt(0)").remove();
			for ( var i = 0; i < response.results.length; i++) {
				$("#outId").find('tbody').append(
						$('<tr>').append(
						$('<td>').text(i + 1).append($('</td>'))).append(
								$('<td>').text(response.results[i]).append(
										$('</td>'))));
			}
			$("#records_count").text(
					"CampusNext Access Logs- Total Records-"
							+ response.total_hits);
		}
	});
}

function getschema(col_name) {
	  $.ajax({
				type : "POST",
				url : "getSchema.html",
				data : "collection=" + col_name,
				success : function(response) {

					$("#resId").find("tr:gt(0)").remove();
					for ( var i = 0; i < response.length; i++) {
						if (response[i].name != null) {
							$("#resId").find('tbody').append(
									$('<tr>').append(
											$('<td>').append(
													'<input class="check" name="select_fields" type="checkbox" value='
															+ response[i].name
															+ '>').append(
													$('</td>'))).append(
											$('<td>').text(response[i].name)
													.append($('</td>'))));
						}
					}
					$('.check').prop('checked', true);
					var selected_field = $('#logs').find(":selected").val();
					var col = "shiksha_cloud_demo";
					var date_filter="";
					var sort_field=null;
					var from_date = $("#from_datepicker").val();
					var to_date = $("#to_datepicker").val();
					var temp_start_datetime = from_date.split(" ");
					var final_from_date = temp_start_datetime[0]+ "T"+ temp_start_datetime[1]+ "Z";
					var temp_end_datetime = to_date.split(" ");
					var final_to_date = temp_end_datetime[0]+ "T"+ temp_end_datetime[1]+ "Z";
			
					if (selected_field == "access") {
						col = "shiksha_cloud_demo";
						date_filter = "request_date:["+ final_from_date + " TO "+ final_to_date + "]";
						sort_field = $('#sorting').find(
						":selected").text();
					} else if (selected_field == "error") {
						col = "shiksha_cloud_error";
						date_filter = "Date:["+ final_from_date + " TO "+ final_to_date + "]";
						sort_field=null;
					}
					var query = $("#query").val();
					var start = $("#start").val();
					var rows = $("#rows").val();
					var filter = date_filter;
					var sort_order = $('#sorting_order').find(":selected").text();
					$("#result_grid").show(500);
					var fields = [];
					$.each(
							$("input[name='select_fields']:checked"),
							function() {
								fields.push($(this)
										.val());
							});
					getselectedfields(col, query, start,
							rows, filter, sort_field,
							sort_order, fields);
					$(".check")
							.change(
									function() {
										var selected_field = $('#logs').find(":selected").val();
										var col = "shiksha_cloud_demo";
										var date_filter="";
										var sort_field=null;
										var from_date = $("#from_datepicker").val();
										var to_date = $("#to_datepicker").val();
										var temp_start_datetime = from_date.split(" ");
										var final_from_date = temp_start_datetime[0]+ "T"+ temp_start_datetime[1]+ "Z";
										var temp_end_datetime = to_date.split(" ");
										var final_to_date = temp_end_datetime[0]+ "T"+ temp_end_datetime[1]+ "Z";
										if (selected_field == "access") {
											col = "shiksha_cloud_demo";
											date_filter = "request_date:["+ final_from_date + " TO "+ final_to_date + "]";
											sort_field = $('#sorting').find(
											":selected").text();
										} else if (selected_field == "error") {
											col = "shiksha_cloud_error";
											date_filter = "Date:["+ final_from_date + " TO "+ final_to_date + "]";
											sort_field=null;
										}
										var query = $("#query").val();
										var start = $("#start").val();
										var rows = $("#rows").val();
										var filter = date_filter;
										var sort_order = $('#sorting_order').find(":selected").text();
										$("#result_grid").show(500);
										var fields = [];
										$.each(
												$("input[name='select_fields']:checked"),
												function() {
													fields.push($(this)
															.val());
												});
										getselectedfields(col, query, start,
												rows, filter, sort_field,
												sort_order, fields);
									});
				}
			});
}

function getsortingfields(col) {
	$.ajax({
		type : "POST",
		url : "getSchema.html",
		data : "collection=" + col,
		success : function(response) {

			$('#sorting').empty();
			for ( var i = 0; i < response.length; i++) {

				if (response[i].type == "long") {

					$("#sorting").append($('<option>').text(response[i].name))
							.append($('</option>'));
				}
			}
		}
	});
}

function activate()
{
$("#element2").addClass("active");
$("#element1").removeClass("active");
}
	
$(function() {
	
	$('#access').prop('checked', true);
	$('#error').prop('checked', true);
	$('#java').prop('checked', true);
	$('#machine').prop('checked', true);
	$('#from_datepicker').datetimepicker({
		format : 'Y-m-d H:i:s',
		dayOfWeekStart : 1,
		lang : 'en'
	});
	
	var d = new Date();
	d.setHours(d.getHours()-12);
	var temp_from_date=d.getDate();
	var temp_from_month=d.getMonth()+1;
	if(temp_from_date<10){
		temp_from_date="0"+temp_from_date;
	}
	if(temp_from_month<10){
		temp_from_month="0"+temp_from_month;
	}
	
	var temp_to_date=d.getFullYear()+"-"+temp_from_month+"-"+temp_from_date+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
	$('#from_datepicker').datetimepicker({
		value : temp_to_date
	});

	$('#to_datepicker').datetimepicker({
		format : 'Y-m-d H:i:s',
		dayOfWeekStart : 1,
		lang : 'en'
	});
	
	var dt = new Date();
	var temp_date=dt.getDate();
	var temp_month=dt.getMonth()+1;
	if(temp_date<10){
		temp_date="0"+temp_date;
	}
	if(temp_month<10){
	temp_month="0"+temp_month;
	}
	
	var final_to_date=dt.getFullYear()+"-"+temp_month+"-"+temp_date+" "+dt.getHours()+":"+dt.getMinutes()+":"+dt.getSeconds();
	$('#to_datepicker').datetimepicker({
		value : final_to_date
	});
		 $( "#accordion" ).accordion();
});
