  function loadWindowsRAM(i, parameterName, dataobj){
	  var hostName = dataobj.alertMessageHostName;
		
		//check for . since it gives error as illegal character
		if(hostName.indexOf(".") > -1) {
			hostName = hostName.split(".").join("_");
		}
		/* appending accordian label starts */
		/* putting i+1 since ids start with 1 and i starts with 0 */
		$("#label_ac-1").show();
		/* appending accordian label ends */
		/* appending split1 to section starts */
		$("#ac1_container_small").show();
		$("#ac1_block_1")
				.append(
						"	<div class='block_split1' id='ac-1_split1_"
								+ hostName
								+ "' onClick=showAC1Details("
								+ (i + 1)
								+ ",id,"
								+ AC1Split1Count
								+ ",'"
								+ dataobj.hazelcastMapName
								+ "','"
								+ parameterName
								+ "','"
								+ dataobj.osType	
								+ "','"
								+ dataobj.alertMessageHostName
								+ "')>"
								+ "<table>"
								+ "<thead>"
								+ "<tr>"
								+ "<th>"
								+ strings['left_block_split1.header']
								+ " "
								+ dataobj.alertMessageHostName
								+" OS = "
								+ dataobj.osType
								+ "<br>   C = "
								+ dataobj.winRAM.critical_High
								+ " Mb    W = "
								+ dataobj.winRAM.warning_High
								+ " Mb <br>"
								+ "<span class='alertdatetime' id='ac1_split1_datespan_"
								+hostName+"'>"+new Date(dataobj.alertMessageDatetime).toString()+"</span>"
								+ "</th>"
								+ "</tr>"
								+ "</thead>"
								+ "</table>"
								+ "<table>"
								+ "<tbody>"
								+ "<tr>"
								+ "	<td class='col1'>"
								+ strings['ac-1_split1.column1']
								+ "<span id='ac1_split1_col1_"
								+ hostName
								+ "'>"
								+ dataobj.winRAM.totalPhysicalMemory
								+ "</span></td>"
								+ "<td class='col2'>"
								+ strings['ac-1_split1.column2']
								+ "<span id='ac1_split1_col2_"
								+ hostName
								+ "'>"
								+ ((dataobj.winRAM.memoryUsage / dataobj.winRAM.totalPhysicalMemory) * 100).toFixed(2)
								+ "</span></td>"
								+ "<td class='col3'>"
								+ strings['ac-1_split1.column3']
								+ "<span id='ac1_split1_col3_"
								+ hostName
								+ "'>"
								+ dataobj.winRAM.memoryUsage
								+ "</span></td>"
								+ "</tr>"
								+ "<tr>"
								+ "<td class='col1'>"
								+ strings['ac-1_split1.column4']
								+ "<span id='ac1_split1_col4_"
								+ hostName
								+ "'>"
								+ dataobj.winRAM.usedVirtualMemory
								+ "</span></td>"
								+ "<td class='col2'>"
								+ strings['ac-1_split1.column5']
								+ "<span id='ac1_split1_col5_"
								+ hostName
								+ "'>"
								+ dataobj.winRAM.totalPhysicalMemory
								+ "</span></td>"
								+ "<td class='col3'>"
								+ strings['ac-1_split1.column6']
								+ "<span id='ac1_split1_col6_"
								+ hostName
								+ "'>"
								+ dataobj.winRAM.availableVirtualMEmory
								+ "</span></td>" 
								+ "</tr>" + "</tbody>" + "</table>" + "</div>");
		/* appending split1 to section ends */
		AC1Split1Count++;
  }
	
  function loadLinuxRam(i, parameterName, dataobj){
	  
	  var hostName = dataobj.alertMessageHostName;
		//check for . since it gives error as illegal character
		if(hostName.indexOf(".") > -1) {
			hostName = hostName.split(".").join("_");
		}
	  
		/* appending accordian label starts */
		/* putting i+1 since ids start with 1 and i starts with 0 */
		$("#label_ac-1").show();
		$("#ac1_container_small").show();
		$("#ac1_block_1")
		.append(
				"	<div class='block_split1' id='ac-1_split1_"
						+ hostName
						+ "' onClick=showAC1Details("
						+ (i + 1)
						+ ",id,"
						+ AC1Split1Count
						+ ",'"
						+ dataobj.hazelcastMapName
						+ "','"
						+ parameterName
						+ "','"
						+ dataobj.osType	
						+ "','"
						+dataobj.alertMessageHostName
						+"')>"
						+ "<table>"
						+ "<thead>"
						+ "<tr>"
						+ "<th>"
						+ strings['left_block_split1.header']
						+ " "
						+ dataobj.alertMessageHostName
						+" OS = "
						+ dataobj.osType
						+ "<br>   C = "
						+ dataobj.linuxRAM.threshold_high_val
						+ " Mb    W = "
						+ dataobj.linuxRAM.threshold_low_val
						+ " Mb <br>" 
						+ "<span class='alertdatetime' id='ac1_split1_datespan_"
						+hostName+"'>"+new Date(dataobj.alertMessageDatetime).toString()+"</span>"
						+ " </th>"
						+ "</tr>"
						+ "</thead>"
						+ "</table>"
						+ "<table>"
						+ "<tbody>"
						+ "<tr>"
						+ "	<td class='col1'>"
						+ strings['ac-1_split1.column1_linux']
						+ "<span id='ac1_split1_col1_"
						+ hostName
						+ "'>"
						+ dataobj.linuxRAM.memCached
						+ "</span></td>"
						+ "<td class='col2'>"
						+ strings['ac-1_split1.column2_linux']
						+ "<span id='ac1_split1_col2_"
						+ hostName
						+ "'>"
						+ ((dataobj.linuxRAM.memUsed / dataobj.linuxRAM.memTotal) * 100).toFixed(2)
						+ "</span></td>"
						+ "<td class='col3'>"
						+ strings['ac-1_split1.column3_linux']
						+ "<span id='ac1_split1_col3_"
						+ hostName
						+ "'>"
						+ dataobj.linuxRAM.memUsed
						+ "</span></td>"
						+ "</tr>"
						+ "<tr>"
						+ "<td class='col1'>"
						+ strings['ac-1_split1.column4_linux']
						+ "<span id='ac1_split1_col4_"
						+ hostName
						+ "'>"
						+ dataobj.linuxRAM.memBuffers
						+ "</span></td>"
						+ "<td class='col2'>"
						+ strings['ac-1_split1.column5_linux']
						+ "<span id='ac1_split1_col5_"
						+ hostName
						+ "'>"
						+ dataobj.linuxRAM.memTotal
						+ "</span></td>"
						+ "<td class='col3'>"
						+ strings['ac-1_split1.column6_linux']
						+ "<span id='ac1_split1_col6_"
						+ hostName
						+ "'>"
						+ dataobj.linuxRAM.memFree
						+ "</span></td>" +

						"</tr>" + "</tbody>" + "</table>" + "</div>");
				/* appending split1 to section ends */
		AC1Split1Count++;
}
  
  function showAC1Details(blockNumber, id, totalBlocks, vm, param, osType,host) {
	  	/* Onclick event for left block split 1 for Accordian 1 that is RAM */

	  	/*
	  	 * clear rest of the split1 blocks toshow only selected block with id as
	  	 * 'id' result
	  	 */
	  var yAxisData=[];
		 var xAxisData=[];
		 yAxisData.push('Memory Used');
		 xAxisData.push('time');
  	$.ajax({
			type : "POST",
			url : "additionalInfo.html",
			data : "parameterName="+param+"&addressedMachine="+vm+"&osType="+osType,

			success : function(response) {
				var result = response.split("@#$");
				var objList = JSON.parse(result[1]);
				/** **data for chart** */
				var series1 = [];
				
				for(b=0;b<objList.length;b++){
					if(osType=="Windows"){
						xAxisData.push(objList[b].dataReceivedTime);
						yAxisData.push(objList[b].memoryUsage);
					series1.push([ objList[b].dataReceivedTime,  
					               objList[b].memoryUsage ]);
					  }
					else if(osType=="Linux"){
						xAxisData.push(objList[b].dataReceivedTime);
						yAxisData.push(objList[b].memUsed);
						series1.push([ objList[b].dataReceivedTime,  
						               objList[b].memUsed ]);
						  }
				}
				//$("#ac1_container_small_second div").empty(); // clear everything and then append
				$("#ac1_block_2 div").empty(); 			//this is to refresh same graph
				$("#ac1_block_2")
						.append(
								 "<div class='block_split2' id='ac-1_rightDiv_"
										+ blockNumber
										+ "'>"
										+ "</div>"

						);
				//c3 chart
				$('#ac-1_rightDiv_' + blockNumber).show();
				var chart = c3.generate({
					bindto: '#ac-1_rightDiv_' + blockNumber,
					data: {
				        x: 'time',
				        columns: [
			              xAxisData,         
			              yAxisData
				        ]
				    },
				    
				    title : { 
				    	text : 'RAM usage trend for last 12 hours of '+host
				    },
				    subchart: {
				        show: true
				    },
				    zoom: {
				        enabled: true
				    },
				    axis : {
				        x : {
				        	label:'Time',
				            type : 'timeseries',
				            tick: {
				            	fit:true,
				            	count: objList.length,
				                format:'%Y-%m-%d %I:%M:%S' /* function (x) { return x.getFullYear(); } */
				              //format: '%Y' // format string is also available for timeseries data
				            }
				        },
				        y : {
				        	label:'Memory Used'
				        }
				    }
				});
			}
  			});
	  }