/*function loadWindowsDISK(i, parameterName, dataobj){
	  var hostName = dataobj.alertMessageHostName;
		
		//check for . since it gives error as illegal character
		if(hostName.indexOf(".") > -1) {
			hostName = hostName.split(".").join("_");
		}
	console.log('Param name is DISK for linux in generateskeleton');
	
	$("#label_ac-3").show();
	$("#ac3_container_small").show();
	$("#ac3_block_1").append(				
					 "<div class='block_split1' id='ac-3_split1_"
					+ hostName
					+ "' onClick=showAC3Details("+ (i + 1)+ ",id,"+ AC3Split1Count + ",'"
					+ dataobj.hazelcastMapName + "','"
					+ parameterName
					+ "','"
					+ dataobj.osType	
					+ "','"
					+dataobj.alertMessageHostName
					+"')>"
					+"<strong>VM: "+dataobj.alertMessageHostName+"</strong>"
					+" <strong>OS: "+dataobj.osType+"</strong><br>"
					+ "<span class='alertdatetime' style='color:black' id='ac3_split1_datespan_"
					+hostName+"'>"+new Date(dataobj.alertMessageDatetime).toString()+"</span>"
					+ "<table id='ac-3_split1_table_"+hostName+"'>"	+ "<thead>"	+ "<tr>"+ "<th>Drive</th>"
					+"<th>Used</th>"
					+"<th>C</th>"
					+"<th>W</th>"
					+ "</tr>"+ "</thead>"
					+ "<tbody>"
					+ "</tbody>" + "</table>"+"</div>"
					);
	console.log('WinDISK div got appended with id '+'ac-3_split1_'+ hostName);
	//filesystem looping
	var fileSystemArr = dataobj.winDisk.fileSystem;
	var appendRow = '';
	for(var count=0;count<fileSystemArr.length;count++){
		appendRow= "<tr><td class='ac-3_col1'>"
		+ "<span id='ac-3_split1_col1_"+ hostName+ "'>"
		+ fileSystemArr[count].name
		+ "</span></td>"
		+ "<td class='ac-3_col2'>"
		+ "<span id='ac-3_split1_col2_"
		+ hostName
		+ "'>"
		+ fileSystemArr[count].usePercent
		+ "%</span></td>"
		+ "<td class='ac-3_col3'>"
		+ "<span id='ac-3_split1_col3_"
		+ hostName
		+ "'>"
		+ dataobj.winDisk.critical_High
		+ "%</span></td>"
		+ "<td class='ac-3_col4'>"
		+ "<span id='ac-3_split1_col4_"
		+ hostName
		+ "'>"
		+ dataobj.winDisk.warning_High
		+ "%</span></td>"
		+"</tr>";
		
		$("#ac-3_split1_table_"+hostName).last().append(appendRow);
	}
	
	AC3Split1Count++;

}
function loadLinuxDISK(i, parameterName, dataobj){
	  var hostName = dataobj.alertMessageHostName;
		
		//check for . since it gives error as illegal character
		if(hostName.indexOf(".") > -1) {
			hostName = hostName.split(".").join("_");
		}
	console.log('Param name is DISK for WINDows in generateskeleton');

	$("#label_ac-3").show();
	$("#ac3_container_small").show();
	$("#ac3_block_1").append(				
					 "<div class='block_split1' id='ac-3_split1_"
					+ hostName
					+ "' onClick=showAC3Details("+ (i + 1)+ ",id,"+ AC3Split1Count + ",'"
					+ dataobj.hazelcastMapName + "','"
					+ parameterName
					+ "','"
					+ dataobj.osType	
					+ "','"
					+dataobj.alertMessageHostName
					+"')>"
					+"<table>"+ "<thead>"+ "<tr>"+ "<th>"
					+ strings['left_block_split1.header']
					+ "<span id='ac-2_split1_header_label'>"
					+ dataobj.alertMessageHostName
					+" OS: "+dataobj.osType
					+ "<br>   C = "
					+ dataobj.linuxDisk.threshold_high_val
					+ "   W = "
					+ dataobj.linuxDisk.threshold_low_val
					+ "</span><br>"
					+ "<span class='alertdatetime' id='ac3_split1_datespan_"
					+hostName+"'>"+new Date(dataobj.alertMessageDatetime).toString()+"</span>"
					+ "</th>"+ "</tr>"
					+ "</thead>"+ "</table>"
					+ "<table>"
					+ "<tbody>"
					+"<tr>"
					+ "<td class='ac-2_col1'> "
					+ "Used"
					+ "<br><span id='ac-2_split1_col1_"
					+ hostName
					+ "'>"
					+dataobj.linuxDisk.totalUsePercent+"%"
					+ "</span></td>"
					+ "</tr>" 
					+ "</tbody>" + "</table>" 
					+"</div>"
					);
	console.log('LinuxDISK div got appended with id '+'ac-3_split1_'+ hostName);
	AC3Split1Count++;

}*/


function draw_chart(divid,vm,osType,param,host){
//	alert("inside utility"+divid+vm+osType+param+host);
	if(osType=="Windows"){
		if (param == "RAM") {
			 loadWindowsRAM(divid,vm, param, host,osType);
		}
		
		if (param == "CPU") {
			loadWindowsCPU(divid,vm, param, host,osType);
		}
		
		if (param == "DISK") {
			loadWindowsDISK(divid,vm, param, host,osType);
		}
	}
	
	if(osType=="Linux"){
		if (param == "RAM") {
			 loadLinuxRam(divid,vm, param, host,osType);
		}
		
		if (param == "CPU") {
			loadLinuxCPU(divid,vm, param, host,osType);
		}

		if (param == "DISK") {
			loadLinuxDISK(divid,vm, param, host,osType);
		}
	}
	
	
}



function loadWindowsDISK(divid,vm, param, host,osType) {
	 var yAxisData=[];
	 var xAxisData=[];
	   var columnData=[];	 
	$.ajax({

		type : "POST",

		url : "additionalInfo.html",

		data : "parameterName=" + param + "&addressedMachine=" + vm+"&osType="+osType,

		success : function(response) {
			/*yAxisData.push('Used');
			xAxisData.push('time');*/
			var result = response.split("@#$");
		
			var chartArray = JSON.parse(result[1]);
			//var chartArray = '[{"dataReceivedTime":1465905603178,"totalUsePercent":null,"lstfileSystemFromCache":[{"name":"C","size":199.9,"used":66.17,"avail":133.73,"usePercent":33.1,"mountedOn":null,"dataReceivedTime_hours":0},{"name":"D","size":265.76,"used":45.39,"avail":220.37,"usePercent":17.08,"mountedOn":null,"dataReceivedTime_hours":0}]},{"dataReceivedTime":1465905838834,"totalUsePercent":null,"lstfileSystemFromCache":[{"name":"C","size":199.9,"used":66.17,"avail":133.73,"usePercent":33.1,"mountedOn":null,"dataReceivedTime_hours":0},{"name":"D","size":265.76,"used":45.39,"avail":220.37,"usePercent":17.08,"mountedOn":null,"dataReceivedTime_hours":0}]},{"dataReceivedTime":1465906173081,"totalUsePercent":null,"lstfileSystemFromCache":[{"name":"C","size":199.9,"used":66.17,"avail":133.73,"usePercent":33.1,"mountedOn":null,"dataReceivedTime_hours":0},{"name":"D","size":265.76,"used":45.39,"avail":220.37,"usePercent":17.08,"mountedOn":null,"dataReceivedTime_hours":0}]},{"dataReceivedTime":1465905840109,"totalUsePercent":null,"lstfileSystemFromCache":[{"name":"C","size":199.9,"used":66.17,"avail":133.73,"usePercent":33.1,"mountedOn":null,"dataReceivedTime_hours":0},{"name":"D","size":265.76,"used":45.39,"avail":220.37,"usePercent":17.08,"mountedOn":null,"dataReceivedTime_hours":0}]},{"dataReceivedTime":1465906161125,"totalUsePercent":null,"lstfileSystemFromCache":[{"name":"C","size":199.9,"used":66.17,"avail":133.73,"usePercent":33.1,"mountedOn":null,"dataReceivedTime_hours":0},{"name":"D","size":265.76,"used":45.39,"avail":220.37,"usePercent":17.08,"mountedOn":null,"dataReceivedTime_hours":0}]},{"dataReceivedTime":1465906113132,"totalUsePercent":null,"lstfileSystemFromCache":[{"name":"C","size":199.9,"used":66.17,"avail":133.73,"usePercent":33.1,"mountedOn":null,"dataReceivedTime_hours":0},{"name":"D","size":265.76,"used":45.39,"avail":220.37,"usePercent":17.08,"mountedOn":null,"dataReceivedTime_hours":0}]}]';

			//chartArray= JSON.parse(chartArray);
			var temp_array=[];
			for (var chart = 0; chart < chartArray.length; chart++) {
				
					for(var i = 0; i < chartArray[chart].lstfileSystemFromCache.length; i++){
						
						temp_array.push(chartArray[chart].lstfileSystemFromCache[i].name);
						
						console.log(chartArray[chart].lstfileSystemFromCache[i].name);
						console.log(chartArray[chart].lstfileSystemFromCache[i].used);

						console.log(chartArray[chart].dataReceivedTime);
				}
			}
			var unique=temp_array.filter(function(itm,i,temp_array){
			    return i==temp_array.indexOf(itm);
			});
			
			
			console.log("uniqueieweeee"+unique);
			
	for(var i=0;i<unique.length;i++){
		yAxisData.push([unique[i]]);
		xAxisData.push(['x'+unique[i]]);
			for (var chart = 0; chart < chartArray.length; chart++) {
				for(var j = 0; j < chartArray[chart].lstfileSystemFromCache.length; j++){
					if(unique[i]==chartArray[chart].lstfileSystemFromCache[j].name){
						yAxisData[i].push(chartArray[chart].lstfileSystemFromCache[j].used);
		                xAxisData[i].push(chartArray[chart].dataReceivedTime);
					}
			    }
			}
				
	}
			var xs = {};
			for(var i=0;i<xAxisData.length;i++){
				columnData.push(xAxisData[i]);
				columnData.push(yAxisData[i]);
				xs[yAxisData[i][0]] = xAxisData[i][0];
			}
			
			
			var chart = c3.generate({
					bindto: '#drawChart_' + divid,
					data: {
						xs:xs,
				        columns: columnData
				    },
				    title : {
						text : 'Drivewise Usage for last 12 hours of '+host,
					},
				    
				    subchart: {
				        show: true
				    },
				    zoom: {
				        enabled: true
				    },
				    axis : {
				        x : {
				        	label:'Time',
				            type : 'timeseries',
				            tick: {
				            	fit:true,
				            	//count: chartArray.length,
				                format:'%Y-%m-%d %I:%M:%S' 
				            }
				        },
				        y : {
				        	label:'Used'
				        }
				    }
				});
			
			/*var chartSeries = new Array();
			var dataReceivedTime = new Array();
			var jsonArray = new Array();
			var seriesNameArr = new Array();

			create list of series names available
			for (var chart = 0; chart < chartArray.length; chart++) {
				var fileSystemList = chartArray[chart].lstfileSystemFromCache;
				for(var count=0;count<fileSystemList.length;count++){
					if(seriesNameArr.indexOf(fileSystemList[count].name) == -1){
					seriesNameArr.push(fileSystemList[count].name)
					}
				}
			}
			
			//create data array for each series name and push to jsonArray 
				for(var a=0;a<seriesNameArr.length;a++){
					var dataArray = new Array();
					for (var chart = 0; chart < chartArray.length; chart++) {
						var fileSystemList = chartArray[chart].lstfileSystemFromCache;
					for(var b=0;b<fileSystemList.length;b++){
						if(seriesNameArr[a] == fileSystemList[b].name){
							var used = fileSystemList[b].used.toFixed(2);
							dataArray.push([chartArray[chart].dataReceivedTime, parseFloat(used)]);
							
						}
					}
					
					}
					dataArray = dataArray.sort(function(a, b) {
						return a[0] - b[0];
					});
					var json = {
						"name":	seriesNameArr[a],
						"data":dataArray
					};
					jsonArray.push(json);
				}
			
			 for(var c=0;c<jsonArray.length;c++){
				 options.series.push(jsonArray[c]);
			 }*/
		}	 
	});
}


function loadLinuxDISK(divid,vm, param, host,osType){
	
	 var yAxisData=[];
	 var xAxisData=[];
		 
	$.ajax({

		type : "POST",

		url : "additionalInfo.html",

		data : "parameterName=" + param + "&addressedMachine=" + vm+"&osType="+osType,

		success : function(response) {
		 yAxisData.push('Use Percent');
		 xAxisData.push('time');
		 response= response.split("@#$");
		 var fileSystems = JSON.parse(response[1]);
		 var filesystemfortable=JSON.parse(response[0]);
			var chartArray = fileSystems;

/*			$("#ac3_block_2").show();
			$("#ac3_block_2 div").empty(); //this is to refresh same graph instead of creating new
			$("#ac3_block_2").append(
					"<div id='ac3_container_small_second'>"
							+ "<div class='block_split2' id='ac-3_rightDiv_"
							+ blockNumber
							+ "'></div>"
							+

							"<div class='block_split3' id='ac-3_split3_"
							+ blockNumber
							+ "'>"
							+ "<h3 class='ac-table_header'>"
							+ strings['ac-3_split3_1.header']
							+ "</h3>"
							+ "<div id='ac-3_split3_1"
							+ blockNumber
							+ "' class='block_split3_divForTable'>"
							+ "<table id='ac-3_table1_"
							+ blockNumber
							+ "' cellpadding='6' class='ac-3_table1'>"
							+ "<thead>"
							+ "<tr>"
							+ "<th>"
							+ strings['ac-3_split3_1.table1.column1']
							+ "</th>"
							+ "<th>"
							+ strings['ac-3_split3_1.table1.column2']
							+ "</th>"
							+ "<th>"
							+ strings['ac-3_split3_1.table1.column3']
							+ "</th>"
							+ "<th>"
							+ strings['ac-3_split3_1.table1.column4']
							+ "</th>"
							+ "<th>"
							+ strings['ac-3_split3_1.table1.column5']
							+ "</th>"
							+ "<th>"
							+ strings['ac-3_split3_1.table1.column6']
							+ "</th>"
							+ "</tr>"
							+ "</thead>"
							+ "<tbody>"
							+ "</tbody>"
							+ "</table>"
							+ "</div>" + "</div>" + "</div>" + "</div>");

			$("#ac-3_table1_" + blockNumber + " tbody tr").each(function() {
				this.parentNode.removeChild(this);

			});

			for ( var a = 0; a < filesystemfortable.fileSystem.length; a++) {

				var appendRow = "<tr>" + "<td>" + filesystemfortable.fileSystem[a].name + "</td>"
						+ "<td>" + filesystemfortable.fileSystem[a].size + "</td>" + "<td>"
						+ filesystemfortable.fileSystem[a].used + "</td>" + "<td>"
						+ filesystemfortable.fileSystem[a].avail + "</td>" + "<td>"
						+ filesystemfortable.fileSystem[a].usePercent + "</td>" + "<td>"
						+ filesystemfortable.fileSystem[a].mountedOn + "</td>" + "</tr>";
				$("#ac-3_table1_" + blockNumber).last().append(appendRow);
			}

			$('#ac-3_rightDiv_' + blockNumber).show();*/

	/*		var size = new Array();
			var used = new Array();
			var avail = new Array();
			var usePercent = new Array();
			var dataReceivedTime_hours = new Array();
			var linDiskArray=new Array();
			
			for ( var chart = 0; chart < chartArray.length; chart++) {
				size.push(chartArray[chart].size);
				used.push(chartArray[chart].used);
				avail.push(Math.floor(chartArray[chart].avail));
				usePercent.push(chartArray[chart].totalUsePercent);
				dataReceivedTime_hours.push(chartArray[chart].dataReceivedTime);
			}
			for ( var chart = 0; chart < chartArray.length; chart++) {
				xAxisData.push(dataReceivedTime_hours[chart]);
				yAxisData.push(usePercent[chart]);
				linDiskArray.push([dataReceivedTime_hours[chart],usePercent[chart]]);
			}
			console.log(linDiskArray);
			
			linDiskArray = linDiskArray.sort(function(a, b) {
				return a[0] - b[0];
			});*/
			//c3 charts code
			var chart = c3.generate({
					bindto: '#drawChart_' + divid,
					data: {
				        x: 'time',
				        columns: [
			              xAxisData,         
			              yAxisData
				        ]
				    },
				    
				    title : {
						text : 'DISK Trend for last 12 hours of '+host,
					},
				    
				    subchart: {
				        show: true
				    },
				    zoom: {
				        enabled: true
				    },
				    axis : {
				        x : {
				        	label:'Time',
				            type : 'timeseries',
				            tick: {
				            	fit:true,
				            	count: chartArray.length,
				                format:'%Y-%m-%d %I:%M:%S'
				            }
				        },
				        y : {
				        	label:'Use percent'
				        }
				    }
				});
	 }
	});

	}


function loadWindowsRAM(divid,vm, param, host,osType){
	
	  var yAxisData=[];
		 var xAxisData=[];
		 yAxisData.push('Memory Used');
		 xAxisData.push('time');
	$.ajax({
			type : "POST",
			url : "additionalInfo.html",
			data : "parameterName="+param+"&addressedMachine="+vm+"&osType="+osType,

			success : function(response) {
				var result = response.split("@#$");
				var objList = JSON.parse(result[1]);
				/** **data for chart** */
				var series1 = [];
				
				for(b=0;b<objList.length;b++){
						xAxisData.push(objList[b].dataReceivedTime);
						yAxisData.push(objList[b].memoryUsage);
					series1.push([ objList[b].dataReceivedTime,  
					               objList[b].memoryUsage ]);
				}
				/*$("#ac1_block_2 div").empty(); 			//this is to refresh same graph
				$("#ac1_block_2")
						.append(
								 "<div class='block_split2' id='ac-1_rightDiv_"
										+ blockNumber
										+ "'>"
										+ "</div>"

						);*/
				//c3 chart
				//$('#ac-1_rightDiv_' + blockNumber).show();
				var chart = c3.generate({
					bindto: '#drawChart_' + divid,
					data: {
				        x: 'time',
				        columns: [
			              xAxisData,         
			              yAxisData
				        ]
				    },
				    
				    title : { 
				    	text : 'RAM usage trend for last 12 hours of '+host
				    },
				    subchart: {
				        show: true
				    },
				    zoom: {
				        enabled: true
				    },
				    axis : {
				        x : {
				        	label:'Time',
				            type : 'timeseries',
				            tick: {
				            	fit:true,
				            	count: objList.length,
				                format:'%Y-%m-%d %I:%M:%S' /* function (x) { return x.getFullYear(); } */
				              //format: '%Y' // format string is also available for timeseries data
				            }
				        },
				        y : {
				        	label:'Memory Used'
				        }
				    }
				});
			}
			});	
	
	
}


function loadLinuxRam(divid,vm, param, host,osType){
	 var yAxisData=[];
	 var xAxisData=[];
	 yAxisData.push('Memory Used');
	 xAxisData.push('time');
$.ajax({
		type : "POST",
		url : "additionalInfo.html",
		data : "parameterName="+param+"&addressedMachine="+vm+"&osType="+osType,

		success : function(response) {
			var result = response.split("@#$");
			var objList = JSON.parse(result[1]);
			/** **data for chart** */
			var series1 = [];
			
			for(b=0;b<objList.length;b++){
				
					xAxisData.push(objList[b].dataReceivedTime);
					yAxisData.push(objList[b].memUsed);
					series1.push([ objList[b].dataReceivedTime,  
					               objList[b].memUsed ]);
					  
			}
			
			var chart = c3.generate({
				bindto:'#drawChart_' + divid,
				data: {
			        x: 'time',
			        columns: [
		              xAxisData,         
		              yAxisData
			        ]
			    },
			    
			    title : { 
			    	text : 'RAM usage trend for last 12 hours of '+host
			    },
			    subchart: {
			        show: true
			    },
			    zoom: {
			        enabled: true
			    },
			    axis : {
			        x : {
			        	label:'Time',
			            type : 'timeseries',
			            tick: {
			            	fit:true,
			            	count: objList.length,
			                format:'%Y-%m-%d %I:%M:%S' 
			            }
			        },
			        y : {
			        	label:'Memory Used'
			        }
			    }
			});
	
		}
});

}

function loadWindowsCPU(divid,vm, param, host,osType){
	 var yAxisData=[];
	 var xAxisData=[];

	 xAxisData.push('time');
	$.ajax({

		type : "POST",

		url : "additionalInfo.html",

		data : "parameterName="+param+"&addressedMachine="+vm+"&osType="+osType,

		success : function(response) {
			var result = response.split("@#$");
			var chartArray = JSON.parse(result[1]);
			for ( var chart = 0; chart < chartArray.length; chart++) {
					yAxisData[0]='CPU Usage in %';
					xAxisData.push(chartArray[chart].dataReceivedTime);
					yAxisData.push(chartArray[chart].cpuUsageInPercent);
			}
			
			
			
			
			/*$("#ac2_block_2 div").empty(); // this is to refresh
											// same graph
			$("#ac2_block_2").append(
					"<div class='block_split2' id='ac-2_rightDiv_"
							+ blockNumber + "'></div>");

			$('#ac-2_rightDiv_' + blockNumber).show();*/
			//c3 charts
			var chart = c3.generate({
				bindto: '#drawChart_' + divid,
				data: {
			        x: 'time',
			        columns: [
		              xAxisData,         
		              yAxisData
		             
			        ]
			    },
			  title: {
				  text : 'CPU Trend for last 12 hours of '+host,
				},
			    subchart: {
			        show: true
			    },
			    zoom: {
			        enabled: true
			    },
			    axis : {
			        x : {
			        	label:'Time',
			            type : 'timeseries',
			            tick: {
			            	fit:true,
                        	count: chartArray.length,
			                format:'%Y-%m-%d %I:%M:%S'
			            }
			        },
			        y : {
			        	label:yAxisData[0]
			        }
			    }
			});
		}
	});	
	
}

function loadLinuxCPU(divid,vm, param, host,osType){
	 var yAxisData=[];
	 var xAxisData=[];

	 xAxisData.push('time');
	$.ajax({

		type : "POST",

		url : "additionalInfo.html",

		data : "parameterName="+param+"&addressedMachine="+vm+"&osType="+osType,

		success : function(response) {
			var result = response.split("@#$");
			var chartArray = JSON.parse(result[1]);
			for ( var chart = 0; chart < chartArray.length; chart++) {
			
					yAxisData[0]='Load Average 5 min';
					xAxisData.push(chartArray[chart].cpudataReceivedTime);
					yAxisData.push(chartArray[chart].loadAverage5min);
			}
			
			/*$("#ac2_block_2 div").empty(); // this is to refresh
											// same graph
			$("#ac2_block_2").append(
					"<div class='block_split2' id='ac-2_rightDiv_"
							+ blockNumber + "'></div>");

			$('#ac-2_rightDiv_' + blockNumber).show();*/
			//c3 charts
			var chart = c3.generate({
				bindto: '#drawChart_' + divid,
				data: {
			        x: 'time',
			        columns: [
		              xAxisData,         
		              yAxisData
			        ]
			    },
			  title: {
				  text : 'CPU Trend for last 12 hours of '+host,
				},
			    subchart: {
			        show: true
			    },
			    zoom: {
			        enabled: true
			    },
			    axis : {
			        x : {
			        	label:'Time',
			            type : 'timeseries',
			            tick: {
			            	fit:true,
			            	count: chartArray.length,
			                format:'%Y-%m-%d %I:%M:%S'
			            }
			        },
			        y : {
			        	label:yAxisData[0]
			        }
			    }
			});
		}
	});	
	
}