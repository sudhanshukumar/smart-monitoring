package com.lnt.smartit.constants;

public class URLConstants {
	 public static String URL_DEL        = "/";
	  public static String HTTP_PROTOCOL  = "http://";
	  public static String HTTP_PROTOCOLS = "https://";
	  public static String AMP_DEL        = "&";
	  public static String EQL_DEL        = "=";
	  public static String QUES_DEL       = "?";
	  public static String TILDE_DEL      = "~";
	  public static String COLON_DEL      = ":";
	  public static String OPEN_BRACKET   = "(";
	  public static String CLOSE_BRACKET  = ")";
	  public static String COMMA_DEL      = ",";
	  public static String SPACE          = "%20";
	  public static String PLUS           = "+";
}
