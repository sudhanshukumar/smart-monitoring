package com.lnt.smartit.solr.pojo;

import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "chart_master")
public class ChartMaster {
	
	private int unique_id;
	private int limit_rows;
	private String user_id;
	private String chart_name;
	private String chart_id;
	private String chart_type;
	private String collection_name;
	private String field;
	private String field_label;
	private String query;
	
	public int getUnique_id() {
		return unique_id;
	}
	public void setUnique_id(int unique_id) {
		this.unique_id = unique_id;
	}
	
	public int getLimit() {
		return limit_rows;
	}
	public void setLimit(int limit) {
		this.limit_rows = limit;
	}
	
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getChart_name() {
		return chart_name;
	}
	public void setChart_name(String chart_name) {
		this.chart_name = chart_name;
	}
	
	public String getChart_id() {
		return chart_id;
	}
	public void setChart_id(String chart_id) {
		this.chart_id = chart_id;
	}
	
	public String getChart_type() {
		return chart_type;
	}
	public void setChart_type(String chart_type) {
		this.chart_type = chart_type;
	}
	
	public String getCollection_name() {
		return collection_name;
	}
	public void setCollection_name(String collection_name) {
		this.collection_name = collection_name;
	}
	
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	
	public String getField_label() {
		return field_label;
	}
	public void setField_label(String field_label) {
		this.field_label = field_label;
	}
	
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
}
