package com.lnt.smartit.models;


public class SystemInfoThreshold {
	
	private String host;
	private String str_ParameterID;
	private double warning_High;
	private double critical_High;
	private double actual_usage;
	private String thresholdId;
	
	public double getActual_usage() {
		return actual_usage;
	}
	public void setActual_usage(double actual_usage) {
		this.actual_usage = actual_usage;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getStr_ParameterID() {
		return str_ParameterID;
	}
	public void setStr_ParameterID(String str_ParameterID) {
		this.str_ParameterID = str_ParameterID;
	}
	public double getWarning_High() {
		return warning_High;
	}
	public void setWarning_High(double warning_High) {
		this.warning_High = warning_High;
	}
	public double getCritical_High() {
		return critical_High;
	}
	public void setCritical_High(double critical_High) {
		this.critical_High = critical_High;
	}
	
	public String getThresholdId() {
		return thresholdId;
	}
	
	public void setThresholdId(String thresholdId) {
		this.thresholdId = thresholdId;
	}
	
	@Override
	public String toString() {
		return "SystemInfoThreshold [host=" + host + ", str_ParameterID="
				+ str_ParameterID + ", warning_High=" + warning_High
				+ ", critical_High=" + critical_High + ", actual_usage="
				+ actual_usage + "]";
	}
}
