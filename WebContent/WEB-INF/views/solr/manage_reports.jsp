<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html >
<html>
<head>
 <meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../assets/ico/favicon.ico">
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

<title>SmartIT</title>

<!-- Bootstrap Styles -->
<link rel="stylesheet" type="text/css"
	href="assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="assets/css/bootstrap-responsive.css">
<link rel="stylesheet" type="text/css"
	href="assets/css/bootstrap-theme.min.css">

<!-- Common Styles -->
<link rel="stylesheet" type="text/css" href="assets/css/footer.css" />
<link href="css/custom/general/style1366.css" rel="stylesheet" />
<link href="css/custom/general/style1024.css" rel="stylesheet" />
<link href="css/custom/general/bluetabs.css" rel="stylesheet" />

<!-- Jquery css -->

<script src="assets/js/jquery-1.9.1.min.js"></script>

<script src="assets/js/jquery-1.10.2.js"></script>

<script src="scripts/common/jquery-ui.js"></script>
	<link href="css/common/jquery-ui-1.9.1.custom.css" rel="stylesheet" />
<!-- Jquery scripts -->
<script type="text/javascript" src="scripts/common/grid.locale-en.js"></script>
<script src="scripts/common/grid.locale-en.js"></script>

<!-- Bootstrap scripts -->
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>


  
  




<!-- Custom javascripts -->
<script type="text/javascript" src="scripts/custom/manageReports.js"></script>
<script type="text/javascript" src="scripts/common/menu/dropdowntabs.js"></script>




</head>
<body>

<header>
		<%@ include file="../common/header.jsp"%>
	</header>


	<nav id="bluemenu" class="bluetabs">
		<ul>
			<li><a href="dashboard.html"><spring:message
						code="label.admin.menu1"></spring:message></a></li>
			<li><a href="problemHistory.html"><spring:message
						code="label.admin.menu2"></spring:message></a></li>
			<li><a href="#" rel="dropmenu1_b"><spring:message
						code="label.admin.menu3"></spring:message></a></li>
			<li><a href="manageNotificationGroup.html"><spring:message
						code="label.admin.menu4"></spring:message></a></li>

 			<li><a id="selected" href="#" rel="dropmenu1_solr"><spring:message
 						code="label.admin.solr.manage.reports"></spring:message></a></li> 
			<li><a href="query.html"><spring:message
						code="label.admin.solr.query"></spring:message></a></li>
						  <li><a href="correlation.html"><spring:message
						code="label.admin.correlation"></spring:message></a></li>
		</ul>
	</nav>
	<div class="ddcolortabsline">&nbsp;</div>

	<div id="dropmenu1_b" class="dropmenudiv_b">
		<a href="manageDevice.html"><spring:message
				code="label.admin.menu3.submenu1"></spring:message></a> <a
			href="manageThresHold.html"><spring:message
				code="label.admin.menu3.submenu2"></spring:message></a> 
			<a href="manageApplication.html"><spring:message
				code="label.admin.menu3.submenu3"></spring:message></a> 
			<a href="manageService.html"><spring:message
				code="label.admin.menu3.submenu4"></spring:message></a> 
			<a href="manageUsers.html"><spring:message
				code="label.admin.menu3.submenu5"></spring:message></a>
<a href="manageLog.html"><spring:message
				code="label.admin.menu3.submenu6"></spring:message></a>

	</div>
	
	<div id="dropmenu1_solr" class="dropmenudiv_b">
	 <a href="charts.html"><spring:message
				code="label.admin.solr.charts"></spring:message></a> <a
			href="createuserdashboard.html"><spring:message
				code="label.admin.solr.custom.dashboard"></spring:message></a> 
				<a	href="list_dashboard.html"><spring:message
				code="label.admin.solr.view.dashboard"></spring:message></a> 
			<%-- <a href="manage_reports.html"><spring:message
				code="label.admin.solr.manage.reports"></spring:message></a>  --%>
				<a href="manage_dashboard.html"><spring:message
				code="label.admin.solr.manage.dashboard"></spring:message></a> 
	</div>

	<script type="text/javascript">
		//SYNTAX: tabdropdown.init("menu_id", [integer OR "auto"])
		tabdropdown.init("bluemenu")
	</script>


<div class="container" style="margin-top: 30px;">
  
  
  <div class="panel-group" id="accordion">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Delete Reports</a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse">
        <div class="panel-body">
        <div class="row">
        <p style="padding-left: 41px;"><strong>Note:</strong> The Reports displayed here are the ones not included in any dashboard.</p>
        <div class="col-md-5" style="border: 0px solid #000;">
						<form class="form-horizontal" role="form">
							<div class="form-group">
								<label class="col-sm-4" style="padding: 10px 0px 10px 40px">Select Report:</label>
								
	<div class="row" id="result_grid"  style="overflow-y: scroll; height: 250px; border: 0.1px solid #b2b2b2; margin-left: -15px;">
					
							<div class="col-md-12" style=" padding-right: 0px;  padding-left: 0px;">
								<table class="table table-bordered" id="resId">
									<thead>
										<tr>
											<th></th>
											<th>Report Name</th>

										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>

							</div>


				</div>

					</div>
					</form>
				</div>
        </div>
          <div class="row">
        
					<div class="col-md-6">
						<input type="button" class="btn btn-primary" id="deleteReports"
							name="Delete" value="Delete"  style="margin-left: 400px;"/>

					</div>
        </div>
        
        
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Select fields for Reports</a>
        </h4>
      </div>
      <div id="collapse2" class="panel-collapse collapse">
        <div class="panel-body">
       
				<div class="row">
					<div class="col-md-5" style="border: 0px solid #000;">
						<form class="form-horizontal" role="form">
							<div class="form-group">
								<label class="col-sm-4" style="padding: 10px 0px 10px 40px">Select Application:</label>
								<div class="col-sm-8">
							<select  class="form-control" id="application">
						
							</select>
							
						</div>

					</div>
					</form>
				</div>
				</div>
         <div class="row">
         <div class="col-md-5" style="border: 0px solid #000;">
						<form class="form-horizontal" role="form">
							<div class="form-group">
								<label class="col-sm-4" style="padding: 10px 0px 10px 40px">Select Log Type:</label>
								<div class="col-sm-8">
									<select class="form-control" id="colValue">
									
									</select>
								</div>
								
								</div>
								</form>
								</div>
         
         
         </div>
          <div class="row" id="fieldrow">
            <div class="col-md-5" style="border: 0px solid #000;">
						<form class="form-horizontal" role="form">
							<div class="form-group">
								<label class="col-sm-4" id="fieldLable" style="padding: 10px 0px 10px 40px">Select Fields:</label>
								
	<div class="row" id="field_grid"  style="overflow-y: scroll; height: 250px; border: 0.1px solid #b2b2b2; margin-left: -15px;">
							<div class="col-md-12" style=" padding-right: 0px;  padding-left: 0px; " >
								<table class="table table-bordered" id="fieldId">
									<thead>
										<tr>
											<th></th>
											<th>Field Name</th>

										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>

					</div>
				</div>

					</div>
					</form>
				</div>
          
          </div>
          
			  <div class="row">
			  
        
					<div class="col-md-6">
						<input type="button" class="btn btn-primary" id="selectFields"
							name="Select" value="Save"  style="margin-left: 400px;"/>

					</div>
        
			  
			  </div>        
        </div>
      </div>
    </div>
  <!--   <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Set Filters</a>
        </h4>
      </div>
      <div id="collapse3" class="panel-collapse collapse ">
        <div class="panel-body">
        <div class="row" style="margin-bottom: 10px;">
        <div class="panel panel-default" style=" margin-left: 20px;  margin-right: 20px;">
      <div class="panel-heading">
        <h2 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion1" href="#collapsesub1">Set Primary Filters</a>
        </h2>
      </div>
      <div id="collapsesub1" class="panel-collapse collapse ">
        <div class="panel-body">
        	<div class="row">
					<div class="col-md-5" style="border: 0px solid #000;">
						<form class="form-horizontal" role="form">
							<div class="form-group">
								<label class="col-sm-4" style="padding: 10px 0px 10px 40px">Select Application:</label>
								<div class="col-sm-8">
							<select  class="form-control" id="applicationforFilter">
						
							</select>
							
						</div>

					</div>
					</form>
				</div>
				</div>
           <div class="row">
         <div class="col-md-5" style="border: 0px solid #000;">
						<form class="form-horizontal" role="form">
							<div class="form-group">
								<label class="col-sm-4" style="padding: 10px 0px 10px 40px">Select Log Type:</label>
								<div class="col-sm-8">
									<select class="form-control" id="colValueforFilter">
									
									</select>
								</div>
								
								</div>
								</form>
								</div>
         
         
         </div>
         <div class="row" id="fieldrow">
            <div class="col-md-5" style="border: 0px solid #000;">
						<form class="form-horizontal" role="form">
							<div class="form-group">
								<label class="col-sm-4" id="fieldLable" style="padding: 10px 0px 10px 40px">Select Fields:</label>
				<div class="col-sm-8">
									<select class="form-control" id="primaryFields">
									
									</select>
								</div>				
					</div>
					</form>
				</div>
          
          </div>
           <div class="row">
			  
        
					<div class="col-md-6">
						<input type="button" class="btn btn-primary" id="selectPrimary"
							name="Select" value="Save"  style="margin-left: 40px;"/>

					</div>
        
			  
			  </div>  
        </div>
        </div>
      </div>
    </div>
    <div class="row">
        <div class="panel panel-default" style=" margin-left: 20px;  margin-right: 20px;">
      <div class="panel-heading">
        <h2 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion1" href="#collapsesub2">Set Secondary Filters</a>
        </h2>
      </div>
      <div id="collapsesub2" class="panel-collapse collapse ">
        <div class="panel-body">
         	<div class="row">
					<div class="col-md-5" style="border: 0px solid #000;">
						<form class="form-horizontal" role="form">
							<div class="form-group">
								<label class="col-sm-4" style="padding: 10px 0px 10px 40px">Select Application:</label>
								<div class="col-sm-8">
							<select  class="form-control" id="applicationforSecFilter">
						
							</select>
							
						</div>

					</div>
					</form>
				</div>
				</div>
           <div class="row">
         <div class="col-md-5" style="border: 0px solid #000;">
						<form class="form-horizontal" role="form">
							<div class="form-group">
								<label class="col-sm-4" style="padding: 10px 0px 10px 40px">Select Log Type:</label>
								<div class="col-sm-8">
									<select class="form-control" id="colValueforSecFilter">
									
									</select>
								</div>
								
								</div>
								</form>
								</div>
         
         
         </div>
         <div class="row" id="fieldSecrow">
            <div class="col-md-5" style="border: 0px solid #000;">
						<form class="form-horizontal" role="form">
							<div class="form-group">
								<label class="col-sm-4" id="fieldLable" style="padding: 10px 0px 10px 40px">Select Fields:</label>
					<div class="row" id="secondary_fields"  style="overflow-y: scroll; height: 250px;">
							<div class="col-md-12" style=" padding-right: 0px;  padding-left: 0px;">
								<table class="table table-bordered" id="secFieldId">
									<thead>
										<tr>
											<th></th>
											<th>Field Name</th>

										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>

					</div>
				</div>			
					</div>
					</form>
				</div>
          
          </div>
           <div class="row">
			  
        
					<div class="col-md-6">
						<input type="button" class="btn btn-primary" id="selectSecondary"
							name="Select" value="Save"  style="margin-left: 40px;"/>

					</div>
        
			  
			  </div>  
        </div>
        </div>
      </div>
    </div>
        </div>
      </div>
    </div> -->
  </div> 
</div>
    


<footer>
		<%@ include file="../common/footer.jsp"%>
	</footer>
</body>
</html>



