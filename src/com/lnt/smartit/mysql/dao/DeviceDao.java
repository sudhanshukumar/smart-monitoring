package com.lnt.smartit.mysql.dao;

import java.util.List;

import com.lnt.smartit.mysql.model.AppDeviceMaster;
import com.lnt.smartit.mysql.model.DeviceParams;

public interface DeviceDao {
	
	
	public void saveAppDeviceMaster(AppDeviceMaster appDeviceMaster);
	public void saveDeviceparams(DeviceParams deviceparams);
	public Integer getMaxAppDeviceId();
	public String getDeviceTypecode(String searchCriteria);
	public AppDeviceMaster getAppDeviceMasterById(AppDeviceMaster deviceMaster);
	public String deleteAppDeviceMaster(AppDeviceMaster devMaster);
	public String getDeviceIdByHostname(String hostname);
	public void deleteDeviceParams(String deviceId);
	public List<DeviceParams> getDeviceParams(String appId);
	public List<DeviceParams> getDeviceParamsById(String deviceid);
	public AppDeviceMaster getOsById(String deviceid);
	public void updateDeviceparams(DeviceParams deviceparams);
	public void updateAppDeviceMaster(AppDeviceMaster appdevicemaster1);
	
}
