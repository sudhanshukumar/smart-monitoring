<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../assets/ico/favicon.ico">

<title>SmartIT</title>

<!-- Jquery scripts -->
<script type="text/javascript" src="assets/js/jquery-1.9.1.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="scripts/common/jquery-ui.js"></script>
<!-- <script src="scripts/common/jquery-ui.min.js"></script> -->
<script src="scripts/common/grid.locale-en.js"></script>
<script src="scripts/common/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="scripts/common/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="scripts/common/exportData/jquery-dateFormat.min.js"></script>

<!-- Bootstrap Styles -->
<link rel="stylesheet" type="text/css" href="css/common/jquery-ui.min.css" />
<link rel="stylesheet" type="text/css" href="css/common/jquery.datetimepicker.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/common/ui.jqgrid.css" />

<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap-responsive.css">
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap-theme.min.css">

<link rel="stylesheet" href="css/custom/specific/nav.css">
<link href="css/custom/specific/correlation.css" rel="stylesheet" />

<!-- Custom scripts for this Page -->
<script src="scripts/custom/correlation.js"></script>

<script src="scripts/custom/tab_menu.js"></script>

<% String session_val = (String)session.getAttribute("modules"); %>

<script type="text/javascript">
	session_obj= '<%=session_val%>';
</script>
</head>
<body style="font-size:11px!important;">

	<div class="container-fluid">
	  <%@ include file="../common/header.jsp"%>

	 <nav class="navbar navbar-inverse" style="margin-top: 0px">
	      <ul class="nav navbar-nav navbar-xs">
	        <li id="Dashboard"><a href="dashboard.html">Dashboard<span class="sr-only">(current)</span></a></li>
	        <li id="AlertHistory"><a href="problemHistory.html">Alert History</a></li>
	        <!-- <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Manage<span class="caret"></span></a>
	          <ul class="dropdown-menu">
	            <li><a href="#">Manage Device</a></li>
	            <li><a href="#">Manage Threshold</a></li>
	          </ul>
	        </li> -->
	        <li id="LogAnalysis"><a href="query.html">Log Analysis</a></li>
	        <li class="active" id="LogCorrelation"><a href="correlation.html">Log Correlation</a></li>
	        <li id="VariableAnalysis"><a href="variableAnalysis.html"><spring:message code="label.admin.menu5"></spring:message></a></li>
	        <li class="dropdown" id="Reports">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reports <span class="caret"></span></a>
	          <ul class="dropdown-menu">
	            	<li id="CreateReport"><a href="charts.html">Create Report</a></li>
		            <li id="CreateDashboard"><a href="createuserdashboard.html">Create Dashboard</a></li>
		            <li id="ViewDashboard"><a href="list_dashboard.html">View Dashboard</a></li>
	          </ul>
	        </li>
	      </ul>
	  </nav>
<div class="container-fluid innerContainer">
		<div class="col-md-3" style="background-color:#e0e0e0; height:515px;  overflow-y:auto; overflow-x:hidden;">
		
		
			<!-- --------------------------------------------------------------------------- -->
		 <div class="container-fluid innerContainer" style="margin-top: 20px; background-color:#e0e0e0; height:23%; overflow-y:auto; overflow-x:hidden;">
		
					 
					 <form class="form-horizontal">
					 <div class="form-group">
					<label class="col-sm-4">From:</label>
					<div class="col-sm-8">
					<div class="inner-addon left-addon">
						<i class="glyphicon glyphicon-calendar"></i> 
						<input class="form-control text-style" type="text" id="datePickerFrom">
					</div>
					</div>
				</div>
					
				<div class="form-group">
					<label class="col-sm-4 text-style">To:</label>
					<div class="col-sm-8">
						<div class="inner-addon left-addon">
							<i class="glyphicon glyphicon-calendar"></i> 
							<input class="form-control text-style" type="text" id="datePickerTo">
						</div>
					</div>
				</div>
			</form>
					 <div class="col-md-12" style="padding-right: 20px;">
						<a id="advanced_feature"> Click here For Advanced Features</a>
			</div>
			
							<div id="advanced1" >
							<form class="form-horizontal">
							
							<div class="form-group">
						<label class="col-sm-4 text-style">Query Field:</label>
						<div class="col-sm-8">
							<select class="form-control text-style" id="query_field"></select>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-4 text-style">Query Text:</label>
						<div class="col-sm-8">
							<input class="form-control text-style" id="query_text" type="text" value=''>		
						</div>
					</div>
					
					 
								
							
						</form>
					</div>
	<!-- --------------------------------------------------------------------------- -->
		 </div>
		<div  class="panel" style="background-color:#e0e0e0;"> </div>
			<div class="row">	
			<div class="col-md-10">	
					<div class="panel-group" id="accordion">
    					<div class="panel panel-default" id="accordian_parent">
    						  
					  </div>
				  </div>
		   </div>
		   </div>
		   
		   <div class="row">
		   <div class="col-sm-3" > 
					<div class="form-group">
							<input type="button" class="btn btn-primary btn-sm" id="execute" name="Execute" value="Execute" style="margin-left:220px;"/>
					</div>
		   </div>	 
		   </div>
		   
		</div>
     <div class="col-md-9">
			
			<div class="row">
			<div class="col-md-9">
			<div id="main_container">
			
			</div>
			</div>
			</div>
	</div>
	<div id="alertDialog">
	</div>	
	<div id="alertDate" title="Alert">
			Please put value inside "" if it contains special characters & space
	</div> 
	<div id="alertLength" title="Alert">
			Value should not exceed more than 20 characters
	</div> 
    <div id="alertSpace" title="Alert"> 
            Please use * for wildcard search eg.space, . , ( , ) , :
   </div>
    <div id="alertLog" title="Alert"> 
           Sorry!! Only two logs can be chosen at a time!
   </div>
</div>
	<%-- <%@ include file="../common/footer.jsp"%> --%>
	</div>
</body>
</html>