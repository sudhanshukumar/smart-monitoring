/**
 * 
 */

$(document)
		.ready(
				function() {
					document.getElementById('userManagementHeading').innerHTML="Add New User"

					$("#alert").hide();
					$("#confirmDelete").hide();
					$("#update_user_submit").hide();
					$("#preview_user_buttons").hide();
					$("#preview_ok").click(function(){
						window.location.href="manageUsers.html";
					});
					var action = getQueryVariable("action");
					var userId = getQueryVariable("userId");
				
					if (action == "preview") {
						document.getElementById('userManagementHeading').innerHTML="Preview User";
						$("#add_user_buttons").hide();
						$("#preview_user_buttons").show();
								$.ajax({
									type : "POST",
									url : "previewUser.html",
									data : "userId=" + userId,
									success : function(response) {
											$("#name").val(response.name);
											$("#username").val(response.username);
											$("#psno").val(response.emailId);
											if(response.role_id ==1)
											{
											$('#userRole').val("ROLE_ADMIN");
											}
											else if(response.role_id ==2)
											{
												$('#userRole').val("ROLE_USER");
											}
										
											$('#name').attr('readonly',true);
											$('#username').attr('readonly',true);
											$('#psno').attr('readonly',true);
											$("#password").hide();
											$("#passwordLabel").hide();
											$('#userRole').attr('disabled', true);
									},
									cache : false
								});
						}
					
					if (action == "edit") {
						$("#preview_user_buttons").hide();
						document.getElementById('userManagementHeading').innerHTML="Edit User"
					
						$.ajax({
							type : "POST",
							url : "previewUser.html",
							data : "userId=" + userId,
							success : function(response) {
									$("#name").val(response.name);
									$("#username").val(response.username);
									$("#psno").val(response.emailId);
									$("#passwordLabel").show();
									$("#password").val(response.password);
									
									if(response.role_id ==1)
									{
									$('#userRole').val("ROLE_ADMIN");
									}
									else if(response.role_id ==2)
									{
										$('#userRole').val("ROLE_USER");
									}
									$('#userRole').attr('disabled', false);
									$("#add_user_submit").hide();
									
									$("#update_user_submit").show();
									
									$("#update_user_submit").click(function() {
										name = $('#name').val();
										emailid = $('#psno').val();
										username = $('#username').val();
										password = $('#password').val();
										rolename = $('#userRole').val();
										
										 var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
										 if( ($('#name').val()!="") &&
													($('#username').val()!="") &&
														($('#password').val()!="")  &&
															($('#userRole').val()!="")  && 
															($('#psno').val()!="")) 
												{
										$.ajax({
											type : "POST",
											url : "updateUser.html",
											data : 'name=' + name + '&emailId=' + emailid+ '&username=' + username+ '&password=' + password+ '&rolename=' + rolename+ '&userId=' + userId,

											success : function(response) {
												alert(response);
												window.location.href="manageUsers.html";
											},
											error : function(jqXHR, textStatus, errorThrown) {
												alert("error occured");
											},
											cache : false
										});
									}
										 else{
											 if( ($('#name').val()=="") ||
														($('#username').val()=="") ||
															($('#password').val()=="") ||
																($('#userRole').val()=="") ||
																	($('#psno').val()==""))
																{
																alert("Please enter all mandatory fields");
																}
												else if(($('#psno').val()!="") && !(re.test(emailid)))
														{
													alert("Please enter the email Id in correct format");
														}
										 }
									});
							},
							cache : false
						});
				}
					
					$("#add_user_submit").click(function() {
						
						name = $('#name').val();
						emailid = $('#psno').val();
						username = $('#username').val();
						password = $('#password').val();
						rolename = $('#userRole').val();
					
						 var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
						
						if( ($('#name').val()!="") &&
								($('#username').val()!="") &&
									($('#password').val()!="")  &&
										($('#userRole').val()!="")  && 
										(($('#psno').val()!="") && (re.test(emailid))) ) 
							{
							
							$.ajax({
								type : "POST",
								url : "addUser.html",
								data : 'name=' + name + '&emailId=' + emailid+ '&username=' + username+ '&password=' + password+ '&rolename=' + rolename,
								success : function(response) {
									alert("User Added Successfully")
									window.location.href="manageUsers.html";
								},
								error : function(jqXHR, textStatus, errorThrown) {
								},
								cache : false
							});
							
							}	
						else
							{
							if( ($('#name').val()=="") ||
									($('#username').val()=="") ||
										($('#password').val()=="") ||
											($('#userRole').val()=="") ||
												($('#psno').val()==""))
											{
											alert("Please enter all mandatory fields");
											}
							else if(($('#psno').val()!="") && !(re.test(emailid)))
									{
								alert("Please enter the email Id in correct format");
									}
							}
					});
					
					$("#add_user_cancel").click(function() {
						window.location.href="manageUsers.html";
					});
				});
				
function getQueryVariable(variable)
{
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}
