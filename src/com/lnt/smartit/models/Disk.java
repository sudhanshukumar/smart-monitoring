/**
 * 
 */
package com.lnt.smartit.models;

import java.util.ArrayList;
import java.util.List;






import com.lnt.smartit.models.FileSystem;

/**
 * @author 299273
 *
 */
public class Disk {
	
	private List<FileSystem> fileSystem = new ArrayList<FileSystem>();
	private Double totalSize;
	private Double totalUsed;
	private Double totalAvail;
	private Double totalUsePercent;
	public List<FileSystem> getFileSystem() {
		return fileSystem;
	}

	public void setFileSystem(List<FileSystem> fileSystem) {
		this.fileSystem = fileSystem;
	}
 
	public void addFileSystem(FileSystem fileSystem) {
		this.fileSystem.add(fileSystem);
	}
	
	public Double getTotalSize() {
		return totalSize;
	}

	public void setTotalSize(Double totalSize) {
		this.totalSize = totalSize;
	}

	public Double getTotalUsed() {
		return totalUsed;
	}

	public void setTotalUsed(Double totalUsed) {
		this.totalUsed = totalUsed;
	}

	public Double getTotalAvail() {
		return totalAvail;
	}

	public void setTotalAvail(Double totalAvail) {
		this.totalAvail = totalAvail;
	}

	public Double getTotalUsePercent() {
		return totalUsePercent;
	}

	public void setTotalUsePercent(Double totalUsePercent) {
		this.totalUsePercent = totalUsePercent;
	}

	@Override
	public String toString() {
		return "Disk [fileSystem=" + fileSystem + ", totalSize=" + totalSize
				+ ", totalUsed=" + totalUsed + ", totalAvail=" + totalAvail
				+ ", totalUsePercent=" + totalUsePercent + "]";
	}
}
