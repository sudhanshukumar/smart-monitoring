package com.lnt.smartit.mysql.model;

import java.io.Serializable;
import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "manage_report")
public class ManageReport implements Serializable {

	private static final long serialVersionUID = 1L;
	@GeneratedValue
	@Id
	@Column(name="id")
	private String id;
	
	@Column(name="col_id")
	private String colId;
	
	@Column(name="field")
	private String field;
	
	@Column(name="filter_type")
	private String filterType;

	public String getColId() {
		return colId;
	}

	public void setColId(String colId) {
		this.colId = colId;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getFilterType() {
		return filterType;
	}

	public void setFilterType(String filterType) {
		this.filterType = filterType;
	}

	@Override
	public String toString() {
		return "ManageReport [colId=" + colId + ", field="
				+ field + ", filterType=" + filterType + "]";
	}
	

	
	
}
