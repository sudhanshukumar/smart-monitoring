package com.lnt.smartit.email.notify;

import java.util.Map;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.lnt.smartit.models.AlertMessage;

/**
 * @author 10614080
 *
 */
public class NotifyUsers {
	
	public void notifyUsers(AlertMessage alert,Map<String,String> map)
			throws Exception {
		InternetAddress[] recipientAddressTo = null ;
		InternetAddress[] recipientAddressCC= null; 
		
		String emailTO = map.get("toAddr");
		String[] recipientListTo = emailTO.split(",|\\;");
		recipientAddressTo = new InternetAddress[recipientListTo.length];
		int counterTo = 0;
		for (String emailTOList : recipientListTo) {
			recipientAddressTo[counterTo] = new InternetAddress(
					emailTOList.trim());
			counterTo++;
		}

		String emailCC = map.get("ccAddr");
		String[] recipientListCC = emailCC.split(",|\\;");
		recipientAddressCC = new InternetAddress[recipientListCC.length];
		int counterCC = 0;
		for (String emailCCList : recipientListCC) {
			recipientAddressCC[counterCC] = new InternetAddress(
					emailCCList.trim());
			counterCC++;
		}

		String from = "Nikita.Dalela@lntinfotech.com";

		// Get system properties
		Properties properties = System.getProperties();

		// Setup mail server
		// properties.setProperty("mail.smtp.host", "smtp.lntinfotech.com");
		// properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", "172.21.102.151");
		properties.put("mail.smtp.user", "NMUMARL\10614385"); // User name
		properties.put("mail.smtp.password", "November123"); // password
		properties.put("mail.smtp.port", "25");

		Session session = Session.getDefaultInstance(properties,
				new Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication("10614385",
								"November123");
					}
				});
		try {

			// Create a default MimeMessage object.
			MimeMessage message1 = new MimeMessage(session);

			// Set From: header field of the header.
			message1.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			message1.setRecipients(Message.RecipientType.TO, recipientAddressTo);
			
			message1.setRecipients(Message.RecipientType.CC, recipientAddressCC);

			// Set Subject: header field
			message1.setSubject(alert.getAlertMessageType() + "  :" + "  "
					+ alert.getAlertMessageHostName() + "  :" + "  "
					+ alert.getDescription());

			// Now set the actual message
			message1.setText("Hi User," + "\n\n" + "Machine:" + "  "
					+ alert.getAlertMessageHostName() + "  "
					+ "has generated a" + "  " + alert.getAlertMessageType()
					+ "  " + "alert" + "\n\n"
					+ "Please find the details regarding the alert below testing:"
					+ "\n\n" + "Host Name:" + "  "
					+ alert.getAlertMessageHostName() + "\n"
					+ "Parameter Name:" + "  "
					+ alert.getAlertMessageparameterName() + "\n"
					+ "Issue Detected On:" + "  "
					+ alert.getAlertMessageDatetime() + "\n" + "Description:"
					+ "  " + alert.getDescription() + "\n" + "\n" + "\n" + "\n"
					+ "Reported By," + "\n" + "Smart Monitoring");

			// Send message
			Transport.send(message1);
			System.out.println("Sent message successfully....");
		} catch (MessagingException mex) {
			mex.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
