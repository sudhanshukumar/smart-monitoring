package com.lnt.smartit.mysql.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@SuppressWarnings("serial")
public class ThresholdLogSize  implements java.io.Serializable {

	@GeneratedValue(strategy = GenerationType.AUTO)
	private String id;
	private String hostName;
	private String logFileName;
	private Double warningThresholdSize;
	private Double criticalThresholdSize;
		
	public ThresholdLogSize() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ThresholdLogSize(String id, String hostName, String logFileName,
			Double warningThresholdSize, Double criticalThresholdSize) {
		super();
		this.id = id;
		this.hostName = hostName;
		this.logFileName = logFileName;
		this.warningThresholdSize = warningThresholdSize;
		this.criticalThresholdSize = criticalThresholdSize;
	}


	public Double getWarningThresholdSize() {
		return warningThresholdSize;
	}

	public void setWarningThresholdSize(Double warningThresholdSize) {
		this.warningThresholdSize = warningThresholdSize;
	}

	public Double getCriticalThresholdSize() {
		return criticalThresholdSize;
	}

	public void setCriticalThresholdSize(Double criticalThresholdSize) {
		this.criticalThresholdSize = criticalThresholdSize;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getLogFileName() {
		return logFileName;
	}

	public void setLogFileName(String logFileName) {
		this.logFileName = logFileName;
	}

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((criticalThresholdSize == null) ? 0 : criticalThresholdSize
						.hashCode());
		result = prime * result
				+ ((hostName == null) ? 0 : hostName.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((logFileName == null) ? 0 : logFileName.hashCode());
		result = prime
				* result
				+ ((warningThresholdSize == null) ? 0 : warningThresholdSize
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ThresholdLogSize other = (ThresholdLogSize) obj;
		if (criticalThresholdSize == null) {
			if (other.criticalThresholdSize != null)
				return false;
		} else if (!criticalThresholdSize.equals(other.criticalThresholdSize))
			return false;
		if (hostName == null) {
			if (other.hostName != null)
				return false;
		} else if (!hostName.equals(other.hostName))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (logFileName == null) {
			if (other.logFileName != null)
				return false;
		} else if (!logFileName.equals(other.logFileName))
			return false;
		if (warningThresholdSize == null) {
			if (other.warningThresholdSize != null)
				return false;
		} else if (!warningThresholdSize.equals(other.warningThresholdSize))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ThresholdLogSize [id=" + id + ", hostName=" + hostName
				+ ", logFileName=" + logFileName + ", warningThresholdSize="
				+ warningThresholdSize + ", criticalThresholdSize="
				+ criticalThresholdSize + "]";
	}

}
