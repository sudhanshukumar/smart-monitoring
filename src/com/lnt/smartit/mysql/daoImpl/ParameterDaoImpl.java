package com.lnt.smartit.mysql.daoImpl;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lnt.smartit.helper.HbmHelper;
import com.lnt.smartit.mysql.dao.ParameterDao;
import com.lnt.smartit.mysql.model.ParameterMaster;

@Repository
@Transactional
public class ParameterDaoImpl implements ParameterDao {
	@Autowired
	private HbmHelper hbmHelper;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<ParameterMaster> getParameters() {
		@SuppressWarnings("unchecked")
		List<ParameterMaster> params = (List<ParameterMaster>) hbmHelper.createQuery("from ParameterMaster", null);
		return params;
	}
	
	public ParameterMaster getParameterMasterById(String id) {
		return (ParameterMaster) hbmHelper.findById(ParameterMaster.class, id);
	}
	
	public String getParameterMasterByParamName(String name) {
		Session session = sessionFactory.getCurrentSession();
		Query query = null;
		String str =null;
		try{
			query = session.createSQLQuery("Select * from parameter_master t where t.parameter_name ='"+name +"';" );
			List<Object> serviceFiles = query.list();
			for (int i = 0; i < serviceFiles.size(); i++) {
			       Object[] row = (Object[]) serviceFiles.get(i);	
			       str=row[0].toString();
			}
		}
		catch(Exception e){
			e.printStackTrace();
			
		}
		finally{
			session.flush();
		}
		return str;
	}
}
