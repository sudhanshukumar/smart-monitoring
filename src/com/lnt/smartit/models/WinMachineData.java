/**
 * 
 */
package com.lnt.smartit.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/**
 * @author 299273
 *
 */
public class WinMachineData implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String hostName;
	private String IP;
	private String Date;
	//Time
	private long dataReceivedTime;
	//Total Physical Memory:     16,383 MB,Available Physical Memory: 5,320 MB,Virtual Memory: Max Size:  32,765 MB,
	//Virtual Memory: Available: 20,524 MB,Virtual Memory: In Use:    12,348 MB
	//RAM
	private double memoryUsage;
	private int totalPhysicalMemory;
	private int availablePhysicalMemory;
	private int availablePhysicalInPercent;
	private int virtualMemoryMaxSize;
	private int availableVirtualMEmory;
	private int usedVirtualMemory;
	private int usedVirtualMemoryInPercent;
	//CPU
	private Double cpuUsageInPercent;
	//Disk
	private Double totalDiskSize;
	private Double totalDiskUsed;
	private Double totalDiskAvail;
	private Double totalUsePercent;
	private Double totalAvailablepercent ;
	private List<FileSystem> fileSystem = new ArrayList<FileSystem>();
	
	public Double getTotalAvailablepercent() {
		return totalAvailablepercent;
	}
	public void setTotalAvailablepercent(Double totalAvailablepercent) {
		this.totalAvailablepercent = totalAvailablepercent;
	}
	public double getMemoryUsage() {
		return memoryUsage;
	}
	public void setMemoryUsage(double memoryUsage) {
		this.memoryUsage = memoryUsage;
	}
	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	public int getTotalPhysicalMemory() {
		return totalPhysicalMemory;
	}
	public void setTotalPhysicalMemory(int totalPhysicalMemory) {
		this.totalPhysicalMemory = totalPhysicalMemory;
	}
	public int getAvailablePhysicalMemory() {
		return availablePhysicalMemory;
	}
	public void setAvailablePhysicalMemory(int availablePhysicalMemory) {
		this.availablePhysicalMemory = availablePhysicalMemory;
	}
	public int getAvailablePhysicalInPercent() {
		return availablePhysicalInPercent;
	}
	public void setAvailablePhysicalInPercent(int availablePhysicalInPercent) {
		this.availablePhysicalInPercent = availablePhysicalInPercent;
	}
	public int getVirtualMemoryMaxSize() {
		return virtualMemoryMaxSize;
	}
	public void setVirtualMemoryMaxSize(int virtualMemoryMaxSize) {
		this.virtualMemoryMaxSize = virtualMemoryMaxSize;
	}
	public int getAvailableVirtualMEmory() {
		return availableVirtualMEmory;
	}
	public void setAvailableVirtualMEmory(int availableVirtualMEmory) {
		this.availableVirtualMEmory = availableVirtualMEmory;
	}
	public int getUsedVirtualMemory() {
		return usedVirtualMemory;
	}
	public void setUsedVirtualMemory(int usedVirtualMemory) {
		this.usedVirtualMemory = usedVirtualMemory;
	}
	public int getUsedVirtualMemoryInPercent() {
		return usedVirtualMemoryInPercent;
	}
	public void setUsedVirtualMemoryInPercent(int usedVirtualMemoryInPercent) {
		this.usedVirtualMemoryInPercent = usedVirtualMemoryInPercent;
	}
	public Double getCpuUsageInPercent() {
		return cpuUsageInPercent;
	}
	public void setCpuUsageInPercent(Double cpuUsageInPercent) {
		this.cpuUsageInPercent = cpuUsageInPercent;
	}
	public List<FileSystem> getFileSystem() {
		return fileSystem;
	}
	public void setFileSystem(List<FileSystem> fileSystem) {
		this.fileSystem = fileSystem;
	}
	public Double getTotalDiskSize() {
		return totalDiskSize;
	}
	public void setTotalDiskSize(Double totalDiskSize) {
		this.totalDiskSize = totalDiskSize;
	}
	public Double getTotalDiskUsed() {
		return totalDiskUsed;
	}
	public void setTotalDiskUsed(Double totalDiskUsed) {
		this.totalDiskUsed = totalDiskUsed;
	}
	public Double getTotalDiskAvail() {
		return totalDiskAvail;
	}
	public void setTotalDiskAvail(Double totalDiskAvail) {
		this.totalDiskAvail = totalDiskAvail;
	}
	public Double getTotalUsePercent() {
		return totalUsePercent;
	}
	public void setTotalUsePercent(Double totalUsePercent) {
		this.totalUsePercent = totalUsePercent;
	}
	public long getDataReceivedTime() {
	return dataReceivedTime;
	}
	public void setDataReceivedTime(long dataReceivedTime) {
		this.dataReceivedTime = dataReceivedTime;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	@Override
	public String toString() {
		return "WinMachineData [hostName=" + hostName + ", IP=" + IP
				+ ", Date=" + Date + ", dataReceivedTime="
				+ dataReceivedTime + ", memoryUsage=" + memoryUsage
				+ ", totalPhysicalMemory=" + totalPhysicalMemory
				+ ", availablePhysicalMemory=" + availablePhysicalMemory
				+ ", availablePhysicalInPercent="
				+ availablePhysicalInPercent + ", virtualMemoryMaxSize="
				+ virtualMemoryMaxSize + ", availableVirtualMEmory="
				+ availableVirtualMEmory + ", usedVirtualMemory="
				+ usedVirtualMemory + ", usedVirtualMemoryInPercent="
				+ usedVirtualMemoryInPercent + ", cpuUsageInPercent="
				+ cpuUsageInPercent + ", fileSystem=" + fileSystem
				+ ", totalDiskSize=" + totalDiskSize + ", totalDiskUsed="
				+ totalDiskUsed + ", totalDiskAvail=" + totalDiskAvail
				+ ", totalUsePercent=" + totalUsePercent
				+ ", totalAvailablepercent=" + totalAvailablepercent + "]";
	}
}
