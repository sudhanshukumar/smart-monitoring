package com.lnt.smartit.serviceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.stereotype.Service;

import com.lnt.smartit.models.GridHeaderModel;
import com.lnt.smartit.services.DataEnrichmentService;

@Service
public class DataEnrichmentServiceImpl implements DataEnrichmentService {

	/*@Override
	public Object[][] enrichDataForExcelExport(String dataStrem)
	{
		Object[][] data = null;
		String [] rowset = dataStrem.split("\n");
		int rowCount = rowset.length;
		int columnCount = rowset[0].split("\t").length;
		
		data = new Object[rowCount][columnCount];
		
		for (int i = 0; i < rowset.length; i++) 
		{
			String row = rowset[i];
			String [] columset = row.trim().split("\t");
			for (int j = 0; j < columset.length; j++) 
			{
				String column = columset[j];
				data[i][j] = column;
			}
		}
		return data;
	}*/
	@Override
	public Object[][] enrichDataForExport(List<Map<String, Object>> gridData, List<GridHeaderModel> headers)
	{
		Object[][] data = null; 
		/*Object [] headerArray
		int i=1;
		for(Map<String, Object> rowData : gridData)
		{
			int j=0;
			for(GridHeaderModel column:headers)
			{
				if(!column.isHidden())
				{
					data[i][j] = rowData.get(column.getName());
				}
			}
		}
		*/
		return data;
	}
	@Override
	public String getExcelFilePath(String fileName)
	{
		String filePath = null;
		String tempDir = System.getenv("temp");
		filePath = tempDir+"\\"+fileName;
		return filePath;
	}
	@Override
	public  List<Map<String, Object>> convertJsonStringToMap(String jsonString){
		ObjectMapper mapper = new ObjectMapper();
		List<Map<String, Object>> data = new ArrayList<Map<String,Object>>();
		try {
			org.json.JSONArray responseArray = new org.json.JSONArray(jsonString);
			for(int i=0;i<responseArray.length();i++)
			{
				Map<String, Object> rowData = new HashMap<String, Object>();
				org.json.JSONObject jsonObj = responseArray.getJSONObject(i);
				rowData = mapper.readValue(jsonObj.toString(), new TypeReference<Map<String, String>>(){});
				data.add(rowData);
			}
			
			return data;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public List<GridHeaderModel> convertJsonStringToObj(String jsonString){
		ObjectMapper mapper = new ObjectMapper();
		List<GridHeaderModel> columnInfo = new ArrayList<GridHeaderModel>();
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			org.json.JSONArray responseArray = new org.json.JSONArray(jsonString);
			for(int i=0;i<responseArray.length();i++)
			{
				org.json.JSONObject jsonObj = responseArray.getJSONObject(i);
				GridHeaderModel columnModel = mapper.readValue(jsonObj.toString(), GridHeaderModel.class);
				columnInfo.add(columnModel);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return columnInfo;
	}
	
	@Override
	public Integer getColumnCount(List<GridHeaderModel> headers)
	{
		Integer columnCount = 0;
		if (headers!=null) 
		{
			for (GridHeaderModel column : headers) {
				if (!column.isHidden()) {
					columnCount++;
				}
			}
		}
		return columnCount;
	}
	public static void main(String[] args) {
		DataEnrichmentService service = new DataEnrichmentServiceImpl();
		System.out.println(service.getExcelFilePath("tempFileName.xls"));
	}
	
}
