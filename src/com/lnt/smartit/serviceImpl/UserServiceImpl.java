package com.lnt.smartit.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lnt.smartit.mysql.dao.UserDao;
import com.lnt.smartit.mysql.model.Roles;
import com.lnt.smartit.mysql.model.Users;
import com.lnt.smartit.services.UserService;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserDao dao;

	public List<Users> getAllUsers() {
		return dao.getAllUsers();
	}

	public int addNewUser(Users user) {
		return dao.addNewUser(user);
	}

	public Users previewService(int userId) {
		return dao.previewService(userId);
	}

	public String deleteUser(int userId) {
		return dao.deleteUser(userId);
	}

	public void updateUser(Users user) {
		dao.updateUser(user);
	}

	public List<Roles> findRolesColl() {
		return dao.findRolesColl();
	}

}
