package com.lnt.smartit.mysql.model;


	import java.io.Serializable;

	import javax.persistence.Column;
	import javax.persistence.Entity;
	import javax.persistence.GeneratedValue;
	import javax.persistence.Id;
	import javax.persistence.JoinColumn;
	import javax.persistence.ManyToOne;
	import javax.persistence.Table;

	@Entity
	@Table(name="report")
	public class Report implements Serializable{
		private static final long serialVersionUID = 1L;
		@GeneratedValue
		@Id
		@Column(name="report_id")
		private String reportId;
		
		@ManyToOne
		@JoinColumn(name="app_id", referencedColumnName = "id")
		private Applications appId;
		
		@Column(name="report_name")
		private String reportName;
		
		@Column(name="report_type")
		private String reportType;
		
		@Column(name="collection")
		private String collection;
/*
		@Column(name="aggregator")
		private String aggregator;
		
		public String getAggregator() {
			return aggregator;
		}

		public void setAggregator(String aggregator) {
			this.aggregator = aggregator;
		}*/

		public String getReportId() {
			return reportId;
		}

		public void setReportId(String reportId) {
			this.reportId = reportId;
		}

		public Applications getAppId() {
			return appId;
		}

		public void setAppId(Applications appId) {
			this.appId = appId;
		}

		public String getReportName() {
			return reportName;
		}

		public void setReportName(String reportName) {
			this.reportName = reportName;
		}

		public String getReportType() {
			return reportType;
		}

		public void setReportType(String reportType) {
			this.reportType = reportType;
		}

		public String getCollection() {
			return collection;
		}

		public void setCollection(String collection) {
			this.collection = collection;
		}

		@Override
		public String toString() {
			return "Report [reportId=" + reportId + ", appId=" + appId
					+ ", reportName=" + reportName + ", reportType="
					+ reportType + ", collection=" + collection
					+ ", aggregator=" /*+ aggregator*/ + "]";
		}

		

	
}
