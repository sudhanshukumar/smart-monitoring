/**
 * 
 */
package com.lnt.smartit.rule.mysql.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author 10612068
 *
 */
public class RuleVO implements Serializable {
	
	private int id;
	private String appLogParamId;
	private String hostName;
	private String paramName;
	private String parameterType;
	private String parameterTypeCode;//
	
	private String expressionTypeLHS;
	private String expressionTypeLHSCode;//
	
	private Object thresholdValueLHS;
	private String logicalOperator;
	private String thresholdType;
	private String expressionTypeRHS;
	private String expressionTypeRHSCode;//
	private Object thresholdValueRHS;
	private long filterDuration;
	private String autoIncident;
	private long deduplicationWindow;
	
	//10614080 on 06/03/2016 start
	private String notification;
	private String notificationTO;
	private String notificationCC;
	//10614080 on 06/03/2016 end
	
	//10612175 on 06/04/2017 maintenance window start
	private String maintenanceWindow;	
	private Date fromMW;
	private Date toMW;	
	
	public String getMaintenanceWindow() {
		return maintenanceWindow;
	}
	public void setMaintenanceWindow(String maintenanceWindow) {
		this.maintenanceWindow = maintenanceWindow;
	}
	public Date getFromMW() {
		return fromMW;
	}
	public void setFromMW(Date fromMW) {
		this.fromMW = fromMW;
	}
	public Date getToMW() {
		return toMW;
	}
	public void setToMW(Date toMW) {
		this.toMW = toMW;
	}
	//10612175 on 06/04/2017 maintenance window end
	public long getDeduplicationWindow() {
		return deduplicationWindow;
	}
	public void setDeduplicationWindow(long deduplicationWindow) {
		this.deduplicationWindow = deduplicationWindow;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the appLogParamId
	 */
	public String getAppLogParamId() {
		return appLogParamId;
	}
	/**
	 * @param appLogParamId the appLogParamId to set
	 */
	public void setAppLogParamId(String appLogParamId) {
		this.appLogParamId = appLogParamId;
	}
	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	public String getParamName() {
		return paramName;
	}
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}
	
	public Object getThresholdValueLHS() {
		return thresholdValueLHS;
	}
	public void setThresholdValueLHS(Object thresholdValueLHS) {
		this.thresholdValueLHS = thresholdValueLHS;
	}
	
	public Object getThresholdValueRHS() {
		return thresholdValueRHS;
	}
	public void setThresholdValueRHS(Object thresholdValueRHS) {
		this.thresholdValueRHS = thresholdValueRHS;
	}
	public long getFilterDuration() {
		return filterDuration;
	}
	public void setFilterDuration(long filterDuration) {
		this.filterDuration = filterDuration;
	}
	public String getParameterTypeCode() {
		return parameterTypeCode;
	}
	public void setParameterTypeCode(String parameterTypeCode) {
		this.parameterTypeCode = parameterTypeCode;
	}
	public String getExpressionTypeLHSCode() {
		return expressionTypeLHSCode;
	}
	public void setExpressionTypeLHSCode(String expressionTypeLHSCode) {
		this.expressionTypeLHSCode = expressionTypeLHSCode;
	}
	public String getExpressionTypeRHSCode() {
		return expressionTypeRHSCode;
	}
	public void setExpressionTypeRHSCode(String expressionTypeRHSCode) {
		this.expressionTypeRHSCode = expressionTypeRHSCode;
	}
	public String getAutoIncident() {
		return autoIncident;
	}
	public void setAutoIncident(String autoIncident) {
		this.autoIncident = autoIncident;
	}
	public String getParameterType() {
		return parameterType;
	}
	public void setParameterType(String parameterType) {
		this.parameterType = parameterType;
	}
	public String getExpressionTypeLHS() {
		return expressionTypeLHS;
	}
	public void setExpressionTypeLHS(String expressionTypeLHS) {
		this.expressionTypeLHS = expressionTypeLHS;
	}
	public String getLogicalOperator() {
		return logicalOperator;
	}
	public void setLogicalOperator(String logicalOperator) {
		this.logicalOperator = logicalOperator;
	}
	public String getThresholdType() {
		return thresholdType;
	}
	public void setThresholdType(String thresholdType) {
		this.thresholdType = thresholdType;
	}
	public String getExpressionTypeRHS() {
		return expressionTypeRHS;
	}
	public void setExpressionTypeRHS(String expressionTypeRHS) {
		this.expressionTypeRHS = expressionTypeRHS;
	}
	
	//10614080 on 06/03/2016 start
	public String getNotification() {
		return notification;
	}
	public void setNotification(String notification) {
		this.notification = notification;
	}
	public String getNotificationTO() {
		return notificationTO;
	}
	public void setNotificationTO(String notificationTO) {
		this.notificationTO = notificationTO;
	}
	public String getNotificationCC() {
		return notificationCC;
	}
	public void setNotificationCC(String notificationCC) {
		this.notificationCC = notificationCC;
	}
	//10614080 on 06/03/2016 end
	
}
