/**
 * 
 */
package com.lnt.smartit.rule.mysql.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author 10612068
 *
 */
@Entity
@Table(name="expression")
public class Expression implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name="id")
	private int exprId;
	
	@Column(name="exp_val")
	private String exprValue;
	
	@ManyToOne
	@JoinColumn(name="expression_data_type_code", referencedColumnName="code")
	private ExpressionDataType exprDatatype;
	
	@ManyToOne
	@JoinColumn(name="expression_type_code", referencedColumnName="code")
	private ExpressionType exprType;
	
	
	public ExpressionDataType getExprDatatype() {
		return exprDatatype;
	}
	public void setExprDatatype(ExpressionDataType exprDatatype) {
		this.exprDatatype = exprDatatype;
	}
	public ExpressionType getExprType() {
		return exprType;
	}
	public void setExprType(ExpressionType exprType) {
		this.exprType = exprType;
	}
	public int getId() {
		return exprId;
	}
	public void setId(int id) {
		this.exprId = id;
	}
	public String getExprValue() {
		return exprValue;
	}
	public void setExprValue(String exprValue) {
		this.exprValue = exprValue;
	}
	
	
}
