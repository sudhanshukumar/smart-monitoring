var report_count;
$(document).ready(function() {
	
	getapplications();
	getcharts();
	$("#save").prop("disabled", true);
	$("#dashboard_name").prop("disabled", true);
	$( "#alertDialog" ).hide();
});

function showAlert(message){
    $( "#alertDialog" ).show();
    $("#alertDialog").text(message);
    $( "#alertDialog" ).dialog({
           buttons:{
                        "OK":function() {
                               $( this ).dialog( "close" );
                              }
                        } 
                                             });

}


function getapplications()
{
	$.ajax({
		type : "GET",
		url : "getApplications.html",
		success : function(response) {
			$('#application').empty();
			 $("#application").append('<option value="" selected disabled>Select Application</option>');
			for ( var i = 0; i < response.length; i++) {
				 $("#application").append('<option value="'+response[i].appId+'">'+response[i].appName+'</option>');
			}
		}
	});
}

function getcollections() {

	$.ajax({
		type : "GET",
		url : "getCollections.html",

		success : function(response) {
			$('#collections1').empty();
			 $("#collections1").append('<option value="" selected disabled>Select Log Type</option>');
			for ( var i = 0; i < response.length; i++) {
				$("#collections1").append(
						$('<option>').text(response[i].col_name)).append(
						$('</option>'));
			}

			$("#col_title").html(response[0].col_name);
			$('#collections1').change(
					function() {
					});
		}
	});
}

function getcharts()
{
	$.ajax({
		type : "GET",
		url : "getChartnames.html",

		success : function(response) {
			$('#charts').empty();
			 //$("#charts").append('<option value="" selected disabled>Select Report</option>');
			for ( var i = 0; i < response.length; i++) {
				
				$("#charts").append(
						$('<option>').text(response[i].reportName)).append(
						$('</option>'));
			}
			$("#charts").multiselect({
				search: true,
				enableFiltering: true,
				selectAll     : false,
	            filterPlaceholder: 'Search for something...'
			});
			$("#charts").removeClass();
			$("#charts").addClass("form-control");
		}
	});
}



function RemoveChart(removecount){
	var chartName = $("#draggable_"+removecount ).attr("name");
	$("#charts").multiselect("widget").find("input[value='" + chartName + "']").click();
	$("#draggable_"+removecount ).remove();
}

function drawchart(selected_field,div)
{
	$.ajax({
		type : "POST",
		url : "drawChart.html",
		data :"chart_name="+encodeURIComponent(selected_field),
		success : function(response) {
			
			if(response[0].chart_type=="pie")
			{
			  $("#dashboard_pannel").append($( "<div id='draggable_"+div+"' class='ui-widget-content'  style='width:400px;height: 402px;' ><button type='button' class='btn btn-info'  style='float:right;' id='removeclick_"+div+"' onclick='RemoveChart("+div+");'> <span class='glyphicon glyphicon-remove'></span></button> <div id='container_"+div+"' style=' margin-top: 50px;'></div></div>" ));
			  $("#draggable_"+div).draggable({ 
	                containment: "#dashboard_pannel", 
	                scroll: false
	         })
			  drawPie(response,selected_field,div);
			}else
			if(response[0].chart_type=="column")
			{
				$("#dashboard_pannel").append($( "<div id='draggable_"+div+"' class='ui-widget-content'  style='height: 422px;'><button type='button' class='btn btn-info'  style='float:right;' id='removeclick_"+div+"' onclick='RemoveChart("+div+");'> <span class='glyphicon glyphicon-remove'></span></button><div id='container_"+div+"' style=' margin-top: 50px;'></div></div>" ));
				  $("#draggable_"+div).draggable({ 
		                containment: "#dashboard_pannel", 
		                scroll: false
				  })
				drawColumn(response,selected_field,div);
			}else
			if(response[0].chart_type=="time")
			{
				$("#dashboard_pannel").append($( "<div id='draggable_"+div+"' class='ui-widget-content' style='height: 402px;'><button type='button' class='btn btn-info'  style='float:right;' id='removeclick_"+div+"' onclick='RemoveChart("+div+");'> <span class='glyphicon glyphicon-remove'></span></button><div id='container_"+div+"' style='margin-top: 50px;'></div></div>" ));
				  $("#draggable_"+div).draggable({ 
		                containment: "#dashboard_pannel", 
		                scroll: false
		         })
				drawTime(response,selected_field,div);
			}else
			if(response[0].chart_type=="grid")
			{
				 $("#dashboard_pannel").append($( "<div id='draggable_"+div+"' class='ui-widget-content'   style='width: 700px;height: 302px;'><button type='button' class='btn btn-info'  style='float:right;' id='removeclick_"+div+"' onclick='RemoveChart("+div+")'> <span class='glyphicon glyphicon-remove'></span></button><div id='container_"+div+"'  style='margin-top: 50px;'><table id='table_"+div+"'  style='margin-left: 0px;'></table></div></div>" ));
				  $("#draggable_"+div).draggable({ 
		                containment: "#dashboard_pannel", 
		                scroll: false
				  })
				 chartgrid(response[0].col_name,response[0].gridcols,div);
			}else
				if(response[0].chart_type=="x-y")
				{
					$("#dashboard_pannel").append($( "<div id='draggable_"+div+"' class='ui-widget-content' style='height: 402px;'><button type='button' class='btn btn-info'  style='float:right;' id='removeclick_"+div+"' onclick='RemoveChart("+div+")'> <span class='glyphicon glyphicon-remove'></span></button><div id='container_"+div+"' style=' margin-top: 50px;'></div></div>" ));
					  $("#draggable_"+div).draggable({ 
			                containment: "#dashboard_pannel", 
			                scroll: false
			         })
					getcount(response,selected_field,div);
				}
		
			last=$('#dashboard_pannel').children().last().attr('id');
			 $('#'+last).attr('name',selected_field);
		}
	});
}

function drawcharts(selected_fields)
{
	var count=$('#dashboard_pannel').children().length;
	var div = count+1;
	/*if(count==1){
		div=0;
	}else{
		div=count-1;
	}*/
	//div++;
	$('#draggable_'+div).attr('name',selected_fields[0]);
	
	$.ajax({
		type : "POST",
		url : "drawChart.html",
		data :"chart_name="+encodeURIComponent(selected_fields[0]),
		success : function(response) {
			
			if(response[0].chart_type=="pie")
			{
			  $("#dashboard_pannel").append($( "<div id='draggable_"+div+"' class='ui-widget-content'  style='width:400px;height: 402px;' ><button type='button' class='btn btn-primary'  style='float:right;' id='removeclick_"+div+"' onclick='RemoveChart("+div+");'> <span class='glyphicon glyphicon-remove'></span></button> <div id='container_"+div+"' style=' margin-top: 50px;'></div></div>" ));
			  $("#draggable_"+div).draggable({ 
	                containment: "#dashboard_pannel", 
	                scroll: false
	         })
			  drawPie(response,selected_fields[0],div);
			}else
			if(response[0].chart_type=="column")
			{
				$("#dashboard_pannel").append($( "<div id='draggable_"+div+"' class='ui-widget-content'  style='height: 422px;'><button type='button' class='btn btn-primary'  style='float:right;' id='removeclick_"+div+"' onclick='RemoveChart("+div+");'> <span class='glyphicon glyphicon-remove'></span></button><div id='container_"+div+"' style=' margin-top: 50px;'></div></div>" ));
				  $("#draggable_"+div).draggable({ 
		                containment: "#dashboard_pannel", 
		                scroll: false
				  })
				drawColumn(response,selected_fields[0],div);
			}else
			if(response[0].chart_type=="time")
			{
				$("#dashboard_pannel").append($( "<div id='draggable_"+div+"' class='ui-widget-content' style='height: 402px;'><button type='button' class='btn btn-primary'  style='float:right;' id='removeclick_"+div+"' onclick='RemoveChart("+div+");'> <span class='glyphicon glyphicon-remove'></span></button><div id='container_"+div+"' style='margin-top: 50px;'></div></div>" ));
				  $("#draggable_"+div).draggable({ 
		                containment: "#dashboard_pannel", 
		                scroll: false
		         })
				drawTime(response,selected_fields[0],div);
			}else
			if(response[0].chart_type=="grid")
			{
				 $("#dashboard_pannel").append($( "<div id='draggable_"+div+"' class='ui-widget-content'   style='width: 700px;height: 302px;'><button type='button' class='btn btn-primary'  style='float:right;' id='removeclick_"+div+"' onclick='RemoveChart("+div+")'> <span class='glyphicon glyphicon-remove'></span></button><div id='container_"+div+"'  style='margin-top: 50px;'><table id='table_"+div+"'  style='margin-left: 0px;'></table><div id='pager_"+div+"'></div></div></div>" ));
				  $("#draggable_"+div).draggable({ 
		                containment: "#dashboard_pannel", 
		                scroll: false
				  })
				 chartgrid(response[0].col_name,response[0].gridcols,div);
			}else
				if(response[0].chart_type=="x-y")
				{
					$("#dashboard_pannel").append($( "<div id='draggable_"+div+"' class='ui-widget-content' style='height: 402px;'><button type='button' class='btn btn-primary'  style='float:right;' id='removeclick_"+div+"' onclick='RemoveChart("+div+")'> <span class='glyphicon glyphicon-remove'></span></button><div id='container_"+div+"' style=' margin-top: 50px;'></div></div>" ));
					  $("#draggable_"+div).draggable({ 
			                containment: "#dashboard_pannel", 
			                scroll: false
			         })
					getcount(response,selected_fields[0],div);
				}
		
			last=$('#dashboard_pannel').children().last().attr('id');
			 $('#'+last).attr('name',selected_fields[0]);
			 selected_fields.splice(0,1);
			 if(selected_fields.length>0)
			 {
				 drawcharts(selected_fields);
			 }
			 report_count= $('#dashboard_pannel').children().length; 
		}
	});
}

function drawPie(response,selected_field,div)
{
    var piedata = new Array();
	var Value=[];
	var Count=[];
	var facetVal=[];
	for ( var i = 0; i < response.length; i++) {
		Count[response[i].facetValue]=response[i].count;
		facetVal.push(response[i].facetValue);
		piedata[i] = [ response[i].lable, response[i].count ];
		Value[i]=response[i].facetValue;
	}
	chart = c3.generate({
		bindto: '#container_'+div ,
		data: {
            json: [ Count ],
            keys: {
                value: facetVal
            },
            type:'pie'
        },
        legend: {
            show: false
        },
        pie: {
            label: {
              format: function(value, ratio, id) {
                return id;
              }
            }
          },
        tooltip: {
            format: {
            },
            contents: function (d, defaultTitleFormat, defaultValueFormat, color) {
                var $$ = this, config = $$.config,
                    titleFormat = config.tooltip_format_title || defaultTitleFormat,
                    nameFormat = config.tooltip_format_name || function (name) { return name; },
                    valueFormat = config.tooltip_format_value || defaultValueFormat,
                    text, i, title, value, name, bgcolor;
                for (i = 0; i < d.length; i++) {
                    if (! (d[i] && (d[i].value || d[i].value === 0))) { continue; }

                    if (! text) {
                        title = titleFormat ? titleFormat(d[i].x) : d[i].x;
                        text = "<table style='border: 1px solid black;' class='" + $$.CLASS.tooltip + "'>" + (title || title === 0 ? "<tr><th colspan='2'>" + title + "</th></tr>" : "");
                    }

                    name = nameFormat(d[i].name);
                    value = valueFormat(d[i].value, d[i].ratio, d[i].id, d[i].index);
                    bgcolor = $$.levelColor ? $$.levelColor(d[i].value) : color(d[i].id);

                    text += "<tr style='border: 1px solid black;' class='" + $$.CLASS.tooltipName + "-" + d[i].id + "'>";
                    text += "<td style='border: 1px solid black;' class='name'><span style='background-color:" + bgcolor + "'></span>Facet Value=" + name + "</td>";
                    text += "<td style='border: 1px solid black;' class='value'>%value=" + value + "</td>";
                    text += "</tr>";
                }
                return text + "</table>";
            }
      }
    });
	$('#container_'+div).attr('chart_type','pie') ;
}
function getcount(response,selected_field,div)
{
	

			var xcolumndata = new Array();
			var ycolumndata = new Array();
			xcolumndata[0]='x';
			ycolumndata[0]='y';
			for ( var i = 0; i < response.length; i++) {
				xcolumndata.push(response[i].facetValue);
				ycolumndata.push(response[i].count);
			}
			
			var chart = c3.generate({
				bindto: '#container_'+div ,
				
			    data: {
			    	
			    	 x: 'x',
			        columns: [
                    xcolumndata,
                    ycolumndata
			        ],
			        type: 'bar'
			    },
			    legend: {
		            show: false
		        }, 
				    subchart: {
				        show: true
				    },
				    zoom: {
				        enabled: true
				    },
				    axis : {
				        x : {
				        	label:selected_field
				            
				        },
				        y : {
				        	label:'Count Value'
				        }
				    }
			
	});
			$('#container_'+div).attr('chart_type','x-y') ;
}
function drawColumn(response,selected_field,div)
{
    var columnData=[];
	var y=[];
	var x=[];
	var field=[];
	
	for(var j=0;j<response.length;j++){
		if(field.indexOf(response[j].lable)<0){
			field.push(response[j].lable);
		}
		
     }
	for(var i=0;i<field.length;i++){
		y.push([field[i]]);
		x.push(['x'+field[i]]);
		for(var j=0;j<response.length;j++){
			if(field[i]==response[j].lable){
				y[i].push(response[j].count);
                var temp=Math.round(response[j].facetValue);
				x[i].push(temp);
			}
	}
		
	}
	var xs = {};
	for(var i=0;i<x.length;i++){
		columnData.push(x[i]);
		columnData.push(y[i]);
		xs[y[i][0]] = x[i][0];
	}
	var chart = c3.generate({
		bindto: '#container_'+div,
		data: {
		xs:xs,
		
	      columns:columnData
	    },
	    axis: {
		      x: {
		        tick: {
		        	
		        	count: response.length,
		          fit: true,
		          format: function(x) {
		             return Math.round(x);
		          }
		        }
		      }
		    },
		    subchart: {
		      show: true
		    }, 
		    zoom: {
		      enabled: true
		    }
	}); 
	
	$('#container_'+div).attr('chart_type','column') ;
}

function chartgrid(col,fieldvalue,div){
	var query="*:*";
	var queryParams = {
             			"collection" : col,
             			"query" : query
           
					  };
	var gridCols=[];
	var modelFields=[];
	gridCols=fieldvalue;
	console.log('0---------------fieldvalue');
	//alert(fieldvalue.length);
	for(var i=0;i<fieldvalue.length;i++){
		var modelFieldsArr={
		  		  name : fieldvalue[i],
		  		  index : fieldvalue[i],	
		  		  width : 200
		  		
		     };
		modelFields.push(modelFieldsArr);
	}
	
$("#table_"+div).jqGrid(                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        {
		datatype : "json",
	    url:'getBasicQueryforReportsNoFilter.html',
	    ajaxGridOptions : {
   		   					contentType : 'application/json'
	    				  },
	    postData:JSON.stringify(queryParams),
	    mtype: 'POST',
	    colNames:gridCols,
	    colModel : modelFields,
	    viewrecords : true,
	    gridview : true,
	    autowidth:true,
	    forceFit:true,
		shrinkToFit:false,
		loadonce: false,
		 caption: 'GRID REPORTS',
		jsonReader: {
						repeatitems: false,
						root: function (obj) { 
												return obj; 
											  },
		                page: function (obj) { return 1; },
		                total: function (obj) { return 1; },
		                records: function (obj) { return obj.length; }                  
					},

	    				  pager : '#pager_'+div,
	    					rowNum : 10,
	    					rowList : [ 5, 10, 15 ],
	    					viewrecords : true,
	    					gridview : true,
	    					loadonce: true,
	    					//width : 600,
	    					//height: 500
	}); 
$('#container_'+div).attr('chart_type','grid') ;
}



function drawTime(response,selected_field,div)
{
	/*var chart;
    $('#draggable_'+div).resizable({
        // On resize, set the chart size to that of the
        // resizer minus padding. If your chart has a lot of data or other
        // content, the redrawing might be slow. In that case, we recommend
        // that you use the 'stop' event instead of 'resize'.
    	resize: function () {
            chart.setSize(
                this.offsetWidth - 20,
                this.offsetHeight - 20,
                false
            );
        }
    });*/
    var timedata = new Array();
	var dataX=[];
	var dataY=[];
	dataX.push("Time");
	dataY.push("Count");
	for ( var i = 1; i < response.length; i++) {
		timedata[i] = [ response[i].lable, response[i].count ];
		dataX[i]=[response[i].lable];
		dataY[i]=[response[i].count];
	}
	
	var chart = c3.generate({
		bindto: '#container_'+div ,
		data: {
		 xs:{
			
			'Time':'Count'
		},
		
	      columns: [dataX,dataY]
									
	    }
	});
	$('#container_'+div).attr('chart_type','time') ;
	
}

function savedashboard()
{
	var charts = new Array();
	var widths = new Array();
	var heights = new Array();
	var lefts = new Array();
	var tops = new Array();
	var divs =new Array();
	var chart_types =new Array();
	var dashboard=$("#dashboard_name").val();
	console.log("report_count"+report_count);
	for(var i=1;i<=report_count;i++)
		{
		var chart=$('#draggable_'+i).attr('name');
		if(chart=== undefined){
			 continue; 
		}
		var position=$('#draggable_'+i).position();
		console.log(position);
		var width=parseInt($('#draggable_'+i).css('width'));
		var height=parseInt($('#draggable_'+i).css('height'));
		var left=0;
		var left_css=position.left;
		if(left_css=="auto")
		{
		  left=0;
		}
		else
		{
		  left=parseFloat(left_css);
		}
		
		var top=0;
		var top_css=position.top;
		if(top_css=="auto")
		{
		  top=0;
		}
		else
		{
		  top=parseFloat(top_css);
		}
		
		
		var div=$('#draggable_'+i).attr('id');
		var chart_type=$('#container_'+i).attr('chart_type');
		
		charts.push(chart);
		widths.push(width);
		heights.push(height);
		lefts.push(left);
		tops.push(top);
		divs.push(div);
		chart_types.push(chart_type);
		}
	
	$.ajax({
		type : "POST",
		url : "savedashboard.html",
		data :"charts="+encodeURIComponent(charts)+"&dashboard="+encodeURIComponent(dashboard)+"&widths="+widths+"&heights="+heights+"&lefts="+lefts+"&tops="+tops+"&divs="+divs+"&chart_types="+chart_types,
		success : function(response) {
			if(response.isSaved == true){
				showAlert("Dashboard saved");
			}
			else{
				showAlert("Dashboard could not be saved because of following reason : \n "+ response.saveFailureReason);
			}
		}
	});	
}

function getReports(){
	$.ajax({
		type : "GET",
		url : "getReports.html",
		success : function(response) {
				$("#resId").find("tr:gt(0)").remove();
				for ( var i = 0; i < response.length; i++) {
				
						$("#resId").find('tbody').append(
								$('<tr>').append(
										$('<td>').append(
												'<input class="check" name="select_reports" type="checkbox" value='
														+ response[i][1] + '>')
												.append($('</td>'))).append(
										$('<td>').text(response[i][0]).append(
												$('</td>'))));
				}
			
		}
	});
}

$(function() {
	
	/*$('#charts').change(
			function() {
				var selected_field = $('#charts').find(
						":selected").text();
				var count=$('#dashboard_pannel').children().length;
				var div;
				if(count==1){
					div=0;
				}else{
					div=count-1;
				}
				div++;
				$('#draggable_'+div).attr('name',selected_field);
				 drawchart(selected_field,div);
				 
			});*/
	
	
	$("#save").click(function() {
		var selected_reports = new Array();
		$("#charts option:selected").each(function () {
			   var $this = $(this);
			   
			   if ($this.length) {
			    var selected_report = $this.text();
			    selected_reports.push(selected_report);

			   }
			});
		var namevalue=$("#dashboard_name").val();
		if(selected_reports.length > 0 )
		{
			if($("#dashboard_name").val()!="" && $("#dashboard_name").val()!=null){
				
				savedashboard();
				
			}
			else{
				showAlert('Dashboard name cannot be empty');
			}
				
		}
		else
		{
			showAlert('Please select at least one Report!');
		}
		
		
	});
	
	$("#load_reports").click(function() {
		$("#save").prop("disabled", false);
		$("#dashboard_name").prop("disabled",false);
		//$('#dashboard_pannel').children().slice(1).remove();
		$('#dashboard_pannel').empty();
		var selected_fields = new Array();
		$("#charts option:selected").each(function () {
			   var $this = $(this);
			   
			   if ($this.length) {
			    var selected_field = $this.text();
			    selected_fields.push(selected_field);

			   }
			});
		if(selected_fields.length > 0)
		{
			drawcharts(selected_fields);	
		}
		else
		{
			$("#save").prop("disabled",true);
			$("#dashboard_name").prop("disabled",true);
			showAlert('Please select at least one Report!');
		}
		
	});
	
	$("#refresh").click(function() {
		$("#save").prop("disabled",true);
		$("#dashboard_name").prop("disabled",true);
		$("#charts").multiselect('uncheckAll');
		getapplications();
		getcollections();
		getcharts();
		$('#dashboard_name').val("");
		//$('#dashboard_pannel').children().slice(1).remove();
		$('#dashboard_pannel').empty();
	});
	
	
	 $("#dialog").dialog({
         modal: true,
         autoOpen: false,
         title: "Delete Report",
        width: 500,
         height: 500,
         position: [485, 185],
         buttons: {
			 "Delete": function() {
				 
				 var selected_reports=[];
					$.each(
							$("input[name='select_reports']:checked"),
							function() {
								selected_reports.push($(this)
										.val());
								
							});
					if(selected_reports.length>0)
					{
									$( "#confirmEdit" ).show();
									$("#confirmEdit").text("Delete this Report permanently?");
									$( "#confirmEdit" ).dialog({
											buttons:{
													"Yes":function() {
														$.ajax({
															type : "GET",
															url : "deleteReports.html",
															data:"selected_reports=" +encodeURIComponent(selected_reports),
															success : function(response) {
							                         window.location.href = "createuserdashboard.html";
															}
															});      
															},
								                     "No": function(){
								                    	 $( this ).dialog( "close" );
								                     				}
													} 
																});
						}else{
							$( "#confirmEdit" ).show();
							$("#confirmEdit").text("Select atleast one Report!");
							$( "#confirmEdit" ).dialog({
								buttons:{
									OK: function() {
			 							$( this ).dialog( "close" );
			 						}	
								}
							});
						}
			 					},
				Cancel: function() {
			
	 							$( this ).dialog( "close" );
	 						}
		 }
         
     });
     $("#deleteReport").click(function () {
         $('#dialog').dialog('open');
         getReports();
         
     });
	
});
