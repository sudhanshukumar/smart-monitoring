/**
 * 
 */
package com.lnt.smartit.rule.mysql.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author 10612068
 *
 */
@Entity
@Table(name="rule")
public class Rule implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name="id")
	private int ruleId;
	
	@Column(name="name")
	private String name;
	
	@ManyToOne
	@JoinColumn(name="rule_type_code", referencedColumnName="name")
	private RuleType ruleType;
	
	@ManyToOne
	@JoinColumn(name="operation_id",referencedColumnName="id")
	private Operation operation;
	
	public int getRuleId() {
		return ruleId;
	}
	public void setRuleId(int ruleId) {
		this.ruleId = ruleId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public RuleType getRuleType() {
		return ruleType;
	}
	public void setRuleType(RuleType ruleType) {
		this.ruleType = ruleType;
	}
	public Operation getOperation() {
		return operation;
	}
	public void setOperation(Operation operation) {
		this.operation = operation;
	}

}
