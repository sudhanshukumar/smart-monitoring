<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
<%String username="Guest User"; 
	if(session.getAttribute("username") != null){
	username=session.getAttribute("username").toString();
	}%>
<script>
	function processLogout(e){
		$.ajax({
			type : "GET",
			url : "logout.html",
			success : function(response) {
				location.href="dashboard.html"
			}
		});
	}
</script>
</head>
<body>

	<div class="page-header"> 
   		<img alt="***LOGO***" id="itLogo" src="images/custom/1366_768/monitorning-380-110.png">
   		<div id="welcomeUser" >
		<table>
			<tr>
			 	<td class="welcomeName">Welcome <%=username%></td>
			 	<td>&nbsp;</td>
				<td> | </td> 
				<td>&nbsp;</td>
				<td><a onclick="processLogout(this);" href="javascript:void(0);">LOGOUT</a></td>
				<td>&nbsp;</td>
			</tr>
		</table>
		</div>
  	</div>

</body>
</html>