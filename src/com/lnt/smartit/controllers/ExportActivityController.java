package com.lnt.smartit.controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.lnt.smartit.models.GridHeaderModel;
import com.lnt.smartit.services.DataEnrichmentService;
import com.lnt.smartit.services.ExportService;
import com.lnt.smartit.util.DateUtil;

/**
 * The Class ExportActivityController.
 * Author 	: Pinakin Abhyankar
 * Date 	: 15/06/2016
 */
@Controller
public class ExportActivityController {
	
	/** The logger. */
	private static Logger logger = Logger.getLogger(ExportActivityController.class);
	
	/** The data enrichment service. */
	@Autowired
	private DataEnrichmentService dataEnrichmentService;
	
	/** The export service. */
	@Autowired
	private ExportService exportService;

	/**
	 * Export to file.
	 *
	 * @param request the request
	 * @param response the response
	 */
	@RequestMapping(value = "exportToFile.html", method = RequestMethod.POST)
	public void exportToFile(HttpServletRequest request,HttpServletResponse response) 
	{
		try 
		{
			String headerString = (String) request.getParameter("headerString");
			String fileData = (String) request.getParameter("gridData");
			String task = (String) request.getParameter("task");
			String fileName = (String) request.getParameter("fileName");
			String sheetName = (String) request.getParameter("sheetName");
			List<Map<String, Object>> gridData = dataEnrichmentService.convertJsonStringToMap(fileData);
			List<GridHeaderModel> gridHeaderList = dataEnrichmentService.convertJsonStringToObj(headerString);
			
			fileName = getUniqueFileName(fileName);
			
			String filePath = dataEnrichmentService.getExcelFilePath(fileName);
			if(fileName!=null && (fileName.endsWith(".xlsx")||fileName.endsWith(".xls")))
			{
				exportService.exportAsExcel(filePath, sheetName, gridData, gridHeaderList);	
			}
			else if(fileName!= null && fileName.endsWith(".pdf"))
			{
				exportService.exportAsPDF(filePath, gridData, gridHeaderList, dataEnrichmentService.getColumnCount(gridHeaderList));
			}
			if(task.trim().equalsIgnoreCase("Download"))
			{
				downloadFile(response, filePath);
			}
			deleteFile(filePath);
			System.out.println(filePath);
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/**
	 * Delete file.
	 *
	 * @param filePath the file path
	 * @return true, if delete file
	 */
	private boolean deleteFile(String filePath)
	{
		File file = new File(filePath);
		if(file.exists())
		{
			return file.delete();
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Download file.
	 *
	 * @param response the response
	 * @param filePath the file path
	 * @return the http servlet response
	 */
	private HttpServletResponse downloadFile(HttpServletResponse response, String filePath)
	{
		try 
		{
			File file = new File(filePath);
			String fileDir = file.getParent();
			String fileName = file.getName();
			
			response.setContentType("text/html");  
			//PrintWriter out = response.getWriter();  
			BufferedOutputStream out = new BufferedOutputStream(response.getOutputStream());
			response.setContentType("APPLICATION/OCTET-STREAM");   
			response.setHeader("Content-Disposition","attachment; filename=\"" + fileName + "\"");   

			FileInputStream fileInputStream = new FileInputStream(fileDir+"\\" + fileName);  

			int i;   
			while ((i=fileInputStream.read()) != -1) 
			{  
				out.write(i);   
			}   
			fileInputStream.close();   
			out.close();
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return response;
	}
	
	/**
	 * Gets the unique file name.
	 *
	 * @param fileName the file name
	 * @return the unique file name
	 */
	private String getUniqueFileName(String fileName)
	{
		if(fileName!=null)
		{
			String extension = FilenameUtils.getExtension(fileName);
			fileName = FilenameUtils.removeExtension(fileName);
			fileName = fileName +"_"+ DateUtil.changeTimeStampFormat(DateUtil.getCurrentTime(),"yyyyMMddhhmmss") +"."+extension;
		}
		return fileName;
	}
}
