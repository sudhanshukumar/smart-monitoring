var manageThresholdGridTableData;
$(document).ready(function(){
	
	/*$('#fromMW').datetimepicker();
	$('#toMW').datetimepicker();*/
	$('#fromMW').datetimepicker({
		format : 'Y-m-d H:i:s',
		dayOfWeekStart : 1,
		lang : 'en'
		/*	,
		allowTimes:[
		            '00:00', '00:30', '01:00','01:30', '02:00', '02:30','03:00', '03:30',
		            '04:00', '04:30', '05:00','05:30', '06:00', '06:30','07:00', '07:30',
		            '08:00', '08:30', '09:00','09:30', '10:00', '10:30','11:00', '11:30',
		            '12:00', '12:30', '13:00','13:30', '14:00', '14:30','15:00', '15:30',
		            '16:00', '16:30', '17:00','17:30', '18:00', '18:30','19:00', '19:30',
		            '20:00', '20:30', '21:00','21:30', '22:00', '22:30','23:00', '23:30',
		           ]*/
	});
	
	$('#toMW').datetimepicker({
		format : 'Y-m-d H:i:s',
		dayOfWeekStart : 1,
		lang : 'en'
	});
	$( "#dialog" ).hide();
	$("#deleteAlert").hide();
	$("#SecondAlertForm").hide();
	loadGrid();
	$("#ruleForm").hide();
	$("#addNewThresHold").click(function(){
		window.location.href = "threshold.html";
	});
	
	//10612175 on 30/01/2017 start
	getParameterTypeList();
	getAlertTypeList()
	getOperatorTypeList()
	//10612175 on 30/01/2017 end
	
	//10612175 on 24/01/2017 start modifiy
	$("#addRuleBtn").click(function(){
		//$(".RHSTable").hide();
		//$('#threshold_ParamType').empty();
		/*$("#expressionTypeRHSLabel").hide();
		$("#expressionTypeRHS").hide();
		$("#thresholdValueRHSLabel").hide();
		$("#thresholdValueRHS").hide();*/
		
		/*10614080 Email Notification Implementation 03/03/2017 Start*/
		/*$("#emailToLabel").hide();
		$("#notificationTO").hide();
		$("#emailCCLabel").hide();
		$("#notificationCC").hide();*/
		$("#notificationTO").attr("disabled", true);
		$("#notificationCC").attr("disabled", true);
		/*10614080 Email Notification Implementation 03/03/2017 End*/
		
		//10612175 on 06/04/2017 maintenance window start
		/* $("#fromMWLabel").hide();
 		 $("#fromMW").hide();
 		 $("#toMWLabel").hide();
 		 $("#toMW").hide();*/
		//10612175 on 06/04/2017 maintenance window end
					
		/* $.ajax({
			 type : "POST",
			 url : "editThreshold.html",
			 data : "thresholdMaster="+JSON.stringify(thresholdObject), 
			 success : function(response) {
				 alert('Rule saved successfully!');
				 window.location.href = "manageThresHold.html";
			 },
			 cache: false
		 });*/	
		getEditThreshold();
	});
	
	
	$("#logicalOperator").change(function(){
		 var optionSelected = $(this).find("option:selected");
	     var valueSelected  = optionSelected.val();
	     if(valueSelected == "AND" || valueSelected == "OR"){
	    	 //$(".RHSTable").show();
	    	 $("#expressionTypeRHSLabel").show();
	 		 $("#expressionTypeRHS").show();
	 		 $("#thresholdValueRHSLabel").show();
	 		 $("#thresholdValueRHS").show();
	     }else{
	    	 /*$("#expressionTypeRHSLabel").hide();
	 		 $("#expressionTypeRHS").hide();
	 		 $("#thresholdValueRHSLabel").hide();
	 		 $("#thresholdValueRHS").hide();*/
	     }
	});
	//10612175 on 24/01/2017 end
	
	//10612175 on 06/04/2017 maintenance window start
	/*$("#maintenanceWindow").change(function(){
		 var optionSelected = $(this).find("option:selected");
	     var valueSelected  = optionSelected.val();
	     if(valueSelected == "Enable"){
	    	 //$(".RHSTable").show();
	    	 $("#fromMWLabel").show();
	 		 $("#fromMW").show();
	 		 $("#toMWLabel").show();
	 		 $("#toMW").show();
	     }else{
	    	
			 $("#fromMWLabel").hide();
	 		 $("#fromMW").hide();
	 		 $("#toMWLabel").hide();
	 		 $("#toMW").hide();
			
	     }
	});*/
	//10612175 on 06/04/2017 maintenance window end
		
	$("#notification").change(function(){
		var optionSelected =$(this).find("option:selected");
		var valueSelected  = optionSelected.val();
		if(valueSelected == "Yes"){
			$("#notificationTO").attr("disabled", false);
			$("#notificationCC").attr("disabled", false);
		}else{
			$("#notificationTO").attr("disabled", true);
			$("#notificationCC").attr("disabled", true);
		}	
	});
	
	$("#threshold_ParamType").change(function(){
		$('#expressionTypeLHS').empty();
		$('#expressionTypeRHS').empty();
		var optionSelected = $(this).find("option:selected");
	     var valueSelected  = optionSelected.val();
	     if(valueSelected == "NBR"){
	    	 $('<option>').val('EQ').text('Equals').appendTo('#expressionTypeLHS');
	    	 $('<option>').val('GT').text('Greater than').appendTo('#expressionTypeLHS');
	    	 $('<option>').val('LT').text('Less than').appendTo('#expressionTypeLHS');
	    	 
	    	 $('<option>').val('EQ').text('Equals').appendTo('#expressionTypeRHS');
	    	 $('<option>').val('GT').text('Greater than').appendTo('#expressionTypeRHS');
	    	 $('<option>').val('LT').text('Less than').appendTo('#expressionTypeRHS');
	     } else if(valueSelected == "CHAR"){
	    	 $('<option>').val('EQ').text('Equals').appendTo('#expressionTypeLHS');
	    	 $('<option>').val('POS').text('Contains').appendTo('#expressionTypeLHS');
	    	 
	    	 $('<option>').val('EQ').text('Equals').appendTo('#expressionTypeRHS');
	    	 $('<option>').val('POS').text('Contains').appendTo('#expressionTypeRHS');
	     }
	});
	
	$("#AddAlert").click(function(){
		$("#SecondAlertForm").show();
	})
	
	;(function($){
	    $.fn.extend({
	        donetyping: function(callback,timeout){
	            timeout = timeout || 1e3; // 1 second default timeout
	            var timeoutReference,
	                doneTyping = function(el){
	                    if (!timeoutReference) return;
	                    timeoutReference = null;
	                    callback.call(el);
	                };
	            return this.each(function(i,el){
	                var $el = $(el);
	                // Chrome Fix (Use keyup over keypress to detect backspace)
	                // thank you @palerdot
	                $el.is(':input') && $el.on('keyup keypress paste disabled',function(e){
	                    // This catches the backspace button in chrome, but also prevents
	                    // the event from triggering too preemptively. Without this line,
	                    // using tab/shift+tab will make the focused element fire the callback.
	                    if (e.type=='keyup' && e.keyCode!=8) return;
	                    
	                    // Check if timeout has been set. If it has, "reset" the clock and
	                    // start over again.
	                    if (timeoutReference) clearTimeout(timeoutReference);
	                    timeoutReference = setTimeout(function(){
	                        // if we made it here, our timeout has elapsed. Fire the
	                        // callback
	                        doneTyping(el);
	                    }, timeout);
	                }).on('blur',function(){
	                    // If we can, fire the event since we're leaving the field
	                    doneTyping(el);
	                });
	            });
	        }
	    });
	})(jQuery);
	
	//10612175 on 25/01/2017 start -- to add delay for doneTyping function call
	
	//setup before functions
	var typingTimer;                //timer identifier
	var doneTypingInterval = 5000;  //time in ms, 5 second for example
	var $input = $('#threshold_hostName');

	//on keyup, start the countdown
	$input.on('keyup', function () {
	  clearTimeout(typingTimer);
	  typingTimer = setTimeout(doneTyping, doneTypingInterval);
	});

	//on keydown, clear the countdown 
	$input.on('keydown', function () {
	  clearTimeout(typingTimer);
	});
	
	//10612175 on 25/01/2017 end
	
	//$("#threshold_hostName").donetyping(function(){
	function doneTyping(){
		  //document.getElementById("wait").style.display = "block";  // show the loading message.
		$('#wait').show(); 	
		 $.ajax({
			 type : "POST",
			 url : "getParamsForHost.html",
			 data : "hostname=" + $("#threshold_hostName").val(), 
			 success : function(response) {
				// document.getElementById("wait").style.display = "none";
				// $('#wait').hide();  
				 console.log(response);
				 if(response.length == 0){
					 //alert('Parameter is not configured for given host.');
					 //jAlert('This is a custom alert box', 'Alert Dialog');
					 $('#wait').hide();
					 alert2($("#threshold_hostName").val()+" is not on boarded into system.","Note : ","Ok");
					 
					 throw new Error('Parameter is not configured for given host.');
					 
					 
				 }else{
					 $('#wait').hide();
					 for(var i=0;i<response.length;i++){
						 $('<option>').val(response[i].appId).text(response[i].paramName).appendTo('#threshold_ParamName');
					 } 
				 }				 
			 },
		 }); 
	}
});

function loadGrid(){
	var gridData;
	/*$.ajax({
		type : "GET",
		url : "populateThresholdGrid.html",
		success : function(response){
			gridData = response.gridData;		
		}
	})*/
	$("#manageThresholdGridTable").jqGrid({
		jsonReader : {
		root : "gridData", // an array that contains the actual data
		page : "page", // current page of the query
		total : "total", // total pages for the query
		records : "records",// total number of records for  the query
		repeatitems : false,
		id : "thresholdId" // the unique id of the row
	},
	url : 'populateThresholdGrid.html',
	datatype :"json",
	mtype : "GET",	
/*	datatype : "local",
	data : gridData,*/
	localReader: {repeatitems: true},
	colModel : [
			{
				name : 'id',
				label : 'Id',
				align : 'left',
				width : 30,
				hidden:true,
				sortable: true
			},
			{
				name : 'appLogParamId',
				label : 'Log Id',
				align : 'left',
				width : 30,
				hidden:true,
				sortable: true
			},
			{
				name : 'hostName',
				label : 'Hostname',
				align : 'left',
				width : 30,
			},
			{
				name : 'paramName',
				label : 'Parameter Name',
				align : 'left',
				width : 50,
				formatter : function(value){
					return checkMaintenanceWindow(value);
				}
			},
			
			{
				name : 'thresholdType',
				label : 'Alert Type',
				align : 'left',
				width : 50,
			},
			//10612175 on 24/01/2017 start
			{
				name : 'filterDuration',
				label : 'Alert Duration(Seconds)',
				align : 'left',
				width : 30,
			},
			{
				name : 'autoIncident',
				label : 'Auto Incident',
				align : 'left',
				width : 30,
			},
			{
				name:'deduplicationWindow',
				label : 'Deduplication Window(Seconds)',
				align : 'left',
				width : 30,
				
			},
			//10612175 on 24/01/2017 end
			{
				name : 'parameterType',
				label : 'Parameter Type',
				align : 'left',
				width : 50,
			},
			{
				name : 'parameterTypeCode',
				label : 'Parameter Type Code',
				align : 'left',
				width : 50,
				hidden:true,
			},
			{
				name : 'expressionTypeLHS',
				label : 'Expression 1',
				align : 'left',
				width : 30,
			},
			{
				name : 'expressionTypeLHSCode',
				label : 'Expression 1 Code',
				align : 'left',
				width : 30,
				hidden:true,				
			},
			{
				name : 'thresholdValueLHS',
				label : 'Threshold Value1',
				align : 'left',
				width : 30,
				formatter : function(value){
					return escapeXml(value);
				}
			},
			{
				name : 'logicalOperator',
				label : 'Operator',
				align : 'left',
				width : 30,
			},
			{
				name : 'expressionTypeRHS',
				label : 'Expression 2',
				align : 'left',
				width : 30,
			},
			{
				name : 'expressionTypeRHSCode',
				label : 'Expression 2 Code',
				align : 'left',
				width : 30,
				hidden:true,				
			},
			{
				name : 'thresholdValueRHS',
				label : 'Threshold Value2',
				align : 'left',
				width : 30,
				formatter : function(value){
					return escapeXml(value);
				}
			},			
			/*10614080 Notification 06/03/2017 start*/
			{
				name : 'notification',
				label : 'Notification',
				align : 'left',
				width : 30,
			},
			/*10614080 Notification 06/03/2017 end*/
			{
				label : 'Actions',
				name : 'actionColumn',
				width : 90,
				fixed : true,
				sortable : false,
				resize : false,
				formatter : function(cellvalue, options,rowObject) {
					return showActionButtons(cellvalue, options, rowObject);
				}
			} ],
	pager : '#manageThresholdGridPager',
	rowNum : -1,
	rowList : [ 10, 20, 30 ],
	viewrecords : true,
	gridview : true,
	loadonce : false,
	width : 1350,
	height: 430

});
//10612175 on 24/01/2017 start
$("#manageThresholdGridTable").jqGrid("setLabel","hostName","",{"text-align":"left"});
$("#manageThresholdGridTable").jqGrid("setLabel","paramName","",{"text-align":"left"});
$("#manageThresholdGridTable").jqGrid("setLabel","parameterType","",{"text-align":"left"});
$("#manageThresholdGridTable").jqGrid("setLabel","thresholdType","",{"text-align":"left"});
$("#manageThresholdGridTable").jqGrid("setLabel","filterDuration","",{"text-align":"left"});
$("#manageThresholdGridTable").jqGrid("setLabel","autoIncident","",{"text-align":"left"});
$("#manageThresholdGridTable").jqGrid("setLabel","deduplicationWindow","",{"text-align":"left"});
$("#manageThresholdGridTable").jqGrid("setLabel","expressionTypeLHS","",{"text-align":"left"});
$("#manageThresholdGridTable").jqGrid("setLabel","thresholdValueLHS","",{"text-align":"left"});
$("#manageThresholdGridTable").jqGrid("setLabel","logicalOperator","",{"text-align":"left"});
$("#manageThresholdGridTable").jqGrid("setLabel","expressionTypeRHS","",{"text-align":"left"});
$("#manageThresholdGridTable").jqGrid("setLabel","thresholdValueRHS","",{"text-align":"left"});
$("#manageThresholdGridTable").jqGrid("setLabel","actionColumn","",{"text-align":"left"});
//10612175 on 24/01/2017 end

//10614080 on 06/03/2017 start
$("#manageThresholdGridTable").jqGrid("setLabel","notification","",{"text-align":"left"});
//10614080 on 06/03/2017 end

$("#manageThresholdGridTable").jqGrid('setGridParam', {
onSelectRow : function(rowid) {
var rowdata = $(this).jqGrid('getRowData', rowid);
addedNOtificationRowdata = JSON.stringify(rowdata);
}
});

}

function showActionButtons(cellvalue, options, rowObject) {
/*	var previewThresholdLink= '<img title="Preview" onClick=previewThreshold("'
	    +encodeURI(JSON.stringify(rowObject))
	    +'") src="images/common/preview-16.png"/>';*/
	

	var editThresholdLink = '<img title="Edit" onClick=editThreshold("'
			+ encodeURI(JSON.stringify(rowObject))
			+ '") src="images/common/edit-6-16.png"/>';
	
	var deleteThresholdLink = '<img title="Delete" onClick=deleteThreshold("'
		+ encodeURI(JSON.stringify(rowObject))
		+ '") src="images/common/delete-3-16.png"/>';

	var actionThresholdLink =  "&nbsp;&nbsp;&nbsp;" + editThresholdLink+ "&nbsp;&nbsp;&nbsp;" +deleteThresholdLink;
	return actionThresholdLink;
} 


function editThreshold(value){
	var dataJSON = JSON.parse(decodeURI(value));
	console.log(dataJSON);
	//10612175 on 24/01/2017 start
	/*if(dataJSON.logicalOperator  == "AND" || dataJSON.logicalOperator  == "OR" ){
		$("#expressionTypeRHSLabel").show();
		$("#expressionTypeRHS").show();
		$("#thresholdValueRHSLabel").show();
		$("#thresholdValueRHS").show();
	}else{
		$("#expressionTypeRHSLabel").hide();
		$("#expressionTypeRHS").hide();
		$("#thresholdValueRHSLabel").hide();
		$("#thresholdValueRHS").hide();
	}*/
	//10612175 on 24/01/2017 end
	//10612175 on 06/04/2017 maintenance window start
	/*if(dataJSON.maintenanceWindow  == "Enable"){
		$("#fromMWLabel").show();
		$("#fromMW").show();
		$("#toMWLabel").show();
		$("#toMW").show();
	}else{
		$("#fromMWLabel").hide();
		$("#fromMW").hide();
		$("#toMWLabel").hide();
		$("#toMW").hide();
	}*/
	//10612175 on 06/04/2017 maintenance window end
	//10614080 on 06/03/2016 start
	if(dataJSON.notification != "Yes"){
		$("#notificationTO").attr("disabled", true);
		$("#notificationCC").attr("disabled", true);
	}
	//10614080 on 06/03/2016 end
	$("#threshold_ParamName").attr("disabled", true);
	$("#threshold_hostName").attr("disabled", true);
	$('#threshold_ParamName').empty();
	$("#threshold_hostName").val(dataJSON.hostName);
	$('<option>').val(dataJSON.appLogParamId).text(dataJSON.paramName).appendTo('#threshold_ParamName');
	$("#threshold_ParamType").val(dataJSON.parameterTypeCode);
	//$("#threshold_ParamTypeCode").val(dataJSON.parameterTypeCode);
	$('#threshold_ParamType').trigger('change');
	$("#expressionTypeLHS").val(dataJSON.expressionTypeLHSCode);
	//$("#expressionTypeLHSCode").val(dataJSON.expressionTypeLHSCode);
	$("#thresholdValueLHS").val(dataJSON.thresholdValueLHS);
	$("#logicalOperator").val(dataJSON.logicalOperator);
	$("#thresholdType").val(dataJSON.thresholdType.toUpperCase());
	$("#expressionTypeRHS").val(dataJSON.expressionTypeRHSCode);
	//$("#expressionTypeRHSCode").val(dataJSON.expressionTypeRHSCode);
	$("#thresholdValueRHS").val(dataJSON.thresholdValueRHS);
	$("#paramRuleId").val(dataJSON.id);
	$("#alertDuration").val(dataJSON.filterDuration);
	$("#autoIncident").val(dataJSON.autoIncident)
	$("#deduplicationWindow").val(dataJSON.deduplicationWindow);
	
	//10614080 on 06/03/2016 start
	$("#notification").val(dataJSON.notification);
	$("#notificationTO").val(dataJSON.notificationTO);
	$("#notificationCC").val(dataJSON.notificationCC);
	//10614080 on 06/03/2016 end
	
	//10612175 on 06/04/2017 maintenance window start
	/*$("#maintenanceWindow").val(dataJSON.maintenanceWindow);*/
	debugger;
	if(dataJSON.fromMW == null){
		$("#fromMW").val("");
	} else{
		var a = new Date(dataJSON.fromMW);
		var month = Number(a.getMonth()) + Number(1); //because a.getMonth() returns 0-11
		var b = a.getFullYear()+'-'+("0" + month).slice(-2)+'-'+("0" + a.getDate()).slice(-2)+' '+("0" + a.getHours()).slice(-2)+':'+("0" + a.getMinutes()).slice(-2)+':'+("0" + a.getSeconds()).slice(-2);
		$("#fromMW").val(b);	
	}
	if(dataJSON.toMW == null){
		$("#toMW").val("");
	} else{
		a = new Date(dataJSON.toMW);
		var month = Number(a.getMonth()) + Number(1); //because a.getMonth() returns 0-11
		b = a.getFullYear()+'-'+("0" + month).slice(-2)+'-'+("0" + a.getDate()).slice(-2)+' '+("0" + a.getHours()).slice(-2)+':'+("0" + a.getMinutes()).slice(-2)+':'+("0" + a.getSeconds()).slice(-2);
		$("#toMW").val(b);
	}
		//10612175 on 06/04/2017 maintenance window end
	
	$("#ruleForm").dialog({
	    modal: true,
	    autoOpen: false,
	    title: "Manage Rule",
	    width: 1040,
	    height: 485,
	    position: [150, 110],
	    class : 'ui-icon ui-icon-closethick',
	    buttons: {
			 "Update": {
				 click : function() {
					 
					 //10614080 on 10/03/2016 start
					 var filterEdit= /\b\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+\b/;
					//10614080 on 10/03/2016 end
					 
					 if($("#threshold_hostName").val() == ""){
						// alert("Please enter Host Name.");
						 alert2("Please enter Host Name.","Note : ","Ok");
							
						 throw new Error('Please enter Host Name.');
					 }
					 if($("#threshold_ParamType").val() == "0"){
						 //alert("Please select Parameter Type.");
						 alert2("Please select Parameter Type.","Note : ","Ok");
							
						 throw new Error('Please select Parameter Type.');
					 }
					 if($("#threshold_ParamName").val() == null){
						// alert("Please select Parameter Name.");
						 alert2("Please select Parameter Name.","Note : ","Ok");
							
						 throw new Error('Please select Parameter Name.');
					 }
					 if($("#expressionTypeLHS").val() == null){
						// alert("Please select Operator.");
						 alert2("Please select Operator.","Note : ","Ok");
							
						 throw new Error('Please select Operator.');
					 }
					 if($("#thresholdType").val() == null){
						// alert("Please select Alert Type");
						 alert2("Please select Alert Type","Note : ","Ok");
							
						 throw new Error('Please select Alert Type');
					 }
					 if($("#thresholdValueLHS").val() == ""){
						// alert("Please enter data for Value.");
						 alert2("Please enter data for Value.","Note : ","Ok");
							
						 throw new Error('Please enter data for Value.');
					 }
					 if($("#logicalOperator").val() != null){
						 if($("#logicalOperator").val() != "null"){
							 if($("#expressionTypeRHS").val() == null){
								 if($("#expressionTypeRHS").val() == "null"){
									// alert("Please select Operator.");
									 alert2("Please select Operator.","Note : ","Ok");
										
									 throw new Error('Please select Operator.');
								 }
							 }
							 if($("#thresholdValueRHS").val() == ""){
								// alert("Please enter data for Value.");
								 alert2("Please enter data for Value.","Note : ","Ok");
									
								 throw new Error('Please enter data for Value.');
							 }
						 }
					 }
					//10614080 on 06/03/2016 start
					 if($("#notification").val() == "0"){

						 alert2("Please select Notification.","Note : ","Ok");
							
						 throw new Error('Please select Notification choice.');
					 }
					 if(($("#notification").val() != null)&& $("#notification").val() != "No"){
						 if($("#notificationTO").val() == ''){

							 alert2("Please enter Email To.","Note : ","Ok");
								
							 throw new Error('Please enter Email To');
						 }
						 if(!filterEdit.test($("#notificationTO").val())){
	 							
							 alert2("Please enter valid Email To format.","Note : ","Ok");
							 							
							 throw new Error('Please enter valid Email To format');
							 }
						 if($("#notificationCC").val() == ''){

								 alert2("Please enter Email CC.","Note : ","Ok");
									
								 throw new Error('Please enter Email CC');
							 }
						 if(!filterEdit.test($("#notificationCC").val())){
	 							
							 alert2("Please enter valid Email CC format.","Note : ","Ok");
							 							
							 throw new Error('Please enter valid Email CC format');
							 }
					 }
					//10614080 on 06/03/2016 end
								 
					 
					 if($("#deduplicationWindow").val() != null){
						 if(!($.isNumeric($("#deduplicationWindow").val()))){
							 alert2("Please enter numeric value for Duplication window.","Note : ","Ok")
							 throw new Error('Please enter numeric value for Duplication window.');
						 }
					 }
					 
					 if($("#alertDuration").val() != null){
						 if(!($.isNumeric($("#alertDuration").val()))){
							 alert2("Please enter numeric value for Alert Duration.","Note : ","Ok");
							 throw new Error('Please enter numeric value for Alert Duration.');
						 }
					 }
					 
					 //10612175 on 12/05/2017 start
					 debugger;
					 if($("#fromMW").val() != null){						 
						 if(new Date($("#fromMW").val()) < new Date()){
							 alert2("Maintenance Start time can't be before current time.","Note : ","Ok");
							 throw new Error("Maintenance Start time can't be before current datetime.");
						 }
						 if($("#toMW").val() == null || $("#toMW").val() == ""){
							 alert2("Please select time for Maintenance End time","Note : ","Ok");
							 throw new Error('Please select time for Maintenance End time');
						 }
						 
					 }
					 if($("#toMW").val() != null){						 
						 if($("#fromMW").val() == null || $("#fromMW").val() ==""){
							 alert2("Please select time for Maintenance Start time","Note : ","Ok");
							 throw new Error('Please select time for Maintenance Start time');
						 }
						 if(new Date($("#toMW").val()) < new Date($("#fromMW").val())){
							 alert2("Maintenance End time can't be before Maintenance Start time.","Note : ","Ok");
							 throw new Error("Maintenance End time can't be before Maintenance Start time.");
						 }
						 if(new Date($("#toMW").val()) == new Date($("#fromMW").val())){
							 alert2("Maintenance End time can't be same as Maintenance Start time.","Note : ","Ok");
							 throw new Error("Maintenance End time can't be same as Maintenance Start time.");
						 }
					 }					 
					 //10612175 on 12/05/2017 end
					 
					 var thresholdObject={
							 paramName:$("#threshold_ParamName").val(),
							 hostName:$("#threshold_hostName").val(),
							 parameterType:$("#threshold_ParamType").val(),
							 parameterTypeCode:$("#threshold_ParamTypeCode").val(),
							 expressionTypeLHS:$("#expressionTypeLHS").val(),
							 expressionTypeLHSCode:$("#expressionTypeLHSCode").val(),
							 thresholdValueLHS:$("#thresholdValueLHS").val(),
							 logicalOperator:$("#logicalOperator").val(),
							 thresholdType:$("#thresholdType").val(),
							 expressionTypeRHS:$("#expressionTypeRHS").val(),
							 expressionTypeRHSCode:$("#expressionTypeRHSCode").val(),
							 thresholdValueRHS:$("#thresholdValueRHS").val(),
							 id:parseInt($("#paramRuleId").val()),
							 filterDuration:parseInt($("#alertDuration").val()),
							 autoIncident:$("#autoIncident").val(),
							 deduplicationWindow:$("#deduplicationWindow").val(),
							 notification:$("#notification").val(),
							 notificationTO:$("#notificationTO").val(),
							 notificationCC:$("#notificationCC").val(),
							/* maintenanceWindow:$("#maintenanceWindow").val(),*/
							 fromMW:new Date($("#fromMW").val()),
							 toMW:new Date($("#toMW").val())
							 };
					 console.log(JSON.stringify(thresholdObject));
					 $.ajax({
						 type : "POST",
						 url : "editThreshold.html",
						 data : "thresholdMaster="+JSON.stringify(thresholdObject), 
						 success : function(response) {
							 alert2("Rule saved successfully!","Note : ","Ok");
								
							 window.location.href = "manageThresHold.html";
						 },
						 cache: false
					 }); 
					 $( this ).dialog( "close" );
				 },
				 text : 'Save',
				 class :'btn btn-primary btn-sm'
			 },
				 
			"Cancel":{
				click : function() {
					$( this ).dialog( "close" );
		 		},
		 		text : 'Cancel',
				 class :'btn btn-primary btn-sm'
			}
			 
	    }
	});
	$('#ruleForm').closest(".ui-dialog")
	     .find(".ui-dialog-titlebar-close")
	     .addClass("ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close");
	$('#ruleForm').closest(".ui-dialog")
	    .find(".ui-dialog-titlebar-close")
	    .html("<span class='ui-button-icon-primary ui-icon ui-icon-closethick'></span>")
	
	 $('#ruleForm').dialog('open');
	//window.location.href = "threshold.html?thresholdId="+thresholdId+"&action=edit";
	
}
/*function previewThreshold(value){
	var dataJSON = JSON.parse(decodeURI(value));
	var thresholdId=dataJSON.thresholdId;
	console.log();
	window.location.href = "threshold.html?thresholdId="+thresholdId+"&action=preview";
	
}*/
function deleteThreshold(value){
	var dataJSON = JSON.parse(decodeURI(value));
	$("#confirmDelete").show();
	$( "#confirmDelete" ).dialog({
		 resizable: false,
		 autoOpen: false,
		 height:200,
		 width: 340,
		 position: [485, 185],
		 modal: true,
		 buttons: {
			 "Yes": function() {
				 var dataJSON = JSON.parse(decodeURI(value));
				 var thresholdId=dataJSON.id;
				// $("deleteAlert").show();
				// $( "#deleteAlert" ).dialog({ buttons:{"Ok":function() {
					 $.ajax({
						 type : "POST",
						 url : "deleteThreshold.html",
						 data : "thresholdId=" + thresholdId, 
						 success : function(response) {
							 window.location.href = "manageThresHold.html";
						 },
						 cache: false
					 }); 
					//}} });
					
					 
				 
				
			 },
				Cancel: function() {
			
	 							$( this ).dialog( "close" );
	 						}
		 }
		 
	});
	/*if(dataJSON.voidInd=='Disabled'){
		$( "#dialog" ).show();
		 $( "#dialog" ).dialog();
		 
	 }
	else{
		$("#confirmDelete").show();
		$( "#confirmDelete" ).dialog({
			 resizable: false,
			 autoOpen: false,
			 height:200,
			 width: 340,
			 position: [485, 185],
			 modal: true,
			 buttons: {
				 "Yes": function() {
					 var dataJSON = JSON.parse(decodeURI(value));
					 var thresholdId=dataJSON.id;
					
						 $.ajax({
							 type : "POST",
							 url : "deleteThreshold.html",
							 data : "thresholdId=" + thresholdId, 
							 success : function(response) {
								 window.location.href = "manageThresHold.html";
							 },
							 cache: false
						 }); 
						 
					 
					
				 },
					Cancel: function() {
				
		 							$( this ).dialog( "close" );
		 						}
			 }
			 
		});
		
	}*/
	
	
	$("#confirmDelete").dialog("open");
	$(".ui-dialog-title").text('Delete?');
	$(".ui-dialog-titlebar-close").hide();
	$("#confirmDelete").text("Delete this Rule ?");
}

function getEditThreshold(){
	
	
	//10612175 on 25/1/2017 start 
	$("#threshold_ParamName").attr("disabled", false);
	$("#threshold_hostName").attr("disabled", false);
	$('#threshold_ParamName').empty();
	$("#threshold_hostName").val("");
	//$('<option>').val(dataJSON.appLogParamId).text(dataJSON.paramName).appendTo('#threshold_ParamName');
	$("#threshold_ParamType").val("0");
	$('#threshold_ParamType').trigger('change');
	$("#expressionTypeLHS").val("0");
	$("#thresholdValueLHS").val("");
	$("#logicalOperator").val("null");
	$("#thresholdType").val("0");
	$("#expressionTypeRHS").val("0");
	$("#thresholdValueRHS").val("");
	$("#paramRuleId").val("");
	$("#alertDuration").val("0");
	$("#autoIncident").val("No");
	$("#deduplicationWindow").val("0");
	//10612175 on 25/1/2017 end
	
	//10614080 on 03/03/2017
	$("#notification").val("0");
	$("#notificationTO").val("");
	$("#notificationCC").val("");
	
	//10612175 on 07/04/2017 strat
	/*$("#maintenanceWindow").val("0");*/
	$("#fromMW").val("");
	$("#toMW").val("");
	//10612175 on 07/04/2017 end
	//$('#myform')[0].reset();
	$("#ruleForm").dialog({
	    modal: true,
	    autoOpen: false,
	    title: "Manage Rule",
	    width: 1040,
	    height: 485,
	    position: [150, 110],
	    class : 'ui-icon ui-icon-closethick',
	    buttons: {
			 "Update": {
				 click : function() {
					 
					//10614080 on 10/03/2016 start
					 var filter= /\b\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+\b/; 
					 //10614080 on 10/03/2016 end
					 
					  //10612175 on 25/01/2017 form validation start		
					  if($("#threshold_hostName").val() == ""){
						// alert("Please enter Host Name.");
						 alert2("Please enter Host Name.","Note : ","Ok");
							
						 throw new Error('Please enter Host Name.');
					 }
					 if($("#threshold_ParamType").val() == "0"){
						// alert("Please select Parameter Type.");
						 alert2('Please select Parameter Type.',"Note : ","Ok");
							
						 throw new Error('Please select Parameter Type.');
					 }
					 if($("#threshold_ParamName").val() == null){
						// alert("Please select Parameter Name.");
						 alert2("Please select Parameter Name.","Note : ","Ok");
							
						 throw new Error('Please select Parameter Name.');
					 }
					 if($("#expressionTypeLHS").val() == null){
						// alert("Please select Operator.");
						 alert2("Please select Operator.","Note : ","Ok");
							
						 throw new Error('Please select Operator.');
					 }
					 if($("#thresholdType").val() == null){
						// alert("Please select Alert Type");
						 alert2("Please select Alert Type","Note : ","Ok");
							
						 throw new Error('Please select Alert Type');
					 }
					 if($("#thresholdValueLHS").val() == ""){
						// alert("Please enter data for Value.");
						 alert2("Please enter data for Value.","Note : ","Ok");
							
						 throw new Error('Please enter data for Value.');
					 }
					 if($("#logicalOperator").val() != null){
						 if($("#logicalOperator").val() != "null"){
							 if($("#expressionTypeRHS").val() == null){
								 if($("#expressionTypeRHS").val() == "null"){
									// alert("Please select Operator.");
									 alert2("Please select Operator.","Note : ","Ok");
										
									 throw new Error('Please select Operator.');
								 }
							 }
							 if($("#thresholdValueRHS").val() == ""){
								// alert("Please enter data for Value.");
								 alert2("Please enter data for Value.","Note : ","Ok");
									
								 throw new Error('Please enter data for Value.');
							 }
						 }
					 }
					//10614080 on 06/03/2016 start
					 if($("#notification").val() == "0"){
						 
						 alert2("Please select Notification.","Note : ","Ok");
							
						 throw new Error('Please select Notification choice.');
					 }
					 if(($("#notification").val() != null)&& $("#notification").val() != "No"){
						 if($("#notificationTO").val() == ''){
							
							 alert2("Please enter Email To.","Note : ","Ok");
								
							 throw new Error('Please enter Email To');
						 }
						 if(!filter.test($("#notificationTO").val())){
								
							 alert2("Please enter valid Email To format.","Note : ","Ok");
								
							 throw new Error('Please enter valid Email To format');
						 }
						 				 
						 if($("#notificationCC").val() == ''){
								
								 alert2("Please enter Email CC.","Note : ","Ok");
									
								 throw new Error('Please enter Email CC');
							 }
						 if(!filter.test($("#notificationCC").val())){
								
							 alert2("Please enter valid Email CC format.","Note : ","Ok");
								
							 throw new Error('Please enter valid Email CC format');
						 }
					 }
					//10614080 on 06/03/2016 end
									 
					 if($("#deduplicationWindow").val() != null){
						 if(!($.isNumeric($("#deduplicationWindow").val()))){
							 alert2("Please enter numeric value for Duplication window.","Note : ","Ok")
							 throw new Error('Please enter numeric value for Duplication window.');
						 }
					 }
					 
					 if($("#alertDuration").val() != null){
						 if(!($.isNumeric($("#alertDuration").val()))){
							 alert2("Please enter numeric value for Alert Duration.","Note : ","Ok");
							 throw new Error('Please enter numeric value for Alert Duration.');
						 }
					 }
					 manageThresholdGridTableData =  $("#manageThresholdGridTable").jqGrid('getRowData');
					 console.log(manageThresholdGridTableData);
					 for (var i = 0; i < manageThresholdGridTableData.length; i++) {
				         if(manageThresholdGridTableData[i].hostName == $("#threshold_hostName").val() && 
				        		 manageThresholdGridTableData[i].paramName == $("#threshold_ParamName option:selected").text() &&
				        		 manageThresholdGridTableData[i].thresholdType == $("#thresholdType").val()){	
				        	 
				        	// alert("Rule already configured for this host with given parameter name and alert type, do you want to override it ?");
				        	 alert2("Rule already configured for this host with given parameter name and alert type, do you want to override it ?","Note : ","Ok");
								
				        	 throw new Error("Rule already configured for this host with given parameter name and alert type, do you want to override it ?");
				         }
				    }
						
					//10612175 on 25/01/2017 form validation end		
					 
						//10612175 on 06/04/2017 maintenance window start
						//$("#maintenanceWindow").val(dataJSON.maintenanceWindow);
						var a = new Date($("#fromMW").val());
						var month = Number(a.getMonth()) + Number(1); //because a.getMonth() returns 0-11
						var b = a.getFullYear()+'-'+("0" + month).slice(-2)+'-'+("0" + a.getDate()).slice(-2)+' '+("0" + a.getHours()).slice(-2)+':'+("0" + a.getMinutes()).slice(-2)+':'+("0" + a.getSeconds()).slice(-2);
						$("#fromMW").val(b);
						
						a = new Date($("#toMW").val());
						var month = Number(a.getMonth()) + Number(1); //because a.getMonth() returns 0-11
						b = a.getFullYear()+'-'+("0" + month).slice(-2)+'-'+("0" + a.getDate()).slice(-2)+' '+("0" + a.getHours()).slice(-2)+':'+("0" + a.getMinutes()).slice(-2)+':'+("0" + a.getSeconds()).slice(-2);
						$("#toMW").val(b);
						//10612175 on 06/04/2017 maintenance window end

					 var thresholdObject={
							 paramName:$("#threshold_ParamName").val(),
							 hostName:$("#threshold_hostName").val(),
							 parameterType:$("#threshold_ParamType").val(),
							 expressionTypeLHS:$("#expressionTypeLHS").val(),
							 thresholdValueLHS:$("#thresholdValueLHS").val(),
							 logicalOperator:$("#logicalOperator").val(),
							 thresholdType:$("#thresholdType").val(),
							 expressionTypeRHS:$("#expressionTypeRHS").val(),
							 thresholdValueRHS:$("#thresholdValueRHS").val(),
							 id:parseInt($("#paramRuleId").val()),
							 filterDuration:parseInt($("#alertDuration").val()),
							 autoIncident:$("#autoIncident").val(),
							 deduplicationWindow:$("#deduplicationWindow").val(),
							 notification:$("#notification").val(),
							 notificationTO:$("#notificationTO").val(),
							 notificationCC:$("#notificationCC").val(),
							/* maintenanceWindow:$("#maintenanceWindow").val(),*/
							 fromMW:new Date($("#fromMW").val()),
							 toMW:new Date($("#toMW").val())
							 };
					 console.log(JSON.stringify(thresholdObject));
					 $.ajax({
						 type : "POST",
						 url : "editThreshold.html",
						 data : "thresholdMaster="+JSON.stringify(thresholdObject), 
						 success : function(response) {
							// alert('Rule saved successfully!');
							 alert2("Rule saved successfully!","Note : ","Ok");
								
							 window.location.href = "manageThresHold.html";
						 },
						 cache: false
					 }); 
					 $( this ).dialog( "close" );
				 },
				 text : 'Save',
				 class :'btn btn-primary btn-sm'
			 },
				 
			"Cancel":{
				click : function() {
					$( this ).dialog( "close" );
		 		},
		 		text : 'Cancel',
				 class :'btn btn-primary btn-sm'
			}
			 
	    }
	});
	$('#ruleForm').closest(".ui-dialog")
	     .find(".ui-dialog-titlebar-close")
	     .addClass("ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close");
	$('#ruleForm').closest(".ui-dialog")
	    .find(".ui-dialog-titlebar-close")
	    .html("<span class='ui-button-icon-primary ui-icon ui-icon-closethick'></span>")
	 $('#ruleForm').dialog('open');
			 
}

function getParameterTypeList(){
	$.ajax({
		type : "GET",
		url : "getParameterTypeList.html",
		success : function(response){
			console.log(response);
			for(var i=0;i<response.length;i++){
				 $('<option>').val(response[i].code).text(response[i].name).appendTo('#threshold_ParamType');
			 }	
			
		}
	});
}

function getAlertTypeList(){
	$.ajax({
		type : "GET",
		url : "getAlertTypeList.html",
		success : function(response){
			console.log(response);
			for(var i=0;i<response.length;i++){
				 $('<option>').val(response[i].code).text(response[i].name).appendTo('#thresholdType');
			 }	
			
		}
	});
}

function getOperatorTypeList(){
	$.ajax({
		type : "GET",
		url : "getOperatorTypeList.html",
		success : function(response){
			console.log(response);
			for(var i=0;i<response.length;i++){
				 $('<option>').val(response[i].code).text(response[i].code).appendTo('#logicalOperator');
			 }	
			
		}
	});
}

function alert2(message, title, buttonText) {

    buttonText = (buttonText == undefined) ? "Ok" : buttonText;
    title = (title == undefined) ? "Note : " : title;

    var div = $('<div>');
    div.html(message);
    div.attr('title', title);
    div.dialog({
        autoOpen: true,
        modal: true,
        draggable: false,
        resizable: false,
        buttons: [{
            text: buttonText,
            click: function () {
                $(this).dialog("close");
                div.remove();
            }
        }]
    });
}


function escapeXml(unsafe) {
    if(unsafe!=null){
return unsafe.replace(/[<>&'"]/g, function (c) {
switch (c) {
    case '<': return '&lt;';
    case '>': return '&gt;';
    case '&': return '&amp;';
    case '\'': return '&apos;';
    case '"': return '&quot;';
}
});
    	}
    	return "";
    }
    
function checkMaintenanceWindow(value){
	if(value.includes("**")){
    		return value.replace(/\*/g," ")+"<img src='maintenance_icon.jpg' />";
    	}
    	return value;
    	
    }