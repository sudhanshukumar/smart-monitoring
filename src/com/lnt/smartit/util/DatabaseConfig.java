package com.lnt.smartit.util;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lnt.smartit.mysql.dao.LogDao;
import com.lnt.smartit.mysql.model.AppServiceCompParams;

@Service
public class DatabaseConfig {
	private static Logger logger = Logger.getLogger(DatabaseConfig.class);
	@Autowired
	private LogDao logDao;
	
	public static List<AppServiceCompParams> config;
	public static List<String> topicName;
	public static String sqlUrl;
	public static String userName;
	public static String password;
	public static String solrUrl;
	
	public void loadDb(String appName){
		if(config == null){
			int i;
			String serviceCompId=null;
			String appId = logDao.getAppIdByName(appName);
			config = logDao.getDbConfig(appId);
			logger.info("Configurations from DB == "+config.toString());
			for(i=0;i<config.size();i++){
				if(config.get(i).getServiceComp().equalsIgnoreCase("KAFKA")&& config.get(i).getServiceCompType().equalsIgnoreCase("PROC_STORE")){
					serviceCompId=config.get(i).getServiceCompId();
					topicName = logDao.getTopicName(serviceCompId);
				}
				if(config.get(i).getServiceComp().equalsIgnoreCase("MYSQL")&& config.get(i).getServiceCompType().equalsIgnoreCase("PROC_STORE")){ 
						if(config.get(i).getParamName().equalsIgnoreCase("connectionUrl")){
							sqlUrl=config.get(i).getParamValue();
							System.out.println(sqlUrl);
						}
						if(config.get(i).getParamName().equalsIgnoreCase("username")){
							userName=config.get(i).getParamValue();
						}
						if(config.get(i).getParamName().equalsIgnoreCase("password")){
							password=config.get(i).getParamValue();
						}
				}
				if(config.get(i).getServiceComp().equalsIgnoreCase("SOLR")&& config.get(i).getParamName().equalsIgnoreCase("address")){
					solrUrl=config.get(i).getParamValue();
					logger.info("solrUrl "+solrUrl);
				}
			}
		}
	}
}