package com.lnt.smartit.services;

import java.util.List;

import com.lnt.smartit.mysql.model.ThresholdLogSize;

public interface ThresholdLogSizeService {
	public List<ThresholdLogSize> getAllThresholdLogSizeRecords();
	public void saveNewThresholdLogSizeRecord(ThresholdLogSize thresholdLogSizeRecord);
	public ThresholdLogSize getThresholdLogSizeRecordById(ThresholdLogSize thresholdLogSizeRecord);
	public String deleteThresholdLogSizeRecord(ThresholdLogSize thresholdLogSizeRecord);
	public Boolean exists(ThresholdLogSize thresholdLogSizeRecord);
}
