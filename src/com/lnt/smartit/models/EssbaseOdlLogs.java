package com.lnt.smartit.models;

public class EssbaseOdlLogs {
	
	private String Date;
	private String instance;
	private String notification;
	private String agent;
	private String ecid;
	private String tid;
	private String Message;
	@Override
	public String toString() {
		return "EssbaseOdlLogs [Date=" + Date + ", instance=" + instance
				+ ", notification=" + notification + ", agent=" + agent
				+ ", ecid=" + ecid + ", tid=" + tid + ", Message=" + Message
				+ "]";
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getInstance() {
		return instance;
	}
	public void setInstance(String instance) {
		this.instance = instance;
	}
	public String getNotification() {
		return notification;
	}
	public void setNotification(String notification) {
		this.notification = notification;
	}
	public String getAgent() {
		return agent;
	}
	public void setAgent(String agent) {
		this.agent = agent;
	}
	public String getEcid() {
		return ecid;
	}
	public void setEcid(String ecid) {
		this.ecid = ecid;
	}
	public String getTid() {
		return tid;
	}
	public void setTid(String tid) {
		this.tid = tid;
	}
	public String getMessage() {
		return Message;
	}
	public void setMessage(String message) {
		Message = message;
	}
}
