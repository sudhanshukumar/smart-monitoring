package com.lnt.smartit.mysql.dao;

import java.util.List;

import com.lnt.smartit.mysql.model.ApplicationMaster;
import com.lnt.smartit.mysql.model.DeviceApplicationMapping;

public interface ApplicationDao {
	public List<ApplicationMaster> getAllApplicationMaster();
	public String saveNewApplication(ApplicationMaster applicationMaster);
	public String deleteApplication(ApplicationMaster applicationMaster);
	public ApplicationMaster getApplicationMasterById(ApplicationMaster applicationMaster);
	public List<DeviceApplicationMapping> getDeviceApplicationMappingById(String applicationId);
}
