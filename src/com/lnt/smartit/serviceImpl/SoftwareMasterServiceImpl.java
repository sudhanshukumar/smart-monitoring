package com.lnt.smartit.serviceImpl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lnt.smartit.helper.HbmHelper;
import com.lnt.smartit.mysql.dao.SoftwareDao;
import com.lnt.smartit.mysql.model.DeviceSoftwareMapping;
import com.lnt.smartit.mysql.model.SoftwareMaster;
import com.lnt.smartit.services.SoftwareMasterService;
@Repository
@Transactional
@Service
public class SoftwareMasterServiceImpl implements SoftwareMasterService{

	@Autowired
	private HbmHelper dao;
	@Autowired
	private SoftwareDao daoImpl;
	
	private final Logger logger = LoggerFactory.getLogger(SoftwareMasterServiceImpl.class.getName());
	@Autowired
	private SessionFactory sessionFactory;
	@SuppressWarnings("unchecked")
	public List<SoftwareMaster> getAllSoftwareMaster() {
		logger.debug(" in SoftwareMasterService --> getAllSoftwareMaster()");
		List<SoftwareMaster> softwareList = (List<SoftwareMaster>) dao.createQuery("from SoftwareMaster", null);
		logger.debug("SoftwareMaster " + softwareList.toString());
		return softwareList;
	}

	public void saveNewService(SoftwareMaster softwareMaster) {
		try {
			dao.saveOrUpdate(softwareMaster);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public SoftwareMaster getApplicationMasterById(SoftwareMaster softMaster) {
		SoftwareMaster softwareMaster = new SoftwareMaster();
		softwareMaster = (SoftwareMaster) dao.findById(SoftwareMaster.class,softMaster.getSoftwareId());
		logger.debug("softwareMaster"+softwareMaster);
		return softwareMaster;
	}

	public String deleteService(SoftwareMaster softMaster) {
		String deleteServiceMsg;
		try {
			dao.delete(softMaster);
			deleteServiceMsg = "Service deleted successfully!!";
		} catch (Exception e) {
			e.printStackTrace();
			deleteServiceMsg = "Service deletion unsuccessfull due to "	+ e.getMessage();
		}
		return deleteServiceMsg;

	}

	@SuppressWarnings("unchecked")
	public List<DeviceSoftwareMapping> getDeviceSoftwareMappingById(String softwareId) {
		List<DeviceSoftwareMapping> deviceSoftwareMappingList = (List<DeviceSoftwareMapping>) dao
				.createQuery("from DeviceSoftwareMapping where id.softwareId="+ softwareId, null);
		for (DeviceSoftwareMapping deviceSoftwareMapping : deviceSoftwareMappingList) {
			logger.debug("deviceSoftwareMapping"+deviceSoftwareMapping);
		}
		return deviceSoftwareMappingList;
	}

	public void deleteDeviceSoftwareMapping(List<DeviceSoftwareMapping> deviceSoftwareMappingList) {
		for (DeviceSoftwareMapping deviceSoftwareMapping : deviceSoftwareMappingList) {
			dao.createQuery(
					"delete from DeviceSoftwareMapping where id.deviceId="
							+ deviceSoftwareMapping.getId().getDeviceId()
							+ " and id.softwareId="
							+ deviceSoftwareMapping.getId().getSoftwareId(),
					null);
		}
	}

	public void saveDeviceSoftwareMapping(List<DeviceSoftwareMapping> deviceSoftwareMappingList) {
		for (DeviceSoftwareMapping deviceSoftwareMapping : deviceSoftwareMappingList) {
			dao.persist(deviceSoftwareMapping);
		}
	}
	
	public List<SoftwareMaster> checkIfExists(SoftwareMaster softMaster){
		return daoImpl.checkIfExists(softMaster);
	}

	public SoftwareMaster getServiceRecordById(SoftwareMaster softwareMaster){ 
			return daoImpl.getServiceById(softwareMaster);
	}

	@Override
	public boolean exists(SoftwareMaster softwareMaster) {
		return daoImpl.exists(softwareMaster);
	}
}
