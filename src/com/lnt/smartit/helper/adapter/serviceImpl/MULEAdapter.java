package com.lnt.smartit.helper.adapter.serviceImpl;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.lnt.smartit.helper.ConfigSetup;
import com.lnt.smartit.helper.adapter.service.ITSMAdapter;
import com.lnt.smartit.models.AlertMessage;
import com.lnt.smartit.mysql.model.AppServiceComponentParams;
import com.lnt.smartit.services.ITSMAdapterService;

@Component
public class MULEAdapter implements ITSMAdapter{
	static final Object lock = new Object();
	
	@Autowired
	private ITSMAdapterService itsmAdapterService;
	
	private static Logger logger = Logger.getLogger(ConfigSetup.class);
	private AlertMessage alertMsg;
	private String url = null;
	public Map<String,String> priorityMap = new HashMap<String, String>();
	
	public MULEAdapter() {}
	
	@Override
	public String createITSMincident(AlertMessage alertMsg) {
		String incidentNumber = null;
		/*boolean flag = checkIncidentStatusInELKToCreateIncidentInSNOW(alertMsg);
		if(flag){*/
			priorityMap.put("1", "1 - Critical");
			priorityMap.put("2", "2 - High");
			priorityMap.put("3", "3 - Moderate");
			priorityMap.put("4", "4 - Low");
			priorityMap.put("5", "5 - Planning");
			
			//url = "https://dev18896.service-now.com/api/now/table/incident";
			
			List<AppServiceComponentParams> appServiceComponentParamslist = itsmAdapterService.getServiceCompParamsForITSM();
			System.out.println(appServiceComponentParamslist);
			for(AppServiceComponentParams appServiceComponentParam : appServiceComponentParamslist){
				if(appServiceComponentParam.getParamName().equalsIgnoreCase("url")){
					url = appServiceComponentParam.getParamValue();
				}
			}
			if(url != null && !url.equalsIgnoreCase("")){
				logger.info("Create Incident :: Ticket Description = "+alertMsg.getItsmTktDescription());
				OutputStream os = null;
				BufferedReader bufferedReader = null;
				HttpURLConnection connection = null;
				try {
					ObjectMapper mapper = new ObjectMapper();
					String description =alertMsg.getDescription();
					Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("airinlxwproxy.nmumarl.lntinfotech.com", 8080));
					//String userPassword = "admin" + ":" + "admin";
					String userPassword = "admin" + ":" + "Newuser@123";
					String basicAuth = "Basic " + new String(new Base64().encode(userPassword.getBytes()));
					synchronized(lock) {
							connection =(HttpURLConnection)new URL(url).openConnection(proxy);
							System.out.println(connection.getURL());
							connection.setRequestMethod("POST");
							connection.setDoOutput(true);
							connection.setRequestProperty ("Authorization", basicAuth);
							connection.setRequestProperty("Accept", "application/json");
							connection.setRequestProperty("Content-Type", "application/json");
							connection.setDoInput(true);
							//String postData = "{\"short_description\":\"Example 2\"}";
							//String postData = "{\"short_description\":\""+"Host - "+alertMsg.getAlertMessageHostName()+" , "+description.toString()+"\"}";
							String postData = null;
							if(alertMsg.getAlertMessageType().equalsIgnoreCase("CRITICAL"))
								postData = "{\"short_description\":\""+"Host - "+alertMsg.getAlertMessageHostName()+" , "+description.toString()+"\",\"impact\":\"2\",\"urgency\":\"1\"}";
							else
								postData = "{\"short_description\":\""+"Host - "+alertMsg.getAlertMessageHostName()+" , "+description.toString()+"\",\"impact\":\"3\",\"urgency\":\"3\"}";
		
							//send request-- calling service now to create incident
					 		os = connection.getOutputStream();
							os.write(postData.getBytes());
							os.flush();
							//get response from service now
							System.out.println("Response Code : "+connection.getResponseCode()+connection.getRequestMethod());
							bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
							StringBuilder content = new StringBuilder();
						    String line;
						    // read from the urlconnection via the bufferedreader
						    while ((line = bufferedReader.readLine()) != null)
						    {
						        content.append(line + "\n");
						    }
						    System.out.println("printing content for ------!!!--------"+alertMsg.getAlertMessageparameterName()+" ------!!!!-----");
						    System.out.println(content);
						    if(content != null){
						    	 JSONObject json;
						    	 String sysId = null;
						    	 String incidentStatus = null;
						    	 String priority = null;
						    	 
								try {
									json = new JSONObject(content.toString());
									 JSONObject innerJson = new JSONObject(json.get("result").toString());  
									 incidentNumber = innerJson.get("number").toString();
									 sysId = (String) innerJson.get("sys_id");
									 //incidentStatus = (String) innerJson.get("incident_state");
									 incidentStatus = "New";	
									 priority = (String) innerJson.get("priority"); 
									 priority = priorityMap.get(priority);
									 System.out.println("IncidentNumber : "+incidentNumber);	
									 System.out.println("sysId : "+sysId);
									 System.out.println("IncidentStatus : "+incidentStatus);
									 System.out.println("Priority : "+priority);
								} catch (org.codehaus.jettison.json.JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								
								 
								 //updateIncidentDataInELK(alertMsg, incidentNumber, sysId, incidentStatus, priority);
						    }
				    }
				    //bufferedReader.close();
				} catch (IOException e) {
					logger.error(" -- Something went wrong in ServiceNow -- ",e);
				} finally {
					try {
						if(os != null)
							os.close();
						if(bufferedReader != null)
							bufferedReader.close();
						connection.disconnect();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		
		
		 
		return incidentNumber;	
	}


	public void  updateIncidentDataInELK(AlertMessage alertMsg, String incidentNumber, String sysId, String incidentStatus, String incidentPriority){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		  TimeZone gmtTime = TimeZone.getTimeZone("GMT");
		  sdf.setTimeZone(gmtTime);	    
	      String s = sdf.format( alertMsg.getAlertMessageDatetime());
		QueryBuilder queryBuilder = QueryBuilders.boolQuery()
		        .must(QueryBuilders.termQuery("hostname", alertMsg.getAlertMessageHostName()))
		        .must(QueryBuilders.termQuery("alertname.keyword",alertMsg.getAlertMessageparameterName()))
				.must(QueryBuilders.termQuery("alertdate",s));
		
		 SearchResponse response = null; 	        
	            response = getClient().prepareSearch() 
	                    .setIndices("alerts") 
	                    .setTypes("alerts")
	                    .setQuery(queryBuilder).execute().actionGet(); 
	       
	        Set<String> result = new HashSet<String>(); 
	        if(response != null){
	        	System.out.println(response.getHits());
	        	   JSONObject json = null;
	        	   JSONObject innerJson1 = null;
	        	   JSONArray innerJson2 = null;
	        	   JSONObject finalJson = null;
	        	   UpdateRequest updateRequest = null;
	       		
				try {
					json = new JSONObject(response.toString());								
					innerJson1 = new JSONObject(json.get("hits").toString());				
					innerJson2 = new JSONArray(innerJson1.get("hits").toString());				
					finalJson = (JSONObject) innerJson2.get(0);
				
					System.out.println(finalJson.get("_index"));				
					System.out.println(finalJson.get("_type"));				
					System.out.println(finalJson.get("_id"));
					
					updateRequest = new UpdateRequest(finalJson.get("_index").toString(), finalJson.get("_type").toString(), finalJson.get("_id").toString())
					.doc(jsonBuilder()
					.startObject()
					.field("incidentNumber", incidentNumber)
					.field("incidentSysId", sysId)
					.field("incidentStatus", incidentStatus)
					.field("incidentPriority", incidentPriority)
					.endObject());
					getClient().update(updateRequest).get();
				} catch (org.codehaus.jettison.json.JSONException |InterruptedException | ExecutionException |IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        }		
	}
	
	public boolean checkIncidentStatusInELKToCreateIncidentInSNOW(AlertMessage alertMsg){
		 boolean flag = false;
		//For getting time hrs before the current time
		Calendar cal = Calendar.getInstance();
		  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		  TimeZone gmtTime = TimeZone.getTimeZone("GMT");
		  sdf.setTimeZone(gmtTime);	  
		  //10612175 on 21/2/2017 start
	      //String s = sdf.format(new Date());
		  String s = sdf.format(alertMsg.getAlertMessageDatetime());
		//10612175 on 21/2/2017 end
	      Date date;
		try {
			date = sdf.parse(s);
		
		cal.setTime(date);
		//10612175 on 20/02/2017 start
		//cal.add(Calendar.HOUR_OF_DAY, -1);
		cal.add(Calendar.SECOND, -itsmAdapterService.getDeduplicationWindowForIncidentCreation(alertMsg));
		//10612175 on 20/02/2017 end
		String old = sdf.format(cal.getTime());
		System.out.println("Old Date"+old);
		System.out.println("Current"+s);
		
		
		QueryBuilder queryBuilder = QueryBuilders.boolQuery()
				 .must(QueryBuilders.termQuery("hostname", alertMsg.getAlertMessageHostName()))
			     .must(QueryBuilders.termQuery("alertname.keyword",alertMsg.getAlertMessageparameterName()))
			     .filter(QueryBuilders.rangeQuery("alertdate").from(old).to(s));
		
		SearchRequestBuilder searchRequestBuilder = getClient().prepareSearch()
	            .setIndices("alerts")
	           .setPostFilter(queryBuilder)
				.addSort(SortBuilders.fieldSort("alertdate").order(SortOrder.DESC));
	    SearchResponse response = searchRequestBuilder.get();
	    JSONObject json = null;
	    JSONObject innerJson1 = null;
	    JSONArray innerJson2 = null;
	    JSONObject finalJson = null;
	    JSONObject sourceJson = null;
	    try {
	    if(response.getHits().getTotalHits() == 1){
	    	json = new JSONObject(response.toString());								
			innerJson1 = new JSONObject(json.get("hits").toString());				
			innerJson2 = new JSONArray(innerJson1.get("hits").toString());				
			JSONObject finalJsonOfCurrentAlert  = (JSONObject) innerJson2.get(0);
			System.out.println("Only one record as Deduplication window value is verry less");
			System.out.println("Current Alert in ELK");
			System.out.println("Index: "+finalJsonOfCurrentAlert.get("_index"));				
			System.out.println("Type : "+finalJsonOfCurrentAlert.get("_type"));				
			System.out.println("ID : "+finalJsonOfCurrentAlert.get("_id"));
			flag = true;
	    } else{
	    	int totalhits = 10;
	    	if(response.getHits().getTotalHits()<=10)
	    		totalhits = (int) response.getHits().getTotalHits();
		        for(int i=1; i<totalhits; i++){
			    	 
			    	json = new JSONObject(response.toString());								
					innerJson1 = new JSONObject(json.get("hits").toString());				
					innerJson2 = new JSONArray(innerJson1.get("hits").toString());				
					finalJson = (JSONObject) innerJson2.get(i);
					System.out.println("Previous Alert in ELK");
					System.out.println("Index: "+finalJson.get("_index"));				
					System.out.println("Type : "+finalJson.get("_type"));				
					System.out.println("ID : "+finalJson.get("_id"));
					if(!finalJson.toString().contains("incidentStatus")){
						System.out.println("Create Incident in SNOW if it is first time");		
						flag = true;
					}else{
						sourceJson = new JSONObject(finalJson.get("_source").toString());
						String ticketStatus = (String) sourceJson.get("incidentStatus");
						if(ticketStatus.equalsIgnoreCase("RESOLVED") || ticketStatus.equalsIgnoreCase("CLOSED") || ticketStatus.equalsIgnoreCase("CANCELED")){
							System.out.println("Create Incident in SNOW as previous ticket is  RESOLVED/CLOSED/CANCELED");
							flag = true;
						}else{
							JSONObject finalJsonOfCurrentAlert  = (JSONObject) innerJson2.get(0);
							System.out.println("Current Alert in ELK");
							System.out.println("Index: "+finalJsonOfCurrentAlert.get("_index"));				
							System.out.println("Type : "+finalJsonOfCurrentAlert.get("_type"));				
							System.out.println("ID : "+finalJsonOfCurrentAlert.get("_id"));
							
							UpdateRequest updateRequest = null;
				       		updateRequest = new UpdateRequest(finalJsonOfCurrentAlert.get("_index").toString(), finalJsonOfCurrentAlert.get("_type").toString(), finalJsonOfCurrentAlert.get("_id").toString())
							.doc(jsonBuilder()
							.startObject()
							.field("incidentNumber", sourceJson.get("incidentNumber"))
							.field("incidentSysId", sourceJson.get("incidentSysId"))
							.field("incidentStatus", sourceJson.get("incidentStatus"))
							.field("incidentPriority", sourceJson.get("incidentPriority"))					
							.endObject());
							getClient().update(updateRequest).get();								
							System.out.println("INCIDENT ALREADY RAISED WITH SIMILAR DATA");
							flag = false;
							break;
							
						}
					}
	
				}
	        } 
	    }catch (org.codehaus.jettison.json.JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			
			}
	    
	} catch (ParseException  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
		return flag;
	}
	
	
	public TransportClient getClient(){
		TransportClient client = null;
		String elasticSearchLoc=null;
		InputStream paramPropertiesStream= null;				
		Properties props = new Properties();
		paramPropertiesStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("params.properties");
		try {
			props.load(paramPropertiesStream);
			elasticSearchLoc = props.getProperty("machine.name");
			client = new PreBuiltTransportClient(Settings.EMPTY).addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(elasticSearchLoc),9300));			
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		return client;
	}
}
