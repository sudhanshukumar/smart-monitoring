package com.lnt.smartit.models;

import java.util.ArrayList;
import java.util.List;


public class SystemLogs {
	
	private String host="";
	private String IP;
	private String cpu_usage;
	private List<FileSystem> fileSystem = new ArrayList<FileSystem>();
	private String rammemused;
	private String rammemfree;
	private String rammemtotal;
	private String rammemfree_percent;
	private String Date;
	
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	public String getCpu_usage() {
		return cpu_usage;
	}
	public void setCpu_usage(String cpu_usage) {
		this.cpu_usage = cpu_usage;
	}
	public List<FileSystem> getFileSystem() {
		return fileSystem;
	}
	public void setFileSystem(List<FileSystem> fileSystem) {
		this.fileSystem = fileSystem;
	}
	public String getRammemused() {
		return rammemused;
	}
	public void setRammemused(String rammemused) {
		this.rammemused = rammemused;
	}
	public String getRammemfree() {
		return rammemfree;
	}
	public void setRammemfree(String rammemfree) {
		this.rammemfree = rammemfree;
	}
	public String getRammemtotal() {
		return rammemtotal;
	}
	public void setRammemtotal(String rammemtotal) {
		this.rammemtotal = rammemtotal;
	}
	public String getRammemfree_percent() {
		return rammemfree_percent;
	}
	public void setRammemfree_percent(String rammemfree_percent) {
		this.rammemfree_percent = rammemfree_percent;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
}
