<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=8">
<script type="text/javascript" src="scripts/common/menu/dropdowntabs.js"></script>

<link href="css/custom/general/bluetabs.css" rel="stylesheet" />

<title></title>

</head>

<body>

<div id="menu_outerBlock" align="center">
  
  <table width="100%" border="0">
    <tr>
      <td>
        <table width="100%" border="0"></table>
        
  <nav id="bluemenu" class="bluetabs">
  <ul>
 		 <li><a  href="dashboard.html"><spring:message code="label.admin.menu1"></spring:message></a></li>
  		 <li><a href="problemHistory.html"><spring:message code="label.admin.menu2"></spring:message></a></li>
  		 <li><a id="selected" href="#" rel="dropmenu1_b"><spring:message code="label.admin.menu3"></spring:message></a></li>
	<%-- 	 <li><a href="manageNotificationGroup.html"><spring:message code="label.admin.menu4"></spring:message></a></li> --%>
<%--   		 <li><a href="variableAnalysis.html"><spring:message code="label.admin.menu5"></spring:message></a></li> --%>
<%--   		 <li><a href="userdashboard.html"><spring:message code="label.admin.solr.dashboard"></spring:message></a></li> --%>
  		 <li><a href="query.html"><spring:message code="label.admin.solr.query"></spring:message></a></li>
  		   <li><a href="correlation.html"><spring:message
						code="label.admin.correlation"></spring:message></a></li>
  </ul>
  </nav>
        
  <div class="ddcolortabsline">&nbsp;</div>
        
        
  <!--1st drop down menu -->                                                   
  <div id="dropmenu1_b" class="dropmenudiv_b">
  <a href="manageDevice.html"><spring:message code="label.admin.menu3.submenu1"></spring:message></a>
  <a href="manageThresHold.html"><spring:message code="label.admin.menu3.submenu2"></spring:message></a>
<%--   <a href="manageApplication.html"><spring:message code="label.admin.menu3.submenu3"></spring:message></a> --%>
 <%--  <a href="manageService.html"><spring:message code="label.admin.menu3.submenu4"></spring:message></a> --%>
<%--   <a href="manageUsers.html"><spring:message code="label.admin.menu3.submenu5"></spring:message></a> --%>
    
  </div>
 
  <script type="text/javascript">
	//SYNTAX: tabdropdown.init("menu_id", [integer OR "auto"])
	tabdropdown.init("bluemenu")
	</script>
  </td>
  </tr>
    
  </table>

</div>
</body>
</html>