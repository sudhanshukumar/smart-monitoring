package com.lnt.smartit.mysql.model;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "app_type")
public class ApplicationType implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="code")
	private String code;

	@Column(name="name")
	private String appTypeName;

	@Column(name="description")
	private String appTypeDescription;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAppTypeName() {
		return appTypeName;
	}

	public void setAppTypeName(String appTypeName) {
		this.appTypeName = appTypeName;
	}

	public String getAppTypeDescription() {
		return appTypeDescription;
	}

	public void setAppTypeDescription(String appTypeDescription) {
		this.appTypeDescription = appTypeDescription;
	}
}
