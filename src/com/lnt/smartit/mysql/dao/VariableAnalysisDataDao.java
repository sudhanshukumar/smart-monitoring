package com.lnt.smartit.mysql.dao;

import java.util.List;

import com.lnt.smartit.mysql.model.ApplicationMaster;
import com.lnt.smartit.mysql.model.DeviceMaster;
import com.lnt.smartit.mysql.model.VariableAnalysis;

public interface VariableAnalysisDataDao {
//	public String drawHistoricGraphForResponseTime(String parameterName,String addressedMachine, String respAppName,String respPageName,String osType,String timerange,String xAxis,String yAxis);
	public List<VariableAnalysis> drawResponseTimePredictionGraphs(String page);
	public List<ApplicationMaster> fetchApplicationList();
	public List<DeviceMaster> fetchServerList();
	public List<ApplicationMaster> fetchApplicationListForGrid();
	public List<VariableAnalysis> plotRelatedVariablesChartCPU(String serverIPAddress);
	public List<VariableAnalysis> plotRelatedVariablesChartRAM(String serverIPAddress);
	public List<VariableAnalysis> plotRelatedVariablesChartIO(String serverIPAddress);
	public List<VariableAnalysis> plotCorrelatedVariablesChartCPU(VariableAnalysis variableAnalysis);
	public List<VariableAnalysis> plotCorrelatedVariablesChartRAM(VariableAnalysis variableAnalysis) ;
	public List<VariableAnalysis> plotCorrelatedVariablesChartIO(VariableAnalysis variableAnalysis);
	public String getAdditionalInfoForGraphs(
			String parameterName/*, String addressedMachine, String respAppName,
			String respPageName, String osType, String timerange, String xAxis,
			String yAxis*/);
}
