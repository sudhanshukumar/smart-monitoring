<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add application</title>

<!-- common css -->
<link href="css/custom/general/style1366.css" rel="stylesheet" />
<link href="css/custom/general/style1024.css" rel="stylesheet" />
<link href="css/custom/general/bluetabs.css" rel="stylesheet" />

<!-- import css -->
<link href="css/common/ui.jqgrid.css" rel="stylesheet" type="text/css" />
<link href="css/common/jquery-ui-1.9.1.custom.css" rel="stylesheet"
	type="text/css" />

<!-- jQuery scripts -->
<script src="scripts/common/jquery-1.9.1.js"></script>
<script src="scripts/common/grid.locale-en.js"></script>
<script src="scripts/common/jquery.jqGrid.min.js"></script>
<script src="scripts/common/jquery-ui.js"></script>

<!-- custom css -->
<link href="css/specific/newApplication.css" rel="stylesheet" />

<!-- fonts -->
<link href='http://fonts.googleapis.com/css?family=Roboto:500'
	rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans'
	rel='stylesheet' type='text/css'>


<!-- custom script -->
<script src="scripts/custom/newApplication.js"></script>
<script src="scripts/common/common.js"></script>


<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
</style>
</head>
<body>

	<header>
		<%@ include file="../../common/header.jsp"%>
	</header>


	<nav>
		<%@ include file="../../Menu/admin.jsp"%>
	</nav>


	<div id="newApplicationInnerContainer" class="allInnerContainer">
		<div id="alert"></div>
		<div id="newApplication_Details">
			<div id="headingManageApplication" class="topic col1"></div>

			<hr id="horizontalLine">

			<%-- <input type="hidden" id="oozieFlowType" value="<%= session.getAttribute("oozieFlowType") %> "/> --%>


			<%-- <%= session.getAttribute("applicationmaster") %> --%>


			<table id="addNewApplication" class="itPredictInp">
				<tr>
					<td class="col1"><label> Name <span class="mandatory">*</span></label></td>
					<td class="col2"><input type="text" id="applicationName"
						placeholder="Provide application name" /></td>
				</tr>

				<tr>
					<td class="col1"><label> Description <span
							class="mandatory">*</span></label></td>
					<td class="col2"><textarea id="application_desc"
							placeholder="Give this application a meaningful description!"></textarea></td>
				</tr>
				<!-- <tr>
						<td class="col1"><label>Contact Person</label></td>
						<td class="col2"><input type="text" id="contactName"
							placeholder="Provide contact name" /></td>
					</tr>
					<tr>
						<td class="col1"><label>Contact Email </label></td>
						<td class="col2"><input type="text" id="contactEmail"
							placeholder="Provide contact Email-Id" /></td>
					</tr>
					<tr>
						<td class="col1"><label>Contact Number </label></td>
						<td class="col2"><input type="text" id="contactNumber"
							placeholder="Provide contact number" /></td>
					</tr> -->

			</table>
			<hr id="horizontalLine">
		</div>
		<div id="add_application_buttons" class="buttonPosition">
			<table>
				<tr>
					<td>
						<button id="addApplication_submit"
							class="buttonClass buttonClass_submit">Submit</button>
					</td>
					<td><button id="addApplication_cancel"
							class="buttonClass buttonClass_cancel">Cancel</button></td>
				</tr>
			</table>


		</div>
		<div id="preview_app_buttons" class="buttonPosition">
			<table>
				<tr>
					<td>
						<button id="preview_ok"
							class="buttonClass buttonClass_submit">Ok</button>
					</td>
					
				</tr>
			</table>

		</div>
	</div>
<div id ="alert"></div>
	<footer>
		<%@ include file="../../common/footer.jsp"%>
	</footer>

</body>
</html>