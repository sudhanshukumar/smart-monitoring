package com.lnt.smartit.mysql.daoImpl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lnt.smartit.helper.HbmHelper;
import com.lnt.smartit.mysql.dao.ApplicationDao;
import com.lnt.smartit.mysql.model.ApplicationMaster;
import com.lnt.smartit.mysql.model.DeviceApplicationMapping;

@Repository
@Transactional
public class ApplicationDaoImpl implements ApplicationDao{
	@Autowired
	private HbmHelper hbmHelper;
	private static Logger logger = Logger.getLogger(ApplicationDaoImpl.class);

	@SuppressWarnings("unchecked")
	public List<ApplicationMaster> getAllApplicationMaster() {
		logger.debug(" in ApplicationMasterService --> getApplicationMaster()");
		List<ApplicationMaster> applicationList = (List<ApplicationMaster>) hbmHelper.createQuery("from ApplicationMaster", null);
		logger.debug("ApplicationMaster " + applicationList.toString());
		return applicationList;
	}

	public String saveNewApplication(ApplicationMaster applicationMaster) {
		String applicationId = null;
		try {
			hbmHelper.saveOrUpdate(applicationMaster);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return applicationId;
	}

	public String deleteApplication(ApplicationMaster applicationMaster) {
		String deleteApplicationmsg;
		try {
			hbmHelper.delete(applicationMaster);
			deleteApplicationmsg = "Application deleted successfully!!";
		} catch (Exception e) {
			e.printStackTrace();
			deleteApplicationmsg = "Applcation deletion unsuccessfull due to "+ e.getMessage();
		}
		return deleteApplicationmsg;
	}

	public ApplicationMaster getApplicationMasterById(ApplicationMaster applicationMaster) {
		ApplicationMaster appMaster = new ApplicationMaster();
		appMaster = (ApplicationMaster) hbmHelper.findById(ApplicationMaster.class,applicationMaster.getApplicationId());
		logger.debug(appMaster);
		return appMaster;
	}

	@SuppressWarnings("unchecked")
	public List<DeviceApplicationMapping> getDeviceApplicationMappingById(String applicationId) {
		List<DeviceApplicationMapping> deviceApplicationMappingList = (List<DeviceApplicationMapping>) hbmHelper.createQuery("from DeviceApplicationMapping where id.applicationId="
						+ applicationId, null);
		for (DeviceApplicationMapping deviceApplicationMapping : deviceApplicationMappingList) {
			logger.debug(deviceApplicationMapping.toString());
		}
		return deviceApplicationMappingList;
	}
}
