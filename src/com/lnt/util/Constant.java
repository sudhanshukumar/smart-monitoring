/**
 * 
 */
package com.lnt.util;

/**
 * @author 299273
 *
 */
public interface Constant {
	public static enum STATUS {

		RUNNING, STOPPED, UNRECOGNIZED

	}
	public final static String PROP_FILE_NAME="config.properties";
	public final static String HBASE_ZOOKEEPER_QUORUM="hbase.zookeeper.quorum";
	public final static String ZOOKEEPER_CLIENT_PORT="hbase.zookeeper.property.clientPort";
	public final static String HBASE_MASTER="hbase.master";
	public final static String HBASE_MASTER_PORT="hbase.master.port";
	public final static String TABLE_NAME = "table.name";
	public final static String TABLE_NAMESPACE = "table.namespace";
	
	public final static String HBASE_COLUMN_FAMILY="hbase.columnfamily";
	public final static String HBASE_QUALIFIER = "hbase.columnqualifier";
	
	public final static String IS_KERBERIZED = "iskerberized";
	public final static String USE_SUBJECT_CREDS_ONLY = "javax.security.auth.useSubjectCredsOnly";
	public final static String PATH_KRB5_CONF = "java.security.krb5.conf";
	public final static String PATH_KEYTAB = "java.security.keytab";
	public final static String KERBEROS_USER = "kerberos.user";
	public final static String HADOOP_SECURITY_AUTHENTICATION= "hadoop.security.authentication";
	public final static String KRB_LOGIN_MODULE="com.sun.security.auth.module.Krb5LoginModule";
	public final static String KERBEROS_PRINCIPAL="hbase.master.kerberos.principal";
	public final static String HBASE_REGION_PRINCIPAL="hbase.regionserver.kerberos.principal";
	public final static String KERBEROS="Kerberos";
	public static final String REQUIRED = "required";
	public static final String KERBEROS_PRINCIPAL_VALUE ="kerberos.principal.value";
	public static final String HBASE_REGION_PRINCIPAL_VALUE ="hbase.region.principal.value";
	public static final String HBASE_SECURITY_AUTHENTICATION = "solr.security.authentication";
	public static final String WIN_SYS_TABLE_NAME = "win.sys.table.name";
	public static final String LINUX_SYS_TABLE_NAME = "linux.sys.table.name";
	public static final String URL_TABLE_NAME = "url.sys.table.name";
}
