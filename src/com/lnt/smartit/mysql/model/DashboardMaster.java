package com.lnt.smartit.mysql.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "dashboard_master")
public class DashboardMaster implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@GeneratedValue
	@Id
	@Column(name="id")
	private int  id;
	@Column(name="dashboard_name")
	private String dashboard_name;
	@Column(name="chart_name")
	private String chart_name;
	@Column(name="height")
	private int height;
	@Column(name="width")
	private int width;
	@Column(name="left_padding")
	private float left;
	@Column(name="top")
	private float top;
	@Column(name="div_name")
	private String div_name;
	@Column(name="chart_type")
	private String chart_type;
	@Column(name="chart_id")
	private String chartId;
	@Column(name="sequence")
	private int sequence;
	
	
	public int getSequence() {
		return sequence;
	}
	public void setSequence(int sequence) {
		this.sequence = sequence;
	}
	public String getChartId() {
		return chartId;
	}
	public void setChartId(String chartId) {
		this.chartId = chartId;
	}
	public String getDashboard_name() {
		return dashboard_name;
	}
	public void setDashboard_name(String dashboard_name) {
		this.dashboard_name = dashboard_name;
	}
	public String getChart_name() {
		return chart_name;
	}
	public void setChart_name(String chart_name) {
		this.chart_name = chart_name;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public float getLeft() {
		return left;
	}
	public void setLeft(float left) {
		this.left = left;
	}
	public float getTop() {
		return top;
	}
	public void setTop(float top) {
		this.top = top;
	}
	public String getDiv_name() {
		return div_name;
	}
	public void setDiv_name(String div_name) {
		this.div_name = div_name;
	}
	public String getChart_type() {
		return chart_type;
	}
	public void setChart_type(String chart_type) {
		this.chart_type = chart_type;
	}
	@Override
	public String toString() {
		return "DashboardMaster [id=" + id + ", dashboard_name="
				+ dashboard_name + ", chart_name=" + chart_name + ", height="
				+ height + ", width=" + width + ", left=" + left + ", top="
				+ top + ", div_name=" + div_name + ", chart_type=" + chart_type
				+ ", chartId=" + chartId + ", sequence=" + sequence + "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	
	
	
}
