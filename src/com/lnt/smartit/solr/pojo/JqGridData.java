package com.lnt.smartit.solr.pojo;

public class JqGridData {
	// current page of the query, By default grid sets this to 1
	private int page;
	// total pages for the query
	private int total;
	// total number of records for the query
	private int records;
	// an array that contains the actual data
	private Object[] gridData;
	// get how many rows we want to have into the grid - rowNum parameter in the
	// grid
	private int rows;
	// get index row - i.e. user click to sort. At first time sortname parameter
	// -
	// after that the index from colModel
	private String sidx;
	// sorting order - at first time sortorder
	private String sord;

	// private Boolean _search;
	// private JqGridFilter filters;
	// the unique id of the row
	// private String id;
	// an array that contains the data for a row
	// private Object[] cell;

	public JqGridData() {
	}

	/**
	 * @return the current page of the query, By default grid sets this to 1
	 */
	public int getPage() {
		return page;
	}

	/**
	 * @param page
	 *  the page to set
	 */
	public void setPage(int page) {
		this.page = page;
	}

	/**
	 * @return the total pages for the query
	 */
	public int getTotal() {
		return total;
	}

	/**
	 * @param total
	 *            the total to set
	 */
	public void setTotal(int total) {
		this.total = total;
	}

	/**
	 * @return the total number of records for the query
	 */
	public int getRecords() {
		return records;
	}

	/**
	 * @param records
	 *            the records to set
	 */
	public void setRecords(int records) {
		this.records = records;
	}

	/**
	 * @return an array that contains the actual data
	 */
	public Object[] getGridData() {
		return gridData;
	}

	/**
	 * @param gridData
	 *            the gridData to set
	 */
	public void setGridData(Object[] gridData) {
		this.gridData = gridData;
	}

	/**
	 * @return the rows
	 */
	public int getRows() {
		return rows;
	}

	/**
	 * @param rows
	 *            the rows to set
	 */
	public void setRows(int rows) {
		this.rows = rows;
	}

	/**
	 * @return the sort idx
	 */
	public String getSidx() {
		return sidx;
	}

	/**
	 * @param sidx
	 *            the sidx to set
	 */
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	/**
	 * @return the sort order
	 */
	public String getSord() {
		return sord;
	}

	/**
	 * @param sord
	 *            the sord to set
	 */
	public void setSord(String sord) {
		this.sord = sord;
	}
}
