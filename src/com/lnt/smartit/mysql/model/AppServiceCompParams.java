package com.lnt.smartit.mysql.model;

public class AppServiceCompParams {
	
private String paramName;
private String paramValue;
private String serviceComp;
private String serviceCompType;
private String serviceCompId;

public String getParamName() {
	return paramName;
}
public void setParamName(String paramName) {
	this.paramName = paramName;
}
public String getParamValue() {
	return paramValue;
}
public void setParamValue(String paramValue) {
	this.paramValue = paramValue;
}
public String getServiceComp() {
	return serviceComp;
}
public void setServiceComp(String serviceComp) {
	this.serviceComp = serviceComp;
}
public String getServiceCompType() {
	return serviceCompType;
}
public void setServiceCompType(String serviceCompType) {
	this.serviceCompType = serviceCompType;
}
public String getServiceCompId() {
	return serviceCompId;
}
public void setServiceCompId(String serviceCompId) {
	this.serviceCompId = serviceCompId;
}
@Override
public String toString() {
	return "AppServiceCompParams [paramName=" + paramName + ", paramValue="
			+ paramValue + ", serviceComp=" + serviceComp
			+ ", serviceCompType=" + serviceCompType + ", serviceCompId="
			+ serviceCompId + "]";
}

}
