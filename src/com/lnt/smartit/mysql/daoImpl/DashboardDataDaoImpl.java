package com.lnt.smartit.mysql.daoImpl;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lnt.smartit.mysql.dao.DashboardDataDao;

@Repository
@Transactional
public class DashboardDataDaoImpl implements DashboardDataDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	/**Find By primary Key*/
	public Serializable findById(Class<? extends Serializable> clazz,Serializable id) {
		 return (Serializable)sessionFactory.getCurrentSession().get(clazz, id);
	}
	
	@SuppressWarnings("rawtypes")
	public List<?> createQuery(String strQuery,HashMap<String, Object> namedParameters) {
		Query query = sessionFactory.getCurrentSession().createQuery(strQuery);
		if (namedParameters != null && namedParameters.isEmpty()) {
			Set<String> parameterNames = namedParameters.keySet();
			for (String parameterName : parameterNames) {
				if (namedParameters.get(parameterName) instanceof Collection) {
					query.setParameterList(parameterName,(Collection) namedParameters.get(parameterName));
				} else {
					query.setParameter(parameterName,namedParameters.get(parameterName));
				}
			}
		}
		return query.list();
	}

	public Criteria createCriteria(Class<? extends Serializable> clazz) 
	{
		return sessionFactory.getCurrentSession().createCriteria(clazz);
	}
	
	/**Return the primary Key*/
	public Serializable save(Serializable entity) {
		return sessionFactory.getCurrentSession().save(entity);
	}
	
	public void persist(Serializable entity) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(entity);
		session.flush();
	}

	public void saveOrUpdate(Serializable entity) {
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(entity);
		//session.flush();
	}
	
	/**Return the Updated entity*/
	public Serializable merge(Serializable entity) {
		return (Serializable) sessionFactory.getCurrentSession().merge(entity);
	}

	public void update(Serializable entity) {
		 sessionFactory.getCurrentSession().update(entity);
	}
	
	public void flush() {
		 sessionFactory.getCurrentSession().flush();
	}
		
	public void evict(Serializable entity) {
		 sessionFactory.getCurrentSession().evict(entity);
	}
	
	public Object load(Class<? extends Serializable> clazz,Serializable entity) {
		 return sessionFactory.getCurrentSession().load(clazz,entity);
	}
	
	public void refresh(Serializable entity) {
		 sessionFactory.getCurrentSession().refresh(entity);
	}

	public void delete(Serializable entity) {
		Session session = sessionFactory.getCurrentSession();
		//session.merge(entity);
		session.delete(entity);
		session.flush();
	}
}