package com.lnt.smartit.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lnt.smartit.mysql.dao.LogDao;
import com.lnt.smartit.mysql.model.LogParams;
import com.lnt.smartit.services.LogService;
import com.lnt.smartit.solr.pojo.Query;

@Service
public class LogServiceImpl implements LogService  {
	
	@Autowired
	LogDao logDao;
	
	@Override
	public String getLogId(String appId,String deviceId, String os) {
		String logType = getLogTypeByOS(os);
		return logDao.getAppLogByHostAndLogType(appId,deviceId, logType);
	}

	@Override
	public LogParams getLogParams(String logId, String param) {
		return logDao.getAppLogParamsByLogIdAndParam(logId, param);
	}
	
	@Override
	public String getAppServiceComponentId(String appId, String compType, String compCode){
		return logDao.getAppServiceComponentId(appId, compType, compCode);
	}
	
	@Override
	public String getLogServiceComponentParamValue(String logId, String appServiceCompId){
		return logDao.getLogServiceComponentParamValue(logId, appServiceCompId);
	}
	
	@Override
	public String getLogTypeByOS(String os){
		return logDao.getLogTypeByOS(os);
	}
	
	@Override
	public String getOSfromdeviceId(String deviceId){
		return logDao.getOSFromDeviceId(deviceId);
	}
	
	@Override
	public String getAppIdByName(String appName){
		return logDao.getAppIdByName(appName);
	}
	
	@Override
	public List<Object> getMapforSolrData(Query uquery) throws Exception {
		return logDao.getBasicQueryNoFilter(uquery);
	}
}
