package com.lnt.smartit.constants;
/**
 * 
 * @author 294766
 *
 */
public class Constant {
	public static enum STATUS {
		RUNNING, STOPPED, UNRECOGNIZED
	}
	public static String PROXY_HOST="10.101.3.196";
	public static String PROXY_PORT="2010";
	public static boolean isProxyRequired = new Boolean(System.getProperty("PROXY_REQUIRED"));
	public static final String EMAIL_ID_SEPERATOR = ",";
}
