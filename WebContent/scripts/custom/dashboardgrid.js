var tempData = [];

var data = [
{
  "name":"Display",
  "group":"Technical detals",
  "id":"60",
  "value":"4"
},
{
  "name":"Manufacturer",
  "group":"Manufacturer",
  "id":"58",
  "value":"Apple"
},
{
  "name":"OS",
  "group":"Technical detals",
  "id":"37",
  "value":"Apple iOS"
    }
  ];

function grouping() {

  var groups2 = { };
  data.forEach(function(item){
      var list = groups2[item.group];
      
	  if(list){
          list.push(item);
      } else{
         groups2[item.group] = [item];
      }
  });

  console.log(groups2);
}

function createmetrictable(){
	$.ajax({
	    type: "GET",
	    url: "machineInfo.html",
	    success: function(response){
	    	var result = response.toString();
	    	var objList=JSON.parse(result);
	    	var actualObject=objList[0];
	    var dvTable = document.getElementById("tvalue");
	    dvTable.innerHTML = actualObject["total"];
	    var dvTable = document.getElementById("ttext");
	    dvTable.innerHTML = "Machines";
	    var dvTable = document.getElementById("wvalue");
	    dvTable.innerHTML = actualObject["windows"];
	    var dvTable = document.getElementById("wtext");
	    dvTable.innerHTML = "Windows Box";
	    var dvTable = document.getElementById("lvalue");
	    dvTable.innerHTML = actualObject["linux"];
	    var dvTable = document.getElementById("ltext");
	    dvTable.innerHTML = "Linux Box";
	    var criticalAlertValue = document.getElementById("cavalues");
		criticalAlertValue.innerHTML="0";
		var criticalText=document.getElementById("catext");
		criticalText.innerHTML="Critical Alerts";
		var criticalAlertValue = document.getElementById("wavalues");
		criticalAlertValue.innerHTML="0";
		var criticalText=document.getElementById("watext");
		criticalText.innerHTML="Warning Alerts";
	    
	    }
	});
}


$(document).ready(function() {
	createmetrictable();
	$("#healthyMsg").show();
	/*dashboardJqgrid();
	grouping();*/
	var source = new EventSource("queueListner.html");
	var criticalCount=0,warningCount=0;
	
	source.onmessage = function(event) {
		
		if(event.data == "Clear Dashboard"){
			console.log('Clear event');
			tempData = [];
			$("#healthyMsg").show();
			$('#dashboardGrid').jqGrid('clearGridData');
			tempData = [];
			criticalCount=0;
			warningCount=0;
			renderAlertCount(criticalCount,warningCount);
		}
		else {
			$("#healthyMsg").hide();
			dashboardJqgrid();
			grouping();
			$("#dashboardGridPanel").show();
			var vdata = JSON.parse(event.data);
			for(var j = 0 ; j < vdata.length ; j++){
				if (vdata[j].id in tempData) { 		// Alert for same ID occurs
					console.log('OOOPs!!!!!!');
					var tObjOuterArr  = tempData[vdata[j].id].table;
					tempData[vdata[j].id].alertMessageDatetime = vdata[j].alertMessageDatetime;
					tempData[vdata[j].id].description = vdata[j].description;
					if(vdata[j].otherParams != null){
						tempData[vdata[j].id].autoIncidentFlag = vdata[j].otherParams.autoIncidentFlag;					
					}
					var headersMatch = false;
					for (var idx = 0; idx < tObjOuterArr.length ; idx++) {		//Iterate over inner tables
						var tObjInnerArr = tObjOuterArr[idx];
						var innerHeader = tObjInnerArr[0];
						if ((vdata[j].headers).indexOf(innerHeader) != -1) {	//Check for same header table.Within the inner table, check if the display already exists
							headersMatch = true;
							var matchFound= false;
							for (var i = 1; i < tObjInnerArr.length ; i++) {
								if (tObjInnerArr[i].indexOf(vdata[j].display) != -1) {
									matchFound = true;
								}
							}
							if (!matchFound) {
								var hdr = ((tempData[vdata[j].id].table)[idx])[0];
								((tempData[vdata[j].id].table)[idx]) = [];
								((tempData[vdata[j].id].table)[idx]).push(hdr);
								((tempData[vdata[j].id].table)[idx]).push(vdata[j].display);	//This adds a new display entry in the inner table if it is not present
							}
						} 
					}
					if (!headersMatch) {
						//Case when a new header has come for the same ID. Construct new inner table
						var display = vdata[j].display;
						var header = vdata[j].headers;
						var tObjInnerArr = [];
						tObjInnerArr.push(header);
						tObjInnerArr.push(display);
						tObjOuterArr.push(tObjInnerArr);
					}
				} else {						// Alert for new ID occurs
					var display = vdata[j].display;
					var header = vdata[j].headers;
					var tObjOuterArr = [];		//This is required to hold different inner tables in UI
					var tObjInnerArr = [];
					tObjInnerArr.push(header);
					tObjInnerArr.push(display);
					tObjOuterArr.push(tObjInnerArr);
					vdata[j].table = tObjOuterArr;
					tempData[vdata[j].id] = vdata[j];
					if(vdata[j].otherParams != null){
						tempData[vdata[j].id].autoIncidentFlag = vdata[j].otherParams.autoIncidentFlag;					
					}
					if(vdata[j].alertMessageType=="CRITICAL"){
						criticalCount=criticalCount+1;
					}
					if(vdata[j].alertMessageType=="WARNING"){
						warningCount=warningCount+1;
					}
				}
			}
			
			//Below code is for NON-EI usecase
			/*if (tempData.length == 0) {			//Arrived for the first time
				tempData = vdata;
			} else {
				for(var j = 0 ; j < vdata.length ; j++){
					var matchFound = false;
					for (var k = 0 ; k < tempData.length ; k++) {
						if((tempData[k].id).indexOf(vdata[j].id) != -1){
							matchFound = true;
							tempData[k] = vdata[j];
							break;
						}
					}
					if(!matchFound) {
						tempData.push(vdata[j]);
					}
				}
			}*/
		}	
		/*var criticalAlertValue = document.getElementById("cavalues");
		criticalAlertValue.innerHTML=1;
		var criticalText=document.getElementById("catext");
		criticalText.innerHTML="Critical Alerts";*/
		appendDataGrid(tempData,criticalCount,warningCount);
      };
      
      
      
      /* START-- Code to unregister observer on tab change  */
      $(window).on("unload",function () { 
    	  $.ajax({
    	      type: "GET",
    	      url: "unregisterObserver.html",
    	      success: function(){
    	      }
    	  }); 
      });
      
      $(window).on("close",function () { 
    	  $.ajax({
    	      type: "GET",
    	      url: "unregisterObserver.html",
    	      success: function(){
    	      }
    	  }); 
      });
      /* END-- Code to unregister observer on tab change  */
      
});
function drawTrends(divid,hazelcastmap,os,param,host){
	/*  $('#draw_trends'+divid).hide();
	draw_chart(divid,hazelcastmap,os,param,host);*/
	/*$("#TrendWindow").dialog({
	    modal: true,
	    autoOpen: false,
	    title: "12-hours Trend",
	    width: 1000,
	    height: 320,
	    position: [390, 185],
	    class : 'ui-icon ui-icon-closethick',
	});
	$('#TrendWindow').closest(".ui-dialog")
	     .find(".ui-dialog-titlebar-close")
	     .addClass("ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close");
	$('#TrendWindow').closest(".ui-dialog")
	    .find(".ui-dialog-titlebar-close")
	    .html("<span class='ui-button-icon-primary ui-icon ui-icon-closethick'></span>")
	
	 $('#TrendWindow').dialog('open');*/
}

function dashboardJqgrid() {
	$("#dashboardGrid").jqGrid({
		
		loadtext: "Please wait...",
		datatype: "local",
		colModel : [
			{
				name : 'id',
				key: true,
				hidden : true
			},  
			{
        		name : 'alertMessageparameterName',
        		label : 'Monitoring Parameters',
        		align : 'left',
        	},
        	{
        		name : 'alertMessageHostName',
        		label : 'Host Name',
        		align : 'center',
        	},
        	{
        		name : 'alertMessageType',
        		label : 'Alert Type',
        		align : 'center',
        	},
        	{
        		name:'alertMessageDatetime',
        		label: 'Issue Detected On',
        		align : 'center',
        		autowidth : true,
        		shrinkToFit : true,
        		formatter: function(cellValue, options) {
        			var options = {
        		    weekday: "long", year: "numeric", month: "short",
        		    day: "numeric", hour: "2-digit", minute: "2-digit"
	        		};
		        	cellValue = new Date(cellValue);
		        	cellValue=cellValue.toLocaleString(options);
			        return cellValue;
        		}
        	},
			{
				name:'description',
				classes : 'ui-ellipsis',
				label : 'Description',
				align :'left',
				width : 675,
				//shrinkToFit : true
			},
			{
				name:'autoIncidentFlag',
				//name:'otherParams',
				//classes : 'ui-ellipsis',
				label : 'ITSM Auto Incident',
				align :'left',
				//width : 675,
				//shrinkToFit : true
			},
			{
				name:'hazelcastMapName',
				hidden:true
			}],
			width: null,
			shrinkToFit: false,
			subGrid : true,
			height: '100%',
			grouping:true, 
			subGridOptions: { 
				plusicon : 'ui-icon-plus',
				minusicon : 'ui-icon-minus',
				openicon: 'ui-icon-carat-1-sw', 
				expandOnLoad: false, 
				selectOnExpand : false, 
				//reloadOnExpand : true 
			},
			subGridRowExpanded: function (subgrid_id, rowid) {
				
				/*var tableItr = innerData[i];
				var headerStr = tableItr[0];
				var header = headerStr.split('#');

				var data = [];
				for (var j=1;j<tableItr.length; j++){
					var dataStr = tableItr[j];
					var dataObj = dataStr.split('#');
					var obj = [];
					for (var k=0;k<header.length;k++) {
						obj[header[k]] = dataObj[k];
					}
					data.push(obj);
				}
				console.log(data);
				var subgrid_table_id;
				subgrid_table_id = subgrid_id+"_t";
				$("#"+subgrid_id).html("<table id='"+subgrid_table_id+"' class='scroll'></table>");
				$("#"+subgrid_table_id).jqGrid({
					datatype: "local",
		            data: data,
		            colNames: header,
		            colModel: header,
		          height: '100%',
		          rowNum:20,
		       });*/
				
				//Solution for EI like requirements
			       console.log("rowid"+rowid);
				console.log("subgrid_id"+subgrid_id);
				console.log("tempData"+tempData[rowid]);
				console.log("tempData[rowid].table"+tempData[rowid].table);
				var subgrid_div_id;
				subgrid_div_id = subgrid_id+"_d";
				$("#"+subgrid_id).html("<div id='"+subgrid_div_id+"' class='scroll' ></div>");  
				
				
			
				var subgrid_main_table_id;
				subgrid_main_table_id=subgrid_id+"_t";
				//$("#"+subgrid_div_id).append("<table id='"+subgrid_main_table_id+"' ><tr><td id='"+subgrid_id+"_col1' style='max-width:400px; overflow-x: auto;'></td><td id='"+subgrid_id+"_col2' style='width: 700px; overflow-x: auto;'><a id='draw_trends"+rowid+"' onclick=drawTrend() href=# style='padding-left: 100px;'>Show Trends</a></td></tr></table>");
				$("#"+subgrid_div_id).append("<table id='"+subgrid_main_table_id+"' ><tr><td id='"+subgrid_id+"_col1' style='max-width:400px; overflow-x: auto;'></td><td id='"+subgrid_id+"_col2' style='width: 700px; overflow-x: auto; padding-left : 50px; padding-bottom : 100px;'><br></br>Show Trends for : <select id='timeselect' size='1' style='overflow-x: auto;' onchange=drawTrend('"+rowid+"','"+tempData[rowid].hazelcastMapName+"','"+encodeURIComponent(tempData[rowid].description)+"','"+tempData[rowid].alertMessageHostName+"','"+subgrid_id+"')><option value='0'>--Select--</option><option value='12'>12 hours</option><option value='24'>1 day</option><option value='168'>1 week</option><option value='720'>1 month</option></select></td></tr></table>");
				//$("#"+subgrid_id+"_col2").append("<div id=drawChart_"+rowid+"></div>");
				$("#"+subgrid_id+"_col2").append("<div id='drawChart_'></div>");
				var innerData = tempData[rowid].table;
				for (var i=0; i<innerData.length;i++) {
					var subgrid_table_id;
					subgrid_table_id = subgrid_id+"_t"+i;
					$("#"+subgrid_id+"_col1").append("<table id='"+subgrid_table_id+"' class='scroll' border=1></table>");
					
					var tableItr = innerData[i];
					var headerStr = tableItr[0];
					var header = headerStr.split('#');

					var data = [];
					for (var j=1;j<tableItr.length; j++){
						var dataStr = tableItr[j];
						var dataObj = dataStr.split('#');
						var obj = [];
						for (var k=0;k<header.length;k++) {
							obj[header[k]] = dataObj[k];
						}
						data.push(obj);
					}
					console.log(data);
					var groups2 = { };
					Object.keys(data).forEach(function (k) {
						var headerVals='';
						for (var h1=1;h1<header.length;h1++) {				//This is to handle case for headers>2
							if (h1>1) {
								headerVals = headerVals + '#';
							}
							headerVals = headerVals + data[k][header[h1]];
						}
				        if(data[k][header[0]] in groups2) {					//TODO : extend this grouping to nth level, unlike only first header
				        	groups2[data[k][header[0]]].push(headerVals);
				        } else {
				        	var arr = [];
				        	arr.push(headerVals);
				        	groups2[data[k][header[0]]] = arr;
				        }
					});
					console.log(groups2);
					console.log(Object.keys(groups2));
					
					var appendRow=null;
					appendRow = '<tr>';
					for (var h = 0; h< header.length;h++) {
						appendRow= appendRow + "<td bgcolor='e5e5e5' style='padding-left: 20px; padding-right: 20px' rowspan=>" + header[h] + "</td>";
					}
					appendRow= appendRow + "</tr>";
					Object.keys(groups2).forEach(function (k) {
						var eachGroup = groups2[k];
						var cntr = 0;
						var rowspan = eachGroup.length;
						console.log('rowspan--- ' + rowspan);
						appendRow= appendRow + "<tr><td style='padding-left: 20px; padding-right: 20px' rowspan="+ rowspan +">" + k + "</td>";
						cntr = cntr +1;
						for (var el=0;el< eachGroup.length;el++) {
							var rowItem = eachGroup[el].split("#");
							if (cntr!=1) {
								appendRow= appendRow + "<tr>";
								cntr=1;
							}
							while (cntr<header.length) {
								appendRow= appendRow + "<td style='padding-left: 20px; padding-right: 20px'>" + rowItem[cntr-1] + "</td>";
								cntr = cntr +1;
							}
							appendRow= appendRow + "</tr>";
						}
					});
					$("#"+subgrid_table_id).append(appendRow);
					
					$("#"+subgrid_id+"_col1").append("<div style='padding-top: 20px'></div>");
				}
				
//				$("#"+subgrid_div_id).append("<a id='draw_trends"+rowid+"' onclick=drawTrends('"+rowid+"','"+tempData[rowid].hazelcastMapName+"','"+tempData[rowid].osType+"','"+tempData[rowid].alertMessageparameterName+"','"+tempData[rowid].alertMessageHostName+"') href=#>Show Trends</a>");
				
				// Below code is for NON-EI usecase
				/*console.log(subgrid_id + "---" + rowid);
				var subgridId_id = subgrid_id+"_t";
				subgridId_id = subgridId_id.split(".").join("_");
				$("#"+subgrid_id).html("<table id="+ subgridId_id+"></table>");
				
				var rowData = $("#dashboardGrid").getRowData(rowid);
				var colData = 'Test Sample';
				var elements = colData.split(",");
				var appendRow=null;
				for(var i=0 ; i<elements.length ; i++){
					appendRow= "<tr><td>"+elements[i].split(":")[0]+"</td><td> : </td>"
								+"<td>"+elements[i].split(":")[1]+"</td></tr>";
				appendRow= "<tr><td>"+colData+"</td><td> : </td>";
					$("#"+subgridId_id).last().append(appendRow);
				}
				
				
				var rowData = $("#dashboardGrid").getRowData(rowid);
				var colData = rowData['description'];
				var elements = colData.split(",");
				var innerData = [];
				var subgrid_table_id;
				subgrid_table_id = subgrid_id+"_t";
				$("#"+rowid).add("<table id='"+subgrid_table_id+"' class='scroll'></table>");
				var appendRow=null;
				for(var i=0 ; i<elements.length ; i++){
					var eachRow = elements[i].split(":");
					var obj = {"key" : "" , "value" : ""};
					obj.key = eachRow[0];
					obj.value = eachRow[1];
					innerData.push(obj);
					appendRow= "<tr><td>"+elements[i].split(":")[0]+"</td><td> : </td>"
								+"<td>"+elements[i].split(":")[1]+"</td></tr>";
					$("#"+subgrid_table_id).last().append(appendRow);
				}*/
				 
            },
            loadComplete: function() {
                $("tr.jqgroup").css("background-color", "#e0e0e0");
            },
        });
}

function appendDataGrid(vdata,criticalCount,warningCount){
	console.log(vdata);
	var gridData = [];
	var critical=0,warning=0;
	var keys = Object.keys(vdata);
	for(var i =0;i<keys.length;i++) {
		gridData.push(vdata[keys[i]]);
		if(vdata[keys[i]].alertMessageType=="CRITICAL"){
			critical=critical+1;
		}
		if(vdata[keys[i]].alertMessageType=="WARNING"){
			warning=warning+1;
		}
	}
	console.log(gridData);
	$("#dashboardGrid").setGridParam({datatype: 'local', 'data': gridData});
	$('#dashboardGrid').setGridParam({grouping : true}); 
	$('#dashboardGrid').jqGrid('groupingGroupBy', 'alertMessageparameterName', {
        groupColumnShow: [false],
    });
	
		var criticalAlertValue = document.getElementById("cavalues");
		criticalAlertValue.innerHTML=critical;
		var criticalText=document.getElementById("catext");
		criticalText.innerHTML="Critical Alerts";
	  var criticalAlertValue = document.getElementById("wavalues");
	  criticalAlertValue.innerHTML=warning;
	  var criticalText=document.getElementById("watext");
	  criticalText.innerHTML="Warning Alerts";
}

function drawTrend(divid,hazelcastMapName,desc,host,subgridId) { 
	var param = divid.substring(divid.lastIndexOf("_")+1,divid.length);
	var descr = decodeURIComponent(desc);
	var graphFor = subgridId.substring(subgridId.lastIndexOf("_")+1,subgridId.length);
	  /*descr = descr.split("<b>")[1];
	  descr = descr.split("</b>")[0];
	  console.log("description after trim= "+descr);
	  var compName = descr.trim();*/
	var e = document.getElementById("timeselect");
	var selectedValue = e.options[e.selectedIndex].value;
	$.ajax({
    type: "GET",
    url: "additionalInfo.html",
    data : "parameterName="+hazelcastMapName+"&addressedMachine="+host+"&osType="+"windows"+"&timerange="+selectedValue+"&xAxisField="+"XAXIS"+"&yAxisField="+graphFor,
    success: function(response){
  	  var result = response.toString();
  	  var xAxisData=[];
  	  var objList=JSON.parse(result);
  	  var series1 = [];
  	  var yAxisArray = [];
  	  for(b=0;b<objList.length;b++){
					var actualObject=objList[b];
					yAxisArray.push(actualObject["yaxis"]);
					series1.push([Date.parse(actualObject["xaxis"]),actualObject["yaxis"]]);
  	  }
  	  
  	  var plot3 = $.jqplot('drawChart_', [series1], {
		      axes:{
		        xaxis:{
		          label : 'Time',
		          renderer:$.jqplot.DateAxisRenderer,
		          tickOptions:{formatString:'%H:%M:%S'}
		        },
		        yaxis:{
		        	label : graphFor,
		        	min : 0,
		        	max : 100
		        }
		      },
		      highlighter: {
		          show: true,
		          sizeAdjust: 7.5
		    },
		    cursor: {
		        show: true,
		        zoom: true,
		        looseZoom:true
		      },
		       
		      series:[{lineWidth:1, markerOptions:{style:'circle',size : 4.0}}]
		  });
    },
    failure : function(){
  	  alert(failure);
    }
	});
	
	function renderAlertCount(criticalAlertCount,warningAlertCount){
		var criticalAlertValue = document.getElementById("cavalues");
			criticalAlertValue.innerHTML=1;
		var criticalText=document.getElementById("catext");
			criticalText.innerHTML="Critical Alerts";
	    var criticalAlertValue = document.getElementById("wavalues");
	      criticalAlertValue.innerHTML=warningCount;
	    var criticalText=document.getElementById("watext");
	      criticalText.innerHTML="Warning Alerts";
	}
	
	
}	
