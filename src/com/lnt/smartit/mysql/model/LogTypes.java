package com.lnt.smartit.mysql.model;

import javax.persistence.Entity;
import javax.persistence.*;

@Entity
@Table(name="log_type")
public class LogTypes {
	
	@Id
	@Column(name="code")
	private String logTypeId;
	
	@Column(name="name")
	private String logTypeName;
	
	@Column(name="description")
	private String logTypeDesc;
	
	public String getLogTypeId() {
		return logTypeId;
	}
	public void setLogTypeId(String logTypeId) {
		this.logTypeId = logTypeId;
	}
	public String getLogTypeName() {
		return logTypeName;
	}
	public void setLogTypeName(String logTypeName) {
		this.logTypeName = logTypeName;
	}
	public String getLogTypeDesc() {
		return logTypeDesc;
	}
	public void setLogTypeDesc(String logTypeDesc) {
		this.logTypeDesc = logTypeDesc;
	}

}
