/**
 * 
 */
package com.lnt.smartit.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
/**
 * @author 299273
 *
 */
public class Util {

	@SuppressWarnings("unchecked")
	public static <T> void saveAuditCreation(T t){
		try {
			Class<T> clazz = (Class<T>) t.getClass();
			Method method = clazz.getDeclaredMethod("setCreatedBy",new Class[]{String.class});
			method.invoke(t,"admin");
			Method method2 = clazz.getDeclaredMethod("setCreationDate",new Class[]{Date.class});
			method2.invoke(t,new Date());
			
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static <T> void saveAuditUpdation(T t){
		try {
			Class<T> clazz = (Class<T>) t.getClass();
			Method method = clazz.getDeclaredMethod("setModifiedBy",new Class[]{String.class});
			method.invoke(t,"admin");
			method = clazz.getDeclaredMethod("setModifyDate",new Class[]{Date.class});
			method.invoke(t,new Date());
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}
}
