package com.lnt.smartit.util;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

public class DateUtil {
	
	private static Calendar today = new GregorianCalendar();
	private static DateFormat sdf;
	private static Date currentDateTime;
	private static Logger logger = Logger.getLogger(DateUtil.class);
	
	protected DateUtil()
	{
		
	}
	public static String convertTimestampToString(Timestamp time, String pattern)
	{
		String convertedDate = convertSQLDateToStringDate(convertTimestampToSQLDate(time), pattern);
		return convertedDate;
	}
	public static String changeTimeStampFormat(Timestamp timeStamp, String timeStampPattern)
	{
		String timeStampString = "";
		try {
			timeStampString = convertTimestampToString(timeStamp, "yyyy-MM-dd hh:mm:ss");
			SimpleDateFormat sdf = new SimpleDateFormat();
			sdf.applyPattern("yyyy-MM-dd hh:mm:ss");
			java.util.Date myDt  = sdf.parse(timeStampString);
			sdf.applyPattern(timeStampPattern);
			timeStampString = sdf.format(myDt);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		 catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return timeStampString;
	}
	public static String convertSQLDateToStringDate(Date sqlDate)
	{
		String stringDate = null;
		try {
			sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			stringDate = sdf.format(sqlDate);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Exception while formatting date : ", e);
		}
		return stringDate;	
	}
	public static String convertSQLDateToStringDate(Date sqlDate,String datePattern)
	{
		String stringDate = null;
		try {
			sdf = new SimpleDateFormat(datePattern);
			stringDate = sdf.format(sqlDate);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Exception while formatting date : ", e);
		}
		return stringDate;	
	}
	public static Date convertStringDateToSQLDate(String stringDate)
	{
		Date sqlDate = null;
		sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		try {
			today.setTime(sdf.parse(stringDate));
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			logger.error("Exception while converting date : ", e);
		}
		sqlDate = new java.sql.Date(today.getTimeInMillis());
		return sqlDate;
	}
	
	public static Date getCurrentDateTime() {
		currentDateTime = new java.sql.Date(new java.util.Date().getTime());
		return currentDateTime;
	}
	
	public static Timestamp getCurrentTime()
	{
		return new Timestamp(getCurrentDateTime().getTime());
	}
	public static Date convertUtilDateToSQLDate(java.util.Date utilDate)
	{
		return new java.sql.Date(utilDate.getTime());	
	}
	
	public static Date getOneDayBeforeDate(Date fromDate)
	{
		Date toDate = null;
		try {
			today.setTime(fromDate);
			today.add(Calendar.DAY_OF_MONTH, -1);
			toDate = new Date(today.getTimeInMillis());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Exception while formatting date : ", e);
		}
		return toDate;
	}
	
	public static Date getOneDayAheadDate(Date fromDate)
	{
		Date toDate = null;
		try {
			today.setTime(fromDate);
			today.add(Calendar.DAY_OF_MONTH, 1);
			toDate = new Date(today.getTimeInMillis());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Exception while formatting date : ", e);
		}
		return toDate;
	}
	
	public static Date convertTimestampToSQLDate(Timestamp time)
	{
		return new Date(time.getTime());
	}
	
	public static Timestamp getOneDayAheadTime(Timestamp time)
	{
		Timestamp newTime = null;
		Date sqlDate = convertTimestampToSQLDate(time);
		Date modifiedDate = getOneDayAheadDate(sqlDate);
		newTime = convertSQLDateToTimestamp(modifiedDate);
		return newTime;
	}
	
	public static Timestamp getOneDayBeforeTime(Timestamp time)
	{
		Timestamp newTime = null;
		Date sqlDate = convertTimestampToSQLDate(time);
		Date modifiedDate = getOneDayBeforeDate(sqlDate);
		newTime = convertSQLDateToTimestamp(modifiedDate);
		return newTime;
	}
	
	
	
	
	public static Timestamp convertSQLDateToTimestamp(Date sqlDate)
	{
		Timestamp timeStamp = new Timestamp(sqlDate.getTime());
		
		return timeStamp;
	}
	
	public static Timestamp convertUtilDateToTimestamp(java.util.Date utilDate)
	{
		Timestamp timeStamp = new Timestamp(utilDate.getTime());
		
		return timeStamp;
	}
	
	public static Date getSQLDate()
	{
		return new java.sql.Date(new java.util.Date().getTime());
	}
	
	public static java.util.Date getUtilDate()
	{
		return new java.util.Date();
	}
	
}
