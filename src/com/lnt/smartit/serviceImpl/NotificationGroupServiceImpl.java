package com.lnt.smartit.serviceImpl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lnt.smartit.helper.HbmHelper;
import com.lnt.smartit.mysql.model.NotificationGroup;
import com.lnt.smartit.mysql.model.ThresholdNotifyGroupMapping;
import com.lnt.smartit.services.NotificationGroupService;

@Service
public class NotificationGroupServiceImpl implements NotificationGroupService{
	
	@Autowired
	private HbmHelper dao;

	private  final Logger logger = LoggerFactory.getLogger(NotificationGroupServiceImpl.class.getName());
	
	public String savenotificationGroup(NotificationGroup notificationGroup){
		String notificationGroupId = null;
		try{
			dao.saveOrUpdate(notificationGroup);
		}catch(Exception e){
			e.printStackTrace();
		}
		logger.debug("---->notificationGroupId---"+notificationGroupId);
		return notificationGroupId;
	}
	
	@SuppressWarnings("unchecked")
	public List<NotificationGroup> getAllNotificationGroup() {
		List<NotificationGroup> notificationGroup = (List<NotificationGroup>) dao.createQuery("from NotificationGroup", null);
		logger.debug(notificationGroup.toString());
		return notificationGroup;
	}

	public NotificationGroup getNotificationGroupById(NotificationGroup notificationGroup) {
		NotificationGroup notiGroup = new NotificationGroup();
		notiGroup= (NotificationGroup) dao.findById(NotificationGroup.class,notificationGroup.getGroupId() );
		logger.debug("notiGroup"+notiGroup);
		return notiGroup;
	}

	public String deleteNotificationGroup(NotificationGroup notificationGroup) {
		String deleteNotificationGroupmsg;
		try{
			dao.delete(notificationGroup);
			deleteNotificationGroupmsg="NotificationGroup deleted successfully!!";
		}catch (Exception e) {
			e.printStackTrace();
			deleteNotificationGroupmsg="NotificationGroup deletion unsuccessfull due to "+e.getMessage();
		}
		return deleteNotificationGroupmsg;
	}

	@SuppressWarnings("unchecked")
	public List<ThresholdNotifyGroupMapping> getNotificationThresholdMappingById(String notificationGroupId) {
		List<ThresholdNotifyGroupMapping> thresholdNotifyGroupMappingList = (List<ThresholdNotifyGroupMapping>) dao.createQuery("from ThresholdNotifyGroupMapping where id.groupId="+notificationGroupId, null);
		logger.debug("-----thresholdNotifyGroupMappingList:  "+thresholdNotifyGroupMappingList.toString());
		return thresholdNotifyGroupMappingList;
	}

	public void saveNotificationThresholdMapping(List<ThresholdNotifyGroupMapping> thresholdNotifyGroupMappingList) {
		for (ThresholdNotifyGroupMapping thresholdNotifyGroupMapping : thresholdNotifyGroupMappingList) {
			dao.saveOrUpdate(thresholdNotifyGroupMapping);
		}
	}
}
