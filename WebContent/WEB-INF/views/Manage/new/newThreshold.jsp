<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><spring:message code="views.manage.new.threshold.title"></spring:message></title>
<!-- common css -->
<link href="css/custom/general/style1366.css" rel="stylesheet" />
<link href="css/custom/general/style1024.css" rel="stylesheet" />
<link href="css/custom/general/bluetabs.css" rel="stylesheet" />

<!-- import css -->
<link href="css/common/ui.jqgrid.css" rel="stylesheet" type="text/css" />
<link href="css/common/jquery-ui-1.9.1.custom.css" rel="stylesheet"
	type="text/css" />

<!-- jQuery scripts -->
<script src="scripts/common/jquery-1.9.1.js"></script>
<script src="scripts/common/grid.locale-en.js"></script>
<script src="scripts/common/jquery.jqGrid.min.js"></script>
<script src="scripts/common/jquery-ui.js"></script>



<!-- custom script -->
<script src="scripts/custom/newThreshold.js"></script>
<script src="scripts/common/common.js"></script>

<!-- jQuery style sheets -->
<link href="css/common/jquery-ui.css" rel="stylesheet" />

<!-- custom css -->
<link href="css/custom/specific/newThreshold.css"/>
<link href="css/custom/specific/manageThreshold.css"/>


<!-- fonts -->
<link href='http://fonts.googleapis.com/css?family=Roboto:500' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

</head>
<body>
	<header>
		<%@ include file="../../common/header.jsp"%>
	</header>


	<nav>
		<%@ include file="../../Menu/admin.jsp"%>
	</nav>
	<div id="newThresholdInnerContaine" class="allInnerContainer">
		<div id="alert"></div>
		<div id="thresHoldDetails">
			<!-- this div will have all the text boxes and check for adding new ThresHold -->
			<div id="headingManageThreshold" class="topic col1"></div>

			<hr id="horizontalLine">
			<table id="addThresHold" class="itPredictInp">
<tr>
				
					<td class="col1"><label>Parameter Name <span
							class="mandatory">*</span></label></td>
					<td class="col2"><input type="text" id="threshold_ParamName" class="textAreaWidth" value="${thresholds.paramName }" readonly/>
					
                   
					</td>
					<td class="col1"><label>Host Name</label></td>
					<td class="col2"><input type="text" id="threshold_hostName" "
						class="textAreaWidth" value="${thresholds.hostName }" readonly>
</td>

				


				</tr>
				<%-- <tr>
				
					<td class="col1"><label>Parameter Name <span
							class="mandatory">*</span></label></td>
					<td class="col2"><select id="threshold_ParamName" class="textAreaWidth" />
					<option value="${thresholds.paramName }">${thresholds.paramName }</option>
                        
					<c:forEach var="threshold_ParamNames" items="${threshold_ParamNames}">
								<option value=${threshold_ParamNames.paramName}>${threshold_ParamNames.paramName}</option>
							</c:forEach> 
					</td>
					<td class="col1"><label>Host Name</label></td>
					<td class="col2"><select id="threshold_hostName" "
						class="textAreaWidth">
						<option value="${thresholds.hostName }">${thresholds.hostName }</option>

							 <c:forEach var="threshold_hostNames" items="${threshold_hostNames}">
								<option value=${threshold_hostNames.value}>${threshold_hostNames.value}</option>
							</c:forEach> 

					</select></td>

					<td class="col1"><label>Monitoring Parameter</label></td>
					<td class="col2"><select id="threshold_parameter"
						class="textAreaWidth" onchange="checkIfAnythingToAdd()">
							<c:forEach var="parameterMaster" items="${parameterMaster}">
								<option value=${parameterMaster.key}>${parameterMaster.value}</option>
							</c:forEach>

					</select></td>


				</tr>
 --%>

				<!-- <tr>
					<td class="col1"><label>Threshold Enabled </label></td>
					<td class="col2"><input type="checkbox"
						id="threshold_enable_status" style="width: 0px;"></input></td>

					<td class="col1"><label>Notification Enabled </label></td>
					<td class="col2"><input type="checkbox"
						id="threshold_notification_status" style="width: 0px;" /></td>
				</tr> -->

				<tr class="separatorLineForManageThreshold">
					<td></td>

				</tr>

				<tr class="hideOnLogAndService">
					<td class="col1" colspan="2"><label class="higherLabel"><b>Warning
								 Settings </b></label></td>
				</tr>

				<!-- Warning settings -->
				<tr class="hideOnLogAndService">
					<td class="col1"><label> Warning Threshold <span
							class="mandatory">*</span></label></td>
					<td class="col2"><input type="text" id="warning_threshold" value="${thresholds.warnThreshold}" /></td>
					<!-- <td class="col1"><label> Higher Threshold <span
							class="mandatory">*</span></label></td>
					<td class="col2"><input type="text"
						id="high_warning_threshold" /></td> -->
					<!-- </tr>
				
				<tr> -->
					<!-- <td class="col1"><label>Warning Breach Duration</label></td>
					<td class="col2"><input type="text"
						id="threshold_warning_breach_duration_val"
						placeholder="In minutes" />&nbsp;&nbsp;mins</td> -->
				</tr>
				<!-- alerts settings -->
				<tr class="separatorLineForManageThreshold">
					<td></td>

				</tr>

				<tr class="hideOnLogAndService">
					<td class="col1" colspan="2"><label class="higherLabel"><b>Critical
								 Settings</b></label></td>
				</tr>

				<tr class="hideOnLogAndService">
					<td class="col1"><label> Critical Threshold <span
							class="mandatory">*</span></label></td>
					<td class="col2"><input type="text" id="critical_threshold" value="${thresholds.criticalThreshold}"/></td>
					<!-- <td class="col1"><label> Higher Threshold <span
							class="mandatory">*</span></label></td>
					<td class="col2"><input type="text" id="high_alert_threshold" /></td>
 -->

					<!-- </tr>
				<tr> -->
					<!-- <td class="col1"><label>Critical Breach Duration</label></td>
					<td class="col2"><input type="text"
						id="threshold_alert_breach_duration_val" placeholder="In minutes" />&nbsp;&nbsp;mins</td> -->
				</tr>

				<tr class="separatorLineForManageThreshold">
					<td></td>

				</tr>

				<!-- <tr class="hideOnLogAndService">
					<td class="col1"><label class="higherLabel"><b>Data
								Settings</b></label></td>
				</tr>

				<tr class="hideOnLogAndService">
					<td class="col1"><label>Data Type</label></td>
					<td class="col2"><select id="threshold_alert_breach_dataType">
							<option value=percentage>Percentage</option>
							<option value=value>Value</option>
					</select></td>
				</tr> -->
				
<!-- 				row for log file size and services
				<tr id="logFileSizeTitle">
					<td class="col1" colspan="2"><label class="higherLabel"><b>Log File Size Setting</b></label></td>
				</tr>
				<tr id="logFileSize">
					<td class="col1"><label>Log File Name</label></td>
					<td class="col2"><input type="text" id="logFileName"></td>
					
					<td class="col1"><label>Critical Threshold</label></td>
					<td class="col2"><input type="text" id="logCriticalThreshold"></td>
					
					<td class="col1"><label>Warning Threshold</label></td>
					<td class="col2"><input type="text" id="logWarningThreshold"></td>
				</tr>
				
				
				<tr id="servicesTitle">
					<td class="col1" colspan="2"><label class="higherLabel"><b>Service Settings</b></label></td>
				</tr>
				<tr id="services">
					<td class="col1"><label>Services</label></td>
					<td class="col2"><input type="text" id="serviceName"></td>
				</tr>
 -->
				<tr class="separatorLineForManageThreshold">
					<td></td>

				</tr>

				<!-- <tr>
					<td class="col1" colspan="2"><label class="higherLabel"><b>Notification
								Settings</b></label></td>
				</tr> -->
			<td class="col2"><input type="hidden" id="id" class="textAreaWidth" value="${thresholds.id}"/>
                    
				</td>
				
				<%-- for void ind <tr class="hideOnLogAndService">
					<td class="col1" colspan="2"><label class="higherLabel"><b>Status</b></label></td>
				</tr>
				<tr>
				<td class="col1"><label> Status <span
							class="mandatory">*</span></label></td>
				<td class="col2"><select id="threshold_status" "
						class="textAreaWidth">
				<option value="${thresholds.voidInd }">${thresholds.voidInd }</option>
 
				<option value="Enabled">Enabled</option>
				<option value="Disabled">Disabled</option>

					</select>
                    
				</td></tr> --%>
			</table>

			<!-- <table id="logFileTable" class="itPredictInp">
			<tr id="logFileSizeTitle"><td class="col1" colspan="2"><label class="higherLabel"><b>Log File Size Setting</b></label><img id="plusImageLog" src="images/custom/1366_768/plus.png"></td>
			</tr>
			</table>
			
			<table id="servicesTable">
			<tr id="servicesTitle">
			<td class="col1" colspan="2"><label class="higherLabel"><b>Service Settings</b></label><img id="plusImageServices" src="images/custom/1366_768/plus.png"></td>
			</tr>
			</table> -->

			<%-- <div id="grp_notification_selected">
				<table id="grp_nameList" class="itPredictInp" cellspacing="2" cellpadding="2">

					<tr>
						<td class="col1" colspan="2"><label class="higherLabel"><b>Notification
									Settings</b></label></td>
					</tr>
					<tr>
						<td class="notificationGroups"><div class="heading1">
								<label>Notification Group <span class="mandatory">*</span></label>
							</div></td>
						<!-- <td></td> -->

						<td><select id="threshold_group_name" name="threhold_grpName"
							class="list_measure" size=${notificationGroupSize} >
								<c:forEach var="notificationGroup" items="${notificationGroup}">
									<option value=${notificationGroup.key}>${notificationGroup.value}</option>
								</c:forEach>
						</select></td>


						<td>&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;</td>


						<td class="selectedGroupName"><div class="heading2">
								<label>Selected Group Names</label>
							</div></td>
						<!-- <td></td>
							<td><div class="heading2">Additional Email ID's </div></td> -->
						<!-- <tr> -->
						
							<td><select id="threshold_group_name"
								name="threhold_grpName" class="list_measure"
								size=${notificationGroupSize} >
									<c:forEach var="notificationGroup" items="${notificationGroup}">
										<option value=${notificationGroup.key}>${notificationGroup.value}</option>
									</c:forEach>
							</select></td>
						<td><img src="images/common/forward_18_18.jpg" title='Add'
							id="AddGroupName"> <br> <img
							src="images/common/back_18_18.jpg" title='Add'
							id="removeGroupName"></td>
						<td><select id="threshold_group_name_sel"
							name="threhold_grpName_sel" class="list_measure"
							size=${notificationGroupSize} >
								<!-- <option value=Admin>Admin</option>
					<option value=Cops>Cops Group</option> -->
						</select></td>

						<td>&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;</td>

						<td class="additionEmailID"><label>Additional Email
								ID's </label></td>

						<td class="col2"><textarea id="threshold_additionalEmailList"
								placeholder="Comma Seperated Email Id's"></textarea></td> 
						<td class="additionEmailID"><label>&nbsp; </label></td>
						<td class="col2"></td>
					</tr>
				</table>

				<!-- 	<hr id="horizontalLine"> -->
			</div> --%>







		</div>

		<!-- <div id="add_Threshold_buttons" class="buttonPosition">

			<table>
				<tr>
					<td><button id="add_threshold_submit"
							class="buttonClass buttonClass_submit">Submit</button></td>
					
					<td>
						<button id="add_threshold_cancel"
							class="buttonClass buttonClass_cancel">Cancel</button>
					</td>
				</tr>
			</table>


		</div>
		<div id="preview_th_buttons" class="buttonPosition">
			<table>
				<tr>
					<td>
						<button id="preview_ok"
							class="buttonClass buttonClass_submit">Ok</button>
					</td>
					
				</tr>
			</table>

		</div> -->
		<div id="update_buttons" class="buttonPosition">

			<table>
				<tr>
					<td><button id="edit_threshold"
							class="buttonClass buttonClass_submit" style="/* display: none; */" >Submit</button></td>
					<td><button id="cancel_threshold" class="buttonClass buttonClass_submit" style="/* display: none; */" >Cancel</button></td>
				</tr>
			</table>


		</div>
	</div>
<div id ="alert"></div>	

<div id="confirmEdit" title="Alert">
  <p>These Changes would be reflected after one business day</p>
</div>
	<footer>
		<%@ include file="../../common/footer.jsp"%>
	</footer>
</body>
</html>