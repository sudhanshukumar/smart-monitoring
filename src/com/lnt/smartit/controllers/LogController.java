package com.lnt.smartit.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lnt.smartit.services.DeviceService;
import com.lnt.smartit.services.LogService;

@Controller
public class LogController {
	
	@Autowired
	LogService logService;
	
	@Autowired
	DeviceService deviceService;
	
	@RequestMapping(value = "getMapNameForChart.html", method = RequestMethod.POST)
	@ResponseBody
	public String getMapNameForChart(@RequestParam(value="hostname")String hostname, @RequestParam(value="component")String comp,HttpServletRequest request){
		HttpSession session = request.getSession();
		if(session.getAttribute("appId")==null){
			String contextPath = request.getContextPath();
			String[] temp = contextPath.split("_");
			String appId = logService.getAppIdByName(temp[temp.length-1]);
			String appName=temp[temp.length-1];
			session.setAttribute("appId",appId);
			session.setAttribute("appname", appName);
		}
		String appId = session.getAttribute("appId").toString();
		String deviceId = deviceService.getDeviceMasterByHostName(hostname);
		String osType = logService.getOSfromdeviceId(deviceId);
		System.out.println(appId +""+deviceId+""+osType );
		String logId = logService.getLogId(appId, deviceId, osType);
		String compId = logService.getAppServiceComponentId(appId, "RAW_STORE", comp.toUpperCase());
		String mapName = logService.getLogServiceComponentParamValue(logId, compId);
		if(osType.toLowerCase().contains("win")){
			osType = "windows";
		}
		else if(osType.toLowerCase().contains("lnx")){
			osType = "linux";
		}
		return "{\"mapName\":\""+mapName+"\",\"osType\":\""+osType+"\"}";
	}
}
