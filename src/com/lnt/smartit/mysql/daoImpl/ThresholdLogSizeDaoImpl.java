package com.lnt.smartit.mysql.daoImpl;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lnt.smartit.helper.HbmHelper;
import com.lnt.smartit.mysql.dao.ThresholdLogSizeDao;
import com.lnt.smartit.mysql.model.ThresholdLogSize;
@Repository
@Transactional
public class ThresholdLogSizeDaoImpl implements ThresholdLogSizeDao,Serializable{

	@Autowired
	private HbmHelper hbmHelper;
	@Autowired
	private SessionFactory sessionFactory;
	private static Logger logger = Logger.getLogger(SoftwareDaoImpl.class);

	/* (non-Javadoc)
	 * @see com.lnt.smartit.mysql.daoImpl.ThresholdLogSizeDao#getAllThresholdLogSizeRecords()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<ThresholdLogSize> getAllThresholdLogSizeRecords() {
		logger.debug(" in ThresholdLogSize --> getAllThresholdLogSizeRecords()");
		List<ThresholdLogSize> thresholdLogSizeRecordList = (List<ThresholdLogSize>) hbmHelper.createQuery("from ThresholdLogSize", null);
		logger.debug("ThresholdLogSizeList "+  thresholdLogSizeRecordList.toString());
		return thresholdLogSizeRecordList;
	}

	/* (non-Javadoc)
	 * @see com.lnt.smartit.mysql.daoImpl.ThresholdLogSizeDao#saveNewThresholdLogSizeRecord(com.lnt.smartit.mysql.model.ThresholdLogSize)
	 */
	@Override
	public void saveNewThresholdLogSizeRecord(ThresholdLogSize thresholdLogSizeRecord) {
		try {
			hbmHelper.saveOrUpdate(thresholdLogSizeRecord);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see com.lnt.smartit.mysql.daoImpl.ThresholdLogSizeDao#getThresholdLogSizeById(com.lnt.smartit.mysql.model.SoftwareMaster)
	 */
	@Override
	public ThresholdLogSize getThresholdLogSizeById(ThresholdLogSize thresholdLogSizeRecord) {
		ThresholdLogSize logSizeRec = new ThresholdLogSize();
		try {
			Session session  = sessionFactory.getCurrentSession();
			String queryString = "from ThresholdLogSize t where t.id = '"+thresholdLogSizeRecord.getId()+"'";
			Query query = session.createQuery(queryString);
			logSizeRec = (ThresholdLogSize) query.uniqueResult();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		logger.debug(logSizeRec);
		return logSizeRec;
	}

	/* (non-Javadoc)
	 * @see com.lnt.smartit.mysql.daoImpl.ThresholdLogSizeDao#deleteThresholdLogSizeRecord(com.lnt.smartit.mysql.model.ThresholdLogSize)
	 */
	@Override
	public String deleteThresholdLogSizeRecord(ThresholdLogSize thresholdLogSizeRecord) {
		String deleteServiceMsg;
		try{
			hbmHelper.delete(thresholdLogSizeRecord);
			deleteServiceMsg="Log file record deleted successfully!!";
		}catch (Exception e) {
			e.printStackTrace();
			deleteServiceMsg="Log File record deletion unsuccessfull due to "+e.getMessage();
		}
		return deleteServiceMsg;
	}
	
	/* (non-Javadoc)
	 * @see com.lnt.smartit.mysql.daoImpl.ThresholdLogSizeDao#exists(com.lnt.smartit.mysql.model.ThresholdLogSize)
	 */
	@Override
	public Boolean exists(ThresholdLogSize thresholdLogSizeRecord)
	{
		try {
			Session session = sessionFactory.getCurrentSession();
			String queryString = "from ThresholdLogSize t where "
					+ "t.hostName = '"+thresholdLogSizeRecord.getHostName()
					+"' and t.logFileName = '"+thresholdLogSizeRecord.getLogFileName()+"'";
			Query query = session.createQuery(queryString);
			if(query.list().isEmpty())
			{
				return false;
			}
			else
			{
				return true;
			}
		} catch (HibernateException e) {
			e.printStackTrace();
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			return true;
		}
	}
}
