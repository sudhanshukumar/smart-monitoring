package com.lnt.smartit.mysql.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="service_component")
public class ServiceComponent implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="code")
	private String serviceCompId;
	
	@Column(name="name")
	private String serviceCompName;
	
	@Column(name="description")
	private String serviceCompDesc;

	public String getServiceCompId() {
		return serviceCompId;
	}

	public void setServiceCompId(String serviceCompId) {
		this.serviceCompId = serviceCompId;
	}

	public String getServiceCompName() {
		return serviceCompName;
	}

	public void setServiceCompName(String serviceCompName) {
		this.serviceCompName = serviceCompName;
	}

	public String getServiceCompDesc() {
		return serviceCompDesc;
	}

	public void setServiceCompDesc(String serviceCompDesc) {
		this.serviceCompDesc = serviceCompDesc;
	}
	
	
}
