package com.lnt.smartit.models;

public class JavaLogs {
	
	double Timestamp;
	double NGCMN;
	double NGCMX;
	double NGC;
	double S0C;
	double S1C;
	double EC;
	double OGCMN;
	double OGCMX;
	double OGC;
	double OC;
	double PGCMN;
	double PGCMX;
	double PGC;
	double PC;
	double YGC;
	double FGC;
	public double getTimestamp() {
		return Timestamp;
	}
	public void setTimestamp(double timestamp) {
		Timestamp = timestamp;
	}
	public double getNGCMN() {
		return NGCMN;
	}
	public void setNGCMN(double nGCMN) {
		NGCMN = nGCMN;
	}
	public double getNGCMX() {
		return NGCMX;
	}
	public void setNGCMX(double nGCMX) {
		NGCMX = nGCMX;
	}
	public double getNGC() {
		return NGC;
	}
	public void setNGC(double nGC) {
		NGC = nGC;
	}
	public double getS0C() {
		return S0C;
	}
	public void setS0C(double s0c) {
		S0C = s0c;
	}
	public double getS1C() {
		return S1C;
	}
	public void setS1C(double s1c) {
		S1C = s1c;
	}
	public double getEC() {
		return EC;
	}
	public void setEC(double eC) {
		EC = eC;
	}
	public double getOGCMN() {
		return OGCMN;
	}
	public void setOGCMN(double oGCMN) {
		OGCMN = oGCMN;
	}
	public double getOGCMX() {
		return OGCMX;
	}
	public void setOGCMX(double oGCMX) {
		OGCMX = oGCMX;
	}
	public double getOGC() {
		return OGC;
	}
	public void setOGC(double oGC) {
		OGC = oGC;
	}
	public double getOC() {
		return OC;
	}
	public void setOC(double oC) {
		OC = oC;
	}
	public double getPGCMN() {
		return PGCMN;
	}
	public void setPGCMN(double pGCMN) {
		PGCMN = pGCMN;
	}
	public double getPGCMX() {
		return PGCMX;
	}
	public void setPGCMX(double pGCMX) {
		PGCMX = pGCMX;
	}
	public double getPGC() {
		return PGC;
	}
	public void setPGC(double pGC) {
		PGC = pGC;
	}
	public double getPC() {
		return PC;
	}
	public void setPC(double pC) {
		PC = pC;
	}
	public double getYGC() {
		return YGC;
	}
	public void setYGC(double yGC) {
		YGC = yGC;
	}
	public double getFGC() {
		return FGC;
	}
	public void setFGC(double fGC) {
		FGC = fGC;
	}
}
