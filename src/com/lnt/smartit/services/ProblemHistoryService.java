package com.lnt.smartit.services;

import java.util.List;

import com.lnt.smartit.mysql.model.Alertmessageinfo;
import com.lnt.smartit.solr.pojo.JqGridData;

public interface ProblemHistoryService {
	public List<Alertmessageinfo> getProblemHistory(Integer pageNo, Integer pageRowCount, Boolean cache);
	public Integer getProblemHistoryRecordCount();
	public Integer getProblemHistoryLastPageNo(Integer pageRowCount, Boolean cache);
	public List<String> getDistinctHostnames();
	public List<String> getDistinctParameters();
	public void load();
	List<String> getDistinctAlertTypes();
	public JqGridData getProblemHistoryWithCriteria(Integer pageNo,
			Integer pageRowCount, Boolean cache, String app, String hostname,
			String parameter, String alertType, String fromDate, String toDate);
}
