/*This file is added by Pinakin on 21st August 2015. This is a utility class
which can be used for Pagination in Hibernate.*/

package com.lnt.smartit.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;

public final class HibernatePageIterator {

	private int pageRowCount;
	private int lastPageNumber;
	private int pageNumber;
	private Query query;
	private Criteria criteria;
	private String identifier;
	private int totalRowCount;

	private static Map<String,HibernatePageIterator> map= new  HashMap<String,HibernatePageIterator>();

	private HibernatePageIterator(String identifier,int pageRowCount, Criteria criteria,boolean cache) {
		super();
//		System.out.println("In Cons - HibernatePageIterator "+ System.currentTimeMillis());
		this.identifier =identifier; 
		this.query = null;
		this.criteria = criteria;
		this.criteria.setCacheable(false);
		this.criteria.setCacheMode(CacheMode.REFRESH);
		this.pageRowCount=pageRowCount;
		getLastPageNumber();
		pageNumber=1;
		if(cache && this.identifier != null)
		{
			map.put(this.identifier,this);
		}
//		System.out.println("In Cons End - HibernatePageIterator "+ System.currentTimeMillis());
	}

	private HibernatePageIterator(String identifier,int pageRowCount,Query query,boolean cache) {
		super();
		this.identifier =identifier;
		this.query = query;
		this.criteria = null;
		this.pageRowCount=pageRowCount;
		getLastPageNumber();
		pageNumber=1;
		if(cache && this.identifier != null)
		{
			map.put(this.identifier,this);
		}
	}

	@SuppressWarnings("rawtypes")
	public List firstPage()
	{
		pageNumber=1;
		return getList(1);
	}

	@SuppressWarnings("rawtypes")
	private List getList(int pageNumber) {
		if(query != null)
		{
			query.setCacheable(false);
			query.setFirstResult((pageNumber-1)*pageRowCount);
			query.setMaxResults(pageRowCount);
			return query.list();
		}
		else if(criteria != null)
		{
			criteria.setFirstResult((pageNumber-1)*pageRowCount);
			criteria.setMaxResults(pageRowCount);
			return criteria.list();
		}
		return null;
	}



	public List list(int pageNumber)
	{
		/*if(pageNumber >= 1 && pageNumber <= lastPageNumber)
		{ */
			this.pageNumber=pageNumber;
			return getList(pageNumber);
//		}

//		return null;
	}



	public List<?> next()
	{
		if(pageNumber+1 == lastPageNumber)
		{
			return lastPage();
		}

		if(pageNumber+1 >= 1 && pageNumber+1 < lastPageNumber)
		{ 
			this.pageNumber++;
			return getList(pageNumber);
		}

		return null;
	}


	public List<?> previous()
	{
		if(pageNumber ==1)
		{
			return getList(1);
		}


		if(pageNumber == lastPageNumber)
		{
			return getList(1);
		}

		if(pageNumber-1 >= 1 && pageNumber-1 <= lastPageNumber)
		{ 
			this.pageNumber--;
			return getList(pageNumber);
		}

		return null;
	}

	public int getLastPageNumber()
	{
		ScrollableResults result = null;
		if(query != null)
		{
			query.setMaxResults(-1);
			result = query.scroll();
		}
		else if(criteria != null)
		{
			criteria.setMaxResults(Integer.MAX_VALUE);
//			System.out.println("Cr = "+criteria+ " - "+System.currentTimeMillis());
			result=criteria.scroll();
		}
		if(result != null)
		{
			result.afterLast();
			result.setRowNumber(-1);
			totalRowCount = result.getRowNumber()+1;
			lastPageNumber = totalRowCount/pageRowCount;
			//Logic for last page no copied from PaginationUtil.java
			int v = 1;
			v = totalRowCount%pageRowCount==0 ? 0 : 1;
			lastPageNumber = totalRowCount==0 ? 1 : totalRowCount/pageRowCount + v;
			pageNumber=lastPageNumber;
		}
//		System.out.println("returning getLastPageNumber - "+System.currentTimeMillis());
		return pageNumber;
	}

	public int getTotalRowCount()
	{
		ScrollableResults result = null;
		if(query != null)
		{
			query.setMaxResults(-1);
			result = query.scroll();
		}
		else if(criteria != null)
		{
			criteria.setMaxResults(-1);
			result=criteria.scroll();
		}
		if(result != null)
		{
			result.afterLast();
			result.setRowNumber(-1);
			totalRowCount = result.getRowNumber()+1;
		}
		else
		{
			totalRowCount=0;
		}
		return totalRowCount;
	}

	public List<?> lastPage()
	{
		return list(getLastPageNumber());
	}

	public int getPageRowCount() {
		return pageRowCount;
	}

	public void setPageRowCount(int pageRowCount) {
		this.pageRowCount = pageRowCount;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public static HibernatePageIterator createInstance(String identifier,int pageRowCount,Query query,boolean cache)
	{
		if(query == null)
		{
			return null;
		}
		if(cache)
		{
			if(identifier != null && !identifier.trim().equals(""))
			{
				if(map.containsKey(identifier))
				{
					return map.get(identifier);
				}
				else
				{
					map.put(identifier, new  HibernatePageIterator(identifier,pageRowCount,query,cache));
					return map.get(identifier);
				}
			}
		}
		return new  HibernatePageIterator(identifier,pageRowCount,query,cache);
	}

	public static HibernatePageIterator createInstance(String identifier,int pageRowCount, Criteria criteria,boolean cache)
	{
		if(criteria == null)
		{
			return null;
		}
		if(cache)
		{
			if(identifier != null && !identifier.trim().equals(""))
			{
				if(map.containsKey(identifier))
				{
					return map.get(identifier);
				}
				else
				{
					map.put(identifier, new  HibernatePageIterator(identifier,pageRowCount,criteria,cache));
					return map.get(identifier);
				}


			}
		}
		else
		{
			return new  HibernatePageIterator(identifier,pageRowCount,criteria,cache);
		}
		return null;
	}
}
