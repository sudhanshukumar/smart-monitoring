package com.lnt.smartit.helper.adapter.serviceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
//import org.lnt.bmc.TicketCreation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.lnt.smartit.helper.ConfigSetup;
import com.lnt.smartit.helper.adapter.service.ITSMAdapter;
import com.lnt.smartit.models.AlertMessage;
import com.lnt.smartit.mysql.model.AppServiceComponentParams;
import com.lnt.smartit.services.ITSMAdapterService;

@Component
public class BMCAdapter implements ITSMAdapter {
static final Object lock = new Object();
	
	@Autowired
	private ITSMAdapterService itsmAdapterService;
	
	private static Logger logger = Logger.getLogger(ConfigSetup.class);
	private AlertMessage alertMsg;
	private String url = null;
	public Map<String,String> priorityMap = new HashMap<String, String>();
	public BMCAdapter() {}
	
	@Override
	public String createITSMincident(AlertMessage alertMsg) {
		String incidentNumber = null;
		//boolean flag = checkIncidentStatusInELKToCreateIncidentInSNOW(alertMsg);
		boolean flag = true;
		if(flag){
			priorityMap.put("1", "1 - Critical");
			priorityMap.put("2", "2 - High");
			priorityMap.put("3", "3 - Moderate");
			priorityMap.put("4", "4 - Low");
			priorityMap.put("5", "5 - Planning");
			
			//url = "https://dev18896.service-now.com/api/now/table/incident";
			
			List<AppServiceComponentParams> appServiceComponentParamslist = itsmAdapterService.getServiceCompParamsForITSM();
			System.out.println(appServiceComponentParamslist);
			for(AppServiceComponentParams appServiceComponentParam : appServiceComponentParamslist){
				if(appServiceComponentParam.getParamName().equalsIgnoreCase("url")){
					url = appServiceComponentParam.getParamValue();
				}
			}
			if(url != null && !url.equalsIgnoreCase("")){
				logger.info("Create Incident :: Ticket Description = "+alertMsg.getItsmTktDescription());
				OutputStream os = null;
				BufferedReader bufferedReader = null;
				HttpURLConnection connection = null;
				try {
					ObjectMapper mapper = new ObjectMapper();
					String description =alertMsg.getDescription();
					synchronized(lock) {
							connection =(HttpURLConnection)new URL(url).openConnection();
							System.out.println(connection.getURL());
							connection.setRequestMethod("POST");
							connection.setDoOutput(true);
							connection.setRequestProperty("Accept", "application/json");
							connection.setRequestProperty("Content-Type", "application/json");
							connection.setDoInput(true);
							String postData = null;
							postData = "{\'description\':\'"+"Host - "+alertMsg.getAlertMessageHostName()+" , "+description.toString()+"Test From SM"+"\'}";
							os = connection.getOutputStream();
							os.write(postData.getBytes());
							os.flush();
							//get response from service now
							System.out.println("Response Code : "+connection.getResponseCode());
							bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
							StringBuilder content = new StringBuilder();
						    String line;
						    // read from the urlconnection via the bufferedreader
						    while ((line = bufferedReader.readLine()) != null)
						    {
						        content.append(line + "\n");
						    }
						    System.out.println("printing content for ------!!!--------"+alertMsg.getAlertMessageparameterName()+" ------!!!!-----");
						    System.out.println(content);
						    if(content != null){
						    	 JSONObject json;
						    	 String sysId = null;
						    	 String incidentStatus = null;
						    	 String priority = null;
						    	 
								/*try {
									json = new JSONObject(content.toString());
									 JSONObject innerJson = new JSONObject(json.get("result").toString());  
									 incidentNumber = innerJson.get("number").toString();
									 sysId = (String) innerJson.get("sys_id");
									 //incidentStatus = (String) innerJson.get("incident_state");
									 incidentStatus = "New";	
									 priority = (String) innerJson.get("priority"); 
									 priority = priorityMap.get(priority);
									 System.out.println("IncidentNumber : "+incidentNumber);	
									 System.out.println("sysId : "+sysId);
									 System.out.println("IncidentStatus : "+incidentStatus);
									 System.out.println("Priority : "+priority);
								} catch (org.codehaus.jettison.json.JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}*/
								
								 
								// updateIncidentDataInELK(alertMsg, incidentNumber, sysId, incidentStatus, priority);
						    }
				    }
				    //bufferedReader.close();
				} catch (IOException e) {
					logger.error(" -- Something went wrong in ServiceNow -- ",e);
				} finally {
					try {
						if(os != null)
							os.close();
						if(bufferedReader != null)
							bufferedReader.close();
						connection.disconnect();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		 
		return incidentNumber;	
	}

	public static void main(String[] args) {/*
		TicketCreation t = new TicketCreation();
		try {
			String s = t.createTicket("Test");
			System.out.println("hELLO");
			System.out.println(s);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	*/

		
		
	
	}

}
