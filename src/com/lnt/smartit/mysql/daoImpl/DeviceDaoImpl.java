package com.lnt.smartit.mysql.daoImpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lnt.smartit.helper.HbmHelper;
import com.lnt.smartit.mysql.dao.DeviceDao;
import com.lnt.smartit.mysql.model.AppDeviceMaster;
import com.lnt.smartit.mysql.model.DeviceApplicationMapping;
import com.lnt.smartit.mysql.model.DeviceApplicationMappingId;
import com.lnt.smartit.mysql.model.DeviceMaster;
import com.lnt.smartit.mysql.model.DeviceParams;
import com.lnt.smartit.mysql.model.DeviceSoftwareMapping;
import com.lnt.smartit.mysql.model.DeviceSoftwareMappingId;
import com.lnt.smartit.mysql.model.DeviceType;

@Repository
@Transactional
public class DeviceDaoImpl implements DeviceDao{
	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private HbmHelper hbmHelper;
	private static Logger logger = Logger.getLogger(DeviceDaoImpl.class);

	@SuppressWarnings("unchecked")
	public List<DeviceParams> getDeviceParams(String appId) {
		@SuppressWarnings("unchecked")
		Session session = sessionFactory.getCurrentSession();
		
		List<DeviceParams> devices= new ArrayList<DeviceParams>();
		List<Object[]> devicelist=new ArrayList<Object[]>();
		Query  query = session.createQuery(" SELECT t2.appDevice,t2.name,t2.value,t2.voidInd FROM DeviceParams t2 INNER JOIN t2.appDevice t1 WHERE t2.appDevice.deviceId=t1.deviceId AND t2.voidInd=t1.voidInd and t1.app.appId=:appId AND t1.voidInd='n'");
		query.setString("appId",appId);
		devicelist=query.list();
		for(Object[] row : devicelist){
			 DeviceParams deviceparam= new DeviceParams();
			 deviceparam.setAppDevice((AppDeviceMaster)row[0]);
			 deviceparam.setName((String) row[1]);
			 deviceparam.setValue((String) row[2]);
			 deviceparam.setVoidInd((String)row[3]);
			 devices.add(deviceparam);
		}
		
		return devices;
	}
	
	

	public void saveDeviceparams(DeviceParams deviceparams) {
		String temp = null;
		try {
			temp = (String) hbmHelper.save(deviceparams);
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.debug("---->temp---" + temp);
		
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getEditSoftwareId(DeviceMaster devMaster) {
		List<String> softwareIds = new ArrayList<String>();
		List<DeviceSoftwareMapping> softwaremappings = (List<DeviceSoftwareMapping>) hbmHelper.createQuery("from DeviceSoftwareMapping where id.deviceId ="+devMaster.getDeviceId(), null);
		for (DeviceSoftwareMapping deviceSoftwareMappingId : softwaremappings) {
			softwareIds.add(deviceSoftwareMappingId.getId().getSoftwareId());
		}
		return softwareIds;
	}
	
	String deleteDeviceMasterMsg;
	public String deleteAppDeviceMaster(AppDeviceMaster devMaster) {
		try{
			Session session = sessionFactory.getCurrentSession();
			String id=devMaster.getDeviceId();
			Query query = session.createQuery("update AppDeviceMaster ap set ap.voidInd=:void_ind where ap.deviceId=:id");
			String void_ind="y";
			query.setString("void_ind", void_ind);
			query.setString("id", id);
			query.executeUpdate();

			deleteDeviceMasterMsg="AppDeviceMaster deleted successfully!!";
		}catch (Exception e) {
			e.printStackTrace();
			deleteDeviceMasterMsg="AppDeviceMaster deletion unsuccessfull due to "+e.getMessage();
		}
		return deleteDeviceMasterMsg;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public String getDeviceIdByHostname(String hostname){
		List<DeviceParams> deviceParams = (List<DeviceParams>) hbmHelper.createQuery("from DeviceParams where name like 'hostname' and value like '"+hostname+"'", null);
		return deviceParams.get(0).getAppDevice().getDeviceId();
	}

	@Override
	public Integer getMaxAppDeviceId() {
		
		    Session session = sessionFactory.getCurrentSession();
		    Query query = session.createQuery("select MAX(appDevice) FROM DeviceParams");
		    AppDeviceMaster am=new AppDeviceMaster();
		    am=(AppDeviceMaster) query.uniqueResult();
		    System.out.println("myvalue"+am.getDeviceId());
		    int maxvalue=Integer.parseInt(am.getDeviceId());
		  System.out.println("maxvalue=="+maxvalue);
		return maxvalue;
	}

	@Override
	public void saveAppDeviceMaster(AppDeviceMaster appDeviceMaster) {
		System.out.println("appid=="+appDeviceMaster.getApp().getAppId());
		
		try {
			
		 hbmHelper.save(appDeviceMaster);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getDeviceTypecode(String searchCriteria) {
		  Session session = sessionFactory.getCurrentSession();
		    Query query = session.createQuery("from DeviceType u where u.name like :userId");
		    query.setString("userId", "%"+searchCriteria+"%");
		    DeviceType deviceTypecode= (DeviceType) query.uniqueResult();
		    System.out.println("getDeviceTypeCode="+deviceTypecode.getDeviceTypeCode());
		return deviceTypecode.getDeviceTypeCode();
	}

	@Override
	public void deleteDeviceParams(String deviceId) {
		try{
			Session session = sessionFactory.getCurrentSession();
			Query query = session.createQuery("update DeviceParams ap set ap.voidInd=:void_ind where ap.appDevice.deviceId=:app_device_id");
			String void_ind="y";
			query.setString("void_ind", void_ind);
			query.setString("app_device_id", deviceId);
			query.executeUpdate();
			deleteDeviceMasterMsg="AppDeviceMaster deleted successfully!!";
		}catch (Exception e) {
			e.printStackTrace();
			deleteDeviceMasterMsg="AppDeviceMaster deletion unsuccessfull due to "+e.getMessage();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DeviceParams> getDeviceParamsById(String deviceid) {
		Session session = sessionFactory.getCurrentSession();
		List<DeviceParams> devicesById= new ArrayList<DeviceParams>();
		Query query = session.createQuery("from DeviceParams dp where dp.appDevice.deviceId=:app_device_id");
		query.setString("app_device_id", deviceid);
		devicesById=query.list();
		return devicesById;
		
	}

	@Override
	public AppDeviceMaster getOsById(String deviceid) {
		Session session = sessionFactory.getCurrentSession();
		List<AppDeviceMaster> appdevicemaster= new ArrayList<AppDeviceMaster>();
		Query query = session.createQuery("from AppDeviceMaster dp where dp.deviceId=:id");
		System.out.println("deviceid in daoimpl"+deviceid);
		query.setString("id", deviceid);
		appdevicemaster=   query.list();
		return appdevicemaster.get(0);
	}



	@Override
	public AppDeviceMaster getAppDeviceMasterById(AppDeviceMaster deviceMaster) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateDeviceparams(DeviceParams deviceparams) {
		Session session = sessionFactory.getCurrentSession();
		String deviceid=deviceparams.getAppDevice().getDeviceId();
		System.out.println("deviceparams daoimpl"+deviceparams.toString());
		if(deviceparams.getName().equalsIgnoreCase("hostname"));{
			Query query = session.createQuery(" update DeviceParams dp set dp.value=:value where dp.appDevice.deviceId=:app_device_id AND dp.name=:name");
			query.setString("value", deviceparams.getValue());
			query.setString("name",deviceparams.getName());
			query.setString("app_device_id", deviceid);
			int i=query.executeUpdate();
			if(i==0){
				saveDeviceparams(deviceparams);
			}
		}
         if(deviceparams.getName().equalsIgnoreCase("ip address"));{
        	 Query query = session.createQuery("update DeviceParams dp set dp.value=:value where dp.appDevice.deviceId=:app_device_id AND dp.name=:name");
        	 query.setString("value", deviceparams.getValue());
 			query.setString("name",deviceparams.getName());
     		query.setString("app_device_id", deviceid);
     		int i= query.executeUpdate();
     		if(i==0){
				saveDeviceparams(deviceparams);
			}
			
		}
         if(deviceparams.getName().equalsIgnoreCase("description"));{
        	 Query query = session.createQuery(" update DeviceParams dp set dp.value=:value where dp.appDevice.deviceId=:app_device_id AND dp.name=:name");
        	 query.setString("value", deviceparams.getValue());
 			query.setString("name",deviceparams.getName());
     		query.setString("app_device_id", deviceid);
     		int i= query.executeUpdate();
     		if(i==0){
				saveDeviceparams(deviceparams);
			}
 		}
	}

	@Override
	public void updateAppDeviceMaster(AppDeviceMaster appdevicemaster1) {
		Session session = sessionFactory.getCurrentSession();
		 Query query = session.createQuery(" update AppDeviceMaster dp set dp.deviceName=:name,dp.aliasName=:alias_name,dp.devicetype.deviceTypeCode=:device_type_code where dp.deviceId=:id");
    	 query.setString("id", appdevicemaster1.getDeviceId());
    	 query.setString("device_type_code", appdevicemaster1.getDevicetype().getDeviceTypeCode()); 
    	 query.setString("name", appdevicemaster1.getDeviceName());
 		query.setString("alias_name", appdevicemaster1.getAliasName());
 		query.executeUpdate();
	}
}
