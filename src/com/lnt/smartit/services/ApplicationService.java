package com.lnt.smartit.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.lnt.smartit.mysql.model.ApplicationMaster;
import com.lnt.smartit.mysql.model.DeviceApplicationMapping;

@Service
public interface ApplicationService {
	public List<ApplicationMaster> getAllApplicationMaster();
	public String saveNewApplication(ApplicationMaster applicationMaster);
	public String deleteApplication(ApplicationMaster applicationMaster);
	public ApplicationMaster getApplicationMasterById(ApplicationMaster applicationMaster);
	public List<DeviceApplicationMapping> getDeviceApplicationMappingById(String applicationId); 
}
