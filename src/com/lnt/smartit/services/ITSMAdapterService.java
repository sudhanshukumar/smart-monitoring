package com.lnt.smartit.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.lnt.smartit.models.AlertMessage;
import com.lnt.smartit.mysql.model.AppServiceComponentParams;

@Service
public interface ITSMAdapterService {
	public String getServiceCompNameForITSM();
	public List<AppServiceComponentParams> getServiceCompParamsForITSM();
	public List<String> getAletTypeForIncidentCreation(String hostname);
	public List<String> checkItsmReqFlag();
	public int getDeduplicationWindowForIncidentCreation(AlertMessage alertMsg);
	//10614080 on 09/03/2017 start
	Map<Integer, HashMap<String,String>> getNotificationDetails(String hostname);
	//10614080 on 09/03/2017 end
}
