package com.lnt.smartit.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lnt.smartit.mysql.dao.SoftwareDao;
import com.lnt.smartit.mysql.model.SoftwareMaster;
import com.lnt.smartit.services.SoftwareService;

@Service
public class SoftwareServiceImpl implements SoftwareService {
	@Autowired
	private SoftwareDao dao;
	
	public List<SoftwareMaster> getAllSoftwareMaster() {
		return dao.getAllSoftwareMaster();
	}

	public void saveNewService(SoftwareMaster softwareMaster) {
		dao.saveNewService(softwareMaster);
	}

	public SoftwareMaster getApplicationMasterById(SoftwareMaster softMaster) {
		return dao.getApplicationMasterById(softMaster);
	}

	public String deleteService(SoftwareMaster softMaster) {
		return dao.deleteService(softMaster);
	}
}
