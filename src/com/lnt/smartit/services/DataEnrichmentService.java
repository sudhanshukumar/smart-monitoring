package com.lnt.smartit.services;

import java.util.List;
import java.util.Map;

import com.lnt.smartit.models.GridHeaderModel;


public interface DataEnrichmentService {

	//public Object[][] enrichDataForExcelExport(String dataStrem);
	public String getExcelFilePath(String fileName);
	public List<Map<String, Object>> convertJsonStringToMap(String jsonString);
	public List<GridHeaderModel> convertJsonStringToObj(String jsonString);
	public Object[][] enrichDataForExport(List<Map<String, Object>> gidData, List<GridHeaderModel> headers);
	public Integer getColumnCount(List<GridHeaderModel> headers);	
}
