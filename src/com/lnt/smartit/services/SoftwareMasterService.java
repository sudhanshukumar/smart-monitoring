package com.lnt.smartit.services;

import java.util.List;

import com.lnt.smartit.mysql.model.DeviceSoftwareMapping;
import com.lnt.smartit.mysql.model.SoftwareMaster;

public interface SoftwareMasterService {
	public List<SoftwareMaster> getAllSoftwareMaster();
	public void saveNewService(SoftwareMaster softwareMaster);
	public SoftwareMaster getApplicationMasterById(SoftwareMaster softMaster);
	public String deleteService(SoftwareMaster softMaster);
	public List<DeviceSoftwareMapping> getDeviceSoftwareMappingById(String softwareId);
	public void deleteDeviceSoftwareMapping(List<DeviceSoftwareMapping> deviceSoftwareMappingList);
	public void saveDeviceSoftwareMapping(List<DeviceSoftwareMapping> deviceSoftwareMappingList);
	public List<SoftwareMaster> checkIfExists(SoftwareMaster softwareMaster);
	public SoftwareMaster getServiceRecordById(SoftwareMaster softwareMaster);
	public boolean exists(SoftwareMaster softwareMaster);
}
