package com.lnt.smartit.models;

import java.sql.Timestamp;


public class AccessLogs {
	
	private String appName;
	private Timestamp recordInsertedTime;
	private Long id;
	private String ip;
	private String dateTime;
	private String url;
	private String httpCode;
	private String httpStatus;
	private double dataBytes;
	private String day;
	private String month;
	private String year;
	private String hourSpan;
	private String weekDay;
	private float responseTime;
	private String userAgentString;
	private String host;
	private String auth;
	private String user;
	private String method;
	private String request;
	private String http_version;
	private long status;
	private long size;
	private long seconds;
	private long micresec;
	private String user_agent;

	public String getAuth() {
		return auth;
	}

	public void setAuth(String auth) {
		this.auth = auth;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getHttp_version() {
		return http_version;
	}

	public void setHttp_version(String http_version) {
		this.http_version = http_version;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public long getSeconds() {
		return seconds;
	}

	public void setSeconds(long seconds) {
		this.seconds = seconds;
	}

	public long getMicresec() {
		return micresec;
	}

	public void setMicresec(long micresec) {
		this.micresec = micresec;
	}

	public long getStatus() {
		return status;
	}

	public void setStatus(long status) {
		this.status = status;
	}

	public String getUser_agent() {
		return user_agent;
	}

	public void setUser_agent(String user_agent) {
		this.user_agent = user_agent;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Timestamp getRecordInsertedTime() {
		return recordInsertedTime;
	}

	public void setRecordInsertedTime(Timestamp recordInsertedTime) {
		this.recordInsertedTime = recordInsertedTime;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getHttpCode() {
		return httpCode;
	}

	public void setHttpCode(String httpCode) {
		this.httpCode = httpCode;
	}

	public String getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(String httpStatus) {
		this.httpStatus = httpStatus;
	}

	public double getDataBytes() {
		return dataBytes;
	}

	public void setDataBytes(double dataBytes) {
		this.dataBytes = dataBytes;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getHourSpan() {
		return hourSpan;
	}

	public void setHourSpan(String hourSpan) {
		this.hourSpan = hourSpan;
	}

	public String getWeekDay() {
		return weekDay;
	}

	public void setWeekDay(String weekDay) {
		this.weekDay = weekDay;
	}

	public float getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(float responseTime) {
		this.responseTime = responseTime;
	}

	public String getUserAgentString() {
		return userAgentString;
	}

	public void setUserAgentString(String userAgentString) {
		this.userAgentString = userAgentString;
	}

	@Override
	public String toString() {
		return "AccessLogs [ip=" + ip + ", dateTime=" + dateTime + ", url="
				+ url + ", httpStatus=" + httpStatus + ", host=" + host
				+ ", auth=" + auth + ", user=" + user + ", method=" + method
				+ ", request=" + request + ", http_version=" + http_version
				+ ", status=" + status + ", size=" + size + ", seconds="
				+ seconds + ", micresec=" + micresec + ", user_agent="
				+ user_agent + "]";
	}
}
