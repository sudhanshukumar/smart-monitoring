package com.lnt.smartit.models;

import java.io.Serializable;
import java.util.Date;
/**
 * 
 * @author 298038
 *
 */
public class AMQServerDown implements Serializable { 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String  deviceIpAddress;
	private String status;
	private Date datetime;
	
	public Date getDatetime() {
		return datetime;
	}
	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}
	public String getDeviceIpAddress() {
		return deviceIpAddress;
	}
	public void setDeviceIpAddress(String deviceIpAddress) {
		this.deviceIpAddress = deviceIpAddress;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "ServerDown [deviceIpAddress=" + deviceIpAddress + ", status="
				+ status + ", datetime=" + datetime + "]";
	}
}
