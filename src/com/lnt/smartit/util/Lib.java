package com.lnt.smartit.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONObject;

// TODO: Auto-generated Javadoc
/**
 * Utility class for any common functionality throughout the project.
 */
public class Lib {
	

	/**
	 * Returns the string implementation of any object.
	 * If the object is null, this method will return blank String.
	 * Especially important when showing numbers or null Strings to user.
	 *
	 * @param content the content
	 * @return the blank if null
	 */
	public static String getBlankIfNull(Object content) {
		if(content == null){
			return "";
		}		
		return String.valueOf(content);
	}

	/**
	 * Encodes a url to UTF-8 standards
	 * Throws an {@link UnsupportedOperationException} if unable to encode.
	 *
	 * @param urlText the text
	 * @return the string
	 */
	public static String urlEncodeUTF8(String urlText) {
		try {
            return URLEncoder.encode(urlText, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new UnsupportedOperationException(e);
        }
	}

	/**
	 * Gets the json response from URL.
	 *
	 * @param url the url
	 * @param parameters the parameters
	 * @return the json response
	 */
	public static String getJsonResponse(String url, Map<String, String> parameters)
	{
		//String url = "https://selfsolve.apple.com/wcResults.do";
		StringBuffer response = null;
		String jsonString = null;
		try 
		{
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			//add reuqest header
			con.setRequestMethod("POST");
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			if (parameters!=null) {
				for (Map.Entry<String, String> entry : parameters.entrySet())
				{
					con.addRequestProperty(entry.getKey(), entry.getValue());	
				}
				/*con.addRequestProperty("ticketNo", parameters.get("ticketNo"));
				con.addRequestProperty("action", parameters.get("action"));
				con.addRequestProperty("approverType",
						parameters.get("approverType"));*/
				//String urlParameters = "sn=C02G8416DRJM&cn=&locale=&caller=&num=12345";
			}
			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			//wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			jsonString  = response.toString();
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//print result
		return jsonString;
		
	}

	/**
	 * Utility method for putting value in a json object.
	 * This handles internally the exception thrown
	 *
	 * @param json the json
	 * @param key the key
	 * @param value the value
	 */
	public static void jsonPut(JSONObject json, String key, boolean value) {
		try{
			json.put(String.valueOf(key), value);
		}
		catch(Exception e){
			throw new UnsupportedOperationException(e);
		}
		
	}

	/**
	 * Utility method for putting value in a json object.
	 * This handles internally the exception thrown
	 *
	 * @param json the json
	 * @param key the key
	 * @param value the value
	 */
	public static void jsonPut(JSONObject json, String key, String value) {
		try{
			json.put(String.valueOf(key), value);
		}
		catch(Exception e){
			throw new UnsupportedOperationException(e);
		}
	}

	/**
	 * Utility method for putting value in a json object.
	 * This handles internally the exception thrown
	 *
	 * @param json the json
	 * @param key the key
	 * @param value the value
	 */
	public static void jsonPut(JSONObject json, String key, Object value) {
		try{
			json.put(String.valueOf(key), value);
		}
		catch(Exception e){
			throw new UnsupportedOperationException(e);
		}
	}
}
