/**
 * 
 */
package com.lnt.smartit.models;

import java.io.Serializable;

/**
 * @author 299273
 *
 */
public class AMQCPU implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Double loadAverage5min;
	private Double loadAverage10min;
	private Double loadAverage15min;
	private int dataReceivedTime_hours;
	//Threshold Limit 
	private Double threshold_loadAverage5min;
	private Double threshold_loadAverage10min;
	private Double threshold_loadAverage15min; 
	private double threshold_high_val;
	private double threshold_low_val;
	
	public double getThreshold_high_val() {
		return threshold_high_val;
	}

	public void setThreshold_high_val(double threshold_high_val) {
		this.threshold_high_val = threshold_high_val;
	}

	public double getThreshold_low_val() {
		return threshold_low_val;
	}

	public void setThreshold_low_val(double threshold_low_val) {
		this.threshold_low_val = threshold_low_val;
	}
	
	public Double getThreshold_loadAverage5min() {
		return threshold_loadAverage5min;
	}

	public void setThreshold_loadAverage5min(Double threshold_loadAverage5min) {
		this.threshold_loadAverage5min = threshold_loadAverage5min;
	}

	public Double getThreshold_loadAverage10min() {
		return threshold_loadAverage10min;
	}

	public void setThreshold_loadAverage10min(Double threshold_loadAverage10min) {
		this.threshold_loadAverage10min = threshold_loadAverage10min;
	}

	public Double getThreshold_loadAverage15min() {
		return threshold_loadAverage15min;
	}

	public void setThreshold_loadAverage15min(Double threshold_loadAverage15min) {
		this.threshold_loadAverage15min = threshold_loadAverage15min;
	}
		
	public int getDataReceivedTime_hours() {
		return dataReceivedTime_hours;
	}

	public void setDataReceivedTime_hours(int dataReceivedTime_hours) {
		this.dataReceivedTime_hours = dataReceivedTime_hours;
	}

	public Double getLoadAverage5min() {
		return loadAverage5min;
	}

	public void setLoadAverage5min(Double loadAverage5min) {
		this.loadAverage5min = loadAverage5min;
	}

	public Double getLoadAverage10min() {
		return loadAverage10min;
	}

	public void setLoadAverage10min(Double loadAverage10min) {
		this.loadAverage10min = loadAverage10min;
	}

	public Double getLoadAverage15min() {
		return loadAverage15min;
	}

	public void setLoadAverage15min(Double loadAverage15min) {
		this.loadAverage15min = loadAverage15min;
	}

	@Override
	public String toString() {
		return "AMQCPU [loadAverage5min=" + loadAverage5min
				+ ", loadAverage10min=" + loadAverage10min
				+ ", loadAverage15min=" + loadAverage15min
				+ ", dataReceivedTime_hours=" + dataReceivedTime_hours
				+ ", threshold_loadAverage5min=" + threshold_loadAverage5min
				+ ", threshold_loadAverage10min=" + threshold_loadAverage10min
				+ ", threshold_loadAverage15min=" + threshold_loadAverage15min
				+ "]";
	}
}
