package com.lnt.smartit.mysql.model;

// Generated Aug 11, 2014 11:29:59 AM by Hibernate Tools 3.4.0.CR1

/**
 * DeviceApplicationMapping generated by hbm2java
 */
@SuppressWarnings("serial")
public class DeviceApplicationMapping implements java.io.Serializable {

	private DeviceApplicationMappingId id;

	public DeviceApplicationMapping() {
	}

	public DeviceApplicationMapping(DeviceApplicationMappingId id) {
		this.id = id;
	}

	public DeviceApplicationMappingId getId() {
		return this.id;
	}

	public void setId(DeviceApplicationMappingId id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "DeviceApplicationMapping [id=" + id + "]";
	}
}
