package com.lnt.smartit.solr.pojo;

import java.util.List;

import com.lnt.smartit.mysql.model.DashboardMaster;

public class Dashboard {
	
	private List<Charting> chart_data;
	private DashboardMaster dashboard_data;
	
	public List<Charting> getChart_data() {
		return chart_data;
	}
	public void setChart_data(List<Charting> chart_data) {
		this.chart_data = chart_data;
	}
	public DashboardMaster getDashboard_data() {
		return dashboard_data;
	}
	public void setDashboard_data(DashboardMaster dashboard_data) {
		this.dashboard_data = dashboard_data;
	}
}
