$(document).ready(function() {
	
	$('#from_datepicker').datetimepicker({ 
		format:'Y-m-d H:i:s',
		dayOfWeekStart : 1, 
		lang:'en', 
		startDate: new Date() 
		});

	$('#from_datepicker').datetimepicker({
		value : '2014-01-01 00:00:00'
		
	});
	
	$('#to_datepicker').datetimepicker({ 
		format:'Y-m-d H:i:s',
		dayOfWeekStart : 1, 
		lang:'en', 
		startDate: new Date() 
		});
	
	$('#to_datepicker').datetimepicker({
		value : '2015-04-07 00:00:00'
	});
	
	getcollections();

});

function getcollections() {

	$.ajax({
		type : "GET",
		url : "getCollections.html",

		success : function(response) {

			for ( var i = 0; i < response.length; i++) {
				$("#collections1").append(
						$('<option>').text(response[i].col_name)).append(
						$('</option>'));
			}

			$("#col_title").html("shiksha_cloud");
			var col="shiksha_cloud";
			var date_filter="request_date:[2014-01-01T00:00:00Z TO 2015-04-03T12:59:59Z]";
			var start_facet="2014-01-01T00:00:00Z";
			var end_facet="2015-04-03T12:59:59Z";
			gettime(col,start_facet,end_facet);
			getpieuser(col,date_filter);
			getpieip(col,date_filter);
			getcolumnstatus(col,date_filter);
			getcolumnurl(col,date_filter);
			$('#collections1').change(
					function() {
						var selected_field = $('#collections1').find(":selected").text();
						$("#col_title").html(selected_field);
					});
		}
	});
}

function gettime(col,start_facet,end_facet) {
	$.ajax({
		type : "POST",
		url : "getCustomTimeSeries.html",
		data : "collection=" + col+"&query=*:*&start_facet="+start_facet+"&end_facet="+end_facet,
		success : function(response) {
			var timedata = new Array();
			for ( var i = 0; i < response.length; i++) {
				timedata[i] = [ response[i].date, response[i].size ];
			}

			$('#chart_container_time').highcharts('StockChart', {
				chart : {

				},
				title : {
					text : 'Application Load'
				// center
				},

				yAxis : {
					min : 0,
					title : {
						text : 'No of requests'
					},
					plotLines : [ {
						value : 0,
						width : 1,
						color : '#808080'
					} ]
				},

				legend : {
					layout : 'vertical',
					align : 'right',
					verticalAlign : 'middle',
					borderWidth : 0
				},
				series : [ {
					
					type : 'column',
					data : timedata

				} ]
			});
		}
	});
}

function getpieuser(col,date_filter) {
	$
			.ajax({
				type : "POST",
				url : "getCustomCharts.html",
				data : "collection=" + col + "&chart=pie&field=user&limit=10&query=*:*&filter="+date_filter,
				success : function(response) {
					var piedata = new Array();
					for ( var i = 0; i < response.length; i++) {
						if(response[i].lable=="-")
							{
							piedata[i] = [ "Not Specified", response[i].count ];
							}
						else
							{
						piedata[i] = [ response[i].lable, response[i].count ];
							}
					}

					$('#chart_container_user')
							.highcharts(
									{
										chart : {
											plotBackgroundColor : null,
											plotBorderWidth : null,
											plotShadow : false
											
										},
										title : {
											text :'User share'
										},
										tooltip : {
											pointFormat : '{series.name}: <b>{point.percentage:.1f}%</b>'
										},
										plotOptions : {
											pie : {
												allowPointSelect : true,
												cursor : 'pointer',
												dataLabels : {
													enabled : true,
													format : '<b>{point.name}</b>: {point.percentage:.1f} %'
												}
											}
										},
										series : [ {
											type : 'pie',
											
											data : piedata
										} ]
									});
				}
			});
}

function getpieip(col,date_filter) {
	$
			.ajax({
				type : "POST",
				url : "getCustomCharts.html",
				data : "collection=" + col + "&chart=pie&field=ip&limit=10&query=*:*&filter="+date_filter,
				success : function(response) {
					var piedata = new Array();
					for ( var i = 0; i < response.length; i++) {
						if(response[i].lable=="-")
							{
							piedata[i] = [ "Not Specified", response[i].count ];
							}
						else
							{
						piedata[i] = [ response[i].lable, response[i].count ];
							}
					}

					$('#chart_container_ip')
							.highcharts(
									{
										chart : {
											plotBackgroundColor : null,
											plotBorderWidth : null,
											plotShadow : false
											
										},
										title : {
											text :'IP share'
										},
										tooltip : {
											pointFormat : '{series.name}: <b>{point.percentage:.1f}%</b>'
										},
										plotOptions : {
											pie : {
												allowPointSelect : true,
												cursor : 'pointer',
												dataLabels : {
													enabled : true,
													format : '<b>{point.name}</b>: {point.percentage:.1f} %'
												}
											}
										},
										series : [ {
											type : 'pie',
											
											data : piedata
										} ]
									});
				}
			});
}

function getcolumnstatus(col,date_filter) {
	
	$.ajax({
		type : "POST",
		url : "getStatusCharts.html",
		data : "collection=" + col + "&chart=column&field=status&limit=10&query=*:*&filter="+date_filter,
		success : function(response) {
			var xcolumndata = new Array();
			var ycolumndata = new Array();
			
			for ( var i = 0; i < response.length; i++) {
				if(response[i].count!=0)
				{
				xcolumndata[i] = '"' + response[i].lable + '"';
				ycolumndata[i] = response[i].count;
				}
			}
				
			$('#chart_container_status').highcharts({
				chart : {
					type : 'column'
				},
				title : {
					text : 'Error Count'
				},
				xAxis : {
					categories : xcolumndata
				},
				series : [ {
					name : 'Error Count',
					data : ycolumndata

				} ]
			});
		}
	});
}

function getcolumnurl(col,date_filter) {
	
	$.ajax({
		type : "POST",
		url : "getURLCharts.html",
		data : "collection=" + col + "&chart=column&field=url&limit=10&query=*:*&filter="+date_filter,
		success : function(response) {
			var xcolumndata = new Array();
			var ycolumndata = new Array();
			for ( var i = 0; i < response.length; i++) {
				if(response[i].count!=0)
				{
				xcolumndata[i] = '"' + response[i].lable + '"';
				ycolumndata[i] = response[i].count;
				}
			}

			$('#chart_container_url').highcharts({
				chart : {
					type : 'column'
				},
				title : {
					text : 'URL Count'
				},
				xAxis : {
					categories : xcolumndata
				},
				series : [ {
					name : 'URL Count',
					data : ycolumndata
				} ]
			});
		}
	});
}

$(function() {
	
	$("#execute").click(function() {
		var from_date = $("#from_datepicker").val();
		var to_date = $("#to_datepicker").val();
		
		var temp_start_datetime=from_date.split(" ");
		var final_from_date=temp_start_datetime[0]+"T"+temp_start_datetime[1]+"Z";
		
		var temp_end_datetime=to_date.split(" ");
		var final_to_date=temp_end_datetime[0]+"T"+temp_end_datetime[1]+"Z";
		
		var col = "shiksha_cloud";
		var date_filter="request_date:["+final_from_date+" TO "+final_to_date+"]";
		
		var start_facet=final_from_date;
		var end_facet=final_to_date;
		
		gettime(col,start_facet,end_facet);
		getpieuser(col,date_filter);
		getpieip(col,date_filter);
		getcolumnstatus(col,date_filter);
		getcolumnurl(col,date_filter);

	});
});
