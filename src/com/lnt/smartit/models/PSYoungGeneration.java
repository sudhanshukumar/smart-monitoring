/**
 * 
 */
package com.lnt.smartit.models;


/**
 * @author 299273
 *
 */
public class PSYoungGeneration {
	
	private Space edenSpace;
	
	private Space fromSpace;
	
	private Space toSpace;

	public Space getEdenSpace() {
		return edenSpace;
	}

	public void setEdenSpace(Space edenSpace) {
		this.edenSpace = edenSpace;
	}

	public Space getFromSpace() {
		return fromSpace;
	}

	public void setFromSpace(Space fromSpace) {
		this.fromSpace = fromSpace;
	}

	public Space getToSpace() {
		return toSpace;
	}

	public void setToSpace(Space toSpace) {
		this.toSpace = toSpace;
	}

	@Override
	public String toString() {
		return "PSYoungGeneration [edenSpace=" + edenSpace + ", fromSpace="
				+ fromSpace + ", toSpace=" + toSpace + "]";
	}
	
}
