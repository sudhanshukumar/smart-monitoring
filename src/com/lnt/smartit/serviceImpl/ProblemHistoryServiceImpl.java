package com.lnt.smartit.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lnt.smartit.mysql.dao.ProblemHistoryDao;
import com.lnt.smartit.mysql.model.Alertmessageinfo;
import com.lnt.smartit.services.ProblemHistoryService;
import com.lnt.smartit.solr.pojo.JqGridData;

@Service
public class ProblemHistoryServiceImpl implements ProblemHistoryService{
	@Autowired
	private ProblemHistoryDao historyDao;
	/**************Changes done by Pinakin on 21st August Starts here******************/
	//Changed method definition to achieve pagination
	public List<Alertmessageinfo> getProblemHistory(Integer pageNo, Integer pageRowCount, Boolean cache){
		return historyDao.getProblemHistory(pageNo, pageRowCount, cache);
	}

	// This method is added to get Total no of records present in AlertMessageInfo table.
	public Integer getProblemHistoryRecordCount(){
		return historyDao.getProblemHistroyRecordsCount();
	}

	// This method is added to get the last page no based on the selected windowsize. 
	public Integer getProblemHistoryLastPageNo(Integer pageRowCount, Boolean cache)
	{
		Integer totalRecords = getProblemHistoryRecordCount();
		if(totalRecords%pageRowCount == 0){
			return (totalRecords/pageRowCount);
		}
		else{
			return (totalRecords/pageRowCount)+1;
		}
	}
	/**************Changes done by Pinakin on 21st August Ends here******************/
	@Override
	public List<String> getDistinctHostnames() {
		return historyDao.getDistinctHostnames();
	}

	@Override
	public List<String> getDistinctParameters() {
		return historyDao.getDistinctParameters();
	}
	
	@Override
	public List<String> getDistinctAlertTypes() {
		return historyDao.getDistinctAlerttypes();
	}
	
	@Override
	public JqGridData getProblemHistoryWithCriteria(Integer pageNo, Integer pageRowCount, Boolean cache, String app, String hostname, String parameter, String alertType, String fromDate,String toDate){
		return historyDao.getProblemHistoryWithCriteria(pageNo, pageRowCount, cache, app, hostname, parameter, alertType, fromDate, toDate);
	}

	@Override
	public void load() {
		historyDao.connectDatabase();
	}
}
