package com.lnt.smartit.mysql.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="app_device_param")
public class DeviceParams implements Serializable{
	private static final long serialVersionUID = 1L;
	@GeneratedValue
	@Id
	@Column(name="id")
	private String deviceParamId;
	
	@ManyToOne
	@JoinColumn(name="app_device_id", referencedColumnName = "id")
	private AppDeviceMaster appDevice;
	
	@Column(name="name")
	private String name;
	
	@Column(name="value")
	private String value;
	
	@Column(name="void_ind")
	private String voidInd;

	public String getDeviceParamId() {
		return deviceParamId;
	}

	public void setDeviceParamId(String deviceParamId) {
		this.deviceParamId = deviceParamId;
	}

	public AppDeviceMaster getAppDevice() {
		return appDevice;
	}

	public void setAppDevice(AppDeviceMaster appDevice) {
		this.appDevice = appDevice;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getVoidInd() {
		return voidInd;
	}

	public void setVoidInd(String voidInd) {
		this.voidInd = voidInd;
	}

	public DeviceParams(AppDeviceMaster appDevice, String name, String value,
			String voidInd) {
		super();
		this.appDevice = appDevice;
		this.name = name;
		this.value = value;
		this.voidInd = voidInd;
	}
 public DeviceParams(){}

@Override
public String toString() {
	return "DeviceParams [deviceParamId=" + deviceParamId + ", appDevice="
			+ appDevice + ", name=" + name + ", value=" + value + ", voidInd="
			+ voidInd + "]";
}
	
	
}
