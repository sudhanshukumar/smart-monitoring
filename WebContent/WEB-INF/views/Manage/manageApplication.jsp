<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><spring:message code="views.manage.application.title"></spring:message></title>

<!-- common css -->
<link href="css/custom/general/style1366.css" rel="stylesheet" />
<link href="css/custom/general/style1024.css" rel="stylesheet" />
<link href="css/custom/general/bluetabs.css" rel="stylesheet" />

<!-- import css -->
<link href="css/common/ui.jqgrid.css" rel="stylesheet" type="text/css" />
<link href="css/common/jquery-ui-1.9.1.custom.css" rel="stylesheet"
	type="text/css" />

<!-- jQuery scripts -->
<script src="scripts/common/jquery-1.9.1.js"></script>
<script src="scripts/common/grid.locale-en.js"></script>
<script src="scripts/common/jquery.jqGrid.min.js"></script>
<script src="scripts/common/jquery-ui.js"></script>



<!-- custom script -->
<script src="scripts/custom/manageApplication.js"></script>
<script src="scripts/common/common.js"></script>

<!-- jQuery style sheets -->
<link href="css/common/jquery-ui.css" rel="stylesheet" />

<!-- custom stylesheet -->
<link href="css/specific/manageApplication.css" />
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
</style>

</head>
<body>

	<header>
		<%@ include file="../common/header.jsp"%>
	</header>


	<nav>
		<%@ include file="../Menu/admin.jsp"%>
	</nav>

	<div id="manageApplicationInnerContainer" class="allInnerContainer">
		<div id="alert"></div>
		<!-- <div>
				<input type="button" id="addNewApplication"
					value="+ New Application" class="buttonClass addNew" />
			</div> -->
		<div id="applicationGrid" class="grid_manageDevice">
			<table id="addedApplicationGrpHistory"></table>
			<div id="addedApplicationGrpPager"></div>
		</div>

		<div>
			<input type="button" id="addNewApplication" value="+ New Application"
				class="buttonClass addNew" />
		</div>

	</div>
	<div id="confirmDelete"></div>
<div id ="alert"></div>	
	<footer>
		<%@ include file="../common/footer.jsp"%>
	</footer>

</body>
</html>