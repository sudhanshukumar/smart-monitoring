function loadvmstatus(i, parameterName, dataobj){
		//clearAccordion('ac6_container_small');
	 var hostName = dataobj.alertMessageHostName;
		
		//check for . since it gives error as illegal character
		if(hostName.indexOf(".") > -1) {
			hostName = hostName.split(".").join("_");
		}
		var myDate = new Date(dataobj.serverDown.datetime);
		var yyyy = myDate.getFullYear();
		var month = myDate.getMonth() + 1;
		var date = myDate.getDate();
		var sec = myDate.getSeconds();
		var hour = myDate.getHours();
		var min = myDate.getMinutes();

		$("#label_ac-6").show();
		$("#ac6_block_1").append(
				"<div class='block_split1' id='ac-6_split1_"
						+ hostName + "'>" + "<table id='ac-6_split1_table_"
						+ (i + 1)
						+ "' class='ac-6_split1_table_content'><thead><tr><th>"
						+ strings['ac-6_split1.header']
						+ "<span id='ac-6_split1_col1_"
						+ dataobj.alertMessageHostName
						+ "'>" + dataobj.serverDown.deviceIpAddress
						+ "</span></th></tr></thead></table>" + "</div>"
						+ "</div>");
		var appendRow = "<tr></tr><tr></tr><tr></tr><tr><td> Down Since : <span id='ac-6_split1_col2_"
				+ hostName
				+ "'>"
				+ date
				+ "-"
				+ month
				+ "-"
				+ yyyy
				+ "  "
				+ hour
				+ ":"
				+ min
				+ ":" + sec + "</span></td></tr>";

		$('#ac-6_split1_table_' + (i + 1)).last().append(appendRow);
}
	