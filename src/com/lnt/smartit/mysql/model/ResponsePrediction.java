package com.lnt.smartit.mysql.model;

import java.io.Serializable;

public class ResponsePrediction implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String url;
	private String logicalurl;
	private double predicted;
	private String hourspan;
	private String algoName;
	private String prediction_Date;
	private int hour;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getLogicalurl() {
		return logicalurl;
	}
	public void setLogicalurl(String logicalurl) {
		this.logicalurl = logicalurl;
	}
	public double getPredicted() {
		return predicted;
	}
	public void setPredicted(double predicted) {
		this.predicted = predicted;
	}
	public String getHourspan() {
		return hourspan;
	}
	public void setHourspan(String hourspan) {
		this.hourspan = hourspan;
	}
	public String getAlgoName() {
		return algoName;
	}
	public void setAlgoName(String algoName) {
		this.algoName = algoName;
	}
	public String getPrediction_Date() {
		return prediction_Date;
	}
	public void setPrediction_Date(String prediction_Date) {
		this.prediction_Date = prediction_Date;
	}
	public int getHour() {
		return hour;
	}
	public void setHour(int hour) {
		this.hour = hour;
	}
	
	

}
