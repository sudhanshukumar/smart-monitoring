$(document).ready(function() {
	$("#alert").hide();
	$("#update_user_submit").hide();
	$("#confirmDelete").hide();
	$("#confirmEdit").hide();
	// click fundtion to move to add new device
	$("#addNewDevice").click(function() {
		window.location.href="device.html";
	});

//temp local grid is displayed
manageDeviceJqGrid();
});

var addedDeviceRowdata = "";
function manageDeviceJqGrid() {
	$("#addedDeviceHistory").jqGrid(
			{
				jsonReader : {
					root : "gridData", // an array that contains the actual data
					page : "page", // current page of the query
					total : "total", // total pages for the query
					records : "records",// total number of records for the query
					repeatitems : false
					//id : "deviceId" // the unique id of the row
				},
				url : 'populateDeviceGrid.html',
				datatype : "json",
				mtype : "POST",
				colModel : [
                                
							{
								name : 'deviceid',
								label : 'Device Id',
								align : 'left',
								width : 30,
								hidden:true,
								sortable: true
							},
							{
								name : 'hostname',
								label : 'Host name',
								align : 'left',
								width : 90,
								sortable: true
							},
							{
								name : 'ip',
								label : 'IP Address',
								align : 'left',
								width : 90,
								sortable: true
							},
							{
								name : 'description',
								label : 'Description',
								align : 'left',
								width : 90,
								sortable: true
							},
							/*{
								name : 'monitoringStatus',
								label : 'Monitoring Status',
								align : 'left',
								width : 90,
								sortable: true
							},*/{
								label : 'Actions',
								width : 90,
								fixed : true,
								sortable : false,
								resize : false,
								formatter : function(cellvalue, options,
										rowObject) {

									return addedDeviceActionButtons(cellvalue,
											options, rowObject);
								}
							} ],
							
							pager : '#addedDeviceHistoryPager',
							rowNum : 10,
							rowList : [ 10, 20, 30 ],
							viewrecords : true,
							gridview : true,
							loadonce: true,
							width : 900,
							height: 300
			});
}

function addedDeviceActionButtons(cellvalue, options, rowObject) {
	/*var previewDevLink= '<img title="Preview" onClick=previewDevice("'
	    +encodeURI(JSON.stringify(rowObject))
	    +'") src="images/common/preview-16.png"/>';*/
	var editDevLink = '<img title="Edit" onClick=editDevice("'
			+ encodeURI(JSON.stringify(rowObject))
			+ '") src="images/common/edit-6-16.png"/>';
	var deleteDevLink = '<img title="Delete" onClick=deleteDevice("'
		+ encodeURI(JSON.stringify(rowObject))
		+ '") src="images/common/delete-3-16.png"/>';
	var actionDeviceLink =  "&nbsp;&nbsp;&nbsp;" + editDevLink+ "&nbsp;&nbsp;&nbsp;" + deleteDevLink;
	return actionDeviceLink;
}
function editDevice(value)
{
	var dataJSON = JSON.parse(decodeURI(value));
	var deviceid=dataJSON.deviceid;	
	
	window.location.href =  "device.html?deviceid="+deviceid+"&action=edit";;
}

function previewDevice(value)
{
	var dataJSON = JSON.parse(decodeURI(value));
	var deviceid=dataJSON.deviceid;	
	window.location.href =  "device.html?deviceid="+deviceid+"&action=preview";
}

function deleteDevice(value){
	$("#confirmDelete").show();
	$( "#confirmDelete" ).dialog({
		 resizable: false,
		 autoOpen: false,
		 height:200,
		 width: 340,
		 position: [485, 185],
		 modal: true,
		 buttons: {
			 "Yes": function() {
	
				 var dataJSON = JSON.parse(decodeURI(value));
				 var deviceid=dataJSON.deviceid;
				 
				 $.ajax({
					 type : "POST",
					 url : "deleteDevice.html",
					 data : "deviceid=" + deviceid, 
					 success : function(response)
					 {
							$( "#confirmEdit" ).show();
							$( "#confirmEdit" ).dialog({ buttons:{"Ok":function() {
								window.location.href = "manageDevice.html";
							}} });
					 },
					 	cache: false
				 	}); 
	
			 },
				Cancel: function() {
			
	 							$( this ).dialog( "close" );
	 						}
		 }
		 
	});
	
	$("#confirmDelete").dialog("open");
	$(".ui-dialog-title").text('Delete?');
	$(".ui-dialog-titlebar-close").hide();
	$("#confirmDelete").text("Delete this device ?");
}

	