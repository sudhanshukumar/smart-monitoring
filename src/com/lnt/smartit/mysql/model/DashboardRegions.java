package com.lnt.smartit.mysql.model;

import java.util.List;

public class DashboardRegions {

	private  String region;
	private List<String> platforms;
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public List<String> getPlatforms() {
		return platforms;
	}
	public void setPlatforms(List<String> platforms) {
		this.platforms = platforms;
	}
	@Override
	public String toString() {
		return "DashboardRegions [region=" + region + ", platforms="
				+ platforms + "]";
	}
	
	
	
}
