/**
 * 
 */
package com.lnt.smartit.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 294593
 *
 */
public class AllMachinesAlertMessages implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * @param args
	 */
	
	private List<AlertMessage> alertMessageList = new ArrayList<AlertMessage>();

	public List<AlertMessage> getAlertMessageList() {
		return alertMessageList;
	}

	public void setAlertMessageList(List<AlertMessage> alertMessageList) {
		this.alertMessageList = alertMessageList;
	}
}
