package com.lnt.smartit.mysql.dao;

import java.util.List;

import com.lnt.smartit.mysql.model.ThresholdLogSize;

public interface ThresholdLogSizeDao {

	public void saveNewThresholdLogSizeRecord(ThresholdLogSize thresholdLogSizeRecord);
	public ThresholdLogSize getThresholdLogSizeById(ThresholdLogSize thresholdLogSizeRecord);
	public String deleteThresholdLogSizeRecord(ThresholdLogSize thresholdLogSizeRecord);
	List<ThresholdLogSize> getAllThresholdLogSizeRecords();
	public Boolean exists(ThresholdLogSize thresholdLogSizeRecord);
}
