<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- common css -->
<link href="css/custom/general/style1366.css" rel="stylesheet" />
<link href="css/custom/general/style1024.css" rel="stylesheet" />
<link href="css/custom/general/bluetabs.css" rel="stylesheet" />

<!-- import css -->
<link href="css/common/ui.jqgrid.css" rel="stylesheet" type="text/css" />
<link href="css/common/jquery-ui-1.9.1.custom.css" rel="stylesheet"
	type="text/css" />

<!-- jQuery scripts -->
<script src="scripts/common/jquery-1.9.1.js"></script>
<script src="scripts/common/grid.locale-en.js"></script>
<script src="scripts/common/jquery.jqGrid.min.js"></script>
<script src="scripts/common/jquery-ui.js"></script>

<!-- custom css -->
<link href="css/specific/newUser.css" rel="stylesheet" />

<!-- fonts -->
<link href='http://fonts.googleapis.com/css?family=Roboto:500'
	rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans'
	rel='stylesheet' type='text/css'>


<!-- custom script -->
<script src="scripts/custom/newUser.js"></script>
<script src="scripts/common/common.js"></script>

<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
</style>
</head>
<body>
	<header>
		<%@ include file="../../common/header.jsp"%>
	</header>


	<nav>
		<%@ include file="../../Menu/admin.jsp"%>
	</nav>

	<div id="newDeviceInnerContainer" class="allInnerContainer">
		<div id="alert"></div>
		<div id="userDetails">
			<!-- this div will have all the text boxes and check for adding new device -->
			<div id="userManagementHeading" class="topic addDeviceClass">
				<label id="" class="topic addDeviceClass"></label>
			</div>

			<hr id="horizontalLine">
			<table id="addUser" class="itPredictInp" cellspacing="14px"
				cellpadding="5px">
				<tr>
					<td class="col1"><label>Name<span class="mandatory">*</span></label></td>
					<td class="col2"><input type="text" id="name"
						placeholder="Name" /></td>
				</tr>
				<tr>
					<td class="col1"><label>Email Id<span
							class="mandatory">*</span></label></td>
					<td class="col2"><input type="text" id="psno"
						placeholder="Email Id" /></td>
				</tr>

				<tr>
					<td class="col1"><label>Username<span
							class="mandatory">*</span></label></td>
					<td class="col2"><input type="text" id="username"
						placeholder="Username" /></td>

				</tr>

				<tr>
					<td class="col1"><label id="passwordLabel">Password<span
							class="mandatory">*</span></label></td>
					<td class="col2"><input type="password" id="password"
						placeholder="Password" /></td>
				</tr>

				<!-- 	<tr>
						<td class="col1"><label>Confirm Password<span class="mandatory">*</span></label></td>
						<td class="col2"><input type="text" id="confirmPassword" placeholder="Password"/></td>
					</tr> -->


				<tr>
					<td class="col1"><label>Role<span class="mandatory">*</span></label></td>
					<td class="col2"><select id="userRole">
							<c:forEach var="roles" items="${roles}">
								<option value=${roles.name}>${roles.name}</option>
							</c:forEach>
							<!-- <option value="ROLE_USER">User</option>
 						    <option value="ROLE_ADMIN">Admin</option> -->

					</select></td>
				</tr>

			</table>
			<hr id="horizontalLine">
		</div>

		<div id="add_user_buttons" class="buttonPosition">
			<table>
				<tr>
					<td>
						<button id="add_user_submit"
							class="buttonClass buttonClass_submit">Save</button>

						<button id="update_user_submit"
							class="buttonClass buttonClass_submit">Update</button>
					</td>
					<td><button id="add_user_cancel"
							class="buttonClass buttonClass_cancel">Cancel</button></td>
				</tr>
			</table>

		</div>
	</div>
	
	<div id="preview_user_buttons" class="buttonPosition">
			<table>
				<tr>
					<td>
						<button id="preview_ok"
							class="buttonClass buttonClass_submit">Ok</button>
					</td>
					
				</tr>
			</table>

		</div>
	

	<div id="confirmDelete"></div>
<div id ="alert"></div>
	<footer>
		<%@ include file="../../common/footer.jsp"%>
	</footer>


</body>
</html>