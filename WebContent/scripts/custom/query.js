var gridCols=[];
var modelFields=[];
$(document).ready(function() {
	$("#alertDate").hide();
	$("#alertSpace").hide();
	$("#alertLength").hide();
	$('[data-toggle="tooltip"]').tooltip();
	getapplication();
	$("#advanced1").hide();
});

function getapplication() {
	var col = $('#logs').val();
	getschema(col);
	/*var date_filter="Date:[2015-03-02T00:00:00Z TO 2015-04-06T12:59:59Z]";
	getsortingfields(col);
	var query = $("#query").val();
	var start = $("#start").val();
	var rows = $("#rows").val();
	var filter = date_filter;

	var sort_field = null;
	var sort_order = $('#sorting_order').find(":selected").text();

	var fields = [];
	$.each($("input[name='select_fields']:checked"), function() {
		fields.push($(this).val());
	});
	getresults(col, query, start, rows, filter, sort_field, sort_order);*/
}
// This method is added by Pinakin on 8/6/2016. This is a generic method which will construct grid 
function drawGidChart(griData, colmodelObj){
	$("#outId").jqGrid(                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        {
			datatype : "local",
			data : griData,
		    colNames:gridCols,
		    colModel : colmodelObj,
		    viewrecords : true,
		    gridview : true,
		    width: 950,
		    height: '100%',
		    forceFit:true,
			shrinkToFit:false,
			caption:"",
			loadonce: false,
			jsonReader: {
							repeatitems: false,
							root: function (obj) { 
													console.log(obj);
													return obj; 
												  },
			                page: function (obj) { return 1; },
			                total: function (obj) { return 1; },
			                records: function (obj) { return obj.length; }                  
						},
					pager : '#chartGridPager',
					rowNum : 15,
					rowList : [ 5, 10, 15 ],
					viewrecords : true,
					gridview : true,
					loadonce: false,
		}); 

	}


function refresh(){
	$('#chart_container').empty();
	//$('#result_grid').empty();
	$('#outId ').jqGrid('GridDestroy');
}
function updateColModel(){
	gridCols=[];
	 modelFields=[];
	
	$.each(
			$("input[name='select_fields']:checked"),
			function() {
				var fieldvalue=$(this)
				.val();
				//pusharray(fieldvalue);
				modifiedpusharray(fieldvalue,false);
			});
	
	$.each(
			$("input[name='select_fields']:not(:checked)"),
			function() {
				var fieldvalue=$(this)
				.val();
				modifiedpusharray(fieldvalue,true);
			});
}
function modifiedpusharray(fieldvalue, isHidden){
	gridCols.push(fieldvalue);
	var modelFieldsArr={
  		  name : fieldvalue,
  		  index : fieldvalue,	
  		  width : 150,
  		  hidden: isHidden
     };
	modelFields.push(modelFieldsArr);
}
function getcollections() {

	$.ajax({
				type : "GET",
				url : "getCollections.html",

				success : function(response) {
					for ( var i = 0; i < response.length; i++) {
						$("#collections1").append(
								$('<option class="text-style">').text(response[i].col_name))
								.append($('</option>'));
					}

					$("#col_title").html(response[0].col_name);
					getsortingfields(response[0].col_name);
					$('#collections1')
							.change(
									function() {
										var selected_field = $('#collections1').find(":selected").text();

										$("#col_title").html(selected_field);
										getsortingfields(selected_field);
										var query = $("#query").val();
										var start = $("#start").val();
										var rows = $("#rows").val();
										var filter = $("#filter").val();
										var col = $("#col_title").text();
										var sort_field = $('#sorting').find(
												":selected").text();
										var sort_order = $('#sorting_order')
												.find(":selected").text();
										$("#result_grid").show(500);
										var fields = [];
								    	$.each(
												$("input[name='select_fields']:checked"),
												function() {
													fields.push($(this)
															.val());
												});

										getresults(col, query, start, rows,
												filter, sort_field, sort_order);
									});
				}
			});
}

function getresults(col, query, start, rows, filter, sort_field, sorting_order) {
	$.ajax({
		type : "POST",
		url : "getBasicQuery.html",
		data : "query=" + query + "&start=" + start + "&rows=" + rows
				+ "&filter=" + filter + "&collection=" + col + "&sort_field="
				+ sort_field + "&sort_order=" + sorting_order,
		success : function(response) {
			getschema(col);
		}
	});
}

function exportTableToPDF(fileName) {
	$('#outId').tableExport({type: "pdf", escape: false, fileName: fileName});
}

function exportAndDownload(fileName,sheetName,task) {

	//alert("Export Button Click Event fired through onClick Event....!!!");
	var headerString=modelFields;
	var gridData = $("#outId").getRowData();
		
	var form = document.createElement("form");    
	form.setAttribute("method", "post");    
	form.setAttribute("action", "exportToFile.html");
	
	form._submit_function_ = form.submit;
	
	var hiddenHeaderField = document.createElement("input");
	hiddenHeaderField.setAttribute("type", "hidden");            
	hiddenHeaderField.setAttribute("name", "headerString");           
	hiddenHeaderField.setAttribute("value", JSON.stringify(headerString));
	
	var hiddenSheetName = document.createElement("input");
	hiddenSheetName.setAttribute("type", "hidden");            
	hiddenSheetName.setAttribute("name", "sheetName");           
	hiddenSheetName.setAttribute("value", sheetName);
	
	var hiddenTableData = document.createElement("input");
	hiddenTableData.setAttribute("type", "hidden");            
	hiddenTableData.setAttribute("name", "gridData");           
	hiddenTableData.setAttribute("value", JSON.stringify(gridData));
	
	var hiddenTask = document.createElement("input");
	hiddenTask.setAttribute("type", "hidden");            
	hiddenTask.setAttribute("name", "task");           
	hiddenTask.setAttribute("value", task);
	
	var hiddenFileName = document.createElement("input");
	hiddenFileName.setAttribute("type", "hidden");            
	hiddenFileName.setAttribute("name", "fileName");           
	hiddenFileName.setAttribute("value", fileName);
	
	form.appendChild(hiddenHeaderField);
	form.appendChild(hiddenSheetName);
	form.appendChild(hiddenTableData);
	form.appendChild(hiddenTask);
	form.appendChild(hiddenFileName);
	document.body.appendChild(form);   
	form._submit_function_();
	
	//alert("Table Header : "+headerString+"\n"+"Table Data : "+tableData+"\n"+"Task : "+task+"\n"+"File Name : "+fileName+"\n"+"Sheet Name : "+sheetName);
}

function exportToPDF(fileName) {
	console.log("Inside getData");
	var tableHeader = [];
	var tableData = [];	
	$("#outId").find('thead').each(function() {
		
			$(this).find('th').each(function() {
				tableHeader.push($(this).html());
			});
	});
	
	$("#outId").find("tr:gt(0)").each(function() {
		var tableRow = [];	
	        $(this).find('td').each(function() {
	        	tableRow.push($(this).html());
	        });
	        tableData.push(tableRow);
	  });	
	var doc = new jsPDF('p', 'pt');
	doc.autoTable(tableHeader, tableData, {
    styles: {fillColor: [250, 170, 150], overflow: 'linebreak'},
    theme: 'grid',
    tableWidth: 'auto',
    valign: 'middle',
    columnStyles: {
        id: {fillColor: 0},
        valign: 'middle'
    },
    headerStyles: {
        id: {fillColor: 0}
    },
    margin: {top: 60}
	});
	doc.save(fileName);
}
function exportToExcel(fileName) {

	var str="";
	var a = document.createElement('a');
	$("#outId").find('thead').each(function() {
		
			$(this).find('th').each(function() {

				str=str+$(this).html()+"\t";
			});
			str=str+"\n";
	});	

	$("#outId").find('tbody').each(function() {

	       $(this).find('tr').each(function(){
	    	
	    	   $(this).find('td').each(function() {

	    		   var temp = $(this).html();
                   temp = temp.replace(new RegExp('\r?\n','g'), ' ');
                   str=str+temp+"\t";

	    	   });
	    	   str=str+"\n";
	       
	       });

	  });
	window.open('data:application/vnd.ms-excel,' + encodeURIComponent(str));
}

var createExcelFromGrid = function(filename) {
   // var grid = $('#' + gridID);
	var grid = $('#outId');
    var rowIDList = grid.getDataIDs();
    var row = grid.getRowData(rowIDList[0]); 
    var colNames = [];
    var i = 0;
    for(var cName in row) {
        colNames[i++] = cName; // Capture Column Names
    }
    var html = "";
    for(var j=0;j<rowIDList.length;j++) {
        row = grid.getRowData(rowIDList[j]); // Get Each Row
        for(var i = 0 ; i<colNames.length ; i++ ) {
            html += row[colNames[i]] + ';'; // Create a CSV delimited with ;
        }
        html += '\n';
    }
    html += '\n';

    var a         = document.createElement('a');
    a.id = 'ExcelDL';
    a.href        = 'data:application/vnd.ms-excel,' + html;
    a.download    = filename ? filename + ".xls" : 'DataList.xls';
    document.body.appendChild(a);
    a.click(); // Downloads the excel document
    document.getElementById('ExcelDL').remove();
}

function getselectedfields(col, query, start, rows, filter, sort_field,
		sorting_order, fields) {
	$.ajax({
		type : "POST",
		url : "getSelectedFieldsQuery.html",
		data : "query=" + query + "&start=" + start + "&rows=" + rows
				+ "&filter=" + filter + "&collection=" + col + "&sort_field="
				+ sort_field + "&sort_order=" + sorting_order + "&fields="
				+ fields,
		success : function(response) {
			for ( var i = 0; i < response.length; i++) {
				var temp_date = response[i].Date;
				temp_date = $.format.date(temp_date, "dd/MM/yyyy HH:mm:ss.SSS");
				response[i].Date = temp_date;
			}
			gridView=$('#result_grid');
			jQuery.data(gridView, "gridDataArray", response );
			updateColModel();
			drawGidChart(response, modelFields);
		
		}
	});
}
function getLogData(collectionName, from_date, to_date) {
	var queryParams = {
             			'collection' : collectionName,
             			'from_date' : from_date, 
						'to_date' : to_date
					  	};
	$.ajax({
		type 		: "POST",
		/*datatype 	: "json",*/
	    url			:'getQueryForGrid.html',
	    data		: "collection=" + collectionName + "&from_date=" + from_date + "&to_date=" + to_date,
		success : function(response) {
			
			for ( var i = 0; i < response.length; i++) {
				var temp_date = response[i].Date;
				temp_date = $.format.date(temp_date, "dd/MM/yyyy HH:mm:ss.SSS");
				response[i].Date = temp_date;
			}
			gridView=$('#result_grid');
			jQuery.data(gridView, "gridDataArray", response );
			refresh();
			$( "#result_grid").append( "<table id='outId'></table><div id='chartGridPager'></div>" );
			updateColModel();
			drawGidChart(response, modelFields);
		}
	});	
}
function checkallfields(){
	$('.check').prop('checked', true);
	$("#result_grid").show(500);
	refresh();
	$( "#result_grid").append("<table id='outId'></table><div id='chartGridPager'></div>" );
	updateColModel();
	drawGidChart(jQuery.data(gridView,"gridDataArray"),modelFields);
}

function uncheckallfields(){
	$('.check').prop('checked', false);
	$("#result_grid").show(500);
	refresh();
	$( "#result_grid").append("<table id='outId'></table><div id='chartGridPager'></div>" );
	updateColModel();
	drawGidChart(jQuery.data(gridView,"gridDataArray"),modelFields);
}

function getschema(col_name) {

	$.ajax({
				type : "POST",
				url : "getSchema.html",
				data : "collection=" + col_name,
				success : function(response) {

					$("#resId").find("tr").remove();
					$("#query_field").empty();
					$("#query_field").append('<option value="" class="text-style" selected >Select</option>');
					var fields = [];
					for ( var i = 0; i < response.length; i++) {
						if (response[i].name != null) {
							$("#resId").find('tbody').append(
									$('<tr>').append(
											$('<td>').append(
													'<input class="check text-style" name="select_fields" type="checkbox" value='
															+ response[i].name
															+ '>').append(
													$('</td>'))).append(
											$('<td>').text(response[i].name)
													.append($('</td>'))));
							fields.push(response[i].name);
						}
					}
					fields.sort();
					for(var i=0; i<fields.length; i++){
						$("#query_field").append('<option class="text-style" value="'+fields[i]+'">'+fields[i]+'</option>');						
					}

					$('.check').prop('checked', true);
					refresh();
					$("#result_grid").show(500);
					$( "#result_grid").append( "<table id='outId'></table><div id='chartGridPager'></div>" );
					
					var date_filter="";	
					var from_date = $("#from_datepicker").val();
					var to_date = $("#to_datepicker").val();
					var temp_start_datetime = from_date.split(" ");
					var final_from_date = temp_start_datetime[0]+ "T"+ temp_start_datetime[1]+ "Z";
					var temp_end_datetime = to_date.split(" ");
					var final_to_date = temp_end_datetime[0]+ "T"+ temp_end_datetime[1]+ "Z";
					date_filter = "Date:["+ final_from_date + " TO "+ final_to_date + "]";
					
					/*var selected_field = $('#logs').find(":selected").val();
					var sort_field = $('#sorting').find(":selected").text();
					var query = $("#query").val();
					var start = $("#start").val();
					var rows = $("#rows").val();
					var filter = date_filter;
					var sort_order = $('#sorting_order').find(":selected").text();
					var fields = [];*/
					
					var selected_field = "";
					var sort_field = "";
					var query = '*:*';
					var start = 0;
					var rows = 0;
					//var filter = 'default';
					var filter = date_filter;
					var sort_order = "";
					var fields = [];
					
					getselectedfields(col_name, query, start, rows, filter, sort_field, sort_order, fields);
					
					
					
					$(".check")
					.change(
							function() {
								$("#result_grid").show(500);
								refresh();
								$( "#result_grid").append("<table id='outId'></table><div id='chartGridPager'></div>" );
								updateColModel();
								drawGidChart(jQuery.data(gridView,"gridDataArray"),modelFields);
							});
				}
			});
}

function getsortingfields(col) {

	$.ajax({
		type : "POST",
		url : "getSchema.html",
		data : "collection=" + col,
		async : false,
		success : function(response) {
			$('#sorting').empty();
			for ( var i = 0; i < response.length; i++) {
				if (response[i].type == "date") {
					$("#sorting").append($('<option>').text(response[i].name))
							.append($('</option>'));
				}
				else if(response[i].type == "long"){
					$("#sorting").append($('<option>').text(response[i].name))
							.append($('</option>'));
				}
			}
		}
	});
}

$(function() {
	$('#from_datepicker').datetimepicker({
		format : 'Y-m-d H:i:s',
		dayOfWeekStart : 1,
		lang : 'en'
	});
	
	var d = new Date();
	d.setHours(d.getHours()-12);
	var temp_from_date=d.getDate();
	var temp_from_month=d.getMonth()+1;
	if(temp_from_date<10){
		temp_from_date="0"+temp_from_date;
	}
	if(temp_from_month<10){
		temp_from_month="0"+temp_from_month;
	}
	var temp_to_date=d.getFullYear()+"-"+temp_from_month+"-"+temp_from_date+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
	$('#from_datepicker').datetimepicker({
		value : temp_to_date
	});

	$('#to_datepicker').datetimepicker({
		format : 'Y-m-d H:i:s',
		dayOfWeekStart : 1,
		lang : 'en'
		
	});
	
	var dt = new Date();
	var temp_date=dt.getDate();
	var temp_month=dt.getMonth()+1;
	
	if(temp_date<10)
		{
		temp_date="0"+temp_date;
		}
	
	if(temp_month<10)
	{
	temp_month="0"+temp_month;
	}
	var final_to_date=dt.getFullYear()+"-"+temp_month+"-"+temp_date+" "+dt.getHours()+":"+dt.getMinutes()+":"+dt.getSeconds();
	$('#to_datepicker').datetimepicker({
		value : final_to_date
	});
	$("#execute").click(
			function() {
				var col = $("#logs").val();
				
				var date_filter="";	
				var from_date = $("#from_datepicker").val();
				var to_date = $("#to_datepicker").val();
				var temp_start_datetime = from_date.split(" ");
				var final_from_date = temp_start_datetime[0]+ "T"+ temp_start_datetime[1]+ "Z";
				var temp_end_datetime = to_date.split(" ");
				var final_to_date = temp_end_datetime[0]+ "T"+ temp_end_datetime[1]+ "Z";
				date_filter = "Date:["+ final_from_date + " TO "+ final_to_date + "]";
				
				/*var selected_field = $('#logs').find(":selected").val();
				var sort_field = $('#sorting').find(":selected").text();
				var query = $("#query").val();
				var start = $("#start").val();
				var rows = $("#rows").val();
				var filter = date_filter;
				var sort_order = $('#sorting_order').find(":selected").text();
				var fields = [];*/
				
				var sort_field = "";
				var query = "";
				
				var query_field = $('#query_field').find(":selected").val();
				var query_text = $("#query_text").val();
				
				if(query_field === 'Select' || query_field === '' || query_field === 'null' || query_field === null){
					query = '*:*';
				}
				else{
					query = query_field+':'+query_text;
					if(query.includes(" ")|query.includes("(")|query.includes(")")){
	 			    	if(query_field=="Date"){
	 			    		//alert('Please add double quotes in Beginnning and end');
	 			    		$("#alertDate").dialog({buttons:{"Ok":function() {
							$("#alertDate").dialog('close');
	 			    		}}});
	 			    	}
	 			    	else{
	 			    		//alert('Please add * between spaces ,(  ,  ), : ,  Beginnning and end ');
	 			    		$("#alertSpace").dialog({buttons:{"Ok":function() {
	 			    			$("#alertSpace").dialog('close');
	 			    		}} });
	 			    	}
					}
				}
 			    
				console.log('Query STrinbg :'+ query);
				var start = 0;
				var rows = 0;
				var filter = date_filter;
				var sort_order = "";
				var fields = [];
				refresh();
				$("#result_grid").show(500);
				$( "#result_grid").append( "<table id='outId'></table><div id='chartGridPager'></div>" );
				
				getselectedfields(col, query, start, rows, filter, sort_field, sort_order, fields);
				
				/*var date_filter="";
				var sort_field=null;
				var selected_field = $('#logs').find(":selected").val();
				var from_date = $("#from_datepicker").val();
				var to_date = $("#to_datepicker").val();
		
				var temp_start_datetime = from_date.split(" ");
				var final_from_date = temp_start_datetime[0]+ "T"+ temp_start_datetime[1]+ "Z";
		
				var temp_end_datetime = to_date.split(" ");
				var final_to_date = temp_end_datetime[0]+ "T"+ temp_end_datetime[1]+ "Z";
			
				$("#result_grid").show(500);
				//getLogData(col, final_from_date, final_to_date);
				col = selected_field;
				date_filter = "Date:["+ final_from_date + " TO "+ final_to_date + "]";
				sort_field = $('#sorting').find(":selected").text();
					
				var query = $("#query").val();
				var start = $("#start").val();
				var rows = $("#rows").val();
				var filter = date_filter;
				var sort_order = $('#sorting_order').find(":selected").text();
				

				var fields = [];
				$.each($("input[name='select_fields']:checked"), function() {
					fields.push($(this).val());
				});
				refresh();
				$( "#result_grid").append( "<table id='outId'></table><div id='chartGridPager'></div>" );
				$( "#result_grid").show();
				getselectedfields(col, query, start,
						rows, filter, sort_field,
						sort_order, fields);*/
				
				
				/*var key=query.substring(0,query.indexOf(':'));
				   var val=query.substring(query.indexOf(':')+1);
				   if(val.length>22){
					   $("#alertLength").dialog({buttons:{"Ok":function() {
							$("#alertLength").dialog('close');

						}} });
				   }
				   else{
					   if(query=='*:*'){
						   getselectedfields(col, query, start, rows, filter, sort_field,sort_order, fields);
							 getresults(col, query, start, rows, filter, sort_field,
							 sort_order);  
					   }else{
						   if(query.includes("\"")){
							   getselectedfields(col, query, start, rows, filter, sort_field,sort_order, fields);
								 getresults(col, query, start, rows, filter, sort_field,
								 sort_order); 
						   }
						   else{
							   if(val.includes(" ")|val.includes(":")|val.includes("(")|val.includes(")")){
								   if(key=="Date"){
									   //alert('Please add double quotes in Beginnning and end');
								      $("#alertDate").dialog({buttons:{"Ok":function() {
											$("#alertDate").dialog('close');

										}} });
								   }else{
									   //alert('Please add * between spaces ,(  ,  ), : ,  Beginnning and end ');
										$("#alertSpace").dialog({buttons:{"Ok":function() {
											$("#alertSpace").dialog('close');

										}} });
								   
								   }
							   }
							   else{
								   getselectedfields(col, query, start, rows, filter, sort_field,sort_order, fields);
									 getresults(col, query, start, rows, filter, sort_field,
									 sort_order);   
							   }
						   }
						   
					   }
				   }*/
				 
				  
				

			});

	$("#advanced_feature").click(function() {
		if($("#advanced1").is(":visible"))
			$("#advanced1").hide();
		else
		$("#advanced1").show(500);
	});
	
	$("#reports").click(function() {
		window.location.href = "userdashboard.html";
	});
	
	/*$('#logs').change(function() {
		$("#advanced1").hide();
		$("#outId").find("tr:gt(0)").remove();
		$("#resId").find("tr").remove();
		$('#query').val('*:*');
		var selected_field = $('#logs').find(":selected").val();
		var col = "WinSysLog";
		var sort_field = null;
	
		col = selected_field;
		getsortingfields(col);
		var query = $("#query").val();
		var start = $("#start").val();
		var rows = $("#rows").val();
		var filter = $("#filter").val();
		var sort_order = $('#sorting_order').find(":selected").text();
		var fields = [];
		$.each($("input[name='select_fields']:checked"), function() {
			fields.push($(this).val());
		});
		getresults(col, query, start, rows, filter, sort_field, sort_order);
	});*/
	$('#logs').change(function() {
		var col_name=$('#logs :selected').val();
		console.log("col_name "+col_name);
		refresh();
		getschema(col_name);
	});

	$("#refresh").click(
			function() {
				var col_name=$('#logs :selected').val();
				console.log("col_name "+col_name);
				refresh();
				getschema(col_name);
				$("#query_text").val('');
			});
});
