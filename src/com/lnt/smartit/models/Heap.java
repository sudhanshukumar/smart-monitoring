/**
 * 
 */
package com.lnt.smartit.models;

/**
 * @author 299273
 *
 */
public class Heap {
	
	private int processId;
	private String MinHeapFreeRatio;
	private String MaxHeapFreeRatio;
	private String MaxHeapSize;
	private String NewSize;     
	private String MaxNewSize;    
	private String OldSize;      
	private String NewRatio;      
	private String SurvivorRatio;   
	private String PermSize;    
	private String MaxPermSize;   
	private String G1HeapRegionSize;
	private Space psOldGenerationSpace;
	private Space psPermGenerationSpace;

	public String getMinHeapFreeRatio() {
		return MinHeapFreeRatio;
	}

	public void setMinHeapFreeRatio(String minHeapFreeRatio) {
		MinHeapFreeRatio = minHeapFreeRatio;
	}

	public String getMaxHeapFreeRatio() {
		return MaxHeapFreeRatio;
	}

	public void setMaxHeapFreeRatio(String maxHeapFreeRatio) {
		MaxHeapFreeRatio = maxHeapFreeRatio;
	}

	public String getMaxHeapSize() {
		return MaxHeapSize;
	}

	public void setMaxHeapSize(String maxHeapSize) {
		MaxHeapSize = maxHeapSize;
	}

	public String getNewSize() {
		return NewSize;
	}

	public void setNewSize(String newSize) {
		NewSize = newSize;
	}

	public String getMaxNewSize() {
		return MaxNewSize;
	}

	public void setMaxNewSize(String maxNewSize) {
		MaxNewSize = maxNewSize;
	}

	public String getOldSize() {
		return OldSize;
	}

	public void setOldSize(String oldSize) {
		OldSize = oldSize;
	}

	public String getNewRatio() {
		return NewRatio;
	}

	public void setNewRatio(String newRatio) {
		NewRatio = newRatio;
	}

	public String getSurvivorRatio() {
		return SurvivorRatio;
	}

	public void setSurvivorRatio(String survivorRatio) {
		SurvivorRatio = survivorRatio;
	}

	public String getPermSize() {
		return PermSize;
	}

	public void setPermSize(String permSize) {
		PermSize = permSize;
	}

	public String getMaxPermSize() {
		return MaxPermSize;
	}

	public void setMaxPermSize(String maxPermSize) {
		MaxPermSize = maxPermSize;
	}

	public String getG1HeapRegionSize() {
		return G1HeapRegionSize;
	}

	public void setG1HeapRegionSize(String g1HeapRegionSize) {
		G1HeapRegionSize = g1HeapRegionSize;
	}

	public Space getPsOldGenerationSpace() {
		return psOldGenerationSpace;
	}

	public void setPsOldGenerationSpace(Space psOldGenerationSpace) {
		this.psOldGenerationSpace = psOldGenerationSpace;
	}

	public Space getPsPermGenerationSpace() {
		return psPermGenerationSpace;
	}

	public void setPsPermGenerationSpace(Space psPermGenerationSpace) {
		this.psPermGenerationSpace = psPermGenerationSpace;
	}

	public int getProcessId() {
		return processId;
	}

	public void setProcessId(int processId) {
		this.processId = processId;
	}

	@Override
	public String toString() {
		return "Heap [processId=" + processId + ", MinHeapFreeRatio="
				+ MinHeapFreeRatio + ", MaxHeapFreeRatio=" + MaxHeapFreeRatio
				+ ", MaxHeapSize=" + MaxHeapSize + ", NewSize=" + NewSize
				+ ", MaxNewSize=" + MaxNewSize + ", OldSize=" + OldSize
				+ ", NewRatio=" + NewRatio + ", SurvivorRatio=" + SurvivorRatio
				+ ", PermSize=" + PermSize + ", MaxPermSize=" + MaxPermSize
				+ ", G1HeapRegionSize=" + G1HeapRegionSize
				+ ", psOldGenerationSpace=" + psOldGenerationSpace
				+ ", psPermGenerationSpace=" + psPermGenerationSpace + "]";
	}
}
