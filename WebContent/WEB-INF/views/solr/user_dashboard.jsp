<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!doctype html>
<html lang="en">
<head>

<link rel="shortcut icon" href="../../assets/ico/favicon.ico">
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

<title>SmartIT</title>

<!-- Bootstrap Styles -->
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap-responsive.css">
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap-theme.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/common/ui.jqgrid.css" />
<link rel="stylesheet" type="text/css" href="css/common/jquery-ui.min.css" />
<link rel="stylesheet" href="css/custom/specific/nav.css">


<script src="scripts/common/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="scripts/common/jquery.jqGrid.min.js"></script>
<!-- <script src="scripts/common/jquery-ui.min.js"></script> -->

<link href="scripts/c3-0.4.10/c3.css" rel="stylesheet" type="text/css" charset="utf-8">

<link href="css/custom/specific/history.css" rel="stylesheet">

<!-- Jquery scripts -->
<script type="text/javascript" src="assets/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="scripts/common/jquery-ui.js"></script>
<script src="scripts/common/grid.locale-en.js"></script>
<script type="text/javascript" src="scripts/common/jquery.jqGrid.min.js"></script>

<!-- Bootstrap scripts -->

<!-- c3 scripts -->
<script src="scripts/d3-master/d3.js" charset="utf-8"></script>
<script src="scripts/c3-0.4.10/c3.js" charset="utf-8"></script>
<script type="text/javascript" src="scripts/custom/user_dashboard.js"></script>

<script type="text/javascript" src="scripts/common/multiselect/jquery.multiselect.filter.js"></script>
<script type="text/javascript" src="scripts/common/multiselect/jquery.multiselect.js"></script>

<link rel="stylesheet" type="text/css" href="css/common/jquery.multiselect.filter.css" />
<link rel="stylesheet" type="text/css"href="css/common/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css" href="css/common/style.css" />

<!-- Highcharts script -->
<script src="scripts/common/highstocks.js"></script>
<script src="scripts/common/highcharts.js"></script>

<script src="scripts/custom/tab_menu.js"></script>

<% String session_val = (String)session.getAttribute("modules"); %>

<script type="text/javascript">
	session_obj= '<%=session_val%>';
</script>

</head>
<body style="font-size:11px!important;">
<div class="container-fluid">
	<%@ include file="../common/header.jsp"%>
 		<nav class="navbar navbar-inverse" style="margin-top: 0px">
	      <ul class="nav navbar-nav navbar-xs">
	        <li id="Dashboard"><a href="dashboard.html">Dashboard<span class="sr-only">(current)</span></a></li>
	        <li id="AlertHistory"><a href="problemHistory.html">Alert History</a></li>
	        <!-- <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Manage<span class="caret"></span></a>
	          <ul class="dropdown-menu">
	            <li><a href="#">Manage Device</a></li>
	            <li><a href="#">Manage Threshold</a></li>
	          </ul>
	        </li> -->
	        <li id="LogAnalysis"><a href="query.html">Log Analysis</a></li>
	        <li id="LogCorrelation"><a href="correlation.html">Log Correlation</a></li>
	        <li id="VariableAnalysis"><a href="variableAnalysis.html"><spring:message code="label.admin.menu5"></spring:message></a></li>
	        <li  class="active" class="dropdown" id="Reports">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reports <span class="caret"></span></a>
	          <ul class="dropdown-menu">
	            	<li id="CreateReport"><a href="charts.html">Create Report</a></li>
		            <li id="CreateDashboard"><a href="createuserdashboard.html">Create Dashboard</a></li>
		            <li id="ViewDashboard"><a href="list_dashboard.html">View Dashboard</a></li>
	          </ul>
	        </li>
	      </ul>
	  </nav> 

	<div class="container-fluid innerContainer">
		<div class="col-md-3" style="background-color:#e0e0e0; height:460px;">
			<div class="row" style=" margin-top: 20px; margin-left: .05px;">
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="col-sm-4" >Application:</label>
						<div class="col-sm-8">
							<select  class="form-control" id="application" style="width:185px;">
							</select>
						</div>
					</div>
				</form>
			</div>
			<div class="row"  style="margin-left: .05px;">
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="col-sm-4">Report:</label>
						<div class="col-sm-8" >
							<select class="form-control" multiple="multiple" id="charts" style="width:185px!important;">
							</select>
						</div>
					</div>
				</form>
			</div>
			<div class="row" style="margin-left: .05px;">
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label  class="col-sm-4">Name:</label>
						<div class="col-sm-8">
							<input class="form-control" id="dashboard_name" type="text"
								placeholder="Enter Name" style="width:185px;">
						</div>
					</div>
				</form>
			</div>
			<div class = "btn-group">
				<input type="button" class="btn btn-primary btn-sm" style=" margin-left: 30px;" id="load_reports" name="Load Reports" value="Load" />
				<input type="button" class="btn btn-primary btn-sm" style=" margin-left: 5px;" id="refresh"  name="Refresh" value="Refresh" />
				<input type="button" class="btn btn-primary btn-sm" style=" margin-left: 5px;" id="save" name="Save" value="Save" />
				<input type="button" class="btn btn-primary btn-sm" style=" margin-left: 5px;" id="deleteReport" name="deleteReport" value="Delete" />
	        </div>
	        <div id="dialog" style="border: 0px solid #000;">
				<table class="table table-bordered" id="resId">
					<thead>
						<tr>
							<th></th>
							<th>Report Name</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
			<div id="confirmEdit" title="Confirm">
			</div>
			<div id="alertDialog" title="Alert">
			</div>
		</div>
		<div class="col-md-8" style="height:460px; overflow: auto">
	        <div class="container" id="dashboard_pannel"">
			</div>
		</div>
	</div>
</div>
</div>
<%@ include file="../common/footer.jsp"%>
	
</body>
</html>