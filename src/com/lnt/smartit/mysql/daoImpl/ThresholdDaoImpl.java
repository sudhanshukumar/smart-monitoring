package com.lnt.smartit.mysql.daoImpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.apache.log4j.helpers.DateTimeDateFormat;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lnt.smartit.helper.HbmHelper;
import com.lnt.smartit.mysql.dao.ThresholdDao;
import com.lnt.smartit.mysql.model.AppLogMaster;
import com.lnt.smartit.mysql.model.DeviceParams;
import com.lnt.smartit.mysql.model.EmailMaster;
import com.lnt.smartit.mysql.model.LogParams;
import com.lnt.smartit.mysql.model.NotificationGroup;
import com.lnt.smartit.mysql.model.ThresholdMaster;
import com.lnt.smartit.mysql.model.ThresholdNotifyGroupMapping;
import com.lnt.smartit.mysql.model.ThresholdNotifyGroupMappingId;
import com.lnt.smartit.rule.mysql.models.Expression;
import com.lnt.smartit.rule.mysql.models.ExpressionDataType;
import com.lnt.smartit.rule.mysql.models.ExpressionType;
import com.lnt.smartit.rule.mysql.models.LogParamsRuleMapping;
import com.lnt.smartit.rule.mysql.models.Operation;
import com.lnt.smartit.rule.mysql.models.OperationType;
import com.lnt.smartit.rule.mysql.models.Rule;
import com.lnt.smartit.rule.mysql.models.RuleItem;
import com.lnt.smartit.rule.mysql.models.RuleType;
import com.lnt.smartit.rule.mysql.models.RuleVO;

@Repository
@Transactional
public class ThresholdDaoImpl implements ThresholdDao{
	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private HbmHelper hbmHelper;
	private static Logger logger = Logger.getLogger(ThresholdDaoImpl.class);
	
	@Value("${application.name}")
	String appId;

	public List<RuleVO> getThresholds(String appId) {
		List<RuleVO> manageThresholds=new ArrayList<RuleVO>();
		Session session = sessionFactory.getCurrentSession();
		//Query query = session.createQuery("select lp from LogParams lp inner join lp.log lg where lg.logId = lp.log.logId and lg.app.appId = '"+appId+"'");
//		List<LogParams> logParams = query.list();
		Query query = null;
        query = session.createQuery("SELECT prx.paramRuleMappingId, prx.logParam.log.logId, r.ruleType.code, e.exprDatatype.code, e.exprType.code, e.exprValue, o.operationType.code, "
        		+"prx.logParam.paramName, prx.filterDuration, prx.itsmReqFlag,r.ruleType.name,e.exprDatatype.name, e.exprType.name, o.exprLHS.exprId, "
        		+"o.exprRHS.exprId,e.exprId,prx.deduplicationWindow, prx.notificationflag, prx.emailTo, prx.emailCC, prx.fromMW, prx.toMW  "
				+ "FROM Operation o, Expression e, Rule r, RuleItem ri, LogParamsRuleMapping prx WHERE "
				+ "(o.exprLHS.exprId = e.exprId OR o.exprRHS.exprId = e.exprId) AND r.operation.operationId=o.operationId AND ri.rule.ruleId = r.ruleId "
				+ "AND prx.ruleItem.ruleItemId = ri.ruleItemId AND prx.logParam.log.app.appId="+appId
				+ " order by prx.logParam.appId, r.ruleType");
		ArrayList<Object[]> rows = (ArrayList<Object[]>) query.list();
	/*	for(int i = 0 ; i < rows.size() ; i=i+2){
			query = session.createQuery("from AppLogMaster ap where ap.logId=:id");
			query.setString("id", (String) rows.get(i)[1]);
			List<AppLogMaster> appLogMasters=query.list();
			query = session.createQuery("from DeviceParams d where d.appDevice.deviceId=:app_device_id and d.name='hostname'");
			query.setString("app_device_id", appLogMasters.get(0).getDevice().getDeviceId());
			List<DeviceParams> deviceParams=query.list();
			RuleVO rule = new RuleVO();
			rule.setId((int) rows.get(i)[0]);
			rule.setAppLogParamId((String) rows.get(i)[1]);
			rule.setHostName(deviceParams.get(0).getValue());		
			
			rule.setParameterType( (String) rows.get(i)[11]);
			rule.setParameterTypeCode((String) rows.get(i)[3]);
			
			rule.setParamName((String) rows.get(i)[7]);
			rule.setThresholdType((String) rows.get(i)[10]);
			rule.setExpressionTypeLHS((String) rows.get(i)[12]);
			
			//ExpressionType expressionType = (ExpressionType) rows.get(i)[4];
			rule.setExpressionTypeLHSCode((String) rows.get(i)[4]);	
			
			rule.setThresholdValueLHS(rows.get(i)[5]);
			rule.setLogicalOperator((String) rows.get(i)[6]);
			rule.setFilterDuration((long) rows.get(i)[8]);
			rule.setAutoIncident((String) rows.get(i)[9]);
			
			manageThresholds.add(rule);
		}
		int j=0;
		for(int i = 1 ; i < rows.size() ; i=i+2){
			RuleVO rule = manageThresholds.get(j);
			//RuleType ruleType = (RuleType) rows.get(i)[2];
			if((rule.getId() == (int) rows.get(i)[0]) && (rule.getThresholdType().equals((String) rows.get(i)[10]))){
				rule.setThresholdValueRHS(rows.get(i)[5]);
				rule.setExpressionTypeRHS((String) rows.get(i)[12]);
				
				//ExpressionType expressionType = (ExpressionType) rows.get(i)[4];
				rule.setExpressionTypeRHSCode((String) rows.get(i)[4]);
			}
			j++;
			
		}
		*/
		int i=0;
		while(rows.size() > i){
			Integer expId = (Integer) rows.get(i)[15];
			Integer expLHS = (Integer) rows.get(i)[13];
			Integer expRHS = (Integer)  rows.get(i)[14];
					 
			query = session.createQuery("from AppLogMaster ap where ap.logId=:id");
			query.setString("id", (String) rows.get(i)[1]);
			List<AppLogMaster> appLogMasters=query.list();
			query = session.createQuery("from DeviceParams d where d.appDevice.deviceId=:app_device_id and d.name='hostname'");
			query.setString("app_device_id", appLogMasters.get(0).getDevice().getDeviceId());
			List<DeviceParams> deviceParams=query.list();
			RuleVO rule = new RuleVO();
			rule.setId((int) rows.get(i)[0]);
			rule.setAppLogParamId((String) rows.get(i)[1]);
			rule.setHostName(deviceParams.get(0).getValue());		
			
			rule.setParameterType( (String) rows.get(i)[11]);
			rule.setParameterTypeCode((String) rows.get(i)[3]);
			
			rule.setParamName((String) rows.get(i)[7]);
			rule.setThresholdType((String) rows.get(i)[10]);
			
			if(expId.equals(expLHS)){			
				rule.setExpressionTypeLHS((String) rows.get(i)[12]);			
				//ExpressionType expressionType = (ExpressionType) rows.get(i)[4];
				rule.setExpressionTypeLHSCode((String) rows.get(i)[4]);			
				rule.setThresholdValueLHS(rows.get(i)[5]);
				if(rows.get(i)[6] != null){
					i++;
						//RuleVO rule = manageThresholds.get(j);
						//RuleType ruleType = (RuleType) rows.get(i)[2];
						//if((rule.getId() == (int) rows.get(i)[0]) && (rule.getThresholdType().equals((String) rows.get(i)[10]))){
					System.out.println("Row Size : "+rows.size());
					System.out.println("Value of i : "+i);
					if(i<rows.size()){						
						int ruleId = (int) rows.get(i)[0];
						if(rule.getId() == ruleId){
							if((rule.getThresholdType().equals((String) rows.get(i)[10]))){
								rule.setThresholdValueRHS(rows.get(i)[5]);
								rule.setExpressionTypeRHS((String) rows.get(i)[12]);
								
								//ExpressionType expressionType = (ExpressionType) rows.get(i)[4];
								rule.setExpressionTypeRHSCode((String) rows.get(i)[4]);
								}
							}
						}else{
							i--;
						}
					}
			}else if(expId.equals(expRHS)){
				
				rule.setExpressionTypeRHS((String) rows.get(i)[12]);			
				//ExpressionType expressionType = (ExpressionType) rows.get(i)[4];
				rule.setExpressionTypeRHSCode((String) rows.get(i)[4]);			
				rule.setThresholdValueRHS(rows.get(i)[5]);
				if(rows.get(i)[6] != null){
					i++;
						//RuleVO rule = manageThresholds.get(j);
						//RuleType ruleType = (RuleType) rows.get(i)[2];
						//if((rule.getId() == (int) rows.get(i)[0]) && (rule.getThresholdType().equals((String) rows.get(i)[10]))){
					System.out.println("Row Size : "+rows.size());
					System.out.println("Value of i : "+i);
					if(i<rows.size()){
						int ruleId = (int) rows.get(i)[0];	
						if(rule.getId() == ruleId){
							if(rule.getThresholdType().equals((String) rows.get(i)[10])){
								rule.setThresholdValueLHS(rows.get(i)[5]);
								rule.setExpressionTypeLHS((String) rows.get(i)[12]);
								
								//ExpressionType expressionType = (ExpressionType) rows.get(i)[4];
								rule.setExpressionTypeLHSCode((String) rows.get(i)[4]);
								}
							}
						}else{
							i--;
						}
					}
			
				
			}
			rule.setLogicalOperator((String) rows.get(i)[6]);
			rule.setFilterDuration((long) rows.get(i)[8]);
			rule.setAutoIncident((String) rows.get(i)[9]);
			rule.setDeduplicationWindow((long) rows.get(i)[16]);
			
			//10614080 on 06/03/2016 start
			rule.setNotification((String) rows.get(i)[17]);
			rule.setNotificationTO((String) rows.get(i)[18]); 
			rule.setNotificationCC((String) rows.get(i)[19]); 
			//10614080 on 06/03/2016 end
		
			//10612175 on 07/04/2017 start
			//rule.setMaintenanceWindow((String) rows.get(i)[20]);
			rule.setFromMW((Date) rows.get(i)[20]); 
			rule.setToMW((Date) rows.get(i)[21]); 
			//10612175 on 07/04/2017 end
			
			//10612175 on 17/04/2017 start
			boolean maintenanceWindowFlag = true;
			SimpleDateFormat sdf = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
			//sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
			Date alertDate = new Date();
			String alertDateString = alertDate.toString();
			try {
				System.out.println("alert date String: "+alertDateString);						
				System.out.println("alert date : "+sdf.parse(alertDateString));
				if(rule.getFromMW() != null && rule.getToMW() != null){
					System.out.println(rule.getFromMW().before(sdf.parse(alertDateString)));
					System.out.println(rule.getToMW().after(sdf.parse(alertDateString)));	
					if(rule.getFromMW().before(sdf.parse(alertDateString))){
						if(rule.getToMW().after(sdf.parse(alertDateString)))
							maintenanceWindowFlag = false;
						else
							maintenanceWindowFlag = true;							
					}else
						maintenanceWindowFlag = true;											
				}
			}catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
			}
			//10612175 on 17/04/2017 end	
			if(!maintenanceWindowFlag){
				rule.setParamName(rule.getParamName()+"**");
			}
			
			manageThresholds.add(rule);
			i++;	
		}
		return manageThresholds;
	}
	
	public List<NotificationGroup> getNotificationGroup() {
		@SuppressWarnings("unchecked")
		List<NotificationGroup> groups = (List<NotificationGroup>) hbmHelper.createQuery("from NotificationGroup", null);
		return groups;
	}

	public List<EmailMaster> getEmailMaster() {
		@SuppressWarnings("unchecked")
		List<EmailMaster> emails = (List<EmailMaster>) hbmHelper.createQuery("from EmailMaster", null);
		return emails;
	}
	//10612175 on 27/01/2017 start

	/*public boolean deleteThreshold(String threshId) {
		hbmHelper.delete(hbmHelper.findById(LogParams.class, threshId));
		return true;
	}*/

	public boolean deleteThreshold(String threshId) {
		LogParamsRuleMapping logParamsRuleMapping = null;
		RuleItem ruleItem = null;
		Rule rule = null;
		Operation operation = null;
		Expression expressionLHS = null;
		Expression expressionRHS = null;
		//hbmHelper.delete(hbmHelper.findById(RuleItem.class, Integer.parseInt(threshId)));
		Integer thresholdId = Integer.parseInt(threshId);
		
		Session session = sessionFactory.getCurrentSession();
		logParamsRuleMapping = (LogParamsRuleMapping) session.get(LogParamsRuleMapping.class, thresholdId);
		
		ruleItem = (RuleItem) session.get(RuleItem.class, logParamsRuleMapping.getRuleItem().getRuleItemId());
		
		rule = (Rule) session.get(Rule.class, ruleItem.getRule().getRuleId());
		
		if(rule.getOperation() != null)
			operation = (Operation) session.get(Operation.class, rule.getOperation().getOperationId());
		
		if(operation.getExprLHS() != null)
			expressionLHS = (Expression) session.get(Expression.class, operation.getExprLHS().getId());
		
		
		if(operation.getExprRHS() != null)
			expressionRHS = (Expression) session.get(Expression.class, operation.getExprRHS().getId());			
		
		
		if(logParamsRuleMapping != null){
			session.delete(logParamsRuleMapping);				
		}
		if(ruleItem != null){
			session.delete(ruleItem);	
		}
		if(rule != null){
			session.delete(rule);	
		}
		if(operation != null){
			session.delete(operation);	
		}
		if(expressionRHS != null){
			session.delete(expressionRHS);	
		}
		if(expressionLHS != null){
			session.delete(expressionLHS);	
		}
		return true;
	}
	//10612175 on 27/01/2017 end

	public ThresholdMaster getthresholdMasterById(ThresholdMaster thresholdMaster) {
		ThresholdMaster threshold = new ThresholdMaster();
		threshold = (ThresholdMaster) hbmHelper.findById(ThresholdMaster.class,thresholdMaster.getThresholdId());
		logger.debug(threshold);
		return threshold;
	}

	public Map<String, String> getThresholdNotifyMapping(ThresholdMaster thresholdMaster) {
		Map<String, String> notificationMap = new HashMap<String, String>();
		@SuppressWarnings("unchecked")
		List<ThresholdNotifyGroupMapping> list = (List<ThresholdNotifyGroupMapping>) sessionFactory
				.getCurrentSession().createQuery("from ThresholdNotifyGroupMapping where id.thresholdId ="+ thresholdMaster.getThresholdId()).list();
		for (ThresholdNotifyGroupMapping thresholdNotifyGroupMapping : list) {
			@SuppressWarnings("unchecked")
			List<NotificationGroup> groupNames = (List<NotificationGroup>) sessionFactory.getCurrentSession().createQuery("from NotificationGroup where groupId ="+ thresholdNotifyGroupMapping.getId().getGroupId()).list();
			notificationMap.put(thresholdNotifyGroupMapping.getId().getGroupId(), groupNames.get(0).getGroupName());
		}
		return notificationMap;
	}

	@Transactional
	public String saveThresholdMaster(ThresholdMaster thresholdMaster) {
		String ThresholdMasterId = null;
		logger.debug("inside save threshold");
		try {
			ThresholdMasterId = (String) hbmHelper.save(thresholdMaster);
			logger.debug("inside dao: "+ThresholdMasterId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ThresholdMasterId;
	}

	public String savenotificationGroup(NotificationGroup notificationGroup) {
		String notificationGroupId = null;
		try {
			hbmHelper.saveOrUpdate(notificationGroup);
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.debug("---->notificationGroupId---" + notificationGroupId);
		return notificationGroupId;

	}

	@SuppressWarnings("unchecked")
	public List<NotificationGroup> getAllNotificationGroup() {
		List<NotificationGroup> notificationGroup = (List<NotificationGroup>) hbmHelper.createQuery("from NotificationGroup", null);
		logger.debug(notificationGroup.toString());
		return notificationGroup;
	}

	public NotificationGroup getNotificationGroupById(NotificationGroup notificationGroup) {
		NotificationGroup notiGroup = new NotificationGroup();
		notiGroup = (NotificationGroup) hbmHelper.findById(NotificationGroup.class, notificationGroup.getGroupId());
		logger.debug(notiGroup);
		return notiGroup;
	}

	public String deleteNotificationGroup(NotificationGroup notificationGroup) {
		String deleteNotificationGroupmsg;
		try {
			hbmHelper.delete(notificationGroup);
			deleteNotificationGroupmsg = "NotificationGroup deleted successfully!!";
		} catch (Exception e) {
			e.printStackTrace();
			deleteNotificationGroupmsg = "NotificationGroup deletion unsuccessfull due to "	+ e.getMessage();
		}
		return deleteNotificationGroupmsg;
	}

	public void editNotificationGroup(NotificationGroup notificationgp) {
		hbmHelper.update(notificationgp);
	}

	@SuppressWarnings("unchecked")
	public List<ThresholdNotifyGroupMapping> getNotificationThresholdMappingById(String notificationGroupId) {
		List<ThresholdNotifyGroupMapping> thresholdNotifyGroupMappingList = (List<ThresholdNotifyGroupMapping>) hbmHelper
				.createQuery("from ThresholdNotifyGroupMapping where id.groupId="+ notificationGroupId, null);
		logger.debug("-----thresholdNotifyGroupMappingList:  " + thresholdNotifyGroupMappingList.toString());
		return thresholdNotifyGroupMappingList;
	}

	public void saveNotificationThresholdMapping(List<ThresholdNotifyGroupMapping> thresholdNotifyGroupMappingList) {
		for (ThresholdNotifyGroupMapping thresholdNotifyGroupMapping : thresholdNotifyGroupMappingList) {
			hbmHelper.persist(thresholdNotifyGroupMapping);
		}
	}

	public void saveThresholdNotifyGroupMappingService(String thresholdMasterId, String thresholdGroupSelectedIds) {
		try {
			String[] thresholdGroupSelectedId = thresholdGroupSelectedIds.split(",");
			for (String grpId : thresholdGroupSelectedId) {
				ThresholdNotifyGroupMapping thresholdNotifyGroupMapping = new ThresholdNotifyGroupMapping();
				ThresholdNotifyGroupMappingId thresholdNotifyGroupMappingId = new ThresholdNotifyGroupMappingId();
				thresholdNotifyGroupMappingId.setGroupId(grpId);
				logger.debug("ThresholdId --->" + thresholdMasterId);
				logger.debug("ThresholdNotifyGroupMappingId -->" + thresholdNotifyGroupMappingId);
				thresholdNotifyGroupMappingId.setThresholdId(thresholdMasterId);
				thresholdNotifyGroupMapping.setId(thresholdNotifyGroupMappingId);
				hbmHelper.persist(thresholdNotifyGroupMapping);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteThresholdNotifyGroupMappingService(ThresholdNotifyGroupMapping thresholdNotifyGroupMapping) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("delete  ThresholdNotifyGroupMapping where id.thresholdId="
						+ thresholdNotifyGroupMapping.getId().getThresholdId());
		query.executeUpdate();
		session.flush();

	}

	public String deleteThresholdMaster(ThresholdMaster threshold) {
		String deleteThresholdMastermsg;
		try {
			hbmHelper.delete(threshold);
			deleteThresholdMastermsg = "Threshold Master deleted successfully!!";
		} catch (Exception e) {
			e.printStackTrace();
			deleteThresholdMastermsg = "Threshold Master deletion unsuccessfull due to " + e.getMessage();
		}
		return deleteThresholdMastermsg;
	}

	public String saveThresholdsForLogs(ThresholdMaster thresholdMaster,String[] logFileName, String[] logCriticalThreshold,
			String[] logWarningThreshold) {
		Session session = sessionFactory.getCurrentSession();
		Query query = null;
		try {
			logger.debug("inside threshold dao");
			for (int i = 0; i < logFileName.length; i++) {
				query = session.createSQLQuery("insert into threshold_logsize(host,log_file_name,warning_threshold_size,critical_threshold_size,id) values('"
								+ thresholdMaster.getDeviceMaster().getHostName()
								+ "','"
								+ logFileName[i]
								+ "','"
								+ logCriticalThreshold[i]
								+ "','"
								+ logWarningThreshold[i]
								+ "','"
								+ thresholdMaster.getThresholdId() + "');");
				logger.debug(query);
				query.executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.flush();
		}
		return query.toString();
	}

	@SuppressWarnings("unchecked")
	public boolean checkId(Integer thresholdId) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("From ThresholdMaster where thresholdId ="+thresholdId);
		List<ThresholdMaster> thresholdObj = query.list();
		if(thresholdObj.size()==0){
			return false;
		}else{
			return true;
		}
	}

	public String saveThresholdsForServices(ThresholdMaster thresholdMaster,String[] serviceName) {
		Session session = sessionFactory.getCurrentSession();
		Query query = null;
		try {
			logger.debug("inside threshold dao for services");
			for (int i = 0; i < serviceName.length; i++) {
				query = session
						.createSQLQuery("insert into threshold_service(host,service_name,threshold_id) values('"
								+ thresholdMaster.getDeviceMaster().getHostName()
								+ "','"
								+ serviceName[i]
								+ "','"
								+ thresholdMaster.getThresholdId()
								+ "');");
				logger.debug(query);
				query.executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.flush();
		}
		return query.toString();
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, String>> previewThresholdForLog(String thresholdId) {
		List<Map<String, String>> logList = new ArrayList<Map<String, String>>();
		Session session = sessionFactory.getCurrentSession();
		Query query = null;
		try {
			logger.debug("inside threshold dao for log preview");
			query = session.createSQLQuery("SELECT host, log_file_name, warning_threshold_size, critical_threshold_size, t.id,threshold_name,parameter_id,threshold_enabled,notification_enabled,group_id FROM itpredictive.threshold_logsize t , itpredictive.threshold_master tm,threshold_notify_group_mapping nm where t.id = tm.threshold_id AND nm.threshold_id=t.id AND t.id='"
							+ thresholdId + "'");
			logger.debug(query);
			List<Object> logFiles = query.list();
			for (int i = 0; i < logFiles.size(); i++) {
				Object[] row = (Object[]) logFiles.get(i);
				Map<String, String> logElements = new HashMap<String, String>();
				logElements.put("HostName", row[0].toString());
				logElements.put("log_file_name", row[1].toString());
				logElements.put("warning_threshold_size", row[2].toString());
				logElements.put("critical_threshold_size", row[3].toString());
				logElements.put("id", row[4].toString());
				logElements.put("threshold_name", row[5].toString());
				logElements.put("parameter_id", row[6].toString());
				logElements.put("threshold_enabled", row[7].toString());
				logElements.put("notification_enabled", row[8].toString());
				logElements.put("group_id", row[9].toString());
				logList.add(logElements);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return logList;
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, String>> previewThresholdForService(String thresholdId) {
		List<Map<String, String>> serviceList = new ArrayList<Map<String, String>>();
		Session session = sessionFactory.getCurrentSession();
		Query query = null;
		try {
			logger.debug("inside threshold dao for service preview");
			query = session.createSQLQuery("SELECT host, service_name, t.id, t.threshold_id,threshold_name,parameter_id,threshold_enabled,notification_enabled,group_id FROM threshold_service t,threshold_master tm,threshold_notify_group_mapping nm "
							+ "where t.threshold_id=tm.threshold_id AND nm.threshold_id=t.threshold_id AND t.threshold_id = '"
							+ thresholdId + "'");
			logger.debug("Service Query : " + query);
			List<Object> serviceFiles = query.list();
			for (int i = 0; i < serviceFiles.size(); i++) {
				Object[] row = (Object[]) serviceFiles.get(i);
				Map<String, String> servcieElements = new HashMap<String, String>();
				servcieElements.put("HostName", row[0].toString());
				logger.debug(row[1].toString());
				servcieElements.put("service_name", row[1].toString());
				servcieElements.put("id", row[2].toString());
				servcieElements.put("threshold_id", row[3].toString());
				servcieElements.put("threshold_name", row[4].toString());
				servcieElements.put("parameter_id", row[5].toString());
				servcieElements.put("threshold_enabled", row[6].toString());
				servcieElements.put("notification_enabled", row[7].toString());
				servcieElements.put("group_id", row[8].toString());
				serviceList.add(servcieElements);
			}
			logger.debug("service List" + serviceList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return serviceList;
	}

	public String updateThresholdMaster(ThresholdMaster thresholdMaster) {
		Session session = sessionFactory.getCurrentSession();
		Query query = null;
		try {
			logger.debug("inside threshold dao for services:" + thresholdMaster);
			query = session.createSQLQuery("update threshold_master set threshold_name='"
							+ thresholdMaster.getThresholdName()
							+ "' where threshold_id='"
							+ thresholdMaster.getThresholdId() + "';");
			logger.debug(query);
			query.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.flush();
		}
		return query.toString();
	}

	public void editThresholdsForLog(ThresholdMaster thresholdMaster,String[] logWarningThreshold, String[] logCriticalThreshold) {
		Session session = sessionFactory.getCurrentSession();
		Query query = null;
		try {
			logger.debug("inside threshold dao for services:" + thresholdMaster);
			for (int i = 0; i < logWarningThreshold.length; i++) {
				query = session.createSQLQuery("update threshold_logsize set warning_threshold_size='"
								+ logWarningThreshold[i]
								+ "',critical_threshold_size='"
								+ logCriticalThreshold[i]
								+ "' where id='"
								+ thresholdMaster.getThresholdId() + "'");
				logger.debug(query);
				query.executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.flush();
		}
	}
	
	public String getThresholdMasterByDeviceIDandParamID(String deviceID,String paramName) {
		Session session = sessionFactory.getCurrentSession();
		Query query = null;
		String str=null;
		try{
			query = session.createSQLQuery("Select * from threshold_master t where t.parameter_id ='"+paramName + "'and t.device_id = '"+deviceID+"';" );
			System.out.println(query);
			List<Object> serviceFiles = query.list();
			for (int i = 0; i < serviceFiles.size(); i++) {
			       Object[] row = (Object[]) serviceFiles.get(i);	
			       System.out.println( "row value 1 :--------------------------------------- " + row[0].toString());
			       System.out.println(  "row value 2 --------------------------------------- " + row[1].toString());
			       str=row[6].toString();
			}
		}
		catch(Exception e){
			e.printStackTrace();
			
		}
		finally{
			session.flush();
		}
		return str;
	}

	@Override
	public List<LogParams> getLogParams() {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("From LogParams GROUP BY param_name");
		List<LogParams> logParams=query.list();
		return logParams;
	}

	@Override
	public List<DeviceParams> getAppDeviceParams() {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("From DeviceParams GROUP BY value");
		List<DeviceParams> devParams=query.list();
		return devParams;
	}
	
	@Override
	public List<LogParams> getLogParamsForHost(String hostname) {
		Session session = sessionFactory.getCurrentSession();
		String appName = appId;
		Query query = session.createQuery("Select lp From LogParams lp, AppLogMaster al, DeviceParams dp where al.device.deviceId = dp.appDevice.deviceId and dp.name='Hostname' and dp.value='"+hostname+"' and lp.log.logId=al.logId and al.app.appName = '"+appName+"'");
		List<LogParams> logParams=query.list();
		return logParams;
	}

	@Override
	public void editThresholds(RuleVO ruleVO) {
		Session session = sessionFactory.getCurrentSession();
		LogParamsRuleMapping logParamsRuleMapping = null;
		if(ruleVO.getId() == 0){
			logParamsRuleMapping = new LogParamsRuleMapping();
			Expression expressionLHS = new Expression();
			
			ExpressionDataType expressionDataType = new ExpressionDataType();
			expressionDataType.setCode(ruleVO.getParameterType());
			expressionLHS.setExprDatatype(expressionDataType);
			
			ExpressionType expressionTypeLHS = new ExpressionType();
			expressionTypeLHS.setCode(ruleVO.getExpressionTypeLHS());
			expressionLHS.setExprType(expressionTypeLHS);
			
			expressionLHS.setExprValue((String) ruleVO.getThresholdValueLHS());
			hbmHelper.saveOrUpdate(expressionLHS);
			
			Operation operation = new Operation();
			operation.setExprLHS(expressionLHS);
			
			Expression expressionRHS = null;
			if(!ruleVO.getLogicalOperator().equals("null")){
				expressionRHS = new Expression();
				expressionRHS.setExprDatatype(expressionDataType);
				
				ExpressionType expressionType2 = new ExpressionType();
				expressionType2.setCode(ruleVO.getExpressionTypeRHS());
				expressionRHS.setExprType(expressionType2);
				
				expressionRHS.setExprValue((String) ruleVO.getThresholdValueRHS());
				hbmHelper.saveOrUpdate(expressionRHS);
				operation.setExprRHS(expressionRHS);
				
				OperationType operationType = new OperationType();
				operationType.setCode(ruleVO.getLogicalOperator());
				operation.setOperationType(operationType);
			}
			
			
			operation.setExecSequence(1);
			hbmHelper.saveOrUpdate(operation);
			Rule rule = new Rule();
			rule.setName(ruleVO.getHostName()+"_"+ruleVO.getParamName());
			rule.setOperation(operation);
			
			RuleType ruleType = new RuleType();
			ruleType.setCode(ruleVO.getThresholdType());
			ruleType.setName(ruleVO.getThresholdType());
			rule.setRuleType(ruleType);
			
			hbmHelper.saveOrUpdate(rule);
			
			RuleItem ruleItem = new RuleItem();
			ruleItem.setExecutionSequence(1);
			ruleItem.setName(ruleVO.getHostName()+"_"+ruleVO.getParamName());
			ruleItem.setRule(rule);
			hbmHelper.saveOrUpdate(ruleItem);
			
			logParamsRuleMapping.setRuleItem(ruleItem);
			
			LogParams logParams = new LogParams();
			logParams.setAppId(ruleVO.getParamName());
			
			logParamsRuleMapping.setLogParam(logParams);
			logParamsRuleMapping.setDescription("");
			logParamsRuleMapping.setFilterDuration(ruleVO.getFilterDuration());
			logParamsRuleMapping.setItsmReqFlag(ruleVO.getAutoIncident());
			logParamsRuleMapping.setDeduplicationWindow(ruleVO.getDeduplicationWindow());
			

			//10614080 on 06/03/2016 start
			logParamsRuleMapping.setNotificationflag(ruleVO.getNotification());
			logParamsRuleMapping.setEmailTo(ruleVO.getNotificationTO());
			logParamsRuleMapping.setEmailCC(ruleVO.getNotificationCC());
			//10614080 on 06/03/2016 end
			
			//10612175 on 06/04/2017 maintenance window start
			//logParamsRuleMapping.setMaintenanceWindow(ruleVO.getMaintenanceWindow());
			logParamsRuleMapping.setFromMW(ruleVO.getFromMW());
			logParamsRuleMapping.setToMW(ruleVO.getToMW());
			//10612175 on 06/04/2017 maintenance window end
			
			hbmHelper.saveOrUpdate(logParamsRuleMapping);
		} else {
			logParamsRuleMapping = (LogParamsRuleMapping) hbmHelper.findById(LogParamsRuleMapping.class, ruleVO.getId());
			Expression expressionLHS = logParamsRuleMapping.getRuleItem().getRule().getOperation().getExprLHS();
		
			ExpressionDataType expressionDataType = new ExpressionDataType();
			expressionDataType.setCode(ruleVO.getParameterType());			
			expressionLHS.setExprDatatype(expressionDataType);
			
			ExpressionType expressionTypeLHS = new ExpressionType();
			expressionTypeLHS.setCode(ruleVO.getExpressionTypeLHS());
			expressionLHS.setExprType(expressionTypeLHS);
			
			expressionLHS.setExprValue((String) ruleVO.getThresholdValueLHS());
			hbmHelper.saveOrUpdate(expressionLHS);
			
			/*Expression expressionRHS = logParamsRuleMapping.getRuleItem().getRule().getOperation().getExprRHS();
			if(expressionRHS != null){
				expressionRHS.setExprDatatype(expressionDataType);
				
				ExpressionType expressionTypeRHS = new ExpressionType();
				expressionTypeRHS.setCode(ruleVO.getExpressionTypeLHS());			
				expressionRHS.setExprType(expressionTypeRHS);
				
				expressionRHS.setExprValue((String) ruleVO.getThresholdValueRHS());
				hbmHelper.saveOrUpdate(expressionRHS);
				
			}		
			Operation operation = logParamsRuleMapping.getRuleItem().getRule().getOperation();
			operation.setExprLHS(expressionLHS);
			if(expressionRHS != null)
				operation.setExprRHS(expressionRHS);
			else
				operation.setExprRHS(null);
			
			OperationType operationType = new OperationType();
			operationType.setCode(ruleVO.getLogicalOperator());
			operation.setOperationType(operationType);*/
			Expression expressionRHS = null;
			Operation operation = logParamsRuleMapping.getRuleItem().getRule().getOperation();
			if(ruleVO.getLogicalOperator() != null){
				if(ruleVO.getLogicalOperator().equalsIgnoreCase("null")){
					expressionRHS = new Expression();
					expressionRHS.setExprDatatype(expressionDataType);
					
					ExpressionType expressionType2 = new ExpressionType();
					expressionType2.setCode(ruleVO.getExpressionTypeRHS());
					expressionRHS.setExprType(expressionType2);
					
					expressionRHS.setExprValue((String) ruleVO.getThresholdValueRHS());
					hbmHelper.delete((expressionRHS));
					operation.setExprRHS(null);
					operation.setOperationType(null);
				}else{
					expressionRHS = new Expression();
					expressionRHS.setExprDatatype(expressionDataType);
					
					ExpressionType expressionType2 = new ExpressionType();
					expressionType2.setCode(ruleVO.getExpressionTypeRHS());
					expressionRHS.setExprType(expressionType2);
					
					expressionRHS.setExprValue((String) ruleVO.getThresholdValueRHS());
					hbmHelper.saveOrUpdate(expressionRHS);	
					operation.setExprRHS(expressionRHS);
					
					OperationType operationType = new OperationType();
					operationType.setCode(ruleVO.getLogicalOperator());
					operation.setOperationType(operationType);
				}
			}			
			hbmHelper.saveOrUpdate(operation);
			
			Rule rule = logParamsRuleMapping.getRuleItem().getRule();
			rule.setOperation(operation);
			
			RuleType ruleType = new RuleType();
			ruleType.setCode(ruleVO.getThresholdType());
			ruleType.setName(ruleVO.getThresholdType());			
			rule.setRuleType(ruleType);
			hbmHelper.saveOrUpdate(rule);
			
			/*RuleItem ruleItem = new RuleItem();
			ruleItem.setExecutionSequence(1);
			ruleItem.setName(ruleVO.getHostName()+"_"+ruleVO.getParamName());
			ruleItem.setRule(rule);
			hbmHelper.saveOrUpdate(ruleItem);
			
			logParamsRuleMapping.setRuleItem(ruleItem);
			
			LogParams logParams = new LogParams();
			logParams.setAppId(ruleVO.getParamName());
			
			logParamsRuleMapping.setLogParam(logParams);
			logParamsRuleMapping.setDescription("");*/
			logParamsRuleMapping.setFilterDuration(ruleVO.getFilterDuration());
			logParamsRuleMapping.setItsmReqFlag(ruleVO.getAutoIncident());
			logParamsRuleMapping.setDeduplicationWindow(ruleVO.getDeduplicationWindow());
			
			//10614080 on 06/03/2016 start
			logParamsRuleMapping.setNotificationflag(ruleVO.getNotification());
			logParamsRuleMapping.setEmailTo(ruleVO.getNotificationTO());
			logParamsRuleMapping.setEmailCC(ruleVO.getNotificationCC());
			//10614080 on 06/03/2016 end
			
			//10612175 on 06/04/2017 maintenance window start
			//logParamsRuleMapping.setMaintenanceWindow(ruleVO.getMaintenanceWindow());
			logParamsRuleMapping.setFromMW(ruleVO.getFromMW());
			logParamsRuleMapping.setToMW(ruleVO.getToMW());
			//10612175 on 06/04/2017 maintenance window end
			
			hbmHelper.saveOrUpdate(logParamsRuleMapping);
		}
		session.flush();
		
		
		/*Query query = session.createQuery("From LogParamsRuleMapping lp where dp.value=:value");
		query.setString("value", manageThresholds.getHostName());
		List<DeviceParams> devParams=query.list();
		System.out.println("devParams.get(0).getDeviceParamId()="+devParams.get(0).getAppDevice().getDeviceId());
		query = session.createQuery("From AppLogMaster ap where ap.device.deviceId=:app_device_id");
		query.setString("app_device_id", devParams.get(0).getAppDevice().getDeviceId());
		List<AppLogMaster> appLog=query.list();
		LogParams logParams=new LogParams();
		logParams.setAppId(manageThresholds.getId());
		
		logParams.setLog(appLog.get(0));
		logParams.setParamName(manageThresholds.getParamName());
		logParams.setMaxValue(manageThresholds.getCriticalThreshold());
		logParams.setMinValue(manageThresholds.getWarnThreshold());
		System.out.println(logParams.toString());
		hbmHelper.saveOrUpdate(logParams);*/
	}

	@Override
	public List<ExpressionDataType> getParameterTypeList() {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(ExpressionDataType.class);
		List<ExpressionDataType> parameterTypeList = criteria.list();		
		return parameterTypeList;
	}

	@Override
	public List<RuleType> getAlertTypeList() {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(RuleType.class);
		List<RuleType> alertTypeList = criteria.list();		
		return alertTypeList;
	}

	@Override
	public List<OperationType> getOperatorTypeList() {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(OperationType.class);
		List<OperationType> operatorTypeList = criteria.list();		
		return operatorTypeList;
	}
}
