package com.lnt.smartit.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lnt.smartit.mysql.dao.DeviceDao;
import com.lnt.smartit.mysql.model.AppDeviceMaster;
import com.lnt.smartit.mysql.model.DeviceApplicationMapping;
import com.lnt.smartit.mysql.model.DeviceMaster;
import com.lnt.smartit.mysql.model.DeviceParams;
import com.lnt.smartit.mysql.model.DeviceSoftwareMapping;
import com.lnt.smartit.services.DeviceService;

@Service
public class DeviceServiceImpl implements DeviceService{
	@Autowired
	private DeviceDao dao;
	
	public void saveDeviceparams(DeviceParams deviceparams) {
		 dao.saveDeviceparams(deviceparams);
	}

	public AppDeviceMaster getAppDeviceMasterById(AppDeviceMaster appDeviceMaster) {
		return dao.getAppDeviceMasterById(appDeviceMaster);
	}
	
	public String deleteAppDeviceMaster(AppDeviceMaster devMaster) {
		return dao.deleteAppDeviceMaster(devMaster);
	}
	
	@Override
	public String getDeviceMasterByHostName(String name) {
		return dao.getDeviceIdByHostname(name);
	}

	@Override
	public Integer getMaxAppDeviceId() {
		return dao.getMaxAppDeviceId();
	}

	@Override
	public void saveAppDeviceMaster(AppDeviceMaster appDeviceMaster) {
		dao.saveAppDeviceMaster(appDeviceMaster);
	}

	@Override
	public String getDeviceTypecode(String searchCriteria) {
		return dao.getDeviceTypecode(searchCriteria);
	}

	@Override
	public void deleteDeviceParams(String deviceId) {
		 dao.deleteDeviceParams(deviceId);
		
	}

	@Override
	public List<DeviceParams> getDeviceParams(String appId) {
		return dao.getDeviceParams(appId);
	}

	@Override
	public List<DeviceParams> getDeviceParamsById(String deviceid) {
		return dao.getDeviceParamsById(deviceid);
	}

	@Override
	public AppDeviceMaster getOsById(String deviceid) {
		return dao.getOsById(deviceid);
	}

	@Override
	public void updateDeviceparams(DeviceParams deviceparams) {
		dao.updateDeviceparams(deviceparams);
	}

	@Override
	public void updateAppDeviceMaster(AppDeviceMaster appdevicemaster1) {
		dao.updateAppDeviceMaster(appdevicemaster1);
	}
}
