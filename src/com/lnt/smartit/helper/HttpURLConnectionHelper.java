package com.lnt.smartit.helper;

/**
 * Created by 10604672 on 3/26/2015.
 */
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpURLConnectionHelper {

   private static final Logger logger = LoggerFactory.getLogger(HttpURLConnectionHelper.class);

    private final String USER_AGENT = "Mozilla/5.0";
    private URL url;
    private HttpURLConnection connection;

    public HttpURLConnectionHelper(String urlStr) throws IOException {
        this.url = new URL(urlStr);
        connection = (HttpURLConnection) url.openConnection();
    }

    public void addParam(String key, Object value) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String str = mapper.writeValueAsString(value);
        this.addParam(key, str);
        //add request header
        connection.setRequestProperty("User-Agent", USER_AGENT);
        connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
    }

    public void addParam(String key, String value)
    {
        connection.addRequestProperty(key, value);
    }

    public String sendGet() throws IOException {

        // optional default is GET
        connection.setRequestMethod("GET");

        int responseCode = connection.getResponseCode();
        logger.info("\nSending 'GET' request to URL : " + url);
        logger.info("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        logger.info("Response found : " + response.toString());

        return response.toString();
    }

    // HTTP POST request
    private void sendPost() throws Exception {

        //add reuqest header
        connection.setRequestMethod("POST");

        String urlParameters = "sn=C02G8416DRJM&cn=&locale=&caller=&num=12345";

        // Send post request
        connection.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = connection.getResponseCode();
        logger.info("\nSending 'POST' request to URL : " + url);
        logger.info("Post parameters : " + urlParameters);
        logger.info("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        logger.info(response.toString());

    }

}