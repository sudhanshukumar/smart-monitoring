package com.lnt.smartit.serviceImpl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lnt.smartit.mysql.dao.ThresholdDao;
import com.lnt.smartit.mysql.model.DeviceParams;
import com.lnt.smartit.mysql.model.EmailMaster;
import com.lnt.smartit.mysql.model.LogParams;
import com.lnt.smartit.mysql.model.ManageThreshold;
import com.lnt.smartit.mysql.model.NotificationGroup;
import com.lnt.smartit.mysql.model.ThresholdMaster;
import com.lnt.smartit.mysql.model.ThresholdNotifyGroupMapping;
import com.lnt.smartit.rule.mysql.models.ExpressionDataType;
import com.lnt.smartit.rule.mysql.models.OperationType;
import com.lnt.smartit.rule.mysql.models.RuleType;
import com.lnt.smartit.rule.mysql.models.RuleVO;
import com.lnt.smartit.services.ThresholdService;

@Service
public class ThresholdServiceImpl implements ThresholdService{
@Autowired
private ThresholdDao dao;

public List<RuleVO> getThresholds(String appId) {
	return dao.getThresholds(appId);
}

public boolean deleteThreshold(String threshId) {
	return dao.deleteThreshold(threshId);
}

public ThresholdMaster getthresholdMasterById(ThresholdMaster thresholdMaster) {
	return dao.getthresholdMasterById(thresholdMaster);
}

public Map<String, String> getThresholdNotifyMapping(ThresholdMaster thresholdMaster) {
	return dao.getThresholdNotifyMapping(thresholdMaster);
}

public String saveThresholdMaster(ThresholdMaster thresholdMaster) {
	return dao.saveThresholdMaster(thresholdMaster);
}

public List<NotificationGroup> getNotificationGroup() {
	return dao.getNotificationGroup();
}

public List<EmailMaster> getEmailMaster() {
	return dao.getEmailMaster();
}

public void saveThresholdNotifyGroupMappingService(String thresholdMasterId, String thresholdGroupSelectedIds){
	dao.saveThresholdNotifyGroupMappingService(thresholdMasterId, thresholdGroupSelectedIds);
}

public void deleteThresholdNotifyGroupMappingService(ThresholdNotifyGroupMapping thresholdNotifyGroupMapping) {
	dao.deleteThresholdNotifyGroupMappingService(thresholdNotifyGroupMapping);

}

public String deleteThresholdMaster(ThresholdMaster threshold) {
	return dao.deleteThresholdMaster(threshold);
}

public String saveThresholdsForLogs(ThresholdMaster thresholdMaster, String[] logFileName,String[] logCriticalThreshold, String[] logWarningThreshold) {
	return dao.saveThresholdsForLogs(thresholdMaster,logFileName,logCriticalThreshold,logWarningThreshold);
}

public boolean checkId(Integer thresholdId) {
	return dao.checkId(thresholdId);
}

public String saveThresholdsForServices(ThresholdMaster thresholdMaster,String[] serviceName) {
	return dao.saveThresholdsForServices(thresholdMaster,serviceName);
}

public List<Map<String,String>> previewThresholdForLog(String thresholdId) {
	return dao.previewThresholdForLog(thresholdId);
}

public List<Map<String,String>> previewThresholdForService(String thresholdId) {
	return dao.previewThresholdForService(thresholdId);
}

public String updateThresholdMaster(ThresholdMaster thresholdMaster) {
	return dao.updateThresholdMaster(thresholdMaster);
}

public void editThresholdsForLog(ThresholdMaster thresholdMaster,String[] logWarningThreshold, String[] logCriticalThreshold) {
	dao.editThresholdsForLog(thresholdMaster,logWarningThreshold,logCriticalThreshold);
}

public String getThresholdMasterByDeviceIDandParamID(String deviceID, String paramID) {
	// TODO Auto-generated method stub
	return dao.getThresholdMasterByDeviceIDandParamID(deviceID,paramID);
}

@Override
public List<LogParams> getLogParams() {
	// TODO Auto-generated method stub
	return dao.getLogParams();
}

@Override
public List<DeviceParams> getAppDeviceParams() {
	// TODO Auto-generated method stub
	return dao.getAppDeviceParams();
}

@Override
public void editThresholds(RuleVO manageThresholds) {
	dao.editThresholds(manageThresholds);
	
}

@Override
public List<LogParams> getLogParamsForHost(String hostname) {
	return dao.getLogParamsForHost(hostname);
}

@Override
public List<ExpressionDataType> getParameterTypeList() {
	return dao.getParameterTypeList();
}

@Override
public List<RuleType> getAlertTypeList() {
	return dao.getAlertTypeList();
}

@Override
public List<OperationType> getOperatorTypeList() {
	return dao.getOperatorTypeList();
}
}
