package com.lnt.smartit.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lnt.smartit.mysql.model.AppLogServiceComponentParam;
import com.lnt.smartit.mysql.model.ApplicationMaster;
import com.lnt.smartit.mysql.model.Applications;
import com.lnt.smartit.mysql.model.ManageReport;
import com.lnt.smartit.mysql.model.Report;
import com.lnt.smartit.mysql.model.ReportMetadata;
import com.lnt.smartit.services.ChartService;
import com.lnt.smartit.solr.dao.ChartDao;
import com.lnt.smartit.solr.pojo.ChartMaster;
import com.lnt.smartit.solr.pojo.Charting;
import com.lnt.smartit.solr.pojo.Dashboard;
import com.lnt.smartit.solr.pojo.DashboardUpdate;
import com.lnt.smartit.mysql.model.DashboardMaster;

@Service
public class ChartServiceImpl implements ChartService {

	@Autowired
	ChartDao chartDao;
	
	@Override
	public String saveChartMaster(Report report) {
		return chartDao.saveChartMaster(report);
	}

	@Override
	public List<Report> getCharts() {
		return chartDao.getCharts();
	}

	@Override
	public List<Charting> drawChart(Report chart) {
		return chartDao.drawChart(chart);
	}

	@Override
	public Boolean savedashboard(List<DashboardMaster> dashboard) {
		return chartDao.savedashboard(dashboard);
	}

	@Override
	public List<DashboardMaster> getDashboards() {
		return chartDao.getDashboards();
	}
	
	@Override
	public List<String> getDashboardNameList() {
		return chartDao.getDashboardNameList();
	}

	@Override
	public List<Dashboard> drawDashboard(DashboardMaster dash_name) {
		return chartDao.drawDashboard(dash_name);
	}

	@Override
	public List<Applications> getApplications() {
		return chartDao.getApplications();
	}

	@Override
	public List<AppLogServiceComponentParam> getCollectionsFromDB(String appsel) {
		
		return chartDao.getCollectionsFromDB(appsel);
	}

	@Override
	public Boolean saveChartMaster(ReportMetadata reportmeta) {
		 return chartDao.saveChartMaster(reportmeta);
	}

	@Override
	public List<Charting> getUpdatedReport(DashboardUpdate dashboardUpdate) {
		
		return  chartDao.getUpdatedReport(dashboardUpdate);
	}

	@Override
	public List<Report> getReports() {
		return  chartDao.getReports();
	}

	@Override
	public Boolean deleteReports(String[] selected_reports) {
		return chartDao.deleteReports(selected_reports);
	}

	@Override
	public Boolean selectReports(List<ManageReport> manageReportList) {
		return chartDao.selectReports(manageReportList);
	}

	@Override
	public List<ManageReport> getManageReportFields(String col) {
		
		return chartDao.getManageReportFields(col);
	}

	@Override
	public void undoFilterType(List<ManageReport> manageReportList) {
		chartDao.undoFilterType(manageReportList);
		
	}

	@Override
	public void undoInvalid(List<ManageReport> manageReportList) {
		chartDao.undoInvalid(manageReportList);
		
	}

	@Override
	public String getPrimaryFilter(String col) {
		return chartDao.getPrimaryFilter(col);
	}

	@Override
	public Boolean deleteDashboards(String select_dashboards) {
		return chartDao.deleteDashboards(select_dashboards);
	}

	@Override
	public Boolean setSequence(String[] sequenceArray,String dashName) {
		 return chartDao.setSequence(sequenceArray,dashName);
	}

	@Override
	public Boolean removeChartfromDash(String chart_name, String dashboardName) {
		return chartDao.removeChartfromDash(chart_name,dashboardName);
	}

	@Override
	public String getReportIdfromName(String chart) {
		return chartDao.getReportIdfromName(chart);
	}

	@Override
	public List<String> getChartNameList() {
		return chartDao.getChartNameList();
	}
}
