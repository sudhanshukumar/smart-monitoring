var count=0;
var rowcount=0;
var correlateFields1=[];
var correlateFields2=[];
var gridCols=[];
var modelFields=[];
var queryFields = [];
var gridView;
var checkbox_count=[];
$(document).ready(function() {
	$("#advanced1").hide();
	$("#alertLog").hide();
	$("#alertLength").hide();
	$("#alertDate").hide();
	$("#alertSpace").hide();
	$("#alertDialog").hide();

	$("#advanced_feature").click(function() {
		if($("#advanced1").is(":visible"))
			$("#advanced1").hide();
		else
		$("#advanced1").show(500);
	});

	$( "#datePickerFrom" ).datetimepicker({
			format : 'Y-m-d H:i:s',
			dayOfWeekStart : 1,
			lang : 'en'
			
		});
	  
	function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".more-less")
            .toggleClass('glyphicon-plus glyphicon-minus');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);
	
	
	  var d = new Date();
		d.setHours(d.getHours()-12);
		
		
		var temp_from_date=d.getDate();
		var temp_from_month=d.getMonth()+1;
		
		if(temp_from_date<10)
			{
			temp_from_date="0"+temp_from_date;
			}
		
		if(temp_from_month<10)
		{
			temp_from_month="0"+temp_from_month;
		}
		
		var temp_to_date=d.getFullYear()+"-"+temp_from_month+"-"+temp_from_date+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
		
		$('#datePickerFrom').datetimepicker({
			value : temp_to_date
		});

	  $( "#datePickerTo" ).datetimepicker({
			format : 'Y-m-d H:i:s',
			dayOfWeekStart : 1,
			lang : 'en'
			
		});
	  
		var dt = new Date();
		var temp_date=dt.getDate();
		var temp_month=dt.getMonth()+1;
		
		if(temp_date<10)
			{
			temp_date="0"+temp_date;
			}
		
		if(temp_month<10)
		{
		temp_month="0"+temp_month;
		}
		
		var final_to_date=dt.getFullYear()+"-"+temp_month+"-"+temp_date+" "+dt.getHours()+":"+dt.getMinutes()+":"+dt.getSeconds();

		$('#datePickerTo').datetimepicker({
			value : final_to_date
		});
		
		getCollections();
		
		$("#execute").click(function() {
			
			var colnames = [];
			$.each(
					$("input[name='select_collection']:checked"),
					function() {
						colnames.push($(this).val());
					});
			
			var date_filter="";	
			var from_date = $("#datePickerFrom").val();
			var to_date = $("#datePickerTo").val();
			var temp_start_datetime = from_date.split(" ");
			var final_from_date = temp_start_datetime[0]+ "T"+ temp_start_datetime[1]+ "Z";
			var temp_end_datetime = to_date.split(" ");
			var final_to_date = temp_end_datetime[0]+ "T"+ temp_end_datetime[1]+ "Z";
			date_filter = "Date:["+ final_from_date + " TO "+ final_to_date + "]";
			var sort_field = "";
			var query = "";
			
			var query_field = $('#query_field').find(":selected").val();
			var query_text = $("#query_text").val();
			
			if(query_field === 'Select' || query_field === '' || query_field === 'null' || query_field === null){
				query = '*:*';
			}
			else{
				if(query_text==='' || query_text==='null' || query_text === null){
					showAlert('Please enter Query Text!');
				}else{
				query = query_field+':'+query_text;
				if(query.includes(" ")|query.includes("(")|query.includes(")")){
 			    	if(query_field=="Date"){
 			    		$("#alertDate").dialog({buttons:{"Ok":function() {
						$("#alertDate").dialog('close');
 			    		}}});
 			    	}
 			    	else{
 			    		$("#alertSpace").dialog({buttons:{"Ok":function() {
 			    			$("#alertSpace").dialog('close');
 			    		}} });
 			    	}
				}
				}
			}
			    
			var start = 0;
			var rows = 0;
			var filter = date_filter;
			var sort_order = "";
			var fields = [];
			if($('#main_container').children().length==0){
				showAlert('Please Select atleast one Log Type!');
			}else{
			for(var p=1;p<=$('#main_container').children().length;p++){
				var idselector=$('#main_container div:nth-child('+p+')').attr('id');
				var substr = idselector.split('_');
				var collection=$('#check'+substr[1]).val();
				clearTable(substr[1]);
				getselectedfields(collection, query, start, rows, filter, sort_field, sort_order, fields,substr[1]);
			}
		}
	});
		
		
});	



function showAlert(message){
	
	$( "#alertDialog" ).show();
	$("#alertDialog").text(message);
	$( "#alertDialog" ).dialog({
		buttons:{
				"OK":function() {
					$( this ).dialog( "close" );
                                 }
				} 
							});
}


function getselectedfields(col, query, start, rows, filter, sort_field,
		sorting_order, fields,z) {
	$.ajax({
		type : "POST",
		url : "getSelectedFieldsQuery.html",
		data : "query=" + query + "&start=" + start + "&rows=" + rows
				+ "&filter=" + filter + "&collection=" + col + "&sort_field="
				+ sort_field + "&sort_order=" + sorting_order + "&fields="
				+ fields,
		success : function(response) {
			
			for ( var i = 0; i < response.length; i++) {
				var temp_date = response[i].Date;
				temp_date = $.format.date(temp_date, "dd/MM/yyyy HH:mm:ss.SSS");
				response[i].Date = temp_date;
			}
			jQuery.data(document.body, "gridDataArray"+z, response );
			updateColModel(z);
			if($("#main_container").has($("#divGrid_"+z)).length==0){
				$("#main_container").append("<div id='divGrid_"+z+"'></div>");
			}
			$("#divGrid_"+z).append("<table id='chartGrid"+z+"'></table>");
			drawGridChart(response,col,"chartGrid"+z);
			
		}
	});
}

function getCollections()
{
	  
	$.ajax({
		type : "GET",
		url : "getCollections.html",

		success : function(response) {
			var keys=[];
			var values = [];
			var result =JSON.parse(response);
			var i=1;
			for(key in result){
				 var childValues='<div class="panel-heading"><h4 class="panel-title"><input class="check" id="check'+i+'" name="select_collection" type="checkbox" onclick=checkStatus("'+key+'",'+i+') value='+key+' /><a data-toggle="collapse" data-parent="#accordion" class="text-style" style="text-decoration:none;" value="'+key+'" href="#collapse'+i+'"><i class="more-less glyphicon glyphicon-plus"></i>'+result[key]+'</a></h4></div><div id="collapse'+i+'" class="panel-collapse collapse"><div class="panel-body"><div class="row" style="overflow-y: scroll;overflow-x: hidden; height: 250px;"><div class="col-md-10"><table class="table table-bordered" id="resId'+i+'"><tbody></tbody></table></div></div></div></div>';
				$("#accordian_parent").last().append(childValues);
				getsortingfields(key,i)
				i++; 
			}
		}
	});
	
}

function pusharray(fieldvalue){
	gridCols.push(fieldvalue);
	var modelFieldsArr={
  		  name : fieldvalue,
  		  index : fieldvalue,	
  		  width : 150
  		
     };
	modelFields.push(modelFieldsArr);
}
function updateColModel(z){
	gridCols=[];
	 modelFields=[];
	
	$.each(
			$("input[name='fields_"+z+"']:checked"),
			function() {
				var fieldvalue=$(this)
				.val();
				modifiedpusharray(fieldvalue,false);
			});
	
	$.each(
			$("input[name='fields_"+z+"']:not(:checked)"),
			function() {
				var fieldvalue=$(this)
				.val();
				modifiedpusharray(fieldvalue,true);
			});
}
function modifiedpusharray(fieldvalue, isHidden){
	gridCols.push(fieldvalue);
	var modelFieldsArr={
  		  name : fieldvalue,
  		  index : fieldvalue,	
  		  width : 150,
  		  hidden: isHidden
     };
	modelFields.push(modelFieldsArr);
}


function clearTable(z){
	$('#chartGrid'+z).jqGrid('GridDestroy');
	$('#chartGrid'+z).remove();
}

function checkStatus(col,z){
	
	checkbox_count=[];
	$.each(
			$("input[name='select_collection']:checked"),
			function() {
				checkbox_count.push($(this).val());
			});
	
	if(checkbox_count.length==0){
   $("#query_field").prop("disabled", true);
	$("#query_text").prop("disabled", true);
	}else{
		$("#query_field").prop("disabled", false);
		$("#query_text").prop("disabled", false);
	}
	
	if ($("#check"+z).is(':checked')) {
		getFields(col);
		loadGrid(col,z)
	} else if( $('#main_container').has($("#divGrid_"+z))){  
		removeFields(col);
   clearTable(z);
   $('#divGrid_'+z).remove();
   $("#resId"+z).find(".check").prop("checked", false);
   
	}else{
		$("#resId"+z).find(".check").prop("checked", false);
	}
}

/*function loadGrid(col,z){
	
	 if($('#main_container').children().length>=2){
		showAlert("You cant select more than two selections!")
		$("#check"+z).prop("checked", false);
	}else{
		selectAll(z);
	var query='*:*';
	var queryParams = {
             			'collection' : col,
             			'query' : query
					  	};
	$.ajax({
		type 		: "POST",
		datatype 	: "json",
	    url			:'getBasicQueryforReportsNoFilter.html',
	    data		: JSON.stringify(queryParams),
		success : function(response) {
			jQuery.data(document.body, "gridDataArray"+z, response );
			if($("#main_container").has($("#divGrid"+z)).length==0){
				$("#main_container").append("<div id='divGrid"+z+"'></div>");
			}
			$("#divGrid"+z).append("<table id='chartGrid"+z+"'></table>");
			drawGridChart(response,col,"chartGrid"+z);
		}
	});	
	}
}
*/

function loadGrid(col,z){
	
	 if($('#main_container').children().length>=2){
		showAlert("You cant select more than two selections!")
		$("#check"+z).prop("checked", false);
	}else{
		 $("#resId"+z).find(".check").prop("checked", true);
		clearTable(z);
		
		var date_filter="";	
		var from_date = $("#datePickerFrom").val();
		var to_date = $("#datePickerTo").val();
		var temp_start_datetime = from_date.split(" ");
		var final_from_date = temp_start_datetime[0]+ "T"+ temp_start_datetime[1]+ "Z";
		var temp_end_datetime = to_date.split(" ");
		var final_to_date = temp_end_datetime[0]+ "T"+ temp_end_datetime[1]+ "Z";
		date_filter = "Date:["+ final_from_date + " TO "+ final_to_date + "]";
		var selected_field = "";
		var sort_field = "";
		var query = '*:*';
		var start = 0;
		var rows = 0;
		var filter = date_filter;
		var sort_order = "";
		var fields = [];
		
		getselectedfields(col, query, start, rows, filter, sort_field, sort_order, fields,z);
		

	}
}


function drawGridChart(updatedData,col,id){
	if(updatedData.length==0){
		
		var emptyJson={};
		for(var i=0;i<gridCols.length;i++){
			emptyJson[gridCols[i]]="   ";
		}
		var emptyJsonarr2  =[];
		emptyJsonarr2.push(JSON.stringify(emptyJson));
		var emptyJsonarr=[];
		emptyJsonarr.push(emptyJson);
		
		$("#"+id).jqGrid(                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        {
			datatype : "local",
			data : emptyJsonarr,
		    colNames:gridCols,
		    colModel : modelFields,
		    viewrecords : true,
		    gridview : true,
		    width: 950,
		    height: '100%',
		    forceFit:true,
			shrinkToFit:false,
			caption:col,
			loadonce: false,
			 emptyrecords: "No records to view",
			 jsonReader : { repeatitems: false },
					rowNum : 15,
					rowList : [ 5, 10, 15 ],
					viewrecords : true,
					gridview : true,
					loadonce: true,
		}); 
		
	}else{
	$("#"+id).jqGrid(                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        {
			datatype : "local",
			data : updatedData,
		    colNames:gridCols,
		    colModel : modelFields,
		    viewrecords : true,
		    gridview : true,
		    width: 950,
		    height: '100%',
		    forceFit:true,
			shrinkToFit:false,
			caption:col,
			loadonce: false,
			 emptyrecords: "No records to view",
			 jsonReader : { repeatitems: false },
					rowNum : 15,
					rowList : [ 5, 10, 15 ],
					viewrecords : true,
					gridview : true,
					loadonce: true,
		}); 
	}
	}
function updateFielddata(col,z){
			clearTable(z);
			$( "#divGrid_"+z).append("<table id='chartGrid"+z+"'></table>");
			updateColModel(z);
			drawGridChart(jQuery.data(document.body,"gridDataArray"+z),col,"chartGrid"+z);
}
function removeFields(col){
	$.ajax({
		type : "POST",
		url : "getSchema.html",
		data : "collection=" + col,
		async : false,
		success : function(response) {
			
			for ( var i = 0; i < response.length; i++) {
				if (response[i].name != null) {
					var index = queryFields.indexOf(response[i].name);
					if (index > -1) {
						queryFields.splice(index, 1);
					}
				}
			}
			$("#query_field").empty();
			$("#query_field").append('<option value="" class="text-style" selected >Select</option>');
			
			var unique=queryFields.filter(function(itm,i,queryFields){
			    return i==queryFields.indexOf(itm);
			});
			
			unique.sort();
			
			for(var i=0; i<unique.length; i++){
				$("#query_field").append('<option class="text-style" value="'+unique[i]+'">'+unique[i]+'</option>');						
			}
		}
	});
}

function getFields(col){
	$.ajax({
		type : "POST",
		url : "getSchema.html",
		data : "collection=" + col,
		async : false,
		success : function(response) {
			
			$("#query_field").empty();
			$("#query_field").append('<option value="" class="text-style" selected >Select</option>');
			
	for ( var i = 0; i < response.length; i++) {
		if (response[i].name != null) {
			queryFields.push(response[i].name);
		}
	}
	
	var unique=queryFields.filter(function(itm,i,queryFields){
	    return i==queryFields.indexOf(itm);
	});
	
	unique.sort();
	
	for(var i=0; i<unique.length; i++){
		$("#query_field").append('<option class="text-style" value="'+unique[i]+'">'+unique[i]+'</option>');						
	}
		}
	});
}

function getsortingfields(col,z) {
	
     
	$.ajax({
		type : "POST",
		url : "getSchema.html",
		data : "collection=" + col,
		async : false,
		success : function(response) {
			
			$("#query_field").empty();
			$("#query_field").append('<option value="" class="text-style" selected >Select</option>');
		
			$("#query_field").prop("disabled", true);
			$("#query_text").prop("disabled", true);

			for ( var i = 0; i < response.length; i++) {
				if(response[i].name=='id' | response[i].name=='title' | response[i].name=='_version_'){
					
				}
				else{
				
					$("#resId"+z).find("tr:gt(0)").remove();
						for ( var i = 0; i < response.length; i++) {
						
								$("#resId"+z).find('tbody').append(
										$('<tr>').append(
												$('<td>').append(
														'<input class="check" name="fields_'+z+'" onclick=updateFielddata("'+col+'",'+z+') type="checkbox"  value='
																+ response[i].name + '>')
														.append($('</td>'))).append(
												$('<td>').text(response[i].name).append(
														$('</td>'))));
						}
						
						
				}
					
					
			}
			

		}
	});

}


