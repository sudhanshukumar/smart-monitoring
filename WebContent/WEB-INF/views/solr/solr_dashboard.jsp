<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../assets/ico/favicon.ico">
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

<title>SmartIT</title>

<!-- Bootstrap Styles -->
<link rel="stylesheet" type="text/css"
	href="assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="assets/css/bootstrap-responsive.css">
<link rel="stylesheet" type="text/css"
	href="assets/css/bootstrap-theme.min.css">

<!-- Common Styles -->
<link rel="stylesheet" type="text/css" href="assets/css/footer.css" />
<link href="css/custom/general/style1366.css" rel="stylesheet" />
<link href="css/custom/general/style1024.css" rel="stylesheet" />
<link href="css/custom/general/bluetabs.css" rel="stylesheet" />

<!-- Jquery css -->
<link rel="stylesheet" type="text/css" href="css/common/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="css/common/jquery.datetimepicker.css" />



<!-- Jquery scripts -->
<script type="text/javascript" src="assets/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="scripts/common/jquery-ui.js"></script>
<script type="text/javascript" src="scripts/common/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="scripts/common/grid.locale-en.js"></script>

<!-- Bootstrap scripts -->
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>

<!-- Highcharts script -->
<script src="scripts/common/highstocks.js"></script>
<script src="scripts/common/highcharts.js"></script>

<!-- Custom javascripts -->
<script type="text/javascript" src="scripts/custom/solr_dashboard.js"></script>
<script type="text/javascript" src="scripts/common/menu/dropdowntabs.js"></script>

</head>
<body>

	<header>
		<%@ include file="../common/header.jsp"%>
	</header>


	<nav id="bluemenu" class="bluetabs">
 		 <ul>
 		 <li><a href="dashboard.html"><spring:message code="label.admin.menu1"></spring:message></a></li>
  		 <li><a href="problemHistory.html"><spring:message code="label.admin.menu2"></spring:message></a></li>
  		 <li><a href="#" rel="dropmenu1_b"><spring:message code="label.admin.menu3"></spring:message></a></li>
		 <li><a href="manageNotificationGroup.html"><spring:message code="label.admin.menu4"></spring:message></a></li>
<%--   		 <li><a href="variableAnalysis.html"><spring:message code="label.admin.menu5"></spring:message></a></li> --%>
   		 <li><a id="selected" href="#" rel="dropmenu1_solr"><spring:message 
					code="label.admin.solr.reports"></spring:message></a></li> 
  		 <li><a href="query.html"><spring:message code="label.admin.solr.query"></spring:message></a></li>
  		 <li><a  href="correlated_query.html">Correlated query</a></li>
  		   <li><a href="correlation.html"><spring:message
						code="label.admin.correlation"></spring:message></a></li>
	  </ul>
 	 </nav>
 	  <div class="ddcolortabsline">&nbsp;</div>

 <div id="dropmenu1_b" class="dropmenudiv_b">
  <a href="manageDevice.html"><spring:message code="label.admin.menu3.submenu1"></spring:message></a>
  <a href="manageThresHold.html"><spring:message code="label.admin.menu3.submenu2"></spring:message></a>
<%--   <a href="manageApplication.html"><spring:message code="label.admin.menu3.submenu3"></spring:message></a> --%>
  <a href="manageService.html"><spring:message code="label.admin.menu3.submenu4"></spring:message></a>
<%--   <a href="manageUsers.html"><spring:message code="label.admin.menu3.submenu5"></spring:message></a> --%>
<a href="manageLog.html"><spring:message
				code="label.admin.menu3.submenu6"></spring:message></a>
    
  </div>
  
  <div id="dropmenu1_solr" class="dropmenudiv_b">
		<a href="userdashboard.html"><spring:message
				code="label.admin.solr.dashboard"></spring:message></a> <a
			href="charts.html"><spring:message
				code="label.admin.solr.charts"></spring:message></a> <a
			href="createuserdashboard.html"><spring:message
				code="label.admin.solr.custom.dashboard"></spring:message></a> <a
			href="list_dashboard.html"><spring:message
				code="label.admin.solr.view.dashboard"></spring:message></a> 
<a href="manage_reports.html"><spring:message
				code="label.admin.solr.manage.reports"></spring:message></a> 
				<a href="manage_dashboard.html"><spring:message
				code="label.admin.solr.manage.dashboard"></spring:message></a> 
	</div>
  
   <script type="text/javascript">
	//SYNTAX: tabdropdown.init("menu_id", [integer OR "auto"])
	tabdropdown.init("bluemenu")
	</script>


	<div class="ac-container">

		<!-- Main component for a primary marketing message or call to action -->
		<div class="row">
			<div class="col-md-5" style="border: 0px solid #000;">
				<form class="form-horizontal" role="form">
					<div class="form-group">

						<label class="col-sm-2" style="padding: 10px 0px 10px 40px">Application:</label>

						<div class="col-sm-10">

							<select class="form-control" id="applications">
								<option value="campus">CampusNext</option>
								<option value="payfast">LMS Sakai</option>
								<option value="servicefirest">FNA & Procurement</option>
								<option value="campus">PayFast</option>
								<option value="payfast">Rendezvous</option>
								<option value="servicefirest">ProcureFirst</option>
								<option value="campus">AppMakerJ</option>
								<option value="payfast">Sapphire</option>
							</select>
						</div>
					</div>


				</form>
			</div>
			
			<div class="col-md-3" style="border: 0px solid #000;">
						<form class="form-horizontal" role="form">
							<div class="form-group">
								<label class="col-sm-3" style="padding: 10px 0px 10px 40px">From:</label>
								<div class="col-sm-8">

									<div class="inner-addon left-addon">
										<i class="glyphicon glyphicon-calendar"></i> <input
											class="form-control" type="text" id="from_datepicker">
									</div>

								</div>
								
							</div>

						</form>
					</div>
					
					<div class="col-md-3" style="border: 0px solid #000;">
						<form class="form-horizontal" role="form">
							<div class="form-group">
								<label class="col-sm-3" style="padding: 10px 0px 10px 40px">To:</label>
								<div class="col-sm-8">

									<div class="inner-addon left-addon">
										<i class="glyphicon glyphicon-calendar"></i> <input
											class="form-control" type="text" id="to_datepicker">
									</div>

								</div>
								
							</div>

						</form>
					</div>
					
					<div class="col-md-1">
				<div class="row">
					<div class="col-md-12" style="border: 0px solid #000;">
						<form class="form-horizontal" role="form">
							<div class="form-group">
								<div class="col-md-12">
									<center>
										<input type="button" class="btn btn-primary" id="execute"
											name="Execute" value="Execute" />
									</center>
								</div>

							</div>

						</form>
					</div>
				</div>

			</div>
				
		</div>

		<div class="row">
			<div class="col-md-12">
					
						<div class="row">
							<div class="col-md-12">
								<div class="panel-body" id="chart_container_time"></div>
							</div>

						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="panel-body" id="chart_container_user"></div>
							</div>
							<div class="col-md-6">
								<div class="panel-body" id="chart_container_ip"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">

								<div class="panel-body" id="chart_container_status"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">

								<div class="panel-body" id="chart_container_url"></div>
							</div>
						</div>
			</div>

		</div>

	</div>
	<!-- /container -->

	<footer>
		<%@ include file="../common/footer.jsp"%>
	</footer>

</body>
</html>