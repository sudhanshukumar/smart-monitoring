<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Smart IT :: Notification Group</title>




<!-- common css -->
<link href="css/custom/general/style1366.css" rel="stylesheet" />
<link href="css/custom/general/style1024.css" rel="stylesheet" />
<link href="css/custom/general/bluetabs.css" rel="stylesheet" />

<!-- import css -->
<link href="css/common/ui.jqgrid.css" rel="stylesheet" type="text/css" />
<link href="css/common/jquery-ui-1.9.1.custom.css" rel="stylesheet"
	type="text/css" />

<!-- jQuery scripts -->
<script src="scripts/common/jquery-1.9.1.js"></script>
<script src="scripts/common/grid.locale-en.js"></script>
<script src="scripts/common/jquery.jqGrid.min.js"></script>
<script src="scripts/common/jquery-ui.js"></script>



<!-- custom script -->
<script src="scripts/custom/manageNotification.js"></script>
<script src="scripts/common/common.js"></script>
<script type="text/javascript" src="scripts/common/menu/dropdowntabs.js"></script>

<!-- jQuery style sheets -->
<link href="css/common/jquery-ui.css" rel="stylesheet" />



<style type="text/css">
body {
  margin-left: 0px;
  margin-top: 0px;
  margin-right: 0px;
  margin-bottom: 0px;
}
</style>
	
</head>
<body>

	<header>
		<%@ include file="../common/header.jsp"%>
	</header>


	 <nav id="bluemenu" class="bluetabs">
 		 <ul>
 		 <li><a href="dashboard.html"><spring:message code="label.admin.menu1"></spring:message></a></li>
  		 <li><a href="problemHistory.html"><spring:message code="label.admin.menu2"></spring:message></a></li>
  		 <li><a href="#" rel="dropmenu1_b"><spring:message code="label.admin.menu3"></spring:message></a></li>
		 <li><a  id="selected" href="manageNotificationGroup.html"><spring:message code="label.admin.menu4"></spring:message></a></li>
<%--   		 <li><a href="variableAnalysis.html"><spring:message code="label.admin.menu5"></spring:message></a></li> --%>
  		 <li><a href="#" rel="dropmenu1_solr"><spring:message 
						code="label.admin.solr.reports"></spring:message></a></li>
  		 <li><a  href="query.html"><spring:message code="label.admin.solr.query"></spring:message></a></li>
  		   <li><a href="correlation.html"><spring:message
						code="label.admin.correlation"></spring:message></a></li>
	  </ul>
 	 </nav>
 	  <div class="ddcolortabsline">&nbsp;</div>
 	  
 	  <div id="dropmenu1_b" class="dropmenudiv_b">
  <a href="manageDevice.html"><spring:message code="label.admin.menu3.submenu1"></spring:message></a>
  <a href="manageThresHold.html"><spring:message code="label.admin.menu3.submenu2"></spring:message></a>
<%--   <a href="manageApplication.html"><spring:message code="label.admin.menu3.submenu3"></spring:message></a> --%>
  <a href="manageService.html"><spring:message code="label.admin.menu3.submenu4"></spring:message></a>
<%--   <a href="manageUsers.html"><spring:message code="label.admin.menu3.submenu5"></spring:message></a> --%>
<a href="manageLog.html"><spring:message code="label.admin.menu3.submenu6"></spring:message></a>
    
  </div>
  
  <div id="dropmenu1_solr" class="dropmenudiv_b">
		<a href="userdashboard.html"><spring:message
				code="label.admin.solr.dashboard"></spring:message></a> <a
			href="charts.html"><spring:message
				code="label.admin.solr.charts"></spring:message></a> <a
			href="createuserdashboard.html"><spring:message
				code="label.admin.solr.custom.dashboard"></spring:message></a> <a
			href="list_dashboard.html"><spring:message
				code="label.admin.solr.view.dashboard"></spring:message></a> 

	</div>
  
   <script type="text/javascript">
	//SYNTAX: tabdropdown.init("menu_id", [integer OR "auto"])
	tabdropdown.init("bluemenu")
	</script>

		<!-- <input type="button" id="addNewNotification" value="addNewNotification" /> -->
		<div id="manageNotificationInnerContaine" class="allInnerContainer">
		<div id = "alert"></div>
		<!-- 	<div>
				<input type="button" id="addNewNotification"
					value="+ New Notification" class="buttonClass  addNew" />
			</div> -->
			
			<div id="notificationGrid" class="grid_manageDevice">
				<table id="addednotificationGrpHistory"></table>
				<div id="addednotificationGrpPager"></div>
			</div>
			
				<div>
				<input type="button" id="addNewNotification"
					value="+ New Notification" class="buttonClass  addNew" />
				</div>
			
		</div>
<div id ="alert"></div>
<div id="confirmDelete"></div>	
		<footer>
		<%@ include file="../common/footer.jsp"%>
	</footer>


</body>
</html>