package com.lnt.smartit.mysql.daoImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lnt.smartit.models.AlertMessage;
import com.lnt.smartit.mysql.dao.ITSMAdapterDao;
import com.lnt.smartit.mysql.model.AppServiceComponentParams;
@Repository
@Transactional
public class ITSMAdapterDaoImpl implements ITSMAdapterDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Value("${application.name}")
	String appId;

	@Override
	public String getServiceCompNameForITSM() {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		//Query query = session.createSQLQuery("SELECT t1.service_component_code FROM app_service_comp t1 inner join application t2 on t1.app_id = t2.id where t1.service_comp_type_code = 'ITSM' and t2.name = :appId");
		Query query = session.createQuery("SELECT t1.serviceComponent.serviceCompId FROM AppServiceComponent t1 WHERE t1.typecode.serviceCompTypeId = 'ITSM' AND t1.app.appName = :appId");
		query.setString("appId", appId);
		List<String> serviceCompCode =  query.list();
		if(serviceCompCode.size()>0){
			return serviceCompCode.get(0);
		}		
		return null;
	}

	@Override
	public List<AppServiceComponentParams> getServiceCompParamsForITSM() {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		List<AppServiceComponentParams> appServiceComponentParams = null;
		/*Criteria c = session.createCriteria(AppServiceComponentParams.class, "appServiceComponentParams");
		c.createAlias("appServiceComponentParams.appServiceComp", "appServiceComp");
		c.add(Restrictions.eq("appServiceComp", getServiceCompNameForITSM()));
		List<AppServiceComponentParams> appServiceComponentParams = c.list();*/
		Query query = session.createQuery("select t1 from AppServiceComponentParams t1 where t1.appServiceComp.app.appName = :appId and t1.appServiceComp.serviceComponent.serviceCompId = :serviceCompTypeId");
		query.setParameter("appId", appId);
		query.setParameter("serviceCompTypeId", getServiceCompNameForITSM());
		appServiceComponentParams = query.list();	
		return appServiceComponentParams;
	}

	@Override
	public List<String> getAletTypeForIncidentCreation(String hostname) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createSQLQuery("SELECT t1.param_name FROM app_log_param t1 inner join app_log_param_rule_xref t2 on t1.id =t2.app_log_param_id inner join app_log t3 on t1.app_log_id = t3.id inner join application t4 on t4.id = t3.app_id inner join app_device t5 on t3.app_device_id = t5.id inner join app_device_param t6 on t6.app_device_id = t5.id where (t6.name = 'hostname' or t6.name = 'ip address') and t6.value = :hostnameValue  and t4.name = :appId and t2.itsm_req_flag = 'Yes'");
		//Query query = session.createQuery("select t1.logParam.paramName from LogParamsRuleMapping t1 inner join DeviceParams t2 on t1.logParam.log.device.deviceId = t2.appDevice.deviceId where t2.name = 'hostname' and t2.value = :hostnameValue and t1.logParam.log.app.appName = :appId and t1.itsmReqFlag = 'y'");
		query.setString("hostnameValue", hostname);
		query.setString("appId", appId);
		List<String> alerttypeList  = query.list();
		return alerttypeList;
	}

	@Override
	public List<String> checkItsmReqFlag() {
		// TODO Auto-generated method stub
		List<String> alertsList = null;
		Session session = sessionFactory.getCurrentSession();
		//Query query = session.createSQLQuery("SELECT t1.param_name FROM app_log_param t1 inner join app_log_param_rule_xref t2 on t1.id =t2.app_log_param_id inner join app_log t3 on t1.app_log_id = t3.id inner join application t4 on t3.app_id = t4.id  where t4.name = :applicationName and t2.itsm_req_flag = 'Yes'");
		Query query = session.createQuery("SELECT t1.logParam.paramName from LogParamsRuleMapping t1 where t1.logParam.log.app.appName = :applicationName and t1.itsmReqFlag = 'Yes'");
		query.setString("applicationName", appId);
		alertsList = query.list();
		return alertsList;
	}
	
	@Override
	public int getDeduplicationWindowForIncidentCreation(AlertMessage alertMsg) {
		// TODO Auto-generated method stub
		int deduplicationWindow = 0;
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createSQLQuery("SELECT t2.deduplication_window FROM app_log_param t1 INNER JOIN app_log_param_rule_xref t2 ON t1.id =t2.app_log_param_id INNER JOIN app_log t3 ON t1.app_log_id = t3.id INNER JOIN application t4 ON t4.id = t3.app_id INNER JOIN app_device t5 ON t3.app_device_id = t5.id INNER JOIN app_device_param t6 ON t6.app_device_id = t5.id WHERE (t6.name = 'hostname' OR t6.name = 'ip address') AND t2.itsm_req_flag = 'Yes' AND t6.value = :hostnameValue  AND t4.name = :appId  AND t1.param_name = :paramName");
		//Query query = session.createQuery("select t1.logParam.paramName from LogParamsRuleMapping t1 inner join DeviceParams t2 on t1.logParam.log.device.deviceId = t2.appDevice.deviceId where t2.name = 'hostname' and t2.value = :hostnameValue and t1.logParam.log.app.appName = :appId and t1.itsmReqFlag = 'y'");
		query.setString("hostnameValue", alertMsg.getAlertMessageHostName());
		query.setString("appId", appId);
		query.setString("paramName", alertMsg.getAlertMessageparameterName());		
		List<Integer> alerttypeList  = query.list();
		if(alerttypeList != null){
			deduplicationWindow = alerttypeList.get(0);
		}
		return deduplicationWindow;
	}
	
	//10614080 on 09/03/2017 start	
	@Override
	public Map<Integer, HashMap<String,String>> getNotificationDetails(String hostname) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session
				.createSQLQuery("SELECT t2.email_to, t2.email_cc,t1.param_name  FROM app_log_param t1 inner join app_log_param_rule_xref t2 on t1.id =t2.app_log_param_id inner join app_log t3 on t1.app_log_id = t3.id inner join application t4 on t4.id = t3.app_id inner join app_device t5 on t3.app_device_id = t5.id inner join app_device_param t6 on t6.app_device_id = t5.id where (t6.name = 'hostname' or t6.name = 'ip address') and t6.value = :hostnameValue  and t4.name = :appId and t2.notification_flag = 'Yes'");
		
		query.setString("hostnameValue", hostname);
		query.setString("appId", appId);
		Map<Integer, HashMap<String,String>> finalmap = new HashMap<Integer, HashMap<String,String>>();
		List<Object> alerttypeList = query.list();
		int count=1;
		for (Object obj : alerttypeList) {
			Map<String, String> map = new HashMap<String, String>();
			Object[] row = (Object[]) obj;
			map.put("toAddr", row[0].toString());
			map.put("ccAddr", row[1].toString());
			map.put("param", row[2].toString());
			finalmap.put(count, (HashMap<String, String>) map);
			count++;
		}
		return finalmap;
	}
	//10614080 on 09/03/2017 end
}
