/**
 * 
 */
$(document).ready(function() {
	document.getElementById('headingManageServices').innerHTML= 'Add New Service';
	$("#alert").hide();
	$("#preview_service_buttons").hide();
	$("#preview_ok").click(function(){
		window.location.href = "manageService.html";
	});
	var action = getQueryVariable("action");
	var id=getQueryVariable("softwareId");
	if(action == "edit")
		{
		$("#preview_service_buttons").hide();
		document.getElementById('headingManageServices').innerHTML= 'Update Service';
		$.ajax({
			type : "POST",
			url : "editService.html",
			data : "softwareId=" + id, 
			success : function(response) {

				$('#serviceName').attr('readonly', false);
				$('#service_desc').attr('readonly', false);
				$('#host_name').attr('readonly', false);
				$('#serviceName').val(response.softwareName);
				$('#service_desc').val(response.description);
				$('#host_name').val(response.hostName);
				}
	        //cache: false
		});
		}
	
	if(action == "preview"){
		$("#preview_service_buttons").show();
		$("#addServices_buttons").hide();
		document.getElementById('headingManageServices').innerHTML= 'Preview Service';
		$.ajax({
		type : "POST",
		url : "previewService.html",
		data : "softwareId=" + id, 
		success : function(response) {
			$('#serviceName').val(response.softwareName);
			$('#service_desc').val(response.description);
			$('#host_name').val(response.hostName);
			$('#serviceName').attr('readonly', true);
			$('#service_desc').attr('readonly', true);
			$('#host_name').attr('readonly', true);
		}
       // cache: false
	});
}
	
	$("#addServices_submit").click(function() {
		addService_submit(id);
	});
	
	$("#addServices_cancel").click(function() {
		window.location.href="manageService.html";
	});
});

function addService_submit(id) {
	
	var service_Name,service_desc,host_name;
	service_Name=$('#serviceName').val();
	service_desc=$('#service_desc').val();
	host_name=$('#host_name').val();
	if(id == false){
	id = "null";
	}
	$.ajax({
		type : "POST",
		url : "addServiceOnSubmit.html",
		data : 'softwareName='+service_Name +'&description='+service_desc +'&softId='+id +'&hostName='+host_name,
		
		success : function(response) {
			if(response.indexOf("mandatory")>-1 || response.indexOf("exists")>-1){
				alert(response);
			}else{
				alert(response);
				window.location.href="manageService.html";
			}
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    	alert(jqXHR);
	    	alert(textStatus);
	    	alert(errorThrown);
	    }
	//	cache : false
	}); 
	
}

function getQueryVariable(variable)
{
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}