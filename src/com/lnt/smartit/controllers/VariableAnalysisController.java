package com.lnt.smartit.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.lnt.smartit.mysql.model.ApplicationMaster;
import com.lnt.smartit.mysql.model.DeviceMaster;
import com.lnt.smartit.mysql.model.GridPojo;
import com.lnt.smartit.mysql.model.VariableAnalysis;
import com.lnt.smartit.serviceImpl.VariableAnalysisServiceImpl;

@Controller
public class VariableAnalysisController {
	
	@Autowired
	VariableAnalysisServiceImpl variableAnalysisService;
	private final Logger logger = LoggerFactory.getLogger(VariableAnalysisController.class.getName());

	//redirection onclick of the menu for variable analysis
	@RequestMapping(value="variableAnalysis.html")
	public ModelAndView variableAnalysis() {
		logger.debug("..........variable analysis........");
		return new ModelAndView("Dashboard/variableAnalysis");
	}
	
	@RequestMapping(value="predictionreport.html")
	public ModelAndView prdictionReport() {
		return new ModelAndView("Dashboard/PredictionKibana");
	}
	
	@RequestMapping(value="trendingreport.html")
	public ModelAndView trendingReport() {
		return new ModelAndView("Dashboard/TrendingKibana");
	}
	
	@RequestMapping(value="alerthistoryreport.html")
	public ModelAndView alertHistoryReport() {
		return new ModelAndView("Dashboard/AlertHistoryKibana");
	}
	
	@RequestMapping(value="correlationreport.html")
	public ModelAndView correlationReport() {
		return new ModelAndView("Dashboard/LogCorrelationkibana");
	}
	
	@RequestMapping(value="weblogicreport.html")
	public ModelAndView weblogicReport() {
		return new ModelAndView("Dashboard/WeblogicReportkibana");
	}
	
	@RequestMapping(value="weblogicdrilldownreport.html")
	public ModelAndView weblogicdrilldownReport() {
		return new ModelAndView("Dashboard/DrillDownReportKibana");
	}
	@RequestMapping(value="sapapplicationreport.html")
	public ModelAndView sapAppReport() {
		return new ModelAndView("Dashboard/SapAppkibana");
	}
	@RequestMapping(value="sapapplicationadoption.html")
	public ModelAndView sapAppAdoption() {
		return new ModelAndView("Dashboard/ApplicationAdoption");
	}
	@RequestMapping(value="sapplatformoperation.html")
	public ModelAndView sapPlatformOperation() {
		return new ModelAndView("Dashboard/PlatformOperations");
	}
	@RequestMapping(value="sapsecurityauditing.html")
	public ModelAndView sapSecurityAuditing() {
		return new ModelAndView("Dashboard/Securityauditing");
	}
	
	//redirection for plotting the response time graph in split2
	@RequestMapping(value = "drawHistoricGraphforResponseTime.html", method = RequestMethod.POST)
	public @ResponseBody String drawHistoricGraph(HttpSession session, HttpServletRequest request) {
		String addressedMachine = null;
		String respAppName = null;
		String respPageName = null;
		String osType=null;
		String timerange=null;
		String xAxis=null;
		String yAxis=null;
	    logger.debug("SmartIT_UX::com.lnt.controllers::DashboardController.getAdditionalData():: called");
		String parameterName = request.getParameter("parameterName").toString();
		if(request.getParameter("addressedMachine") != null){
		 addressedMachine = request.getParameter("addressedMachine").toString();
		} if(request.getParameter("urlName") != null){
			respAppName = request.getParameter("urlName");
		}if(request.getParameter("page") != null){
			respPageName = request.getParameter("page");
		}if(request.getParameter("osType") != null){
			osType = request.getParameter("osType").toString();
		}if(request.getParameter("timerange")!=null){
			timerange=request.getParameter("timerange").toString();
		}if(request.getParameter("xAxisField")!=null){
			xAxis=request.getParameter("xAxisField").toString();
		}if(request.getParameter("yAxisField")!=null){
			yAxis=request.getParameter("yAxisField").toString();
		}
		String historicGraphList = variableAnalysisService.drawHistoricResponseTimeGraph(parameterName/*, addressedMachine,respAppName,respPageName,osType,timerange,xAxis,yAxis*/);
		return historicGraphList;
	}
	
	//redirection for plotting the prediction graph
	@RequestMapping(value = "showPredictionGraph.html", method = RequestMethod.POST)
	public @ResponseBody List<VariableAnalysis> showResponseTimePredictionGraph(@Valid VariableAnalysis variableAnalysis,BindingResult bindingResult,
			HttpSession session, HttpServletRequest request) {
		logger.debug("variable analysis in controller for prediction graph");
		List<VariableAnalysis> predictionGraphList = new ArrayList<VariableAnalysis>();
		predictionGraphList = variableAnalysisService.drawResponseTimePredictionGraphs(variableAnalysis.getPage());
		return predictionGraphList;
	}
	
	//redirection for plotting related graphs for CPU
	@RequestMapping(value = "plotRelatedVariabesChartsCPU.html", method = RequestMethod.POST)
	public @ResponseBody List<VariableAnalysis> plotRelatedVariablesChartCPU(@Valid VariableAnalysis variableAnalysis,BindingResult bindingResult,
			HttpSession session, HttpServletRequest request) {
		logger.debug("variable analysis in controller for showing related variables graphs");
		List<VariableAnalysis> plotRelatedVariablesChartList = new ArrayList<VariableAnalysis>();
		plotRelatedVariablesChartList = variableAnalysisService.plotRelatedVariablesChartCPU(variableAnalysis.getServerIPAddress());
		return plotRelatedVariablesChartList;
	}
	
	//redirection for plotting related graphs for RAM
	@RequestMapping(value = "plotRelatedVariabesChartsRAM.html", method = RequestMethod.POST)
	public @ResponseBody List<VariableAnalysis> plotRelatedVariablesChartRAM(@Valid VariableAnalysis variableAnalysis,BindingResult bindingResult,
			HttpSession session, HttpServletRequest request) {
		logger.debug("variable analysis in controller for showing related variables graphs");
		List<VariableAnalysis> plotRelatedVariablesChartList = new ArrayList<VariableAnalysis>();
		plotRelatedVariablesChartList = variableAnalysisService.plotRelatedVariablesChartRAM(variableAnalysis.getServerIPAddress());
		return plotRelatedVariablesChartList;
	}
	
	//redirection for plotting related graphs for IO
	@RequestMapping(value = "plotRelatedVariabesChartsIO.html", method = RequestMethod.POST)
	public @ResponseBody List<VariableAnalysis> plotRelatedVariablesChartIO(@Valid VariableAnalysis variableAnalysis,BindingResult bindingResult,
			HttpSession session, HttpServletRequest request) {
		logger.debug("variable analysis in controller for showing related variables graphs");
		List<VariableAnalysis> plotRelatedVariablesChartList = new ArrayList<VariableAnalysis>();
		plotRelatedVariablesChartList = variableAnalysisService.plotRelatedVariablesChartIO(variableAnalysis.getServerIPAddress());
		return plotRelatedVariablesChartList;
	}
	
	//fetching application List to autopopulate
	@RequestMapping(value="fetchApplicationList.html")
	public  @ResponseBody List<ApplicationMaster> fetchApplicationList() {
		logger.debug("..........fetching application list........");
		List<ApplicationMaster> fetchApplicationList = new ArrayList<ApplicationMaster>();
		fetchApplicationList = variableAnalysisService.fetchApplicationList();
		return fetchApplicationList;
	}
	
	//fetching server List to autopopulate
	@RequestMapping(value="fetchServerList.html")
	public  @ResponseBody List<DeviceMaster> fetchServerList() {
		logger.debug("..........fetching server list........");
		List<DeviceMaster> fetchServerList = new ArrayList<DeviceMaster>();
		fetchServerList = variableAnalysisService.fetchServerList();
		return fetchServerList;
	}
	
	//fetching the application List to display in the grid
	@RequestMapping(value = "populateApplicationListGrid.html", method = RequestMethod.POST)
	public @ResponseBody
	GridPojo populateApplicationListGrid(HttpServletRequest request, GridPojo gridPojo) {
		logger.debug("entering variable analysis controller for application list grid");
		List<ApplicationMaster> applicationList = new ArrayList<ApplicationMaster>();
		try {
			applicationList = variableAnalysisService.fetchApplicationListForGrid();
			gridPojo.setGridData(applicationList.toArray());
			gridPojo.setRows(applicationList.size());
			gridPojo.setRecords(applicationList.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return gridPojo;
	}			
					
	//fetching the server List to display in the grid
	@RequestMapping(value = "populateServerListGrid.html", method = RequestMethod.POST)
	public @ResponseBody GridPojo populateAddedDeviceGrid(HttpServletRequest request, GridPojo gridPojo) {
		logger.debug("entering variable analysis controller forserver list grid");
		List<DeviceMaster> serverListForGrid = new ArrayList<DeviceMaster>();
		try {
			serverListForGrid = variableAnalysisService.fetchServerList();
			gridPojo.setGridData(serverListForGrid.toArray());
			gridPojo.setRows(serverListForGrid.size());
			gridPojo.setRecords(serverListForGrid.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return gridPojo;
	}			

	//redirection for plotting correlated graphs for CPU
	@RequestMapping(value = "showCorrelatedDataForRelatedVariablesCPU.html", method = RequestMethod.POST)
	public @ResponseBody List<VariableAnalysis> showCorrelatedDataForRelatedVariablesCPU(@Valid VariableAnalysis variableAnalysis,BindingResult bindingResult,
			HttpSession session, HttpServletRequest request) {
		logger.debug("end date in controller ::::::::::"+variableAnalysis.getEndDate());
		List<VariableAnalysis> plotCorelatedRelatedVariablesChartListCPU = new ArrayList<VariableAnalysis>();
		plotCorelatedRelatedVariablesChartListCPU = variableAnalysisService.showCorrelatedDataForRelatedVariablesCPU(variableAnalysis);
		return plotCorelatedRelatedVariablesChartListCPU;
	}
	
	//redirection for plotting correlated graphs for RAM
	@RequestMapping(value = "showCorrelatedDataForRelatedVariablesRAM.html", method = RequestMethod.POST)
	public @ResponseBody List<VariableAnalysis> showCorrelatedDataForRelatedVariablesRAM(@Valid VariableAnalysis variableAnalysis,BindingResult bindingResult,
			HttpSession session, HttpServletRequest request) {
		logger.debug("variable analysis in controller for correlation showing related variables graphs for RAM");
		List<VariableAnalysis> plotCorelatedRelatedVariablesChartListRAM = new ArrayList<VariableAnalysis>();
		plotCorelatedRelatedVariablesChartListRAM = variableAnalysisService.showCorrelatedDataForRelatedVariablesRAM(variableAnalysis);
		return plotCorelatedRelatedVariablesChartListRAM;
	}
	
	//redirection for plotting correlated graphs for IO
	@RequestMapping(value = "showCorrelatedDataForRelatedVariablesIO.html", method = RequestMethod.POST)
	public @ResponseBody List<VariableAnalysis> showCorrelatedDataForRelatedVariablesIO(@Valid VariableAnalysis variableAnalysis,BindingResult bindingResult,
			HttpSession session, HttpServletRequest request) {
		logger.debug("variable analysis in controller for correlation showing related variables graphs for IO");
		List<VariableAnalysis> plotCorelatedRelatedVariablesChartListIO = new ArrayList<VariableAnalysis>();
		plotCorelatedRelatedVariablesChartListIO = variableAnalysisService.showCorrelatedDataForRelatedVariablesIO(variableAnalysis);
		return plotCorelatedRelatedVariablesChartListIO;
	}
}
