package com.lnt.automation.filter;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.util.Properties;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginFilter implements Filter{

	protected FilterConfig filterConfig;
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest)request;
		HttpServletResponse httpResponse = (HttpServletResponse)response;
		Properties prop = new Properties();
		try{
			InputStream paramPropertiesStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("params.properties");
			prop.load(paramPropertiesStream);
			String loginRequired = (prop.get("isLoginRequired")).toString();
			if(loginRequired != null && loginRequired.equalsIgnoreCase("YES"))
				identifyApplication(httpRequest, httpResponse, chain);
			else{
				chain.doFilter(request, response);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	private void identifyApplication(ServletRequest request,ServletResponse response,FilterChain chain) throws IOException {

		HttpServletRequest httpRequest = (HttpServletRequest)request;
		HttpServletResponse httpResponse = (HttpServletResponse)response;
		HttpSession session= httpRequest.getSession(true);
		System.out.println("Requested URL == "+httpRequest.getRequestURI());
		Properties prop = new Properties();
		String uri = null;
		if(httpRequest.getRequestURL()+"?"+httpRequest.getQueryString()!=null) {
			uri = httpRequest.getRequestURL() + "?" + httpRequest.getQueryString();
			System.out.println(uri);
		}
		String queryString = httpRequest.getQueryString();
		System.out.println("queryString   "+queryString + " --- " +"session attribute  "+ session.getAttribute("userIdInSession"));

		if ((session.getAttribute("isUserValidated")) != null && (session.getAttribute("isUserValidated")).equals("Y")) {
			System.out.println("-- User Already Validated!! --");
			try {
				chain.doFilter(request, response);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return;
		}
		//Redirect user to login
		if(queryString==null && ((session.getAttribute("userIdInSession"))==null)){
			System.out.println("---User Not Validated yet.. redirect to Login---");
			try(InputStream paramPropertiesStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("params.properties")){
				prop.load(paramPropertiesStream);
				String loginUrl = (prop.get("loginUrl")).toString();
				httpResponse.sendRedirect(loginUrl);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else{
			System.out.println("---User has been Validated now.. set the user in the session and redirect to home---");
			String attrs = URLDecoder.decode(queryString, "UTF-8");
			String attributes[] = attrs.split("&");
			String user = attributes[0].split("=")[1];
			String modules = attributes[1].split("=")[1];
			/*String role = attributes[4].split("=")[1];
			System.out.println("#### role  LoginFilter --> "+role);*/
			session.setAttribute("username", attributes[2].split("=")[1]);
			session.setAttribute("userIdInSession", user);
			session.setAttribute("isUserValidated", "Y");
			session.setAttribute("modules", modules);
		//	session.setAttribute("role", attributes[4].split("=")[1]);
			InputStream paramPropertiesStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("params.properties");
			prop.load(paramPropertiesStream);
			String sessionValidUrl = (prop.get("sessionValidUrl")).toString();
			String site = sessionValidUrl;
//			String site = new String("/resln/index.jsp");
			httpResponse.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
			httpResponse.setHeader("Location", site);
			
			httpResponse.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
			httpResponse.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
			httpResponse.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
			httpResponse.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility
		}
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
	}

}
