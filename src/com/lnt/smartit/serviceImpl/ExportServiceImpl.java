package com.lnt.smartit.serviceImpl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.lnt.smartit.models.GridHeaderModel;
import com.lnt.smartit.services.ExportService;
import com.lnt.smartit.util.ExportUtil;

@Service
public class ExportServiceImpl implements ExportService{

	@Override
	public boolean exportAsPDF(String pdfFilePath,
			List<Map<String, Object>> gridData, List<GridHeaderModel> headers,
			Integer columnCount) {
		// TODO Auto-generated method stub
		return ExportUtil.exportAsPDF(pdfFilePath, gridData, headers, columnCount);
	}

	@Override
	public boolean exportAsExcel(String excelFilePath, String sheetName,
			List<Map<String, Object>> gridData, List<GridHeaderModel> headers) {
		// TODO Auto-generated method stub
		return ExportUtil.exportAsExcel(excelFilePath, sheetName, gridData, headers);
	}		

}
