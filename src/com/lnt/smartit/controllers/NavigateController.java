package com.lnt.smartit.controllers;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.lnt.smartit.constants.MenuToPageMappings;
import com.lnt.smartit.helper.AMQListner;
import com.lnt.smartit.mysql.model.AppLogServiceComponentParam;
import com.lnt.smartit.services.LogService;
import com.lnt.smartit.services.ProblemHistoryService;
import com.lnt.smartit.services.QueryService;
import com.lnt.smartit.util.DatabaseConfig;

@Controller
public class NavigateController {
	@Autowired
	@Qualifier("amqListner")
	private AMQListner amqListner;
	
	@Autowired
	private DatabaseConfig dbconfig;
	
	@Autowired
	QueryService queryService;
	
	@Autowired
	private ProblemHistoryService problemHistoryService;
	
	@Autowired
	private LogService logService;
	
	private static Logger logger = Logger.getLogger(NavigateController.class);

	
	@RequestMapping(value = "dashboard.html", method = RequestMethod.GET)
	public ModelAndView dashboardPage(HttpServletRequest request,HttpServletResponse response,ModelMap map) {
		logger.debug("NavigateController :: getDashboardPage :: Dashboard");
		String session_val = (String)request.getSession().getAttribute("modules");
        String page = null;
		if(session_val != null){
			Map<String, String> menuPageMap = MenuToPageMappings.menuPageMap;
			for(Entry<String, String> entry : menuPageMap.entrySet()){
				if(session_val.contains(entry.getKey())){
					page = entry.getValue();
		 		    break;
				}
			}
			if(page.equalsIgnoreCase("AlertHistory")){
				//problemHistoryService.load();
			}
			else if(page.equalsIgnoreCase("LogAnalysis")){
				String appname = null;
				InputStream paramPropertiesStream= null;
				try{
					Properties props = new Properties();
					paramPropertiesStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("params.properties");
					props.load(paramPropertiesStream);
					appname = props.getProperty("application.name");
				}
				catch(FileNotFoundException e){
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				finally{
					try {
						paramPropertiesStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				String appId = logService.getAppIdByName(appname);
				String id = logService.getAppServiceComponentId(appId, "RAW_STORE", "SOLR");
				List<AppLogServiceComponentParam> collectionnames = queryService.getCollectionNames(id);
				Map<String, String> colMap = new HashMap<String, String>();
				for(AppLogServiceComponentParam app : collectionnames){
					colMap.put(app.getParamValue(), app.getDescription());
				}
				System.out.println("Collections == "+colMap);
				map.addAttribute("Collections", colMap);
			}
			return new ModelAndView(page);
		} else {
			return new ModelAndView("Dashboard/dashboard");
		}
	}

	@RequestMapping(value = "problemHistory.html")
	public ModelAndView openProblemHistory(ModelMap map) {
		//problemHistoryService.load();
		/*List<String> hostnames = problemHistoryService.getDistinctHostnames();
		List<String> params = problemHistoryService.getDistinctParameters();
		List<String> alerttypes = problemHistoryService.getDistinctAlertTypes();
		map.addAttribute("systems", hostnames);
		map.addAttribute("params", params);
		map.addAttribute("alerttypes", alerttypes);
		System.out.println(map.toString());*/
		return new ModelAndView("History/admin");
	}
	
	@RequestMapping(value = "logout.html")
	public void processLogout(HttpServletRequest request,HttpServletResponse response){
		HttpSession session = request.getSession(false);
		if(session != null){
			session.setAttribute("userIdInSession", null);
			session.setAttribute("isUserValidated",null);
			session.invalidate();
		} else {
			//do nothing
		}
	}
}
