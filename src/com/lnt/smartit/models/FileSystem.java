/**
 * 
 */
package com.lnt.smartit.models;

/**
 * @author 299273
 *
 */
public class FileSystem implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private Double size;
	private Double used;
	private Double avail;
	private Double usePercent;
	private String mountedOn;
	private int dataReceivedTime_hours;

	public int getDataReceivedTime_hours() {
		return dataReceivedTime_hours;
	}

	public void setDataReceivedTime_hours(int dataReceivedTime_hours) {
		this.dataReceivedTime_hours = dataReceivedTime_hours;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getSize() {
		return size;
	}

	public void setSize(Double size) {
		this.size = size;
	}

	public Double getUsed() {
		return used;
	}

	public void setUsed(Double used) {
		this.used = used;
	}

	public Double getAvail() {
		return avail;
	}

	public void setAvail(Double avail) {
		this.avail = avail;
	}
	
	public Double getUsePercent() {
		return usePercent;
	}

	public void setUsePercent(Double usePercent) {
		this.usePercent = usePercent;
	}

	public String getMountedOn() {
		return mountedOn;
	}

	public void setMountedOn(String mountedOn) {
		this.mountedOn = mountedOn;
	}

	@Override
	public String toString() {
		return "FileSystem [name=" + name + ", size=" + size + ", used=" + used
				+ ", avail=" + avail + ", usePercent=" + usePercent
				+ ", mountedOn=" + mountedOn + "]";
	}
}
