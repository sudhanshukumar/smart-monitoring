package com.lnt.smartit.observer;

import org.springframework.stereotype.Component;

@Component
public interface QueueSubjectI {
	void pushDataInQueue();
	public void register(ObserverI observer);
	public void unRegister(ObserverI observer);
	public void notifyObservers();
	public Object getUpdates(ObserverI observer);
}
