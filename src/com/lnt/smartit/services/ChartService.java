package com.lnt.smartit.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.lnt.smartit.mysql.model.AppLogServiceComponentParam;
import com.lnt.smartit.mysql.model.Applications;
import com.lnt.smartit.mysql.model.DashboardMaster;
import com.lnt.smartit.mysql.model.ManageReport;
import com.lnt.smartit.mysql.model.Report;
import com.lnt.smartit.mysql.model.ReportMetadata;
import com.lnt.smartit.solr.pojo.Charting;
import com.lnt.smartit.solr.pojo.Dashboard;
import com.lnt.smartit.solr.pojo.DashboardUpdate;

@Service
public interface ChartService {
	
	public String saveChartMaster(Report report);
	public List<Report> getCharts();
	public List<Charting> drawChart(Report chart);
	public Boolean savedashboard(List<DashboardMaster> dashboard);
	public List<DashboardMaster> getDashboards();
	public List<Dashboard> drawDashboard(DashboardMaster dash_name);
	public List<Applications> getApplications();
	public List<AppLogServiceComponentParam> getCollectionsFromDB(String appsel);
	public Boolean saveChartMaster(ReportMetadata reportmeta);
	public List<Charting> getUpdatedReport(DashboardUpdate dashboardUpdate);
	public List<Report> getReports();
	public Boolean deleteReports(String[] selected_reports);
	public Boolean selectReports(List<ManageReport> manageReportList);
	public List<ManageReport> getManageReportFields(String collection);
	public void undoFilterType(List<ManageReport> manageReportList);
	public void undoInvalid(List<ManageReport> manageReportList);
	public String getPrimaryFilter(String col);
	public Boolean deleteDashboards(String selected_reports);
	public Boolean setSequence(String[] sequenceArray, String dashName);
	public Boolean removeChartfromDash(String chart_name, String dashboardName);
	public String getReportIdfromName(String charts);
	List<String> getChartNameList();
	List<String> getDashboardNameList();
}
