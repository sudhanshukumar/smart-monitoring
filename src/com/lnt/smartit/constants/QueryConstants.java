package com.lnt.smartit.constants;

public class QueryConstants {
	
		  public static final String hive_connection_url    = "jdbc:hive2://172.25.7.91:10000/";
		  public static final String dbName                 = "smartitdb";
		  public static final String driver  			    = "org.apache.hive.jdbc.HiveDriver";
		  
		  public static final String page                    = "page";
		  public static final String server_ip_address       = "serverIPAddress";
		  public static String startDate  					= "startDate";
		 
		  public static final String historic_response_time_graph   = "SELECT DATE_FORMAT(data_datetime,'%d-%m-%Y %H'),count(url) as cnt,avg(ResponseTime) , DATE_FORMAT(data_datetime,'%d') , DATE_FORMAT(data_datetime,'%m'),DATE_FORMAT(data_datetime,'%Y'),DATE_FORMAT(data_datetime,'%H')  FROM access_logs a where url= :page group by DATE_FORMAT(data_datetime,'%d-%m-%Y %H') order by DATE_FORMAT(data_datetime,'%Y-%m-%d %H')";
		  public static final String prediction_response_time_graph = "SELECT DATE_FORMAT(access_date,'%d-%m-%Y'),actual,predicted  FROM response_time_prediction where url=:page group by DATE_FORMAT(access_date,'%d-%m-%Y') order by DATE_FORMAT(access_date,'%Y-%m-%d');";
		  public static final String responseTimeVsCPU_graph 		= "SELECT DATE_FORMAT(storeDateTime,'%d-%m-%Y'),avg(fiveminloadavg),avg(tenminloadavg),avg(fifteenminloadavg) , DATE_FORMAT(storeDateTime,'%d'), DATE_FORMAT(storeDateTime,'%m'),DATE_FORMAT(storeDateTime,'%Y'),DATE_FORMAT(storeDateTime,'%H') FROM raw_cpu_data r where ipaddress=:serverIPAddress group by DATE_FORMAT(storeDateTime,'%d-%m-%Y %H') order by DATE_FORMAT(storeDateTime,'%Y-%m-%d %H');";
		  public static final String responseTimeVsRAM_graph 		= "SELECT DATE_FORMAT(machinedatareceivable,'%d-%m-%Y'),avg(rammemused), DATE_FORMAT(machinedatareceivable,'%d'), DATE_FORMAT(machinedatareceivable,'%m'),DATE_FORMAT(machinedatareceivable,'%Y'),DATE_FORMAT(machinedatareceivable,'%H') FROM raw_ram_data r where ipaddress=:serverIPAddress group by DATE_FORMAT(machinedatareceivable,'%d-%m-%Y %H') order by DATE_FORMAT(machinedatareceivable,'%Y-%m-%d %H');";
		  public static final String responseTimeVsIO_graph 		= "SELECT DATE_FORMAT(storeDateTime,'%d-%m-%Y'),avg(iowait), DATE_FORMAT(storeDateTime,'%d'), DATE_FORMAT(storeDateTime,'%m'),DATE_FORMAT(storeDateTime,'%Y'),DATE_FORMAT(storeDateTime,'%H') FROM raw_io_data r where ipaddress=:serverIPAddress group by DATE_FORMAT(storeDateTime,'%d-%m-%Y %H')  order by DATE_FORMAT(storeDateTime,'%Y-%m-%d %H');";
		  
		  public static final String correlation_responseTimeVsCPU_graph = "SELECT DATE_FORMAT(storeDateTime,'%d-%m-%Y'),avg(fiveminloadavg),avg(tenminloadavg),avg(fifteenminloadavg) , DATE_FORMAT(storeDateTime,'%d'), DATE_FORMAT(storeDateTime,'%m'),DATE_FORMAT(storeDateTime,'%Y'),DATE_FORMAT(storeDateTime,'%H') FROM raw_cpu_data r where ipaddress=:serverIPAddress and storeDateTime > :startDate group by DATE_FORMAT(storeDateTime,'%d-%m-%Y %H') order by DATE_FORMAT(storeDateTime,'%Y-%m-%d %H')";
		  public static final String correlation_responseTimeVsRAM_graph = "SELECT DATE_FORMAT(machinedatareceivable,'%d-%m-%Y'),avg(rammemused), DATE_FORMAT(machinedatareceivable,'%d'), DATE_FORMAT(machinedatareceivable,'%m'),DATE_FORMAT(machinedatareceivable,'%Y'),DATE_FORMAT(machinedatareceivable,'%H') FROM raw_ram_data r where ipaddress=:serverIPAddress and machinedatareceivable  > :startDate group by DATE_FORMAT(machinedatareceivable,'%d-%m-%Y %H') order by DATE_FORMAT(machinedatareceivable,'%Y-%m-%d %H')";
		  public static final String correlation_responseTimeVsIO_graph = "SELECT DATE_FORMAT(storeDateTime,'%d-%m-%Y'),avg(iowait), DATE_FORMAT(storeDateTime,'%d'), DATE_FORMAT(storeDateTime,'%m'),DATE_FORMAT(storeDateTime,'%Y'),DATE_FORMAT(storeDateTime,'%H') FROM raw_io_data r where ipaddress=:serverIPAddress and storeDateTime  > :startDate 00:00:00' group by DATE_FORMAT(storeDateTime,'%d-%m-%Y %H')  order by DATE_FORMAT(storeDateTime,'%Y-%m-%d %H');";
		  //public static final String fetchApplicationList = "SELECT application_name FROM itpredictive.application_master a";
		  public static final String fetchApplicationList = "SELECT application_name FROM application_master a";
		  public static final String fetchDeviceList =  "SELECT ip_address FROM device_master d";
		  //public static final String fetchApplicationListGrid = "SELECT application_name FROM itpredictive.application_master a";
		  public static final String fetchApplicationListGrid = "SELECT application_name FROM application_master a";
	
		 

}
