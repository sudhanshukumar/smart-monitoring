$(document).ready(function() {
	getdashboards();
	getapplications();
	$("#filterform").hide();
	$("#dialog").hide();
	$("#confirmDelete").hide();
	$("#confirmDelete_reports").hide();
});
  

function showAlert(message){
    $( "#alertDialog" ).show();
    $("#alertDialog").text(message);
    $( "#alertDialog" ).dialog({
           buttons:{
                        "OK":function() {
                               $( this ).dialog( "close" );
                              }
                        } 
                                             });

}

/*function getdatepicker(count)
 {
	
	
	$('#from_datepicker_'+count).datetimepicker({
		format : 'Y-m-d H:i:s',
		dayOfWeekStart : 1,
		lang : 'en'
	});
	
	var d = new Date();
	d.setHours(d.getHours()-12);
	var temp_from_date=d.getDate();
	var temp_from_month=d.getMonth()+1;
	if(temp_from_date<10){
		temp_from_date="0"+temp_from_date;
	}
	
	if(temp_from_month<10){
		temp_from_month="0"+temp_from_month;
	}
	
	var temp_to_date=d.getFullYear()+"-"+temp_from_month+"-"+temp_from_date+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
	$('#from_datepicker_'+count).datetimepicker({
		value : temp_to_date
	});

	$('#to_datepicker_'+count).datetimepicker({
		format : 'Y-m-d H:i:s',
		dayOfWeekStart : 1,
		lang : 'en'
	});
	
	var dt = new Date();
	var temp_date=dt.getDate();
	var temp_month=dt.getMonth()+1;
	
	if(temp_date<10){
		temp_date="0"+temp_date;
	}
	
	if(temp_month<10){
	temp_month="0"+temp_month;
	}
	
	var final_to_date=dt.getFullYear()+"-"+temp_month+"-"+temp_date+" "+dt.getHours()+":"+dt.getMinutes()+":"+dt.getSeconds();

	$('#to_datepicker_'+count).datetimepicker({
		value : final_to_date
	});
	
	
	
	
}*/
function getdatepicker()
{
	$('#from_datepicker').datetimepicker({
		format : 'Y-m-d H:i:s',
		dayOfWeekStart : 1,
		lang : 'en',
	});
	var d = new Date();
	d.setHours(d.getHours()-12);
	var temp_from_date=d.getDate();
	var temp_from_month=d.getMonth()+1;
	if(temp_from_date<10){
		temp_from_date="0"+temp_from_date;
	}
	
	if(temp_from_month<10){
		temp_from_month="0"+temp_from_month;
	}
	
	var temp_to_date=d.getFullYear()+"-"+temp_from_month+"-"+temp_from_date+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
	$('#from_datepicker').datetimepicker({
		value : temp_to_date
	});

	$('#to_datepicker').datetimepicker({
		format : 'Y-m-d H:i:s',
		dayOfWeekStart : 1,
		lang : 'en',
		beforeShow: function(input, inst) { 
	        inst.dpDiv.css({"z-index":100});
	    }
	});
	
	var dt = new Date();
	var temp_date=dt.getDate();
	var temp_month=dt.getMonth()+1;
	
	if(temp_date<10){
		temp_date="0"+temp_date;
	}
	
	if(temp_month<10){
	temp_month="0"+temp_month;
	}
	
	var final_to_date=dt.getFullYear()+"-"+temp_month+"-"+temp_date+" "+dt.getHours()+":"+dt.getMinutes()+":"+dt.getSeconds();

	$('#to_datepicker').datetimepicker({
		value : final_to_date
	});
	
	$('#from_datepicker').draggable();
	$('#to_datepicker').draggable();
	
}

function refreshpage(){
	getdashboards();
	getapplications();
}

function PopupFilter(buttoncount){
	
	$("#filterform").dialog({
	    modal: true,
	    autoOpen: false,
	    title: "Filter Report",
	   width: 300,
	    height: 275,
	    position: [485, 185],
	    buttons: {
			 "Update": function() {
				 //Code when update is clicked will come here
				 UpdateChart("draggable_"+buttoncount);
				 $( this ).dialog( "close" );
			 },
			Cancel: function() {
				$( this ).dialog( "close" );
	 		}
		 }
	    
	});
	
	 $('#filterform').dialog('open');

	getdatepicker();
	
}


function RemoveChart(removecount,name){
    var dashName=$('#dashboards :selected').val();
  
   
	 /*********************Following ajax call is to check the number of reports left in the dashboard******************/
	  var reportcount;
	    $.ajax({
			type : "POST",
			url : "drawDashboard.html",
			data :"dash_name="+encodeURIComponent(dashName),
			success : function(response) {
				reportcount=response.length;
				 if(reportcount==1){
				    	refreshpage();
				    }
			}
	    });
	  /**********************************************************************************************************************/  
	    
	
	
	
	$("#confirmDelete_reports").show();
	$( "#confirmDelete_reports" ).dialog({
		 resizable: false,
		 autoOpen: false,
		 height:200,
		 width: 340,
		 position: [485, 185],
		 modal: true,
		 buttons: {
			 "Yes": function() {
					var dashName=$('#dashboards :selected').val();
				 $.ajax({
					 type : "GET",
					 url : "removeChartfromDash.html",
						data:"chart_name=" +encodeURIComponent(name)+"&dashboardName=" +encodeURIComponent(dashName),
					 success : function(response)
					 {
						  $( "#draggable_"+removecount ).remove();
						
					 },
					 	cache: false
				 	}); 
				 		$( this ).dialog( "close" );
			 },
				No: function() {
			
	 							$( this ).dialog( "close" );
	 						}
		 }
		 
	});
	
	$("#confirmDelete_reports").dialog("open");
	$(".ui-dialog-title").text('Delete');
	$(".ui-dialog-titlebar-close").hide();
	$("#confirmDelete_reports").text("Delete this Report permanently?");
	
	
    
}



function deleteDashboard(){
	if($("#dashboards option:selected").val() === "Select Dashboard" )
	{
		showAlert('Please select at least one Dashboard!');		
	}
	else
	{
		$("#confirmDelete").show();
		$( "#confirmDelete" ).dialog({
			 resizable: false,
			 autoOpen: false,
			 height:200,
			 width: 340,
			 position: [485, 185],
			 modal: true,
			 buttons: {
				 "Yes": function() {
						var dashName=$('#dashboards :selected').val();
					 $.ajax({
						 type : "GET",
						 url : "deleteDashboards.html",
						 data : "&select_dashboards=" +encodeURIComponent(dashName), 
						 success : function(response)
						 {
							 $('#dashboard_element_container').children().slice(1).remove();
							 window.location.href = "list_dashboard.html";
								getdashboards();
						 },
						 	cache: false
					 	}); 
		
				 },
					No: function() {
				
		 							$( this ).dialog( "close" );
		 						}
			 }
			 
		});
		
		$("#confirmDelete").dialog("open");
		$(".ui-dialog-title").text('Delete');
		$(".ui-dialog-titlebar-close").hide();
		$("#confirmDelete").text("Delete this Dashboard permanently?");
		
	}
	
	
}

function getapplications()
{
	$.ajax({
		type : "GET",
		url : "getApplications.html",
		success : function(response) {
			$('#application').empty();
			$("#application").append('<option value="" selected disabled>Select Application</option>');
			for ( var i = 0; i < response.length; i++) {
				 $("#application").append('<option value="'+response[i].appId+'">'+response[i].appName+'</option>');
			}
		}
	});
}

function getdashboards()
{
	$.ajax({
		type : "GET",
		url : "getDashboardnames.html",

		success : function(response) {
			$('#dashboards').empty();
			 $("#dashboards").append('<option value="Select Dashboard" disabled selected>Select Dashboard</option>');

			for ( var i = 0; i < response.length; i++) {
				$("#dashboards").append(
						$('<option>').text(response[i].dashboard_name)).append(
						$('</option>'));
			}
			$('#dashboards').change(
					function() {
						$("div").remove(".charts");
						var selected_field = $('#dashboards').find(
						":selected").text();
						drawdashboard(selected_field);
					});
		}
	});
}

/***************************************************Export Functionality code starts here*************************************/
function exportDashBoardToPDF(){
	savePDF();
}

function savePDF(){


    try {
                  var dashboard_name = $('#dashboards').find(
                  ":selected").text();
                  html2canvas($("#dashboard_element_container"),{
                        onrendered: function(canvas) {
                               theCanvas = canvas;
                               document.body.appendChild(canvas);

                    // Convert and download as image 
                   
                        canvas.getContext('2d');
                        console.log(canvas);
                        
                        var resizeCanvas = document.createElement("canvas");
                        resizeCanvas.height = canvas.height;
                        resizeCanvas.width = canvas.width;
                        
                        var resizeCtx = resizeCanvas.getContext('2d');
                        /*resizeCtx.drawImage(canvas, 0, 0);
                        resample_hermite(resizeCanvas, resizeCanvas.width, resizeCanvas.height, 1082.9, 1178.8);*/
                        var width  = (canvas.width * 25.4) / 300; // DPI
                        var height = (canvas.height * 25.4) / 300; // DPI
                
                         /*resizeCanvas.width = width;
                        resizeCanvas.height = height;*/
                        
                        
                        var imgData = canvas.toDataURL("image/png", 1.0);
                        //var pdf = new jsPDF('p', 'mm', [595.28, 841.89]);
                        var pdf = new jsPDF('p', 'mm', [595.28/1.1, 841.89/2.6]);
                        //pdf.addImage(imgData, 'JPEG', 5, 5);
                        pdf.addImage(imgData, 'PNG', 15, 40, width*3, height*3);
                        //var namefile = prompt("insert name of file");
                        var pdffilename = dashboard_name+'_'+$.now()+'.pdf';
                        pdf.save(pdffilename);
                  }
           });

    } catch(e) {
           showAlert("Error description: " + e.message);
    }
}

/*function savePDF(){


	try {
			var dashboard_name = $('#dashboards').find(
			":selected").text();
			html2canvas($("#dashboard_element_container"),{
				onrendered: function(canvas) {
					theCanvas = canvas;
					document.body.appendChild(canvas);
				canvas.getContext('2d');
				console.log(canvas);
				var resizeCanvas = document.createElement("canvas");
				resizeCanvas.height = canvas.height;
				resizeCanvas.width = canvas.width;
				var resizeCtx = resizeCanvas.getContext('2d');
				var width  = (canvas.width * 25.4) / 300; // DPI
				var height = (canvas.height * 25.4) / 300; // DPI
				var imgData = canvas.toDataURL("image/png", 1.0);
				var pdf = new jsPDF('p', 'mm', [595.28/1.1, 841.89/2.6]);
				pdf.addImage(imgData, 'PNG', 15, 40, width*3, height*3);
				//var namefile = prompt("insert name of file");
				var currentTime = $.format.date(new Date($.now()), "yyyy-MM-dd HH:mm:ss");
				var pdffilename = dashboard_name+'_'+currentTime+'.pdf';
				pdf.save(pdffilename);
			}
		});

	} catch(e) {
		showAlert("Error description: " + e.message);
	}
}*/

function exportDashBoardToPPT(){
	savePPT();
}

function savePPT(){
	showAlert('Save PPT Button Clicked....!!');
	var str="";
	var a = document.createElement('a');
	str = $("#dashboard_element_container").innerHtml();
	window.open('data:application/vnd.ms-powerpoint,' + encodeURIComponent(str));
	
	/* Response.ContentType = "application/vnd.ms-powerpoint";
	 Response.AddHeader("content-disposition", "attachment; filename=sample.ppt");  
	 Response.Write(elements);
	 Response.End();*/
}
/*****************************************Export Functionality Ends Here*********************************************/
function drawdashboard(selected_field)
{
	$.ajax({
		type : "POST",
		url : "drawDashboard.html",
		data :"dash_name="+encodeURIComponent(selected_field),
		success : function(response) {
			for(var i = 0; i < response.length; i++)
			{
				
				var dashboard_element= response[i].dashboard_data;
				var dataarray=response[i].chart_data;
				if(i==0)
					{
					var last_div=$('#chart_container').attr('id');
					drawdiv(dashboard_element,dataarray,last_div);
					}
				else{
					var last_div=$('#dashboard_element_container').children().last().attr('id');
					drawdiv(dashboard_element,dataarray,last_div);
				}
			}
		}
	});
}

 function UpdateChart(targetdivid){
	   
	//var divname=updatedivname;
	var dashname = $('#dashboards :selected').val();
	var divId=targetdivid;
	var helper=divId.split("_");
	var count=helper[1];
 
	var from_date = $("#from_datepicker").val();
	var to_date = $("#to_datepicker").val();
	console.log(divId+dashname+from_date+to_date); 
	var temp_start_datetime = from_date.split(" ");
	var final_from_date = temp_start_datetime[0]+ "T"+ temp_start_datetime[1]+ "Z";
	var temp_end_datetime = to_date.split(" ");
	var final_to_date = temp_end_datetime[0]+ "T"+ temp_end_datetime[1]+ "Z";
	
	$.ajax({
		type : "POST",
		url : "updateReportonDashboard.html",
		data : "dashname=" + encodeURIComponent(dashname)+"&divId="+divId+"&from_date="+final_from_date+"&to_date="+final_to_date,
		success : function(response) {
			var count=response[0].div_count;
			var name=response[0].chart_name;
			console.log(response);
			console.log(response[0].div_count);
			if(response[0].chart_type=='pie')
			{
				drawPie(count,response,name);
			}
			else if(response[0].chart_type=='column')
			{
				drawColumn(count,response,name);
			}
			else if(response[0].chart_type=='time')
			{
				drawTime(count,response,name);
			}
			else if(response[0].chart_type=='x-y')
			{
				getcount(count,response,name);
			}
			else if((response[0].chart_type=='grid'))
			{
				updateGridChart($("#table_"+count), response);
			}
			
		}
	});
	
	 $('#mask , .login-popup').fadeOut(300 , function() {
		    $('#mask').remove();  
		}); 
 }
 
 function updateGridChart(gridName, dataArray) {
	 gridName.jqGrid('clearGridData');
	 gridName.jqGrid('setGridParam',{data:dataArray[0].gridData, datatype:'local'});
	 gridName.trigger('reloadGrid');
}
 
function drawdiv(dashboard_element,dataarray,last_element)
{
	
	var res = (dashboard_element.div_name).split("_");
	var count=res[1];
	var name=dashboard_element.chart_name;
	var updatedivname=dashboard_element.div_name;
	
	if(dashboard_element.chart_type=='grid'){
		$( "<div id='"+dashboard_element.div_name+"' style='border:1px solid #000;'><button type='button' class='btn btn-primary '  style='float:right;' id='removeclick_"+count+"'   onclick='RemoveChart("+'"'+count+'"'+","+'"'+name+'"'+")'> <span class='glyphicon glyphicon-remove'></span></button><button type='button' class='btn btn-primary ' id='filterclick_"+count+"' style='float:right;'  onclick='PopupFilter("+count+")'> <span class='glyphicon glyphicon-filter'></span>Filter</button><div id='container_"+count+"'  style='margin-top: 50px;'></div><table id='table_"+count+"'></table><div id='gridPager_"+count+"'></div></div>" ).insertAfter( "#"+last_element);
		// getdatepicker(count);
	}else{
		
	$( "<div id='"+dashboard_element.div_name+"' style='border:1px solid #000;' ><button type='button' class='btn btn-primary ' style='float:right;' id='removeclick_"+count+"'   onclick='RemoveChart("+'"'+count+'"'+","+'"'+name+'"'+")'> <span class='glyphicon glyphicon-remove'></span></button><button type='button' class='btn btn-primary ' id='filterclick_"+count+"'  style='float:right;' onclick='PopupFilter("+count+")'> <span class='glyphicon glyphicon-filter'></span>Filter</button><div id='container_"+count+"'  style='margin-top: 50px;'></div></div>" ).insertAfter( "#"+last_element);
	 //    getdatepicker(count);
	
	}
	/*  Comment THREE
	var from_date = $("#from_datepicker_"+count).val();
	var to_date = $("#to_datepicker_"+count).val();
	var temp_start_datetime = from_date.split(" ");
	var final_from_date = temp_start_datetime[0]+ "T"+ temp_start_datetime[1]+ "Z";
	var temp_end_datetime = to_date.split(" ");
	var final_to_date = temp_end_datetime[0]+ "T"+ temp_end_datetime[1]+ "Z";
	
	*/
	
	$('#'+dashboard_element.div_name).height(dashboard_element.height);
	$('#'+dashboard_element.div_name).width(dashboard_element.width);
	$('#'+dashboard_element.div_name).attr('class','charts');	
	$('#'+dashboard_element.div_name).css({position:'absolute'});
	if(dashboard_element.top!=0)
		{
		$('#'+dashboard_element.div_name).css({top: dashboard_element.top});
		}
    if(dashboard_element.left!=0)
    	{
    	$('#'+dashboard_element.div_name).css({left: dashboard_element.left});
    	}
	
	if(dashboard_element.chart_type=='pie')
		{
		
		drawPie(count,dataarray,name);
		}
	
	else if(dashboard_element.chart_type=='column')
		{
		
		drawColumn(count,dataarray,name);
		}
	else if(dashboard_element.chart_type=='time')
	{
		drawTime(count,dataarray,name);
	}
	else if(dashboard_element.chart_type=='x-y')
	{
		getcount(count,dataarray,name);
	}
	else if(dashboard_element.chart_type=='grid')
	{
		loadGridChart(count,dataarray,name);
	}
}

function drawPie(count,dataarray,name)
{
/*	var chart;
    $('#draggable_'+count).resizable({
        // On resize, set the chart size to that of the
        // resizer minus padding. If your chart has a lot of data or other
        // content, the redrawing might be slow. In that case, we recommend
        // that you use the 'stop' event instead of 'resize'.
    	
    	resize: function () {
            chart.setSize(
                this.offsetWidth - 20,
                this.offsetHeight - 20,
                false
            );
        }
    });*/
    var piedata = new Array();
	var Value=[];
	var Count=[];
	var facetVal=[];
	for ( var i = 0; i < dataarray.length; i++) {
		Count[dataarray[i].facetValue]=dataarray[i].count;
		facetVal.push(dataarray[i].facetValue);
		piedata[i] = [ dataarray[i].lable, dataarray[i].count ];
		Value[i]=dataarray[i].facetValue;
	}
	chart = c3.generate({
		bindto: '#container_'+count ,
		data: {
            json: [ Count ],
            keys: {
                value: facetVal
            },
            type:'pie'
        },
        legend: {
            show: false
        },
        pie: {
            label: {
              format: function(value, ratio, id) {
                return id;
              }
            }
          },
          tooltip: {
            format: {
            },
            contents: function (d, defaultTitleFormat, defaultValueFormat, color) {
                var $$ = this, config = $$.config,
                    titleFormat = config.tooltip_format_title || defaultTitleFormat,
                    nameFormat = config.tooltip_format_name || function (name) { return name; },
                    valueFormat = config.tooltip_format_value || defaultValueFormat,
                    text, i, title, value, name, bgcolor;
                for (i = 0; i < d.length; i++) {
                    if (! (d[i] && (d[i].value || d[i].value === 0))) { continue; }

                    if (! text) {
                        title = titleFormat ? titleFormat(d[i].x) : d[i].x;
                        text = "<table style='border: 1px solid black;' class='" + $$.CLASS.tooltip + "'>" + (title || title === 0 ? "<tr><th colspan='2'>" + title + "</th></tr>" : "");
                    }

                    name = nameFormat(d[i].name);
                    value = valueFormat(d[i].value, d[i].ratio, d[i].id, d[i].index);
                    bgcolor = $$.levelColor ? $$.levelColor(d[i].value) : color(d[i].id);

                    text += "<tr style='border: 1px solid black;' class='" + $$.CLASS.tooltipName + "-" + d[i].id + "'>";
                    text += "<td style='border: 1px solid black;' class='name'><span style='background-color:" + bgcolor + "'></span>Facet Value=" + name + "</td>";
                    text += "<td style='border: 1px solid black;' class='value'>%value=" + value + "</td>";
                    text += "</tr>";
                }
                return text + "</table>";
            }
      }
    });
 
	
	
}

function drawColumn(count,dataarray,name)
{
	/*var chart;
    $('#draggable_'+count).resizable({
        // On resize, set the chart size to that of the
        // resizer minus padding. If your chart has a lot of data or other
        // content, the redrawing might be slow. In that case, we recommend
        // that you use the 'stop' event instead of 'resize'.
    	
    	resize: function () {
            chart.setSize(
                this.offsetWidth - 20,
                this.offsetHeight - 20,
                false
            );
        }
    });*/
    var columnData=[];
	var y=[];
	var x=[];
var field=[];
	
	for(var j=0;j<dataarray.length;j++){
		if(field.indexOf(dataarray[j].lable)<0){
			field.push(dataarray[j].lable);
		}
		
     }
	for(var i=0;i<field.length;i++){
		y.push([field[i]]);
		x.push(['x'+field[i]]);
		for(var j=0;j<dataarray.length;j++){
			if(field[i]==dataarray[j].lable){
				y[i].push(dataarray[j].count);
                var temp=Math.round(dataarray[j].facetValue);
				x[i].push(temp);
			}
	}
		
	}
	var xs = {};
	for(var i=0;i<x.length;i++){
		columnData.push(x[i]);
		columnData.push(y[i]);
		xs[y[i][0]] = x[i][0];
	}
	
	var chart = c3.generate({
		bindto: '#container_'+count ,
		data: {
		xs:xs,
		
	      columns:columnData
	    },
	    axis: {
		      x: {
		        tick: {count: dataarray.length,
		          fit: true,
		          format: function(x) {
		             return Math.round(x);
		          }
		        }
		      }
		    },
		    subchart: {
		      show: true
		    }, 
		    zoom: {
		      enabled: true
		    }
	}); 
    //bar chart in c3
   /* var xcolumndata = new Array();
	var ycolumndata = new Array();
	var TwoDArray=new Array();
	xcolumndata.push('Status');
	ycolumndata.push('Values');
	var label = new Array();			
	for ( var i = 0; i < dataarray.length; i++) {
		xcolumndata.push(dataarray[i].facetValue);
		ycolumndata.push(dataarray[i].count);
		label.push(dataarray[i].lable);
		TwoDArray.push([dataarray[i].facetValue,dataarray[i].count]);
	}
	var chart = c3.generate({
	    bindto: '#container_'+count,
	    data: {
	    	 x: 'Status',
	      columns: [
	        
          xcolumndata,
          ycolumndata
	      ],
	      type: 'bar'
	      
	    },
	    bar: {
	      width: { ratio: 0.5 }
	    },
	    axis: {
	      x: {
	        tick: {count: dataarray.length,
	          fit: true
	        }
	      }
	    },
	    subchart: {
	      show: true
	    }, 
	    zoom: {
	      enabled: true
	    },
	    tooltip: {
	    	format: {
	           
	        },
	        contents: function (d, defaultTitleFormat, defaultValueFormat, color) {
	            var $$ = this, config = $$.config,
	                titleFormat = config.tooltip_format_title || defaultTitleFormat,
	                nameFormat = config.tooltip_format_name || function (name) { return name; },
	                valueFormat = config.tooltip_format_value || defaultValueFormat,
	                text, i, title, value, name, bgcolor;
	                
	            for (i = 0; i < d.length; i++) {
	                if (! (d[i] && (d[i].value || d[i].value === 0))) { continue; }

	                if (! text) {
	                    title = titleFormat ? titleFormat(d[i].x) : d[i].x;
	                    text = "<table class='" + $$.CLASS.tooltip + "'>" + (title || title === 0 ? "<tr><th colspan='2'>" + title + "</th></tr>" : "");
	                }
	                var index;
	                name = nameFormat(d[i].name);
	                value = valueFormat(d[i].value, d[i].ratio, d[i].id, d[i].index);
	                bgcolor = $$.levelColor ? $$.levelColor(d[i].value) : color(d[i].id);
                    for(var k = 0; k < response.length; k++){
	                	
	                    if(TwoDArray[k][0].toString()==title.toString() && TwoDArray[k][1].toString()==value.toString()){
	                     index=k;
	                        break;
	                    }
	                }
	                text += "<tr class='" + $$.CLASS.tooltipName + "-" + d[i].id + "'>";
	                text += "<td class='name'><span style='background-color:" + bgcolor + "'></span>" + name + "</td>";
	                text += "<td class='value'>" + value + "</td>";
	                
	                text += "</tr>";
	                text += "<tr class='" + 'Type' + "-" + d[i].id + "'>";
	                text += "<td class='name'><span style='background-color:" + bgcolor + "'></span>" + 'Type' + "</td>";
	                text += "<td class='Type'>" + label[index] + "</td>";
	                
	                text += "</tr>";
	            }
	            return text + "</table>";
	        }
	    }
	});*/

	
	
}
// This method will be called when grid chart is initially loaded on the page.
function loadGridChart(count,dataarray,name){
	var query="*:*";
	var col="";
	var fieldvalue=[];
	for ( var i = 0; i < dataarray.length; i++) {
		col =  dataarray[i].col_name;
		fieldvalue = dataarray[i].gridcols;
	}
	var queryParams = {
             			"collection" : col,
             			"query" : query
           
					  };
	var gridCols=[];
	var modelFields=[];
	gridCols=fieldvalue;
	//showAlert(fieldvalue.length);
	for(var i=0;i<fieldvalue.length;i++){
		var modelFieldsArr={
		  		  name : fieldvalue[i],
		  		  index : fieldvalue[i],	
		  		  width : 200
		  		
		     };
		modelFields.push(modelFieldsArr);
		
	}
	
$("#table_"+count).jqGrid(                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        {
		datatype : "json",
	    url:'getBasicQueryforReportsNoFilter.html',
	    ajaxGridOptions : {
   		   					contentType : 'application/json'
	    				  },
	    postData:JSON.stringify(queryParams),
	    mtype: 'POST',
	    colNames:gridCols,
	    colModel : modelFields,
	    viewrecords : true,
	    gridview : true,
	    //width:600,
	    //height:350,
	    forceFit:true,
	    autowidth:true,
		shrinkToFit:false,
		loadonce: false,
		
		jsonReader: {
						repeatitems: false,
						root: function (obj) { 
												console.log(obj);
												return obj; 
											  },
		                page: function (obj) { return 1; },
		                total: function (obj) { return 1; },
		                records: function (obj) { return obj.length; }                  
					},
				   
	    				  pager : '#gridPager_'+count,
	    					rowNum : 10,
	    					rowList : [ 5, 10, 15 ],
	    					viewrecords : true,
	    					gridview : true,
	    					loadonce: true,
	    					//width : 600,
	    					//height: 500
	}); 

}

function getcount(count,dataarray,name)
{
	

			var xcolumndata = new Array();
			var ycolumndata = new Array();
			xcolumndata[0]='x';
			ycolumndata[0]='y';
			for ( var i = 0; i < dataarray.length; i++) {
				xcolumndata.push(dataarray[i].lable);
				ycolumndata.push(dataarray[i].count);
			}
			
			var chart = c3.generate({
				bindto: '#container_'+count ,
				
			    data: {
			    	
			    	 x: 'x',
			        columns: [
                    xcolumndata,
                    ycolumndata
			        ],
			        type: 'bar'
			    },
			    legend: {
		            show: false
		        }, 
				    subchart: {
				        show: true
				    },
				    zoom: {
				        enabled: true
				    },
				    axis : {
				        x : {
				        	label:name
				            
				        },
				        y : {
				        	label:'Count Value'
				        }
				    }
			
	});
}



function drawTime(count,dataarray,name)
{
	var chart;
    $('#draggable_'+count).resizable({
        // On resize, set the chart size to that of the
        // resizer minus padding. If your chart has a lot of data or other
        // content, the redrawing might be slow. In that case, we recommend
        // that you use the 'stop' event instead of 'resize'.
    	
    	resize: function () {
            chart.setSize(
                this.offsetWidth - 20,
                this.offsetHeight - 20,
                false
            );
        }
    });
    var timedata = new Array();
	var dataX=[];
	var dataY=[];
	dataX.push("Time");
	dataY.push("Count");
	for ( var i = 1; i < dataarray.length; i++) {
		timedata[i] = [ dataarray[i].lable, dataarray[i].count ];
		dataX[i]=[dataarray[i].lable];
		dataY[i]=[dataarray[i].count];
	}
	
	var chart = c3.generate({
		bindto: '#container_'+count ,
		data: {
		 xs:{
			
			'Time':'Count'
		},
		
	      columns: [dataX,dataY]
									
	    }
	});
	
}




