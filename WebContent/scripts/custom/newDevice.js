/**
 * 
 */

$(document)
		.ready(
				function() {
					$("#preview_device_buttons").hide();
					document.getElementById('headingManageDevice').innerHTML= "Add New Device";
					$("#alert").hide();
					$("#confirmEdit").hide();
					$("#preview_ok").click(function(){
						window.location.href="manageDevice.html";
					});
					/*
					 * private String deviceId; private int version; private
					 * String hostName; private String ipAddress; private String
					 * ramSize; private String hddSize; private int noOfCore;
					 * private String systemType; private String processorType;
					 * private String domain; private boolean activeStatus;
					 * private String monitoringStatus; private String
					 * physicalNode; private String hostDownMsg; private String
					 * description; private boolean deletionStatus; private
					 * String createdBy; private Date creationDate; private
					 * String modifiedBy; private Date modifyDate; private
					 * String osInformation; private Set applicationMasters =
					 * new HashSet(0); private Set alerts = new HashSet(0);
					 * private Set thresholdMasters = new HashSet(0); private
					 * Set deviceSoftwareMappings = new HashSet(0);
					 */
					var action = getQueryVariable("action");
					var id = getQueryVariable("deviceid");
					if (action == "edit") {
						$("#preview_device_buttons").hide();
						document.getElementById('headingManageDevice').innerHTML= "Update Device";
						$.ajax({
									type : "POST",
									url : "editDevice.html",
									data : "deviceid=" + id,
									success : function(response) {
										
										$('#deivce_hostname').attr('readonly',false);
										$('#deivce_ipadress').attr('readonly',false);
									//	$('#deivce_ramSize').attr('readonly',false);
									//	$('#deivce_hardDiskSize').attr('readonly', false);
									//	$('#deivce_noOfCore').attr('readonly',false);
										$('#os_information').attr('disabled',false);
										$('#device_hostName_desc').attr('readonly', false);
									//	$('#device_hostName_downMessae').attr('readonly', false);
										$('#device_Applications').attr('disabled', false);
									//	$('#device_SoftwareInstalled').attr('disabled', false);
										$('#deivce_hostname').val(response.hostname);
									
										$('#deivce_ipadress').val(response.ip);
									//	$('#deivce_ramSize').val(response.ramSize);
									//	$('#deivce_hardDiskSize').val(response.hddSize);
									//	$('#deivce_noOfCore').val(response.noOfCore);
										$('#os_information').val(response.os);
										$('#device_hostName_desc').val(response.description);
										
									/*	
									*	$('#device_hostName_downMessae').val(response.hostDownMsg);
									*	if (response.activeStatus == true) {
									*		$('#device_host_status').attr(
									*				'checked', true);
									*	} else {
									*		$('#device_host_status').attr(
									*				'checked', false);
									*	}
									*	if (response.physicalNode == "VirtualNode") {
									*		$('input:radio[name=machineType]')[1].checked = true;
									*	}
									*	if (response.physicalNode == "PhysicalNode") {
									*		$('input:radio[name=machineType]')[0].checked = true;
									*	}
									*/	
									/*	var valArr = response.applicationIds, i = 0, size = valArr.length, $options = $('#device_Applications option');
										for (i; i < size; i++) {
											$options.filter(
													'[value="' + valArr[i]	+ '"]').prop(
													'selected', true);
										}*/

										/*var valArr1 = response.softwareIds, i = 0, size = valArr1.length, $options = $('#device_SoftwareInstalled option');
										// set selected in OS information combo
										setSelectedInList('os_information', response.systemType);
										for (i; i < size; i++) {
											$options.filter(
													'[value="' + valArr1[i]
															+ '"]').prop(
													'selected', true);
										}*/
									},
									cache : false
								});
					}

					if (action == "preview") {
						$("#add_device_buttons").hide();
						$("#preview_device_buttons").show();
						document.getElementById('headingManageDevice').innerHTML= "Preview Device";
						$.ajax({
									type : "POST",
									url : "previewDevice.html",
									data : "deviceId=" + id,
									success : function(response) {
										$('#deivce_hostname').val(response.hostName);
										$('#deivce_ipadress').val(response.ipAddress);
										//$('#deivce_ramSize').val(response.ramSize);
										//$('#deivce_hardDiskSize').val(response.hddSize);
										//$('#deivce_noOfCore').val(response.noOfCore);
										$('#os_information').val(response.systemType.toLowerCase());
										$('#device_hostName_desc').val(response.description);
										//$('#device_hostName_downMessae').val(response.hostDownMsg);
									/*	if (response.activeStatus == true) {
									*		$('#device_host_status').attr('checked', true);
									*	} else {
									*		$('#device_host_status').attr('checked', false);
									*	}
									*	if (response.physicalNode == "VirtualNode") {
									*		$('input:radio[name=machineType]')[1].checked = true;
									*	}
									*	if (response.physicalNode == "PhysicalNode") {
									*		$('input:radio[name=machineType]')[0].checked = true;
									*	}
									*/

										var valArr = response.applicationIds, i = 0, size = valArr.length, $options = $('#device_Applications option');
										for (i; i < size; i++) {
											$options.filter('[value="' + valArr[i] + '"]').prop('selected', true);
										}

										var valArr1 = response.softwareIds, i = 0, size = valArr1.length, $options = $('#device_SoftwareInstalled option');
										// set selected in OS information combo
										setSelectedInList('os_information', response.systemType);
										
										for (i; i < size; i++) {
											$options.filter('[value="' + valArr1[i] + '"]').prop('selected', true);
										}

										$('#deivce_hostname').attr('readonly',true);
										$('#deivce_ipadress').attr('readonly',true);
										//$('#deivce_ramSize').attr('readonly',true);
										//$('#deivce_hardDiskSize').attr('readonly', true);
										//$('#deivce_noOfCore').attr('readonly',true);
										$('#os_information').attr("disabled",true);
										$('#device_hostName_desc').attr('readonly', true);
										//$('#device_hostName_downMessae').attr('readonly', true);
										$('#device_Applications').attr('disabled', true);
										//$('#device_SoftwareInstalled').attr('disabled', true);
									},
									cache : false
								});
					}
					$("#add_device_submit").click(function() {
						
						addDeviceSubmit(id);
					});

					$("#add_device_cancel").click(function() {
						window.location.href = "manageDevice.html";
					});
				});

var hostName, ipAddress, ramSize, hddSize, noOfCore, systemType, activeStatus, isPhysicalNode, hostDownMsg, description, device_application, device_software, processorType;
// ,domain ,deletionStatus
// ,createdBy,creationDate,modifiedBy,modifyDate,alerts,deviceSoftwareMappings
// how to map software and applications

function addDeviceSubmit(id) {
	if (id == false) {
		id = "null";
	}
/*	
*	if (document.getElementById('PhysicalNode').checked == true) {
*		physicalNode = $('#PhysicalNode').val();
*	} else
*		physicalNode = $('#VirtualNode').val();
*
*/	hostName = $('#deivce_hostname').val();
	ipAddress = $('#deivce_ipadress').val();
	//ramSize = $('#deivce_ramSize').val();
	//hddSize = $('#deivce_hardDiskSize').val();
	//noOfCore = $('#deivce_noOfCore').val();
	systemType = $('#os_information').val();
	//activeStatus = document.getElementById('device_host_status').checked;
	//hostDownMsg = $('#device_hostName_downMessae').val();
	description = $('#device_hostName_desc').val();
	device_application = $('#device_Applications').val();
	//device_software = $('#device_SoftwareInstalled').val();
	$.ajax({
		type : "POST",
		url : "addNewDeviceOnSubmit.html",
		data : 'hostname=' + hostName + '&ip=' + ipAddress /*+ '&ramSize='
				+ ramSize + '&hddSize=' + hddSize + '&noOfCore=' + noOfCore*/
				+ '&os=' + systemType /*+ '&activeStatus=' + activeStatus
				+ '&physicalNode=' + physicalNode + '&hostDownMsg='
				+ hostDownMsg*/ + '&description=' + description
				+ '&appname=' + device_application
				/*+ '&device_software=' + device_software */+ '&deviceid=' + id,

		success : function(response) {
			if (response.indexOf("empty") > -1 || response.indexOf("HostName") > -1
					|| response.indexOf("proper") > -1
					|| response.indexOf("Integer") > -1
					|| response.indexOf("atleast")>-1) {
				alert(response);
			} else {
				$( "#confirmEdit" ).show();
				$( "#confirmEdit" ).dialog({ buttons:{"Ok":function() {
				window.location.href = "manageDevice.html";
				}} });
			}
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert(textStatus);
			alert(errorThrown);
			window.location.href = "addNewDevice.html";
		},
		cache : false
	});

}
function getQueryVariable(variable) {
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split("=");
		if (pair[0] == variable) {
			return pair[1];
		}
	}
	return (false);
}


function setSelectedInList(componentId, selectedValue)
{
	$options = $('#' + componentId + ' option');
	$options.filter('[value="' + selectedValue+ '"]').prop('selected', true);
 }
