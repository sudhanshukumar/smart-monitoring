package com.lnt.smartit.solr.daoImpl;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrServer;
import org.apache.solr.client.solrj.impl.HttpClientUtil;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer.RemoteSolrException;
import org.apache.solr.client.solrj.impl.Krb5HttpClientConfigurer;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.RangeFacet;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lnt.java.main.SolrConnectionManager;
import com.lnt.java.main.SolrOperations;
import com.lnt.smartit.services.ChartService;
import com.lnt.smartit.solr.dao.QueryDao;
import com.lnt.smartit.solr.pojo.Charting;
import com.lnt.smartit.solr.pojo.Core;
import com.lnt.smartit.solr.pojo.Correlation;
import com.lnt.smartit.solr.pojo.Field;
import com.lnt.smartit.solr.pojo.Query;


@Service
public class QueryDaoImpl implements QueryDao {
	
	@Autowired
	ChartService chartService;
	
	private static Logger logger = Logger.getLogger(QueryDaoImpl.class);

	SolrConnectionManager objSolrConnectionManager= new SolrConnectionManager();
	SolrOperations objSolrOperations = new SolrOperations();
	public void setSystemProperties(){
		InputStream paramPropertiesStream = null;
		try{
			logger.debug("------Setting system properties-----");
			HttpClientUtil.setConfigurer(new Krb5HttpClientConfigurer()); 
			Properties props = new Properties();
			paramPropertiesStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("params.properties");
			props.load(paramPropertiesStream);
			logger.debug("Inside QueryDaoImpl :: javax.net.ssl.trustStore = "+props.getProperty("truststore") );
			logger.debug("Inside QueryDaoImpl :: javax.net.ssl.trustStorePassword = "+props.getProperty("trustStorePassword") );
			logger.debug("Inside QueryDaoImpl :: java.security.auth.login.config = "+props.getProperty("jaas"));
			logger.debug("Inside QueryDaoImpl :: java.security.krb5.conf = "+props.getProperty("krb5"));
			System.setProperty("javax.net.ssl.trustStore", props.getProperty("truststore"));
		    System.setProperty("javax.net.ssl.trustStorePassword", props.getProperty("trustStorePassword"));
		    System.setProperty("java.security.auth.login.config", props.getProperty("jaas"));
		    System.setProperty("java.security.krb5.conf", props.getProperty("krb5"));
		    
		}
		catch(FileNotFoundException e){
			logger.error("QueryDaoImpl :: Error Found in setSystemProperties methold : "+e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			logger.error("QueryDaoImpl :: Error Found in setSystemProperties methold : "+e.getMessage());
			e.printStackTrace();
		}
		catch (Exception e) {
			logger.error("QueryDaoImpl :: Error Found in setSystemProperties methold : "+e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				if(paramPropertiesStream != null)
					paramPropertiesStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public Query getBasicQuery(Query uquery) throws Exception {
		String url = uquery.getUrl() + uquery.getCollection();
		logger.debug("QueryDaoImpl :: getBasicQuery :: URL = "+ url);
		String sort = uquery.getSort_order();
		String sort_field=uquery.getSort_field();
		String selectes_fields = "";
		String[] get_selected_fields=uquery.getSelected_fields();
		Query result_data = new Query();
		SolrQuery query = new SolrQuery();
		query.setQuery(uquery.getQuery());
		if (uquery.getSelected_fields() != null	&& uquery.getSelected_fields().length > 0) {
			selectes_fields = Arrays.toString(uquery.getSelected_fields());
			selectes_fields = selectes_fields.substring(1,selectes_fields.length() - 1);
			logger.debug("QueryDaoImpl :: getBasicQuery :: fields = "+ selectes_fields);
			query.setFields(selectes_fields);
		}
		query.setStart(uquery.getStart());
		query.setRows(uquery.getRows());
		if (!uquery.getFilter().equalsIgnoreCase("default")) {
			logger.debug("QueryDaoImpl :: getBasicQuery :: filter = "+ uquery.getFilter());
			query.addFilterQuery(uquery.getFilter());
		}
		if(!sort_field.equalsIgnoreCase("null")) {
			logger.debug("QueryDaoImpl :: getBasicQuery :: sortfield = "+ sort_field + " sortorder = "+sort);
			if (sort.equalsIgnoreCase("Descending")) {
				query.addSort(sort_field, SolrQuery.ORDER.desc);
			} else {
				query.addSort(sort_field, SolrQuery.ORDER.asc);
			}
		}
		setSystemProperties();
		SolrDocumentList results=objSolrOperations.executeQuery(uquery.getUrl(), uquery.getCollection(), query);
		long numFound = results.getNumFound();
		List<String> docs = new ArrayList<String>();
		if (uquery.getSelected_fields() != null	&& uquery.getSelected_fields().length > 0) {
			for (SolrDocument doc : results) {
				StringBuffer result_string = new StringBuffer("");
				for (String field : get_selected_fields) {
					logger.debug("QueryDaoImpl :: getBasicQuery :: Field = " + field + " :: value = "+doc.get(field));
					String temp_field_result = "";
					if(doc.get(field) == null || doc.get(field).equals("null")){
						temp_field_result = "#";
					}
					else if(doc.get(field) instanceof Long){
						temp_field_result = ((Long) doc.get(field)).toString();
					} else if(doc.get(field) instanceof Double){
						temp_field_result = ((Double) doc.get(field)).toString();
					} else if(doc.get(field) instanceof Date){
						SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						f.setTimeZone(TimeZone.getTimeZone("GMT"));
						String new_date = f.format((Date) doc.get(field));
						temp_field_result = new_date;
					}else if(doc.get(field) instanceof ArrayList){
						temp_field_result =  doc.get(field).toString();
					} else {
						temp_field_result = (String) doc.get(field);
					}
					result_string.append(field);
					result_string.append(":");
					result_string.append(temp_field_result);
					result_string.append("  ");
				}
				docs.add(result_string.toString());
			}
		}
		else
		{
			for (SolrDocument doc : results) {
				docs.add(doc.toString());
			}
		}
		result_data.setResults(docs);
		result_data.setTotal_hits(numFound);
		logger.debug("Result object-"+result_data);
		return result_data;
	}
	
	
	@Override
	public  List<Object> getBasicQueryForLogAnalysis(Query uquery) throws Exception {
		
		String solrURL = uquery.getUrl() + "/solr";
	    CloudSolrServer solrSvr = new CloudSolrServer(solrURL);
	    solrSvr.setDefaultCollection(uquery.getCollection());
		String sort = uquery.getSort_order();
		String sort_field=uquery.getSort_field();
		String selectes_fields = "";
		SolrQuery query = new SolrQuery();
		query.setQuery(uquery.getQuery());
		if (uquery.getSelected_fields() != null	&& uquery.getSelected_fields().length > 0) {
			selectes_fields = Arrays.toString(uquery.getSelected_fields());
			selectes_fields = selectes_fields.substring(1,selectes_fields.length() - 1);
			logger.debug("QueryDaoImpl :: getBasicQuery :: fields = "+ selectes_fields);
			query.setFields(selectes_fields);
		}
		query.setStart(uquery.getStart());
		//query.setRows(uquery.getRows());
		if (!uquery.getFilter().equalsIgnoreCase("default")) {
			logger.debug("QueryDaoImpl :: getBasicQuery :: filter = "+ uquery.getFilter());
			query.addFilterQuery(uquery.getFilter());
		}
		if(!sort_field.equalsIgnoreCase("null") && !sort_field.equalsIgnoreCase("")) {
			logger.debug("QueryDaoImpl :: getBasicQuery :: sortfield = "+ sort_field + " sortorder = "+sort);
			if (sort.equalsIgnoreCase("Descending")) {
				query.addSort(sort_field, SolrQuery.ORDER.desc);
			} else {
				query.addSort(sort_field, SolrQuery.ORDER.asc);
			}
		}
		QueryResponse res = solrSvr.query(query);
	    SolrDocumentList docs = res.getResults();
		
		
		System.out.println("docs"+docs);
		List<Object> records=new ArrayList<Object>();
		for (SolrDocument doc : docs) {
			
			Map<String, Object> docMap = doc.getFieldValueMap();
			Iterator itr = (docMap.keySet()).iterator();
			Map<String, Object> docReturnMap = new HashMap<String, Object>(); 
			while( itr.hasNext()) {
				String key = (String)itr.next();
				Object value = docMap.get(key);
				docReturnMap.put(key, value);
			}
			records.add(docReturnMap);
		}
		return records;
	}
	
	@Override
	public List<Object> getBasicQueryforReports(Query uquery) throws Exception{
	
		String solrURL = uquery.getUrl() + "/solr";
	    CloudSolrServer solrSvr = new CloudSolrServer(solrURL);
	    solrSvr.setDefaultCollection(uquery.getCollection());
    SolrQuery querySlr = new SolrQuery();
	querySlr.setQuery( "*:*" );
	String PrimFilter=chartService.getPrimaryFilter(uquery.getCollection());
	querySlr.addFilterQuery(PrimFilter+":["+ uquery.getStart_facet() + " TO "+ uquery.getEnd_facet() + "]");
	
    QueryResponse res = solrSvr.query(querySlr);
    SolrDocumentList docs = res.getResults();
	
	
	System.out.println("docs"+docs);
	List<Object> records=new ArrayList<Object>();
	for (SolrDocument doc : docs) {
		
		Map<String, Object> docMap = doc.getFieldValueMap();
		Iterator itr = (docMap.keySet()).iterator();
		Map<String, Object> docReturnMap = new HashMap<String, Object>(); 
		while( itr.hasNext()) {
			String key = (String)itr.next();
			Object value = docMap.get(key);
			docReturnMap.put(key, value);
		}
		records.add(docReturnMap);
	}
	//uquery.setResults(records);
	//records.add(getDefaultRecord());
	return records;
}
	
	private Map<String, String> getDefaultRecord()
	{
		Map<String, String> defaultRecordMap = new HashMap();
		
		defaultRecordMap.put("DUsePercent","");
		defaultRecordMap.put("TotalDiskSize","");
		defaultRecordMap.put("Host","");
		defaultRecordMap.put("TotalPhysicalMemory","");
		defaultRecordMap.put("AvailableVirtualMEmory","");
		defaultRecordMap.put("DDiskAvail","");
		defaultRecordMap.put("AvailablePhysicalInPercent","");
		defaultRecordMap.put("id","");
		defaultRecordMap.put("EUsePercent","");
		defaultRecordMap.put("CDiskSize","");
		defaultRecordMap.put("CUsePercent","");
		defaultRecordMap.put("Date","");
		defaultRecordMap.put("TotalDiskUsed","");
		defaultRecordMap.put("CDiskAvail","");
		defaultRecordMap.put("EDiskAvail","");
		defaultRecordMap.put("VirtualMemoryMaxSize","");
		defaultRecordMap.put("TotalAvailablepercent","");
		defaultRecordMap.put("_version_","");
		defaultRecordMap.put("CDiskUsed","");
		defaultRecordMap.put("DDiskUsed","");
		defaultRecordMap.put("IP","");
		defaultRecordMap.put("TotalUsePercent","");
		defaultRecordMap.put("EDiskUsed","");
		defaultRecordMap.put("MemoryUsage","");
		defaultRecordMap.put("UsedVirtualMemory","");
		defaultRecordMap.put("TotalDiskAvail","");
		defaultRecordMap.put("EDiskSize=","");
		defaultRecordMap.put("CpuUsageInPercent","");
		defaultRecordMap.put("UsedVirtualMemoryInPercent","");
		defaultRecordMap.put("DDiskSize","");
		defaultRecordMap.put("AvailablePhysicalMemory","");
		
		return defaultRecordMap;
	}
	@Override
	public List<Object> getBasicQueryforReportsNoFilter(Query uquery) throws Exception{
	
		String solrURL = uquery.getUrl() + "/solr";
	    CloudSolrServer solrSvr = new CloudSolrServer(solrURL);
	    solrSvr.setDefaultCollection(uquery.getCollection());
    SolrQuery querySlr = new SolrQuery();
	querySlr.setQuery( "*:*" );
    QueryResponse res = solrSvr.query(querySlr);
    SolrDocumentList docs = res.getResults();
	
	
	System.out.println("docs"+docs);
	List<Object> records=new ArrayList<Object>();
	for (SolrDocument doc : docs) {
		
		Map<String, Object> docMap = doc.getFieldValueMap();
		Iterator itr = (docMap.keySet()).iterator();
		Map<String, Object> docReturnMap = new HashMap<String, Object>(); 
		while( itr.hasNext()) {
			String key = (String)itr.next();
			Object value = docMap.get(key);
			docReturnMap.put(key, value);
		}
		records.add(docReturnMap);
	}
	//uquery.setResults(records);
	return records;
}
	
	@SuppressWarnings("deprecation")
	@Override
	public void getFacetingQuery(String uquery, String coll_url) throws Exception {
		logger.debug("Inside dao getFacetingQuery----------");
		String url = "http://172.25.37.66:8983/solr/collection4_shard1_replica1";
		SolrServer solrSvr = new HttpSolrServer(url);
		SolrQuery query = new SolrQuery();
		query.setQuery(uquery);
		query.setFacet(true);
		query.addFacetField("ip");
		QueryResponse res = solrSvr.query(query);
		List<FacetField> facets = res.getFacetFields();
		for (FacetField ff : facets) {
			List<Count> counts = ff.getValues();
			for (Count c : counts) {
				String facetLabel = c.getName();
				long facetCount = c.getCount();
			}
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void getJoinQuery(String uquery, String coll_url) throws Exception {
		logger.debug("Inside dao getJoinQuery----------");
		String url = "http://172.25.37.66:8983/solr/collection4_shard1_replica1";
		SolrServer solrSvr = new HttpSolrServer(url);
		SolrQuery query = new SolrQuery();
		query.setQuery(uquery);
		query.setFilterQueries("{!join fromIndex=Shiksha_cloud1_shard1_replica1 from=ip to=ip}status:200");
		QueryResponse res = solrSvr.query(query);
		SolrDocumentList results = res.getResults();
		long numFound = results.getNumFound();
		logger.debug("num hits: " + numFound);
		for (SolrDocument doc : results) {
			logger.debug(doc);
		}
	}

	@SuppressWarnings("static-access")
	@Override
	public List<Core> getCollections(String coll_url) throws Exception {
		logger.debug("Inside dao getCollections----------" + coll_url);
		String url = coll_url;
		List<Core> coreList = new ArrayList<Core>();
		// Request core list
		/*
		 * CoreAdminRequest request = new CoreAdminRequest();
		 * request.setAction(CoreAdminAction.STATUS); CoreAdminResponse cores =
		 * request.process(solrServer);
		 */
//		CloudSolrServer solrServer = objSolrConnectionManager.getConnection(coll_url);
		// List of the cores
		/*
		 * CollectionAdminRequest col = new CollectionAdminRequest();
		 * col.setAction(CollectionAction.LIST); CollectionAdminResponse resp =
		 * col.process(solrServer);
		 */
		/*
		 * List<String> collections = (List<String>) resp.getResponse().get(
		 * "collections");
		 */
//		List<String> collections = objSolrOperations.getCollections(solrServer);
		/*for (String colle : collections) {
			Core c = new Core();
			logger.debug("collection--" + colle);
			c.setCol_name(colle);
			coreList.add(c);
		}*/
		return coreList;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<Field> getSchema(Core c) throws Exception {
		setSystemProperties();
		List<Field> fields = new ArrayList<Field>();
		Map<String, String> SchemaMap = new HashMap<String, String>();
		
		
		 SchemaMap = objSolrOperations.getSchema(c.getUrl(), c.getCol_name());
		
	
		Iterator it = SchemaMap.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry) it.next();
			if(!(pair.getKey().toString().equals("_version_") || pair.getKey().toString().equals("id"))){
				Field f = new Field();
				String key = (String) pair.getKey();
				String value = (String) pair.getValue();
				logger.debug(pair.getKey() + " = " + pair.getValue());
				f.setName(key);
				f.setType(value);
				fields.add(f);
				it.remove(); // avoids a ConcurrentModificationException
			}
		}
		logger.debug("fields==" + fields);
		return fields;
	}

	@SuppressWarnings({ "deprecation", "unchecked", "rawtypes" })
	@Override
	public List<Charting> getTimeSeries(Query uquery) throws Exception {
		logger.debug("Inside dao getTimeSeries----------");
		String url = uquery.getUrl() + uquery.getCollection();
		List<Charting> series = new ArrayList<Charting>();
		SolrServer solrSvr = new HttpSolrServer(url);
		SolrQuery query = new SolrQuery();
		query.setQuery("*:*");
		query.setFacet(true).set("facet.range", "id").set("facet.range.start", "2014-01-01T03:00:00Z").set("facet.range.end", "2014-12-30T03:00:00Z").set("facet.range.gap", "+1DAY");
		QueryResponse res = solrSvr.query(query);
		RangeFacet facets_range = res.getFacetRanges().get(0);
		List<RangeFacet.Count> x=facets_range.getCounts();
		for(RangeFacet.Count m:x)
		{
			String facetLabel = m.getValue();
			long facetCount = m.getCount();
			  SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'"); 
			  Date d =f.parse(facetLabel); long milliseconds = d.getTime();
			  Charting t = new Charting();
				t.setDate(milliseconds);
				t.setSize(facetCount);
				series.add(t);
		}
		return series;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<Charting> getCustomTimeSeries(Query uquery) throws Exception {
		logger.debug("Inside dao getTimeSeries----------");
		List<Charting> series = new ArrayList<Charting>();
		Map<String, Long> facetQueryOutput = new HashMap<String, Long>();
		SolrQuery query = new SolrQuery();
		String solrURL = uquery.getUrl() + "/solr";
	    CloudSolrServer solrSvr = new CloudSolrServer(solrURL);
	    solrSvr.setDefaultCollection(uquery.getCollection());  
		    query.setQuery("*:*");
		    query.setFacet(true);
		    query.addFacetField(uquery.getSelected_fields());

		    QueryResponse res = solrSvr.query(query);
		    List<FacetField> facets = res.getFacetFields();

		    for (FacetField ff : facets)
		    {
		      List<Count> counts = ff.getValues();

		      for (FacetField.Count c : counts)
		      {
		        String facetLabel = c.getName();
		        long facetCount = c.getCount();
		        System.out.println(facetLabel);
		        System.out.println(Long.valueOf(facetCount));
		    	Charting t = new Charting();
				t.setLable(facetLabel);
				t.setCount(facetCount);
				t.setChart_type(uquery.getChart());
				t.setCol_name(ff.getName());
				series.add(t);
		      }
		    }
		   
		    
		return series;
	}
	@SuppressWarnings("rawtypes")
	@Override
	public List<Charting> getNumeric(Query uquery) throws Exception {
		logger.debug("Inside dao getNumeric----------");
		List<Charting> series = new ArrayList<Charting>();
		Map<String, Long> facetQueryOutput = new HashMap<String, Long>();
		SolrQuery query = new SolrQuery();
		String url = uquery.getUrl() + uquery.getCollection();
		SolrServer solrSvr = new CloudSolrServer(url);
		query.setQuery(uquery.getQuery());
		 query.addFilterQuery(uquery.getFilter());
		 query.setFacet(true);
		
		    query.addFacetField(uquery.getField_value());
				
				QueryResponse res = solrSvr.query(query);
				List<FacetField> facets = res.getFacetFields();
				
				for (FacetField ff : facets) {
					List<Count> counts = ff.getValues();
					System.out.println(ff.getName());
					for (Count c : counts) {
						String facetLabel =c.getName();
						long facetCount = c.getCount();
						Charting t = new Charting();
						t.setLable(facetLabel);
						t.setCount(facetCount);
						t.setChart_type(uquery.getChart());
						t.setCol_name(ff.getName());
						series.add(t);
					}
				}
		return series;
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<Charting> getCharts(Query uquery) throws Exception {
		logger.debug("Inside dao getCharts----------chart" + uquery.getChart() + "-field--" + uquery.getChart_filed());
		String url = uquery.getUrl() + uquery.getCollection();
		List<Charting> series = new ArrayList<Charting>();
		SolrServer solrSvr = new HttpSolrServer(url);
		SolrQuery query = new SolrQuery();
		query.setQuery("*:*");
		query.setFacet(true);
		query.addFacetField(uquery.getChart_filed());
		QueryResponse res = solrSvr.query(query);
		List<FacetField> facets = res.getFacetFields();
		for (FacetField ff : facets) {
			List<Count> counts = ff.getValues();
			for (Count c : counts) {
				String facetLabel = c.getName();
				long facetCount = c.getCount();
				Charting t = new Charting();
				t.setLable(facetLabel);
				t.setCount(facetCount);
				series.add(t);
			}
		}
		return series;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<Charting> getCustomCharts(Query uquery) throws Exception {
		logger.debug("Inside dao getCustomCharts----------chart" + uquery.getChart() + "-field--" + uquery.getChart_filed());
		List<Charting> series = new ArrayList<Charting>();
		String chart_name=uquery.getChart_name();
		Map<String, Long> facetQueryOutput = new HashMap<String, Long>();
		String solrURL = uquery.getUrl() + "/solr";
	    CloudSolrServer solrSvr = new CloudSolrServer(solrURL);
	    solrSvr.setDefaultCollection(uquery.getCollection());
		
		SolrQuery query = new SolrQuery();
		String PrimFilter=chartService.getPrimaryFilter(uquery.getCollection());
			query.setQuery(uquery.getQuery());
			query.addFilterQuery(PrimFilter+":["+ uquery.getStart_facet() + " TO "+ uquery.getEnd_facet() + "]");
			/* query.set("facet.range.start",uquery.getStart_facet());
			 query.set("facet.range.end",uquery.getEnd_facet());
			 query.set("facet.range.gap",uquery.getRange_gap());*/
			   query.setFacet(true);
			  query.addFacetField(uquery.getSelected_fields());

			logger.debug("Query" + query);
			QueryResponse res = solrSvr.query(query);
			List<FacetField> facets = res.getFacetFields();
			
			for (FacetField ff : facets) {
				List<Count> counts = ff.getValues();
				System.out.println(ff.getName());
				for (Count c : counts) {
					String facetLabel = ff.getName();
					long facetCount = c.getCount();
					Charting t = new Charting();
					t.setLable(facetLabel);
					t.setCount(facetCount);
					t.setChart_type(uquery.getChart());
					t.setChart_name(chart_name);
					series.add(t);
				}
			}
			
		
		return series;
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<Charting> getTopTenURLs(Query uquery) throws Exception {
		logger.debug("Inside dao getTopTenURLs");
		String url = uquery.getUrl() + uquery.getCollection();
		List<Charting> series = new ArrayList<Charting>();
		SolrServer solrSvr = new HttpSolrServer(url);
		SolrQuery query = new SolrQuery();
		query.setQuery("*:*");
		query.setFacet(true);
		query.addFacetField("url");
		QueryResponse res = solrSvr.query(query);
		List<FacetField> facets = res.getFacetFields();
		for (FacetField ff : facets) {
			List<Count> counts = ff.getValues();
			for (int i = 0; i < 10; i++) {
				String facetLabel = counts.get(i).getName();
				long facetCount = counts.get(i).getCount();
				Charting t = new Charting();
				t.setLable(facetLabel);
				t.setCount(facetCount);
				series.add(t);
			}
		}
		return series;
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<Charting> getTopTenIPs(Query uquery) throws Exception {
		logger.debug("Inside dao getTopTenIPs");
		String url = uquery.getUrl() + uquery.getCollection();
		List<Charting> series = new ArrayList<Charting>();
		SolrServer solrSvr = new HttpSolrServer(url);
		SolrQuery query = new SolrQuery();
		query.setQuery("*:*");
		query.setFacet(true);
		query.addFacetField("ip");
		QueryResponse res = solrSvr.query(query);
		List<FacetField> facets = res.getFacetFields();
		for (FacetField ff : facets) {
			List<Count> counts = ff.getValues();
			for (int i = 0; i < 10; i++) {
				String facetLabel = counts.get(i).getName();
				long facetCount = counts.get(i).getCount();
				Charting t = new Charting();
				t.setLable(facetLabel);
				t.setCount(facetCount);
				series.add(t);
			}
		}
		return series;
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<Charting> getFailures(Query uquery) throws Exception {
		logger.debug("Inside dao getFailures");
		String url = uquery.getUrl() + uquery.getCollection();
		List<Charting> series = new ArrayList<Charting>();
		SolrServer solrSvr = new HttpSolrServer(url);
		SolrQuery query = new SolrQuery();
		query.setQuery("status: [300 TO 600]");
		query.setFacet(true);
		query.addFacetField("status");
		QueryResponse res = solrSvr.query(query);
		List<FacetField> facets = res.getFacetFields();
		for (FacetField ff : facets) {
			List<Count> counts = ff.getValues();
			for (int i = 0; i < 10; i++) {
				String facetLabel = counts.get(i).getName();
				long facetCount = counts.get(i).getCount();
				Charting t = new Charting();
				t.setLable(facetLabel);
				t.setCount(facetCount);
				series.add(t);
			}
		}
		return series;
	}

	@SuppressWarnings("deprecation")
	@Override
	public Query getUrlCount(Query uquery) throws Exception {
		logger.debug("Inside dao getUrlCount");
		String url = uquery.getUrl() + uquery.getCollection();
		Query result_count = new Query();
		SolrServer solrSvr = new HttpSolrServer(url);
		SolrQuery query = new SolrQuery();
		query.setQuery("*:*");
		query.setFacet(true);
		query.addFacetField("url");
		QueryResponse res = solrSvr.query(query);
		List<FacetField> facets = res.getFacetFields();
		for (FacetField ff : facets) {
			List<Count> counts = ff.getValues();
			result_count.setTotal_hits(counts.size());
		}
		return result_count;
	}

	@SuppressWarnings("deprecation")
	@Override
	public Query getErrorCount(Query uquery) throws Exception {
		logger.debug("Inside dao getErrorCount");
		String url = uquery.getUrl() + uquery.getCollection();
		Query result_count = new Query();
		SolrServer solrSvr = new HttpSolrServer(url);
		SolrQuery query = new SolrQuery();
		query.setQuery("status: [300 TO 600]");
		QueryResponse res = solrSvr.query(query);
		result_count.setTotal_hits(res.getResults().size());
		return result_count;
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<Charting> getResponseTime(Query uquery) throws Exception {
		logger.debug("Inside dao getResponseTime----------");
		String url = uquery.getUrl() + uquery.getCollection();
		List<Charting> series = new ArrayList<Charting>();
		SolrServer solrSvr = new HttpSolrServer(url);
		SolrQuery query = new SolrQuery();
		query.setQuery("*:*");
		query.setFields("request_date,microsec");
		QueryResponse res = solrSvr.query(query);
		SolrDocumentList results = res.getResults();
		for (SolrDocument doc : results) {
			SimpleDateFormat f = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyyy"); // Mon Mar 31 12:01:53 IST 2014
			Date d = f.parse(doc.get("request_date").toString());
			long milliseconds = d.getTime();
			Charting t = new Charting();
			t.setDate(milliseconds);
			t.setSize((Long) doc.get("microsec"));
			series.add(t);
		}
		return series;
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<Charting> getErrorCountSeries(Query uquery) throws Exception {
		logger.debug("Inside dao getErrorCountSeries----------");
		String url = uquery.getUrl() + uquery.getCollection();
		List<Charting> series = new ArrayList<Charting>();
		SolrServer solrSvr = new HttpSolrServer(url);
		SolrQuery query = new SolrQuery();
		query.setQuery("status: [300 TO 600]");
		query.setFacet(true);
		query.addFacetField("request_date");
		QueryResponse res = solrSvr.query(query);
		List<FacetField> facets = res.getFacetFields();
		for (FacetField ff : facets) {
			List<Count> counts = ff.getValues();
			for (Count c : counts) {
				String facetLabel = c.getName();
				long facetCount = c.getCount();
				SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				Date d = f.parse(facetLabel);
				long milliseconds = d.getTime();
				Charting t = new Charting();
				t.setDate(milliseconds);
				t.setSize(facetCount);
				series.add(t);
			}
		}
		return series;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<Field> getTimeSchema(Core c) throws Exception {
		List<Field> fields = new ArrayList<Field>();
		Map<String, String> SchemaMap = new HashMap<String, String>();
		SchemaMap = objSolrOperations.getSchema(c.getUrl(), c.getCol_name());
		logger.debug("fields==" + SchemaMap.size());
		Iterator it = SchemaMap.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry) it.next();
			Field f = new Field();
			String key = (String) pair.getKey();
			String value = (String) pair.getValue();
			if (value.equalsIgnoreCase("date")) {
				f.setName(key);
				f.setType(value);
				fields.add(f);
			}
		}
		return fields;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<Charting> getCount(Query uquery) throws Exception {
		logger.debug("Inside dao getCustomCharts----------chart" + uquery.getChart() + "-field--" + uquery.getChart_filed());
		List<Charting> series = new ArrayList<Charting>();
		Map<String, Long> facetQueryOutput = new HashMap<String, Long>();
		SolrQuery query = new SolrQuery();
		try {
			query.setQuery(uquery.getQuery());
			logger.debug("Query" + query);
			String[] selected=uquery.getSelected_fields();
			String field_value=selected[0];
			facetQueryOutput = objSolrOperations.executeStringFacetQuery(uquery.getUrl(), uquery.getCollection(), query,
					field_value);
			Iterator it = facetQueryOutput.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				String facetLabel = (String) pair.getKey();
				long facetCount = (Long) pair.getValue();
				Charting t = new Charting();
				t.setLable(uquery.getField_value());
				t.setFacetValue(facetLabel);
				t.setCount(facetCount);
				t.setChart_type(uquery.getChart());
				series.add(t);
				it.remove();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return series;
	}

	
	@SuppressWarnings("deprecation")
	@Override
	public List<Charting> getResponseTimeSeries(Query uquery) throws Exception {
		logger.debug("Inside dao getResponseTimeSeries----------" + uquery.getCollection() + "--" + uquery.getQuery());
		String url = uquery.getUrl() + uquery.getCollection();
		List<Charting> series = new ArrayList<Charting>();
		SolrServer solrSvr = new HttpSolrServer(url);
		SolrQuery query = new SolrQuery();
		query.setQuery(uquery.getQuery());
		query.setFields("microsec", "request_date");
		if (uquery.getStart() != 0) {
			query.setStart(uquery.getStart());
		} else {
			query.setStart(0);
		}
		query.setRows(500);
		query.setSortField("request_date", SolrQuery.ORDER.asc);
		if (uquery.getMonth().equalsIgnoreCase("Jan")) {
			query.addFilterQuery("request_date:[2015-01-01T00:00:00Z TO 2015-02-01T00:00:00Z]");
		}
		if (uquery.getMonth().equalsIgnoreCase("Feb")) {
			query.addFilterQuery("request_date:[2015-02-01T00:00:00Z TO 2015-03-01T00:00:00Z]");
		}
		if (uquery.getMonth().equalsIgnoreCase("March")) {
			query.addFilterQuery("request_date:[2015-03-01T00:00:00Z TO 2015-04-01T00:00:00Z]");
		}
		if (uquery.getMonth().equalsIgnoreCase("April")) {
			query.addFilterQuery("request_date:[2015-04-01T00:00:00Z TO 2015-05-01T00:00:00Z]");
		}
		if (uquery.getMonth().equalsIgnoreCase("May")) {
			query.addFilterQuery("request_date:[2014-05-01T00:00:00Z TO 2014-06-01T00:00:00Z]");
		}
		if (uquery.getMonth().equalsIgnoreCase("June")) {
			query.addFilterQuery("request_date:[2014-06-01T00:00:00Z TO 2014-07-01T00:00:00Z]");
		}
		if (uquery.getMonth().equalsIgnoreCase("July")) {
			query.addFilterQuery("request_date:[2014-07-01T00:00:00Z TO 2014-08-01T00:00:00Z]");
		}
		if (uquery.getMonth().equalsIgnoreCase("August")) {
			query.addFilterQuery("request_date:[2014-08-01T00:00:00Z TO 2014-09-01T00:00:00Z]");
		}
		if (uquery.getMonth().equalsIgnoreCase("Sep")) {
			query.addFilterQuery("request_date:[2014-09-01T00:00:00Z TO 2014-10-01T00:00:00Z]");
		}
		if (uquery.getMonth().equalsIgnoreCase("Oct")) {
			query.addFilterQuery("request_date:[2014-10-01T00:00:00Z TO 2014-11-01T00:00:00Z]");
		}
		if (uquery.getMonth().equalsIgnoreCase("Nov")) {
			query.addFilterQuery("request_date:[2014-11-01T00:00:00Z TO 2014-12-01T00:00:00Z]");
		}
		if (uquery.getMonth().equalsIgnoreCase("Dec")) {
			query.addFilterQuery("request_date:[2014-12-01T00:00:00Z TO 2014-12-30T00:00:00Z]");
		}
		logger.debug("Query-" + query);
		QueryResponse res = solrSvr.query(query);
		SolrDocumentList results = res.getResults();
		for (SolrDocument doc : results) {
			Date d = (Date) doc.get("request_date");
			long count = (Long) doc.get("microsec");
			long milliseconds = d.getTime();
			Charting chart = new Charting();
			chart.setDate(milliseconds);
			chart.setSize(count);
			series.add(chart);
		}
		return series;
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<Charting> getStatusCharts(Query uquery) throws Exception {
		logger.debug("Inside dao getCustomCharts----------chart" + uquery.getChart() + "-field--" + uquery.getChart_filed());
		String url = uquery.getUrl() + uquery.getCollection();
		List<Charting> series = new ArrayList<Charting>();
		SolrServer solrSvr = new HttpSolrServer(url);
		SolrQuery query = new SolrQuery();
		query.setQuery(uquery.getQuery());
		query.addFilterQuery("status : [400 TO 600]");
		query.addFilterQuery(uquery.getFilter());
		query.setFacet(true);
		query.addFacetField(uquery.getChart_filed());
		QueryResponse res = solrSvr.query(query);
		List<FacetField> facets = res.getFacetFields();
		for (FacetField ff : facets) {
			List<Count> counts = ff.getValues();
				for (Count c : counts) {
					String facetLabel = c.getName();
					long facetCount = c.getCount();
					Charting t = new Charting();
					t.setLable(facetLabel);
					t.setCount(facetCount);
					t.setChart_type(uquery.getChart());
					series.add(t);
			} 
		}
		return series;
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<Charting> getURLCharts(Query uquery) throws Exception {
		logger.debug("Inside dao getURLCharts----------chart" + uquery.getChart() + "-field--" + uquery.getChart_filed()+"filter-"+uquery.getFilter());
		String url = uquery.getUrl() + uquery.getCollection();
		List<Charting> series = new ArrayList<Charting>();
		SolrServer solrSvr = new HttpSolrServer(url);
		SolrQuery query = new SolrQuery();
		query.setQuery(uquery.getQuery());
		query.addFilterQuery(uquery.getFilter());
		query.setFacet(true);
		query.addFacetField(uquery.getChart_filed());
		QueryResponse res = solrSvr.query(query);
		int count=0;
		List<FacetField> facets = res.getFacetFields();
		for (FacetField ff : facets) {
			List<Count> counts = ff.getValues();
			for (Count c : counts) {
				String facetLabel = c.getName();
				long facetCount = c.getCount();
				if ((c.getName()).contains(".do") && count < 10) {
					Charting t = new Charting();
					t.setLable(facetLabel);
					t.setCount(facetCount);
					t.setChart_type(uquery.getChart());
					series.add(t);
					count++;
				}
			}
		}
		return series;
	}

	
	@Override
	public List<Correlation> getBasicQueryForCorrelation(Query uquery) {
		logger.debug("Inside dao getBasicQuery----------"+Arrays.toString(uquery.getSelected_fields()));
		String url = uquery.getUrl() + uquery.getCollection();
		String sort = uquery.getSort_order();
		String sort_field=uquery.getSort_field();
		String selectes_fields = "";
		String[] get_selected_fields=uquery.getSelected_fields();
		
		if (uquery.getSelected_fields() != null	&& uquery.getSelected_fields().length > 0) {
		}
		//objSolrOperations.
		Query result_data = new Query();
		SolrQuery query = new SolrQuery();
		query.setQuery(uquery.getQuery());
		if (uquery.getSelected_fields() != null	&& uquery.getSelected_fields().length > 0) {
			selectes_fields = Arrays.toString(uquery.getSelected_fields());
			selectes_fields = selectes_fields.substring(1,selectes_fields.length() - 1);
			query.setFields(selectes_fields);
		}
		query.setStart(uquery.getStart());
		query.setRows(uquery.getRows());
		if (!uquery.getFilter().equalsIgnoreCase("default")) {
			query.addFilterQuery(uquery.getFilter());
		}
		if(!sort_field.equalsIgnoreCase("null"))
		{
			logger.debug("Inside not null condition");
			if(sort_field.equalsIgnoreCase("Request_Date"))
			{
				sort_field="request_date";
			}
			else if(sort_field.equalsIgnoreCase("Response_Time"))
			{
				sort_field="microsec";
			}
			else if(sort_field.equalsIgnoreCase("Response_Size"))
			{
				sort_field="size";
			}
			else if(sort_field.equalsIgnoreCase("_version_"))
			{
				sort_field="_version_";
			}
		if (sort.equalsIgnoreCase("Descending")) {
			query.addSort(sort_field, SolrQuery.ORDER.desc);
		} else {
			query.addSort(sort_field, SolrQuery.ORDER.asc);
		}
		}

		SolrDocumentList results = null;
		try {
			results = objSolrOperations.executeQuery(uquery.getUrl(), uquery.getCollection(), query);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (SolrServerException e) {
			e.printStackTrace();
		}
		
		long numFound = results.getNumFound();
		List<String> docs = new ArrayList<String>();
		if (uquery.getSelected_fields() != null	&& uquery.getSelected_fields().length > 0) {
			for (SolrDocument doc : results) {
				StringBuffer result_string=new StringBuffer("");
				for(String field:get_selected_fields)
				{
					logger.debug("Field---------------"+field);
					logger.debug("value---------------"+doc.get(field));
					String temp_field_result="";
					if(field.equalsIgnoreCase("dateTime"))
					{
						SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'"); 
						f.setTimeZone(TimeZone.getTimeZone("GMT"));
						String new_date=f.format((Date) doc.get(field));
						temp_field_result=new_date;
					}
					
					else if(field.equalsIgnoreCase("TotalPhysicalMemory")  || field.equalsIgnoreCase("AvailablePhysicalMemory")|| field.equalsIgnoreCase("AvailablePhysicalInPercent")|| field.equalsIgnoreCase("VirtualMemoryMaxSize")|| field.equalsIgnoreCase("AvailableVirtualMEmory")|| field.equalsIgnoreCase("UsedVirtualMemory")|| field.equalsIgnoreCase("UsedVirtualMemoryInPercent")|| field.equalsIgnoreCase("_version_"))
					{
						if(doc.get(field)==null)
						{
							temp_field_result="0";	
							logger.debug("Inside null");
						}
						else
						{
						temp_field_result=((Long) doc.get(field)).toString();
						}
					}
					else if(field.equalsIgnoreCase("Size")  || field.equalsIgnoreCase("MemoryUsage")|| field.equalsIgnoreCase("CpuUsageInPercent")|| field.equalsIgnoreCase("TotalDiskSize")||field.equalsIgnoreCase("TotalDiskUsed")||field.equalsIgnoreCase("TotalDiskAvail")||field.equalsIgnoreCase("TotalUsePercent")||field.equalsIgnoreCase("TotalAvailablepercent")||field.equalsIgnoreCase("CDiskSize")||field.equalsIgnoreCase("CDiskUsed")||field.equalsIgnoreCase("CDiskAvail")||field.equalsIgnoreCase("CUsePercent")||field.equalsIgnoreCase("DDiskSize")||field.equalsIgnoreCase("DDiskUsed")||field.equalsIgnoreCase("DDiskAvail")||field.equalsIgnoreCase("DUsePercent")||field.equalsIgnoreCase("EDiskSize")||field.equalsIgnoreCase("EDiskUsed")||field.equalsIgnoreCase("EDiskAvail")||field.equalsIgnoreCase("EUsePercent")||field.equalsIgnoreCase("FDiskSize")||field.equalsIgnoreCase("FDiskUsed")||field.equalsIgnoreCase("FDiskAvail")||field.equalsIgnoreCase("FUsePercent"))
					{
						
						if(doc.get(field)==null)
						{
							temp_field_result="0.0";	
							logger.debug("Inside null");
						}
						else
						{
						temp_field_result=((Double) doc.get(field)).toString();
						}
					}
					else
					{
						temp_field_result=(String) doc.get(field);
					}
					result_string.append(field);
					result_string.append(":");
					result_string.append(temp_field_result);
					result_string.append("  ");
				}
				docs.add(result_string.toString());
			}
		}
		else
		{
			for (SolrDocument doc : results) {
				docs.add(doc.toString());
			}
		}
		result_data.setResults(docs);
		result_data.setTotal_hits(numFound);
		logger.debug("Result object-"+result_data);
		//Code for correlation starts
		List<Correlation> correlationList = new ArrayList<Correlation>();
		logger.debug("get result data size"+result_data.getResults().size());
		for(int i=0;i<result_data.getResults().size();i++)
		{
			Correlation correlation = new Correlation();
			correlation.setRecord(result_data.getResults().get(i));
			correlationList.add(correlation);
			logger.debug("Record inside the correlation POJO"+result_data.getResults().get(i));
		}
		return correlationList;
	}

	@Override
	public List<Correlation> getSelectedCorrelationFieldsQuery(Query uquery) {
        int flag=0;
		logger.info("Inside getSelectedCorrelationFieldsQuery :: collection name ="+uquery.getCollection()+ " query ="+uquery.getQuery());
        logger.debug("Inside dao getSelectedCorrelationFieldsQuery----------"+Arrays.toString(uquery.getSelected_fields()));
		String sort = uquery.getSort_order();
		String sort_field=uquery.getSort_field();
		String selectes_fields = "";
		String[] get_selected_fields=uquery.getSelected_fields();
		
		//objSolrOperations.
		Query result_data = new Query();
		SolrQuery query = new SolrQuery();
		query.setQuery(uquery.getQuery());
		if (uquery.getSelected_fields() != null && uquery.getSelected_fields().length > 0) {
			selectes_fields = Arrays.toString(uquery.getSelected_fields());
			selectes_fields = selectes_fields.substring(1,selectes_fields.length() - 1);
			query.setFields(selectes_fields);
		}
		query.setStart(uquery.getStart());
		query.setRows(uquery.getRows());
		if (!uquery.getFilter().equalsIgnoreCase("default")) {
			query.addFilterQuery(uquery.getFilter());
		}
		if(!sort_field.equalsIgnoreCase("null"))
		{
			if (sort.equalsIgnoreCase("Descending")) {
				query.addSort(sort_field, SolrQuery.ORDER.desc);
			} else {
				query.addSort(sort_field, SolrQuery.ORDER.asc);
			}
		}

		SolrDocumentList results = null;
		try {
			results = objSolrOperations.executeQuery(uquery.getUrl(), uquery.getCollection(), query);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (SolrServerException e) {
			e.printStackTrace();
		}
		catch(RemoteSolrException re){
			re.printStackTrace();
			flag=1;
		}
		if(flag==1){
			return null;
		}
		else {
			long numFound = results.getNumFound();
			List<String> docs = new ArrayList<String>();
			if (uquery.getSelected_fields() != null	&& uquery.getSelected_fields().length > 0) {
				for (SolrDocument doc : results) {
					StringBuffer result_string=new StringBuffer("");
					for(String field:get_selected_fields)
					{
						logger.debug("Field---------------"+field);
						logger.debug("value---------------"+doc.get(field));
						String temp_field_result="";
						
						if(doc.get(field) == null || doc.get(field).equals("null")){
							temp_field_result = "#";
						} else if(doc.get(field) instanceof Long){
							temp_field_result = ((Long) doc.get(field)).toString();
						} else if(doc.get(field) instanceof Double){
							temp_field_result = ((Double) doc.get(field)).toString();
						} else if(doc.get(field) instanceof Date){
							SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							f.setTimeZone(TimeZone.getTimeZone("GMT"));
							String new_date = f.format((Date) doc.get(field));
							temp_field_result = new_date;
						} else if(doc.get(field) instanceof ArrayList){
							temp_field_result =  doc.get(field).toString();
						} else {
							temp_field_result = (String) doc.get(field);
						}
						result_string.append(field);
						result_string.append(":");
						result_string.append(temp_field_result);
						result_string.append("  ");
					}
					docs.add(result_string.toString());
				}
			}
			else
			{
				for (SolrDocument doc : results) {
					docs.add(doc.toString());
				}
			}
		
			result_data.setResults(docs);
			result_data.setTotal_hits(numFound);
			logger.debug("Result object-"+result_data);
			
			//Code for correlation starts
			List<Correlation> correlationList = new ArrayList<Correlation>();
			logger.debug("get result data size"+result_data.getResults().size());
			
			for(int i=0;i<result_data.getResults().size();i++)
			{
				Correlation correlation = new Correlation();
				correlation.setRecord(result_data.getResults().get(i));
				correlationList.add(correlation);
				logger.debug("Record inside the correlation POJO"+result_data.getResults().get(i));
			}
		
		return correlationList;
		}
	}

	@Override
	public List<Charting> getCustomChartsNofilter(Query query_param) throws SolrServerException, MalformedURLException{
	logger.debug("Inside dao getCustomChartsNofilter----------chart" + query_param.getChart() + "-field--" + query_param.getChart_filed());
	List<Charting> series = new ArrayList<Charting>();
	String chart_name=query_param.getChart_name();
	Map<String, Long> facetQueryOutput = new HashMap<String, Long>();
	String solrURL = query_param.getUrl() + "/solr";
    CloudSolrServer solrSvr = new CloudSolrServer(solrURL);
    solrSvr.setDefaultCollection(query_param.getCollection());
	SolrQuery query = new SolrQuery();
	
		query.setQuery(query_param.getQuery());
		   query.setFacet(true);
		  query.addFacetField(query_param.getSelected_fields());

		logger.debug("Query" + query);
		QueryResponse res = solrSvr.query(query);
		List<FacetField> facets = res.getFacetFields();
		
		for (FacetField ff : facets) {
			List<Count> counts = ff.getValues();
			System.out.println(ff.getName());
			for (Count c : counts) {
				String facetLabel = ff.getName();
				long facetCount = c.getCount();
				String facetValue=c.getName();
				Charting t = new Charting();
				t.setLable(facetLabel);
				t.setCount(facetCount);
				t.setChart_type(query_param.getChart());
				t.setChart_name(chart_name);
				t.setFacetValue(facetValue);
				series.add(t);
			}
		}
		
	
	return series;
	}
	
}
