/**
 * 
 */
package com.lnt.smartit.models;

import java.io.Serializable;

/**
 * @author 299273
 *
 */
public class AMQWinCPU implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private double cpuUsageInPercent;
	private double warning_High;
	private double critical_High;
	
	public double getCpuUsageInPercent() {
		return cpuUsageInPercent;
	}
	public void setCpuUsageInPercent(double cpuUsageInPercent) {
		this.cpuUsageInPercent = cpuUsageInPercent;
	}
	public double getWarning_High() {
		return warning_High;
	}
	public void setWarning_High(double warning_High) {
		this.warning_High = warning_High;
	}
	public double getCritical_High() {
		return critical_High;
	}
	public void setCritical_High(double critical_High) {
		this.critical_High = critical_High;
	}
}
