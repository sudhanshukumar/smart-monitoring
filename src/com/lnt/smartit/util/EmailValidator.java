package com.lnt.smartit.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.lnt.smartit.constants.Constant;

public class EmailValidator {
	
	private Pattern pattern;
	private Matcher matcher;
	private static Logger logger = Logger.getLogger(EmailValidator.class);

	private static final String EMAIL_PATTERN = 
		"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
		+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
 
	public EmailValidator() {
		pattern = Pattern.compile(EMAIL_PATTERN);
	}

	public boolean validateEmail(final String hexemailIds) {
		List<String> emailList = new ArrayList<String>();
		String emailIds = hexemailIds.trim();
		if (emailIds.contains(Constant.EMAIL_ID_SEPERATOR)) {
			emailList = Arrays.asList(emailIds.split(Constant.EMAIL_ID_SEPERATOR));
		} else {
			emailList.add(emailIds);
		}
		ArrayList<Boolean> emailRes = new ArrayList<Boolean>();
		for (String eid : emailList) {
			matcher = pattern.matcher(eid);
			emailRes.add(matcher.matches());
		}
		if (emailRes.contains(false)) {
			return false;
		} else
			return true;
	}
	
	public static void main(String[] args) {
		EmailValidator emailValidator=new EmailValidator();
		boolean isEmailValid=emailValidator.validateEmail("Austin.Sequeira@lntinfotech.com,Brijesh.Singh@lntinfotech.com,Viswanathan.N@lntinfotech.com");
		logger.debug(isEmailValid);
	}
}
