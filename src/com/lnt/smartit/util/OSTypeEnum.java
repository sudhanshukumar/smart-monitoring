package com.lnt.smartit.util;

public enum OSTypeEnum {
	
	//kindly make changes here to update OS LIST 
	
	WINDOWS("Windows"),LINUX("Linux");
	
	private final String osTypes;
	private OSTypeEnum(String ostype) {
		osTypes=ostype;
	}
	
	
	
	public String getOsTypes() {
		return osTypes;
	}



	@Override
	  public String toString() {
	    return osTypes;
	  }

}
