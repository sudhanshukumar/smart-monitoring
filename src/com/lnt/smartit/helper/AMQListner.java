package com.lnt.smartit.helper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import kafka.consumer.ConsumerIterator;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.Logger;
import org.fusesource.hawtbuf.ByteArrayInputStream;
import org.springframework.stereotype.Component;
import com.kafka.consumer.Consumer;
import com.lnt.smartit.controllers.DashboardController;
import com.lnt.smartit.models.AlertMessage;
import com.lnt.smartit.models.AllMachinesAlertMessages;
import com.lnt.smartit.observer.QueueSubject;
import com.lnt.smartit.util.DatabaseConfig;

@Component("amqListner")
public class AMQListner implements MessageListener,ExceptionListener{
	private static Logger logger = Logger.getLogger(AMQListner.class);

	private int ackMode = Session.AUTO_ACKNOWLEDGE;
	private Session session;
	private MessageProducer replyProducer;
	private boolean verbose;
	private boolean transacted;
	private long messagesReceived;
	private boolean running;
	private long batch = 10;
	private int i = 1;
	PrintWriter writer;
	ActiveMQConnectionFactory connectionFactory;
	public MessageConsumer consumer;
	private Consumer kafkaConsumer= null;
	
	@SuppressWarnings("unused")
	public void recieveMessage() throws IOException{
		logger.debug("--------In infinite while------");
		FileOutputStream fos=null;
		PrintWriter pw = null;
		String url =null;
		String timeout=null;
		String groupId=null;
		String partitionCount=null;
		String fetchSizeBytes=null;
		String zookeeperBasePath=null;
		String brokerPort=null;
		String brokersCommaSeperatedList=null;
		List<String> topicName= null;
		Map<String,String> propsMap = new HashMap<String, String>();
		for(i=0;i<DatabaseConfig.config.size();i++){
			if(DatabaseConfig.config.get(i).getServiceComp().equalsIgnoreCase("KAFKA")&& DatabaseConfig.config.get(i).getServiceCompType().equalsIgnoreCase("PROC_STORE")){
				topicName=DatabaseConfig.topicName;
				if(DatabaseConfig.config.get(i).getParamName().equalsIgnoreCase("zookeeperAddressCommaSeperatedList")){
					url=DatabaseConfig.config.get(i).getParamValue();
				}
				else if(DatabaseConfig.config.get(i).getParamName().equalsIgnoreCase("zookeeperSessionTimeout")){
					timeout=DatabaseConfig.config.get(i).getParamValue();
					System.out.println(timeout);
				}
				else if(DatabaseConfig.config.get(i).getParamName().equalsIgnoreCase("partitionCount")){
					partitionCount=DatabaseConfig.config.get(i).getParamValue();
				}
				else if(DatabaseConfig.config.get(i).getParamName().equalsIgnoreCase("consumerId")){
					groupId=DatabaseConfig.config.get(i).getParamValue();
				}
				else if(DatabaseConfig.config.get(i).getParamName().equalsIgnoreCase("fetchSizeBytes")){
					fetchSizeBytes=DatabaseConfig.config.get(i).getParamValue();
				}
				else if(DatabaseConfig.config.get(i).getParamName().equalsIgnoreCase("zookeeperBasePath")){
					zookeeperBasePath=DatabaseConfig.config.get(i).getParamValue();
				}
				else if(DatabaseConfig.config.get(i).getParamName().equalsIgnoreCase("brokerPort")){
					brokerPort=DatabaseConfig.config.get(i).getParamValue();
				}
				else if(DatabaseConfig.config.get(i).getParamName().equalsIgnoreCase("brokersCommaSeperatedList")){
					brokersCommaSeperatedList=DatabaseConfig.config.get(i).getParamValue();
				}
			}
		}
		propsMap.put("zookeeper.connect",url+":2181");
		propsMap.put("zookeeper.connection.timeout.ms",timeout);
		propsMap.put("group.id", groupId+"12");
		propsMap.put("partitionCount",partitionCount);
			
		logger.info("SmartIT :: KAFKA Consumer creation :: Topic name = "+ topicName.get(0) +" Configurations = "+propsMap);	
		kafkaConsumer = new Consumer(topicName.get(0), groupId+"12", propsMap);
		
	    ConsumerIterator<byte[], byte[]> it =  kafkaConsumer.getTopicIterator();
        while (it.hasNext()){
        	byte[] bytes = it.next().message();
        	try {
        		logger.info("Message arrived in dashboard consumer thread !!");
				ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(bytes));
				AllMachinesAlertMessages allMachinesAlertMessages = (AllMachinesAlertMessages) objectInputStream.readObject();
				List<AlertMessage> alerMessageList = allMachinesAlertMessages.getAlertMessageList();
				logger.debug("SmartIT :: KAFKA message recieved in dashboard consumer :: alertList :: " + alerMessageList.toString());
				QueueSubject.alertMessageEventQueue.add(allMachinesAlertMessages);
				// EventCounter increases by 1 when a message is recieved 
				QueueSubject.eventCounter = QueueSubject.eventCounter + 1;
				QueueSubject.changed = true;
				DashboardController.queueSubject.notifyObservers();
				QueueSubject.changed=false;
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
        	
        }  			        	
	}

	public void onMessage(Message message) {
		
		logger.debug("On Message ");
		messagesReceived++;
		try {
			if (message instanceof ObjectMessage) {
				logger.debug("Messages recieved .......");
				ObjectMessage alertObject = (ObjectMessage) message;
				AllMachinesAlertMessages allMachinesAlertMessages = (AllMachinesAlertMessages) alertObject.getObject();
				List<AlertMessage> alerMessageList = allMachinesAlertMessages.getAlertMessageList();
				logger.debug(allMachinesAlertMessages.toString());
				logger.debug("IP count msg-----------------" + alerMessageList.toString());
				QueueSubject.alertMessageEventQueue.add(allMachinesAlertMessages);
				QueueSubject.changed = true;
				DashboardController.queueSubject.notifyObservers();
				QueueSubject.changed=false;
			} else {
				if (verbose) {
					logger.debug("Count ---------------" + message);
				}
			}
			if (message.getJMSReplyTo() != null) {
				replyProducer.send(
						message.getJMSReplyTo(),
						session.createTextMessage("Reply: "
								+ message.getJMSMessageID()));
			}
			if (transacted) {
				if ((messagesReceived % batch) == 0) {
					logger.debug("Commiting transaction for last "
							+ batch + " messages; messages so far = "
							+ messagesReceived);
					session.commit();
				}
			} else if (ackMode == Session.CLIENT_ACKNOWLEDGE) {
				if ((messagesReceived % batch) == 0) {
					logger.debug("Acknowledging last " + batch
							+ " messages; messages so far = "
							+ messagesReceived);
					message.acknowledge();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onException(JMSException arg0) {
		running = false;
	}

}