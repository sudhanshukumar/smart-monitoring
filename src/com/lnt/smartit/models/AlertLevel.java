package com.lnt.smartit.models;


public enum AlertLevel{
	INFO("INFO"), WARNING("WARNING"), CRITICAL("CRITICAL");
	private String value;
	AlertLevel(String value){
		this.value = value;
	}
}
