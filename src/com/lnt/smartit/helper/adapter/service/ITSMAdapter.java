package com.lnt.smartit.helper.adapter.service;

import org.springframework.stereotype.Component;

import com.lnt.smartit.models.AlertMessage;

@Component
public interface ITSMAdapter {
	public String createITSMincident(AlertMessage alertMsg);	
}
