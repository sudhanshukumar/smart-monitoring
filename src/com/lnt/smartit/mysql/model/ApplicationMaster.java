package com.lnt.smartit.mysql.model;

// Generated Aug 11, 2014 11:23:06 AM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * ApplicationMaster generated by hbm2java
 */
public class ApplicationMaster implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String applicationId;
	private int version;
	@NotEmpty
	private String applicationName;
	@NotEmpty
	private String description;
	private String contactPerson;
	private String contactPersonEmail;
	private String contactNumber;
	private String createdBy;
	private Date creationDate;
	private String modifiedBy;
	private Date modifyDate;
	private Set<?> deviceMasters = new HashSet<Object>(0);

	public ApplicationMaster() {
	}

	public ApplicationMaster(String applicationId, String applicationName,
			String contactPerson, String contactPersonEmail, String createdBy,
			Date creationDate) {
		this.applicationId = applicationId;
		this.applicationName = applicationName;
		this.contactPerson = contactPerson;
		this.contactPersonEmail = contactPersonEmail;
		this.createdBy = createdBy;
		this.creationDate = creationDate;
	}

	public ApplicationMaster(String applicationId, String applicationName,
			String description, String contactPerson,
			String contactPersonEmail, String contactNumber, String createdBy,
			Date creationDate, String modifiedBy, Date modifyDate,
			Set<?> deviceMasters) {
		this.applicationId = applicationId;
		this.applicationName = applicationName;
		this.description = description;
		this.contactPerson = contactPerson;
		this.contactPersonEmail = contactPersonEmail;
		this.contactNumber = contactNumber;
		this.createdBy = createdBy;
		this.creationDate = creationDate;
		this.modifiedBy = modifiedBy;
		this.modifyDate = modifyDate;
		this.deviceMasters = deviceMasters;
	}

	public String getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	@JsonIgnore
	public int getVersion() {
		return this.version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getApplicationName() {
		return this.applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	@JsonIgnore
	public String getContactPerson() {
		return this.contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
	@JsonIgnore
	public String getContactPersonEmail() {
		return this.contactPersonEmail;
	}

	public void setContactPersonEmail(String contactPersonEmail) {
		this.contactPersonEmail = contactPersonEmail;
	}
	@JsonIgnore
	public String getContactNumber() {
		return this.contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	@JsonIgnore
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@JsonIgnore
	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	@JsonIgnore
	public String getModifiedBy() {
		return this.modifiedBy;
	}
	@JsonIgnore
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	@JsonIgnore
	public Date getModifyDate() {
		return this.modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	@JsonIgnore
	public Set<?> getDeviceMasters() {
		return this.deviceMasters;
	}

	public void setDeviceMasters(Set<?> deviceMasters) {
		this.deviceMasters = deviceMasters;
	}

	@Override
	public String toString() {
		return "ApplicationMaster [applicationId=" + applicationId
				+ ", version=" + version + ", applicationName="
				+ applicationName + ", description=" + description
				+ ", contactPerson=" + contactPerson + ", contactPersonEmail="
				+ contactPersonEmail + ", contactNumber=" + contactNumber
				+ ", createdBy=" + createdBy + ", creationDate=" + creationDate
				+ ", modifiedBy=" + modifiedBy + ", modifyDate=" + modifyDate
				+ "]";
	}
}
