$(document).ready(function() {
	$("#result_grid").hide();
	//$("#field_grid").hide();
	//$("#fieldrow").hide();
	 getReports();
	 getapplications();
	 getapplicationsforPrim();
	 getapplicationsforSec();
});


function getReports(){
	$.ajax({
		type : "GET",
		url : "getReports.html",
		success : function(response) {
				$("#resId").find("tr:gt(0)").remove();
				for ( var i = 0; i < response.length; i++) {
				
						$("#resId").find('tbody').append(
								$('<tr>').append(
										$('<td>').append(
												'<input class="check" name="select_reports" type="checkbox" value='
														+ response[i][1] + '>')
												.append($('</td>'))).append(
										$('<td>').text(response[i][0]).append(
												$('</td>'))));
				}
				$("#result_grid").show();
			
		}
	});
}

$(function() {
	 $("#deleteReports").click(function() {
	var selected_reports=[];
	$.each(
			$("input[name='select_reports']:checked"),
			function() {
				selected_reports.push($(this)
						.val());
				
			});
	
	$.ajax({
		type : "GET",
		url : "deleteReports.html",
		data:"selected_reports=" +encodeURIComponent(selected_reports),
		success : function(response) {
			getReports();
			if(response==true){
				alert("Report Deleted");
			}
			else{
				alert("Report not deleted");
			}
		}
	});
	
});

	 

	 $("#selectFields").click(function() {
	var selected_fields=[];
	var col_name=$('#colValue :selected').val();
	$.each(
			$("input[name='select_fields']:checked"),
			function() {
				selected_fields.push($(this)
						.val());
				
			});
	
	$.ajax({
		type : "GET",
		url : "selectReports.html",
		data:"selected_fields=" +encodeURIComponent(selected_fields)+"&col_name="+encodeURIComponent(col_name)+"&flag=Invalid",
		success : function(response) {
			getschema(col_name);
			if(response==true){
				 getapplicationsforPrim();
				 getapplicationsforSec();
				alert("Fields Saved");
			}
			else{
				alert("Fields not Saved");
			}
		}
	});
	
});

	 $("#selectPrimary").click(function() {
			var col_name=$('#colValueforFilter :selected').val();
			var field_name=$('#primaryFields :selected').val();
			$.ajax({
				type : "GET",
				url : "selectReports.html",
				data:"selected_fields=" +field_name+"&col_name="+col_name+"&flag=Primary",
				success : function(response) {
					if(response==true){
						alert("Primary Filter Saved");
					}
					else{
						alert("Primary Filter not Saved");
					}
				}
			});
			
		});
	 
	 $("#selectSecondary").click(function() {
			var selected_fields=[];
			var col_name=$('#colValueforSecFilter :selected').val();
			$.each(
					$("input[name='select_Sec_fields']:checked"),
					function() {
						selected_fields.push($(this)
								.val());
						
					});
			
			$.ajax({
				type : "GET",
				url : "selectReports.html",
				data:"selected_fields=" +selected_fields+"&col_name="+col_name+"&flag=Secondary",
				success : function(response) {
					getschemaforSec(col_name);
					if(response==true){
						alert("Secondary Filter Saved");
					}
					else{
						alert("Secondary Filter not Saved");
					}
				}
			});
			
		});

	 
});


function getapplications()
{
	$.ajax({
		type : "GET",
		url : "getApplications.html",
		success : function(response) {
			for ( var i = 0; i < response.length; i++) {
				 $("#application").append('<option value="'+response[i].appId+'">'+response[i].appName+'</option>');
			}
			var appsel=$('#application :selected').val();
			
			getCollectionsFromDB(appsel);
			$('#application').change(
					function() {
			var appsel=$('#application :selected').val();
			
			getCollectionsFromDB(appsel);
					});
		}
	});
}
function getapplicationsforPrim()
{
	$.ajax({
		type : "GET",
		url : "getApplications.html",
		success : function(response) {
			for ( var i = 0; i < response.length; i++) {
				 $("#applicationforFilter").append('<option value="'+response[i].appId+'">'+response[i].appName+'</option>');
			}
			var appsel=$('#applicationforFilter :selected').val();
			
			getCollectionsforPrim(appsel);
			$('#applicationforFilter').change(
					function() {
			var appsel=$('#applicationforFilter :selected').val();
			
			getCollectionsforPrim(appsel);
					});
		}
	});
}

function getapplicationsforSec()
{
	$.ajax({
		type : "GET",
		url : "getApplications.html",
		success : function(response) {
			for ( var i = 0; i < response.length; i++) {
				 $("#applicationforSecFilter").append('<option value="'+response[i].appId+'">'+response[i].appName+'</option>');
			}
			var appsel=$('#applicationforSecFilter :selected').val();
			
			getCollectionsforSec(appsel);
			$('#applicationforSecFilter').change(
					function() {
			var appsel=$('#applicationforSecFilter :selected').val();
			
			getCollectionsforSec(appsel);
					});
		}
	});
}


function getCollectionsFromDB(appsel){
	$.ajax({
		type : "GET",
		url : "getCollectionsFromDB.html",
		data:"appname="+encodeURIComponent(appsel),
		success : function(response) {
			$("#colValue").empty();
			for ( var i = 0; i < response.length; i++) {
				 $("#colValue").append('<option value="'+response[i]+'">'+response[i]+'</option>');
			}
			var col_name=$('#colValue :selected').val();
			console.log("col_name "+col_name);
			getschema(col_name);
			$('#colValue').change(
					function() {
			var col_name=$('#colValue :selected').val();
			console.log("col_name "+col_name);
			getschema(col_name);
					});
			
		}
	});
	
}

function getCollectionsforPrim(appsel){
	$.ajax({
		type : "GET",
		url : "getCollectionsFromDB.html",
		data:"appname="+encodeURIComponent(appsel),
		success : function(response) {
			$("#colValueforFilter").empty();
			for ( var i = 0; i < response.length; i++) {
				 $("#colValueforFilter").append('<option value="'+response[i]+'">'+response[i]+'</option>');
			}
			var col_name=$('#colValueforFilter :selected').val();
			console.log("col_name "+col_name);
			getschemaforPrim(col_name);
			$('#colValueforFilter').change(
					function() {
			var col_name=$('#colValueforFilter :selected').val();
			console.log("col_name "+col_name);
			getschemaforPrim(col_name);
					});
			
		}
	});
	
}

function getCollectionsforSec(appsel){
	$.ajax({
		type : "GET",
		url : "getCollectionsFromDB.html",
		data:"appname="+encodeURIComponent(appsel),
		success : function(response) {
			$("#colValueforSecFilter").empty();
			for ( var i = 0; i < response.length; i++) {
				 $("#colValueforSecFilter").append('<option value="'+response[i]+'">'+response[i]+'</option>');
			}
			var col_name=$('#colValueforSecFilter :selected').val();
			console.log("col_name "+col_name);
			getschemaforSec(col_name);
			$('#colValueforSecFilter').change(
					function() {
			var col_name=$('#colValueforSecFilter :selected').val();
			console.log("col_name "+col_name);
			getschemaforSec(col_name);
					});
			
		}
	});
	
}


function getschema(col_name) {
//	$("#fieldrow").show();
	$.ajax({
		type : "POST",
		url : "getSchema.html",
		data : "collection=" + encodeURIComponent(col_name),
		success : function(response) {
				$("#fieldId").find("tr:gt(0)").remove();
				for ( var i = 0; i < response.length; i++) {
				if(response[i].isselected){
					$("#fieldId").find('tbody').append(
								$('<tr>').append(
										$('<td>').append(
												'<input class="check" name="select_fields" type="checkbox" checked="checked" value='
														+ response[i].name + '>')
												.append($('</td>'))).append(
										$('<td>').text(response[i].name).append(
												$('</td>'))));
				}else{
					$("#fieldId").find('tbody').append(
							$('<tr>').append(
									$('<td>').append(
											'<input class="check" name="select_fields" type="checkbox" value='
													+ response[i].name + '>')
											.append($('</td>'))).append(
									$('<td>').text(response[i].name).append(
											$('</td>'))));
				}
						
				}
				//$("#field_grid").show();
				
				
			}
	});
}
	
function getschemaforPrim(col_name) {
	$.ajax({
		type : "POST",
		url : "getManageReportFields.html",
		data : "collection=" + encodeURIComponent(col_name)+"&flag=Primary",
		success : function(response) {
			$("#primaryFields").empty();
			for ( var i = 0; i < response.length; i++) {
				 $("#primaryFields").append('<option value="'+response[i].name+'">'+response[i].name+'</option>');
			}
			
		}
	});
}

function getschemaforSec(col_name) {
	$.ajax({
		type : "POST",
		url : "getManageReportFields.html",
		data : "collection=" + encodeURIComponent(col_name)+"&flag=Secondary",
		success : function(response) {
				$("#secFieldId").find("tr:gt(0)").remove();
				for ( var i = 0; i < response.length; i++) {
					if(response[i].issecondary){
						$("#secFieldId").find('tbody').append(
								$('<tr>').append(
										$('<td>').append(
												'<input class="check" name="select_Sec_fields" checked="checked" type="checkbox" value='
														+ response[i].name + '>')
												.append($('</td>'))).append(
										$('<td>').text(response[i].name).append(
												$('</td>'))));
					}
					else{
						$("#secFieldId").find('tbody').append(
								$('<tr>').append(
										$('<td>').append(
												'<input class="check" name="select_Sec_fields" type="checkbox" value='
														+ response[i].name + '>')
												.append($('</td>'))).append(
										$('<td>').text(response[i].name).append(
												$('</td>'))));
					}
						
				}
		}
		});
}

