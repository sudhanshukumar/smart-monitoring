/*$(document).ready(function(){
	//createpiechart();
	createmetrictable();
	//createwindowsmachinediv();
});
function createpiechart(){
	$.ajax({
	    type: "GET",
	    url: "pieChart.html",
	    success: function(response){
	  	  var result = response.toString();
	  	  var xAxisData=[];
	  	  var objList=JSON.parse(result);
	  	  var series1 = [];
	  	  var yAxisArray = [];
	  	  for(b=0;b<objList.length;b++){
						var actualObject=objList[b];
						series1.push([actualObject["alertType"],actualObject["alertValue"]]);
	  	  }
	  	  
	  	var chart = c3.generate({
		    data: {
		        // iris data from R
		        columns: series1,
		        type : 'pie',
		        onclick: function(d, element) {
	    	        chart.internal.config.tooltip_show = true;
	    	        chart.internal.showTooltip([d], element);
	    	        chart.internal.config.tooltip_show = false;
	    	    }
		    },
		    label : {
		    	show : false
		    },
		    tooltip : {
		    	show: true
		    },
		    legend: {
		         show: true
		    },
		    size: {
		        width: 200,
		        height: 200
		    },
		    label: {
		       format: function (value, ratio, id) {
		          return d3.format('$')(value);
		        }
		  }
		});
		d3.select('#chart').insert('div', '.chart').attr('class', 'legend').selectAll('span')
		   	.data([])
		    .enter().append('span')
		    .attr('data-id', function (id) { return id; })
		    .html(function (id) { return id; })
		    .each(function (id) {
		      //  d3.select(this).style('background-color', chart.color(id));
		    })
		    .on('mouseover', function (id) {
		        chart.focus(id);
		    })
		    .on('mouseout', function (id) {
		        chart.revert();
		    })
		    .on('click', function (id) {
		        chart.toggle(id);
		    });
	  	  
	    }
	});
	}
function createmetrictable(){
	$.ajax({
	    type: "GET",
	    url: "machineInfo.html",
	    success: function(response){
	    	var result = response.toString();
	    	var objList=JSON.parse(result);
	    	var actualObject=objList[0];
	    	var customers = new Array();
	    	customers.push(["Total Machines Monitored : "+actualObject["noofmachines"]]);
	    	customers.push(["Windows OS Machines :  "+"1"]);
	    	customers.push(["Linux OS Machines : "+"0"]);
	    	
	    //Create a HTML Table element.
	    var table = document.createElement("TABLE");
	    table.border = "2";
	   
	   // var table = document.getElementById("MetricTable");
	 
	    //Get the count of columns.
	    var columnCount = customers[0].length;
	 
	    //Add the header row.
	    var row = table.insertRow(-1);
	    for (var i = 0; i < columnCount; i++) {
	        var headerCell = document.createElement("TH");
	        headerCell.innerHTML = '<h5>'+customers[i]+'</h5>';
	        headerCell.style.color = 'green';
	        row.appendChild(headerCell);
	        
	    }
	 
	    //Add the data rows.
	    for (var i = 1; i < customers.length; i++) {
	    	var row = table.insertRow(-1);
	        //for (var j = 0; j < columnCount; j++) {
	            var cell = row.insertCell(-1);
	            cell.innerHTML = '<h5>'+customers[i]+'</h5>';
	            cell.style.color = "olive";
	       //}
	    }
	 
	    var dvTable = document.getElementById("tvalue");
	    dvTable.innerHTML = actualObject["total"];
	    var dvTable = document.getElementById("ttext");
	    dvTable.innerHTML = "Machines";
	    var dvTable = document.getElementById("wvalue");
	    dvTable.innerHTML = actualObject["windows"];
	    var dvTable = document.getElementById("wtext");
	    dvTable.innerHTML = "Windows Box";
	    var dvTable = document.getElementById("lvalue");
	    dvTable.innerHTML = actualObject["linux"];
	    var dvTable = document.getElementById("ltext");
	    dvTable.innerHTML = "Linux Box";
	    var criticalAlertValue = document.getElementById("cavalues");
		criticalAlertValue.innerHTML="0";
		var criticalText=document.getElementById("catext");
		criticalText.innerHTML="Critical Alerts";
		var criticalAlertValue = document.getElementById("wavalues");
		criticalAlertValue.innerHTML="0";
		var criticalText=document.getElementById("watext");
		criticalText.innerHTML="Warning Alerts";
	    dvTable.appendChild(table);
	    
	    }
	});
	
	function createwindowsmachinediv(){
		$.ajax({
		    type: "GET",
		    url: "windowsMac.html",
		    success: function(response){
		    	var result = response.toString();
		    	var objList=JSON.parse(result);
		    	var actualObject=objList[0];
		    	var customers = new Array();
		    	customers.push(["Total Machines Monitored : "+actualObject["noofmachines"]]);
		    	customers.push(["Windows OS Machines :  "+"1"]);
		    	customers.push(["Linux OS Machines : "+"0"]);
		    	
		    //Create a HTML Table element.
		    var table = document.createElement("TABLE");
		    table.border = "2";
		   
		   // var table = document.getElementById("MetricTable");
		 
		    //Get the count of columns.
		    var columnCount = customers[0].length;
		 
		    //Add the header row.
		    var row = table.insertRow(-1);
		    for (var i = 0; i < columnCount; i++) {
		        var headerCell = document.createElement("TH");
		        headerCell.innerHTML = '<h5>'+customers[i]+'</h5>';
		        headerCell.style.color = 'green';
		        row.appendChild(headerCell);
		        
		    }
		 
		    //Add the data rows.
		    for (var i = 1; i < customers.length; i++) {
		    	var row = table.insertRow(-1);
		        //for (var j = 0; j < columnCount; j++) {
		            var cell = row.insertCell(-1);
		            cell.innerHTML = '<h5>'+customers[i]+'</h5>';
		            cell.style.color = "olive";
		       //}
		    }
		 
		    var dvTable = document.getElementById("windowsmachines");
		    dvTable.innerHTML = result +" "+"Windows Box";
		    dvTable.appendChild(table);
		    
		    }
		});
	}
}*/