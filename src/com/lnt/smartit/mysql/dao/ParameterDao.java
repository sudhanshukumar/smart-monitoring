package com.lnt.smartit.mysql.dao;

import java.util.List;

import com.lnt.smartit.mysql.model.ParameterMaster;

public interface ParameterDao {
	public List<ParameterMaster> getParameters();
	public ParameterMaster getParameterMasterById(String id);
	public String getParameterMasterByParamName(String name);
}
