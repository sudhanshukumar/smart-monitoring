package com.lnt.smartit.models;


public class CEDErrorLog {
	private String errorDate;
	private String errorCode;
	private String errorMessage;
	private String hostName;
	
	public String getErrorDate() {
		return errorDate;
	}
	public void setErrorDate(String errorDate) {
		this.errorDate = errorDate;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	@Override
	public String toString() {
		return "CEDErrorLog [errorDate=" + errorDate + ", errorCode="
				+ errorCode + ", errorMessage=" + errorMessage + ", hostName="
				+ hostName + "]";
	}
}
