package com.lnt.smartit.controllers;

import static com.lnt.smartit.util.Util.saveAuditCreation;
import static com.lnt.smartit.util.Util.saveAuditUpdation;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lnt.smartit.mysql.model.DeviceSoftwareMapping;
import com.lnt.smartit.mysql.model.SoftwareMaster;
import com.lnt.smartit.services.SoftwareMasterService;
import com.lnt.smartit.solr.pojo.JqGridData;

@Controller
public class ServiceMasterController {

	@Autowired
	private SoftwareMasterService softwareMasterService; 
	private static Logger logger = Logger.getLogger(NotificationGroupController.class);

	/**
	 * @author 298038 addNewService redirects to add new Service
	 *         Page
	 * */
	@RequestMapping("addNewService.html")
	public String addNewService(ModelMap map) {
		return "Manage/new/newService";
	}

	/**
	 * @author 298038
	 * manageService redirects to add manageService page
	 * */
	@RequestMapping("manageService.html")
	public String manageService(ModelMap map) {
		logger.debug("Inside controller ..........");
		return "Manage/manageService";
	}

	@RequestMapping(value = "populateServiceGrid.html", method = RequestMethod.POST)
	public @ResponseBody JqGridData populateServiceGrid(HttpServletRequest request,JqGridData gridPojo) {
		System.out.println("Entering ServiceMasterController-->populateServiceGrid (POST)");
		List<SoftwareMaster> softwareList = new ArrayList<SoftwareMaster>();
		try {
			softwareList = softwareMasterService.getAllSoftwareMaster();
			for (SoftwareMaster software : softwareList) {
				logger.debug(software);
			}
			gridPojo.setGridData(softwareList.toArray());
			gridPojo.setRows(softwareList.size());
			gridPojo.setRecords(softwareList.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return gridPojo;
	}

	/*@SuppressWarnings("null")
	@RequestMapping(value = "addServiceOnSubmit.html", method = RequestMethod.POST)
	public @ResponseBody String addServiceOnSubmit(@Valid SoftwareMaster softwareMaster,BindingResult bindingResult,
			HttpSession session,HttpServletRequest request) {

		String serviceMsg = null;
		logger.debug("Entering ServiceMasterController-->addServiceOnSubmit()");
		//TODO needs refactoring since the pojos are copied from the latest version. getHostname()
		if (!(!(softwareMaster.getSoftwareName().isEmpty()) && softwareMaster.getSoftwareName() != null) || (!(!(softwareMaster.getHostName().isEmpty()) && softwareMaster.getHostName() != null))){
			serviceMsg = "Please fill all the mandatory fields!<br />";
		}
		else 
		{
			Integer softwareId;
			String softId = request.getParameter("softId");
			logger.debug("softwareMaster " + softwareMaster.toString());
			if (null == softId || softId.equalsIgnoreCase("null")) 
			{
				softwareId = ((int) (Math.random() * 1000));
				saveAuditCreation(softwareMaster);
				softwareMaster.setSoftwareId(softwareId.toString());
				try {
					List<SoftwareMaster> sftMaster = softwareMasterService.checkIfExists(softwareMaster);
					if(sftMaster==null || (sftMaster.isEmpty()))
					{
						softwareMasterService.saveNewService(softwareMaster);
						serviceMsg = "New service details have been saved Successfully !! ";
					}
					else
					{
						serviceMsg = "Record with same Host name & Service name already exists...!!";
					}
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
					serviceMsg = "New service details save failed due to "
						+ e.getMessage();
				}
			} 
			else 
			{
				softwareMaster.setSoftwareId(softId);
				SoftwareMaster softMaster = softwareMasterService.getApplicationMasterById(softwareMaster);
				List<DeviceSoftwareMapping> deviceSoftwareMappingList = softwareMasterService.getDeviceSoftwareMappingById(softId);
				if(deviceSoftwareMappingList!=null || !(deviceSoftwareMappingList.isEmpty())){
				softwareMasterService.deleteDeviceSoftwareMapping(deviceSoftwareMappingList);
				softwareMasterService.deleteService(softMaster);
				}
				saveAuditUpdation(softwareMaster);
				String hostName=softwareMaster.getHostName();
				String softwareName=softwareMaster.getSoftwareName();
				String softwareDescription = softwareMaster.getDescription();
				System.out.println(hostName+".........."+softwareName);
				softwareMaster=softwareMasterService.getServiceRecordById(softwareMaster);
				if ((hostName.equalsIgnoreCase(softwareMaster.getHostName())) && (softwareName.equalsIgnoreCase(softwareMaster.getSoftwareName())))
				{
					softwareMaster.setCreatedBy(softMaster.getCreatedBy());
					softwareMaster.setCreationDate(softMaster.getCreationDate());
					softwareMaster.setHostName(softwareMaster.getHostName());
					softwareMaster.setDescription(softwareDescription);
					softwareMaster.setSoftwareName(softMaster.getSoftwareName());
				}
				else
				{
					softwareMaster.setHostName(hostName);
					softwareMaster.setSoftwareName(softwareName);
					if(softwareMasterService.exists(softwareMaster))
					{
						serviceMsg = "Record with same Host name & Log file name already exists...!!";
						return serviceMsg;
					}
					else{
						softwareMaster.setCreatedBy(softMaster.getCreatedBy());
						softwareMaster.setCreationDate(softMaster.getCreationDate());
						softwareMaster.setHostName(softwareMaster.getHostName());
						softwareMaster.setDescription(softwareDescription);
						softwareMaster.setSoftwareName(softwareName);
					}
				}
				try {
					softwareMasterService.saveNewService(softwareMaster);
					softwareMasterService.saveDeviceSoftwareMapping(deviceSoftwareMappingList);
					serviceMsg = "New service details have been updated Successfully !! ";
				} catch (Exception e) {
					e.printStackTrace();
					serviceMsg = "New service details update failed due to "
						+ e.getMessage();
				}
			}

			return serviceMsg;
		}
		return serviceMsg;
	}*/

	/**
	 * @author 298038
	 * @param SoftwareMaster
	 * @return
	 */ 
	@RequestMapping(value = "deleteService.html", method = RequestMethod.POST)
	public @ResponseBody String deleteService(SoftwareMaster softwareMaster){
		logger.debug("Entering ServiceMasterController-->deleteService()");
		logger.debug("softwareMaster "+softwareMaster);
		SoftwareMaster softMaster;
		String deleteServiceMsg;
		try{
			softMaster=softwareMasterService.getApplicationMasterById(softwareMaster);
			deleteServiceMsg=softwareMasterService.deleteService(softMaster);
		}catch(Exception e ){
			e.printStackTrace();
			deleteServiceMsg="Delete Service failed due to "+e.getMessage();
		}
		return deleteServiceMsg;
	}

	/**
	 * @author 298038
	 * @param softwareMaster
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "editService.html", method = RequestMethod.POST)
	public @ResponseBody SoftwareMaster editService(SoftwareMaster softwareMaster,HttpServletRequest request) {
		logger.debug("Entering ServiceMasterController-->editService()");
		logger.debug("softwareMaster "+softwareMaster);
		SoftwareMaster softMaster;
		softMaster=softwareMasterService.getApplicationMasterById(softwareMaster);
		logger.debug(softMaster);
		return softMaster;
	}

	@RequestMapping(value = "previewService.html", method = RequestMethod.POST)
	public @ResponseBody SoftwareMaster previewService(SoftwareMaster softwareMaster,HttpServletRequest request) {
		logger.debug("Entering ApplicationMasterController-->previewService()");
		logger.debug("SoftwareMaster "+softwareMaster);
		SoftwareMaster softMaster=softwareMasterService.getApplicationMasterById(softwareMaster);
		logger.debug(softMaster);
		return softMaster;
	}
}