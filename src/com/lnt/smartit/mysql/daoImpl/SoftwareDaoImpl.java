package com.lnt.smartit.mysql.daoImpl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lnt.smartit.helper.HbmHelper;
import com.lnt.smartit.mysql.dao.SoftwareDao;
import com.lnt.smartit.mysql.model.SoftwareMaster;

@Repository
@Transactional
public class SoftwareDaoImpl implements SoftwareDao{

	@Autowired
	private HbmHelper hbmHelper;
	private static Logger logger = Logger.getLogger(SoftwareDaoImpl.class);
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public List<SoftwareMaster> getAllSoftwareMaster() {
		logger.debug(" in SoftwareMasterService --> getAllSoftwareMaster()");
		List<SoftwareMaster> softwareList = (List<SoftwareMaster>) hbmHelper.createQuery("from SoftwareMaster", null);
		logger.debug("SoftwareMaster "+  softwareList.toString());
		return softwareList;
	}

	public void saveNewService(SoftwareMaster softwareMaster) {
		try {
			hbmHelper.saveOrUpdate(softwareMaster);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public SoftwareMaster getApplicationMasterById(SoftwareMaster softMaster) {
		SoftwareMaster softwareMaster = new SoftwareMaster();
		softwareMaster= (SoftwareMaster) hbmHelper.findById(SoftwareMaster.class,softMaster.getSoftwareId() );
		logger.debug(softwareMaster);
		return softwareMaster;
	}

	public String deleteService(SoftwareMaster softMaster) {
		String deleteServiceMsg;
		try{
			hbmHelper.delete(softMaster);
			deleteServiceMsg="Service deleted successfully!!";
		}catch (Exception e) {
			e.printStackTrace();
			deleteServiceMsg="Service deletion unsuccessfull due to "+e.getMessage();
		}
		return deleteServiceMsg;
	}
	/**
	 * To check if service and host name already exists
	 */
	@SuppressWarnings("unchecked")
	public List<SoftwareMaster> checkIfExists(SoftwareMaster softMaster){
		Session session = sessionFactory.getCurrentSession();
		Query query = null;
		List<SoftwareMaster> str =null;
		try{
			//TODO needs refactoring since the pojos are copied from the latest version.
			//query = session.createSQLQuery("Select * from software_master s where s.software_name='"+softMaster.getSoftwareName()+"' and s.host_name='"+softMaster.getHostName()+"';");
			System.out.println(query);
			str= query.list();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			session.flush();
		}
		return str;
	}
/**
 * To get all the service details by service id
 */
	@Override
	public SoftwareMaster getServiceById(SoftwareMaster softwareMaster) {
		SoftwareMaster softwareRec =null;
		try {
			System.out.println(softwareMaster+"---- dao");
			Session session  = sessionFactory.getCurrentSession();
			String queryString = "from SoftwareMaster s where s.softwareId = '"+softwareMaster.getSoftwareId()+"'";
			Query query = session.createQuery(queryString);
			softwareRec=(SoftwareMaster) query.uniqueResult();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		logger.debug(softwareRec);
		return softwareRec;
	}

	/**
	 * To check if service and host name exists
	 */
	@Override
	public boolean exists(SoftwareMaster softwareMaster) {

		try {
			Session session = sessionFactory.getCurrentSession();
			String queryString = "from SoftwareMaster s where "
					+ "s.hostName = '" //+softwareMaster.getHostName() //TODO needs refactoring since the pojos are copied from the latest version.
					+"' and s.softwareName = '"+softwareMaster.getSoftwareName()+"'";
			Query query = session.createQuery(queryString);
			if(query.list().isEmpty())
			{
				return false;
			}
			else
			{
				return true;
			}
		} catch (HibernateException e) {
			e.printStackTrace();
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			return true;
		}
	}
}
