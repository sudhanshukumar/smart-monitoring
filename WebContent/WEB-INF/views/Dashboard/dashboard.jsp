<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
   <%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title> Dashboard Page</title>
<link rel="shortcut icon" type='image/x-icon' href="favicon.ico" />
<!-- Common CSS files of Bootstrap and JQuery -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="assets/css/bootstrap-combined.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/common/ui.jqgrid.css" />
<link rel="stylesheet" type="text/css" href="css/common/jquery-ui.min.css" />
<link href="scripts/c3-0.4.10/c3.css" rel="stylesheet" type="text/css" charset="utf-8">

<!-- Custom CSS files for this Page -->
<link rel="stylesheet" href="css/custom/specific/nav.css">
<link rel="stylesheet" href="css/custom/specific/dashboard.css">

<!-- Common Scripts of Bootstrap and JQuery -->
<script src="scripts/common/jquery-2.2.3.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="scripts/common/jquery.jqGrid.min.js"></script>
<script src="scripts/common/grid.locale-en.js"></script>
<script src="scripts/common/jquery-ui.js"></script>

<!-- C3 scripts -->
<script src="scripts/d3-master/d3.js" charset="utf-8"></script>
<script src="scripts/c3-0.4.10/c3.js" charset="utf-8"></script>

<!-- Custom scripts for this Page -->
<script src="scripts/custom/dashboardgrid.js"></script>
<script src="scripts/custom/utility.js"></script>
<script src="scripts/custom/tab_menu.js"></script>

<!--Added by vandana start  -->
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/plugins/jqplot.json2.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/jquery.jqplot.min.css"/>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/plugins/jqplot.dateAxisRenderer.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/plugins/jqplot.cursor.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/plugins/jqplot.highlighter.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/plugins/jqplot.dragable.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/plugins/jqplot.trendline.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/plugins/jqplot.meterGaugeRenderer.min.js"></script>
<!-- <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/d3/3.4.11/d3.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/c3/0.1.29/c3.js"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/c3/0.1.29/c3.css" rel="stylesheet" type="text/css">
<script src="scripts/custom/donutchart.js"></script>
<script src="scripts/custom/jsgrid.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqgrid/4.6.0/js/i18n/grid.locale-en.js"></script>
<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.css" />
<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid-theme.min.css" />
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script> -->
<script src="scripts/custom/piechart.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/plugins/jqplot.pieRenderer.min.js"></script> -->

<% String session_val = (String)session.getAttribute("modules"); %>

<script type="text/javascript">
	session_obj= '<%=session_val%>';
</script>

</head>
<body>
	<div class="container-fluid">
	  <%@ include file="../common/header.jsp"%>
  
	 <nav class="navbar navbar-inverse" style="margin-top: 0px">
	      <ul class="nav navbar-nav navbar-xs">
	        <li class="active" id="Dashboard_ELK"><a href="dashboard.html">Dashboard<span class="sr-only">(current)</span></a></li>
	        <li id="ManageThreshold_ELK"><a href="manageThresHold.html">Manage Rule</a></li>
	        <!-- <li id="AlertHistory"><a href="problemHistory.html">Alert History</a></li> -->
	        <li class="dropdown" id="Insights">
	          <a href="#" class="dropdown-toggle" id="Insights" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Insights<span class="caret"></span></a>
	          <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
	            <li id="Analytics"><a href="query.html" style="font-size:11px!important;">Analytics</a></li>
	           <!--  <li><a href="kibanaDashboard.html" style="font-size:11px!important;">Prediction</a></li> -->
	            <!-- <li><a href="variableAnalysis.html">Variable Analysis</a></li> -->
	            <li class="dropdown-submenu" id="Reports"> <a tabindex="-1" href="#" style="font-size:11px!important;">Reports</a>
                      <ul class="dropdown-menu">
							<li id="Prediction">
								<a href="predictionreport.html" style="font-size:11px!important;">Prediction</a> </li>
							<li id="Trends">
								<a href="trendingreport.html" style="font-size:11px!important;">Trends</a> </li>
							<li id="Alert_History">
							 	<a href="alerthistoryreport.html" style="font-size:11px!important;">Alert History</a> </li>
							<li id="LogCorrelation">
								<a href="correlationreport.html" style="font-size:11px!important;">Log Correlation</a> </li>
							<li id="WeblogicReport">
								<a href="weblogicreport.html" style="font-size:11px!important;">Weblogic Report</a> </li>
							<li id="WeblogicDrillDownReport">
								<a href="weblogicdrilldownreport.html" style="font-size:11px!important;">Weblogic DrillDown Report</a> </li>
							<li id="SapApplicationReport">
								<a href="sapapplicationreport.html" style="font-size:11px!important;">Sap Application Report</a> </li>
						    <li id="SapApplicationAdoption">
                           <a href="sapapplicationadoption.html" style="font-size:11px!important;">Sap Application Adoption</a> </li>
                            <li id ="SapPlatformOperation">
                           <a href="sapplatformoperation.html" style="font-size:11px!important;">Sap Platform Operations</a> </li>
                           <li id ="SapSecurityAuditing">
                           <a href="sapsecurityauditing.html" style="font-size:11px!important;">Sap Security Auditing</a> </li>
						</ul>
                    </li>
	          </ul>
	        </li>
	        <!-- <li class="active" id="LogAnalysis"><a href="query.html">Insights</a></li> -->
	        <%-- <li id="LogCorrelation"><a href="correlation.html">Log Correlation</a></li>
	        <li id="VariableAnalysis"><a href="variableAnalysis.html"><spring:message code="label.admin.menu5"></spring:message></a></li>
	        <li class="dropdown" id="Reports">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reports <span class="caret"></span></a>
	          <ul class="dropdown-menu">
	            	<li id="CreateReport"><a href="charts.html">Create Report</a></li>
		            <li id="CreateDashboard"><a href="createuserdashboard.html">Create Dashboard</a></li>
		            <li id="ViewDashboard"><a href="list_dashboard.html">View Dashboard</a></li>
	          </ul>
	        </li> --%>
	      </ul>
	  </nav>
	 <div> <!-- style="height: calc(100% - 120px);" -->
	  <div style="height : 150px; width : 100%; overflow: hidden;">
 	  	<!-- <div class="chart dashboard-chart" id="donutchart"style="height : 220px; width : 35%;float: left; position : relative"></div>
	  	<div class="grid dashboard-grid" id="donutgrid"> 
	  		<table id="JsGrid"></table><div id="pager1"></div></div>
	  	<div class="chart dashboard-piechart" id="chart" style="height : 220px; width : 30%; float: left;position : relative"> style="min-width: 310px; height: 220px; max-width: 500px;  float: left;"</div>-->
	  	<!-- <div class="chart dashboard-metricchart" id="metric" style="height :220px;width : 35%; float:right; position : relative; padding-top : 50px"></div>  -->
	  
	  	<div class="tmclass" id="totalmachines">
	  		<div class="tmvalueclass" id=tvalue></div>
	  		<div class="tmtextclass" id=ttext></div>
	  	</div>
	  	<div class="wmclass" id="windowsmachines">
	  		<div class="wmvalueclass" id=wvalue></div>
	  		<div class="wmtextclass" id=wtext></div>
	  	</div>
	  	<div class="lmclass" id="linuxmachines">
	  		<div class="wmvalueclass" id=lvalue></div>
	  		<div class="wmtextclass" id=ltext></div>
	  	</div>
	  	<div class="criticalclass" id="criticalalerts">
	  		<div class="cavaluesclass" id=cavalues></div>
	  		<div class="catextclass" id=catext></div>
	  	</div>
	  	<div class="warningclass" id="warningalerts">
	  		<div class="wavaluesclass" id=wavalues></div>
	  		<div class="watextclass" id=watext></div>
	  	</div>
	  
	  	
	  </div>
	  	
		 <div class="alert alert-success" role="alert" id="healthyMsg">All Systems / Applications are healthy !!!</div> 
	  	
		<div class="panel panel-body">
			<table id="dashboardGrid" class="table"></table>
	 	</div> 
	 <!-- <div id="TrendWindow">
	 	<iframe src="http://10.103.20.64:5601/app/kibana#/visualize/edit/CPU-12-hours-trend?_g=(filters:!(),refreshInterval:(display:Off,pause:!f,value:0),time:(from:now-12h%2Fh,mode:relative,to:now))&_a=(filters:!(),linked:!f,query:(query_string:(analyze_wildcard:!t,query:'*')),uiState:(),vis:(aggs:!((id:'1',params:(customLabel:'CPU_Usage%20in%20%25',field:CPU_USAGE),schema:metric,type:max),(id:'2',params:(customInterval:'2h',extended_bounds:(),field:logDate,interval:auto,min_doc_count:1),schema:segment,type:date_histogram)),listeners:(),params:(addLegend:!t,addTimeMarker:!f,addTooltip:!t,defaultYExtents:!f,drawLinesBetweenPoints:!t,interpolate:linear,radiusRatio:9,scale:linear,setYExtents:!f,shareYAxis:!t,showCircles:!t,smoothLines:!f,times:!(),yAxis:()),title:'CPU%2012-hours%20trend',type:line))" style="height:300px; width:990px; ">
	 	</iframe>
	 </div> -->
	</div>
		<%@ include file="../common/footer.jsp"%>
	</div>
</body>
</html>