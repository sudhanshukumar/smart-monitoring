package com.lnt.smartit.mysql.daoImpl;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lnt.smartit.mysql.dao.ProblemHistoryDao;
import com.lnt.smartit.mysql.model.Alertmessageinfo;
import com.lnt.smartit.solr.pojo.JqGridData;
import com.lnt.smartit.util.DatabaseConfig;
import com.lnt.smartit.util.HibernatePageIterator;

@Repository
@Transactional
public class ProblemHistoryDaoImpl implements ProblemHistoryDao{
	private SessionFactory sessionFactory;
	private static Logger logger = Logger.getLogger(ProblemHistoryDaoImpl.class);

/**************************Code change done by Pinakin on 21st August starts here****************************/
	/* This method will get only required no of records from DB
	 * based on pageNo and pageRowCount passed.
	 * @param pageRowCount - No of records to be displayed on single page (Page window size)
	 * @param cache - Can be used for caching the query. 
	 * @param pageNo - Page No for which we want to get the data *********
	 * *************/
	@SuppressWarnings("unchecked")
	public List<Alertmessageinfo> getProblemHistory(Integer pageNo, Integer pageRowCount, boolean cache) {
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		List<Alertmessageinfo> list = null;
		try {
			String queryString = "select a.amId, a.amHostname, a.applicationName, a.amParametername, a.amType, a.amDatetime, a.description from Alertmessageinfo a order by a.amDatetime desc";
			Query query = session.createQuery(queryString);
			HibernatePageIterator hpi = HibernatePageIterator.createInstance("alertHistory", pageRowCount, query, cache);
			List<Object []> list1 = hpi.list(pageNo);
			list = new ArrayList<Alertmessageinfo>(pageRowCount);
			if(list1.size() > 0){
				for(Object[] row :list1)
				{
					Alertmessageinfo alertRow = new Alertmessageinfo();
					alertRow.setAmId((Integer) row[0]);
					alertRow.setAmHostname((String) row[1]);
					alertRow.setApplicationName((String) row[2]);
					alertRow.setAmName((String) row[3]);
					alertRow.setAmType((String) row[4]);
					alertRow.setAmDatetime((String) row[5]);
					alertRow.setDescription((String) row[6]);
					list.add(alertRow);
				}
				logger.debug("ID of first element for the page No : "+pageNo+" is : "+list.get(0).getAmId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			//session.close();
			tx.commit();
		}
		return list;
	}
	
	/*
	 * This method is for getting the toal no of records present in a table.
	 * This will help us while calculating last page No. 
	 */
	public Integer getProblemHistroyRecordsCount(){
		Integer count = 0;
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		try {
			Criteria criteria = session.createCriteria(Alertmessageinfo.class);
			count = ((Long) criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		tx.commit();
		return count;
	}
	/**************************Code change done by Pinakin on 21st August ends here****************************/
	
	@Override
	@SuppressWarnings("unchecked")
	public JqGridData getProblemHistoryWithCriteria(Integer pageNo, Integer pageRowCount, boolean cache, String app, String hostname, String parameter, String alertType, String fromDate,String toDate) {
		/*logger.debug("ProblemHistoryDaoImpl :: getProblemHistoryWithCriteria :: pageNo="+pageNo+" rowcount="+pageRowCount+" hostname="+hostname+" parameter="+parameter+" alertType="+alertType+" fromDate="+fromDate+" toDate="+toDate);
		JqGridData jqGridData = new JqGridData();
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		Criteria cr = session.createCriteria(Alertmessageinfo.class)
				.setProjection(Projections.projectionList()
		        .add( Projections.property("amId") )
		        .add( Projections.property("amHostname") )
		        .add( Projections.property("applicationName") )
		        .add( Projections.property("amParametername") )
		        .add( Projections.property("amType") )
		        .add( Projections.property("amDatetime") )
		        .add( Projections.property("description") )
		        );
		System.out.println(fromDate + " " + toDate);
		if(app!=null && !app.equals("null") &&  !app.equals(""))
			cr.add(Restrictions.like("applicationName", app));
		if(hostname != null && !hostname.equals("null") && !hostname.equals(""))
			cr.add(Restrictions.like("amHostname", "%"+hostname+"%"));
		if(parameter != null && !parameter.equals("null") && !parameter.equals(""))
			cr.add(Restrictions.eq("amParametername",parameter));
		if(alertType != null && !alertType.equals("null") && !alertType.equals(""))
			cr.add(Restrictions.eq("amType",alertType));
		if((fromDate!= null && !fromDate.equals("")) || (toDate!= null && !toDate.equals(""))){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			try {
				cr.add(Restrictions.between("amDatetime", sdf.format(sdf.parseObject(fromDate)), sdf.format(sdf.parseObject(toDate))));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		cr.addOrder(Order.desc("amDatetime"));
		logger.debug("ProblemHistoryDaoImpl :: getProblemHistoryWithCriteria :: Criteria ="+cr.toString());
		System.out.println("Creteria == "+cr);
		List<Alertmessageinfo> list = null;
		try {
			// START -- Code changes done by Sayali on 24/02/16 for AlertHistory loading issue 
			System.out.println("getting query result = "+System.currentTimeMillis());
//			HibernatePageIterator hpi = HibernatePageIterator.createInstance("alertHistory", pageRowCount, cr, cache);
//			List<Object []> list1 = hpi.list(pageNo);
			cr.setFirstResult((pageNo-1)*pageRowCount);
			cr.setMaxResults(pageRowCount);
			List<Object[]> list1 = cr.list();
			System.out.println("got query result = "+System.currentTimeMillis());
			// END -- Code changes done by Sayali on 24/02/16 for AlertHistory loading issue 
			logger.debug("ProblemHistoryDaoImpl :: getProblemHistoryWithCriteria :: Results found ="+list1.size());
			list = new ArrayList<Alertmessageinfo>(pageRowCount);
			if(list1.size() > 0){
				for(Object[] row :list1)
				{
					Alertmessageinfo alertRow = new Alertmessageinfo();
					alertRow.setAmId((Integer) row[0]);
					alertRow.setAmHostname((String) row[1]);
					alertRow.setApplicationName((String) row[2]);
					alertRow.setAmName((String) row[3]);
					alertRow.setAmType((String) row[4]);
					alertRow.setAmDatetime((String) row[5]);
					alertRow.setDescription((String) row[6]);
					list.add(alertRow);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			tx.commit();
		}
		Integer totalRecords = getProblemHistroyRecordsCount(app,hostname, parameter, alertType, fromDate,toDate);
		logger.debug("ProblemHistoryDaoImpl :: getProblemHistoryWithCriteria :: Total records = "+totalRecords);
		int lastPageNo = 0;
		if(totalRecords%pageRowCount == 0){
			lastPageNo = (totalRecords/pageRowCount);
		}
		else{
			lastPageNo = (totalRecords/pageRowCount)+1;
		}
		jqGridData.setGridData(list.toArray());
		jqGridData.setTotal(lastPageNo);
		jqGridData.setRecords(totalRecords);
		jqGridData.setPage(pageNo);
		jqGridData.setRows(pageRowCount);
		return jqGridData;*/
		
		JqGridData jqGridData = new JqGridData();
		TransportClient client = null;
		/*try {
			client = TransportClient.builder().build().addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("INFARSZC90433"), 9300));
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		SearchRequestBuilder searchRequestBuilder = client.prepareSearch()
	            .setIndices("alerts")
	            .setTypes("alerts")            //.setQuery(QueryBuilders.termQuery("IP_Address", "10.103.20.64"))
	            .setScroll(new TimeValue(60000)).setSize(100);
	            //.setPostFilter(QueryBuilders.rangeQuery("logdate").from("2016-10-07T10:30:00.000Z").to("2016-10-07T18:29:59.999Z"));
		System.out.println(searchRequestBuilder);
	    SearchResponse response = searchRequestBuilder.execute().actionGet();
	    System.out.println("Response ==  "+response);
	    SearchHits searchHits = response.getHits();
	    List<Object> list = new ArrayList<Object>();
	    for(SearchHit hit : searchHits){
	    	list.add(hit.getSource());
	    }
	    System.out.println(list);
	    jqGridData.setGridData(list.toArray());
		jqGridData.setTotal(1);
		jqGridData.setRecords(2);
		jqGridData.setPage(pageNo);
		jqGridData.setRows(pageRowCount);*/
	    return jqGridData;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getDistinctHostnames(){
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		Query query = session.createQuery("Select distinct a.amHostname from Alertmessageinfo a");
		List<String> list = query.list();
		List<String> hostnameList = new ArrayList<String>();
		for(String type : list){
			if(type != null){
				hostnameList.add(type);
			}
		}
		logger.debug("ProblemHistoryDaoImpl :: getDistinctHostnames :: Hostnames = "+hostnameList.toString());
		tx.commit();
		return hostnameList;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getDistinctParameters(){
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		Query query = session.createQuery("Select distinct a.amParametername from Alertmessageinfo a");
		List<String> list = query.list();
		List<String> parameterList = new ArrayList<String>();
		for(String type : list){
			if(type != null){
				parameterList.add(type);
			}
		}
		logger.debug("ProblemHistoryDaoImpl :: getDistinctParameters :: Parameters = "+parameterList.toString());
		tx.commit();
		return parameterList;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<String> getDistinctAlerttypes(){
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		Query query = session.createQuery("Select distinct a.amType from Alertmessageinfo a");
		List<String> list = query.list();
		List<String> alerttypesList = new ArrayList<String>();
		for(String type : list){
			if(type != null){
				alerttypesList.add(type);
			}
		}
		logger.debug("ProblemHistoryDaoImpl :: getDistinctAlerttypes :: alert types = "+alerttypesList.toString());
		tx.commit();
		return alerttypesList;
	}

	@Override
	public void connectDatabase() {
		logger.debug("ProblemHistoryDaoImpl :: connectDatabase :: "+DatabaseConfig.sqlUrl + DatabaseConfig.userName  + DatabaseConfig.password);
		Configuration configuration = new Configuration();
		configuration.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5InnoDBDialect");
		configuration.setProperty("hibernate.current_session_context_class", "thread");
		configuration.setProperty("hibernate.connection.url", DatabaseConfig.sqlUrl);
		configuration.setProperty("hibernate.connection.username", DatabaseConfig.userName);
		configuration.setProperty("hibernate.connection.password", DatabaseConfig.password);
		configuration.addAnnotatedClass(Alertmessageinfo.class);
		sessionFactory = configuration.buildSessionFactory();
		logger.debug("ProblemHistoryDaoImpl :: connectDatabase :: Session factory created");
	}
	
	@Override
	public Integer getProblemHistroyRecordsCount(String app, String hostname, String parameter, String alertType, String fromDate, String toDate){
		Integer count = 0;
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		try {
			Criteria criteria = session.createCriteria(Alertmessageinfo.class).setProjection(Projections.rowCount());
			if(app!=null && !app.equals("null") &&  !app.equals(""))
				criteria.add(Restrictions.like("applicationName", app));
			if(hostname != null && !hostname.equals("null") && !hostname.equals(""))
				criteria.add(Restrictions.like("amHostname", "%"+hostname+"%"));
			if(parameter != null && !parameter.equals("null") && !parameter.equals(""))
				criteria.add(Restrictions.eq("amParametername",parameter));
			if(alertType != null && !alertType.equals("null") && !alertType.equals(""))
				criteria.add(Restrictions.eq("amType",alertType));
			if(fromDate!= null && !fromDate.equals("") || (toDate!= null && !toDate.equals(""))){
				criteria.add(Restrictions.between("amDatetime", fromDate, toDate));
			}
			count = ((Long) criteria.uniqueResult()).intValue();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		tx.commit();
		return count;
	}
	
}
