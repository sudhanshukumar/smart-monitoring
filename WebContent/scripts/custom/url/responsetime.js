function loadresponsetime(i, parameterName, dataobj){
	 var hostName = dataobj.alertMessageHostName;
		//check for . since it gives error as illegal character
		if(hostName.indexOf(".") > -1) {
			hostName = hostName.split(".").join("_");
		}  
		var appName= JSON.stringify(dataobj.responsetime.appName);
		var page= JSON.stringify(dataobj.responsetime.page);
		var threshold_high_val= JSON.stringify(dataobj.responsetime.threshold_high_val);
		var threshold_low_val= JSON.stringify(dataobj.responsetime.threshold_low_val);
		var respTimeInMillis= JSON.stringify(dataobj.responsetime.respTimeInMillis);
		console.log(respTimeInMillis);
//		alert('inside response: '+JSON.stringify(dataobj.responsetime));
		$("#label_ac-4").show();
		$("#ac4_container_small").show();
		$("#ac4_block_1").append(
//						"<div class='block_small'>"
						"<div class='block_split1' id='ac-4_split1_"
						+ hostName
						+ "' onClick=showAC4Details("
						+ (i + 1)
						+ ",id,"
						+ AC4Split1Count
						+ ",'"
						+ dataobj.alertMessageHostName
						+ "','"
						+ appName.split(" ").join("+")
						+ "','"
						+ page
						+ "')>"
						+ "<table>"
						+ "<thead>"
						+ "<tr>"
						+ "<th>"
						+ strings['ac-4_split1.header']
						+ dataobj.alertMessageHostName
						+ "  C = "
						+ threshold_high_val
						+ " ms   W = "
						+ threshold_low_val
						+ " ms </th>"
						+ "</tr>"
						+ "</thead>"
						+ "</table>"
						+ "<table>"
						+ "<tbody>"
					/*	+ "<tr>"
						+ "<td>"
						+ "<div>"
						+ "<strong>"
						+ strings['ac-4_split1.column1.header']
						+ "</strong>"
						+ "</div>"
						+ "<div id='ac4_split1_col1_"
						+ hostName
						+ "'>"
						+ appName
						+ "</div>"
						+ "</td>"
						+ "</tr>"
						+ "<tr>"
						+ "<td>"
						+ "<div>"
						+ "<strong>"
						+ strings['ac-4_split1.column2.header']
						+ "</strong>"
						+ "</div>"
						+ "<div id='ac4_split1_col2_"
						+ hostName
						+ "'>"
						+ page
						+ "</div>"
						+ "</td>"
						+ "</tr>"*/
						+ "<tr>"
										
						+ "<td class='ac-2_col1'>"
						+ "<div>"
						+ "<strong>"
						+ strings['ac-4_split1.column3.header']
						+ "</strong>"
						+ "</div>"
						+ "<span id='ac4_split1_col3_"
						+ hostName
						+ "'>"
						+ dataobj.responsetime.respTimeInMillis
						+ "</span>" + "</td>" + "</tr>" 
						+ "</tbody>" + "</table>" + "</div>");
		AC4Split1Count++;
}


function showAC4Details(blockNumber, id, totalBlocks, vm, appName, pageName) {
	/* Onclick event for left block split 1 for Accordian 4 that is RESPONSETIME */

	/*
	 * clear rest of the split1 blocks toshow only selected block with id as
	 * 'id' result
	 */
	/*
	 * for(var i=0;i<totalBlocks;i++) { }
	 */
	$
			.ajax({
				type : "POST",
				url : "additionalInfo.html",
				data : "parameterName=RESPONSETIME&addressedMachine=" + vm
						+ "&appname=" + appName + "&page=" + pageName,

				success : function(response) {
					
					var result = JSON.parse(response);
					/*
					 * removing if buttons and divs are already created; else on
					 * every click buttons for correlate etc will get added.
					 */
					$("#ac4_split2_table1_" + blockNumber).each(function() {
						this.parentNode.removeChild(this);
					});
					$("#ac-4_split3_" + blockNumber).each(function() {
						this.parentNode.removeChild(this);
					});
					$("#ac4_container_small")
							.append(
									"<div id='ac4_container_small_second'>"
											+ "<div class='block_split2,ac-4_rightDiv' id='ac-4_rightDiv_"
											+ blockNumber
											+ "' align='right'></div>"
											+ "<div class='buttonsRow' id='ac-4_button_row_"
											+ blockNumber
											+ "'>"
											+ "<table id='ac4_split2_table1_"
											+ blockNumber
											+ "'>"
											+ "<tbody>"
											+ "<tr>"
											+ "<td><input type='button' value='Correlate'	class='rightButton' id='ac-4_btn1_"
											+ blockNumber
											+ "' onClick=correlate(id,"
											+ blockNumber
											+ ",'"
											+ vm
											+ "')></td>"
											+ "<td><input type='button' value='Prediction' class='rightButton' id='ac-4_btn2_"
											+ blockNumber
											+ "' onClick=predict(id,"
											+ blockNumber
											+ ",'"
											+ appName
											+ "','"
											+ pageName
											+ "')></td>"
											+ "<td><input type='button' value='Reports'	class='rightButton' id='ac-4_btn3_"
											+ blockNumber
											+ "' onClick='reports()'></td>"
											+ "<td><input type='button' value='Logs'	class='rightButton' id='ac-4_btn4_"
											+ blockNumber
											+ "' onClick='more()'></td>"
											+ "</tr>"
											+ "</tbody>"
											+ "</table>"
											+ "</div>"
											+ "<div class='block_split3' id='ac-4_split3_"
											+ blockNumber
											+ "'>"
											+ "<div class='ac-4_split3_1'>"
											+ "<div class='ac-4_split3_1_1' id='ac-4_split3_1_1_"
											+ blockNumber
											+ "'></div>"
											+ "<div class='ac-4_split3_1_2' id='ac-4_split3_1_2_"
											+ blockNumber
											+ "'></div>"
											+ "<div class='ac-4_split3_1_3' id='ac-4_split3_1_3_"
											+ blockNumber
											+ "'></div>"
											+ "</div>"
											+ "<div class='ac-4_split3_2' id='ac-4_split3_2_"
											+ blockNumber
											+ "'>"
											+ "<div class='ac-4_split3_2_1' id='ac-4_split3_2_1_"
											+ blockNumber
											+ "' align='center'></div>"
											+ "</div>" + "</div>" + "</div>");
					/* Hiding correlate and prediction charts */
					$("#ac-4_split3_" + blockNumber + "").hide();
					/* Right div's chart starts here. */

					var yAxisData = [];
					var xAxisData = [];
					xAxisData.push('time');
					yAxisData.push('time to Connect');
					/*for (k = 0; k < result.length; k++) {
						if(result[k].status == "OK")
							result[k].status = 1;
						else if(result[k].status == "FAIL")
							result[k].status = 0;
					}*/

					for (k = 0; k < result.length; k++) {
						xAxisData.push(result[k].dataReceivedTime);
						yAxisData.push(result[k].timeToConnect);
						/*yAxisData.push([ result[k].dataReceivedTime,
								result[k].timeToConnect]); */
					}
					//c3 charts code
					var chart = c3.generate({
  						bindto: '#ac-4_rightDiv_' + blockNumber,
  						data: {
  					        x: 'time',
  					        columns: [
  				              xAxisData,         
  				              yAxisData
  					        ]
  					    },
  					    
  					    subchart: {
  					        show: true
  					    },
  					    zoom: {
  					        enabled: true
  					    },
  					    axis : {
  					        x : {
  					        	label:'Time',
  					            type : 'timeseries',
  					            tick: {
  					            	fit:true,
  					            	count: result.length,
  					                format:'%I:%M:%S' /* function (x) { return x.getFullYear(); } */
  					              //format: '%Y' // format string is also available for timeseries data
  					            }
  					        },
  					        y : {
  					        	label:'time to Connect'
  					        }
  					    }
  					});
					//high charts code
					// sorting data on timestamp, if to be sorted on values make
					// it a[1] > b[1]
					/*yAxisData = yAxisData.sort(function(a, b) {
						return a[0] - b[0];
					});
					Highcharts.setOptions({
						global : {
							timezoneOffset : -(330)
						}
					});
					$('#ac-4_rightDiv_' + blockNumber)
							.highcharts(
									'StockChart',
									{
										chart : {
											type : 'line',
											zoomType : 'x',
											height : 215,
											width : 900
										},
										rangeSelector : {
											buttons : [ {
												type : 'hour',
												count : 1,
												text : '1h'
											}, {
												type : 'day',
												count : 1,
												text : '1d'
											}, {
												type : 'month',
												count : 1,
												text : '1m'
											}, {
												type : 'year',
												count : 1,
												text : '1y'
											}, {
												type : 'all',
												text : 'All'
											} ],
											inputEnabled : false, // it
											// supports
											// only days
											selected : 4
										// all
										},

										xAxis : {

											// minRange: 3600 * 1000, // one
											// hour

											title : {
												enabled : true,
												text : ''
											},

											events : {
												setExtremes : function(event) {

													var start_timestamp = event.min;

													var end_timestamp = event.max;
													refreshCorrelatedGraphs(
															start_timestamp,
															end_timestamp,
															blockNumber);
												}
											}
										},

										yAxis : [ {
											floor : 0,
											title : {
												text : 'Response Time',
												style : {
													color : Highcharts
															.getOptions().colors[1]
												}
											}
										} ],
										legend : {
											layout : 'vertical',
											align : 'left',
											x : 120,
											verticalAlign : 'top',
											y : 100,
											floating : true,
											backgroundColor : (Highcharts.theme && Highcharts.theme.legendBackgroundColor)
													|| '#FFFFFF'
										},

										title : {
											text : 'Response Time for last 12 hours'
										},
										plotOptions : {
											showInLegend : true

										},
										series : [ {
											name : 'Response Time',
											data : yAxisData,

											tooltip : {
												valueDecimals : 2,
												valueSuffix : 'sec'
											}
										} ]
									});*/
				}
			});
}

var ramObjArr;
var cpuObjArr;
var ioObjArr;
var ramDataArr = [];
var cpuDataArr = [];
var ioDataArr = [];
 
function correlate(id, blockNumber, vm) {
	$("#ac-4_split3_" + blockNumber + "").show();
	$
			.ajax({

				type : "POST",

				url : "correlateResponseTime.html",

				data : "parameterName=CORELATERESPONSETIME&addressedMachine="
						+ vm,

				success : function(response) {
					var result = JSON.parse(response);
					ramObjArr = result.winRAM;
					cpuObjArr = result.winCPU;
					ioObjArr = result.IO;
					ramDataArr = [];
					cpuDataArr = [];
					ioDataArr = [];
					 var yAxisDataRAM=[];
					 var xAxisDataRAM=[];
					 var yAxisDataCPU=[];
					 var xAxisDataCPU=[];
					 var yAxisDataIO=[];
					 var xAxisDataIO=[];
					 yAxisDataCPU.push('CPU Memory used');
					 xAxisDataCPU.push('timeCPU');
					 yAxisDataRAM.push('RAM Memory used');
					 xAxisDataRAM.push('timeRAM');
					 yAxisDataIO.push('IO Wait');
					 xAxisDataIO.push('timeIO');
					for (j = 0; j < ramObjArr.length; j++) {
						xAxisDataRAM.push(ramObjArr[j].dataReceivedTime);
						yAxisDataRAM.push(ramObjArr[j].memUsed);
						ramDataArr.push([ ramObjArr[j].dataReceivedTime,
								ramObjArr[j].memUsed ]);
					}
					for (a = 0; a < cpuObjArr.length; a++) {
						xAxisDataCPU.push(cpuObjArr[a].cpudataReceivedTime);
						yAxisDataCPU.push(cpuObjArr[a].loadAverage5min);
						cpuDataArr.push([ cpuObjArr[a].cpudataReceivedTime,
								cpuObjArr[a].loadAverage5min ]);
					}
					for (b = 0; b < ioObjArr.length; b++) {
						xAxisDataIO.push(ioObjArr[b].dataInsertedTime);
						yAxisDataIO.push(ioObjArr[b].io_wait);
						ioDataArr.push([ ioObjArr[b].dataInsertedTime,
								ioObjArr[b].io_wait ]);
					}
					//c3 charts code
					var chart = c3.generate({
  						bindto: '#ac-4_split3_1_1_' + blockNumber,
  						data: {
  					        x: 'timeRAM',
  					        columns: [
  				              xAxisDataRAM,         
  				              yAxisDataRAM
  					        ]
  					    },
  					    
  					    subchart: {
  					        show: true
  					    },
  					    zoom: {
  					        enabled: true
  					    },
  					    axis : {
  					        x : {
  					        	label:'Time',
  					            type : 'timeseries',
  					            tick: {
  					            	fit:true,
  					            	count: result.length,
  					                format:'%I:%M:%S' /* function (x) { return x.getFullYear(); } */
  					              //format: '%Y' // format string is also available for timeseries data
  					            }
  					        },
  					        y : {
  					        	label:'time to Connect'
  					        }
  					    }
  					});
					var chart = c3.generate({
  						bindto: '#ac-4_split3_1_2_' + blockNumber,
  						data: {
  					        x: 'timeCPU',
  					        columns: [
  				              xAxisDataCPU,         
  				              yAxisDataCPU
  					        ]
  					    },
  					    
  					    subchart: {
  					        show: true
  					    },
  					    zoom: {
  					        enabled: true
  					    },
  					    axis : {
  					        x : {
  					        	label:'Time',
  					            type : 'timeseries',
  					            tick: {
  					            	fit:true,
  					            	count: result.length,
  					                format:'%I:%M:%S' /* function (x) { return x.getFullYear(); } */
  					              //format: '%Y' // format string is also available for timeseries data
  					            }
  					        },
  					        y : {
  					        	label:'time to Connect'
  					        }
  					    }
  					});
					var chart = c3.generate({
  						bindto: '#ac-4_split3_1_3_' + blockNumber,
  						data: {
  					        x: 'timeIO',
  					        columns: [
  				              xAxisDataIO,         
  				              yAxisDataIO
  					        ]
  					    },
  					    
  					    subchart: {
  					        show: true
  					    },
  					    zoom: {
  					        enabled: true
  					    },
  					    axis : {
  					        x : {
  					        	label:'Time',
  					            type : 'timeseries',
  					            tick: {
  					            	fit:true,
  					            	count: result.length,
  					                format:'%I:%M:%S' /* function (x) { return x.getFullYear(); } */
  					              //format: '%Y' // format string is also available for timeseries data
  					            }
  					        },
  					        y : {
  					        	label:'time to Connect'
  					        }
  					    }
  					});
					//high charts code

					/*
					 * sorting by timestamp. if to be sorted on second value
					 * make it a[1] > b[1]
					 */
					/*ramDataArr = ramDataArr.sort(function(a, b) {
						return a[0] - b[0];
					});

					cpuDataArr = cpuDataArr.sort(function(a, b) {
						return a[0] - b[0];
					});

					ioDataArr = ioDataArr.sort(function(a, b) {
						return a[0] - b[0];
					});

					Highcharts.setOptions({
						global : {
							timezoneOffset : -(330)
						}
					});

					$('#ac-4_split3_1_1_' + blockNumber)
							.highcharts(
									'StockChart',
									{
										chart : {
											type : 'line',
											zoomType : 'x',
											width : 420,
											height : 350
										},
										title : {
											text : 'RAM Usage Trend'
										},
										subtitle : {
											text : document.ontouchstart === undefined ? 'Click and drag in the plot area to zoom in'
													: 'Pinch the chart to zoom in'
										},
										xAxis : {

											// minRange: 3600 * 1000, // one
											// hour
											title : {
												enabled : true,
												text : ''
											}
										},
										yAxis : {
											title : {
												text : 'Usage in Mb'
											}
										},
										rangeSelector : {
											buttons : [ {
												type : 'hour',
												count : 1,
												text : '1h'
											}, {
												type : 'day',
												count : 1,
												text : '1d'
											}, {
												type : 'month',
												count : 1,
												text : '1m'
											}, {
												type : 'year',
												count : 1,
												text : '1y'
											}, {
												type : 'all',
												text : 'All'
											} ],
											inputEnabled : false, // it
											// supports
											// only days
											selected : 4
										// all
										},
										tooltip : {
											valueSuffix : 'Mb',
											valueDecimals : 2
										},
										legend : {
											enabled : false
										},
										plotOptions : {
											area : {
												fillColor : {
													linearGradient : {
														x1 : 0,
														y1 : 0,
														x2 : 0,
														y2 : 1
													},
													stops : [
															[
																	0,
																	Highcharts
																			.getOptions().colors[0] ],
															[
																	1,
																	Highcharts
																			.Color(
																					Highcharts
																							.getOptions().colors[0])
																			.setOpacity(
																					0)
																			.get(
																					'rgba') ] ]
												},
												marker : {
													radius : 2
												},
												lineWidth : 1,
												states : {
													hover : {
														lineWidth : 1
													}
												},
												threshold : null
											}
										},

										series : [ {

											name : 'Usage',
											
											 * pointInterval: 24 * 3600 * 1000,
											 * pointInterval: 3600 * 1000,
											 * pointStart: Date.UTC(2006, 0, 1),
											 
											data : ramDataArr,
											dataGrouping : {
												enabled : false
											}
										} ]
									});

					 show related graphs for RAM ends 

					 show related graphs for CPU starts 
					Highcharts.setOptions({
						global : {
							timezoneOffset : -(330)
						}
					});
					$('#ac-4_split3_1_2_' + blockNumber)
							.highcharts(
									'StockChart',
									{
										chart : {
											type : 'line',
											zoomType : 'x',
											width : 420,
											height : 350
										},
										title : {
											text : 'CPU Load Trend'
										},
										subtitle : {
											text : document.ontouchstart === undefined ? 'Click and drag in the plot area to zoom in'
													: 'Pinch the chart to zoom in'
										},
										xAxis : {
											minRange : 3600 * 1000, // one hour

											title : {
												enabled : true,
												text : 'Hours of the day'
											}
										},
										rangeSelector : {
											buttons : [ {
												type : 'hour',
												count : 1,
												text : '1h'
											}, {
												type : 'day',
												count : 1,
												text : '1d'
											}, {
												type : 'month',
												count : 1,
												text : '1m'
											}, {
												type : 'year',
												count : 1,
												text : '1y'
											}, {
												type : 'all',
												text : 'All'
											} ],
											inputEnabled : false, // it
											// supports
											// only days
											selected : 4
										// all
										},

										yAxis : {
											floor : 0,
											title : {
												text : ''
											}
										},
										tooltip : {
											valueSuffix : '',
											valueDecimals : 2

										},
										legend : {
											enabled : false
										},
										plotOptions : {
											area : {
												fillColor : {
													linearGradient : {
														x1 : 0,
														y1 : 0,
														x2 : 0,
														y2 : 1
													},
													stops : [
															[
																	0,
																	Highcharts
																			.getOptions().colors[0] ],
															[
																	1,
																	Highcharts
																			.Color(
																					Highcharts
																							.getOptions().colors[0])
																			.setOpacity(
																					0)
																			.get(
																					'rgba') ] ]
												},
												marker : {
													radius : 2
												},
												lineWidth : 1,
												states : {
													hover : {
														lineWidth : 1
													}
												},
												threshold : null
											}
										},

										series : [ {

											name : 'Usage',
											
											 * pointInterval: 3600 * 1000,
											 * pointStart: Date.UTC(2006, 0, 1),
											 
											data : cpuDataArr,
											dataGrouping : {
												enabled : false
											}
										} ]
									});
					
					 show related graphs for CPU ends 

					 show related graphs for IO starts 
					Highcharts.setOptions({
						global : {
							timezoneOffset : -(330)
						}
					});
					$('#ac-4_split3_1_3_' + blockNumber)
							.highcharts(
									'StockChart',
									{
										chart : {
											type : 'line',
											zoomType : 'x',
											width : 420,
											height : 350
										},
										title : {
											text : 'IO Trend'
										},
										subtitle : {
											text : document.ontouchstart === undefined ? 'Click and drag in the plot area to zoom in'
													: 'Pinch the chart to zoom in'
										},
										xAxis : {
											minRange : 3600 * 1000, // one hour

											title : {
												enabled : true,
												text : 'Hours of the day'
											}
										},
										rangeSelector : {
											buttons : [ {
												type : 'hour',
												count : 1,
												text : '1h'
											}, {
												type : 'day',
												count : 1,
												text : '1d'
											}, {
												type : 'month',
												count : 1,
												text : '1m'
											}, {
												type : 'year',
												count : 1,
												text : '1y'
											}, {
												type : 'all',
												text : 'All'
											} ],
											inputEnabled : false, // it
											// supports
											// only days
											selected : 4
										// all
										},

										yAxis : {
											floor : 0,
											title : {
												text : 'IO wait in %'
											}
										},
										tooltip : {
											valueSuffix : '%',
											valueDecimals : 2
										},

										legend : {
											enabled : false
										},
										plotOptions : {
											area : {
												fillColor : {
													linearGradient : {
														x1 : 0,
														y1 : 0,
														x2 : 0,
														y2 : 1
													},
													stops : [
															[
																	0,
																	Highcharts
																			.getOptions().colors[0] ],
															[
																	1,
																	Highcharts
																			.Color(
																					Highcharts
																							.getOptions().colors[0])
																			.setOpacity(
																					0)
																			.get(
																					'rgba') ] ]
												},
												marker : {
													radius : 2
												},
												lineWidth : 1,
												states : {
													hover : {
														lineWidth : 1
													}
												},
												threshold : null
											}
										},

										series : [ {

											name : 'IO',
											
											 * pointInterval: 3600 * 1000,
											 * pointStart: Date.UTC(2006, 0, 1),
											 

											data : ioDataArr
										} ]
									});*/
				}

			});
}


function predict(id, blockNumber, appname, pagename) {
	if (!($('ac-4_split3_' + blockNumber).is(':visible'))) {
		$('ac-4_split3_' + blockNumber).show();
	}

	if ($("#" + id).val() == 'Hide Prediction') {
		$('#ac-4_split3_2_' + blockNumber).hide();
		$("#" + id).val('Show Prediction');
	} else {
		$('#ac-4_split3_2_' + blockNumber).show();
		$("#" + id).val('Hide Prediction');
	}
	/* query to get last 4 hours data of resp time */
	var categories0 = [];
	var yAxisData0 = [];
	var series1DataArr = [];
	var series2DataArr = [];
	var yAxisData1 = [];
	var xAxisData1 = [];
	var yAxisData2 = [];
	var xAxisData2= [];
	yAxisData1.push('TimeSeries1');
	xAxisData1.push('time1');
	yAxisData2.push('TimeSeries2');
	xAxisData2.push('time2');
	$.ajax({

		type : "POST",
		url : "lastUpdatedRespTime.html",
		data : "parameterName=RESPONSETIMEPREDICT&appName=" + appname
				+ "&page=" + pagename + "&limit=720",

		success : function(response) {

			var result = JSON.parse(response);
			for (k = 0; k < result.length; k++) {
				yAxisData1.push((result[k].responsetimeinmillies)/ 1000 );
				xAxisData1.push(result[k].responseDataReceivedTime);
				series1DataArr.push([ result[k].responseDataReceivedTime,
						(result[k].responsetimeinmillies) / 1000 ]);
			}
			/* sorting it by hours */
			/*series1DataArr = series1DataArr.sort(function(a, b) {
				return a[0] - b[0];
			});*/
		}
	});

	$
			.ajax({

				type : "POST",
				url : "respPrediction.html",
				data : "parameterName=RESPONSETIMEPREDICT&appName=" + appname
						+ "&page=" + pagename,

				success : function(response) {

					var result = JSON.parse(response);
					var categories = [];
					var predictedDataArr = [];
					for (b = 0; b < result.length; b++) {
						xAxisData2.push( result[b].prediction_Date);
						yAxisData2.push(result[b].predicted);
						series2DataArr.push([ result[b].prediction_Date,
								result[b].predicted ]);
					}
					//c3 charts code
					var chart = c3.generate({
  						bindto: '#ac-4_split3_2_1_' + blockNumber,
  						data: {
  							xs:{
  								'TimeSeries1':'time1',
  								'TimeSeries2':'time2'
  							},
  					        //x: 'time',
  					        columns: [
  				              xAxisData1,         
  				              yAxisData1,
  				            xAxisData2,         
				              yAxisData2
  					        ]
  					    },
  					    
  					    subchart: {
  					        show: true
  					    },
  					    zoom: {
  					        enabled: true
  					    },
  					    axis : {
  					        x : {
  					        	label:'Time',
  					            type : 'timeseries',
  					            tick: {
  					            	fit:true,
  					            	count: result.length,
  					                format:'%I:%M:%S' /* function (x) { return x.getFullYear(); } */
  					              //format: '%Y' // format string is also available for timeseries data
  					            }
  					        },
  					        y : {
  					        	label:'predicted value'
  					        }
  					    }
  					});
					//high charts code
					/* sorting it by hours */
				/*	series2DataArr = series2DataArr.sort(function(a, b) {
						return a[0] - b[0];
					});

					
					 * Highcharts.setOptions({ global: { timezoneOffset: -(330) }
					 * });
					 
					$('#ac-4_split3_2_1_' + blockNumber)
							.highcharts(
									'StockChart',
									{
										chart : {
											zoomType : 'x',
											type : 'line',
											height : 273,
											width : 1000
										},
										rangeSelector : {
											buttons : [ {
												type : 'hour',
												count : 1,
												text : '1h'
											}, {
												type : 'day',
												count : 1,
												text : '1d'
											}, {
												type : 'month',
												count : 1,
												text : '1m'
											}, {
												type : 'year',
												count : 1,
												text : '1y'
											}, {
												type : 'all',
												text : 'All'
											} ],
											inputEnabled : false, // it
											// supports
											// only days
											selected : 4
										// all
										},
										dateTimeLabelFormats : {
											
											 * hour: '%I %p', minute: '%I:%M %p'
											 

											hour : '%H',
											day : '%e. %b',
											week : '%e. %b',
											month : '%b \'%y',
											year : '%Y'
										},
										title : {
											text : 'Hourly Prediction for response time'
										},

										yAxis : {
											title : {
												text : 'Avg Response Time'
											}
										},
										legend : {
											enabled : false
										},
										plotOptions : {
											area : {
												fillColor : {
													linearGradient : {
														x1 : 0,
														y1 : 0,
														x2 : 0,
														y2 : 1
													},
													stops : [
															[
																	0,
																	Highcharts
																			.getOptions().colors[0] ],
															[
																	1,
																	Highcharts
																			.Color(
																					Highcharts
																							.getOptions().colors[0])
																			.setOpacity(
																					0)
																			.get(
																					'rgba') ] ]
												},
												marker : {
													radius : 2
												},
												lineWidth : 1,
												states : {
													hover : {
														lineWidth : 1
													}
												},
												threshold : null
											}
										},
										tooltip : {
											valueSuffix : 'sec',
											valueDecimals : 2
										},
										series : [ {

											name : 'Actual Response Time',
											data : series1DataArr,
											dataGrouping : {
												enabled : false
											}
										}, {
											name : 'Predicted Response Time',
											data : series2DataArr,
											dataGrouping : {
												enabled : false
											}
										} ]
									});*/
				}
			});

}

function more() {
	window.location.href = "query.html";
}

function reports() {
	window.location.href = "userdashboard.html";
}