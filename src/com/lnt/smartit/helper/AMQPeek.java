package com.lnt.smartit.helper;

import java.io.IOException;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lnt.smartit.models.AlertMessage;
import com.lnt.smartit.models.AllMachinesAlertMessages;
import com.lnt.smartit.observer.QueueSubject;

public class AMQPeek {
	private static final Logger logger = LoggerFactory.getLogger(AMQPeek.class.getName());

public static String peekDatainQueue() throws JsonGenerationException, JsonMappingException, IOException {
		
		AllMachinesAlertMessages queueData = QueueSubject.alertMessageEventQueue.peek();
		logger.debug("QueueData-------------------"+queueData);
		ObjectMapper mapper = new ObjectMapper();
		List<AlertMessage> alList = queueData.getAlertMessageList(); 
		logger.debug("AMQPeek Size " + alList.size());
		// Ishita Added -3rd Aug Start
		for (int i = 0; i < alList.size(); i++) {

			logger.debug("AMQPeek data " + alList.get(i));
		}
		return mapper.writeValueAsString(alList);
	}
}
