<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../assets/ico/favicon.ico">
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

<title>SmartIT</title>

<!-- Bootstrap Styles -->
<link rel="stylesheet" type="text/css"
	href="assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="assets/css/bootstrap-responsive.css">
<link rel="stylesheet" type="text/css"
	href="assets/css/bootstrap-theme.min.css">

<!-- Common Styles -->
<link rel="stylesheet" type="text/css" href="assets/css/footer.css" />
<link href="css/custom/general/style1366.css" rel="stylesheet" />
<link href="css/custom/general/style1024.css" rel="stylesheet" />
<link href="css/custom/general/bluetabs.css" rel="stylesheet" />


<!-- Jquery css -->
<link rel="stylesheet" type="text/css" href="css/common/jquery-ui.css" />
<link rel="stylesheet" type="text/css"
	href="css/common/jquery.datetimepicker.css" />

<!-- Jquery scripts -->
<script type="text/javascript" src="assets/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="scripts/common/jquery-ui.js"></script>
<script type="text/javascript"
	src="scripts/common/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="scripts/common/grid.locale-en.js"></script>


<!-- Bootstrap scripts -->
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>


<!-- Custom javascripts -->
<script type="text/javascript" src="scripts/custom/correlated_query.js"></script>
<script type="text/javascript" src="scripts/common/menu/dropdowntabs.js"></script>


</head>
<body>

	<header>
		<%@ include file="../common/header.jsp"%>
	</header>


	<nav id="bluemenu" class="bluetabs">
		<ul>
			<li><a href="dashboard.html"><spring:message
						code="label.admin.menu1"></spring:message></a></li>
			<li><a href="problemHistory.html"><spring:message
						code="label.admin.menu2"></spring:message></a></li>
			<li><a href="#" rel="dropmenu1_b"><spring:message
						code="label.admin.menu3"></spring:message></a></li>
		<%-- 	<li><a href="manageNotificationGroup.html"><spring:message
						code="label.admin.menu4"></spring:message></a></li> --%>
<%-- 			<li><a href="variableAnalysis.html"><spring:message --%>
<%-- 						code="label.admin.menu5"></spring:message></a></li> --%>
<%-- 			<li><a href="#" rel="dropmenu1_solr"><spring:message --%>
						code="label.admin.solr.reports"></spring:message></a></li> 
			<li><a id="selected" href="query.html"><spring:message
						code="label.admin.solr.query"></spring:message></a></li>
						  <li><a href="correlation.html"><spring:message
						code="label.admin.correlation"></spring:message></a></li>
		</ul>
	</nav>
	<div class="ddcolortabsline">&nbsp;</div>

	<div id="dropmenu1_b" class="dropmenudiv_b">
		<a href="manageDevice.html"><spring:message
				code="label.admin.menu3.submenu1"></spring:message></a> <a
			href="manageThresHold.html"><spring:message
				code="label.admin.menu3.submenu2"></spring:message></a> 
<%-- 			<a href="manageApplication.html"><spring:message --%>
<%-- 				code="label.admin.menu3.submenu3"></spring:message></a>  --%>
	<%-- 		<a href="manageService.html"><spring:message
				code="label.admin.menu3.submenu4"></spring:message></a>  --%>
<%-- 			<a href="manageUsers.html"><spring:message --%>
<%-- 				code="label.admin.menu3.submenu5"></spring:message></a> --%>
<%-- <a href="manageLog.html"><spring:message
				code="label.admin.menu3.submenu6"></spring:message></a> --%>

	</div>

	<div id="dropmenu1_solr" class="dropmenudiv_b">
		<a href="userdashboard.html"><spring:message
				code="label.admin.solr.dashboard"></spring:message></a> <a
			href="charts.html"><spring:message code="label.admin.solr.charts"></spring:message></a>
		<a href="createuserdashboard.html"><spring:message
				code="label.admin.solr.custom.dashboard"></spring:message></a> <a
			href="list_dashboard.html"><spring:message
				code="label.admin.solr.view.dashboard"></spring:message></a>

	</div>

	<script type="text/javascript">
		//SYNTAX: tabdropdown.init("menu_id", [integer OR "auto"])
		tabdropdown.init("bluemenu")
	</script>

	<div class="container">

		<!-- Main component for a primary marketing message or call to action -->

		<div class="row">

			<div class="col-md-4">

				<div class="row" id="default_params">

				
							<div class="col-md-12" style="border: 0px solid #000;">
								<form class="form-horizontal" role="form">
									<div class="form-group">

										<label class="col-sm-4" style="padding: 10px 0px 10px 40px">Application:</label>

										<div class="col-sm-8">

											<select class="form-control" id="applications">
												<option value="campus">CampusNext</option>
												<option value="payfast">LMS Sakai</option>
												<option value="servicefirest">FNA & Procurement</option>
												<option value="campus">PayFast</option>
												<option value="payfast">Rendezvous</option>
												<option value="servicefirest">ProcureFirst</option>
												<option value="campus">AppMakerJ</option>
												<option value="payfast">Sapphire</option>
											</select>
										</div>
									</div>


								</form>
							</div>
						</div>
					
			</div>



	<div class="col-md-4">

				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="col-sm-4" style="padding: 10px 0px 10px 40px">From:</label>
						<div class="col-sm-8">
							<div class="inner-addon left-addon">
								<i class="glyphicon glyphicon-calendar"></i> <input
									class="form-control" type="text" id="from_datepicker">
							</div>
						</div>

					</div>


				</form>


			</div>
			<div class="col-md-4">

				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="col-sm-4" style="padding: 10px 0px 10px 40px">To:</label>
						<div class="col-sm-8">
							<div class="inner-addon left-addon">
								<i class="glyphicon glyphicon-calendar"></i> <input
									class="form-control" type="text" id="to_datepicker">
							</div>
						</div>

					</div>

				</form>

			</div>


		</div>

		
		<div class="row" id="result_grid">
			<div class="col-md-12">
				<div class="panel panel-info">
					<div class="panel-heading">

						<h3 class="panel-title" id="records_count">Results</h3>
					</div>
					<div class="panel-body">
						<div class="row">

							<div class="col-md-12"
								style="overflow: scroll; width: 1160px; height: 400px;">
								<table class="table table-bordered" id="outId">
									<thead>
										<tr>
										<th>Logs</th>
											<th>Time</th>
											<th>Log Type</th>
											<th>Record</th>
										</tr>
									</thead>
									<tbody>

										<tr>
										<td rowspan="8"><div style="height: 500px;"> 
										<div id="accordion">
  <h5>Access Logs</h5>
  <div>
    <table>
    <tr>
    <td><input type="checkbox"/>Date</td>
    </tr>
     <tr>
    <td><input type="checkbox"/>IP</td>
    </tr>
     <tr>
    <td><input type="checkbox"/>Response Time</td>
    </tr>
    </table>
  </div>
  <h3>Error Logs</h3>
  <div>
   <table>
    <tr>
    <td><input type="checkbox"/>Date</td>
    </tr>
     <tr>
    <td><input type="checkbox"/>IP</td>
    </tr>
     <tr>
    <td><input type="checkbox"/>Error Cause</td>
    </tr>
    </table>
  </div>
  <h3>Java Logs</h3>
  <div>
    <table>
    <tr>
    <td><input type="checkbox"/>Date</td>
    </tr>
     <tr>
    <td><input type="checkbox"/>NGCMN</td>
    </tr>
     <tr>
    <td><input type="checkbox"/>OGCMX</td>
    </tr>
    </table>
  </div>
  <h3>Machine Logs</h3>
  <div>
    <table>
    <tr>
    <td><input type="checkbox"/>Date</td>
    </tr>
     <tr>
    <td><input type="checkbox"/>CPU</td>
    </tr>
     <tr>
    <td><input type="checkbox"/>Memory</td>
    </tr>
    </table>
  </div>
</div>
										</div></td>
											<td rowspan="4">2015-04-16 22:20:25Z</td>
											<td>Access log</td>
											<td>"application": "shikshacloud", "host":
												"ltiappshop.com:443", "status": 200, "request_date":
												"2015-04-16 22:20:25Z", "url":
												"https://vjti.ltiappshop.com/shikshacloud/viewPerformSearchM311CStAypStudentProfileEMS.do",
												"ip": "120.60.168.136", "size": 377, "microsec": 40049,
												"request":
												"/shikshacloud/getTitleForM311CStAypStudentProfileEMS.do",
												"user_agent": "Mozilla/5.0 (Windows NT 5.1)
												AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101
												Safari/537.36", "method": "GET", "user": "124140007",</td>
										</tr>

										<tr>
											
											<td>Error log</td>
											<td>"Log_type": "error", "Date": "2015-04-16 22:20:25Z",
												"IP": "66.249.79.99", "cause_message": "[client
												66.249.79.99] File does not exist:
												/var/www/robots.txt\nREMOVE THESE 2 statements later: Formed
												dynamic CAS Login URL as :
												https://vjti.ltiappshop.com/cas/login",</td>
										</tr>

										<tr>
											
											<td>Java log</td>
											<td>"EC": 1295040, "PGC": 524288, "NGCMN": 1572864,
												"OGC": 4718592, "OGCMN": 4718592, "id":
												"096e24f9-5931-4d17-98ff-04084c5f138e#11", "NGCMX": 2097152,
												"S0C": 124608, "OGCMX": 6291456, "FGC": 2, "OC": 4718592,
												"YGC": 281, "PGCMN": 262144, "Timestamp": "2015-04-16
												22:20:25Z", "S1C": 133952, "PGCMX": 524288, "NGC": 1572864,
												"PC": 524288,</td>
										</tr>

										<tr>
											
											<td>Machine log</td>
											<td>"CPUAll_CPUs": 4, "VM_pginodesteal": 68,
												"DISKREAD_sda": 0, "VM_nr_slab": -1, "VM_pgsteal_high": 0,
												"MEM_cached": 463.6, "CPU01_User": 0.2, "DISKBSIZE_sda": 0,
												"VM_pgdeactivat": 0, "CPU02_Idle": 97.8, "PROC_msg": -1,
												"VM_pswpin": 0, "DISKWRITE_sda": 0, "VM_pgrotated": 0,
												"VM_pgalloc_high": 0, "VM_nr_page_table_pages": 18,
												"VM_kswapd_inodesteal": 16, "CPU01_Wait": 0, "MEM_active":
												7561.2, "DISKBSIZE_sda5": 0, "DISKBSIZE_sda1": 0,
												"DISKBSIZE_sda2": 0, "CPU03_Sys": 0, "DISKREAD_dm_1": 0,
												"DISKREAD_dm_0": 0, "VM_pgscan_kswapd_high": 0,
												"MEM_swaptotal": 2044, "VM_pgalloc_dma": 0,
												"NETPACK_eth0_read": 6.6, "CPU04_Idle": 99.6, "PROC_sem":
												-1, "MEM_swapcached": 0, "VM_nr_unstable": -1,
												"VM_nr_writeback": -1, "DISKBSIZE_dm_0": 0, "PROC_Swap_in":
												-1, "DISKBUSY_sda": 0, "DISKBSIZE_dm_1": 0,
												"VM_pgsteal_dma": 0, "MEM_highfree": 0, "CPUAll_Sys": 0.2,
												"VM_pgscan_kswapd_dma": 8, "CPU04_Sys": 0.2, "MEM_swapfree":
												2044, "CPU03_Idle": 99.8, "PROC_pswitch": 1907.6,
												"Input_date": "2015-04-16 22:20:25Z", "MEM_lowtotal": 0,
												"NET_eth0_write": 0.2, "MEM_memshared": 0, "MEM_memfree":
												2115.5, "CPU04_Wait": 0, "MEM_inactive": 148.8, "CPU01_Sys":
												0, "VM_pgsteal_normal": 0, "DISKBUSY_sda5": 0,
												"VM_pgscan_direct_normal": 0, "DISKBUSY_sda1": 0,
												"DISKBUSY_sda2": 0, "VM_pgrefill_dma": 0, "PROC_syscall":
												-1, "VM_kswapd_steal": 0, "DISKXFER_sda": 0,
												"VM_slabs_scanned": 68, "VM_pgfree": 0, "CPUAll_Busy": 0,
												"DISKXFER_sda5": 0, "DISKWRITE_sda1": 0, "DISKBUSY_dm_0": 0,
												"DISKWRITE_sda2": 0, "VM_pgmajfault": 0, "DISKXFER_sda2": 0,
												"VM_pgscan_direct_high": 0, "DISKBUSY_dm_1": 0,
												"DISKXFER_sda1": 0, "MEM_bigfree": -1, "MEM_memtotal":
												10002.3, "DISKXFER_dm_0": 0, "CPU03_Wait": 0,
												"DISKXFER_dm_1": 0, "VM_pageoutrun": 0, "DISKREAD_sda5": 0,
												"NET_lo_write": 0, "DISKWRITE_sda5": 0, "VM_pswpout": 0,
												"NET_lo_read": 0, "VM_pgpgout": 0, "VM_pgactivat": 0,
												"VM_nr_mapped": -1, "DISKREAD_sda2": 0, "DISKWRITE_dm_0": 0,
												"DISKREAD_sda1": 0, "VM_nr_dirty": -1, "time_point":
												"T0005", "DISKWRITE_dm_1": 0, "VM_pgfault": 0,
												"NETPACK_lo_read": 0, "MEM_lowfree": 0, "VM_pgalloc_normal":
												0, "CPUAll_User": 0.7, "CPUAll_Idle": 99.2, "PROC_fork":
												0.2, "id": "13e3ba80-4a8b-4476-81f0-cd4bb4c08f98#4",
												"VM_pgscan_kswapd_normal": 0, "NET_eth0_read": 0.5,
												"VM_pgscan_direct_dma": 0, "CPU04_User": 0.2,
												"NETPACK_eth0_write": 3.2, "CPU02_Sys": 0.2, "CPUAll_Wait":
												0, "CPU01_Idle": 99.8, "VM_allocstall": 0, "CPU02_Wait": 0,
												"MEM_hightotal": 0, "PROC_write": -1, "CPU03_User": 0.2,
												"VM_pgpgin": 0, "MEM_buffers": 176.6, "VM_pgrefill_normal":
												0, "CPU02_User": 2, "NETPACK_lo_write": 0, "PROC_read": -1,
												"PROC_Runnable": 2, "VM_pgrefill_high": 0, "PROC_exec": -1,
											</td>
										</tr>
										
										<tr>
										
											<td rowspan="4">2015-04-16 22:20:25Z</td>
											<td>Access log</td>
											<td>"application": "shikshacloud", "host":
												"ltiappshop.com:443", "status": 200, "request_date":
												"2015-04-16 22:20:25Z", "url":
												"https://vjti.ltiappshop.com/shikshacloud/viewPerformSearchM311CStAypStudentProfileEMS.do",
												"ip": "120.60.168.136", "size": 377, "microsec": 40049,
												"request":
												"/shikshacloud/getTitleForM311CStAypStudentProfileEMS.do",
												"user_agent": "Mozilla/5.0 (Windows NT 5.1)
												AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101
												Safari/537.36", "method": "GET", "user": "124140007",</td>
										</tr>

										<tr>
											
											<td>Error log</td>
											<td>"Log_type": "error", "Date": "2015-04-16 22:20:25Z",
												"IP": "66.249.79.99", "cause_message": "[client
												66.249.79.99] File does not exist:
												/var/www/robots.txt\nREMOVE THESE 2 statements later: Formed
												dynamic CAS Login URL as :
												https://vjti.ltiappshop.com/cas/login",</td>
										</tr>

										<tr>
											
											<td>Java log</td>
											<td>"EC": 1295040, "PGC": 524288, "NGCMN": 1572864,
												"OGC": 4718592, "OGCMN": 4718592, "id":
												"096e24f9-5931-4d17-98ff-04084c5f138e#11", "NGCMX": 2097152,
												"S0C": 124608, "OGCMX": 6291456, "FGC": 2, "OC": 4718592,
												"YGC": 281, "PGCMN": 262144, "Timestamp": "2015-04-16
												22:20:25Z", "S1C": 133952, "PGCMX": 524288, "NGC": 1572864,
												"PC": 524288,</td>
										</tr>

										<tr>
											
											<td>Machine log</td>
											<td>"CPUAll_CPUs": 4, "VM_pginodesteal": 68,
												"DISKREAD_sda": 0, "VM_nr_slab": -1, "VM_pgsteal_high": 0,
												"MEM_cached": 463.6, "CPU01_User": 0.2, "DISKBSIZE_sda": 0,
												"VM_pgdeactivat": 0, "CPU02_Idle": 97.8, "PROC_msg": -1,
												"VM_pswpin": 0, "DISKWRITE_sda": 0, "VM_pgrotated": 0,
												"VM_pgalloc_high": 0, "VM_nr_page_table_pages": 18,
												"VM_kswapd_inodesteal": 16, "CPU01_Wait": 0, "MEM_active":
												7561.2, "DISKBSIZE_sda5": 0, "DISKBSIZE_sda1": 0,
												"DISKBSIZE_sda2": 0, "CPU03_Sys": 0, "DISKREAD_dm_1": 0,
												"DISKREAD_dm_0": 0, "VM_pgscan_kswapd_high": 0,
												"MEM_swaptotal": 2044, "VM_pgalloc_dma": 0,
												"NETPACK_eth0_read": 6.6, "CPU04_Idle": 99.6, "PROC_sem":
												-1, "MEM_swapcached": 0, "VM_nr_unstable": -1,
												"VM_nr_writeback": -1, "DISKBSIZE_dm_0": 0, "PROC_Swap_in":
												-1, "DISKBUSY_sda": 0, "DISKBSIZE_dm_1": 0,
												"VM_pgsteal_dma": 0, "MEM_highfree": 0, "CPUAll_Sys": 0.2,
												"VM_pgscan_kswapd_dma": 8, "CPU04_Sys": 0.2, "MEM_swapfree":
												2044, "CPU03_Idle": 99.8, "PROC_pswitch": 1907.6,
												"Input_date": "2015-04-16 22:20:25Z", "MEM_lowtotal": 0,
												"NET_eth0_write": 0.2, "MEM_memshared": 0, "MEM_memfree":
												2115.5, "CPU04_Wait": 0, "MEM_inactive": 148.8, "CPU01_Sys":
												0, "VM_pgsteal_normal": 0, "DISKBUSY_sda5": 0,
												"VM_pgscan_direct_normal": 0, "DISKBUSY_sda1": 0,
												"DISKBUSY_sda2": 0, "VM_pgrefill_dma": 0, "PROC_syscall":
												-1, "VM_kswapd_steal": 0, "DISKXFER_sda": 0,
												"VM_slabs_scanned": 68, "VM_pgfree": 0, "CPUAll_Busy": 0,
												"DISKXFER_sda5": 0, "DISKWRITE_sda1": 0, "DISKBUSY_dm_0": 0,
												"DISKWRITE_sda2": 0, "VM_pgmajfault": 0, "DISKXFER_sda2": 0,
												"VM_pgscan_direct_high": 0, "DISKBUSY_dm_1": 0,
												"DISKXFER_sda1": 0, "MEM_bigfree": -1, "MEM_memtotal":
												10002.3, "DISKXFER_dm_0": 0, "CPU03_Wait": 0,
												"DISKXFER_dm_1": 0, "VM_pageoutrun": 0, "DISKREAD_sda5": 0,
												"NET_lo_write": 0, "DISKWRITE_sda5": 0, "VM_pswpout": 0,
												"NET_lo_read": 0, "VM_pgpgout": 0, "VM_pgactivat": 0,
												"VM_nr_mapped": -1, "DISKREAD_sda2": 0, "DISKWRITE_dm_0": 0,
												"DISKREAD_sda1": 0, "VM_nr_dirty": -1, "time_point":
												"T0005", "DISKWRITE_dm_1": 0, "VM_pgfault": 0,
												"NETPACK_lo_read": 0, "MEM_lowfree": 0, "VM_pgalloc_normal":
												0, "CPUAll_User": 0.7, "CPUAll_Idle": 99.2, "PROC_fork":
												0.2, "id": "13e3ba80-4a8b-4476-81f0-cd4bb4c08f98#4",
												"VM_pgscan_kswapd_normal": 0, "NET_eth0_read": 0.5,
												"VM_pgscan_direct_dma": 0, "CPU04_User": 0.2,
												"NETPACK_eth0_write": 3.2, "CPU02_Sys": 0.2, "CPUAll_Wait":
												0, "CPU01_Idle": 99.8, "VM_allocstall": 0, "CPU02_Wait": 0,
												"MEM_hightotal": 0, "PROC_write": -1, "CPU03_User": 0.2,
												"VM_pgpgin": 0, "MEM_buffers": 176.6, "VM_pgrefill_normal":
												0, "CPU02_User": 2, "NETPACK_lo_write": 0, "PROC_read": -1,
												"PROC_Runnable": 2, "VM_pgrefill_high": 0, "PROC_exec": -1,
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- /container -->

	<footer>
		<%@ include file="../common/footer.jsp"%>
	</footer>

</body>
</html>