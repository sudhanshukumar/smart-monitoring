package com.lnt.smartit.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.lnt.smartit.mysql.model.AppDeviceMaster;
import com.lnt.smartit.mysql.model.Applications;
import com.lnt.smartit.mysql.model.DeviceParams;
import com.lnt.smartit.mysql.model.DeviceType;
import com.lnt.smartit.mysql.model.Managedevice;
import com.lnt.smartit.services.DeviceService;
import com.lnt.smartit.services.LogService;
import com.lnt.smartit.solr.pojo.JqGridData;
import com.lnt.smartit.util.OSTypeEnum;

@Controller
public class DeviceController {
	@Autowired
	private DeviceService deviceService;
	
	@Autowired
	private LogService logService;
	
	private static Logger logger = Logger.getLogger(DeviceController.class);

	@RequestMapping(value = "manageDevice.html")
	public ModelAndView manageThreshold() {
		return new ModelAndView("Manage/manageDevice");
	}
	
	@RequestMapping(value="populateDeviceGrid.html", method = RequestMethod.POST)
	public @ResponseBody JqGridData getThresholds(JqGridData jqGridData,HttpServletRequest request) {
		try{
		HttpSession session= request.getSession();
		if(session.getAttribute("appId")==null){
			String contextPath = request.getContextPath();
			String[] temp = contextPath.split("_");
			String appId = logService.getAppIdByName(temp[temp.length-1]);
			String appName=temp[temp.length-1];
			session.setAttribute("appId",appId);
			session.setAttribute("appname", appName);
		}
		String appId=session.getAttribute("appId").toString();
		List<DeviceParams> devices = new ArrayList<DeviceParams>();
		List<Managedevice> rowdata = new ArrayList<Managedevice>();
		devices = (List<DeviceParams>) deviceService.getDeviceParams(appId);
		
		for(int i=0;i<devices.size();i++){
			Managedevice managedevice = new Managedevice();
			List<DeviceParams> devicesWithSameId = new ArrayList<DeviceParams>();
			String id = devices.get(i).getAppDevice().getDeviceId();
		
			String deviceparamId=devices.get(i).getDeviceParamId();
			managedevice.setDeviceid(id);
		    managedevice.setDeviceparamid(deviceparamId);
			if(rowdata.size() != 0){
			loop1:
			for(int a=0;a<rowdata.size();a++){
				if(id.equalsIgnoreCase(rowdata.get(a).getDeviceid())){
					break loop1;
				}
				else{
					if(a == rowdata.size()-1){
						for(int j=0;j<devices.size();j++){
							if(devices.get(j).getAppDevice().getDeviceId().equalsIgnoreCase(id)){
								devicesWithSameId.add(devices.get(j));
							}
						}
						for(int k =0;k<devicesWithSameId.size();k++){
							if(devicesWithSameId.get(k).getName().equalsIgnoreCase("hostname")){
								managedevice.setHostname(devicesWithSameId.get(k).getValue());
							}
							else if(devicesWithSameId.get(k).getName().equalsIgnoreCase("ip address")){
								managedevice.setIp(devicesWithSameId.get(k).getValue());
							}
							else if(devicesWithSameId.get(k).getName().equalsIgnoreCase("description")){
								managedevice.setDescription(devicesWithSameId.get(k).getValue());					
							}
						}
						rowdata.add(managedevice);
					}
				}
			}
		}
		else{
			for(int j=0;j<devices.size();j++){
				if(devices.get(j).getAppDevice().getDeviceId().equalsIgnoreCase(id)){
					devicesWithSameId.add(devices.get(j));
				}
			}
			for(int k =0;k<devicesWithSameId.size();k++){
				if(devicesWithSameId.get(k).getName().equalsIgnoreCase("hostname")){
					managedevice.setHostname(devicesWithSameId.get(k).getValue());
				}
				else if(devicesWithSameId.get(k).getName().equalsIgnoreCase("ip address")){
					managedevice.setIp(devicesWithSameId.get(k).getValue());
				}
				else if(devicesWithSameId.get(k).getName().equalsIgnoreCase("description")){
					managedevice.setDescription(devicesWithSameId.get(k).getValue());					
				}
			}
			rowdata.add(managedevice);
		}
			
		}
		
	    jqGridData.setGridData(rowdata.toArray());
	    jqGridData.setRows(rowdata.size());
	    jqGridData.setRecords(rowdata.size());
		}catch(Exception e){
			e.printStackTrace();
		}
	    return jqGridData;
	  }
	
	@RequestMapping("device.html")
	public String getAllDeviceMasterRecords(ModelMap map,HttpSession session, HttpServletRequest request) {
		
	session= request.getSession();
	String appId=session.getAttribute("appId").toString();
	String appName=session.getAttribute("appname").toString();
		HashMap<String,String> ApplicationNameMap = new HashMap<String,String>();
		logger.debug("in DeviceMasterController getAllDeviceMasterRecords() ");
		List<OSTypeEnum> osList = new ArrayList<OSTypeEnum>(Arrays.asList(OSTypeEnum.values()));
		logger.debug("oslist "+osList.toString());
		ApplicationNameMap.put(appId,appName);
		map.addAttribute("applicationMaster", ApplicationNameMap);
		map.addAttribute("osList",osList);
		return "Manage/new/newDevice";
	}
	
	/**
	 * @author 298038
	 * addNewDeviceOnSubmit method is used to save new device deatils to DB . it gets all device details in pojo and gets 
	 * software id and application id which is persisted in application device mapping and software device mapping. Device deatils are
	 * persisted in Device master. 
	 * */
	@RequestMapping(value = "addNewDeviceOnSubmit.html", method = RequestMethod.POST)
	public @ResponseBody String addNewDeviceOnSubmit(@Valid Managedevice managedevice,BindingResult bindingResult,
			HttpSession session, HttpServletRequest request) {
		
		 session = request.getSession();
		logger.debug("Entering DeviceMasterController-->addNewDeviceSubmit()");
		logger.debug(managedevice.toString());
		if(managedevice.getDeviceid().equalsIgnoreCase("null"))
		{
       int maxvalue=deviceService.getMaxAppDeviceId();
		AppDeviceMaster appdevicemaster=new AppDeviceMaster();
		appdevicemaster.setDeviceId(String.valueOf(maxvalue++));
		DeviceParams deviceParams1=new DeviceParams(appdevicemaster,"hostname",managedevice.getHostname(),"n");
		DeviceParams deviceParams2=new DeviceParams(appdevicemaster,"ip address",managedevice.getIp(),"n");
		DeviceParams deviceParams3=new DeviceParams(appdevicemaster,"description",managedevice.getDescription(),"n");
		
	List<DeviceParams> deviceparamlist= new ArrayList<DeviceParams>();
	deviceparamlist.add(deviceParams1);
	deviceparamlist.add(deviceParams2);
	deviceparamlist.add(deviceParams3);
	
    appdevicemaster.setAliasName(managedevice.getOs());
    Applications applications=new Applications();
    applications.setAppId(session.getAttribute("appId").toString());
    
    appdevicemaster.setApp(applications);
    appdevicemaster.setVoidInd("n");
    DeviceType devicetype=new DeviceType();
    String searchCriteria=managedevice.getOs();
    String deviceTypecode= deviceService.getDeviceTypecode(searchCriteria);
    devicetype.setDeviceTypeCode(deviceTypecode);
    appdevicemaster.setDevicetype(devicetype);
    appdevicemaster.setDeviceName(managedevice.getHostname());
    
    deviceService.saveAppDeviceMaster(appdevicemaster);
	Iterator<DeviceParams> deviceIterator =deviceparamlist.iterator();
	while(deviceIterator.hasNext()){
		deviceService.saveDeviceparams(deviceIterator.next());
	}
		}
		else
		{ 
			 Applications applications=new Applications();
			 AppDeviceMaster appdevicemaster1=new AppDeviceMaster();
			    applications.setAppId(session.getAttribute("appId").toString());
			    appdevicemaster1.setDeviceId(managedevice.getDeviceid());
			    appdevicemaster1.setApp(applications);
			    appdevicemaster1.setVoidInd("n");
			 appdevicemaster1.setAliasName(managedevice.getOs());
			 DeviceType devicetype=new DeviceType();
			    String searchCriteria=managedevice.getOs();
			   String deviceTypecode= deviceService.getDeviceTypecode(searchCriteria);
			   devicetype.setDeviceTypeCode(deviceTypecode);
			    appdevicemaster1.setDevicetype(devicetype);
			    appdevicemaster1.setDeviceName(managedevice.getHostname());
			deviceService.updateAppDeviceMaster(appdevicemaster1);
		    List<DeviceParams> deviceparamslist=new ArrayList<DeviceParams>();
		    DeviceParams deviceParams=new DeviceParams();
		    AppDeviceMaster appdevicemaster= new AppDeviceMaster();
		    appdevicemaster.setDeviceId(managedevice.getDeviceid());
		    deviceParams.setAppDevice(appdevicemaster);
		    deviceParams.setName("hostname");
		    deviceParams.setValue(managedevice.getHostname());
		    deviceParams.setVoidInd("n");
		    deviceparamslist.add(deviceParams);
		    DeviceParams deviceParams1=new DeviceParams();
		    deviceParams1.setAppDevice(appdevicemaster);
		    deviceParams1.getAppDevice().setDeviceId(managedevice.getDeviceid());
		    deviceParams1.setName("ip address");
		    deviceParams1.setValue(managedevice.getIp());
		    deviceParams1.setVoidInd("n");
		    deviceparamslist.add(deviceParams1);
		    DeviceParams deviceParams2=new DeviceParams();
		    deviceParams2.setAppDevice(appdevicemaster);
		    deviceParams2.getAppDevice().setDeviceId(managedevice.getDeviceid());
		    deviceParams2.setName("description");
		    deviceParams2.setValue(managedevice.getDescription());
		    deviceParams2.setVoidInd("n");
		    deviceparamslist.add(deviceParams2);
		    Iterator<DeviceParams> deviceIterator1 =deviceparamslist.iterator();
			while(deviceIterator1.hasNext()){
				deviceService.updateDeviceparams(deviceIterator1.next());
		}
	}
		return " ";
	}
	
	/**
	 * @author 298038
	 * @param applicationMaster
	 * @return
	 */
	@RequestMapping(value = "deleteDevice.html", method = RequestMethod.POST)
	public @ResponseBody String deleteDevice(String deviceid){
		AppDeviceMaster appdevicemaster = new AppDeviceMaster();
		appdevicemaster.setDeviceId(deviceid);
		System.out.println("id==="+appdevicemaster.getDeviceId());
		String deleteDeviceMsg=null;
		try{
			 deviceService.deleteDeviceParams(appdevicemaster.getDeviceId());
			
	         deviceService.deleteAppDeviceMaster(appdevicemaster);
		
		}catch(Exception e ){
			e.printStackTrace();
			deleteDeviceMsg="Delete Device failed due to "+e.getMessage();
		}
		return deleteDeviceMsg;
	}
	
	@RequestMapping(value = "editDevice.html", method = RequestMethod.POST)
	public @ResponseBody Managedevice  editDevice(String deviceid,HttpServletRequest request){
		System.out.println("device id in editdevice.html"+deviceid);
		Managedevice formdata = new Managedevice();
		List<DeviceParams> deviceParamsById = deviceService.getDeviceParamsById(deviceid);
		logger.debug("Entering DeviceMasterController-->editDevice()");
		logger.debug("deviceParamsById "+deviceParamsById);
       AppDeviceMaster  getOsById = deviceService.getOsById(deviceid);
       System.out.println("appdevicemaster"+getOsById.toString());
		formdata.setOs(getOsById.getAliasName());
		
		System.out.println("alias name"+getOsById.getAliasName());
		for(int i=0;i<deviceParamsById.size();i++){
		
			if(deviceParamsById.get(i).getName().equalsIgnoreCase("hostname")){
				formdata.setHostname(deviceParamsById.get(i).getValue());
			}
			if(deviceParamsById.get(i).getName().equalsIgnoreCase("ip address")){
				formdata.setIp(deviceParamsById.get(i).getValue());
			}
			if(deviceParamsById.get(i).getName().equalsIgnoreCase("description")){
				formdata.setDescription(deviceParamsById.get(i).getValue());
			}
		}
		return formdata;
	}
}
