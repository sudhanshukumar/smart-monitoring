package com.lnt.smartit.models;

import java.util.Date;

public class JavaLog {

	private String logType;
	private String errorType;
	private Date date;
	private String message;
	private String hostName;
	
	public String getLogType() {
		return logType;
	}
	public void setLogType(String logType) {
		this.logType = logType;
	}
	public String getErrorType() {
		return errorType;
	}
	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	@Override
	public String toString() {
		return "JavaLog [logType=" + logType + ", errorType=" + errorType
				+ ", date=" + date + ", message=" + message + ", hostName="
				+ hostName + "]";
	}
}
