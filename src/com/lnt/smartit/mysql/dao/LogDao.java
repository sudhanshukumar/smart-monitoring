package com.lnt.smartit.mysql.dao;

import java.util.List;

import com.lnt.smartit.mysql.model.AppLogServiceComponentParam;
import com.lnt.smartit.mysql.model.AppServiceCompParams;
import com.lnt.smartit.mysql.model.LogParams;
import com.lnt.smartit.solr.pojo.Query;

public interface LogDao {
	
	public LogParams getAppLogParamsByLogIdAndParam(String logId, String paramName);

	public String getLogTypeByOS(String os);

	public String getAppLogByHostAndLogType(String appId, String hostname,String logType);

	public String getAppServiceComponentId(String appId, String compType,String compCode);

	public String getLogServiceComponentParamValue(String logId,String appCompServiceId);

	public String getOSFromDeviceId(String deviceId);

	public String getAppIdByName(String appName);
	
	public List<AppLogServiceComponentParam> getCollectionNames(String appId);
	
	public List<AppServiceCompParams> getDbConfig(String appId);
	
	public List<String> getTopicName(String serviceCompId);

	public List<Object> getBasicQueryNoFilter(Query uquery) throws Exception;

	public List<Object> getBasicQueryForLogs(Query uquery) throws Exception;

}
