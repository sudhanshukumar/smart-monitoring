<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title><spring:message code="views.dashboard.user.title"></spring:message>
</title>
<!-- jQuery scripts -->
<script src="scripts/common/jquery-1.9.1.js"></script>
<script src="scripts/common/jquery-ui.js"></script>

<!-- jQuery style sheets -->
<link href="css/common/jquery-ui.css" rel="stylesheet" />


<!-- Common stylesheets -->

<link href="css/custom/common/style1366.css" rel="stylesheet" />
<link href="css/custom/common/style1024.css" rel="stylesheet" />
<link href="css/custom/general/bluetabs.css" rel="stylesheet" />

<!-- Custom stylesheets -->
<link rel="stylesheet" href="css/custom/specific/dashboard.css">

<!-- Custom scripts -->
<script src="scripts/custom/dashboard.js"></script>
<script type="text/javascript" src="scripts/common/menu/dropdowntabs.js"></script>
</head>
<body>
	<header>
		<%@ include file="../common/header.jsp"%>
	</header>

	<nav id="bluemenu" class="bluetabs">
		<ul>
			<li><a id="selected" href="dashboard.html"><spring:message
						code="label.admin.menu1"></spring:message></a></li>
			<li><a href="problemHistory.html"><spring:message
						code="label.admin.menu2"></spring:message></a></li>
			<li><a href="#" rel="dropmenu1_b"><spring:message
						code="label.admin.menu3"></spring:message></a></li>
			<li><a href="manageNotificationGroup.html"><spring:message
						code="label.admin.menu4"></spring:message></a></li>
			<li><a href="variableAnalysis.html"><spring:message
						code="label.admin.menu5"></spring:message></a></li>
			<li><a href="#" rel="dropmenu1_solr"><spring:message
						code="label.admin.solr.reports"></spring:message></a></li>
			<li><a  href="query.html"><spring:message
						code="label.admin.solr.query"></spring:message></a></li>
						<li><a  href="correlated_query.html">Correlated query</a></li>
		</ul>
	</nav>
	<div class="ddcolortabsline">&nbsp;</div>

	<div id="dropmenu1_b" class="dropmenudiv_b">
		<a href="manageDevice.html"><spring:message
				code="label.admin.menu3.submenu1"></spring:message></a> 
		<a href="manageThresHold.html"><spring:message
				code="label.admin.menu3.submenu2"></spring:message></a>
		<a href="manageApplication.html"><spring:message
				code="label.admin.menu3.submenu3"></spring:message></a> 
		<a href="manageService.html"><spring:message
				code="label.admin.menu3.submenu4"></spring:message></a> 
		<a href="manageUsers.html"><spring:message
				code="label.admin.menu3.submenu5"></spring:message></a>
		<a href="manageLog.html"><spring:message
				code="label.admin.menu3.submenu6"></spring:message></a>
	</div>
	
	<div id="dropmenu1_solr" class="dropmenudiv_b">
		<a href="userdashboard.html"><spring:message
				code="label.admin.solr.dashboard"></spring:message></a> <a
			href="charts.html"><spring:message
				code="label.admin.solr.charts"></spring:message></a> <a
			href="createuserdashboard.html"><spring:message
				code="label.admin.solr.custom.dashboard"></spring:message></a> <a
			href="list_dashboard.html"><spring:message
				code="label.admin.solr.view.dashboard"></spring:message></a> 
	</div>

	<script type="text/javascript">
		//SYNTAX: tabdropdown.init("menu_id", [integer OR "auto"])
		tabdropdown.init("bluemenu")
	</script>
	<div class="ac-container">
		<div>
			<input id="ac-1" name="accordion-1" type="checkbox" /> <label
				for="ac-1"><spring:message code="accordian.label.first"></spring:message>
			</label>
			<section class="ac-small">
				<div class="block_small">
					<div class="block_split1" id="ac-1_split1">
						<table>
							<thead>
								<tr>
									<th><spring:message code="left_block_split1.header"></spring:message>
										172.25.38.49</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><spring:message code="left_block_split1.column1"></spring:message>
										80%</td>
								</tr>
							</tbody>

						</table>
					</div>
					<div class="block_split2" id="ac-1_split2"></div>
						<div class="block_split3" id="ac-1_split3">

					<div id="ac-1_split3_1">
						<h3>
							<spring:message code="ac-1_split3_1.header"></spring:message>
						</h3>
						<table>

							<tbody>
								<tr>
									<td><strong><spring:message
												code="ac-1_split3_1.table1.column1"></spring:message></strong></td>
									<td>1502</td>
								</tr>
								<tr>
									<td><strong><spring:message
												code="ac-1_split3_1.table1.column2"></spring:message></strong></td>
									<td>cloudx-1326</td>
								</tr>
								<tr>
									<td><strong><spring:message
												code="ac-1_split3_1.table1.column3"></spring:message></strong></td>
									<td>1502</td>
								</tr>
								<tr>
									<td><strong><spring:message
												code="ac-1_split3_1.table1.column4"></spring:message></strong></td>
									<td>1502</td>
								</tr>
								<tr>
									<td><strong><spring:message
												code="ac-1_split3_1.table1.column5"></spring:message></strong></td>
									<td>1502</td>
								</tr>
								<tr>
									<td><strong><spring:message
												code="ac-1_split3_1.table1.column6"></spring:message></strong></td>
									<td>1502</td>
								</tr>
							</tbody>
						</table>
						
					</div>

					<div id="ac-1_split3_2">
						<h3>
							<spring:message code="ac-1_split3_2.header"></spring:message>
						</h3>
						<table>

							<tbody>
								<tr>
									<td><strong><spring:message
												code="ac-1_split3_2.table2.column1"></spring:message></strong></td>
									<td>1502</td>
								</tr>
								<tr>
									<td><strong><spring:message
												code="ac-1_split3_2.table2.column2"></spring:message></strong></td>
									<td>cloudx-1326</td>
								</tr>
								<tr>
									<td><strong><spring:message
												code="ac-1_split3_2.table2.column3"></spring:message></strong></td>
									<td>1502</td>
								</tr>
								<tr>
									<td><strong><spring:message
												code="ac-1_split3_2.table2.column4"></spring:message></strong></td>
									<td>1502</td>
								</tr>
								<tr>
									<td><strong><spring:message
												code="ac-1_split3_2.table2.column5"></spring:message></strong></td>
									<td>1502</td>
								</tr>
								<tr>
									<td><strong><spring:message
												code="ac-1_split3_2.table2.column6"></spring:message></strong></td>
									<td>1502</td>
								</tr>
								<tr>
									<td><strong><spring:message
												code="ac-1_split3_2.table2.column7"></spring:message></strong></td>
									<td>1502</td>
								</tr>
								<tr>
									<td><strong><spring:message
												code="ac-1_split3_2.table2.column8"></spring:message></strong></td>
									<td>1502</td>
								</tr>
								<tr>
									<td><strong><spring:message
												code="ac-1_split3_2.table2.column9"></spring:message></strong></td>
									<td>1502</td>
								</tr>
								<tr>
									<td><strong><spring:message
												code="ac-1_split3_2.table2.column10"></spring:message></strong></td>
									<td>1502</td>
								</tr>
								<tr>
									<td><strong><spring:message
												code="ac-1_split3_2.table2.column11"></spring:message></strong></td>
									<td>1502</td>
								</tr>
							</tbody>
						</table>
					</div>
				
							<div id="ac-1_split3_3">
						<h3>
							<spring:message code="ac-1_split3_3.header"></spring:message>
						</h3>
						<table>

							<tbody>
								<tr>
									<td><strong><spring:message
												code="ac-1_split3_3.table3.header"></spring:message></strong></td>
									<td>
										<table>
											<tr>
												<td><strong><spring:message
															code="ac-1_split3_3.table3.1.1"></spring:message></strong></td>
												<td>1500</td>
											</tr>
											<tr>
												<td><strong><spring:message
															code="ac-1_split3_3.table3.1.2"></spring:message></strong></td>
												<td>1500</td>
											</tr>
											<tr>
												<td><strong><spring:message
															code="ac-1_split3_3.table3.1.3"></spring:message></strong></td>
												<td>1500</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td><strong><spring:message
												code="ac-1_split3_3.table3.column2"></spring:message></strong></td>
									<td>cloudx-1326</td>
								</tr>
								<tr>
									<td><strong><spring:message
												code="ac-1_split3_3.table3.column3"></spring:message></strong></td>
									<td>1502</td>
								</tr>

							</tbody>
						</table>
					</div>
				</div>
				</div>
			</section>
		</div>

		<div>
			<input id="ac-2" name="accordion-2" type="checkbox" /> <label
				for="ac-2">CPU</label>
			<section class="ac-small">
				<p>This is section 2</p>
			</section>

		</div>
	</div>


	<footer>
		<%@ include file="../common/footer.jsp"%>
	</footer>

</body>
</html>