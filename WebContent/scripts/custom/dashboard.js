AC1Split1Count = 0;
AC2Split1Count = 0;
AC3Split1Count = 0;
AC4Split1Count = 0;
AC5Split1Count = 0;
AC6Split1Count = 0;
AC7Split1Count = 0;
AC8Split1Count = 0;
AC9Split1Count = 0;
$(document).ready(function() {
	$("#sysHealthy").show();
	$("#ac1_container_small").hide();
	$("#ac2_container_small").hide();
	$("#ac3_container_small").hide();
	$("#ac4_container_small").hide();
	$("#ac5_container_small").hide();
	$("#ac7_container_small").hide();
	$("#ac8_container_small").hide();
	$("#ac9_container_small").hide();
	/* Hiding all accordians on load */
	$("#label_ac-1").hide();
	$("#label_ac-2").hide();
	$("#label_ac-3").hide();
	$("#label_ac-4").hide();
	$("#label_ac-5").hide();
	$("#label_ac-6").hide();
	$("#label_ac-7").hide();
	$("#label_ac-8").hide();
	$("#label_ac-9").hide();
	
	/* plus minus images of accordians show/hide starts */
	$("#ac-1_container_minus").hide();
	$("#ac-1_container_plus").show();
	$("#ac-2_container_minus").hide();
	$("#ac-2_container_plus").show();
	$("#ac-3_container_minus").hide();
	$("#ac-3_container_plus").show();
	$("#ac-4_container_minus").hide();
	$("#ac-4_container_plus").show();
	$("#ac-5_container_minus").hide();
	$("#ac-5_container_plus").show();
	$("#ac-6_container_minus").hide();
	$("#ac-6_container_plus").show();
	$("#ac-7_container_minus").hide();
	$("#ac-7_container_plus").show();
	$("#ac-8_container_minus").hide();
	$("#ac-8_container_plus").show();
	$("#ac-9_container_minus").hide();
	$("#ac-9_container_plus").show();

	$("#ac-1").click(function() {
		$("#ac1_block_2 div").empty();
		if ($('#ac-1_container_plus').is(':visible')) {

			$("#ac-1_container_minus").show();
			$("#ac-1_container_plus").hide();

		} else if ($('#ac-1_container_minus').is(':visible')) {
			$("#ac-1_container_minus").hide();
			$("#ac-1_container_plus").show();
		}
	});
	$("#ac-2").click(function() {
		$("#ac2_block_2 div").empty();
		if ($('#ac-2_container_plus').is(':visible')) {

			$("#ac-2_container_minus").show();
			$("#ac-2_container_plus").hide();

		} else if ($('#ac-2_container_minus').is(':visible')) {
			$("#ac-2_container_minus").hide();
			$("#ac-2_container_plus").show();
		}
	});
	$("#ac-3").click(function() {
		$("#ac3_block_2 div").empty();
		if ($('#ac-3_container_plus').is(':visible')) {

			$("#ac-3_container_minus").show();
			$("#ac-3_container_plus").hide();

		} else if ($('#ac-3_container_minus').is(':visible')) {
			$("#ac-3_container_minus").hide();
			$("#ac-3_container_plus").show();
		}
	});
	$("#ac-4").click(function() {
		$("#ac4_container_small_second div").empty();
		if ($('#ac-4_container_plus').is(':visible')) {

			$("#ac-4_container_minus").show();
			$("#ac-4_container_plus").hide();

		} else if ($('#ac-4_container_minus').is(':visible')) {
			$("#ac-4_container_minus").hide();
			$("#ac-4_container_plus").show();
		}
	});
	$("#ac-5").click(function() {
		$("#ac5_container_small_second div").empty();
		if ($('#ac-5_container_plus').is(':visible')) {

			$("#ac-5_container_minus").show();
			$("#ac-5_container_plus").hide();

		} else if ($('#ac-5_container_minus').is(':visible')) {
			$("#ac-5_container_minus").hide();
			$("#ac-5_container_plus").show();
		}
	});
	
	$("#ac-6").click(function() {
		$("#ac6_container_small_second div").empty();
		if ($('#ac-6_container_plus').is(':visible')) {

			$("#ac-6_container_minus").show();
			$("#ac-6_container_plus").hide();

		} else if ($('#ac-6_container_minus').is(':visible')) {
			$("#ac-6_container_minus").hide();
			$("#ac-6_container_plus").show();
		}
	});

	$("#ac-7").click(function() {
		$("#ac7_container_small_second div").empty();
		if ($('#ac-7_container_plus').is(':visible')) {

			$("#ac-7_container_minus").show();
			$("#ac-7_container_plus").hide();

		} else if ($('#ac-7_container_minus').is(':visible')) {
			$("#ac-7_container_minus").hide();
			$("#ac-7_container_plus").show();
		}
	});
	$("#ac-8").click(function() {
		$("#ac8_container_small_second div").empty();
		if ($('#ac-8_container_plus').is(':visible')) {

			$("#ac-8_container_minus").show();
			$("#ac-8_container_plus").hide();

		} else if ($('#ac-8_container_minus').is(':visible')) {
			$("#ac-8_container_minus").hide();
			$("#ac-8_container_plus").show();
		}
	});
	$("#ac-9").click(function() {
		$("#ac9_container_small_second div").empty();
		if ($('#ac-9_container_plus').is(':visible')) {

			$("#ac-9_container_minus").show();
			$("#ac-9_container_plus").hide();

		} else if ($('#ac-9_container_minus').is(':visible')) {
			$("#ac-9_container_minus").hide();
			$("#ac-9_container_plus").show();
		}
	});

	/* plus minus images of accordians show/hide ends */
	
	
		//---------- very imp below json
	//refreshAccordians('[{"description":"RAM = Free : 20325 Memory Buffers : 918 Cached : 9686 Free Memory : 324 Shared Memory : 0 Total Memory : 15953 Used Memory : 15629 Used RAM : 16132 Total : 36457 High Threshold : 50.0 Mb Low Threshold : 20.0 Mb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"RAM","alertMessageType":"CRITICAL","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"RAM","logFileStatus":[],"osType":"Linux","linuxRAM":{"total":36457,"used":16132,"memTotal":15953,"memUsed":15629,"memFree":324,"memBuffers":918,"memShared":0,"memCached":9686,"free":20325,"threshold_high_val":20.0,"threshold_low_val":50.0,"thresholdlimit_MemTotal":null,"thresholdlimit_MemUsed":null,"thresholdlimit_MemFree":null,"thresholdlimit_MemBuffers":null,"thresholdlimit_MemShared":null,"thresholdlimit_MemCached":null,"thresholdlimit_Total":null,"thresholdlimit_Used":null,"thresholdlimit_Free":null,"dataReceivedTime_hours":0},"linuxCPU":null,"linuxDisk":null,"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"CPU Load Average for 5 mins : 0.1 CPU Load Average for 10 mins : 0.2 CPU Load Average for 15 mins : 0.3 High Threshold : 0.0 Low Threshold : 0.0","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"CPU","alertMessageType":"CRITICAL","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"CPU","logFileStatus":[],"osType":"Linux","linuxRAM":null,"linuxCPU":{"threshold_high_val":0.0,"threshold_low_val":0.0,"dataReceivedTime_hours":0,"threshold_loadAverage5min":null,"threshold_loadAverage10min":null,"threshold_loadAverage15min":null,"loadAverage5min":0.1,"loadAverage10min":0.2,"loadAverage15min":0.3},"linuxDisk":null,"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"Total Used : 12.0 Total Size :17.0 Used Percent : 72.0 Total Available : 4.8 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"DISK","logFileStatus":[],"osType":"Linux","linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":17.0,"used":12.0,"avail":4.8,"usePercent":72.0,"mountedOn":"/","dataReceivedTime_hours":0},{"name":"tmpfs","size":7.8,"used":264.0,"avail":7.8,"usePercent":1.0,"mountedOn":"/dev/shm","dataReceivedTime_hours":0},{"name":"/dev/sda1","size":194.0,"used":33.0,"avail":152.0,"usePercent":18.0,"mountedOn":"/boot","dataReceivedTime_hours":0},{"name":"/dev/sdb2","size":50.0,"used":31.0,"avail":17.0,"usePercent":65.0,"mountedOn":"/data","dataReceivedTime_hours":0},{"name":"/dev/sdc3","size":50.0,"used":23.0,"avail":25.0,"usePercent":48.0,"mountedOn":"/usr/sap","dataReceivedTime_hours":0},{"name":"/dev/sdd2","size":138.0,"used":105.0,"avail":27.0,"usePercent":80.0,"mountedOn":"/sybase","dataReceivedTime_hours":0},{"name":"/dev/sde4","size":20.0,"used":2.3,"avail":17.0,"usePercent":13.0,"mountedOn":"/sapmnt","dataReceivedTime_hours":0},{"name":"/dev/sdf","size":50.0,"used":28.0,"avail":20.0,"usePercent":59.0,"mountedOn":"/oracle","dataReceivedTime_hours":0}],"thresholdlimit_TotalUsePercent":null,"totalUsePercent":63.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"totalSize":330.0,"totalUsed":198.0,"totalAvail":117.0},"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"Total Used : 264.0 Total Size :7.8 Used Percent : 1.0 Total Available : 7.8 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"WARNING","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"DISK","logFileStatus":[],"osType":"Linux","linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":17.0,"used":12.0,"avail":4.8,"usePercent":72.0,"mountedOn":"/","dataReceivedTime_hours":0},{"name":"tmpfs","size":7.8,"used":264.0,"avail":7.8,"usePercent":1.0,"mountedOn":"/dev/shm","dataReceivedTime_hours":0},{"name":"/dev/sda1","size":194.0,"used":33.0,"avail":152.0,"usePercent":18.0,"mountedOn":"/boot","dataReceivedTime_hours":0},{"name":"/dev/sdb2","size":50.0,"used":31.0,"avail":17.0,"usePercent":65.0,"mountedOn":"/data","dataReceivedTime_hours":0},{"name":"/dev/sdc3","size":50.0,"used":23.0,"avail":25.0,"usePercent":48.0,"mountedOn":"/usr/sap","dataReceivedTime_hours":0},{"name":"/dev/sdd2","size":138.0,"used":105.0,"avail":27.0,"usePercent":80.0,"mountedOn":"/sybase","dataReceivedTime_hours":0},{"name":"/dev/sde4","size":20.0,"used":2.3,"avail":17.0,"usePercent":13.0,"mountedOn":"/sapmnt","dataReceivedTime_hours":0},{"name":"/dev/sdf","size":50.0,"used":28.0,"avail":20.0,"usePercent":59.0,"mountedOn":"/oracle","dataReceivedTime_hours":0}],"thresholdlimit_TotalUsePercent":null,"totalUsePercent":63.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"totalSize":330.0,"totalUsed":198.0,"totalAvail":117.0},"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"Total Used : 33.0 Total Size :194.0 Used Percent : 18.0 Total Available : 152.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"WARNING","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"DISK","logFileStatus":[],"osType":"Linux","linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":17.0,"used":12.0,"avail":4.8,"usePercent":72.0,"mountedOn":"/","dataReceivedTime_hours":0},{"name":"tmpfs","size":7.8,"used":264.0,"avail":7.8,"usePercent":1.0,"mountedOn":"/dev/shm","dataReceivedTime_hours":0},{"name":"/dev/sda1","size":194.0,"used":33.0,"avail":152.0,"usePercent":18.0,"mountedOn":"/boot","dataReceivedTime_hours":0},{"name":"/dev/sdb2","size":50.0,"used":31.0,"avail":17.0,"usePercent":65.0,"mountedOn":"/data","dataReceivedTime_hours":0},{"name":"/dev/sdc3","size":50.0,"used":23.0,"avail":25.0,"usePercent":48.0,"mountedOn":"/usr/sap","dataReceivedTime_hours":0},{"name":"/dev/sdd2","size":138.0,"used":105.0,"avail":27.0,"usePercent":80.0,"mountedOn":"/sybase","dataReceivedTime_hours":0},{"name":"/dev/sde4","size":20.0,"used":2.3,"avail":17.0,"usePercent":13.0,"mountedOn":"/sapmnt","dataReceivedTime_hours":0},{"name":"/dev/sdf","size":50.0,"used":28.0,"avail":20.0,"usePercent":59.0,"mountedOn":"/oracle","dataReceivedTime_hours":0}],"thresholdlimit_TotalUsePercent":null,"totalUsePercent":63.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"totalSize":330.0,"totalUsed":198.0,"totalAvail":117.0},"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"Total Used : 31.0 Total Size :50.0 Used Percent : 65.0 Total Available : 17.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"DISK","logFileStatus":[],"osType":"Linux","linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":17.0,"used":12.0,"avail":4.8,"usePercent":72.0,"mountedOn":"/","dataReceivedTime_hours":0},{"name":"tmpfs","size":7.8,"used":264.0,"avail":7.8,"usePercent":1.0,"mountedOn":"/dev/shm","dataReceivedTime_hours":0},{"name":"/dev/sda1","size":194.0,"used":33.0,"avail":152.0,"usePercent":18.0,"mountedOn":"/boot","dataReceivedTime_hours":0},{"name":"/dev/sdb2","size":50.0,"used":31.0,"avail":17.0,"usePercent":65.0,"mountedOn":"/data","dataReceivedTime_hours":0},{"name":"/dev/sdc3","size":50.0,"used":23.0,"avail":25.0,"usePercent":48.0,"mountedOn":"/usr/sap","dataReceivedTime_hours":0},{"name":"/dev/sdd2","size":138.0,"used":105.0,"avail":27.0,"usePercent":80.0,"mountedOn":"/sybase","dataReceivedTime_hours":0},{"name":"/dev/sde4","size":20.0,"used":2.3,"avail":17.0,"usePercent":13.0,"mountedOn":"/sapmnt","dataReceivedTime_hours":0},{"name":"/dev/sdf","size":50.0,"used":28.0,"avail":20.0,"usePercent":59.0,"mountedOn":"/oracle","dataReceivedTime_hours":0}],"thresholdlimit_TotalUsePercent":null,"totalUsePercent":63.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"totalSize":330.0,"totalUsed":198.0,"totalAvail":117.0},"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"Total Used : 23.0 Total Size :50.0 Used Percent : 48.0 Total Available : 25.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"DISK","logFileStatus":[],"osType":"Linux","linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":17.0,"used":12.0,"avail":4.8,"usePercent":72.0,"mountedOn":"/","dataReceivedTime_hours":0},{"name":"tmpfs","size":7.8,"used":264.0,"avail":7.8,"usePercent":1.0,"mountedOn":"/dev/shm","dataReceivedTime_hours":0},{"name":"/dev/sda1","size":194.0,"used":33.0,"avail":152.0,"usePercent":18.0,"mountedOn":"/boot","dataReceivedTime_hours":0},{"name":"/dev/sdb2","size":50.0,"used":31.0,"avail":17.0,"usePercent":65.0,"mountedOn":"/data","dataReceivedTime_hours":0},{"name":"/dev/sdc3","size":50.0,"used":23.0,"avail":25.0,"usePercent":48.0,"mountedOn":"/usr/sap","dataReceivedTime_hours":0},{"name":"/dev/sdd2","size":138.0,"used":105.0,"avail":27.0,"usePercent":80.0,"mountedOn":"/sybase","dataReceivedTime_hours":0},{"name":"/dev/sde4","size":20.0,"used":2.3,"avail":17.0,"usePercent":13.0,"mountedOn":"/sapmnt","dataReceivedTime_hours":0},{"name":"/dev/sdf","size":50.0,"used":28.0,"avail":20.0,"usePercent":59.0,"mountedOn":"/oracle","dataReceivedTime_hours":0}],"thresholdlimit_TotalUsePercent":null,"totalUsePercent":63.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"totalSize":330.0,"totalUsed":198.0,"totalAvail":117.0},"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"Total Used : 105.0 Total Size :138.0 Used Percent : 80.0 Total Available : 27.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"DISK","logFileStatus":[],"osType":"Linux","linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":17.0,"used":12.0,"avail":4.8,"usePercent":72.0,"mountedOn":"/","dataReceivedTime_hours":0},{"name":"tmpfs","size":7.8,"used":264.0,"avail":7.8,"usePercent":1.0,"mountedOn":"/dev/shm","dataReceivedTime_hours":0},{"name":"/dev/sda1","size":194.0,"used":33.0,"avail":152.0,"usePercent":18.0,"mountedOn":"/boot","dataReceivedTime_hours":0},{"name":"/dev/sdb2","size":50.0,"used":31.0,"avail":17.0,"usePercent":65.0,"mountedOn":"/data","dataReceivedTime_hours":0},{"name":"/dev/sdc3","size":50.0,"used":23.0,"avail":25.0,"usePercent":48.0,"mountedOn":"/usr/sap","dataReceivedTime_hours":0},{"name":"/dev/sdd2","size":138.0,"used":105.0,"avail":27.0,"usePercent":80.0,"mountedOn":"/sybase","dataReceivedTime_hours":0},{"name":"/dev/sde4","size":20.0,"used":2.3,"avail":17.0,"usePercent":13.0,"mountedOn":"/sapmnt","dataReceivedTime_hours":0},{"name":"/dev/sdf","size":50.0,"used":28.0,"avail":20.0,"usePercent":59.0,"mountedOn":"/oracle","dataReceivedTime_hours":0}],"thresholdlimit_TotalUsePercent":null,"totalUsePercent":63.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"totalSize":330.0,"totalUsed":198.0,"totalAvail":117.0},"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"Total Used : 2.3 Total Size :20.0 Used Percent : 13.0 Total Available : 17.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"WARNING","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"DISK","logFileStatus":[],"osType":"Linux","linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":17.0,"used":12.0,"avail":4.8,"usePercent":72.0,"mountedOn":"/","dataReceivedTime_hours":0},{"name":"tmpfs","size":7.8,"used":264.0,"avail":7.8,"usePercent":1.0,"mountedOn":"/dev/shm","dataReceivedTime_hours":0},{"name":"/dev/sda1","size":194.0,"used":33.0,"avail":152.0,"usePercent":18.0,"mountedOn":"/boot","dataReceivedTime_hours":0},{"name":"/dev/sdb2","size":50.0,"used":31.0,"avail":17.0,"usePercent":65.0,"mountedOn":"/data","dataReceivedTime_hours":0},{"name":"/dev/sdc3","size":50.0,"used":23.0,"avail":25.0,"usePercent":48.0,"mountedOn":"/usr/sap","dataReceivedTime_hours":0},{"name":"/dev/sdd2","size":138.0,"used":105.0,"avail":27.0,"usePercent":80.0,"mountedOn":"/sybase","dataReceivedTime_hours":0},{"name":"/dev/sde4","size":20.0,"used":2.3,"avail":17.0,"usePercent":13.0,"mountedOn":"/sapmnt","dataReceivedTime_hours":0},{"name":"/dev/sdf","size":50.0,"used":28.0,"avail":20.0,"usePercent":59.0,"mountedOn":"/oracle","dataReceivedTime_hours":0}],"thresholdlimit_TotalUsePercent":null,"totalUsePercent":63.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"totalSize":330.0,"totalUsed":198.0,"totalAvail":117.0},"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"Total Used : 28.0 Total Size :50.0 Used Percent : 59.0 Total Available : 20.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"DISK","logFileStatus":[],"osType":"Linux","linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":17.0,"used":12.0,"avail":4.8,"usePercent":72.0,"mountedOn":"/","dataReceivedTime_hours":0},{"name":"tmpfs","size":7.8,"used":264.0,"avail":7.8,"usePercent":1.0,"mountedOn":"/dev/shm","dataReceivedTime_hours":0},{"name":"/dev/sda1","size":194.0,"used":33.0,"avail":152.0,"usePercent":18.0,"mountedOn":"/boot","dataReceivedTime_hours":0},{"name":"/dev/sdb2","size":50.0,"used":31.0,"avail":17.0,"usePercent":65.0,"mountedOn":"/data","dataReceivedTime_hours":0},{"name":"/dev/sdc3","size":50.0,"used":23.0,"avail":25.0,"usePercent":48.0,"mountedOn":"/usr/sap","dataReceivedTime_hours":0},{"name":"/dev/sdd2","size":138.0,"used":105.0,"avail":27.0,"usePercent":80.0,"mountedOn":"/sybase","dataReceivedTime_hours":0},{"name":"/dev/sde4","size":20.0,"used":2.3,"avail":17.0,"usePercent":13.0,"mountedOn":"/sapmnt","dataReceivedTime_hours":0},{"name":"/dev/sdf","size":50.0,"used":28.0,"avail":20.0,"usePercent":59.0,"mountedOn":"/oracle","dataReceivedTime_hours":0}],"thresholdlimit_TotalUsePercent":null,"totalUsePercent":63.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"totalSize":330.0,"totalUsed":198.0,"totalAvail":117.0},"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"RAM = Free : 4911 Memory Buffers : 385 Cached : 2936 Free Memory : 995 Shared Memory : 0 Total Memory : 7873 Used Memory : 6877 Used RAM : 6961 Total : 11873 High Threshold : 40.0 Mb Low Threshold : 20.0 Mb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"RAM","alertMessageType":"CRITICAL","alertMessageDatetime":1439181961000,"alertMessageHostName":"gc12c-oms.coelab.com","alertMessageparameterName":"RAM","logFileStatus":[],"linuxRAM":{"total":11873,"used":6961,"threshold_high_val":20.0,"threshold_low_val":40.0,"thresholdlimit_MemTotal":null,"thresholdlimit_MemUsed":null,"thresholdlimit_MemFree":null,"thresholdlimit_MemBuffers":null,"thresholdlimit_MemShared":null,"thresholdlimit_MemCached":null,"thresholdlimit_Total":null,"thresholdlimit_Used":null,"thresholdlimit_Free":null,"dataReceivedTime_hours":0,"memTotal":7873,"memUsed":6877,"memFree":995,"memBuffers":385,"memShared":0,"memCached":2936,"free":4911},"linuxCPU":null,"linuxDisk":null,"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Linux"},{"description":"CPU Load Average for 5 mins : 0.1 CPU Load Average for 10 mins : 0.08 CPU Load Average for 15 mins : 0.08 High Threshold : 0.0 Low Threshold : 0.0","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"CPU","alertMessageType":"CRITICAL","alertMessageDatetime":1439181961000,"alertMessageHostName":"gc12c-oms.coelab.com","alertMessageparameterName":"CPU","logFileStatus":[],"linuxRAM":null,"linuxCPU":{"threshold_high_val":0.0,"threshold_low_val":0.0,"dataReceivedTime_hours":0,"threshold_loadAverage5min":null,"threshold_loadAverage10min":null,"threshold_loadAverage15min":null,"loadAverage5min":0.1,"loadAverage10min":0.08,"loadAverage15min":0.08},"linuxDisk":null,"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Linux"},{"description":"Total Used : 13.0 Total Size :16.0 Used Percent : 86.0 Total Available : 2.2 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439181961000,"alertMessageHostName":"gc12c-oms.coelab.com","alertMessageparameterName":"DISK","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":16.0,"used":13.0,"avail":2.2,"dataReceivedTime_hours":0,"usePercent":86.0,"mountedOn":"/"},{"name":"tmpfs","size":3.9,"used":668.0,"avail":3.2,"dataReceivedTime_hours":0,"usePercent":17.0,"mountedOn":"/dev/shm"},{"name":"/dev/sda1","size":243.0,"used":33.0,"avail":198.0,"dataReceivedTime_hours":0,"usePercent":15.0,"mountedOn":"/boot"},{"name":"/dev/sdb1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u0"},{"name":"/dev/sdc1","size":50.0,"used":5.7,"avail":42.0,"dataReceivedTime_hours":0,"usePercent":13.0,"mountedOn":"/u1"},{"name":"/dev/sdd1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u2"}],"totalSize":168.0,"totalUsed":65.0,"totalAvail":95.0,"totalUsePercent":41.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"thresholdlimit_TotalUsePercent":null},"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Linux"},{"description":"Total Used : 668.0 Total Size :3.9 Used Percent : 17.0 Total Available : 3.2 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"WARNING","alertMessageDatetime":1439181961000,"alertMessageHostName":"gc12c-oms.coelab.com","alertMessageparameterName":"DISK","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":16.0,"used":13.0,"avail":2.2,"dataReceivedTime_hours":0,"usePercent":86.0,"mountedOn":"/"},{"name":"tmpfs","size":3.9,"used":668.0,"avail":3.2,"dataReceivedTime_hours":0,"usePercent":17.0,"mountedOn":"/dev/shm"},{"name":"/dev/sda1","size":243.0,"used":33.0,"avail":198.0,"dataReceivedTime_hours":0,"usePercent":15.0,"mountedOn":"/boot"},{"name":"/dev/sdb1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u0"},{"name":"/dev/sdc1","size":50.0,"used":5.7,"avail":42.0,"dataReceivedTime_hours":0,"usePercent":13.0,"mountedOn":"/u1"},{"name":"/dev/sdd1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u2"}],"totalSize":168.0,"totalUsed":65.0,"totalAvail":95.0,"totalUsePercent":41.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"thresholdlimit_TotalUsePercent":null},"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Linux"},{"description":"Total Used : 33.0 Total Size :243.0 Used Percent : 15.0 Total Available : 198.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"WARNING","alertMessageDatetime":1439181961000,"alertMessageHostName":"gc12c-oms.coelab.com","alertMessageparameterName":"DISK","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":16.0,"used":13.0,"avail":2.2,"dataReceivedTime_hours":0,"usePercent":86.0,"mountedOn":"/"},{"name":"tmpfs","size":3.9,"used":668.0,"avail":3.2,"dataReceivedTime_hours":0,"usePercent":17.0,"mountedOn":"/dev/shm"},{"name":"/dev/sda1","size":243.0,"used":33.0,"avail":198.0,"dataReceivedTime_hours":0,"usePercent":15.0,"mountedOn":"/boot"},{"name":"/dev/sdb1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u0"},{"name":"/dev/sdc1","size":50.0,"used":5.7,"avail":42.0,"dataReceivedTime_hours":0,"usePercent":13.0,"mountedOn":"/u1"},{"name":"/dev/sdd1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u2"}],"totalSize":168.0,"totalUsed":65.0,"totalAvail":95.0,"totalUsePercent":41.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"thresholdlimit_TotalUsePercent":null},"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Linux"},{"description":"Total Used : 23.0 Total Size :50.0 Used Percent : 49.0 Total Available : 25.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439181961000,"alertMessageHostName":"gc12c-oms.coelab.com","alertMessageparameterName":"DISK","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":16.0,"used":13.0,"avail":2.2,"dataReceivedTime_hours":0,"usePercent":86.0,"mountedOn":"/"},{"name":"tmpfs","size":3.9,"used":668.0,"avail":3.2,"dataReceivedTime_hours":0,"usePercent":17.0,"mountedOn":"/dev/shm"},{"name":"/dev/sda1","size":243.0,"used":33.0,"avail":198.0,"dataReceivedTime_hours":0,"usePercent":15.0,"mountedOn":"/boot"},{"name":"/dev/sdb1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u0"},{"name":"/dev/sdc1","size":50.0,"used":5.7,"avail":42.0,"dataReceivedTime_hours":0,"usePercent":13.0,"mountedOn":"/u1"},{"name":"/dev/sdd1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u2"}],"totalSize":168.0,"totalUsed":65.0,"totalAvail":95.0,"totalUsePercent":41.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"thresholdlimit_TotalUsePercent":null},"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Linux"},{"description":"Total Used : 5.7 Total Size :50.0 Used Percent : 13.0 Total Available : 42.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"WARNING","alertMessageDatetime":1439181961000,"alertMessageHostName":"gc12c-oms.coelab.com","alertMessageparameterName":"DISK","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":16.0,"used":13.0,"avail":2.2,"dataReceivedTime_hours":0,"usePercent":86.0,"mountedOn":"/"},{"name":"tmpfs","size":3.9,"used":668.0,"avail":3.2,"dataReceivedTime_hours":0,"usePercent":17.0,"mountedOn":"/dev/shm"},{"name":"/dev/sda1","size":243.0,"used":33.0,"avail":198.0,"dataReceivedTime_hours":0,"usePercent":15.0,"mountedOn":"/boot"},{"name":"/dev/sdb1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u0"},{"name":"/dev/sdc1","size":50.0,"used":5.7,"avail":42.0,"dataReceivedTime_hours":0,"usePercent":13.0,"mountedOn":"/u1"},{"name":"/dev/sdd1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u2"}],"totalSize":168.0,"totalUsed":65.0,"totalAvail":95.0,"totalUsePercent":41.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"thresholdlimit_TotalUsePercent":null},"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Linux"},{"description":"Total Used : 23.0 Total Size :50.0 Used Percent : 49.0 Total Available : 25.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439181961000,"alertMessageHostName":"gc12c-oms.coelab.com","alertMessageparameterName":"DISK","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":16.0,"used":13.0,"avail":2.2,"dataReceivedTime_hours":0,"usePercent":86.0,"mountedOn":"/"},{"name":"tmpfs","size":3.9,"used":668.0,"avail":3.2,"dataReceivedTime_hours":0,"usePercent":17.0,"mountedOn":"/dev/shm"},{"name":"/dev/sda1","size":243.0,"used":33.0,"avail":198.0,"dataReceivedTime_hours":0,"usePercent":15.0,"mountedOn":"/boot"},{"name":"/dev/sdb1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u0"},{"name":"/dev/sdc1","size":50.0,"used":5.7,"avail":42.0,"dataReceivedTime_hours":0,"usePercent":13.0,"mountedOn":"/u1"},{"name":"/dev/sdd1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u2"}],"totalSize":168.0,"totalUsed":65.0,"totalAvail":95.0,"totalUsePercent":41.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"thresholdlimit_TotalUsePercent":null},"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Linux"}]');
	//refreshAccordians('[{"description":"RAM = Free : 20325 Memory Buffers : 918 Cached : 9686 Free Memory : 324 Shared Memory : 0 Total Memory : 15953 Used Memory : 15629 Used RAM : 16132 Total : 36457 High Threshold : 50.0 Mb Low Threshold : 20.0 Mb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"RAM","alertMessageType":"CRITICAL","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"RAM","logFileStatus":[],"osType":"Linux","linuxRAM":{"total":36457,"used":16132,"memTotal":15953,"memUsed":15629,"memFree":324,"memBuffers":918,"memShared":0,"memCached":9686,"free":20325,"threshold_high_val":20.0,"threshold_low_val":50.0,"thresholdlimit_MemTotal":null,"thresholdlimit_MemUsed":null,"thresholdlimit_MemFree":null,"thresholdlimit_MemBuffers":null,"thresholdlimit_MemShared":null,"thresholdlimit_MemCached":null,"thresholdlimit_Total":null,"thresholdlimit_Used":null,"thresholdlimit_Free":null,"dataReceivedTime_hours":0},"linuxCPU":null,"linuxDisk":null,"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"CPU Load Average for 5 mins : 0.1 CPU Load Average for 10 mins : 0.2 CPU Load Average for 15 mins : 0.3 High Threshold : 0.0 Low Threshold : 0.0","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"CPU","alertMessageType":"CRITICAL","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"CPU","logFileStatus":[],"osType":"Linux","linuxRAM":null,"linuxCPU":{"threshold_high_val":0.0,"threshold_low_val":0.0,"dataReceivedTime_hours":0,"threshold_loadAverage5min":null,"threshold_loadAverage10min":null,"threshold_loadAverage15min":null,"loadAverage5min":0.1,"loadAverage10min":0.2,"loadAverage15min":0.3},"linuxDisk":null,"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"Total Used : 12.0 Total Size :17.0 Used Percent : 72.0 Total Available : 4.8 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"DISK","logFileStatus":[],"osType":"Linux","linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":17.0,"used":12.0,"avail":4.8,"usePercent":72.0,"mountedOn":"/","dataReceivedTime_hours":0},{"name":"tmpfs","size":7.8,"used":264.0,"avail":7.8,"usePercent":1.0,"mountedOn":"/dev/shm","dataReceivedTime_hours":0},{"name":"/dev/sda1","size":194.0,"used":33.0,"avail":152.0,"usePercent":18.0,"mountedOn":"/boot","dataReceivedTime_hours":0},{"name":"/dev/sdb2","size":50.0,"used":31.0,"avail":17.0,"usePercent":65.0,"mountedOn":"/data","dataReceivedTime_hours":0},{"name":"/dev/sdc3","size":50.0,"used":23.0,"avail":25.0,"usePercent":48.0,"mountedOn":"/usr/sap","dataReceivedTime_hours":0},{"name":"/dev/sdd2","size":138.0,"used":105.0,"avail":27.0,"usePercent":80.0,"mountedOn":"/sybase","dataReceivedTime_hours":0},{"name":"/dev/sde4","size":20.0,"used":2.3,"avail":17.0,"usePercent":13.0,"mountedOn":"/sapmnt","dataReceivedTime_hours":0},{"name":"/dev/sdf","size":50.0,"used":28.0,"avail":20.0,"usePercent":59.0,"mountedOn":"/oracle","dataReceivedTime_hours":0}],"thresholdlimit_TotalUsePercent":null,"totalUsePercent":63.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"totalSize":330.0,"totalUsed":198.0,"totalAvail":117.0},"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"Total Used : 264.0 Total Size :7.8 Used Percent : 1.0 Total Available : 7.8 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"WARNING","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"DISK","logFileStatus":[],"osType":"Linux","linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":17.0,"used":12.0,"avail":4.8,"usePercent":72.0,"mountedOn":"/","dataReceivedTime_hours":0},{"name":"tmpfs","size":7.8,"used":264.0,"avail":7.8,"usePercent":1.0,"mountedOn":"/dev/shm","dataReceivedTime_hours":0},{"name":"/dev/sda1","size":194.0,"used":33.0,"avail":152.0,"usePercent":18.0,"mountedOn":"/boot","dataReceivedTime_hours":0},{"name":"/dev/sdb2","size":50.0,"used":31.0,"avail":17.0,"usePercent":65.0,"mountedOn":"/data","dataReceivedTime_hours":0},{"name":"/dev/sdc3","size":50.0,"used":23.0,"avail":25.0,"usePercent":48.0,"mountedOn":"/usr/sap","dataReceivedTime_hours":0},{"name":"/dev/sdd2","size":138.0,"used":105.0,"avail":27.0,"usePercent":80.0,"mountedOn":"/sybase","dataReceivedTime_hours":0},{"name":"/dev/sde4","size":20.0,"used":2.3,"avail":17.0,"usePercent":13.0,"mountedOn":"/sapmnt","dataReceivedTime_hours":0},{"name":"/dev/sdf","size":50.0,"used":28.0,"avail":20.0,"usePercent":59.0,"mountedOn":"/oracle","dataReceivedTime_hours":0}],"thresholdlimit_TotalUsePercent":null,"totalUsePercent":63.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"totalSize":330.0,"totalUsed":198.0,"totalAvail":117.0},"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"Total Used : 33.0 Total Size :194.0 Used Percent : 18.0 Total Available : 152.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"WARNING","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"DISK","logFileStatus":[],"osType":"Linux","linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":17.0,"used":12.0,"avail":4.8,"usePercent":72.0,"mountedOn":"/","dataReceivedTime_hours":0},{"name":"tmpfs","size":7.8,"used":264.0,"avail":7.8,"usePercent":1.0,"mountedOn":"/dev/shm","dataReceivedTime_hours":0},{"name":"/dev/sda1","size":194.0,"used":33.0,"avail":152.0,"usePercent":18.0,"mountedOn":"/boot","dataReceivedTime_hours":0},{"name":"/dev/sdb2","size":50.0,"used":31.0,"avail":17.0,"usePercent":65.0,"mountedOn":"/data","dataReceivedTime_hours":0},{"name":"/dev/sdc3","size":50.0,"used":23.0,"avail":25.0,"usePercent":48.0,"mountedOn":"/usr/sap","dataReceivedTime_hours":0},{"name":"/dev/sdd2","size":138.0,"used":105.0,"avail":27.0,"usePercent":80.0,"mountedOn":"/sybase","dataReceivedTime_hours":0},{"name":"/dev/sde4","size":20.0,"used":2.3,"avail":17.0,"usePercent":13.0,"mountedOn":"/sapmnt","dataReceivedTime_hours":0},{"name":"/dev/sdf","size":50.0,"used":28.0,"avail":20.0,"usePercent":59.0,"mountedOn":"/oracle","dataReceivedTime_hours":0}],"thresholdlimit_TotalUsePercent":null,"totalUsePercent":63.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"totalSize":330.0,"totalUsed":198.0,"totalAvail":117.0},"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"Total Used : 31.0 Total Size :50.0 Used Percent : 65.0 Total Available : 17.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"DISK","logFileStatus":[],"osType":"Linux","linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":17.0,"used":12.0,"avail":4.8,"usePercent":72.0,"mountedOn":"/","dataReceivedTime_hours":0},{"name":"tmpfs","size":7.8,"used":264.0,"avail":7.8,"usePercent":1.0,"mountedOn":"/dev/shm","dataReceivedTime_hours":0},{"name":"/dev/sda1","size":194.0,"used":33.0,"avail":152.0,"usePercent":18.0,"mountedOn":"/boot","dataReceivedTime_hours":0},{"name":"/dev/sdb2","size":50.0,"used":31.0,"avail":17.0,"usePercent":65.0,"mountedOn":"/data","dataReceivedTime_hours":0},{"name":"/dev/sdc3","size":50.0,"used":23.0,"avail":25.0,"usePercent":48.0,"mountedOn":"/usr/sap","dataReceivedTime_hours":0},{"name":"/dev/sdd2","size":138.0,"used":105.0,"avail":27.0,"usePercent":80.0,"mountedOn":"/sybase","dataReceivedTime_hours":0},{"name":"/dev/sde4","size":20.0,"used":2.3,"avail":17.0,"usePercent":13.0,"mountedOn":"/sapmnt","dataReceivedTime_hours":0},{"name":"/dev/sdf","size":50.0,"used":28.0,"avail":20.0,"usePercent":59.0,"mountedOn":"/oracle","dataReceivedTime_hours":0}],"thresholdlimit_TotalUsePercent":null,"totalUsePercent":63.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"totalSize":330.0,"totalUsed":198.0,"totalAvail":117.0},"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"Total Used : 23.0 Total Size :50.0 Used Percent : 48.0 Total Available : 25.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"DISK","logFileStatus":[],"osType":"Linux","linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":17.0,"used":12.0,"avail":4.8,"usePercent":72.0,"mountedOn":"/","dataReceivedTime_hours":0},{"name":"tmpfs","size":7.8,"used":264.0,"avail":7.8,"usePercent":1.0,"mountedOn":"/dev/shm","dataReceivedTime_hours":0},{"name":"/dev/sda1","size":194.0,"used":33.0,"avail":152.0,"usePercent":18.0,"mountedOn":"/boot","dataReceivedTime_hours":0},{"name":"/dev/sdb2","size":50.0,"used":31.0,"avail":17.0,"usePercent":65.0,"mountedOn":"/data","dataReceivedTime_hours":0},{"name":"/dev/sdc3","size":50.0,"used":23.0,"avail":25.0,"usePercent":48.0,"mountedOn":"/usr/sap","dataReceivedTime_hours":0},{"name":"/dev/sdd2","size":138.0,"used":105.0,"avail":27.0,"usePercent":80.0,"mountedOn":"/sybase","dataReceivedTime_hours":0},{"name":"/dev/sde4","size":20.0,"used":2.3,"avail":17.0,"usePercent":13.0,"mountedOn":"/sapmnt","dataReceivedTime_hours":0},{"name":"/dev/sdf","size":50.0,"used":28.0,"avail":20.0,"usePercent":59.0,"mountedOn":"/oracle","dataReceivedTime_hours":0}],"thresholdlimit_TotalUsePercent":null,"totalUsePercent":63.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"totalSize":330.0,"totalUsed":198.0,"totalAvail":117.0},"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"Total Used : 105.0 Total Size :138.0 Used Percent : 80.0 Total Available : 27.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"DISK","logFileStatus":[],"osType":"Linux","linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":17.0,"used":12.0,"avail":4.8,"usePercent":72.0,"mountedOn":"/","dataReceivedTime_hours":0},{"name":"tmpfs","size":7.8,"used":264.0,"avail":7.8,"usePercent":1.0,"mountedOn":"/dev/shm","dataReceivedTime_hours":0},{"name":"/dev/sda1","size":194.0,"used":33.0,"avail":152.0,"usePercent":18.0,"mountedOn":"/boot","dataReceivedTime_hours":0},{"name":"/dev/sdb2","size":50.0,"used":31.0,"avail":17.0,"usePercent":65.0,"mountedOn":"/data","dataReceivedTime_hours":0},{"name":"/dev/sdc3","size":50.0,"used":23.0,"avail":25.0,"usePercent":48.0,"mountedOn":"/usr/sap","dataReceivedTime_hours":0},{"name":"/dev/sdd2","size":138.0,"used":105.0,"avail":27.0,"usePercent":80.0,"mountedOn":"/sybase","dataReceivedTime_hours":0},{"name":"/dev/sde4","size":20.0,"used":2.3,"avail":17.0,"usePercent":13.0,"mountedOn":"/sapmnt","dataReceivedTime_hours":0},{"name":"/dev/sdf","size":50.0,"used":28.0,"avail":20.0,"usePercent":59.0,"mountedOn":"/oracle","dataReceivedTime_hours":0}],"thresholdlimit_TotalUsePercent":null,"totalUsePercent":63.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"totalSize":330.0,"totalUsed":198.0,"totalAvail":117.0},"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"Total Used : 2.3 Total Size :20.0 Used Percent : 13.0 Total Available : 17.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"WARNING","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"DISK","logFileStatus":[],"osType":"Linux","linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":17.0,"used":12.0,"avail":4.8,"usePercent":72.0,"mountedOn":"/","dataReceivedTime_hours":0},{"name":"tmpfs","size":7.8,"used":264.0,"avail":7.8,"usePercent":1.0,"mountedOn":"/dev/shm","dataReceivedTime_hours":0},{"name":"/dev/sda1","size":194.0,"used":33.0,"avail":152.0,"usePercent":18.0,"mountedOn":"/boot","dataReceivedTime_hours":0},{"name":"/dev/sdb2","size":50.0,"used":31.0,"avail":17.0,"usePercent":65.0,"mountedOn":"/data","dataReceivedTime_hours":0},{"name":"/dev/sdc3","size":50.0,"used":23.0,"avail":25.0,"usePercent":48.0,"mountedOn":"/usr/sap","dataReceivedTime_hours":0},{"name":"/dev/sdd2","size":138.0,"used":105.0,"avail":27.0,"usePercent":80.0,"mountedOn":"/sybase","dataReceivedTime_hours":0},{"name":"/dev/sde4","size":20.0,"used":2.3,"avail":17.0,"usePercent":13.0,"mountedOn":"/sapmnt","dataReceivedTime_hours":0},{"name":"/dev/sdf","size":50.0,"used":28.0,"avail":20.0,"usePercent":59.0,"mountedOn":"/oracle","dataReceivedTime_hours":0}],"thresholdlimit_TotalUsePercent":null,"totalUsePercent":63.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"totalSize":330.0,"totalUsed":198.0,"totalAvail":117.0},"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"Total Used : 28.0 Total Size :50.0 Used Percent : 59.0 Total Available : 20.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"DISK","logFileStatus":[],"osType":"Linux","linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":17.0,"used":12.0,"avail":4.8,"usePercent":72.0,"mountedOn":"/","dataReceivedTime_hours":0},{"name":"tmpfs","size":7.8,"used":264.0,"avail":7.8,"usePercent":1.0,"mountedOn":"/dev/shm","dataReceivedTime_hours":0},{"name":"/dev/sda1","size":194.0,"used":33.0,"avail":152.0,"usePercent":18.0,"mountedOn":"/boot","dataReceivedTime_hours":0},{"name":"/dev/sdb2","size":50.0,"used":31.0,"avail":17.0,"usePercent":65.0,"mountedOn":"/data","dataReceivedTime_hours":0},{"name":"/dev/sdc3","size":50.0,"used":23.0,"avail":25.0,"usePercent":48.0,"mountedOn":"/usr/sap","dataReceivedTime_hours":0},{"name":"/dev/sdd2","size":138.0,"used":105.0,"avail":27.0,"usePercent":80.0,"mountedOn":"/sybase","dataReceivedTime_hours":0},{"name":"/dev/sde4","size":20.0,"used":2.3,"avail":17.0,"usePercent":13.0,"mountedOn":"/sapmnt","dataReceivedTime_hours":0},{"name":"/dev/sdf","size":50.0,"used":28.0,"avail":20.0,"usePercent":59.0,"mountedOn":"/oracle","dataReceivedTime_hours":0}],"thresholdlimit_TotalUsePercent":null,"totalUsePercent":63.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"totalSize":330.0,"totalUsed":198.0,"totalAvail":117.0},"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"RAM = Free : 4911 Memory Buffers : 385 Cached : 2936 Free Memory : 995 Shared Memory : 0 Total Memory : 7873 Used Memory : 6877 Used RAM : 6961 Total : 11873 High Threshold : 40.0 Mb Low Threshold : 20.0 Mb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"RAM","alertMessageType":"CRITICAL","alertMessageDatetime":1439181961000,"alertMessageHostName":"gc12c-oms.coelab.com","alertMessageparameterName":"RAM","logFileStatus":[],"linuxRAM":{"total":11873,"used":6961,"threshold_high_val":20.0,"threshold_low_val":40.0,"thresholdlimit_MemTotal":null,"thresholdlimit_MemUsed":null,"thresholdlimit_MemFree":null,"thresholdlimit_MemBuffers":null,"thresholdlimit_MemShared":null,"thresholdlimit_MemCached":null,"thresholdlimit_Total":null,"thresholdlimit_Used":null,"thresholdlimit_Free":null,"dataReceivedTime_hours":0,"memTotal":7873,"memUsed":6877,"memFree":995,"memBuffers":385,"memShared":0,"memCached":2936,"free":4911},"linuxCPU":null,"linuxDisk":null,"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Linux"},{"description":"CPU Load Average for 5 mins : 0.1 CPU Load Average for 10 mins : 0.08 CPU Load Average for 15 mins : 0.08 High Threshold : 0.0 Low Threshold : 0.0","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"CPU","alertMessageType":"CRITICAL","alertMessageDatetime":1439181961000,"alertMessageHostName":"gc12c-oms.coelab.com","alertMessageparameterName":"CPU","logFileStatus":[],"linuxRAM":null,"linuxCPU":{"threshold_high_val":0.0,"threshold_low_val":0.0,"dataReceivedTime_hours":0,"threshold_loadAverage5min":null,"threshold_loadAverage10min":null,"threshold_loadAverage15min":null,"loadAverage5min":0.1,"loadAverage10min":0.08,"loadAverage15min":0.08},"linuxDisk":null,"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Linux"},{"description":"Total Used : 13.0 Total Size :16.0 Used Percent : 86.0 Total Available : 2.2 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439181961000,"alertMessageHostName":"gc12c-oms.coelab.com","alertMessageparameterName":"DISK","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":16.0,"used":13.0,"avail":2.2,"dataReceivedTime_hours":0,"usePercent":86.0,"mountedOn":"/"},{"name":"tmpfs","size":3.9,"used":668.0,"avail":3.2,"dataReceivedTime_hours":0,"usePercent":17.0,"mountedOn":"/dev/shm"},{"name":"/dev/sda1","size":243.0,"used":33.0,"avail":198.0,"dataReceivedTime_hours":0,"usePercent":15.0,"mountedOn":"/boot"},{"name":"/dev/sdb1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u0"},{"name":"/dev/sdc1","size":50.0,"used":5.7,"avail":42.0,"dataReceivedTime_hours":0,"usePercent":13.0,"mountedOn":"/u1"},{"name":"/dev/sdd1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u2"}],"totalSize":168.0,"totalUsed":65.0,"totalAvail":95.0,"totalUsePercent":41.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"thresholdlimit_TotalUsePercent":null},"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Linux"},{"description":"Total Used : 668.0 Total Size :3.9 Used Percent : 17.0 Total Available : 3.2 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"WARNING","alertMessageDatetime":1439181961000,"alertMessageHostName":"gc12c-oms.coelab.com","alertMessageparameterName":"DISK","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":16.0,"used":13.0,"avail":2.2,"dataReceivedTime_hours":0,"usePercent":86.0,"mountedOn":"/"},{"name":"tmpfs","size":3.9,"used":668.0,"avail":3.2,"dataReceivedTime_hours":0,"usePercent":17.0,"mountedOn":"/dev/shm"},{"name":"/dev/sda1","size":243.0,"used":33.0,"avail":198.0,"dataReceivedTime_hours":0,"usePercent":15.0,"mountedOn":"/boot"},{"name":"/dev/sdb1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u0"},{"name":"/dev/sdc1","size":50.0,"used":5.7,"avail":42.0,"dataReceivedTime_hours":0,"usePercent":13.0,"mountedOn":"/u1"},{"name":"/dev/sdd1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u2"}],"totalSize":168.0,"totalUsed":65.0,"totalAvail":95.0,"totalUsePercent":41.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"thresholdlimit_TotalUsePercent":null},"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Linux"},{"description":"Total Used : 33.0 Total Size :243.0 Used Percent : 15.0 Total Available : 198.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"WARNING","alertMessageDatetime":1439181961000,"alertMessageHostName":"gc12c-oms.coelab.com","alertMessageparameterName":"DISK","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":16.0,"used":13.0,"avail":2.2,"dataReceivedTime_hours":0,"usePercent":86.0,"mountedOn":"/"},{"name":"tmpfs","size":3.9,"used":668.0,"avail":3.2,"dataReceivedTime_hours":0,"usePercent":17.0,"mountedOn":"/dev/shm"},{"name":"/dev/sda1","size":243.0,"used":33.0,"avail":198.0,"dataReceivedTime_hours":0,"usePercent":15.0,"mountedOn":"/boot"},{"name":"/dev/sdb1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u0"},{"name":"/dev/sdc1","size":50.0,"used":5.7,"avail":42.0,"dataReceivedTime_hours":0,"usePercent":13.0,"mountedOn":"/u1"},{"name":"/dev/sdd1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u2"}],"totalSize":168.0,"totalUsed":65.0,"totalAvail":95.0,"totalUsePercent":41.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"thresholdlimit_TotalUsePercent":null},"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Linux"},{"description":"Total Used : 23.0 Total Size :50.0 Used Percent : 49.0 Total Available : 25.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439181961000,"alertMessageHostName":"gc12c-oms.coelab.com","alertMessageparameterName":"DISK","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":16.0,"used":13.0,"avail":2.2,"dataReceivedTime_hours":0,"usePercent":86.0,"mountedOn":"/"},{"name":"tmpfs","size":3.9,"used":668.0,"avail":3.2,"dataReceivedTime_hours":0,"usePercent":17.0,"mountedOn":"/dev/shm"},{"name":"/dev/sda1","size":243.0,"used":33.0,"avail":198.0,"dataReceivedTime_hours":0,"usePercent":15.0,"mountedOn":"/boot"},{"name":"/dev/sdb1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u0"},{"name":"/dev/sdc1","size":50.0,"used":5.7,"avail":42.0,"dataReceivedTime_hours":0,"usePercent":13.0,"mountedOn":"/u1"},{"name":"/dev/sdd1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u2"}],"totalSize":168.0,"totalUsed":65.0,"totalAvail":95.0,"totalUsePercent":41.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"thresholdlimit_TotalUsePercent":null},"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Linux"},{"description":"Total Used : 5.7 Total Size :50.0 Used Percent : 13.0 Total Available : 42.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"WARNING","alertMessageDatetime":1439181961000,"alertMessageHostName":"gc12c-oms.coelab.com","alertMessageparameterName":"DISK","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":16.0,"used":13.0,"avail":2.2,"dataReceivedTime_hours":0,"usePercent":86.0,"mountedOn":"/"},{"name":"tmpfs","size":3.9,"used":668.0,"avail":3.2,"dataReceivedTime_hours":0,"usePercent":17.0,"mountedOn":"/dev/shm"},{"name":"/dev/sda1","size":243.0,"used":33.0,"avail":198.0,"dataReceivedTime_hours":0,"usePercent":15.0,"mountedOn":"/boot"},{"name":"/dev/sdb1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u0"},{"name":"/dev/sdc1","size":50.0,"used":5.7,"avail":42.0,"dataReceivedTime_hours":0,"usePercent":13.0,"mountedOn":"/u1"},{"name":"/dev/sdd1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u2"}],"totalSize":168.0,"totalUsed":65.0,"totalAvail":95.0,"totalUsePercent":41.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"thresholdlimit_TotalUsePercent":null},"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Linux"},{"description":"Total Used : 23.0 Total Size :50.0 Used Percent : 49.0 Total Available : 25.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439181961000,"alertMessageHostName":"gc12c-oms.coelab.com","alertMessageparameterName":"DISK","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":16.0,"used":13.0,"avail":2.2,"dataReceivedTime_hours":0,"usePercent":86.0,"mountedOn":"/"},{"name":"tmpfs","size":3.9,"used":668.0,"avail":3.2,"dataReceivedTime_hours":0,"usePercent":17.0,"mountedOn":"/dev/shm"},{"name":"/dev/sda1","size":243.0,"used":33.0,"avail":198.0,"dataReceivedTime_hours":0,"usePercent":15.0,"mountedOn":"/boot"},{"name":"/dev/sdb1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u0"},{"name":"/dev/sdc1","size":50.0,"used":5.7,"avail":42.0,"dataReceivedTime_hours":0,"usePercent":13.0,"mountedOn":"/u1"},{"name":"/dev/sdd1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u2"}],"totalSize":168.0,"totalUsed":65.0,"totalAvail":95.0,"totalUsePercent":41.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"thresholdlimit_TotalUsePercent":null},"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Linux"},{"description":"Total physical memory: 16382 Mb Memory used : 59.67 Mb Used virtual memory: 41136 Mb High Threshold : 0.0 Mb Low Threshold : 0.0 Mb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"RAM","alertMessageType":"CRITICAL","alertMessageDatetime":1439182020000,"alertMessageHostName":"DL3807","alertMessageparameterName":"RAM","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":null,"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":{"memoryUsage":59.67,"totalPhysicalMemory":16382,"availablePhysicalMemory":6598,"availablePhysicalInPercent":0,"virtualMemoryMaxSize":56735,"availableVirtualMEmory":15599,"usedVirtualMemory":41136,"usedVirtualMemoryInPercent":0,"warning_High":0.0,"critical_High":0.0},"serverDown":null,"osType":"Windows"},{"description":"Usage in percent : 0.0 High Threshold : 0.0 Low Threshold : 0.0","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"CPU","alertMessageType":"CRITICAL","alertMessageDatetime":1439182020000,"alertMessageHostName":"DL3807","alertMessageparameterName":"CPU","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":null,"urlDown":null,"cedError":null,"winDisk":null,"winCPU":{"warning_High":0.0,"critical_High":0.0,"cpuUsageInPercent":0.0},"winRAM":null,"serverDown":null,"osType":"Windows"},{"description":"C DRIVE - Actual Value : 78.57, Warning level : 0.0, Critical level : 0.0","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439182020000,"alertMessageHostName":"DL3807","alertMessageparameterName":"DISK","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":null,"urlDown":null,"cedError":null,"winDisk":{"fileSystem":[{"name":"C","size":35.0,"used":27.5,"avail":7.5,"dataReceivedTime_hours":0,"usePercent":78.57,"mountedOn":null},{"name":"D","size":300.0,"used":123.11,"avail":176.89,"dataReceivedTime_hours":0,"usePercent":41.04,"mountedOn":null},{"name":"E","size":300.0,"used":295.04,"avail":4.96,"dataReceivedTime_hours":0,"usePercent":98.35,"mountedOn":null}],"warning_High":0.0,"critical_High":0.0,"totalDiskSize":635.0,"totalDiskUsed":445.65000000000003,"totalDiskAvail":189.35,"totalUsePercent":70.18110236220473},"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Windows"},{"description":"D DRIVE - Actual Value : 41.04, Warning level : 0.0, Critical level : 0.0","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439182020000,"alertMessageHostName":"DL3807","alertMessageparameterName":"DISK","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":null,"urlDown":null,"cedError":null,"winDisk":{"fileSystem":[{"name":"C","size":35.0,"used":27.5,"avail":7.5,"dataReceivedTime_hours":0,"usePercent":78.57,"mountedOn":null},{"name":"D","size":300.0,"used":123.11,"avail":176.89,"dataReceivedTime_hours":0,"usePercent":41.04,"mountedOn":null},{"name":"E","size":300.0,"used":295.04,"avail":4.96,"dataReceivedTime_hours":0,"usePercent":98.35,"mountedOn":null}],"warning_High":0.0,"critical_High":0.0,"totalDiskSize":635.0,"totalDiskUsed":445.65000000000003,"totalDiskAvail":189.35,"totalUsePercent":70.18110236220473},"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Windows"},{"description":"E DRIVE - Actual Value : 98.35, Warning level : 0.0, Critical level : 0.0","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439182020000,"alertMessageHostName":"DL3807","alertMessageparameterName":"DISK","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":null,"urlDown":null,"cedError":null,"winDisk":{"fileSystem":[{"name":"C","size":35.0,"used":27.5,"avail":7.5,"dataReceivedTime_hours":0,"usePercent":78.57,"mountedOn":null},{"name":"D","size":300.0,"used":123.11,"avail":176.89,"dataReceivedTime_hours":0,"usePercent":41.04,"mountedOn":null},{"name":"E","size":300.0,"used":295.04,"avail":4.96,"dataReceivedTime_hours":0,"usePercent":98.35,"mountedOn":null}],"warning_High":0.0,"critical_High":0.0,"totalDiskSize":635.0,"totalDiskUsed":445.65000000000003,"totalDiskAvail":189.35,"totalUsePercent":70.18110236220473},"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Windows"}]');
//	refreshAccordians('[{"description":"RAM = Free : 20325 Memory Buffers : 918 Cached : 9686 Free Memory : 324 Shared Memory : 0 Total Memory : 15953 Used Memory : 15629 Used RAM : 16132 Total : 36457 High Threshold : 50.0 Mb Low Threshold : 20.0 Mb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"RAM","alertMessageType":"CRITICAL","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"RAM","logFileStatus":[],"osType":"Linux","linuxRAM":{"total":36457,"used":16132,"memTotal":15953,"memUsed":15629,"memFree":324,"memBuffers":918,"memShared":0,"memCached":9686,"free":20325,"threshold_high_val":20.0,"threshold_low_val":50.0,"thresholdlimit_MemTotal":null,"thresholdlimit_MemUsed":null,"thresholdlimit_MemFree":null,"thresholdlimit_MemBuffers":null,"thresholdlimit_MemShared":null,"thresholdlimit_MemCached":null,"thresholdlimit_Total":null,"thresholdlimit_Used":null,"thresholdlimit_Free":null,"dataReceivedTime_hours":0},"linuxCPU":null,"linuxDisk":null,"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"CPU Load Average for 5 mins : 0.1 CPU Load Average for 10 mins : 0.2 CPU Load Average for 15 mins : 0.3 High Threshold : 0.0 Low Threshold : 0.0","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"CPU","alertMessageType":"CRITICAL","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"CPU","logFileStatus":[],"osType":"Linux","linuxRAM":null,"linuxCPU":{"threshold_high_val":0.0,"threshold_low_val":0.0,"dataReceivedTime_hours":0,"threshold_loadAverage5min":null,"threshold_loadAverage10min":null,"threshold_loadAverage15min":null,"loadAverage5min":0.1,"loadAverage10min":0.2,"loadAverage15min":0.3},"linuxDisk":null,"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"Total Used : 12.0 Total Size :17.0 Used Percent : 72.0 Total Available : 4.8 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"DISK","logFileStatus":[],"osType":"Linux","linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":17.0,"used":12.0,"avail":4.8,"usePercent":72.0,"mountedOn":"/","dataReceivedTime_hours":0},{"name":"tmpfs","size":7.8,"used":264.0,"avail":7.8,"usePercent":1.0,"mountedOn":"/dev/shm","dataReceivedTime_hours":0},{"name":"/dev/sda1","size":194.0,"used":33.0,"avail":152.0,"usePercent":18.0,"mountedOn":"/boot","dataReceivedTime_hours":0},{"name":"/dev/sdb2","size":50.0,"used":31.0,"avail":17.0,"usePercent":65.0,"mountedOn":"/data","dataReceivedTime_hours":0},{"name":"/dev/sdc3","size":50.0,"used":23.0,"avail":25.0,"usePercent":48.0,"mountedOn":"/usr/sap","dataReceivedTime_hours":0},{"name":"/dev/sdd2","size":138.0,"used":105.0,"avail":27.0,"usePercent":80.0,"mountedOn":"/sybase","dataReceivedTime_hours":0},{"name":"/dev/sde4","size":20.0,"used":2.3,"avail":17.0,"usePercent":13.0,"mountedOn":"/sapmnt","dataReceivedTime_hours":0},{"name":"/dev/sdf","size":50.0,"used":28.0,"avail":20.0,"usePercent":59.0,"mountedOn":"/oracle","dataReceivedTime_hours":0}],"thresholdlimit_TotalUsePercent":null,"totalUsePercent":63.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"totalSize":330.0,"totalUsed":198.0,"totalAvail":117.0},"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"Total Used : 264.0 Total Size :7.8 Used Percent : 1.0 Total Available : 7.8 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"WARNING","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"DISK","logFileStatus":[],"osType":"Linux","linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":17.0,"used":12.0,"avail":4.8,"usePercent":72.0,"mountedOn":"/","dataReceivedTime_hours":0},{"name":"tmpfs","size":7.8,"used":264.0,"avail":7.8,"usePercent":1.0,"mountedOn":"/dev/shm","dataReceivedTime_hours":0},{"name":"/dev/sda1","size":194.0,"used":33.0,"avail":152.0,"usePercent":18.0,"mountedOn":"/boot","dataReceivedTime_hours":0},{"name":"/dev/sdb2","size":50.0,"used":31.0,"avail":17.0,"usePercent":65.0,"mountedOn":"/data","dataReceivedTime_hours":0},{"name":"/dev/sdc3","size":50.0,"used":23.0,"avail":25.0,"usePercent":48.0,"mountedOn":"/usr/sap","dataReceivedTime_hours":0},{"name":"/dev/sdd2","size":138.0,"used":105.0,"avail":27.0,"usePercent":80.0,"mountedOn":"/sybase","dataReceivedTime_hours":0},{"name":"/dev/sde4","size":20.0,"used":2.3,"avail":17.0,"usePercent":13.0,"mountedOn":"/sapmnt","dataReceivedTime_hours":0},{"name":"/dev/sdf","size":50.0,"used":28.0,"avail":20.0,"usePercent":59.0,"mountedOn":"/oracle","dataReceivedTime_hours":0}],"thresholdlimit_TotalUsePercent":null,"totalUsePercent":63.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"totalSize":330.0,"totalUsed":198.0,"totalAvail":117.0},"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"Total Used : 33.0 Total Size :194.0 Used Percent : 18.0 Total Available : 152.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"WARNING","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"DISK","logFileStatus":[],"osType":"Linux","linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":17.0,"used":12.0,"avail":4.8,"usePercent":72.0,"mountedOn":"/","dataReceivedTime_hours":0},{"name":"tmpfs","size":7.8,"used":264.0,"avail":7.8,"usePercent":1.0,"mountedOn":"/dev/shm","dataReceivedTime_hours":0},{"name":"/dev/sda1","size":194.0,"used":33.0,"avail":152.0,"usePercent":18.0,"mountedOn":"/boot","dataReceivedTime_hours":0},{"name":"/dev/sdb2","size":50.0,"used":31.0,"avail":17.0,"usePercent":65.0,"mountedOn":"/data","dataReceivedTime_hours":0},{"name":"/dev/sdc3","size":50.0,"used":23.0,"avail":25.0,"usePercent":48.0,"mountedOn":"/usr/sap","dataReceivedTime_hours":0},{"name":"/dev/sdd2","size":138.0,"used":105.0,"avail":27.0,"usePercent":80.0,"mountedOn":"/sybase","dataReceivedTime_hours":0},{"name":"/dev/sde4","size":20.0,"used":2.3,"avail":17.0,"usePercent":13.0,"mountedOn":"/sapmnt","dataReceivedTime_hours":0},{"name":"/dev/sdf","size":50.0,"used":28.0,"avail":20.0,"usePercent":59.0,"mountedOn":"/oracle","dataReceivedTime_hours":0}],"thresholdlimit_TotalUsePercent":null,"totalUsePercent":63.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"totalSize":330.0,"totalUsed":198.0,"totalAvail":117.0},"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"Total Used : 31.0 Total Size :50.0 Used Percent : 65.0 Total Available : 17.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"DISK","logFileStatus":[],"osType":"Linux","linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":17.0,"used":12.0,"avail":4.8,"usePercent":72.0,"mountedOn":"/","dataReceivedTime_hours":0},{"name":"tmpfs","size":7.8,"used":264.0,"avail":7.8,"usePercent":1.0,"mountedOn":"/dev/shm","dataReceivedTime_hours":0},{"name":"/dev/sda1","size":194.0,"used":33.0,"avail":152.0,"usePercent":18.0,"mountedOn":"/boot","dataReceivedTime_hours":0},{"name":"/dev/sdb2","size":50.0,"used":31.0,"avail":17.0,"usePercent":65.0,"mountedOn":"/data","dataReceivedTime_hours":0},{"name":"/dev/sdc3","size":50.0,"used":23.0,"avail":25.0,"usePercent":48.0,"mountedOn":"/usr/sap","dataReceivedTime_hours":0},{"name":"/dev/sdd2","size":138.0,"used":105.0,"avail":27.0,"usePercent":80.0,"mountedOn":"/sybase","dataReceivedTime_hours":0},{"name":"/dev/sde4","size":20.0,"used":2.3,"avail":17.0,"usePercent":13.0,"mountedOn":"/sapmnt","dataReceivedTime_hours":0},{"name":"/dev/sdf","size":50.0,"used":28.0,"avail":20.0,"usePercent":59.0,"mountedOn":"/oracle","dataReceivedTime_hours":0}],"thresholdlimit_TotalUsePercent":null,"totalUsePercent":63.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"totalSize":330.0,"totalUsed":198.0,"totalAvail":117.0},"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"Total Used : 23.0 Total Size :50.0 Used Percent : 48.0 Total Available : 25.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"DISK","logFileStatus":[],"osType":"Linux","linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":17.0,"used":12.0,"avail":4.8,"usePercent":72.0,"mountedOn":"/","dataReceivedTime_hours":0},{"name":"tmpfs","size":7.8,"used":264.0,"avail":7.8,"usePercent":1.0,"mountedOn":"/dev/shm","dataReceivedTime_hours":0},{"name":"/dev/sda1","size":194.0,"used":33.0,"avail":152.0,"usePercent":18.0,"mountedOn":"/boot","dataReceivedTime_hours":0},{"name":"/dev/sdb2","size":50.0,"used":31.0,"avail":17.0,"usePercent":65.0,"mountedOn":"/data","dataReceivedTime_hours":0},{"name":"/dev/sdc3","size":50.0,"used":23.0,"avail":25.0,"usePercent":48.0,"mountedOn":"/usr/sap","dataReceivedTime_hours":0},{"name":"/dev/sdd2","size":138.0,"used":105.0,"avail":27.0,"usePercent":80.0,"mountedOn":"/sybase","dataReceivedTime_hours":0},{"name":"/dev/sde4","size":20.0,"used":2.3,"avail":17.0,"usePercent":13.0,"mountedOn":"/sapmnt","dataReceivedTime_hours":0},{"name":"/dev/sdf","size":50.0,"used":28.0,"avail":20.0,"usePercent":59.0,"mountedOn":"/oracle","dataReceivedTime_hours":0}],"thresholdlimit_TotalUsePercent":null,"totalUsePercent":63.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"totalSize":330.0,"totalUsed":198.0,"totalAvail":117.0},"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"Total Used : 105.0 Total Size :138.0 Used Percent : 80.0 Total Available : 27.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"DISK","logFileStatus":[],"osType":"Linux","linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":17.0,"used":12.0,"avail":4.8,"usePercent":72.0,"mountedOn":"/","dataReceivedTime_hours":0},{"name":"tmpfs","size":7.8,"used":264.0,"avail":7.8,"usePercent":1.0,"mountedOn":"/dev/shm","dataReceivedTime_hours":0},{"name":"/dev/sda1","size":194.0,"used":33.0,"avail":152.0,"usePercent":18.0,"mountedOn":"/boot","dataReceivedTime_hours":0},{"name":"/dev/sdb2","size":50.0,"used":31.0,"avail":17.0,"usePercent":65.0,"mountedOn":"/data","dataReceivedTime_hours":0},{"name":"/dev/sdc3","size":50.0,"used":23.0,"avail":25.0,"usePercent":48.0,"mountedOn":"/usr/sap","dataReceivedTime_hours":0},{"name":"/dev/sdd2","size":138.0,"used":105.0,"avail":27.0,"usePercent":80.0,"mountedOn":"/sybase","dataReceivedTime_hours":0},{"name":"/dev/sde4","size":20.0,"used":2.3,"avail":17.0,"usePercent":13.0,"mountedOn":"/sapmnt","dataReceivedTime_hours":0},{"name":"/dev/sdf","size":50.0,"used":28.0,"avail":20.0,"usePercent":59.0,"mountedOn":"/oracle","dataReceivedTime_hours":0}],"thresholdlimit_TotalUsePercent":null,"totalUsePercent":63.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"totalSize":330.0,"totalUsed":198.0,"totalAvail":117.0},"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"Total Used : 2.3 Total Size :20.0 Used Percent : 13.0 Total Available : 17.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"WARNING","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"DISK","logFileStatus":[],"osType":"Linux","linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":17.0,"used":12.0,"avail":4.8,"usePercent":72.0,"mountedOn":"/","dataReceivedTime_hours":0},{"name":"tmpfs","size":7.8,"used":264.0,"avail":7.8,"usePercent":1.0,"mountedOn":"/dev/shm","dataReceivedTime_hours":0},{"name":"/dev/sda1","size":194.0,"used":33.0,"avail":152.0,"usePercent":18.0,"mountedOn":"/boot","dataReceivedTime_hours":0},{"name":"/dev/sdb2","size":50.0,"used":31.0,"avail":17.0,"usePercent":65.0,"mountedOn":"/data","dataReceivedTime_hours":0},{"name":"/dev/sdc3","size":50.0,"used":23.0,"avail":25.0,"usePercent":48.0,"mountedOn":"/usr/sap","dataReceivedTime_hours":0},{"name":"/dev/sdd2","size":138.0,"used":105.0,"avail":27.0,"usePercent":80.0,"mountedOn":"/sybase","dataReceivedTime_hours":0},{"name":"/dev/sde4","size":20.0,"used":2.3,"avail":17.0,"usePercent":13.0,"mountedOn":"/sapmnt","dataReceivedTime_hours":0},{"name":"/dev/sdf","size":50.0,"used":28.0,"avail":20.0,"usePercent":59.0,"mountedOn":"/oracle","dataReceivedTime_hours":0}],"thresholdlimit_TotalUsePercent":null,"totalUsePercent":63.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"totalSize":330.0,"totalUsed":198.0,"totalAvail":117.0},"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"Total Used : 28.0 Total Size :50.0 Used Percent : 59.0 Total Available : 20.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439179681000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"DISK","logFileStatus":[],"osType":"Linux","linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":17.0,"used":12.0,"avail":4.8,"usePercent":72.0,"mountedOn":"/","dataReceivedTime_hours":0},{"name":"tmpfs","size":7.8,"used":264.0,"avail":7.8,"usePercent":1.0,"mountedOn":"/dev/shm","dataReceivedTime_hours":0},{"name":"/dev/sda1","size":194.0,"used":33.0,"avail":152.0,"usePercent":18.0,"mountedOn":"/boot","dataReceivedTime_hours":0},{"name":"/dev/sdb2","size":50.0,"used":31.0,"avail":17.0,"usePercent":65.0,"mountedOn":"/data","dataReceivedTime_hours":0},{"name":"/dev/sdc3","size":50.0,"used":23.0,"avail":25.0,"usePercent":48.0,"mountedOn":"/usr/sap","dataReceivedTime_hours":0},{"name":"/dev/sdd2","size":138.0,"used":105.0,"avail":27.0,"usePercent":80.0,"mountedOn":"/sybase","dataReceivedTime_hours":0},{"name":"/dev/sde4","size":20.0,"used":2.3,"avail":17.0,"usePercent":13.0,"mountedOn":"/sapmnt","dataReceivedTime_hours":0},{"name":"/dev/sdf","size":50.0,"used":28.0,"avail":20.0,"usePercent":59.0,"mountedOn":"/oracle","dataReceivedTime_hours":0}],"thresholdlimit_TotalUsePercent":null,"totalUsePercent":63.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"totalSize":330.0,"totalUsed":198.0,"totalAvail":117.0},"urlDown":null,"cedError":null,"winDisk":null,"winRAM":null,"winCPU":null,"serverDown":null},{"description":"RAM = Free : 4911 Memory Buffers : 385 Cached : 2936 Free Memory : 995 Shared Memory : 0 Total Memory : 7873 Used Memory : 6877 Used RAM : 6961 Total : 11873 High Threshold : 40.0 Mb Low Threshold : 20.0 Mb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"RAM","alertMessageType":"CRITICAL","alertMessageDatetime":1439181961000,"alertMessageHostName":"gc12c-oms.coelab.com","alertMessageparameterName":"RAM","logFileStatus":[],"linuxRAM":{"total":11873,"used":6961,"threshold_high_val":20.0,"threshold_low_val":40.0,"thresholdlimit_MemTotal":null,"thresholdlimit_MemUsed":null,"thresholdlimit_MemFree":null,"thresholdlimit_MemBuffers":null,"thresholdlimit_MemShared":null,"thresholdlimit_MemCached":null,"thresholdlimit_Total":null,"thresholdlimit_Used":null,"thresholdlimit_Free":null,"dataReceivedTime_hours":0,"memTotal":7873,"memUsed":6877,"memFree":995,"memBuffers":385,"memShared":0,"memCached":2936,"free":4911},"linuxCPU":null,"linuxDisk":null,"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Linux"},{"description":"CPU Load Average for 5 mins : 0.1 CPU Load Average for 10 mins : 0.08 CPU Load Average for 15 mins : 0.08 High Threshold : 0.0 Low Threshold : 0.0","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"CPU","alertMessageType":"CRITICAL","alertMessageDatetime":1439181961000,"alertMessageHostName":"gc12c-oms.coelab.com","alertMessageparameterName":"CPU","logFileStatus":[],"linuxRAM":null,"linuxCPU":{"threshold_high_val":0.0,"threshold_low_val":0.0,"dataReceivedTime_hours":0,"threshold_loadAverage5min":null,"threshold_loadAverage10min":null,"threshold_loadAverage15min":null,"loadAverage5min":0.1,"loadAverage10min":0.08,"loadAverage15min":0.08},"linuxDisk":null,"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Linux"},{"description":"Total Used : 13.0 Total Size :16.0 Used Percent : 86.0 Total Available : 2.2 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439181961000,"alertMessageHostName":"gc12c-oms.coelab.com","alertMessageparameterName":"DISK","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":16.0,"used":13.0,"avail":2.2,"dataReceivedTime_hours":0,"usePercent":86.0,"mountedOn":"/"},{"name":"tmpfs","size":3.9,"used":668.0,"avail":3.2,"dataReceivedTime_hours":0,"usePercent":17.0,"mountedOn":"/dev/shm"},{"name":"/dev/sda1","size":243.0,"used":33.0,"avail":198.0,"dataReceivedTime_hours":0,"usePercent":15.0,"mountedOn":"/boot"},{"name":"/dev/sdb1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u0"},{"name":"/dev/sdc1","size":50.0,"used":5.7,"avail":42.0,"dataReceivedTime_hours":0,"usePercent":13.0,"mountedOn":"/u1"},{"name":"/dev/sdd1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u2"}],"totalSize":168.0,"totalUsed":65.0,"totalAvail":95.0,"totalUsePercent":41.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"thresholdlimit_TotalUsePercent":null},"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Linux"},{"description":"Total Used : 668.0 Total Size :3.9 Used Percent : 17.0 Total Available : 3.2 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"WARNING","alertMessageDatetime":1439181961000,"alertMessageHostName":"gc12c-oms.coelab.com","alertMessageparameterName":"DISK","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":16.0,"used":13.0,"avail":2.2,"dataReceivedTime_hours":0,"usePercent":86.0,"mountedOn":"/"},{"name":"tmpfs","size":3.9,"used":668.0,"avail":3.2,"dataReceivedTime_hours":0,"usePercent":17.0,"mountedOn":"/dev/shm"},{"name":"/dev/sda1","size":243.0,"used":33.0,"avail":198.0,"dataReceivedTime_hours":0,"usePercent":15.0,"mountedOn":"/boot"},{"name":"/dev/sdb1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u0"},{"name":"/dev/sdc1","size":50.0,"used":5.7,"avail":42.0,"dataReceivedTime_hours":0,"usePercent":13.0,"mountedOn":"/u1"},{"name":"/dev/sdd1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u2"}],"totalSize":168.0,"totalUsed":65.0,"totalAvail":95.0,"totalUsePercent":41.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"thresholdlimit_TotalUsePercent":null},"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Linux"},{"description":"Total Used : 33.0 Total Size :243.0 Used Percent : 15.0 Total Available : 198.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"WARNING","alertMessageDatetime":1439181961000,"alertMessageHostName":"gc12c-oms.coelab.com","alertMessageparameterName":"DISK","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":16.0,"used":13.0,"avail":2.2,"dataReceivedTime_hours":0,"usePercent":86.0,"mountedOn":"/"},{"name":"tmpfs","size":3.9,"used":668.0,"avail":3.2,"dataReceivedTime_hours":0,"usePercent":17.0,"mountedOn":"/dev/shm"},{"name":"/dev/sda1","size":243.0,"used":33.0,"avail":198.0,"dataReceivedTime_hours":0,"usePercent":15.0,"mountedOn":"/boot"},{"name":"/dev/sdb1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u0"},{"name":"/dev/sdc1","size":50.0,"used":5.7,"avail":42.0,"dataReceivedTime_hours":0,"usePercent":13.0,"mountedOn":"/u1"},{"name":"/dev/sdd1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u2"}],"totalSize":168.0,"totalUsed":65.0,"totalAvail":95.0,"totalUsePercent":41.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"thresholdlimit_TotalUsePercent":null},"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Linux"},{"description":"Total Used : 23.0 Total Size :50.0 Used Percent : 49.0 Total Available : 25.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439181961000,"alertMessageHostName":"gc12c-oms.coelab.com","alertMessageparameterName":"DISK","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":16.0,"used":13.0,"avail":2.2,"dataReceivedTime_hours":0,"usePercent":86.0,"mountedOn":"/"},{"name":"tmpfs","size":3.9,"used":668.0,"avail":3.2,"dataReceivedTime_hours":0,"usePercent":17.0,"mountedOn":"/dev/shm"},{"name":"/dev/sda1","size":243.0,"used":33.0,"avail":198.0,"dataReceivedTime_hours":0,"usePercent":15.0,"mountedOn":"/boot"},{"name":"/dev/sdb1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u0"},{"name":"/dev/sdc1","size":50.0,"used":5.7,"avail":42.0,"dataReceivedTime_hours":0,"usePercent":13.0,"mountedOn":"/u1"},{"name":"/dev/sdd1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u2"}],"totalSize":168.0,"totalUsed":65.0,"totalAvail":95.0,"totalUsePercent":41.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"thresholdlimit_TotalUsePercent":null},"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Linux"},{"description":"Total Used : 5.7 Total Size :50.0 Used Percent : 13.0 Total Available : 42.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"WARNING","alertMessageDatetime":1439181961000,"alertMessageHostName":"gc12c-oms.coelab.com","alertMessageparameterName":"DISK","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":16.0,"used":13.0,"avail":2.2,"dataReceivedTime_hours":0,"usePercent":86.0,"mountedOn":"/"},{"name":"tmpfs","size":3.9,"used":668.0,"avail":3.2,"dataReceivedTime_hours":0,"usePercent":17.0,"mountedOn":"/dev/shm"},{"name":"/dev/sda1","size":243.0,"used":33.0,"avail":198.0,"dataReceivedTime_hours":0,"usePercent":15.0,"mountedOn":"/boot"},{"name":"/dev/sdb1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u0"},{"name":"/dev/sdc1","size":50.0,"used":5.7,"avail":42.0,"dataReceivedTime_hours":0,"usePercent":13.0,"mountedOn":"/u1"},{"name":"/dev/sdd1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u2"}],"totalSize":168.0,"totalUsed":65.0,"totalAvail":95.0,"totalUsePercent":41.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"thresholdlimit_TotalUsePercent":null},"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Linux"},{"description":"Total Used : 23.0 Total Size :50.0 Used Percent : 49.0 Total Available : 25.0 High Threshold : 30.0 Gb Low Threshold : 10.0 Gb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439181961000,"alertMessageHostName":"gc12c-oms.coelab.com","alertMessageparameterName":"DISK","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":{"fileSystem":[{"name":"/dev/sda3","size":16.0,"used":13.0,"avail":2.2,"dataReceivedTime_hours":0,"usePercent":86.0,"mountedOn":"/"},{"name":"tmpfs","size":3.9,"used":668.0,"avail":3.2,"dataReceivedTime_hours":0,"usePercent":17.0,"mountedOn":"/dev/shm"},{"name":"/dev/sda1","size":243.0,"used":33.0,"avail":198.0,"dataReceivedTime_hours":0,"usePercent":15.0,"mountedOn":"/boot"},{"name":"/dev/sdb1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u0"},{"name":"/dev/sdc1","size":50.0,"used":5.7,"avail":42.0,"dataReceivedTime_hours":0,"usePercent":13.0,"mountedOn":"/u1"},{"name":"/dev/sdd1","size":50.0,"used":23.0,"avail":25.0,"dataReceivedTime_hours":0,"usePercent":49.0,"mountedOn":"/u2"}],"totalSize":168.0,"totalUsed":65.0,"totalAvail":95.0,"totalUsePercent":41.0,"threshold_high_val":30.0,"threshold_low_val":10.0,"thresholdlimit_TotalSize":null,"thresholdlimit_TotalUsed":null,"thresholdlimit_TotalAvail":null,"thresholdlimit_TotalUsePercent":null},"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Linux"},{"description":"Total physical memory: 16382 Mb Memory used : 59.67 Mb Used virtual memory: 41136 Mb High Threshold : 0.0 Mb Low Threshold : 0.0 Mb","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"RAM","alertMessageType":"CRITICAL","alertMessageDatetime":1439182020000,"alertMessageHostName":"DL3807","alertMessageparameterName":"RAM","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":null,"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":{"memoryUsage":59.67,"totalPhysicalMemory":16382,"availablePhysicalMemory":6598,"availablePhysicalInPercent":0,"virtualMemoryMaxSize":56735,"availableVirtualMEmory":15599,"usedVirtualMemory":41136,"usedVirtualMemoryInPercent":0,"warning_High":0.0,"critical_High":0.0},"serverDown":null,"osType":"Windows"},{"description":"Usage in percent : 0.0 High Threshold : 0.0 Low Threshold : 0.0","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"CPU","alertMessageType":"CRITICAL","alertMessageDatetime":1439182020000,"alertMessageHostName":"DL3807","alertMessageparameterName":"CPU","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":null,"urlDown":null,"cedError":null,"winDisk":null,"winCPU":{"warning_High":0.0,"critical_High":0.0,"cpuUsageInPercent":0.0},"winRAM":null,"serverDown":null,"osType":"Windows"},{"description":"C DRIVE - Actual Value : 78.57, Warning level : 0.0, Critical level : 0.0","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439182020000,"alertMessageHostName":"DL3807","alertMessageparameterName":"DISK","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":null,"urlDown":null,"cedError":null,"winDisk":{"fileSystem":[{"name":"C","size":35.0,"used":27.5,"avail":7.5,"dataReceivedTime_hours":0,"usePercent":78.57,"mountedOn":null},{"name":"D","size":300.0,"used":123.11,"avail":176.89,"dataReceivedTime_hours":0,"usePercent":41.04,"mountedOn":null},{"name":"E","size":300.0,"used":295.04,"avail":4.96,"dataReceivedTime_hours":0,"usePercent":98.35,"mountedOn":null}],"warning_High":0.0,"critical_High":0.0,"totalDiskSize":635.0,"totalDiskUsed":445.65000000000003,"totalDiskAvail":189.35,"totalUsePercent":70.18110236220473},"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Windows"},{"description":"D DRIVE - Actual Value : 41.04, Warning level : 0.0, Critical level : 0.0","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439182020000,"alertMessageHostName":"DL3807","alertMessageparameterName":"DISK","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":null,"urlDown":null,"cedError":null,"winDisk":{"fileSystem":[{"name":"C","size":35.0,"used":27.5,"avail":7.5,"dataReceivedTime_hours":0,"usePercent":78.57,"mountedOn":null},{"name":"D","size":300.0,"used":123.11,"avail":176.89,"dataReceivedTime_hours":0,"usePercent":41.04,"mountedOn":null},{"name":"E","size":300.0,"used":295.04,"avail":4.96,"dataReceivedTime_hours":0,"usePercent":98.35,"mountedOn":null}],"warning_High":0.0,"critical_High":0.0,"totalDiskSize":635.0,"totalDiskUsed":445.65000000000003,"totalDiskAvail":189.35,"totalUsePercent":70.18110236220473},"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Windows"},{"description":"E DRIVE - Actual Value : 98.35, Warning level : 0.0, Critical level : 0.0","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"DISK","alertMessageType":"CRITICAL","alertMessageDatetime":1439182020000,"alertMessageHostName":"DL3807","alertMessageparameterName":"DISK","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":null,"urlDown":null,"cedError":null,"winDisk":{"fileSystem":[{"name":"C","size":35.0,"used":27.5,"avail":7.5,"dataReceivedTime_hours":0,"usePercent":78.57,"mountedOn":null},{"name":"D","size":300.0,"used":123.11,"avail":176.89,"dataReceivedTime_hours":0,"usePercent":41.04,"mountedOn":null},{"name":"E","size":300.0,"used":295.04,"avail":4.96,"dataReceivedTime_hours":0,"usePercent":98.35,"mountedOn":null}],"warning_High":0.0,"critical_High":0.0,"totalDiskSize":635.0,"totalDiskUsed":445.65000000000003,"totalDiskAvail":189.35,"totalUsePercent":70.18110236220473},"winCPU":null,"winRAM":null,"serverDown":null,"osType":"Windows"},{"description":"http://sap4all.lntinfotech.com:50000/irj/portal","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"URLLOG","alertMessageType":"FAIL","alertMessageDatetime":1439179681000,"alertMessageHostName":"dl3807","alertMessageparameterName":"http://sap4all.lntinfotech.com:50000/irj/portal","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":null,"urlDown":{"responseCode":407,"urlName":"http://sap4all.lntinfotech.com:50000/irj/portal","datetime":1439179681000,"status":"DOWN"},"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"serverDown":null,"osType":null},{"description":"http://172.17.99.161:50200/startPage","logFilesize":null,"serviceStatus":[],"responsetime":null,"alertMessageId":null,"alertMessageName":"URLLOG","alertMessageType":"FAIL","alertMessageDatetime":1439179802000,"alertMessageHostName":"SYBSERVER21","alertMessageparameterName":"http://172.17.99.161:50200/startPage","logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":null,"urlDown":{"responseCode":407,"urlName":"http://172.17.99.161:50200/startPage","datetime":1439179802000,"status":"DOWN"},"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"serverDown":null,"osType":null}]');
//	refreshAccordians('[{"description":"Log file : registry.log, File actual size : 129.5078125, Warning size : 0.0, Critical size : 0.0, Alert date : null","logFilesize":{"fileName":"registry.log","warningSizeLimit":0.0,"criticalSizeLimit":0.0,"fileSize":129.5078125},"serviceStatus":[],"responsetime":null,"serverDown":null,"alertMessageId":null,"alertMessageName":"LOGFILESIZE","alertMessageType":"CRITICAL","alertMessageDatetime":1440497304000,"alertMessageHostName":"CLDX-502-431","alertMessageparameterName":null,"logFileStatus":[],"linuxRAM":null,"linuxCPU":null,"linuxDisk":null,"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"osType":null}]');
//	refreshAccordians('[{"description":"HyS9aifWeb_epmsystem1","linuxRAM":null,"linuxCPU":null,"linuxDisk":null,"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"osType":null,"logFilesize":null,"serviceStatus":["HyS9aifWeb_epmsystem1-Stopped", "HyS9HsfSrv_epmsystem1-Running"],"responsetime":null,"serverDown":null,"alertMessageId":null,"alertMessageName":"SERVICE","alertMessageType":"CRITICAL","alertMessageDatetime":1441099868000,"alertMessageHostName":"CLDX-502-431","alertMessageparameterName":null,"logFileStatus":[]},{"description":"Service : HyS9HsfSrv_epmsystem1 is not running","linuxRAM":null,"linuxCPU":null,"linuxDisk":null,"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"osType":null,"logFilesize":null,"serviceStatus":["HyS9HsfSrv_epmsystem1-STOPPED"],"responsetime":null,"serverDown":null,"alertMessageId":null,"alertMessageName":"SERVICE","alertMessageType":"CRITICAL","alertMessageDatetime":1441099869000,"alertMessageHostName":"CLDX-502-431","alertMessageparameterName":null,"logFileStatus":[]},{"description":"Service : OracleProcessManager_EPM_epmsystem1 is not running","linuxRAM":null,"linuxCPU":null,"linuxDisk":null,"urlDown":null,"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"osType":null,"logFilesize":null,"serviceStatus":["OracleProcessManager_EPM_epmsystem1-STOPPED"],"responsetime":null,"serverDown":null,"alertMessageId":null,"alertMessageName":"SERVICE","alertMessageType":"CRITICAL","alertMessageDatetime":1441099870000,"alertMessageHostName":"CLDX-502-431","alertMessageparameterName":null,"logFileStatus":[]}]');
//	refreshAccordians('[{"alertMessageId":null, "alertMessageName":"RESPONSETIME", "alertMessageType":"CRITICAL", "alertMessageHostName":"CLOUDX-513-467", "alertMessageparameterName":null, "alertMessageDatetime":"Mon Dec 28 16:36:11 IST 2015", "description":null, "logFilesize":null, "winRAM":null, "winCPU":null, "winDisk":null, "linuxRAM":null, "linuxCPU":null, "linuxDisk":null, "cedError":null, "urlDown":null, "osType":null, "serviceStatus":[], "logFileStatus":[], "serverDow":null, "responsetime":{"respTimeInMillis":"2222.0", "threshold_high_val":"0.0", "threshold_low_val":"0.0", "page":"vjti.ltiappshop.com", "appName":"shikshacloud2"}}]');
//	refreshAccordians('[{"description":"http://172.17.99.161:50200/startPage","linuxRAM":null,"linuxCPU":null,"linuxDisk":null,"urlDown":{"responseCode":407,"urlName":"http//172.17.99.161:50200/startPage111","datetime":1442901962000,"status":"DOWN"},"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"osType":null,"logFilesize":null,"serviceStatus":[],"responsetime":null,"serverDown":null,"alertMessageId":null,"alertMessageName":"URLLOG","alertMessageType":"FAIL","alertMessageDatetime":1442901962000,"alertMessageHostName":"SYBSERVER21","hazelcastMapName":"mapJNJLNXURL01","alertMessageparameterName":"http://172.17.99.161:50200/startPage","logFileStatus":[]},{"description":"http://172.17.99.161:50200/startPage","linuxRAM":null,"linuxCPU":null,"linuxDisk":null,"urlDown":{"responseCode":407,"urlName":"http//172.17.99.161:50200/startPage111","datetime":1442901962000,"status":"DOWN"},"cedError":null,"winDisk":null,"winCPU":null,"winRAM":null,"osType":null,"logFilesize":null,"serviceStatus":[],"responsetime":null,"serverDown":null,"alertMessageId":null,"alertMessageName":"URLLOG","alertMessageType":"FAIL","alertMessageDatetime":1442901962000,"alertMessageHostName":"DL3807","hazelcastMapName":"mapJNJWINURL01","alertMessageparameterName":"http://172.17.99.161:50200/startPage2","logFileStatus":[]}]');

	var source = new EventSource("queueListner.html");
	source.onmessage = function(event) {
		var now=new Date();
		refreshAccordians(event.data);
      };
     
     /* START-- Code to unregister observer on tab change  */
      $(window).on("unload",function () { 
    	  $.ajax({
    	      type: "GET",
    	      url: "unregisterObserver.html",
    	      success: function(){
    	      }
    	  }); 
      });
      
      $(window).on("close",function () { 
    	  $.ajax({
    	      type: "GET",
    	      url: "unregisterObserver.html",
    	      success: function(){
    	      }
    	  }); 
      });
      /* END-- Code to unregister observer on tab change  */
});

 function generateSkeleton(i, parameterName, dataobj) {
	var hostName = dataobj.alertMessageHostName;
	//check for . since it gives error as illegal character
	if(hostName.indexOf(".") > -1) {
		hostName = hostName.split(".").join("_");
	}
	if(dataobj.osType=="Windows"){
		if (parameterName == "RAM") {
			 loadWindowsRAM(i, parameterName, dataobj);
		}
		
		if (parameterName == "CPU") {
			loadWindowsCPU(i, parameterName, dataobj);
		}
		
		if (parameterName == "DISK") {
			loadWindowsDISK(i, parameterName, dataobj);
		}
	}
	
	if(dataobj.osType=="Linux"){
		if (parameterName == "RAM") {
			 loadLinuxRam(i, parameterName, dataobj);
		}
		
		if (parameterName == "CPU") {
			loadLinuxCPU(i, parameterName, dataobj);
		}

		if (parameterName == "DISK") {
			loadLinuxDISK(i, parameterName, dataobj);
		}
	}
	
	if (parameterName == "RESPONSETIME") {
		loadresponsetime(i, parameterName, dataobj)
	}
	
	if (parameterName == "SERVICE") {
		loadservicestatus(i, parameterName, dataobj);
	}
	
	if (parameterName == "VMDOWN") {
		loadvmstatus(i, parameterName, dataobj);
	}

	if (parameterName == "LOGFILESIZE") {
		loadlogfilesize(i, parameterName, dataobj);
	}
	
	if(parameterName == "URLLOG") {
		 loadURL(i, parameterName, dataobj);
	}
	
	if(parameterName == "CEDERROR") {	
		loadcederror(i, parameterName, dataobj);
	}
}

function refreshAccordians(newcontent) {
	$("#sysHealthy").hide();
	/** flags to check if data for accordian exists */
	var isAC1Exists = false;
	var isAC2Exists = false;
	var isAC3Exists = false;
	var isAC4Exists = false;
	var isAC5Exists = false;
	var isAC6Exists = false;
	var isAC7Exists = false;
	var isAC8Exists = false;
	var isAC9Exists = false;

	var pushedData = JSON.parse(newcontent);
	for (var i = 0; i < pushedData.length; i++) {
		var pushedDataObj = pushedData[i];
		var parameterName = pushedDataObj.alertMessageName;
		if (parameterName == "RAM") {
			isAC1Exists = true;
		}
		if (parameterName == "CPU") {
			isAC2Exists = true;
		}
		if (parameterName == "DISK") {
			isAC3Exists = true;
		}
		if (parameterName == "RESPONSETIME") {
			isAC4Exists = true;
		}
		if (parameterName == "SERVICE") {
			isAC5Exists = true;
		}
		if (parameterName == "VMDOWN") {
			isAC6Exists = true;
		}
		if (parameterName == "LOGFILESIZE") {
			isAC7Exists = true;
		}
		if (parameterName == "URLLOG") {
			isAC8Exists = true;
		}
		if (parameterName == "CEDERROR") {
			isAC9Exists = true;
		}
	}

	for (var i = 0; i < pushedData.length; i++) {

		var pushedDataObj = pushedData[i];
		var parameterName = pushedDataObj.alertMessageName;
		var hostName = pushedDataObj.alertMessageHostName;
		//check for . since it gives error as illegal character
		if(hostName.indexOf(".") > -1) {
			hostName = hostName.split(".").join("_");
		}
		if (isAC1Exists && pushedDataObj.winRAM != null) {
			/* check if accordian exists; if not create it first */
			if ($('#label_ac-1').is(':visible')) {
				if($('#ac-1_split1_'+hostName).is(':visible')) {
				$('#ac1_split1_col1_'+ hostName).text('');
				$('#ac1_split1_col2_'+ hostName).text('');
				$('#ac1_split1_col3_'+ hostName).text('');
				$('#ac1_split1_col4_'+ hostName).text('');
				$('#ac1_split1_col5_'+ hostName).text('');
				$('#ac1_split1_col6_'+ hostName).text('');
				$('#ac1_split1_datespan_'+ hostName).text('');

				$('#ac1_split1_col1_'+ hostName).text('' + pushedDataObj.winRAM.totalPhysicalMemory + ' Mb');
				$('#ac1_split1_col2_'+ hostName).text(''+ (((pushedDataObj.winRAM.memoryUsage / pushedDataObj.winRAM.totalPhysicalMemory) * 100)).toFixed(2) + '%');
				$('#ac1_split1_col3_'+ hostName).text('' + pushedDataObj.winRAM.memoryUsage+' Mb');
				$('#ac1_split1_col4_'+ hostName).text('' + pushedDataObj.winRAM.usedVirtualMemory + 'Mb');
				$('#ac1_split1_col5_'+ hostName).text('' + pushedDataObj.winRAM.totalPhysicalMemory + 'Mb');
				$('#ac1_split1_col6_'+ hostName).text('' + pushedDataObj.winRAM.availableVirtualMEmory + 'Mb');
				$('#ac1_split1_datespan_'+ hostName).text(''+new Date(pushedDataObj.alertMessageDatetime).toString());
				}else{
					generateSkeleton(i, "RAM", pushedDataObj);
				}
			} else {
				generateSkeleton(i, "RAM", pushedDataObj);
			}
		} 
		
		else if (isAC1Exists && pushedDataObj.linuxRAM != null) {
			/* check if accordian exists; if not create it first */
			if ($('#label_ac-1').is(':visible')) {
				if($('#ac-1_split1_'+hostName).is(':visible')) {
				$('#ac1_split1_col1_'+ hostName).text('');
				$('#ac1_split1_col2_'+ hostName).text('');
				$('#ac1_split1_col3_'+ hostName).text('');
				$('#ac1_split1_col4_'+ hostName).text('');
				$('#ac1_split1_col5_'+ hostName).text('');
				$('#ac1_split1_col6_'+ hostName).text('');
				$('#ac1_split1_datespan_'+ hostName).text('');

				$('#ac1_split1_col1_'+ hostName).text('' + pushedDataObj.linuxRAM.memCached);
				$('#ac1_split1_col3_'+ hostName).text('' + pushedDataObj.linuxRAM.memUsed);
				$('#ac1_split1_col2_'+ hostName).text('' + ((pushedDataObj.linuxRAM.memUsed / pushedDataObj.linuxRAM.memTotal) * 100).toFixed(2));
				$('#ac1_split1_col4_'+ hostName).text('' + pushedDataObj.linuxRAM.memBuffers);
				$('#ac1_split1_col5_'+ hostName).text('' + pushedDataObj.linuxRAM.memTotal);
				$('#ac1_split1_col6_'+ hostName).text('' + pushedDataObj.linuxRAM.memFree);
				$('#ac1_split1_datespan_'+ hostName).text(''+new Date(pushedDataObj.alertMessageDatetime).toString());
				}else{
					
					generateSkeleton(i, "RAM", pushedDataObj);
				}
			} else {
				
				generateSkeleton(i, "RAM", pushedDataObj);
			}

		}
		else if (!isAC1Exists && !isAC2Exists && !isAC3Exists) {
			$('#label_ac-1').hide();
			clearAccordion('ac1_block_1');
			clearAccordion('ac1_block_2');
			$("#ac1_container_small").hide();
		}
		
		if (isAC2Exists && pushedDataObj.winCPU != null) {
			/* check if accordian exists; if not create it first */
			if ($('#label_ac-2').is(':visible')) {
				if($('#ac-2_split1_'+hostName).is(':visible')) {
				$('#ac-2_split1_col1_'+ hostName).text('');	
				$('#ac2_split1_datespan_'+ hostName).text('');
				$('#ac-2_split1_col1_'+ hostName).text('' + pushedDataObj.winCPU.cpuUsageInPercent.toFixed(2)+'%');
				$('#ac2_split1_datespan_'+ hostName).text(''+new Date(pushedDataObj.alertMessageDatetime).toString());
				}else{
					generateSkeleton(i, "CPU", pushedDataObj);
				}
			} else {
				generateSkeleton(i, "CPU", pushedDataObj);
			}
		}
		
		else if (isAC2Exists && pushedDataObj.linuxCPU != null) {
			/* check if accordian exists; if not create it first */
			if ($('#label_ac-2').is(':visible')) {
				if($('#ac-2_split1_'+hostName).is(':visible')) {
				$('#ac-2_split1_col1_'+ hostName).text('');	
				$('#ac-2_split1_col2_'+ hostName).text('');	
				$('#ac-2_split1_col3_'+ hostName).text('');	
				$('#ac2_split1_datespan_'+ hostName).text('');
				$('#ac-2_split1_col1_'+ hostName).text('' + pushedDataObj.linuxCPU.loadAverage5min);
				$('#ac-2_split1_col2_'+ hostName).text('' + pushedDataObj.linuxCPU.loadAverage10min);
				$('#ac-2_split1_col3_'+ hostName).text('' + pushedDataObj.linuxCPU.loadAverage15min);
				$('#ac2_split1_datespan_'+ hostName).text(''+new Date(pushedDataObj.alertMessageDatetime).toString());
				}else{
					generateSkeleton(i, "CPU", pushedDataObj);
				}
			} else {
				
				generateSkeleton(i, "CPU", pushedDataObj);
			}
		}
		else if (!isAC1Exists && !isAC2Exists && !isAC3Exists) {
			$('#label_ac-2').hide();
			clearAccordion('ac2_block_1');
			clearAccordion('ac2_block_2');
			$("#ac2_container_small").hide();
		}
		
	if (isAC3Exists && pushedDataObj.winDisk != null) {
		/* check if accordian exists; if not create it first */
		if ($('#label_ac-3').is(':visible')) {
			if($('#ac-3_split1_'+hostName).is(':visible')) {
				$('#ac3_split1_datespan_'+ hostName).text('');
				$('#ac3_split1_datespan_'+ hostName).text('' + new Date(pushedDataObj.alertMessageDatetime).toString());
					//clearing the table
				$('#ac-3_split1_table_'+hostName+' tbody tr').each(
						function() {
							this.parentNode.removeChild(this);
						});
				var fileSystemArr = pushedDataObj.winDisk.fileSystem;
				var appendRow ='';
				for(var count=0;count<fileSystemArr.length;count++){
					appendRow= "<tr><td class='ac-3_col1'>"
					+ "<span id='ac-3_split1_col1_"+ hostName+ "'>"
					+ fileSystemArr[count].name
					+ "</span></td>"
					+ "<td class='ac-3_col2'>"
					+ "<span id='ac-3_split1_col2_"
					+ hostName
					+ "'>"
					+ fileSystemArr[count].usePercent
					+ "%</span></td>"
					+ "<td class='ac-3_col3'>"
					+ "<span id='ac-3_split1_col3_"
					+ hostName
					+ "'>"
					+ pushedDataObj.winDisk.critical_High
					+ "</span></td>"
					+ "<td class='ac-3_col4'>"
					+ "<span id='ac-3_split1_col4_"
					+ hostName
					+ "'>"
					+ pushedDataObj.winDisk.warning_High
					+ "</span></td>"
					+ "</tr>";
					$("#ac-3_split1_table_"+hostName).last().append(appendRow);
				}
			}else{
				generateSkeleton(i, "DISK", pushedDataObj);
			}
		} else {
			generateSkeleton(i, "DISK", pushedDataObj);
		}
	} 
	
	else if (isAC3Exists && pushedDataObj.linuxDisk != null) {
		/* check if accordian exists; if not create it first */
		if ($('#label_ac-3').is(':visible')) {
			if($('#ac-3_split1_'+hostName).is(':visible')) {
				$('#ac3_split1_datespan_'+ hostName).text('');
				$('#ac3_split1_datespan_'+ hostName).text('' + new Date(pushedDataObj.alertMessageDatetime).toString());
					//clearing the table
				$('#ac-3_split1_table_'+hostName+' tbody tr').each(
						function() {
							this.parentNode.removeChild(this);
						});
				
				var fileSystemArr = pushedDataObj.linuxDisk.fileSystem;
				var appendRow ='';
				for(var count=0;count<fileSystemArr.length;count++){
					appendRow= "<tr><td class='ac-3_col1'>"
					+ "<span id='ac-3_split1_col1_"+ hostName+ "'>"
					+ fileSystemArr[count].name
					+ "</span></td>"
					+ "<td class='ac-3_col2'>"
					+ "<span id='ac-3_split1_col2_"
					+ hostName
					+ "'>"
					+ fileSystemArr[count].usePercent
					+ "%</span></td>"
					+ "<td class='ac-3_col3'>"
					+ "<span id='ac-3_split1_col3_"
					+ hostName
					+ "'>"
					+ pushedDataObj.linuxDisk.critical_High
					+ "</span></td>"
					+ "<td class='ac-3_col4'>"
					+ "<span id='ac-3_split1_col4_"
					+ hostName
					+ "'>"
					+ pushedDataObj.linuxDisk.warning_High
					+ "</span></td>"
					+ "</tr>";
					$("#ac-3_split1_table_"+hostName).last().append(appendRow);
				}
			}else{
				generateSkeleton(i, "DISK", pushedDataObj);
			}
		} else {
			generateSkeleton(i, "DISK", pushedDataObj);
		}
	}
	else if (!isAC1Exists && !isAC2Exists && !isAC3Exists) {
		$('#label_ac-3').hide();
		clearAccordion('ac3_block_1');
		clearAccordion('ac3_block_2');
		$("#ac3_container_small").hide();
	}
	 if (isAC4Exists && pushedDataObj.responsetime != null) {
		var appName= JSON.stringify(pushedDataObj.responsetime.appName);
		var page= JSON.stringify(pushedDataObj.responsetime.page);
		var threshold_high_val= JSON.stringify(pushedDataObj.responsetime.threshold_high_val);
		var threshold_low_val= JSON.stringify(pushedDataObj.responsetime.threshold_low_val);
//		 console.log('inside AC4');
//		 console.log((pushedDataObj.responsetime.respTimeInMillis)/1000);
		/* check if accordian exists; if not create it first */
		if ($('#label_ac-4').is(':visible')) {
			clearAccordion('ac4_block_2');
			if($('#ac-4_split1_'+hostName).is(':visible')) {
				clearAccordion('ac4_block_2');
				$('#ac4_split1_col3_'+ hostName).text('');
				$('#ac4_split1_col3_'+ hostName).text('' + (pushedDataObj.responsetime.respTimeInMillis)/1000);
			
			}else{
				generateSkeleton(i, "RESPONSETIME", pushedDataObj);
			}
		} else {
			generateSkeleton(i, "RESPONSETIME", pushedDataObj);
		}
	 }
	else if (!isAC4Exists) {
		$('#label_ac-4').hide();
		clearAccordion('ac4_block_1');
		clearAccordion('ac4_block_2');
		$("#ac4_container_small").hide();
	}
	 
	if (isAC5Exists && pushedDataObj.serviceStatus.length != 0) {
		console.log('inside isAC5Exists');
		/* check if accordian exists; if not create it first */
		if ($('#label_ac-5').is(':visible')) {
			console.log('inside label is visible');
			if($('#ac-5_split1_'+hostName).is(':visible')) {
				$('#ac-5_split1_table_'+hostName+' tbody tr').each(
						function() {
							this.parentNode
									.removeChild(this);
						});
				$('#ac5_split1_col1_'+ hostName).text('' + pushedDataObj.serviceStatus);
				var logFilesArrayValues= new Array();
				var logFilesArray = pushedDataObj.serviceStatus; 
			}else{
				generateSkeleton(i, "SERVICE", pushedDataObj);
			}
		} else {
			generateSkeleton(i, "SERVICE", pushedDataObj);
		}
	} 
	else if (!isAC5Exists) {
			$('#label_ac-5').hide();
			clearAccordion('ac5_block_1');
			clearAccordion('ac5_block_2');
			$("#ac5_container_small").hide();
	}
	
	if (isAC7Exists && pushedDataObj.logFilesize.length != 0) {
		console.log('inside isAC7Exists');
		/* check if accordian exists; if not create it first */
		if ($('#label_ac-7').is(':visible')) {
			console.log('inside label visible');
			if($('#ac-7_split1_'+hostName).is(':visible')) {
				$('#ac-7_split1_table_'+hostName+' tbody tr').each(
						function() {
							this.parentNode
									.removeChild(this);
						});
				
				var logFilesArrayValues= new Array();
				var logFilesArray = pushedDataObj.logFilesize; 
				for(var a=0;a<logFilesArray.length;a++) {
					
					var str = logFilesArray[a].split("-");
					logFilesArrayValues.push([str[0],parseFloat(str[1])]);
				}
				console.log(pushedDataObj);
				logFilesArrayValues = logFilesArrayValues.sort(function(a,b){return b[1] > a[1];});
				var servicesLength = logFilesArrayValues.length;
				console.log(servicesLength);
				for (var serviceIndex = 0; serviceIndex < servicesLength; serviceIndex++) {
					var appendRow = "<tr id='ac-7_split1_table_" + serviceIndex
					+ "' class='ac-7_split1_table_service'>" + "<td>"
					+ logFilesArrayValues[serviceIndex] + "</td>" + "</tr>";
					$("#ac-7_split1_table_" + hostName).last().append(appendRow);
				}
			
			}else{
				generateSkeleton(i, "LOGFILESIZE", pushedDataObj);
			}
		} else {
			generateSkeleton(i, "LOGFILESIZE", pushedDataObj);
		}} 
	else if (!isAC7Exists) {
			$('#label_ac-7').hide();
			clearAccordion('ac7_block_1');
			clearAccordion('ac7_block_2');
			$("#ac7_container_small").hide();
	}
	
	if (isAC8Exists && pushedDataObj.urlDown != null) {
		 var urlname = pushedDataObj.urlDown.urlName;
		 if(urlname.indexOf("/") > -1) {
			 urlname = urlname.split("/")[2];
			 urlname = urlname.split(".").join("_").split(":")[0];
		 }
//			 check if accordian exists; if not create it first 
		if ($('#label_ac-8').is(':visible')) {
			if($('#ac-8_split1_'+hostName).is(':visible')) {
				if($('#ac-8_spilt2_table_tr_'+urlname).is(':visible')){
					$('#ac-8_split1_col1_'+ urlname).text('');
					$('#ac-8_split1_col1_'+ urlname).text(pushedDataObj.urlDown.urlName);
					$('#ac-8_split1_col3_' + urlname).text('');
					$('#ac-8_split1_col3_' + urlname).text(pushedDataObj.urlDown.status);
				}
				else {
					appendURL(i, pushedDataObj);
				}
			}else{
				generateSkeleton(i, "URLLOG", pushedDataObj);
			}
		} else {
			generateSkeleton(i, "URLLOG", pushedDataObj);
		}
		}
	else if (!isAC8Exists) {
		$('#label_ac-8').hide();
		clearAccordion('ac8_block_1');
		clearAccordion('ac8_block_2');
		$("#ac8_container_small").hide();
	}
		
		
		if (isAC9Exists && pushedDataObj.cedError != null) {
			/* check if accordian exists; if not create it first */
			if ($('#label_ac-9').is(':visible')) {
				if($('#ac-9_split1_'+hostName).is(':visible')) {
					/*$('#ac9_split1_col1_' + hostName).text('');
					$('#ac9_split1_col1_' + hostName).text(pushedDataObj.cedError.errorMessage);*/
					$('#ac9_split1_datespan_'+ hostName).text('');
					$('#ac9_split1_datespan_'+ hostName).text(''+new Date(pushedDataObj.alertMessageDatetime).toString());
					appendOracleLog(pushedDataObj);
					var numOfRecords = $('#ac-9_split2_table_'+hostName + ' tr').length;
					$('#oracle_alert').html("<img id='alertLogo' width='10px' src='images/custom/alertVal.png'>"+numOfRecords);
				}else{
					generateSkeleton(i, "CEDERROR", pushedDataObj);
				}
			} else {
				generateSkeleton(i, "CEDERROR", pushedDataObj);
			}
		}
		else if (!isAC9Exists) {
			$('#label_ac-9').hide();
			clearAccordion('ac9_block_1');
			clearAccordion('ac9_block_2');
			$("#ac9_container_small").hide();
//			clearAccordion('ac9_container_small');
		}
		
	}
	return true;
}

function clearAccordion(id) {
	var el = document.getElementById(id);
	if(el != null){
		while (el.hasChildNodes()) {
			el.removeChild(el.lastChild);
		}
	}
}

/* START -- global functions for all parameters to clear its accordions */
function clearAccordion_URL() {
	$("#ac8_container_small").hide();
	$("#label_ac-8").hide();
	clearAccordion('ac8_container_small');
}

function clearAccordian_CEDError(){
	$("#ac9_container_small").hide();
	$("#label_ac-9").hide();
	clearAccordion('ac9_container_small');
}

function clearAccordian_logFileStatus(){
	$("#ac7_container_small").hide();
	$("#label_ac-7").hide();
	clearAccordion('ac7_container_small');
}

function clearAccordian_SystemInfo(){
	$("#ac1_container_small").hide();
	$("#ac2_container_small").hide();
	$("#ac3_container_small").hide();
	
	$("#label_ac-1").hide();
	$("#label_ac-2").hide();
	$("#label_ac-3").hide();
	
	clearAccordion('ac1_block_1');
	clearAccordion('ac1_block_2');
	clearAccordion('ac2_block_1');
	clearAccordion('ac2_block_2');
	clearAccordion('ac3_block_1');
	clearAccordion('ac3_block_2');
}
/* END -- global functions for all parameters to clear its accordions */

function sortBySecondColumn(a, b) {
    if (a[0] === b[0]) {
        return 0;
    }
    else {
        return (a[0] < b[0]) ? -1 : 1;
    }
}
