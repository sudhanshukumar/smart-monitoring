package com.lnt.smartit.mysql.model;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="application")
public class Applications implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private String appId;
	
	@Column(name="name")
	private String appName;
	
	@Column(name="description")
	private String description;
	
	@ManyToOne
	@JoinColumn(name = "app_type_code", referencedColumnName = "code", insertable = false, updatable = false)
	private ApplicationType appType;
	
	@Column(name="void_ind")
	private String voidInd;
	
	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ApplicationType getAppType() {
		return appType;
	}

	public void setAppType(ApplicationType appType) {
		this.appType = appType;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}
	
	public String getVoidInd() {
		return voidInd;
	}

	public void setVoidInd(String voidInd) {
		this.voidInd = voidInd;
	}

}
