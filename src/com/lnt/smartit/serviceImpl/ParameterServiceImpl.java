package com.lnt.smartit.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lnt.smartit.mysql.dao.ParameterDao;
import com.lnt.smartit.mysql.model.ParameterMaster;
import com.lnt.smartit.services.ParameterService;

@Service
public class ParameterServiceImpl implements ParameterService {
	@Autowired
	private ParameterDao dao;
	
	public List<ParameterMaster> getParameters() {
		return dao.getParameters();
	}
	
	public ParameterMaster getParameterMasterById(String id) {
		return dao.getParameterMasterById(id);
	}
	
	public String getParameterMasterByParamName(String name) {
		return dao.getParameterMasterByParamName(name);
	}
}
