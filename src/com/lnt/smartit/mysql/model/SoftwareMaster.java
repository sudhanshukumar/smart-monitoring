package com.lnt.smartit.mysql.model;

// Generated Aug 11, 2014 11:23:06 AM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * SoftwareMaster generated by hbm2java
 */
public class SoftwareMaster implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String softwareId;
	private int version;
	@NotEmpty
	private String softwareName;
	private String description;
	private boolean deletionStatus;
	private String createdBy;
	private Date creationDate;
	private String modifiedBy;
	private Date modifyDate;
	private Set<?> deviceMasters = new HashSet<Object>(0);

	public SoftwareMaster() {
	}

	public SoftwareMaster(String softwareId, String softwareName,
			boolean deletionStatus, String createdBy, Date creationDate) {
		this.softwareId = softwareId;
		this.softwareName = softwareName;
		this.deletionStatus = deletionStatus;
		this.createdBy = createdBy;
		this.creationDate = creationDate;
	}

	public SoftwareMaster(String softwareId, String softwareName,
			String description, boolean deletionStatus, String createdBy,
			Date creationDate, String modifiedBy, Date modifyDate,
			Set<?> deviceMasters) {
		this.softwareId = softwareId;
		this.softwareName = softwareName;
		this.description = description;
		this.deletionStatus = deletionStatus;
		this.createdBy = createdBy;
		this.creationDate = creationDate;
		this.modifiedBy = modifiedBy;
		this.modifyDate = modifyDate;
		this.deviceMasters = deviceMasters;
	}

	public String getSoftwareId() {
		return this.softwareId;
	}

	public void setSoftwareId(String softwareId) {
		this.softwareId = softwareId;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getSoftwareName() {
		return this.softwareName;
	}

	public void setSoftwareName(String softwareName) {
		this.softwareName = softwareName;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isDeletionStatus() {
		return this.deletionStatus;
	}

	public void setDeletionStatus(boolean deletionStatus) {
		this.deletionStatus = deletionStatus;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifyDate() {
		return this.modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	@JsonIgnore
	public Set<?> getDeviceMasters() {
		return this.deviceMasters;
	}

	public void setDeviceMasters(Set<?> deviceMasters) {
		this.deviceMasters = deviceMasters;
	}

	@Override
	public String toString() {
		return "SoftwareMaster [softwareId=" + softwareId + ", version="
				+ version + ", softwareName=" + softwareName + ", description="
				+ description + ", deletionStatus=" + deletionStatus
				+ ", createdBy=" + createdBy + ", creationDate=" + creationDate
				+ ", modifiedBy=" + modifiedBy + ", modifyDate=" + modifyDate
				+ "]";
	}
}
