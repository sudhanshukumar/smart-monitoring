/**
 * 
 */
$(document).ready(function() {
	$("#alert").hide();
	$("#confirmDelete").hide();

	$("#addNewApplication").click(function() {
		window.location.href = "application.html";
	});
	manageApplicationGrid();

});

function manageApplicationGrid() {
	$("#addedApplicationGrpHistory")
			.jqGrid(
					{
						jsonReader : {
							root : "gridData", // an array that contains the
												// actual data
							page : "page", // current page of the query
							total : "total", // total pages for the query
							records : "records",// total number of records for
												// the query
							repeatitems : true,
							id : "applicationId" // the unique id of the row
						},
						url : 'populateApplicationGrpGrid.html',
						datatype : "json",
						mtype : "POST",
						colNames : [ 'Application Id', 'Application Name','Application Description', 'Actions' ],
						colModel : [
								{
									name : 'applicationId',
									label : 'Application Id',
									align : 'center',
									width : 30,
									hidden:true,
									sortable: true
								},
								{
									name : 'applicationName',
									label : 'Application Name',
									align : 'center',
									width : 50,
									sortable: true
								},
								{
									name : 'description',
									label : 'Application Description',
									align : 'center',
									width : 70,
									sortable: true
								},
								{
									width : 80,
									fixed : true,
									sortable : false,
									resize : false,
									formatter : function(cellvalue, options,
											rowObject) {

										return addedApplicationActionButtons(cellvalue,
												options, rowObject);
									}
								} ],
						pager : '#addedApplicationGrpPager',
						rowNum : 10,
						rowList : [ 10, 20, 30 ],
						viewrecords : true,
						gridview : true,
						loadonce: true,
						width : 900,
						height: 300
					});
}

function addedApplicationActionButtons(cellvalue, options, rowObject) {
	var previewApplicationLink= '<img title="Preview" onClick=previewApplication("'
	    +encodeURI(JSON.stringify(rowObject))
	    +'") src="images/common/preview-16.png"/>';
	var updateApplicationLink = '<img title="Edit"  onClick=editApplication("'
			+ encodeURI(JSON.stringify(rowObject))
			+ '"); src="images/common/edit-6-16.png" />';
	var deleteApplicationLink = '<img title="Delete" onClick=deleteApplication("'
			+ encodeURI(JSON.stringify(rowObject))
			+ '") src="images/common/delete-3-16.png"/>';

	var actionLink = "&nbsp;" + previewApplicationLink + "&nbsp;" + "&nbsp;" + updateApplicationLink+ "&nbsp;" + "&nbsp;" + deleteApplicationLink;
	return actionLink;
}

function deleteApplication(value){
	$("#confirmDelete").show();
	$( "#confirmDelete" ).dialog({
		 resizable: false,
		 autoOpen: false,
		 height:200,
		 width: 340,
		 position: [485, 185],
		 modal: true,
		 buttons: {
			 "Yes": function() {
				 	var dataJSON = JSON.parse(decodeURI(value));
				 	var appId=dataJSON.applicationId;
				 	$.ajax({
				 		type : "POST",
				 		url : "deleteApplication.html",
				 		data : "applicationId=" + appId, 
				 		success : function(response) {
				 			window.location.href = "manageApplication.html";
				 		},
				 		cache: false
				 	}); 
			 },
				Cancel: function() {
			
	 							$( this ).dialog( "close" );
	 						}
		 }
		 
	});
	
	$("#confirmDelete").dialog("open");
	$(".ui-dialog-title").text('Delete?');
	$(".ui-dialog-titlebar-close").hide();
	$("#confirmDelete").text("Delete this Application ?");
	
}
function editApplication(value){
	var dataJSON = JSON.parse(decodeURI(value));
	var appId=dataJSON.applicationId;	
	window.location.href =  "application.html?appId="+appId+"&action=edit";
	
}
function previewApplication(value){
	var dataJSON = JSON.parse(decodeURI(value));
	var appId=dataJSON.applicationId;
	window.location.href =  "application.html?appId="+appId+"&action=preview";	
}