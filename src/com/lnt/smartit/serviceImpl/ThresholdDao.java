package com.lnt.smartit.serviceImpl;

import java.util.List;
import java.util.Map;

import com.lnt.smartit.mysql.model.EmailMaster;
import com.lnt.smartit.mysql.model.NotificationGroup;
import com.lnt.smartit.mysql.model.ThresholdMaster;
import com.lnt.smartit.mysql.model.ThresholdNotifyGroupMapping;

public interface ThresholdDao {
	public List<ThresholdMaster> getThresholds();
	public List<NotificationGroup> getNotificationGroup();
	public List<EmailMaster> getEmailMaster();
	public boolean deleteThreshold(String threshId);
	public ThresholdMaster getthresholdMasterById(ThresholdMaster thresholdMaster);
	public Map<String, String> getThresholdNotifyMapping(ThresholdMaster thresholdMaster);
	public String saveThresholdMaster(ThresholdMaster thresholdMaster);
	public String savenotificationGroup(NotificationGroup notificationGroup);
	public List<NotificationGroup> getAllNotificationGroup();
	public NotificationGroup getNotificationGroupById(NotificationGroup notificationGroup);
	public String deleteNotificationGroup(NotificationGroup notificationGroup);
	public void editNotificationGroup(NotificationGroup notificationgp);
	public List<ThresholdNotifyGroupMapping> getNotificationThresholdMappingById(String notificationGroupId);
	public void saveNotificationThresholdMapping(List<ThresholdNotifyGroupMapping> thresholdNotifyGroupMappingList);
	public void saveThresholdNotifyGroupMappingService(String thresholdMasterId, String thresholdGroupSelectedIds);
	public void deleteThresholdNotifyGroupMappingService(ThresholdNotifyGroupMapping thresholdNotifyGroupMapping);
	public String deleteThresholdMaster(ThresholdMaster threshold);
	public String saveThresholdsForLogs(ThresholdMaster thresholdMaster,String[] logFileName, String[] logCriticalThreshold,
			String[] logWarningThreshold);
	public boolean checkId(Integer thresholdId);
	public String saveThresholdsForServices(ThresholdMaster thresholdMaster,String[] serviceName);
	public List<Map<String,String>> previewThresholdForLog(String thresholdId);
	public List<Map<String,String>> previewThresholdForService(String thresholdId);
	public String updateThresholdMaster(ThresholdMaster thresholdMaster);
	public void editThresholdsForLog(ThresholdMaster thresholdMaster,String[] logWarningThreshold, String[] logCriticalThreshold);
}

