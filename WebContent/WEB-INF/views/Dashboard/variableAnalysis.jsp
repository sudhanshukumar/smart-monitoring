<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><spring:message code="variable.analysis.page.title"></spring:message>
</title>
<!-- Common CSS files of Bootstrap and JQuery -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="assets/css/bootstrap-combined.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/common/ui.jqgrid.css" />
<link rel="stylesheet" type="text/css" href="css/common/jquery-ui.min.css" />
<link href="scripts/c3-0.4.10/c3.css" rel="stylesheet" type="text/css" charset="utf-8">

<!-- Custom CSS files for this Page -->
<link rel="stylesheet" href="css/custom/specific/nav.css">

<!-- C3 scripts -->
<script src="scripts/d3-master/d3.js" charset="utf-8"></script>
<script src="scripts/c3-0.4.10/c3.js" charset="utf-8"></script>

<!-- Common Scripts of Bootstrap and JQuery -->
<script src="scripts/common/jquery-2.2.3.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="scripts/common/jquery.jqGrid.min.js"></script>
<script src="scripts/common/jquery-ui.js"></script>
<script src="scripts/common/grid.locale-en.js"></script>

<!-- Custom stylesheets -->
<link rel="stylesheet" href="css/custom/specific/varAnal.css">

<!-- Custom scripts -->
<script src="scripts/custom/varAnal.js"></script>

<!-- Highcharts script -->
<script src="scripts/common/highstocks.js"></script>
<script src="scripts/common/highcharts.js"></script>

<script src="scripts/custom/tab_menu.js"></script>

<% String session_val = (String)session.getAttribute("modules"); %>

<script type="text/javascript">
	session_obj= '<%=session_val%>';
</script>

</head>
<body>

	<div class="container-fluid" style="height: 100%">
	  <%@ include file="../common/header.jsp"%>
  
	  <nav class="navbar navbar-inverse" style="margin-top: 0px">
	      <ul class="nav navbar-nav navbar-xs">
	        <li "id="Dashboard_ELK"><a href="dashboard.html">Dashboard<span class="sr-only">(current)</span></a></li>
	        <li id="ManageThreshold_ELK"><a href="manageThresHold.html">Manage Rule</a></li>
	       <!--  <li id="AlertHistory"><a href="problemHistory.html">Alert History</a></li> -->
	        <li class="dropdown active" >
	          <a href="#" class="dropdown-toggle" id="Insights" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Insights<span class="caret"></span></a>
	          <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
	            <li><a href="query.html" style="font-size:11px!important;">Analytics</a></li>
	            <!-- <li><a href="kibanaDashboard.html" style="font-size:11px!important;">Prediction</a></li> -->
	            <!-- <li><a href="variableAnalysis.html">Variable Analysis</a></li> -->
	            <li class="dropdown-submenu"> <a tabindex="-1" href="#" style="font-size:11px!important;">Reports</a>
                      <ul class="dropdown-menu">
							<li id="Prediction">
								<a href="predictionreport.html" style="font-size:11px!important;">Prediction</a> </li>
							<li id="Trends">
								<a href="trendingreport.html" style="font-size:11px!important;">Trends</a> </li>
							<li id="Alert_History">
							 	<a href="alerthistoryreport.html" style="font-size:11px!important;">Alert History</a> </li>
							<li id="LogCorrelation">
								<a href="correlationreport.html" style="font-size:11px!important;">Log Correlation</a> </li>
							<li id="WeblogicReport">
								<a href="weblogicreport.html" style="font-size:11px!important;">Weblogic Report</a> </li>
							<li id="WeblogicDrillDownReport">
								<a href="weblogicdrilldownreport.html" style="font-size:11px!important;">Weblogic DrillDown Report</a> </li>
							<li id="SapApplicationReport">
								<a href="sapapplicationreport.html" style="font-size:11px!important;">Sap Application Report</a> </li>
						 <li id="SapApplicationAdoption">
                           <a href="sapapplicationadoption.html" style="font-size:11px!important;">Sap Application Adoption</a> </li>
                            <li id="SapPlatformOperation">
                           <a href="sapplatformoperation.html" style="font-size:11px!important;">Sap Platform Operations</a> </li>
                           <li id="SapSecurityAuditing">
                           <a href="sapsecurityauditing.html" style="font-size:11px!important;">Sap Security Auditing</a> </li>
						</ul>
                    </li>
	          </ul>
          </li>
	        <!-- <li class="active" id="LogAnalysis"><a href="query.html">Insights</a></li> -->
	       <%--  <li id="LogCorrelation"><a href="correlation.html">Log Correlation</a></li>
	        <li id="VariableAnalysis"><a href="variableAnalysis.html"><spring:message code="label.admin.menu5"></spring:message></a></li>
	        <li class="dropdown" id="Reports">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reports <span class="caret"></span></a>
	          <ul class="dropdown-menu">
	            	<li id="CreateReport"><a href="charts.html">Create Report</a></li>
		            <li id="CreateDashboard"><a href="createuserdashboard.html">Create Dashboard</a></li>
		            <li id="ViewDashboard"><a href="list_dashboard.html">View Dashboard</a></li>
	          </ul>
	        </li> --%>
	      </ul>
	  </nav>

	<div id="varContainer" class="varMainContainer">
		<section id="split1_section">
			<div id="split1">
				<div class="split_1_heading">
					<spring:message code="split1.heading"></spring:message>
				</div>
				<div id="split1_panel">
					<table>

						<tbody>
							<%-- <tr id="split1_table1_column1">
								<th><strong><spring:message
											code="split1.table1.column1"></spring:message></strong></th>
								<td><input type="text" id="split1_input1" /></td>
								<td><label id="split1_input1_label" class="small_text"
									onclick="return viewList(id);"><spring:message
											code="split1.input2"></spring:message> </label></td>
							</tr> --%>
							<tr class="split1.table1.column2">
								<th><strong><spring:message
											code="split1.table1.column2"></spring:message></strong></th>
								<td><input type="text" id="split1_input2" /></td>
								<td><label id="split1_input2_label" class="small_text"
									onclick="return viewList(id);"><spring:message
											code="split1.input4"></spring:message> </label></td>
							</tr>
							<tr class="split1.table1.column2">
								<th><strong><spring:message code="split1.table1.column3"></spring:message></strong></th>
								<td>
						  			<select id="select_param">
										<option><spring:message code="split1.radio1"></spring:message> </option>
										<option><spring:message code="split1.radio2"></spring:message> </option>
										<option><spring:message code="split1.radio3"></spring:message> </option>
										<%-- <option><spring:message code="split1.radio4"></spring:message> </option> --%>
									 </select>
								</td>
							</tr>
							<!-- <tr>
								<td>
									<input type="button" value="Go" class="split1_section_button_Go"></input>
								</td>
							</tr> -->
							
						</tbody>
					</table>
					<%-- <div class="split1_radio_buttons_box">
						<table>
							<tbody>
								<tr>
									<td><input type="radio" class="split1_radio" name="split1_radio"
										id="split1_radio1" checked="checked" /><label
										for="split1_radio1"><spring:message
												code="split1.radio1"></spring:message> </label></td>
									<td><input type="radio" class="split1_radio" name="split1_radio"
										id="split1_radio2" /><label for="split1_radio2"><spring:message
												code="split1.radio2"></spring:message> </label></td>
								</tr>
								<tr>
									<td><input type="radio" class="split1_radio" name="split1_radio"
										id="split1_radio3" /><label for="split1_radio3"><spring:message
												code="split1.radio3"></spring:message> </label></td>
									<td><input type="radio" class="split1_radio" name="split1_radio"
										id="split1_radio4" /><label for="split1_radio4"><spring:message
												code="split1.radio4"></spring:message> </label></td>
								</tr>
								<tr>

									<td><input type="radio" class="split1_radio" name="split1_radio"
										id="split1_radio5" /><label for="split1_radio5"><spring:message
												code="split1.radio5"></spring:message> </label></td>
									<td><input type="radio" class="split1_radio" name="split1_radio"
										id="split1_radio6" /><label for="split1_radio6"><spring:message
												code="split1.radio6"></spring:message> </label></td>
								</tr>
							</tbody>
						</table>
						<th><strong><spring:message code="split1.table1.column3"></spring:message></strong></th>
						<select id="select_param">
							<option><spring:message code="split1.radio1"></spring:message> </option>
							<option><spring:message code="split1.radio2"></spring:message> </option>
							<option><spring:message code="split1.radio3"></spring:message> </option>
							<option><spring:message code="split1.radio4"></spring:message> </option>
						</select>
					</div>
					 --%>
					<div id="split1_submit">
					<input type="button" value="Go" class="split1_section_button_Go"></input>
					</div>
				</div>
			</div>
		</section>

		<section id="split2_section">
			<div id="split2_section_responseTime">

				<!-- <div id="split2_section_pageSelectioncontainer">
					Select Page URL : <input type="text" class="split2_section_pageURL"></input>

					<input type="button" class="split2_section_button_search"
						value="Search"></input> 	
					<input type="button"
						class="split2_section_button_Go" value="Go"></input> <input
						type="button" class="split2_section_button_reset" value="Reset"></input>

				</div> -->
				<div id="split2_section_responseTimeGraphContainer"></div>

				<div id="split2_section_pageSelectionDialog">
					<table id="split2_section_pageSelectionGrid"></table>
					<div id="split2_section_pageSelectionGridPager"></div>
				</div>

				<!-- <div id="split2_section_correlation_buttons">
					<input type="button"
						class="split2_section_button_showRelatedVariables"
						value="Show Related Variables"></input>
					<input type="button" class="split2_section_button_prediction"
						value="Predictions"></input>
				</div>
 -->
			</div>
		</section>


		<section id="split3_section">
			<div id="split3_section_CorrelatedGraphs">
				<div id="split3_section_Labels">Related Variables</div>

				<div id="split3_section_CorrelatedGraphs_1"
					class="split3_section_CorrelatedGraph_1"></div>

				<div id="split3_section_CorrelatedGraphs_2"
					class="split3_section_CorrelatedGraph_2"></div>

				<!-- <div id="split3_section_CorrelatedGraphs_3"
					class="split3_section_CorrelatedGraph_3"></div> -->
			</div>

			<div id="split3_section_PredictionGraphs">
				<div id="split3_section_Labels">Prediction</div>

				<div id="split3_section_PredictionGraph"></div>
			</div>
		</section>
	</div>

	<div id="listView">
		<table id="listGridTable"></table>
		<div id="listGridPager"></div>
	</div>
	
	<%@ include file="../common/footer.jsp"%>
	</div>
</body>
</html>