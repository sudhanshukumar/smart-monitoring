<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
	<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!doctype html>
<html lang="en">
<head>

<link rel="shortcut icon" href="../../assets/ico/favicon.ico">
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

<title>SmartIT</title>

<!-- Bootstrap Styles -->
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap-responsive.css">
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap-theme.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/common/ui.jqgrid.css" />
<link rel="stylesheet" type="text/css" href="css/common/jquery-ui.min.css" />


<link rel="stylesheet" href="css/custom/specific/nav.css">

<script src="scripts/common/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="scripts/common/jquery.jqGrid.min.js"></script>
<script src="scripts/common/jquery-ui.min.js"></script>
<script type="text/javascript" src="scripts/common/jquery-ui.js"></script>
<script type="text/javascript" src="scripts/common/grid.locale-en.js"></script>
<script type="text/javascript" src="scripts/common/jquery.datetimepicker.js"></script>

<link href="scripts/c3-0.4.10/c3.css" rel="stylesheet" type="text/css" charset="utf-8">

<!-- Jquery css -->
<!-- <link rel="stylesheet" type="text/css" href="css/common/jquery-ui.css" />
<link href="css/common/jquery-ui-1.9.1.custom.css" rel="stylesheet" /> -->
<link rel="stylesheet" type="text/css" href="css/common/jquery.datetimepicker.css" />

<!-- Jquery scripts -->
<!-- <script type="text/javascript" src="assets/js/jquery-1.9.1.min.js"></script> -->



<!-- C3 scripts -->
<script src="scripts/d3-master/d3.js" charset="utf-8"></script>
<script src="scripts/c3-0.4.10/c3.js" charset="utf-8"></script>

<!-- API for generating Canvas Object -->
<script type="text/javascript" src="scripts/common/exportData/canvas2image.js"></script>
<script type="text/javascript" src="scripts/common/exportData/html2canvas.js"></script>
<script type="text/javascript" src="scripts/common/exportData/base64.js"></script>

<!-- API for generating PDF using Canvas Object -->
<script src="http://mrrio.github.io/jsPDF/dist/jspdf.debug.js"></script>

<!-- API for resizing Canvas Object -->
<script type="text/javascript" src="scripts/common/canvas/worker-handler.js"></script>
<script type="text/javascript" src="scripts/common/canvas/worker-hermite.js"></script>
<script type="text/javascript" src="scripts/common/canvas/hermite.js"></script>

<script type="text/javascript" src="scripts/common/exportData/jquery-dateFormat.min.js"></script>

<!-- Custom javascripts -->
<script type="text/javascript" src="scripts/custom/list_dashboard.js"></script>
<!-- <script type="text/javascript" src="scripts/common/menu/dropdowntabs.js"></script> -->

<!-- script for masking and unmasking -->
<script type="text/javascript" src="scripts/common/jquery.loadmask.js"></script>

<link rel="stylesheet" type="text/css" href="css/common/popupstyle.css" />
<link rel="stylesheet" type="text/css" href="css/common/nav.css" />

<!-- Highcharts script -->
<script src="scripts/common/highstocks.js"></script>
<script src="scripts/common/highcharts.js"></script>

<script src="scripts/custom/tab_menu.js"></script>

<% String session_val = (String)session.getAttribute("modules"); %>

<script type="text/javascript">
	session_obj= '<%=session_val%>';
</script>

</head>
<body style="font-size:11px!important;">
	<div id="mainDiv" class="container-fluid" style="height:100%">
		<%@ include file="../common/header.jsp"%>
		 <nav class="navbar navbar-inverse" style="margin-top: 0px">
	      <ul class="nav navbar-nav navbar-xs">
	        <li id="Dashboard"><a href="dashboard.html">Dashboard<span class="sr-only">(current)</span></a></li>
	        <li id="AlertHistory"><a href="problemHistory.html">Alert History</a></li>
	        <!-- <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Manage<span class="caret"></span></a>
	          <ul class="dropdown-menu">
	            <li><a href="#">Manage Device</a></li>
	            <li><a href="#">Manage Threshold</a></li>
	          </ul>
	        </li> -->
	        <li id="LogAnalysis"><a href="query.html">Log Analysis</a></li>
	        <li id="LogCorrelation"><a href="correlation.html">Log Correlation</a></li>
	        <li id="VariableAnalysis"><a href="variableAnalysis.html"><spring:message code="label.admin.menu5"></spring:message></a></li>
	        <li  class="active" class="dropdown" id="Reports">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reports <span class="caret"></span></a>
	          <ul class="dropdown-menu">
	            	<li id="CreateReport"><a href="charts.html">Create Report</a></li>
		            <li id="CreateDashboard"><a href="createuserdashboard.html">Create Dashboard</a></li>
		            <li id="ViewDashboard"><a href="list_dashboard.html">View Dashboard</a></li>
	          </ul>
	        </li>
	      </ul>
	  </nav> 
		
	  
	<div class="container-fluid innerContainer">	
		<div class="col-md-3" style="background-color:#e0e0e0; height:460px;">
			<div class="row" style=" margin-top: 20px; margin-left: .05px;">
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="col-sm-4" >Application:</label>
						<div class="col-sm-8">
							<select  class="form-control" id="application" style="width:165px;">
							</select>
						</div>
						<!-- <input type="button" id="exportToPPTButton" value="Export to PPT" class='btn btn-primary' onclick="exportDashBoardToPPT()"> -->
						<div id="editor"></div>
					</div>
				</form>
			</div>
			<div class="row"  style="margin-left: .05px;">
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="col-sm-4" >Dashboards:</label>
						<div class="col-sm-8">
							<select class="form-control" id="dashboards" style="width:165px;">
							</select>
						</div>
					</div>
				</form>	
			</div>
			
			<div class = "btn-group">
				<!-- <input type="button" class="btn btn-primary btn-sm" style=" margin-left: 55px;" id="exportToPDFButton" value="Export to PDF" onclick="exportDashBoardToPDF()"> -->
				<input type="button" class="btn btn-primary btn-sm" style="margin-left: 152px;" id="delete_Dashboard" value="Delete dashboard" onclick="deleteDashboard()">
	        </div>
	        <div id="confirmDelete"></div>
		</div>
		<div class="col-md-9" style="height:460px; overflow: auto">
			<div id="dashboard_element_container" class="container">
				<div id="chart_container" class="row"></div>
			</div>
			<div id='filterform'>
				<a href="#" class="close"><img src="images/common/close_pop.png" class="btn_close" title="Close Window" alt="Close" /></a>
				<table>
					<tr>
						<td>
							<label style='padding: 10px 0px 10px 40px; background: #00000'>From:</label>
						</td>
						<td>
							<div class='inner-addon left-addon'>
								<i class='glyphicon glyphicon-calendar'></i> 
								<input class='form-control' type='text' id='from_datepicker' style="position:relative; z-index: 99999999">
							</div>
							<div class="xdsoft_datetimepicker xdsoft_ xdsoft_noselect" style="display: block; left: 695.967px; top: 210.15px; position: absolute;">
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<label class='col-sm-4' style='padding: 10px 0px 10px 40px'>To:</label>
						</td>
						<td>
							<div class='inner-addon left-addon'>
								<i class='glyphicon glyphicon-calendar'></i>
								<input class='form-control' type='text' id='to_datepicker'>
							</div>
						</td>
					</tr>
				</table>
			</div>
			<div id="confirmDelete_reports"></div>
			<div id="alertDialog" title="Alert"></div>
</div>
	</div>
	<%@ include file="../common/footer.jsp"%>
	</div>
</body>
</html>