/**
 * 
 */

function openAlert(msg){
	
	var d = document.getElementById("main_container");
	
	$("#alert").dialog({
	    resizable: false,
	    autoOpen: false,
	    height:160,
	    width: 300,
	    position:  'center',
	    modal: true,
	    buttons: {
         
          "OK": function() {
              $(this).dialog("close");
              $(this).css({float:"center"});
              d.style.opacity=1.0;
          }
      }
		});
	
	$(".ui-dialog-titlebar-close").show();
	$(".ui-dialog-title").text('');
	$(".ui-dialog-title").text('Alert!');
	$(".ui-dialog-content").text('');
	$(".ui-dialog-content").html(msg);
	$("#alert").css('opacity', '1.0');
	$("#alert").dialog("open");
	
}