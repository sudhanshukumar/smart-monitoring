/**
 * 
 */
package com.lnt.smartit.models;

import java.util.ArrayList;
import java.util.List;

import com.lnt.smartit.models.FileSystem;

/**
 * @author 299273
 *
 */
public class AMQWinDisk implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<FileSystem> fileSystem = new ArrayList<FileSystem>();
	private Double totalDiskSize;
	private Double totalDiskUsed;
	private Double totalDiskAvail;
	private Double totalUsePercent=0.0;
	private double warning_High;
	private double critical_High;
	
	public double getWarning_High() {
		return warning_High;
	}
	public void setWarning_High(double warning_High) {
		this.warning_High = warning_High;
	}
	public double getCritical_High() {
		return critical_High;
	}
	public void setCritical_High(double critical_High) {
		this.critical_High = critical_High;
	}
	public List<FileSystem> getFileSystem() {
		return fileSystem;
	}
	public void setFileSystem(List<FileSystem> fileSystem) {
		this.fileSystem = fileSystem;
	}
	public Double getTotalDiskSize() {
		return totalDiskSize;
	}
	public void setTotalDiskSize(Double totalDiskSize) {
		this.totalDiskSize = totalDiskSize;
	}
	public Double getTotalDiskUsed() {
		return totalDiskUsed;
	}
	public void setTotalDiskUsed(Double totalDiskUsed) {
		this.totalDiskUsed = totalDiskUsed;
	}
	public Double getTotalDiskAvail() {
		return totalDiskAvail;
	}
	public void setTotalDiskAvail(Double totalDiskAvail) {
		this.totalDiskAvail = totalDiskAvail;
	}
	public Double getTotalUsePercent() {
		return totalUsePercent;
	}
	public void setTotalUsePercent(Double totalUsePercent) {
		this.totalUsePercent = totalUsePercent;
	}
}
