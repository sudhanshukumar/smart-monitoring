/**
 * 
 */
package com.lnt.smartit.models;

import java.io.Serializable;

/**
 * @author 299273
 *
 */
public class CPU implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Double loadAverage5min;
	private Double loadAverage10min;
	private Double loadAverage15min;
	private int dataReceivedTime_hours;
	
	public int getDataReceivedTime_hours() {
		return dataReceivedTime_hours;
	}

	public void setDataReceivedTime_hours(int dataReceivedTime_hours) {
		this.dataReceivedTime_hours = dataReceivedTime_hours;
	}

	public Double getLoadAverage5min() {
		return loadAverage5min;
	}

	public void setLoadAverage5min(Double loadAverage5min) {
		this.loadAverage5min = loadAverage5min;
	}

	public Double getLoadAverage10min() {
		return loadAverage10min;
	}

	public void setLoadAverage10min(Double loadAverage10min) {
		this.loadAverage10min = loadAverage10min;
	}

	public Double getLoadAverage15min() {
		return loadAverage15min;
	}

	public void setLoadAverage15min(Double loadAverage15min) {
		this.loadAverage15min = loadAverage15min;
	}

	@Override
	public String toString() {
		return "CPU [loadAverage5min=" + loadAverage5min
				+ ", loadAverage10min=" + loadAverage10min
				+ ", loadAverage15min=" + loadAverage15min + "]";
	}
}
