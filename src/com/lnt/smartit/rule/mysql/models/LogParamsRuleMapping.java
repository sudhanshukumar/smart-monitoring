/**
 * 
 */
package com.lnt.smartit.rule.mysql.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.lnt.smartit.mysql.model.LogParams;

/**
 * @author 10612068
 *
 */
@Entity
@Table(name="app_log_param_rule_xref")
public class LogParamsRuleMapping implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name="id")
	@Id
	@GeneratedValue
	private int paramRuleMappingId;
	
	@ManyToOne
	@JoinColumn(name = "app_log_param_id", referencedColumnName = "id")
	private LogParams logParam;
	
	@ManyToOne
	@JoinColumn(name = "rule_id", referencedColumnName = "id")
	private RuleItem ruleItem;
	
	@Column(name="filter_duration")
	private long filterDuration;
	
	@Column(name="description")
	private String description;
	
	@Column(name = "itsm_req_flag")
	private String itsmReqFlag;
	
	@Column(name = "deduplication_window")
	private long deduplicationWindow;
	
	//10614080 on 06/03/2016 start
	@Column(name="notification_flag")
	private String notificationflag;
	
	@Column(name="email_to")
	private String emailTo;
	
	@Column(name="email_cc")
	private String emailCC;
	
	//10614080 on 06/03/2016 end
	
	//10612175 on 06/04/2017 maintenance window start
	@Column(name="fromMW")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fromMW;
	
	@Column(name="toMW")
	@Temporal(TemporalType.TIMESTAMP)
	private Date toMW;
	
	public Date getFromMW() {
		return fromMW;
	}
	public void setFromMW(Date fromMW) {
		this.fromMW = fromMW;
	}
	public Date getToMW() {
		return toMW;
	}
	public void setToMW(Date toMW) {
		this.toMW = toMW;
	}	
	//10612175 on 06/04/2017 maintenance window end
	
	public long getDeduplicationWindow() {
		return deduplicationWindow;
	}
	public void setDeduplicationWindow(long deduplicationWindow) {
		this.deduplicationWindow = deduplicationWindow;
	}
	public String getItsmReqFlag() {
		return itsmReqFlag;
	}
	public void setItsmReqFlag(String itsmReqFlag) {
		this.itsmReqFlag = itsmReqFlag;
	}
	public int getParamRuleMappingId() {
		return paramRuleMappingId;
	}
	public void setParamRuleMappingId(int paramRuleMappingId) {
		this.paramRuleMappingId = paramRuleMappingId;
	}
	public LogParams getLogParam() {
		return logParam;
	}
	public void setLogParam(LogParams logParam) {
		this.logParam = logParam;
	}
	public RuleItem getRuleItem() {
		return ruleItem;
	}
	public void setRuleItem(RuleItem ruleItem) {
		this.ruleItem = ruleItem;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public long getFilterDuration() {
		return filterDuration;
	}
	public void setFilterDuration(long filterDuration) {
		this.filterDuration = filterDuration;
	}
	
	//10614080 on 06/03/2016 start
	public String getNotificationflag() {
		return notificationflag;
	}
	public void setNotificationflag(String notificationflag) {
		this.notificationflag = notificationflag;
	}
	public String getEmailTo() {
		return emailTo;
	}
	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}
	public String getEmailCC() {
		return emailCC;
	}
	public void setEmailCC(String emailCC) {
		this.emailCC = emailCC;
	}
	
	//10614080 on 06/03/2016 end
	
}
