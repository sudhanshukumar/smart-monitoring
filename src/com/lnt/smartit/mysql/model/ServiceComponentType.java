package com.lnt.smartit.mysql.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="service_comp_type")
public class ServiceComponentType implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="code")
	private String serviceCompTypeId;
	
	@Column(name="name")
	private String serviceCompTypeName;
	
	@Column(name="description")
	private String serviceCompTypeDescription;

	public String getServiceCompTypeId() {
		return serviceCompTypeId;
	}

	public void setServiceCompTypeId(String serviceCompTypeId) {
		this.serviceCompTypeId = serviceCompTypeId;
	}

	public String getServiceCompTypeName() {
		return serviceCompTypeName;
	}

	public void setServiceCompTypeName(String serviceCompTypeName) {
		this.serviceCompTypeName = serviceCompTypeName;
	}

	public String getServiceCompTypeDescription() {
		return serviceCompTypeDescription;
	}

	public void setServiceCompTypeDescription(String serviceCompTypeDescription) {
		this.serviceCompTypeDescription = serviceCompTypeDescription;
	}
	
}
