package com.lnt.smartit.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lnt.smartit.mysql.model.Alertmessageinfo;
import com.lnt.smartit.services.LogService;
import com.lnt.smartit.services.ProblemHistoryService;
import com.lnt.smartit.solr.pojo.JqGridData;

@Controller
public class HistoryController {
	@Autowired
	private ProblemHistoryService historyService;
	
	@Autowired
	private LogService logService;
	private static Logger logger = Logger.getLogger(HistoryController.class);

	/**************Changes done by Pinakin on 21st August Starts here******************/
	/*Method definition is changed. pageNo and pageRowCount parameters are added
	 * to achieve pagination at server side. Page No and page row count would be
	 * sent from the UI as a part of URL of this controller.*/
	@RequestMapping(value = "populateproblemHistoryGrid.html")
	public @ResponseBody JqGridData getProblemHistory(
		/*@ModelAttribute(value = "group")*/ @RequestBody JqGridData jqGridData,HttpServletRequest request) {
		logger.debug(jqGridData.toString());
		List<Alertmessageinfo> historyList = historyService.getProblemHistory(jqGridData.getPage(), jqGridData.getRows(), false);
		jqGridData.setGridData(historyList.toArray());
		jqGridData.setTotal(historyService.getProblemHistoryLastPageNo(jqGridData.getRows(), false));
	    jqGridData.setRecords(historyService.getProblemHistoryRecordCount());
		return jqGridData;
	}
	/**************Changes done by Pinakin on 21st August Ends here******************/
	
	/*
	 * This method added for criteria search for AlertHistory.pageNo and pageRowCount parameters are added
	 * to achieve pagination at server side. And other parameters for search.
	 */
	@RequestMapping(value = "populateproblemHistoryGrid2.html")
	public @ResponseBody JqGridData getProblemHistoryWithCriteria(@RequestBody String queryParams,HttpServletRequest request){
//		System.out.println("Entry in Controller = "+System.currentTimeMillis());
		logger.debug("HistoryController :: getProblemHistoryWithCriteria :: queryparams ="+queryParams);
		HttpSession session = request.getSession();
		if(session.getAttribute("appId")==null){
			String contextPath = request.getContextPath();
			String[] temp = contextPath.split("_");
			String appId = logService.getAppIdByName(temp[temp.length-1]);
			String appName=temp[temp.length-1];
			session.setAttribute("appId",appId);
			session.setAttribute("appname", appName);
		}
		JSONObject json;
		JqGridData jqGridData = new JqGridData();
		try {
			json = new JSONObject(queryParams);
			jqGridData  = historyService.getProblemHistoryWithCriteria(Integer.parseInt(json.get("page").toString()), Integer.parseInt(json.get("rows").toString()), false,session.getAttribute("appname").toString(), json.get("hostname").toString() ,json.get("parametername").toString(), json.get("alerttype").toString(),json.get("fromLogDate").toString(),json.get("toLogDate").toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
//		System.out.println("returning controller = "+System.currentTimeMillis());
		return jqGridData;
	}
	
	@RequestMapping(value = "getUniqueHostnames.html")
	public @ResponseBody List<String> getUniqueHostnames(){
		List<String> hostnames = historyService.getDistinctHostnames();
		return hostnames;
	}
	
	@RequestMapping(value = "getUniqueAlerttypes.html")
	public @ResponseBody List<String> getUniqueAlerttypes(){
		List<String> hostnames = historyService.getDistinctAlertTypes();
		return hostnames;
	}
	
	@RequestMapping(value = "getUniqueParameters.html")
	public @ResponseBody List<String> getUniqueParamters(){
		List<String> hostnames = historyService.getDistinctParameters();
		return hostnames;
	}
}
