package com.lnt.smartit.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lnt.smartit.mysql.model.ThresholdLogSize;
import com.lnt.smartit.services.ThresholdLogSizeService;
import com.lnt.smartit.solr.pojo.JqGridData;

@Controller
public class LogMasterController {

	@Autowired
	private ThresholdLogSizeService thresholdLogSizeService; 
	private static Logger logger = Logger.getLogger(NotificationGroupController.class);

	/**
	 * @author 10607228 addNewLog redirects to add new Log
	 *         Page
	 * */
	@RequestMapping("addNewLog.html")
	public String addNewLog(ModelMap map) {
		return "Manage/new/newLog";
	}

	/**
	 * @author 298038
	 * manageLog redirects to add manageLog page
	 * */
	@RequestMapping("manageLog.html")
	public String manageLog(ModelMap map) {
		logger.debug("Inside controller ..........");
		return "Manage/manageLog";
	}

	@RequestMapping(value = "populateLogGrid.html", method = RequestMethod.POST)
	public @ResponseBody JqGridData populateLogGrid(HttpServletRequest request,JqGridData gridPojo) {
		System.out.println("Entering LogMasterController-->populateLogGrid (POST)");
		List<ThresholdLogSize> thresholdLogSizeRecordList = new ArrayList<ThresholdLogSize>();
		try {
			thresholdLogSizeRecordList = thresholdLogSizeService.getAllThresholdLogSizeRecords();
			for (ThresholdLogSize thresholdLogSizeRecord : thresholdLogSizeRecordList) {
				logger.debug(thresholdLogSizeRecord);
			}
			gridPojo.setGridData(thresholdLogSizeRecordList.toArray());
			gridPojo.setRows(thresholdLogSizeRecordList.size());
			gridPojo.setRecords(thresholdLogSizeRecordList.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return gridPojo;
	}

	@RequestMapping(value = "addLogOnSubmit.html", method = RequestMethod.POST)
	public @ResponseBody String addLogOnSubmit(@Valid ThresholdLogSize thresholdLogSizeRecord,BindingResult bindingResult,
			HttpSession session,HttpServletRequest request) {
		String logMsg = "";
		logger.debug("Entering LogMasterController-->addLogOnSubmit()");
		String hostName = request.getParameter("hostName");
		String logFileName = request.getParameter("logFileName");
		String warningThresholdSize = request.getParameter("warningThresholdSize");
		String criticalThresholdSize = request.getParameter("criticalThresholdSize");
		String action = request.getParameter("action");

		thresholdLogSizeRecord.setHostName(hostName);
		thresholdLogSizeRecord.setLogFileName(logFileName);


		if(action.equalsIgnoreCase("edit")) 
		{
			String idString = request.getParameter("id");
			thresholdLogSizeRecord.setId(idString.toString());
			thresholdLogSizeRecord = thresholdLogSizeService.getThresholdLogSizeRecordById(thresholdLogSizeRecord);

			if (thresholdLogSizeRecord.getHostName().equalsIgnoreCase(hostName) && thresholdLogSizeRecord.getLogFileName().equalsIgnoreCase(logFileName)) 
			{
				thresholdLogSizeRecord.setWarningThresholdSize(Double
						.parseDouble(warningThresholdSize));
				thresholdLogSizeRecord.setCriticalThresholdSize(Double
						.parseDouble(criticalThresholdSize));
				logMsg = "Log details have been updated Successfully !! ";
			}
			else
			{
				thresholdLogSizeRecord.setHostName(hostName);
				thresholdLogSizeRecord.setLogFileName(logFileName);
				if(thresholdLogSizeService.exists(thresholdLogSizeRecord))
				{
					logMsg = "Record with same Host name & Log file name already exists...!!";
					return logMsg;

				}
				else
				{
					thresholdLogSizeRecord.setWarningThresholdSize(Double
							.parseDouble(warningThresholdSize));
					thresholdLogSizeRecord.setCriticalThresholdSize(Double
							.parseDouble(criticalThresholdSize));
					logMsg = "Log details have been updated Successfully !! ";
					
				}
			}
		} 
		else
		{
			if(thresholdLogSizeService.exists(thresholdLogSizeRecord))
			{
				logMsg = "Record with same Host name & Log file name already exists...!!";
				return logMsg;

			}
			else
			{
				String idString =request.getParameter("id");

				thresholdLogSizeRecord = new ThresholdLogSize();
				if(null == idString || idString.equalsIgnoreCase("null"))
				{
					Integer id = ((int) (Math.random() * 1000));
					thresholdLogSizeRecord.setId(id.toString());

				}
				else
				{
					thresholdLogSizeRecord.setId(idString);	
				}
				thresholdLogSizeRecord.setHostName(hostName);
				thresholdLogSizeRecord.setLogFileName(logFileName);
				thresholdLogSizeRecord.setWarningThresholdSize(Double.parseDouble(warningThresholdSize));
				thresholdLogSizeRecord.setCriticalThresholdSize(Double.parseDouble(criticalThresholdSize));
				logMsg = "New Log details have been saved Successfully !! ";

			}

		}



		logger.debug("thresholdLogSize " + thresholdLogSizeRecord.toString());

		try {
			thresholdLogSizeService.saveNewThresholdLogSizeRecord(thresholdLogSizeRecord);
		} catch (Exception e) {
			e.printStackTrace();
			logMsg = "New Log details save failed due to "
					+ e.getMessage();
		}

		return logMsg;
	}

	/**
	 * @author 298038
	 * @param SoftwareMaster
	 * @return
	 */ 
	@RequestMapping(value = "deleteLog.html", method = RequestMethod.POST)
	public @ResponseBody String deleteLog(ThresholdLogSize thresholdLogSizeRecord){
		logger.debug("Entering LogMasterController-->deleteLog()");
		logger.debug("thresholdLogSizeRecord "+thresholdLogSizeRecord);
		ThresholdLogSize logRec;
		String deleteLogMsg;
		try{
			logRec=thresholdLogSizeService.getThresholdLogSizeRecordById(thresholdLogSizeRecord);
			deleteLogMsg=thresholdLogSizeService.deleteThresholdLogSizeRecord(logRec);
			deleteLogMsg="Record deleted Successfully..!!";
		}catch(Exception e ){
			e.printStackTrace();
			deleteLogMsg="Delete Log failed due to "+e.getMessage();
		}
		return deleteLogMsg;
	}

	/**
	 * @author 298038
	 * @param thresholdLogSizeRecord
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "editLog.html", method = RequestMethod.POST)
	public @ResponseBody ThresholdLogSize editLog(ThresholdLogSize thresholdLogSizeRecord,HttpServletRequest request) {
		logger.debug("Entering LogMasterController-->editLog()");
		logger.debug("softwareMaster "+thresholdLogSizeRecord);
		ThresholdLogSize logSizeRec;
		logSizeRec=thresholdLogSizeService.getThresholdLogSizeRecordById(thresholdLogSizeRecord);
		logger.debug(logSizeRec);
		return logSizeRec;
	}

	@RequestMapping(value = "previewLog.html", method = RequestMethod.POST)
	public @ResponseBody ThresholdLogSize previewLog(ThresholdLogSize thresholdLogSizeRecord,HttpServletRequest request) {
		logger.debug("Entering ApplicationMasterController-->previewLog()");
		logger.debug("SoftwareMaster "+thresholdLogSizeRecord);
		ThresholdLogSize logSizeRec=thresholdLogSizeService.getThresholdLogSizeRecordById(thresholdLogSizeRecord);
		logger.debug(logSizeRec);
		return logSizeRec;
	}
}
