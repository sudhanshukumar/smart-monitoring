package com.lnt.smartit.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
/**
 * 
 * @author 298038
 *
 */
public class AMQCEDError implements Serializable { 
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "AMQCEDError [errorCode=" + errorCode + ", errorMessage="
				+ errorMessage + ", hostname=" + hostname + "]";
	}
	private List<String> errorCode;
	private String errorMessage;
	private String hostname;
	private Date cedErrorDatetime;
	
	public List<String> getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(List<String> errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getHostname() {
		return hostname;
	}
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	public Date getCedErrorDatetime() {
		return cedErrorDatetime;
	}
	public void setCedErrorDatetime(Date cedErrorDatetime) {
		this.cedErrorDatetime = cedErrorDatetime;
	}
}
