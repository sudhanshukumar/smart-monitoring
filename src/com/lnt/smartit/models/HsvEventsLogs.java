package com.lnt.smartit.models;

public class HsvEventsLogs {
	
	private String ref;
	private String app;
	private String user;
	private int dbupdate;
	private String log_code;
	private String Date;
	private String server;
	private String file;
	private int line;
	private String version;
	private String message;
	private String log_type;
	public String getRef() {
		return ref;
	}
	public void setRef(String ref) {
		this.ref = ref;
	}
	public String getApp() {
		return app;
	}
	public void setApp(String app) {
		this.app = app;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public int getDbupdate() {
		return dbupdate;
	}
	public void setDbupdate(int dbupdate) {
		this.dbupdate = dbupdate;
	}
	public String getLog_code() {
		return log_code;
	}
	public void setLog_code(String log_code) {
		this.log_code = log_code;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}
	public int getLine() {
		return line;
	}
	public void setLine(int line) {
		this.line = line;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getLog_type() {
		return log_type;
	}
	public void setLog_type(String log_type) {
		this.log_type = log_type;
	}
	@Override
	public String toString() {
		return "HsvEventsLogs [ref=" + ref + ", app=" + app + ", user=" + user
				+ ", dbupdate=" + dbupdate + ", log_code=" + log_code
				+ ", Date=" + Date + ", server=" + server + ", file=" + file
				+ ", line=" + line + ", version=" + version + ", message="
				+ message + ", log_type=" + log_type + "]";
	}
}
