package com.lnt.smartit.mysql.dao;

import java.util.List;

import com.lnt.smartit.mysql.model.Roles;
import com.lnt.smartit.mysql.model.Users;

public interface UserDao {
	public List<Users> getAllUsers();
	public int addNewUser(Users user);
	public Users previewService(int userId);
	public String deleteUser(int userId);
	public void updateUser(Users user);
	public List<Roles> findRolesColl();
}
