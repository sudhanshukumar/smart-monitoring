package com.lnt.smartit.services;

import java.util.List;

import com.lnt.smartit.mysql.model.SoftwareMaster;

public interface SoftwareService {
	public List<SoftwareMaster> getAllSoftwareMaster();
	public void saveNewService(SoftwareMaster softwareMaster);
	public SoftwareMaster getApplicationMasterById(SoftwareMaster softMaster);
	public String deleteService(SoftwareMaster softMaster);
}
