package com.lnt.smartit.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.lnt.smartit.observer.ObserverI;

@Component
public class ObserverFactory {

  @Autowired
  private ObserverLookUp  observerLookUp;

  public ObserverI getSubscriber() {
      return observerLookUp.getAlertMessageSubscriber();

  }
}
