<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add device</title>

<!-- common css -->
<link href="css/custom/general/style1366.css" rel="stylesheet" />
<link href="css/custom/general/style1024.css" rel="stylesheet" />
<link href="css/custom/general/bluetabs.css" rel="stylesheet" />

<!-- import css -->
<link href="css/common/ui.jqgrid.css" rel="stylesheet" type="text/css" />
<link href="css/common/jquery-ui-1.9.1.custom.css" rel="stylesheet"
	type="text/css" />

<!-- jQuery scripts -->
<script src="scripts/common/jquery-1.9.1.js"></script>
<script src="scripts/common/grid.locale-en.js"></script>
<script src="scripts/common/jquery.jqGrid.min.js"></script>
<script src="scripts/common/jquery-ui.js"></script>

<!-- custom css -->
<link href="css/custom/specific/newDevice.css" rel="stylesheet" />

<!-- fonts -->
<link href='http://fonts.googleapis.com/css?family=Roboto:500'
	rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans'
	rel='stylesheet' type='text/css'>


<!-- custom script -->
<script src="scripts/custom/newDevice.js"></script>
<script src="scripts/common/common.js"></script>

<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
</style>
</head>
<body>
	<header>
		<%@ include file="../../common/header.jsp"%>
	</header>


	<nav>
		<%@ include file="../../Menu/admin.jsp"%>
	</nav>


	<div id="newDeviceInnerContainer" class="allInnerContainer">
		<div id="alert"></div>
		<div id="deviceDetails">
			<!-- this div will have all the text boxes and check for adding new device -->
			<div id="headingManageDevice" class="topic addDeviceClass"></div>

			<hr id="horizontalLine">
			<table id="addDevice" class="itPredictInp" cellspacing="14px"
				cellpadding="5px">
				<tr>
					<td class="col1"><label>Host Name <span
							class="mandatory">*</span></label></td>
					<td class="col2"><input type="text" id="deivce_hostname"
						placeholder="Fully Qualified Host Name" /></td>
					<!-- </tr>
				<tr> -->
				
					<td class="col1"><label>IP Address <span
							class="mandatory">*</span></label></td>
					<td class="col2"><input type="text" id="deivce_ipadress"
						placeholder="Ipv4" /></td>


				</tr>
				<tr>
					<td class="col1"><label>Installed OS  <span
							class="mandatory">*</span></label></td>
					<td class="col2"><select class="textAreaWidth"
						id="os_information">
							<c:forEach var="osList" items="${osList}">
								<option value=${osList}>${osList}</option>
							</c:forEach>
					</select></td>


					<td class="col1"><label>Application <span
							class="mandatory">*</span></label></td>
					<td class="col2"><select class="textAreaWidth"
						id="device_Applications" multiple>

							 <c:forEach var="applicationMaster" items="${applicationMaster}">
								<option value=${applicationMaster.key}>${applicationMaster.value}
								</option>
							</c:forEach> 
							
					</select></td>

				</tr>
				<tr>
					<td class="col1"><label>Description </label></td>
					<td class="col2"><textarea id="device_hostName_desc"
							placeholder="Give this host a meaningful description!"></textarea></td>
				</tr>

			</table>
			<hr id="horizontalLine">
		</div>

		<div id="add_device_buttons" class="buttonPosition">
			<table>
				<tr>
					<td>
						<button id="add_device_submit"
							class="buttonClass buttonClass_submit">Submit</button>
					</td>
					<td><button id="add_device_cancel"
							class="buttonClass buttonClass_cancel">Cancel</button></td>
				</tr>
			</table>

		</div>
		<div id="preview_device_buttons" class="buttonPosition">
			<table>
				<tr>
					<td>
						<button id="preview_ok"
							class="buttonClass buttonClass_submit">Ok</button>
					</td>
					
				</tr>
			</table>

		</div>
	</div>

<div id ="alert"></div>
	<div id="confirmEdit" title="Alert">
  <p>These Changes would be reflected after one business day</p>
</div>
	<footer>
		<%@ include file="../../common/footer.jsp"%>
	</footer>


</body>
</html>