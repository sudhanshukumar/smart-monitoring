package com.lnt.smartit.solr.pojo;

public class DashboardUpdate {

private String dashname;
private String divname;
private String startdate;
private String enddate;
private String startvalue;
private String endvalue;
private String numericgap;
private String chartname;


public String getChartname() {
	return chartname;
}
public void setChartname(String chartname) {
	this.chartname = chartname;
}
public String getDashname() {
	return dashname;
}
public void setDashname(String dashname) {
	this.dashname = dashname;
}
public String getDivname() {
	return divname;
}
public void setDivname(String divname) {
	this.divname = divname;
}
public String getStartdate() {
	return startdate;
}
public void setStartdate(String startdate) {
	this.startdate = startdate;
}
public String getEnddate() {
	return enddate;
}
public void setEnddate(String enddate) {
	this.enddate = enddate;
}
public String getStartvalue() {
	return startvalue;
}
public void setStartvalue(String startvalue) {
	this.startvalue = startvalue;
}
public String getEndvalue() {
	return endvalue;
}
public void setEndvalue(String endvalue) {
	this.endvalue = endvalue;
}
public String getNumericgap() {
	return numericgap;
}
public void setNumericgap(String numericgap) {
	this.numericgap = numericgap;
}



	
}
