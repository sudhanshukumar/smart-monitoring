package com.lnt.smartit.mysql.dao;

import java.util.List;
import java.util.Map;

import com.lnt.smartit.mysql.model.DeviceParams;
import com.lnt.smartit.mysql.model.EmailMaster;
import com.lnt.smartit.mysql.model.LogParams;
import com.lnt.smartit.mysql.model.ManageThreshold;
import com.lnt.smartit.mysql.model.NotificationGroup;
import com.lnt.smartit.mysql.model.ThresholdMaster;
import com.lnt.smartit.mysql.model.ThresholdNotifyGroupMapping;
import com.lnt.smartit.rule.mysql.models.ExpressionDataType;
import com.lnt.smartit.rule.mysql.models.OperationType;
import com.lnt.smartit.rule.mysql.models.RuleType;
import com.lnt.smartit.rule.mysql.models.RuleVO;

public interface ThresholdDao {
	public List<RuleVO> getThresholds(String appId);
	List<LogParams> getLogParams();
	List<DeviceParams> getAppDeviceParams();
//	public void editThresholds(ManageThreshold manageThresholds);
	public List<NotificationGroup> getNotificationGroup();
	public List<EmailMaster> getEmailMaster();
	public boolean deleteThreshold(String threshId);
	public ThresholdMaster getthresholdMasterById(ThresholdMaster thresholdMaster);
	public Map<String, String> getThresholdNotifyMapping(ThresholdMaster thresholdMaster);
	public String saveThresholdMaster(ThresholdMaster thresholdMaster);
	public String savenotificationGroup(NotificationGroup notificationGroup);
	public List<NotificationGroup> getAllNotificationGroup();
	public NotificationGroup getNotificationGroupById(NotificationGroup notificationGroup);
	public String deleteNotificationGroup(NotificationGroup notificationGroup);
	public void editNotificationGroup(NotificationGroup notificationgp);
	public List<ThresholdNotifyGroupMapping> getNotificationThresholdMappingById(String notificationGroupId);
	public void saveNotificationThresholdMapping(List<ThresholdNotifyGroupMapping> thresholdNotifyGroupMappingList);
	public void saveThresholdNotifyGroupMappingService(String thresholdMasterId, String thresholdGroupSelectedIds);
	public void deleteThresholdNotifyGroupMappingService(ThresholdNotifyGroupMapping thresholdNotifyGroupMapping);
	public String deleteThresholdMaster(ThresholdMaster threshold);
	public String saveThresholdsForLogs(ThresholdMaster thresholdMaster,String[] logFileName, String[] logCriticalThreshold,
			String[] logWarningThreshold);
	public boolean checkId(Integer thresholdId);
	public String saveThresholdsForServices(ThresholdMaster thresholdMaster,String[] serviceName);
	public List<Map<String,String>> previewThresholdForLog(String thresholdId);
	public List<Map<String,String>> previewThresholdForService(String thresholdId);
	public String updateThresholdMaster(ThresholdMaster thresholdMaster);
	public void editThresholdsForLog(ThresholdMaster thresholdMaster,String[] logWarningThreshold, String[] logCriticalThreshold);
	public String getThresholdMasterByDeviceIDandParamID(String deviceID,
			String paramID);
	public void editThresholds(RuleVO ruleVO);
	public List<LogParams> getLogParamsForHost(String hostname);
	public List<ExpressionDataType> getParameterTypeList();
	public List<RuleType> getAlertTypeList();
	public List<OperationType> getOperatorTypeList();
}

