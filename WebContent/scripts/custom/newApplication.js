$(document).ready(function() {
	document.getElementById("headingManageApplication").innerHTML="Add New Application";
	$("#preview_app_buttons").hide();
	$("#preview_ok").click(function(){
		window.location.href = "manageApplication.html";
	});
	$("#alert").hide();
	var action = getQueryVariable("action");
	var id=getQueryVariable("appId");
	
	if(action == "edit")
		{
		$("#preview_app_buttons").hide();
		document.getElementById("headingManageApplication").innerHTML="Edit Application";
		$.ajax({
			type : "POST",
			url : "editApplication.html",
			data : "applicationId=" + id, 
			success : function(response) {
				$('#applicationName').attr('readonly', false);
				$('#application_desc').attr('readonly', false);
				$('#applicationName').val(response.applicationName);
				$('#application_desc').val(response.description);	
			}
	        //cache: false
		});
		}
	
	if(action == "preview")
{
		$("#add_application_buttons").hide();
		$("#preview_app_buttons").show();
		document.getElementById("headingManageApplication").innerHTML="Preview Application";
		$.ajax({
		type : "POST",
		url : "previewApplication.html",
		data : "applicationId=" + id, 
		success : function(response) {
			$('#applicationName').val(response.applicationName);
			$('#application_desc').val(response.description);
			$('#applicationName').attr('readonly', true);
			$('#application_desc').attr('readonly', true);
		}
       // cache: false
	});
}
	
	
	$("#addApplication_submit").click(function() {
		addApplicationGroup_submit(id);
	});
	
	$("#addApplication_cancel").click(function() {
		window.location.href="manageApplication.html";
	});
});

function addApplicationGroup_submit(id) {
	
	var application_Name,application_desc,contactName,contactEmail,contact_Number;
	
	application_Name=$('#applicationName').val();
	application_desc=$('#application_desc').val();
	contactName=$('#contactName').val();
	contactEmail=$('#contactEmail').val();
	contact_Number=$('#contactNumber').val();
	if(id == false)
		{
		id = "null";
		}
	
	$.ajax({
		type : "POST",
		url : "addApplicationOnSubmit.html",
		data : 'applicationName='+application_Name +'&description='+application_desc +'&contactPerson='+contactName
		+'&contactPersonEmail='+contactEmail +'&contactNumber='+contact_Number+'&appId='+id,
		
		success : function(response) {
			if(response.indexOf("Name")>-1 ||response.indexOf("Description")>-1){
				openAlert(response);
			}else{
				alert(response);
				window.location.href="/manageApplication.html";
			}
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    	openAlert(jqXHR);
	    	openAlert(textStatus);
	    	openAlert(errorThrown);
	    }
	//	cache : false
	}); 
}

function getQueryVariable(variable)
{
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}
